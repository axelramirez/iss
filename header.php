<?php
date_default_timezone_set('America/Los_Angeles');
if (!isset($_SESSION)) {
    session_start();
}
if (!isset($preDir)) {
    $preDir = '';
}

ob_start();
if (!isset($charset)) {
    echo "<meta http-equiv = 'content-type' content = 'text/html; charset=iso-8859-1' />";
} else {
    echo "<meta charset='$charset' />";
}
?>
<link href="<?= $preDir ?>style/menu.css" type="text/css" rel="stylesheet">
<link href="<?= $preDir ?>style/login.css" type="text/css" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<script src="js/jquery-1.12.4.js"></script>

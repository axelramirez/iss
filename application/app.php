<?php
//Import Libraries
require_once("libraries/Gravity/Gravity.php");
//Import Controllers
require_once("controllers/Dashboard.php");
require_once("controllers/Inventory.php");
/*
 **************************************************************************************************
 *************************Name: Router Class
 *************************Description: Gestiona un conjunto de rutas http del servidor
 **************************************************************************************************
*/
class App{
  private $app;

  private $dashboard;
  private $inventory;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    /*
    **********************************************
    -------------Start sessions
    **********************************************
    */
    ob_start();
    session_start();
    /*
    **********************************************
    -------------App Gravity
    **********************************************
    */
    $this->app = new Gravity();
    /*
    **********************************************
    -------------Controllers
    **********************************************
    */
    $this->dashboard = new Dashboard();
    $this->inventory = new Inventory();
    /*
    **********************************************
    -------------Routes
    **********************************************
    */
    //********************************************
    //----------Logic
    //********************************************

    $this->app->get("dashboard/get", array($this->dashboard, "getInfoDashboard"));
    $this->app->get("ISS/get", array($this->inventory, "getProducts"));
    $this->app->get("ISS/search", array($this->inventory, "getProductsByFilter"));
    $this->app->put("ISS/update", array($this->inventory, "updateProduct"));
    $this->app->put("ISS/insert", array($this->inventory, "insertProduct"));
    $this->app->delete("ISS/delete", array($this->inventory, "deleteProduct"));
    $this->app->put("ISS/insertReceipt", array($this->inventory, "insertReceipt"));
    $this->app->put("ISS/insertShipping", array($this->inventory, "insertShipping"));
    $this->app->get("ISS/get/colors", array($this->inventory, "getColors"));
    $this->app->put("ISS/insert/color", array($this->inventory, "insertColor"));
    $this->app->put("ISS/update/color", array($this->inventory, "updateColor"));
    $this->app->delete("ISS/delete/color", array($this->inventory, "deleteColor"));
    $this->app->put("ISS/insert/set", array($this->inventory, "insertSet"));
    $this->app->get("ISS/get/set", array($this->inventory, "getSet"));

    //********************************************
    //----------System
    //********************************************
    $this->app->post("gravity_info", function(){
      header("content-type: application/json");
      echo json_encode(array(
        "code"     =>   200,
        "response" => array(
          "microframework" =>      "Gravity",
          "version"        =>        "0.0.1"
        )
      ));
    });
    ob_end_flush();
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de verificar que ruta fue ingresada en la url del navegador y checar si
   coincide con una ruta declarada en la API
   **************************************************************************************************
  */
  public function render(){
    $this->app->listen();
  }
}
?>

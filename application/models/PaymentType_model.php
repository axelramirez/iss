<?php
 require_once("adapters/MySQLConnection.php");
 /*
  **************************************************************************************************
  ***********************Name: PaymentType Class Model
  ***********************Description: Realiza las consultas correspondientes a la tabla payment_type
  *********************************** que es la que gestiona los tipos de pago (efectivo, transaccion, etc.)
  **************************************************************************************************
 */
class PaymentType_model{
  private $db;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todos los pagos
   **************************************************************************************************
  */
  public function get(){
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select * from payment_type;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>                           TRUE,
        "query"  => $query->fetch_all(MYSQLI_ASSOC)
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }


  public function getById($id){
    //Se ejecuta la consulta y se obtiene el resultado
    if($sentence = $this->db->prepare("select * from payment_type where payment_type_id = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $id);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>                  TRUE,
          "query"  => $query->fetch_assoc()
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
}
?>

<?php

require_once("adapters/MySQLConnection.php");


class Audit{
    private $db;
    private $model;

    public function __construct(){
        $this->db = MySQLConnection::getInstance()->getConnection();
    }

    private function cmpStr($type,$action,$startDate,$endDate,$number,$idcustomer,$idUser){
       return strcmp($type,"All") == 0 &&  strcmp($action,"All") == 0 && strcmp($startDate,"") == 0 && strcmp($endDate,"") == 0 && strcmp($number,"") == 0 && strcmp($idcustomer,"All") == 0 && strcmp($idUser,"All") == 0  ;
    }

    public function get($type,$action,$startDate,$endDate,$number,$idcustomer,$idUser,$numberPage){
        //Se define cuantos elementos se van a consultar
       $elementsByPage = 5;
       $isData =  $this->cmpStr($type,$action,$startDate,$endDate,$number,$idcustomer,$idUser);
       // printf($isData);
       //Se define desde donde se va a consultar
       $offset = ($numberPage - 1) * $elementsByPage;
       $filterType = '%' . $type . '%';
       $filterAction = '%' . $action . '%';
       $filterNumber = '%' . $number . '%';
       $filterIdCustomer = '%' . $idcustomer . '%';
       $filterIdUser = '%' . $iduser . '%';

       $where = "WHERE number LIKE '$filterNumber' AND action LIKE '$filterAction' AND type LIKE '$filterType' AND idcustomer LIKE '$filterIdCustomer' AND idUser LIKE '$filterIdUser' AND (date>= '$startDate' AND date<= '$endDate')";
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      $queryCount = $isData == 1 ? "SELECT COUNT(*) AS entries FROM auditlogview" : "SELECT COUNT(*) AS entries FROM auditlogview $where";
      // $queryCount = "SELECT COUNT(*) AS entries FROM auditlogview $where";
      if($sentence1 = $this->db->prepare("$queryCount;")){
       if($isData != 1){
            $sentence1->bind_param("sssssii",$type,$action,$startdate,$endDate,$number,$idcustomer,$idUser);
            $this->db->query("START TRANSACTION");
        }
         if($sentence1->execute()){
            $sentence1->bind_result($numberEntries);
            $sentence1->fetch();
            $sentence1->close();
          }
      }
      //Se calcula el numero de paginas que tendra la consulta
      $pages         =                                           $numberEntries / $elementsByPage;
      $numberPages   =                (Integer)( $pages > (Integer)$pages ? $pages + 1 : $pages );
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
     $command = $isData == 1 ? "SELECT * FROM auditlogview ORDER BY date DESC LIMIT $offset,$elementsByPage;;" : "SELECT * FROM auditlogview $where ORDER BY date DESC LIMIT $offset,$elementsByPage;;";
     if($sentence = $this->db->prepare($command)){
        //si hay datos que filtrar pasa la parametrizacion
        if($isData != 1){
          $sentence->bind_param("sssssii",$type,$action,$startdate,$endDate,$number,$idcustomer,$idUser);
          $this->db->query("START TRANSACTION");
        }
       //Se ejecuta la sentencia preparada
       if($sentence->execute()){
         //Se obtiene el resultado de la ejecucion de la sentencia
         $query = $sentence->get_result();
         //Se cierra la sentencia
         $sentence->close();
         //Se devuelve el resultado con la sentencia
         return array(
           "status" =>                  TRUE,
           "query"  => $query->fetch_all(MYSQLI_ASSOC),
           "numberEntries" => $numberEntries,
           "numberPages" => $numberPages
         );
       }
     }
     //Se devuelve el resultado con el mensaje de error
     return array(
       "status"  =>            FALSE,
       "message" => $this->db->error
     );
    }
}

?>

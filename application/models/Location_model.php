<?php
require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: Unit Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla docks
 **************************************************************************************************
 */
class Location_model{
  private $db;
  private $model;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }

  public function get($numberPage){
      //Se define cuantos elementos se van a consultar
     $elementsByPage = 5;
     //Se define desde donde se va a consultar
     $offset = ($numberPage - 1) * $elementsByPage;
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    $queryCount = "SELECT COUNT(*) AS entries FROM locations";
    if($query = $this->db->query("$queryCount;")){
      //Se obtiene el numero de resultados que arrojo la consulta
      $query         =            $query->fetch_assoc();
      $numberEntries = (Integer)$query["entries"];
    }
    //Se calcula el numero de paginas que tendra la consulta
    $pages         =                                           $numberEntries / $elementsByPage;
    $numberPages   =                (Integer)( $pages > (Integer)$pages ? $pages + 1 : $pages );
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("SELECT * FROM locations LIMIT $offset,$elementsByPage;")){
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC),
         "numberEntries" => $numberEntries,
         "numberPages" => $numberPages
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
   **************************************************************************************************
   Este metodo inserta un nuevo location
   **************************************************************************************************
  */
  public function insert($locationName){
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("INSERT INTO locations (location, createdTimestamp) VALUES (?, NOW());")){
        //Se asignan parametros al '?'
        $sentence->bind_param("s", $locationName);
        $this->db->query("START TRANSACTION");
        //Se ejecuta la sentencia preparada
        if ($sentence->execute()) {
            if($sentence->close()){
             //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
             $this->db->query("COMMIT");
             //Se devuelve status correcto
             return TRUE;
            }else{
             //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
             $this->db->query("ROLLBACK");
             //Se devuelve el resultado con el mensaje de error
             return FALSE;
            }
        }
    }
}
/*
 **************************************************************************************************
 Este metodo elimina un location
 **************************************************************************************************
*/
public function delete($idLocation){
    $status = false;
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("DELETE FROM locations WHERE idLocation = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $idLocation);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        $status = ( $sentence->affected_rows > 0 ) ? TRUE : FALSE;
        $sentence->close();
      }
    }
  return $status;
}
/*
 **************************************************************************************************
 Este metodo obtiene los resultado de la busqueda
 **************************************************************************************************
 */
 public function searchbypagination($numberPage, $filter){
     //Se define cuantos elementos se van a consultar
    $elementsByPage = 5;
    $filterLike = '%' . $filter . '%';
    // print($filterLike);
    //Se define desde donde se va a consultar
    $offset = ($numberPage - 1) * $elementsByPage;
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   $queryCount = "SELECT COUNT(*) AS entries FROM locations WHERE location LIKE '$filterLike'";
   if($query = $this->db->query("$queryCount;")){
     //Se obtiene el numero de resultados que arrojo la consulta
     $query         =            $query->fetch_assoc();
     $numberEntries = (Integer)$query["entries"];
   }
   //Se calcula el numero de paginas que tendra la consulta
   $pages         =                                           $numberEntries / $elementsByPage;
   $numberPages   =                (Integer)( $pages > (Integer)$pages ? $pages + 1 : $pages );
  //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
  if($sentence = $this->db->prepare("SELECT * FROM locations WHERE location LIKE '$filterLike' LIMIT $offset,$elementsByPage;")){
    //Se ejecuta la sentencia preparada
    if($sentence->execute()){
      //Se obtiene el resultado de la ejecucion de la sentencia
      $query = $sentence->get_result();
      //Se cierra la sentencia
      $sentence->close();
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>                  TRUE,
        "query"  => $query->fetch_all(MYSQLI_ASSOC),
        "numberEntries" => $numberEntries,
        "numberPages" => $numberPages
      );
    }
  }
  //Se devuelve el resultado con el mensaje de error
  return array(
    "status"  =>            FALSE,
    "message" => $this->db->error
  );
 }
}
?>

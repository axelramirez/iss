<?php
require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: Unit Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla po
 **************************************************************************************************
 */
class Carrier_model{
  private $db;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }

  /*
   **************************************************************************************************
   Este metodo inserta un nuevo transportista
   **************************************************************************************************
  */
  public function insert($newcarrier){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("INSERT INTO carrier(name) values(?)")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("s", $name["name"]);

      $this->db->query("START TRANSACTION");

      if($sentence->close()){
       //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
       $this->db->query("COMMIT");
       //Se devuelve status correcto
       return TRUE;
      }else{
       //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
       $this->db->query("ROLLBACK");
       //Se devuelve el resultado con el mensaje de error
       return FALSE;
      }
  }
}
/*
 **************************************************************************************************
 Este metodo trae los transportistas ordenados por nombre
 **************************************************************************************************
*/
  public function get(){
      //Se ejecuta la consulta y se obtiene el resultado
      if($sentence = $this->db->prepare("SELECT * FROM carrier ORDER BY name;")){
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
        $query = $sentence->get_result();
          //Se cierra la sentencia
          $sentence->close();
          //Se devuelve el resultado con la sentencia
          return array(
            "status" =>                     TRUE,
            "query"  => $query->fetch_all(MYSQLI_ASSOC)
          );
        }
      }
      //Se devuelve el resultado con el mensaje de error
      return array(
        "status"  =>            FALSE,
        "message" => $this->db->error
      );
  }
  public function getByOptions(){
      //Se ejecuta la consulta y se obtiene el resultado
      if($sentence = $this->db->prepare("SELECT carrierid as value, name FROM carrier ORDER BY name;")){
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
        $query = $sentence->get_result();
          //Se cierra la sentencia
          $sentence->close();
          //Se devuelve el resultado con la sentencia
          return array(
            "status" =>                     TRUE,
            "query"  => $query->fetch_all(MYSQLI_ASSOC)
          );
        }
      }
      //Se devuelve el resultado con el mensaje de error
      return array(
        "status"  =>            FALSE,
        "message" => $this->db->error
      );
  }
  public function saveSimple($name){
      //Se ejecuta la consulta y se obtiene el resultado
      if($sentence = $this->db->prepare("insert into carrier(name) values(?);")){
        $sentence->bind_param('s', $name);
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
        $query = $sentence->get_result();
          //Se cierra la sentencia
          $sentence->close();
          //Se devuelve el resultado con la sentencia
          return array(
            "status" =>                     TRUE,
            "last_id"  => $this->db->insert_id
          );
        }
      }
      //Se devuelve el resultado con el mensaje de error
      return array(
        "status"  =>            FALSE,
        "message" => $this->db->error
      );
  }

  public function getByName($name){
      //Se ejecuta la consulta y se obtiene el resultado
      if($sentence = $this->db->prepare("select carrierid, name from carrier where name like ?")){
        $sentence->bind_param('s', $name);
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
        $query = $sentence->get_result();
          //Se cierra la sentencia
          $sentence->close();
          //Se devuelve el resultado con la sentencia
          return array(
            "status" =>                     TRUE,
            "query"  => $query->fetch_assoc()
          );
        }
      }
      //Se devuelve el resultado con el mensaje de error
      return array(
        "status"  =>            FALSE,
        "message" => $this->db->error
      );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todas las descripciones de carrier por limit
   **************************************************************************************************
   */
   public function getbypagination($numberPage){
       //Se define cuantos elementos se van a consultar
      $elementsByPage = 5;
      //Se define desde donde se va a consultar
      $offset = ($numberPage - 1) * $elementsByPage;
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
     $queryCount = "SELECT COUNT(*) AS entries FROM carrier";
     if($query = $this->db->query("$queryCount;")){
       //Se obtiene el numero de resultados que arrojo la consulta
       $query         =            $query->fetch_assoc();
       $numberEntries = (Integer)$query["entries"];
     }
     //Se calcula el numero de paginas que tendra la consulta
     $pages         =                                           $numberEntries / $elementsByPage;
     $numberPages   =                (Integer)( $pages > (Integer)$pages ? $pages + 1 : $pages );
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("SELECT * FROM carrier ORDER BY name LIMIT $offset,$elementsByPage;")){
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>                  TRUE,
          "query"  => $query->fetch_all(MYSQLI_ASSOC),
          "numberEntries" => $numberEntries,
          "numberPages" => $numberPages
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
   }
   /*
    **************************************************************************************************
    Este metodo actualiza los campos de un carrier
    **************************************************************************************************
   */
   public function updateCarrier($data, $carrierId){
       $carrierNo = $data["carrierNo"];
       $carrierName = $data["carrierName"];
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
       if($sentence = $this->db->prepare("UPDATE carrier SET carrierno = ?, name = ? WHERE carrierid = ?;")){
         //Se asignan parametros al '?'
         $sentence->bind_param("isi", $carrierNo, $carrierName, $carrierId);
         $this->db->query("START TRANSACTION");
         //Se ejecuta la sentencia preparada
         if ($sentence->execute()) {
             if($sentence->close()){
              //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
              $this->db->query("COMMIT");
              //Se devuelve status correcto
              return TRUE;
             }else{
              //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
              $this->db->query("ROLLBACK");
              //Se devuelve el resultado con el mensaje de error
              return FALSE;
             }
         }
     }
  }
  /*
   **************************************************************************************************
   Este metodo inserta un nuevo carrier con todos sus campos
   **************************************************************************************************
  */
  public function insertCarrier($data){
      $carrierNo = $data["carrierNo"];
      $carrierName = $data["carrierName"];
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("INSERT INTO carrier (carrierno, name) VALUES (?, ?);")){
        //Se asignan parametros al '?'
        $sentence->bind_param("is", $carrierNo, $carrierName);
        $this->db->query("START TRANSACTION");
        //Se ejecuta la sentencia preparada
        if ($sentence->execute()) {
            if($sentence->close()){
             //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
             $this->db->query("COMMIT");
             //Se devuelve status correcto
             return TRUE;
            }else{
             //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
             $this->db->query("ROLLBACK");
             //Se devuelve el resultado con el mensaje de error
             return FALSE;
            }
        }
    }
  }
  /*
   **************************************************************************************************
   Este metodo obtiene los resultado de la busqueda
   **************************************************************************************************
   */
   public function searchbypagination($numberPage, $filter){
       //Se define cuantos elementos se van a consultar
      $elementsByPage = 5;
      $filterLike = '%' . $filter . '%';
      // print($filterLike);
      //Se define desde donde se va a consultar
      $offset = ($numberPage - 1) * $elementsByPage;
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
     $queryCount = "SELECT COUNT(*) AS entries FROM carrier WHERE name LIKE '$filterLike' OR carrierno LIKE '$filterLike'";
     if($query = $this->db->query("$queryCount;")){
       //Se obtiene el numero de resultados que arrojo la consulta
       $query         =            $query->fetch_assoc();
       $numberEntries = (Integer)$query["entries"];
     }
     //Se calcula el numero de paginas que tendra la consulta
     $pages         =                                           $numberEntries / $elementsByPage;
     $numberPages   =                (Integer)( $pages > (Integer)$pages ? $pages + 1 : $pages );
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("SELECT * FROM carrier WHERE name LIKE '$filterLike' OR carrierno LIKE '$filterLike' LIMIT $offset,$elementsByPage;")){
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>                  TRUE,
          "query"  => $query->fetch_all(MYSQLI_ASSOC),
          "numberEntries" => $numberEntries,
          "numberPages" => $numberPages
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
   }
}
?>

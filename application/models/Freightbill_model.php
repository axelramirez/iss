<?php
require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: Unit Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla po
 **************************************************************************************************
 */
class Freightbill_model{
  private $db;
  private $model;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
    $this->model = array(
      "frid" => array("type" => "Integer"),
      "fr" => array("type" => "String"),
      "receivingid" => array("type" => "Integer")
    );
  }

  public function getById($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from freightbill where frid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_assoc()
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }

  /*
   **************************************************************************************************
   Este metodo elimina un un purchase order de receiving details
   **************************************************************************************************
  */
  public function delete($arrays, $receivingId){
    $status = false;
    foreach($arrays as $key => $array){
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("delete from freightbill where frid = ? and receivingid = ?")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        $sentence->bind_param("ii", $array['frid'], $receivingId);
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
          $status = ( $sentence->affected_rows > 0 ) ? TRUE : FALSE;
          $sentence->close();
        }
      }
    }
    return $status;
  }
  /*
   **************************************************************************************************
   Este metodo inserta un nuevo purchase Order
   **************************************************************************************************
  */
  public function insert($nfbs, $receivingId){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("INSERT INTO freightbill(fr, receivingid) values(?, ?)")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("si", $frname, $receivingId);

      $this->db->query("START TRANSACTION");
      foreach($nfbs as $fr){
       //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
       $frname = $fr["frname"];

       //Se ejecuta la sentencia preparada
       $sentence->execute();
      }
      if($sentence->close()){
       //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
       $this->db->query("COMMIT");
       //Se devuelve status correcto
       return TRUE;
      }else{
       //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
       $this->db->query("ROLLBACK");
       //Se devuelve el resultado con el mensaje de error
       return FALSE;
      }
  }
}
  public function update($fbs, $receivingid){
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("UPDATE freightbill SET fr = ? WHERE receivingid = ?;")){
        //Se asignan parametros al '?'
        $sentence->bind_param("si", $frname, $receivingid);
        //Se declara un bloque de transaccion para que se asegure que "todos" los datos del arreglo sea guardado o no
        $this->db->query("START TRANSACTION");
        foreach($fbs as $fb){
         //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
         $frname = $fb["frname"];
         //Se ejecuta la sentencia preparada
         $sentence->execute();
        }
        if($sentence->close()){
         //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
         $this->db->query("COMMIT");
         //Se devuelve status correcto
         return TRUE;
        }else{
         //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
         $this->db->query("ROLLBACK");
         //Se devuelve el resultado con el mensaje de error
         return FALSE;
        }
      }
  }
  public function updateMain($fields, $arrays){
    $status = FALSE;
    foreach($arrays as $key => $array){
      $fr = $this->getById($array["frid"]);
      $fr = $fr["query"];
      list($haveRegister, $params_type, $query_string, $params) = generateUpdateQuery($fields, $array, $fr, $this->model);
      if($haveRegister){
        $params_type .= "i";
        array_unshift($params, $params_type);
        array_push($params, $array["frid"]);
        print_r($array);
        print_r($query_string);
        //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
        if($sentence = $this->db->prepare("update freightbill set $query_string where frid = ?")){
          //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
          call_user_func_array(array($sentence, 'bind_param'), referencesValue($params));
          //Se ejecuta la sentencia preparada
          if($sentence->execute()){
            //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
            $status = ( $sentence->affected_rows > 0 ) ? TRUE : FALSE;
            $sentence->close();
          }
        }
      }
    }
  }
  /*
   **************************************************************************************************
   Este metodo inserta un nuevo Freightbill
   **************************************************************************************************
  */
  public function save($arrays, $receivingId){
    $status = FALSE;
    foreach($arrays as $key => $array){
      $this->db->query("START TRANSACTION");
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("INSERT INTO freightbill(fr, receivingid) values(?, ?);")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        $sentence->bind_param("si", $array["fr"], $receivingId);
        $sentence->execute();
        if($sentence->close()){
          $this->db->query("COMMIT");
          $status = TRUE;
        }else{
          $this->db->query("ROLLBACK");
          $status = FALSE;
          break;
        }
      }
    }
    return $status;
  }

  public function getByReceivingId($id){
      //Se ejecuta la consulta y se obtiene el resultado
      if($sentence = $this->db->prepare("SELECT * FROM freightbill WHERE receivingid = ?;")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        $sentence->bind_param("i", $id);
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
        $query = $sentence->get_result();
          //Se cierra la sentencia
          $sentence->close();
          //Se devuelve el resultado con la sentencia
          return array(
            "status" =>                     TRUE,
            "query"  => $query->fetch_all(MYSQLI_ASSOC)
          );
        }
      }
      //Se devuelve el resultado con el mensaje de error
      return array(
        "status"  =>            FALSE,
        "message" => $this->db->error
      );
  }
}
?>

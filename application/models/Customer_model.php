<?php
 require_once("adapters/MySQLConnection.php");
 /*
  **************************************************************************************************
  ***********************Name: Customer Class Model
  ***********************Description: Realiza las consultas correspondientes a la tabla AutoInvoice
  *********************************** que es la que gestiona las facturas de los clientes
  **************************************************************************************************
*/
class Customer_model{
  private $db;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todos los clientes del sistema
   **************************************************************************************************
  */
  public function get(){
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select * from customer order by name;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>   TRUE,
        "query"  => $query
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todas las descripciones de vendors por limit
   **************************************************************************************************
   */
   public function getbypagination($numberPage){
       //Se define cuantos elementos se van a consultar
      $elementsByPage = 5;
      //Se define desde donde se va a consultar
      $offset = ($numberPage - 1) * $elementsByPage;
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
     $queryCount = "SELECT COUNT(*) AS entries FROM customer";
     if($query = $this->db->query("$queryCount;")){
       //Se obtiene el numero de resultados que arrojo la consulta
       $query         =            $query->fetch_assoc();
       $numberEntries = (Integer)$query["entries"];
     }
     //Se calcula el numero de paginas que tendra la consulta
     $pages         =                                           $numberEntries / $elementsByPage;
     $numberPages   =                (Integer)( $pages > (Integer)$pages ? $pages + 1 : $pages );
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("SELECT * FROM customer ORDER BY name LIMIT $offset,$elementsByPage;")){
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>                  TRUE,
          "query"  => $query->fetch_all(MYSQLI_ASSOC),
          "numberEntries" => $numberEntries,
          "numberPages" => $numberPages
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
   }
   /*
    **************************************************************************************************
    Este metodo obtiene los resultado de la busqueda
    **************************************************************************************************
    */
    public function searchbypagination($numberPage, $filter){
        //Se define cuantos elementos se van a consultar
       $elementsByPage = 5;
       $filterLike = '%' . $filter . '%';
       // print($filterLike);
       //Se define desde donde se va a consultar
       $offset = ($numberPage - 1) * $elementsByPage;
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      $queryCount = "SELECT COUNT(*) AS entries FROM customer WHERE name LIKE '$filterLike' OR custno LIKE '$filterLike'";
      if($query = $this->db->query("$queryCount;")){
        //Se obtiene el numero de resultados que arrojo la consulta
        $query         =            $query->fetch_assoc();
        $numberEntries = (Integer)$query["entries"];
      }
      //Se calcula el numero de paginas que tendra la consulta
      $pages         =                                           $numberEntries / $elementsByPage;
      $numberPages   =                (Integer)( $pages > (Integer)$pages ? $pages + 1 : $pages );
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
     if($sentence = $this->db->prepare("SELECT * FROM customer WHERE name LIKE '$filterLike' OR custno LIKE '$filterLike' LIMIT $offset,$elementsByPage;")){
       //Se ejecuta la sentencia preparada
       if($sentence->execute()){
         //Se obtiene el resultado de la ejecucion de la sentencia
         $query = $sentence->get_result();
         //Se cierra la sentencia
         $sentence->close();
         //Se devuelve el resultado con la sentencia
         return array(
           "status" =>                  TRUE,
           "query"  => $query->fetch_all(MYSQLI_ASSOC),
           "numberEntries" => $numberEntries,
           "numberPages" => $numberPages
         );
       }
     }
     //Se devuelve el resultado con el mensaje de error
     return array(
       "status"  =>            FALSE,
       "message" => $this->db->error
     );
    }
   /*
    **************************************************************************************************
    Este metodo inserta un nuevo customer con todos sus campos
    **************************************************************************************************
   */
   public function insertCustomer($data){
       $customerNo = $data["customerNo"];
       $customerName = $data["customerName"];
       $customerTax = $data["customerTax"];
       $customerAddress = $data["customerAddress"];
       $customerCity = $data["customerCity"];
       $customerCountry = $data["customerCountry"];
       $customerState = $data["customerState"];
       $customerZipcode = $data["customerZipcode"];
       $customerPhone = $data["customerPhone"];
       $customerFax = $data["customerFax"];
       $customerContactname = $data["customerContactname"];
       $customerEmails = $data["customerEmails"];
       $destinationName = $data["destinationName"];
       $destinationAddress = $data["destinationAddress"];
       $receivingEmail = $data["receivingEmail"];
       $shippingEmail = $data["shippingEmail"];
       $labelFormat = $data["labelFormat"];
       $customerType = $data["customerType"];
       $receiptMode = $data["receiptMode"];
       $ediFile = $data["ediFile"];
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
       if($sentence = $this->db->prepare("INSERT INTO customer (custno, name, taxid,
         address, city, state, zipcode, country, phone, fax, contact, email,
         sendRecMail, sendShipMail, `outFile`, labelFormat, ownInventory,
         receiptMode) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);")){
         //Se asignan parametros al '?'
         $sentence->bind_param("ssssssssssssssiiii", $customerNo, $customerName, $customerTax, $customerAddress, $customerCity, $customerState, $customerZipcode, $customerCountry, $customerPhone, $customerFax, $customerContactname, $customerEmails, $receivingEmail, $shippingEmail, $ediFile, $labelFormat, $customerType, $receiptMode);
         $this->db->query("START TRANSACTION");
         //Se ejecuta la sentencia preparada
         if ($sentence->execute()) {
             if($sentence->close()){
              //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
              $this->db->query("COMMIT");
              //Se devuelve status correcto
              return TRUE;
             }else{
              //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
              $this->db->query("ROLLBACK");
              //Se devuelve el resultado con el mensaje de error
              return FALSE;
             }
         }
     }
     //Se imprime el error y se retorna falso de que no se completo la operacion
     print_r($this->db->error);
 }
 /*
  **************************************************************************************************
  Este metodo inserta un nuevo customer con todos sus campos
  **************************************************************************************************
 */
 public function updateCustomer($data, $customerId){
     $customerNo = $data["customerNo"];
     $customerName = $data["customerName"];
     $customerTax = $data["customerTax"];
     $customerAddress = $data["customerAddress"];
     $customerCity = $data["customerCity"];
     $customerCountry = $data["customerCountry"];
     $customerState = $data["customerState"];
     $customerZipcode = $data["customerZipcode"];
     $customerPhone = $data["customerPhone"];
     $customerFax = $data["customerFax"];
     $customerContactname = $data["customerContactname"];
     $customerEmails = $data["customerEmails"];
     $destinationName = $data["destinationName"];
     $destinationAddress = $data["destinationAddress"];
     $receivingEmail = $data["receivingEmail"];
     $shippingEmail = $data["shippingEmail"];
     $labelFormat = $data["labelFormat"];
     $customerType = $data["customerType"];
     $receiptMode = $data["receiptMode"];
     $ediFile = $data["ediFile"];
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
     if($sentence = $this->db->prepare("UPDATE customer SET custno = ?, name = ?, taxid = ?,
       address = ?, city = ?, state = ?, zipcode = ?, country = ?, phone = ?, fax = ?, contact = ?, email = ?,
       sendRecMail = ?, sendShipMail = ?, `outFile` = ?, labelFormat = ?, ownInventory = ?,
       receiptMode = ?, destinationname = ?, destinationaddress = ? WHERE customerid = ?;")){
       //Se asignan parametros al '?'
       $sentence->bind_param("isssssisiissssiiiissi", $customerNo, $customerName, $customerTax, $customerAddress, $customerCity, $customerState, $customerZipcode, $customerCountry, $customerPhone, $customerFax, $customerContactname, $customerEmails, $receivingEmail, $shippingEmail, $ediFile, $labelFormat, $customerType, $receiptMode, $destinationName, $destinationAddress, $customerId);
       $this->db->query("START TRANSACTION");
       //Se ejecuta la sentencia preparada
       if ($sentence->execute()) {
           if($sentence->close()){
            //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
            $this->db->query("COMMIT");
            //Se devuelve status correcto
            return TRUE;
           }else{
            //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
            $this->db->query("ROLLBACK");
            //Se devuelve el resultado con el mensaje de error
            return FALSE;
           }
       }
   }
   //Se imprime el error y se retorna falso de que no se completo la operacion
   print_r($this->db->error);
}
  /*
   **************************************************************************************************
   Este metodo obtiene un cliente del sistema por id
   **************************************************************************************************
  */
  public function getById($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from customer where customerid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>   TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC),
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene los productos por customer id
   **************************************************************************************************
  */
  public function getByCustomerId($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("SELECT productno, productid, description FROM product WHERE customerid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC)
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todos los clientes del sistema que sean diferente al id de la consulta
   **************************************************************************************************
  */
  public function getWithoutId($id){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("select * from customer where customerid != ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $id);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>   TRUE,
          "query"  => $query
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo actualiza el monto de credito ocupado con el de la nueva salida que realiza el cliente
   sumando el monto recibido a due_amount
   **************************************************************************************************
  */
  public function updateDueAmount($id, $amount){
    if($sentence = $this->db->prepare("update customer set due_amount = due_amount + ? where customerid = ?")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("di", $amount, $id);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
        if( $this->db->affected_rows == 0 ){
          return  TRUE;
        }else{
          return FALSE;
        }
      }
    }
    //Se imprime el error y se retorna falso de que no se completo la operacion
    print_r($this->db->error);
    return FALSE;
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todos los clientes que cuentan aun con credito para pedir nuevas salidas
   **************************************************************************************************
   */
  public function getCustomersExcept($id){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("select * from customer where allow_amount > due_amount and customerid !=  ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $id);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>   TRUE,
          "query"  => $query->fetch_all(MYSQLI_ASSOC)
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
}
?>

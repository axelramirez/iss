<?php
 require_once("adapters/MySQLConnection.php");
 /*
  **************************************************************************************************
  ***********************Name: Invoice Class Model
  ***********************Description: Realiza las consultas correspondientes a la tabla Invoice
  *********************************** que es la que gestiona las facturas de los clientes
  **************************************************************************************************
  */
class Invoice_model{
  private $db;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
  */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todas las facturas
   **************************************************************************************************
  */
  public function get(){
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select * from invoice;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>   TRUE,
        "query"  => $query
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todas las facturas
   **************************************************************************************************
  */
  public function getAllByIds($ids){
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select * from invoice where invoiceid in (".implode(", ", $ids).");")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>   TRUE,
        "query"  => $query->fetch_all(MYSQLI_ASSOC)
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene una factura por su id
   **************************************************************************************************
  */
  public function getById($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from invoice where invoiceid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>   TRUE,
         "query"  => $query
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene una factura por su shippingid
   **************************************************************************************************
  */
  public function getByShippingId($id){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("select * from invoice where shippingid = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $id);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        if( $query->num_rows > 0 ){
          //Se devuelve el resultado con la sentencia
          return array(
            "status" =>                  TRUE,
            "query"  => $query->fetch_assoc()
          );
        }
        return array(
          "status" =>  FALSE,
          "query"  => "NULL"
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo inserta una nueva factura
   **************************************************************************************************
  */
  public function save($invoice){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("insert into invoice(shippingid, date, subtotal, tax) values(?, ?, ?, ?)")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("isdd", $invoice["shippingid"], date("Y-m-d"), $invoice["total"], $invoice["tax"]);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>                     TRUE,
          "invoiceid"  => $this->db->insert_id
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo actualiza una factura existente
   **************************************************************************************************
  */
  public function update($invoice){
   $subtotal  =  $invoice["subtotal"];
   $invoiceid = $invoice["invoiceid"];
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("update invoice set subtotal = ? where invoiceid = ?")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("di", $subtotal, $invoiceid);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
       if( $this->db->affected_rows == 0 ){
         return  TRUE;
       }else{
         return FALSE;
       }
     }
   }
   //Se imprime el error y se retorna falso de que no se completo la operacion
   print_r($this->db->error);
   return FALSE;
  }

  /*
   **************************************************************************************************
   Este metodo actualiza una factura existente
   **************************************************************************************************
  */
  public function updateAll($invoices){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("update invoice set subtotal = ? where invoiceid = ?")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("di", $subtotal, $invoiceid);
      //Se declara un bloque de transaccion para que se asegure que "todos" los datos del arreglo sea guardado o no
      $this->db->query("START TRANSACTION");
      foreach($invoices as $invoice){
       //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
       $subtotal  =  $invoice["subtotal"];
       $invoiceid = $invoice["invoiceid"];
       //Se ejecuta la sentencia preparada
       $sentence->execute();
      }
      if($sentence->close()){
       //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
       $this->db->query("COMMIT");
       //Se devuelve status correcto
       return array(
         "status"  => TRUE
       );
      }else{
       //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
       $this->db->query("ROLLBACK");
       //Se devuelve el resultado con el mensaje de error
       return array(
         "status"  =>            FALSE,
         "message" => $this->db->error
       );
      }
    }
  }
}
?>

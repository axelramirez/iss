<?php
require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: statusAutoInvoice Class Model
 ***********************Description: Realiza las consultas correspondientes a la vista statusAutoInvoice
 *********************************** que es la que gestiona las facturas del usuario
 **************************************************************************************************
 */
class StatusAutoInvoice_model{
  private $db;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }

  /*
   **************************************************************************************************
   Este metodo obtiene todos las facturas y con su estado (pagada, cancelada, total, monto pagado, monto
   que se debe) de los clientes del sistema
   **************************************************************************************************
   */
  public function get(){
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select * from statusAutoInvoiceView;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>   TRUE,
        "query"  => $query
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status" =>             FALSE,
      "message" => $this->db->error
    );
  }
  /*
  **************************************************************************************************
  Este metodo obtiene una factura con su estado (pagada, cancelada, total, monto pagado, monto
  que se debe) de un cliente del sistema
  **************************************************************************************************
  */
  public function getById($id){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("select * from statusAutoInvoiceView where autoid = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param('i', $id);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>   TRUE,
          "query"  => $query
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todos las facturas y con su estado (pagada, cancelada, total, monto pagado, monto
   que se debe) de un cliente del sistema
   **************************************************************************************************
   */
  public function getByClientId($id){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("select * from statusAutoInvoiceView where customer_id = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param('i', $id);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>   TRUE,
          "query"  => $query
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene la informacion de una factura con su estado (pagada, cancelada, total, monto
   pagado, monto que se debe) de un cliente por el customer_id y el autoid
   **************************************************************************************************
  */
  public function getByAutoInvoiceClient($id, $customerId){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("select * from statusAutoInvoiceView where autoid = ? and customer_id = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param('ii', $id, $customerId);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        if($query->num_rows == 0){
          //Se devuelve el resultado cuando no se encontraron registros con el mensaje de error
          return array(
            "status"  =>                                                                                                                 FALSE,
            "message" => "Error: No fue posible realizar la operacion, el cliente no existe o no cuenta con la factura que se trata de pagar."
          );
        }else{
          //Se devuelve el resultado cuando se encuentran registros con la sentencia
          return array(
            "status" =>   TRUE,
            "query"  => $query
          );
        }
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todos las facturas y con su estado (pagada, cancelada, total, monto pagado, monto
   que se debe) de los clientes del sistema
   **************************************************************************************************
   */
  public function getByDesc($numberPage){
    $pages          =    0;
    $query          = null;
    $numberPages    =    0;
    $numberEntries  =    0;
    $elementsByPage =   15;
    //Se calcula la posicion en el que se mostrara la informacion del resultado de la consulta
    $offset         = ($numberPage - 1) * $elementsByPage;
    //Se realiza una consulta para contar los elementos resultado de la consultas a realizar
    if($query = $this->db->query("select count(*) as numberEntries from statusAutoInvoiceView;")){
      //Se obtiene el numero de resultados que arrojo la consulta
      $query         =            $query->fetch_assoc();
      $numberEntries = (Integer)$query["numberEntries"];
    }
    //Se calcula el numero de paginas que tendra la consulta
    $pages         =                                           $numberEntries / $elementsByPage;
    $numberPages   =                (Integer)( $pages > (Integer)$pages ? $pages + 1 : $pages );
    if($query = $this->db->query("select * from statusAutoInvoiceView order by autoid desc limit $elementsByPage offset $offset;")){
      //Se devuelve el resultado con la sentencia y los resultados de paginacion
      return array(
        "status"        =>                           TRUE,
        "query"         => $query->fetch_all(MYSQL_ASSOC),
        "numberPages"   =>                   $numberPages,
        "numberEntries" =>                 $numberEntries
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status" =>             FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todos las facturas y con su estado (pagada, cancelada, total, monto pagado, monto
   que se debe) de los clientes del sistema
   **************************************************************************************************
   */
   public function searchInvoiceBy($numberPage, $params){
     $pages          =       0;
     $query          =    null;
     $empty          =    TRUE;
     $unique         =    TRUE;
     $numberPages    =       0;
     $numberEntries  =       0;
     $elementsByPage =      15;
     $result         = array();
     //Query Count and Search
     $where          =                                                      "where ";
     $queryCount     = "select count(*) as numberEntries from statusAutoInvoiceView";
     $querySearch    =                         "select * from statusAutoInvoiceView";
     //Se calcula la posicion en el que se mostrara la informacion del resultado de la consulta
     $offset = ($numberPage - 1) * $elementsByPage;
     //autoid section where
     $result          = $this->checkFragment((Integer)$params["invoiceId"]  > 0, "autoinvoice_number = $params[invoiceId]", $unique);
     $unique          =   $result["unique"];
     $empty           =    $result["empty"];
     $invoiceId       = $result["fragment"];
     //customer_id section where
     $result          = $this->checkFragment((Integer)$params["customerId"] > 0, "customer_id = $params[customerId]", $unique);
     $unique          =   $result["unique"];
     $empty           =    $result["empty"];
     $customerId      = $result["fragment"];
     //pedimentoInvoice section where
     $result          = $this->checkFragment((Integer)$params["pedimentNo"] > 0, "pedimentoInvoice = $params[pedimentNo]", $unique);
     $unique          =   $result["unique"];
     $empty           =    $result["empty"];
     $pedimentNo      = $result["fragment"];
     //typeInvoice section where
     $result          = $this->checkFragment((Integer)$params["invoiceType"] >= 0, "typeInvoice = $params[invoiceType]", $unique);
     $unique          =   $result["unique"];
     $empty           =    $result["empty"];
     $invoiceType     = $result["fragment"];
     //Date section where
     $dateTo          =                        (strcmp($params["dateTo"], "") != 0) ? "dateInvoice <= '$params[dateTo]'" : "";
     $dateFrom        =                    (strcmp($params["dateFrom"], "") != 0) ? "dateInvoice >= '$params[dateFrom]'" : "";
     $result          =                                                         $this->checkDate($dateFrom, $dateTo, $unique);
     $unique          =   $result["unique"];
     $empty           =    $result["empty"];
     $dateSection     = $result["fragment"];
     // //Status section where
     $statusPaid      =                                       ((Boolean)$params["statusInvoice"]["paid"]) ? "isPaid = 1" : "";
     $statusActive    =                                    ((Boolean)$params["statusInvoice"]["active"]) ? "isAllow = 1" : "";
     $statusCancelled =                             ((Boolean)$params["statusInvoice"]["cancelled"]) ? "isCancelled = 1" : "";
     $result          =                             $this->checkStatus($statusActive, $statusPaid, $statusCancelled, $unique);
     $unique          =   $result["unique"];
     $empty           =    $result["empty"];
     $statusSection   = $result["fragment"];

     if($empty){
       $where = "";
     }else{
       $where .= "$invoiceId $customerId $pedimentNo $invoiceType $dateSection $statusSection ";
     }
     //Despues del filtro realiza las consultas de contar y de busqueda
     //Se realiza una consulta para contar los elementos resultado de la consultas a realizar
     if($query = $this->db->query("$queryCount $where;")){
       //Se obtiene el numero de resultados que arrojo la consulta
       $query         =            $query->fetch_assoc();
       $numberEntries = (Integer)$query["numberEntries"];
     }
     //Se calcula el numero de paginas que tendra la consulta
     $pages         =                                           $numberEntries / $elementsByPage;
     $numberPages   =                (Integer)( $pages > (Integer)$pages ? $pages + 1 : $pages );
     if($query = $this->db->query("$querySearch $where order by autoid desc limit $elementsByPage offset $offset;")){
       //Se devuelve el resultado con la sentencia y los resultados de paginacion
       return array(
         "status"        =>                           TRUE,
         "query"         => $query->fetch_all(MYSQL_ASSOC),
         "numberPages"   =>                   $numberPages,
         "numberEntries" =>                 $numberEntries
       );
     }
     //Se devuelve el resultado con el mensaje de error
     return array(
       "status" =>             FALSE,
       "message" => $this->db->error
     );
   }

   private function checkEmpty($fragment, $unique){
     $separator = "and";
     if( $unique ){
       $separator = "";
       $unique = FALSE;
     }
     return "$separator $fragment";
   }

   /*
    **************************************************************************************************
    Este metodo obtiene todos las facturas y con su estado (pagada, cancelada, total, monto pagado, monto
    que se debe) de los clientes del sistema
    **************************************************************************************************
    */
   private function checkDate($startDate, $endDate, $unique){
     $fragment = "";
     $separator = "and";
     if( $unique ){
       $separator = "";
     }
     if( strcmp($startDate, "") != 0 && strcmp($endDate, "") != 0 ){
       $fragment = "$separator ($startDate and $endDate)";
       $unique = FALSE;
     }else if( strcmp($startDate, "") != 0 ){
       $fragment =                "$separator $startDate";
       $unique = FALSE;
     }else if( strcmp($endDate, "") != 0 ){
       $fragment =                  "$separator $endDate";
       $unique = FALSE;
     }
     return array(
       "empty"    =>   $unique,
       "unique"   =>   $unique,
       "fragment" => $fragment
     );
   }

   /*
    **************************************************************************************************
    Este metodo obtiene todos las facturas y con su estado (pagada, cancelada, total, monto pagado, monto
    que se debe) de los clientes del sistema
    **************************************************************************************************
    */
   private function checkStatus($isAllow, $isPaid, $isCancelled, $unique){
     $fragment = "";
     $separator = "and";
     if( $unique ){
       $separator = "";
     }
     if( strcmp($isAllow, "") != 0 && strcmp($isPaid, "") != 0 && strcmp($isCancelled, "") != 0 ){
       $fragment = "$separator ($isAllow or $isPaid or $isCancelled)";
       $unique = FALSE;
     }else if( strcmp($isAllow, "") != 0 && strcmp($isPaid, "") != 0 ){
       $fragment = "$separator ($isAllow or $isPaid)";
       $unique = FALSE;
     }else if( strcmp($isAllow, "") != 0 && strcmp($isCancelled, "") != 0 ){
       $fragment = "$separator ($isAllow or $isCancelled)";
       $unique = FALSE;
     }else if( strcmp($isPaid, "") != 0 && strcmp($isCancelled, "") != 0 ){
       $fragment = "$separator ($isPaid or $isCancelled)";
       $unique = FALSE;
     }else if( strcmp($isAllow, "") != 0 ){
       $fragment = "$separator $isAllow";
       $unique = FALSE;
     }else if( strcmp($isPaid, "") != 0 ){
       $fragment = "$separator $isPaid";
       $unique = FALSE;
     }else if( strcmp($isCancelled, "") != 0 ){
       $fragment = "$separator $isCancelled";
       $unique = FALSE;
     }
     return array(
       "empty"    =>   $unique,
       "unique"   =>   $unique,
       "fragment" => $fragment
     );
   }

   /*
    **************************************************************************************************
    Este metodo obtiene todos las facturas y con su estado (pagada, cancelada, total, monto pagado, monto
    que se debe) de los clientes del sistema
    **************************************************************************************************
    */
   private function checkFragment($condition, $fragment, $unique){
     if( $condition ){
       return array(
         "empty"    =>                                 FALSE,
         "unique"   =>                                 FALSE,
         "fragment" => $this->checkEmpty($fragment, $unique)
       );
     }
     return array(
       "empty"    => $unique,
       "unique"   => $unique,
       "fragment" =>      ""
     );
   }
}
?>

<?php
 require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: Receiving Invoice Class Model
 ***********************Description: Realiza las consultas correspondientes a la vista
 *********************************** receivingInvoiceView que es la que gestiona los invoice list
 *********************************** ligados a una autoinvoice de tipo Receiving
 **************************************************************************************************
*/
class ReceivingInvoice_model{
  private $db;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
  */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todos las autoinvoice de los clientes
   **************************************************************************************************
  */
  public function get(){
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select * from receivingInvoiceView;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>                            TRUE,
        "query"  => $query->fetch_all(MYSQLI_ASSOC)
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene un autoinvoice por su id
   **************************************************************************************************
  */
  public function getByAutoInvoiceId($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from receivingInvoiceView where autoinvoice_id = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                           TRUE,
         "query"  => $query->fetch_all(MYSQL_ASSOC)
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
}
?>

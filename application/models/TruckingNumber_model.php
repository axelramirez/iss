<?php
require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: Unit Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla po
 **************************************************************************************************
 */
class TruckingNumber_model{
  private $db;
  private $model;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
    $this->model = array(
      "truckingid" => array("type" => "Integer"),
      "truckingno" => array("type" => "String"),
      "receivingid" => array("type" => "Integer")
    );
  }
  /*
   **************************************************************************************************
   Este metodo inserta un nuevo Trucking Number
   **************************************************************************************************
  */
  public function save($trucking){
    $response = $response = array(
      "status" => FALSE
    );
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("INSERT INTO truckingnumber(truckingno, receivingid) values(?, ?)")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("si", $trucking["truckingno"], $trucking["receivingid"]);

      if($sentence->execute()){
        $response = array(
          "status" => TRUE,
          "last_id" => $this->db->insert_id
        );
      }
      $sentence->close();
    }
    return $response;
  }
  public function deleteByReceivingDetailId($arrays){
    $status = false;
    foreach($arrays as $key => $array){
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("delete from truckingnumber where truckingid in (select truckingid from receivingdetail where receivingdetailid = ?)")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        $sentence->bind_param("i", $array['receivingdetailid']);
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
          $status = ( $sentence->affected_rows > 0 ) ? TRUE : FALSE;
          $sentence->close();
        }
      }
    }
    return $status;
  }
}
?>

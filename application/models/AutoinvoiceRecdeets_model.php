<?php
 require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: AutoInvoice Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla AutoInvoice
 *********************************** que es la que gestiona las facturas de los clientes
 **************************************************************************************************
*/
class AutoInvoiceRecdeets_model{
  private $db;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
  */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todos las autoinvoice de los clientes
   **************************************************************************************************
  */
  public function get(){
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select * from receivingDetailStorageInvoiceView;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>   TRUE,
        "query"  => $query
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene un autoinvoice por su id
   **************************************************************************************************
  */
  public function getByAutoInvoiceId($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from receivingDetailStorageInvoiceView where autoinvoice_id = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>   TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC)
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }

  public function getReceivingDetailToUpdate($id){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("SELECT autoinvoice_recdeets.receivingdetailid, autoinvoice_recdeets.ChargedDays, DATE_FORMAT(DATE_SUB(receivingdetail.lastStorageDate, INTERVAL autoinvoice_recdeets.ChargedDays DAY),'%Y-%m-%d') AS newDate FROM autoinvoice_recdeets INNER JOIN receivingdetail ON receivingdetail.receivingdetailid = autoinvoice_recdeets.receivingdetailid WHERE autoinvoice_recdeets.autoid = ? AND autoinvoice_recdeets.isCancelled = 0;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $id);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>   TRUE,
          "query"  => $query->fetch_all(MYSQLI_ASSOC)
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }

  /*
   **************************************************************************************************
   Este metodo actualiza una autoinvoice existente
   **************************************************************************************************
  */
  public function updateAll($invoiceId, $receivingDetails){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("update autoinvoice_recdeets set UnitPrice = ?, ChargedDays = ? where autoid = ? and receivingdetailid = ?")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("diii", $unitPrice, $chargedDays, $invoiceId, $receivingDetailId);
      //Se declara un bloque de transaccion para que se asegure que "todos" los datos del arreglo sea guardado o no
      $this->db->query("START TRANSACTION");
      foreach($receivingDetails as $receivingDetail){
       //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
       $unitPrice             =         $receivingDetail["UnitPrice"];
       $chargedDays    = $receivingDetail["ChargedDays"];
       $receivingDetailId    = $receivingDetail["receiving_detail_id"];
       //Se ejecuta la sentencia preparada
       $sentence->execute();
      }
      if($sentence->close()){
       //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
       $this->db->query("COMMIT");
       //Se devuelve status correcto
       return array(
         "status"  => TRUE
       );
      }else{
       //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
       $this->db->query("ROLLBACK");
       //Se devuelve el resultado con el mensaje de error
       return array(
         "status"  =>            FALSE,
         "message" => $this->db->error
       );
      }
    }
  }

  public function update($receivingDetail){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("update autoinvoice_recdeets set UnitPrice = ?, ChargedDays = ? where autoid = ? and receivingdetailid = ?")){
     $sentence->bind_param("diii", $receivingDetail["UnitPrice"], $receivingDetail["ChargedDays"], $receivingDetail["autoinvoice_id"], $receivingDetail["receiving_detail_id"]);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
       if( $this->db->affected_rows == 0 ){
         return  TRUE;
       }else{
         return FALSE;
       }
     }
   }
   //Se imprime el error y se retorna falso de que no se completo la operacion
   print_r($this->db->error);
   return FALSE;
  }

  public function updateRecdeets($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("UPDATE autoinvoice_recdeets SET isCancelled = 1 WHERE autoid = ? AND autoinvoice_recdeets.isCancelled = 0;")){
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
       if( $this->db->affected_rows == 0 ){
         return  TRUE;
       }else{
         return FALSE;
       }
     }
   }
   //Se imprime el error y se retorna falso de que no se completo la operacion
   print_r($this->db->error);
   return FALSE;
  }
}
?>

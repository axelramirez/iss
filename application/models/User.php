<?php

require_once("adapters/MySQLConnection.php");


class User{
    private $db;
    private $model;
    
    

    public function __construct(){
        $this->db = MySQLConnection::getInstance()->getConnection();
    }

    private function cmpStr($firstname,$lastname,$roleid){
       return strcmp($firstname,"") == 0 &&  strcmp($lastname,"") == 0 && strcmp($roleid,"-1") == 0; 
    } 

    public function get($numberPage,$firstname,$lastname,$roleid){

        //Se define cuantos elementos se van a consultar
       $elementsByPage = 5;

       $isData =  $this->cmpStr($firstname,$lastname,$roleid);
     
       //Se define desde donde se va a consultar
       $offset = ($numberPage - 1) * $elementsByPage;
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'

      $queryCount = $isData == 1 ? "SELECT COUNT(*) AS entries FROM user" : "SELECT COUNT(*) AS entries FROM user WHERE firstname=? OR lastname=? OR roleid=?";
      

      if($sentence1 = $this->db->prepare("$queryCount;")){
       if($isData != 1){
            $sentence1->bind_param("ssi",$firstname,$lastname,$roleid);
            $this->db->query("START TRANSACTION");
        }
         if($sentence1->execute()){
              $sentence1->bind_result($numberEntries);
              $sentence1->fetch();
              $sentence1->close();
          }

      }
    
      /*if($query = $this->db->query("$queryCount;")){
        //Se obtiene el numero de resultados que arrojo la consulta
        $query         =            $query->fetch_assoc();
        $numberEntries = (Integer)$query["entries"];
      }*/


      //Se calcula el numero de paginas que tendra la consulta
      $pages         =                                           $numberEntries / $elementsByPage;
      $numberPages   =                (Integer)( $pages > (Integer)$pages ? $pages + 1 : $pages );
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
       
     $command = $isData == 1 ? "SELECT * FROM user LIMIT $offset,$elementsByPage;;" : "SELECT * FROM user WHERE firstname=? OR lastname=? OR roleid=?  LIMIT $offset,$elementsByPage;;";
     if($sentence = $this->db->prepare($command)){
        //si hay datos que filtrar pasa la parametrizacion
        if($isData != 1){
          $sentence->bind_param("ssi",$firstname,$lastname,$roleid);
          $this->db->query("START TRANSACTION");
        }
       //Se ejecuta la sentencia preparada
       if($sentence->execute()){
         //Se obtiene el resultado de la ejecucion de la sentencia
         $query = $sentence->get_result();
         //Se cierra la sentencia
         $sentence->close();
         //Se devuelve el resultado con la sentencia
         return array(
           "status" =>                  TRUE,
           "query"  => $query->fetch_all(MYSQLI_ASSOC),
           "numberEntries" => $numberEntries,
           "numberPages" => $numberPages
         );
       }
     }
     //Se devuelve el resultado con el mensaje de error
     return array(
       "status"  =>            FALSE,
       "message" => $this->db->error
     );
    }


    public function getResult($userid,$pass){

      $command = "SELECT password FROM user WHERE userid=? and password=?;;";
      if($sentence = $this->db->prepare($command)){
          //si hay datos que filtrar pasa la parametrizacion
          
            $sentence->bind_param("is",$userid,$pass);
            $this->db->query("START TRANSACTION");
          
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
          //Se obtiene el resultado de la ejecucion de la sentencia
          $sentence->bind_result($result);
          $sentence->fetch();
          //Se cierra la sentencia
          $sentence->close();
        }
      }

      if(strcmp($pass,$result) == 0)
        return array(
          "status" =>                  TRUE,
        );
      //Se devuelve el resultado con el mensaje de error
      return array(
        "status"  =>            FALSE,
        "message" => $this->db->error
      );
  }

    public function insert($login,$password,$roleid,$firstname,$lastname){
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
       if($sentence = $this->db->prepare("INSERT INTO user (login,password,roleid,firstname,lastname) VALUES (?,?,?,?,?);")){
         //Se asignan parametros al '?'
         $sentence->bind_param("ssiss", $login,$password,$roleid,$firstname,$lastname);
         $this->db->query("START TRANSACTION");
         //Se ejecuta la sentencia preparada
         if ($sentence->execute()) {
             if($sentence->close()){
              //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
              $this->db->query("COMMIT");
              //Se devuelve status correcto
              return TRUE;
             }else{
              //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
              $this->db->query("ROLLBACK");
              //Se devuelve el resultado con el mensaje de error
              return FALSE;
             }
         }
     }
 }

    public function update($login,$roleid,$firstname,$lastname, $userid){
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
       if($sentence = $this->db->prepare("UPDATE user SET login = ?, roleid = ?,firstname = ?, lastname = ? WHERE userid = ?;")){
         //Se asignan parametros al '?'
         $sentence->bind_param("sissi", $login,$roleid,$firstname,$lastname,$userid);
         $this->db->query("START TRANSACTION");
         //Se ejecuta la sentencia preparada
         if ($sentence->execute()) {
             if($sentence->close()){
              //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
              $this->db->query("COMMIT");
              //Se devuelve status correcto
              return TRUE;
             }else{
              //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
              $this->db->query("ROLLBACK");
              //Se devuelve el resultado con el mensaje de error
              return FALSE;
             }
         }
     }
   }

   public function updateResult($userid,$pass){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
     if($sentence = $this->db->prepare("UPDATE user SET password = ? WHERE userid = ?;")){
       //Se asignan parametros al '?'
       $sentence->bind_param("si", $pass,$userid);
       $this->db->query("START TRANSACTION");
       //Se ejecuta la sentencia preparada
       if ($sentence->execute()) {
           if($sentence->close()){
            //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
            $this->db->query("COMMIT");
            //Se devuelve status correcto
            return TRUE;
           }else{
            //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
            $this->db->query("ROLLBACK");
            //Se devuelve el resultado con el mensaje de error
            return FALSE;
           }
       }
   }
  }

   public function validateLogin($login){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
     $loginResult = "";
     if($sentence = $this->db->prepare("SELECT login FROM user WHERE login = ?;")){
       //Se asignan parametros al '?'
       $sentence->bind_param("s", $login);
       $this->db->query("START TRANSACTION");
       //Se ejecuta la sentencia preparada
       if ($sentence->execute()) {
           $sentence->bind_result($loginResult);
           $sentence->fetch();
           if($sentence->close() && strcmp($loginResult,$login) == 0){
            //Se devuelve status correcto

            return array(
              "status" =>                  TRUE,
            );
           }else{
            //Se devuelve el resultado con el mensaje de error
            return array(
              "status"  =>            FALSE,
              "message" => $this->db->error
            );
           }
       }
   }
 }

   public function delete($userid){
        $status = false;
        //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
        if($sentence = $this->db->prepare("DELETE FROM user WHERE userid = ?;")){
          //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
          $sentence->bind_param("i", $userid);
          //Se ejecuta la sentencia preparada
          if($sentence->execute()){
            $status = ( $sentence->affected_rows > 0 ) ? TRUE : FALSE;
            $sentence->close();
          }
        }
        return $status;
    }

}

?>









































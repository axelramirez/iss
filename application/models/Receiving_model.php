<?php
 require_once("adapters/MySQLConnection.php");

 require_once("helpers/generateUpdateQuery_helper.php");
 require_once("helpers/selectTypeField_helper.php");
 require_once("helpers/referencesValue_helper.php");
/*
 **************************************************************************************************
 ***********************Name: Receiving Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla Receiving
 *********************************** que es la que gestiona los receiving de los clientes
 **************************************************************************************************
*/
class Receiving_model{
  private $db;
  private $model;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
  */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
    $this->model = array(
      "receivingid" => array("type" => "Integer", "primary_key" => TRUE, "null" => FALSE, "unique" => TRUE, "auto_increment" => TRUE, "select"),
      "receivingno" => array("type" => "String"),
      "customerid" => array("type" => "Integer", "null" => FALSE),
      "vendorid" => array("type" => "Integer", "null" => FALSE),
      "carrierid" => array("type" => "Integer", "null" => FALSE),
      "freightbill" => array("type" => "String", "null" => FALSE),
      "invoicenumber" => array("type" => "String"),
      "freightcompany" => array("type" => "String"),
      "file_name" => array("type" => "String"),
      "mime_type" => array("type" => "String"),
      "comments" => array("type" => "String", "null" => FALSE),
      "priority" => array("type" => "Integer", "null" => FALSE),
      "isshipped" => array("type" => "String", "null" => FALSE),
      "createddate" => array("type" => "String", "null" => FALSE),
      "status" => array("type" => "Integer"),
      "userid" => array("type" => "Integer"),
      "NorthOrSouth" => array("type" => "String", "DEFAULT" => TRUE),
      "hazmat" => array("type" => "String", "DEFAULT" => TRUE),
      "damaged" => array("type" => "String", "DEFAULT" => TRUE),
      "hasLinkedDocs" => array("type" => "String", "DEFAULT" => TRUE),
      "sentMail" => array("type" => "String", "DEFAULT" => TRUE),
      "createdOn" => array("type" => "String", "null" => FALSE),
      "autoid" => array("type" => "Integer")
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todos los receiving de los clientes
   **************************************************************************************************
  */
  public function get(){
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select * from receiving;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>                            TRUE,
        "query"  => $query->fetch_all(MYSQLI_ASSOC)
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene un receiving de por su id
   **************************************************************************************************
  */
  public function getById($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from receiving where receivingid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_assoc()
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene los cargos extra por cliente
   **************************************************************************************************
  */
  public function getByCustomer($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("SELECT servicecustomer.servicecustomerid, servicecustomer.customerid, services.serviceid, servicecustomer.cost, services.description, services.`code`, services.variant,
       revisionrates.increment, revisionrates.baserate FROM services LEFT JOIN servicecustomer
       ON services.serviceid = servicecustomer.serviceid AND servicecustomer.customerid = ? LEFT JOIN revisionrates ON revisionrates.customerid = servicecustomer.customerid
       WHERE (servicecustomerid IS NOT NULL AND (services.system = 0 OR (services.system = '1' AND services.description = 'revision'))) OR services.variant = 1 ORDER BY services.description;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC)
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene los shipping por receiving detail
   **************************************************************************************************
  */
  public function getByReceivingDetailId($receivingdetaildId){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("SELECT sh.shippingid, sh.shippingno FROM shippingdetail shd, shipping sh WHERE sh.shippingid = shd.shippingid
       AND shd.quantity > 0 AND shd.receivingdetailid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $receivingdetaildId);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC)
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene los cargos extra por cliente
   **************************************************************************************************
  */
  public function getByreceivingId($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("SELECT sr.servicereceivingid, s.variant, s.serviceid, s.code, s.description, sr.cost FROM services s, servicereceiving sr
       WHERE sr.serviceid = s.serviceid AND sr.receivingid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC)
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
    **************************************************************************************************
    Este metodo obtiene
    **************************************************************************************************
  */
  public function getByShippingId($shippingId, $filter){
    $c = 0;
    $receivingids = 0;
    $ids = 0;
    $rec = "SELECT DISTINCT receivingdetailid AS id FROM shippingdetail WHERE shippingid= $shippingId;";
    if($query = $this->db->query("$rec;")) {
      $receivingids = $query->fetch_all(MYSQLI_ASSOC);
    }
    $lenght = count($receivingids);
    for ($i=0; $i < $lenght; $i++) {
      if ($c == 0) {
        $c++;
        $ids = $receivingids[$i]['id'];
      }
      else {
        $ids.= ',' . $receivingids[$i]['id'];
      }
    }
    $where = "WHERE a.customerid = b.customerid AND a.isshipped = 'F' AND a.status = 1 AND rd.receivingdetailid NOT IN ($ids) AND a.receivingid=rd.receivingid";
    if ($filter != '') {
      $filter = '%' . $filter . '%';
      $where.= " AND a.freightbill LIKE '$filter' OR a.receivingno  LIKE '$filter'";
    }
    //Se ejecuta la consulta y se obtiene el resultado
    if($sentence = $this->db->prepare("SELECT DISTINCT *, b.name AS customername FROM receivingdetail rd, receiving a, customer b $where GROUP BY a.receivingid ORDER BY a.createddate DESC;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      // $sentence->bind_param("i", $id);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
      $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>                     TRUE,
          "query"  => $query->fetch_all(MYSQLI_ASSOC),
          "receivingsid" => $ids
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
    **************************************************************************************************
    Este metodo actualiza el estado de isShipped de un receiving detail
    **************************************************************************************************
  */
  public function updateShipped($receiving){
    $isShipped    =    $receiving["isShipped"];
    $receivingid = $receiving["receivingid"];
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("update receiving set isshipped = ? where receivingid = ?")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("si", $isShipped, $receivingid);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
        if( $this->db->affected_rows == 0 ){
          return  TRUE;
        }else{
          return FALSE;
        }
      }
    }
    //Se imprime el error y se retorna falso de que no se completo la operacion
    print_r($this->db->error);
    return FALSE;
  }
  /*
    **************************************************************************************************
    Esta funcion inserta un nuevo receiving
    **************************************************************************************************
  */
  public function insertreceiving($created){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("INSERT INTO receiving (createddate, isshipped, userid, createdOn, status) VALUES (NOW(),'F', ? ,'C', 1);")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $created);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>   TRUE,
          "receivingid"  => $this->db->insert_id
        );
      }
      //Se cierra la sentencia
      $sentence->close();
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
    **************************************************************************************************
    Esta funcion actualiza un receiving completo
    **************************************************************************************************
  */
  public function update($fields, $array, $receivingId){
    $receiving = $this->getById($receivingId);
    $receiving = $receiving["query"];
    list($haveRegister, $params_type, $query_string, $params) = generateUpdateQuery($fields, $array, $receiving, $this->model);
    if($haveRegister){
      $params_type .= "i";
      array_unshift($params, $params_type);
      array_push($params, $receivingId);
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("UPDATE receiving SET $query_string WHERE receivingid = ?;")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        call_user_func_array(array($sentence, 'bind_param'), referencesValue($params));
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
          //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
          if( $sentence->affected_rows > 0 ){
            //Se cierra la sentencia
            $sentence->close();
            return  TRUE;
          }else{
            $sentence->close();
            return FALSE;
          }
        }
      }
      //Se imprime el error y se retorna falso de que no se completo la operacion
      print_r($this->db->error);
      return FALSE;
    }
    return FALSE;
  }

  public function updateAllHazmat($receivings){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("update receiving set hazmat = ? where receivingid = ?")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("si", $hazmat, $receivingid);
      //Se declara un bloque de transaccion para que se asegure que "todos" los datos del arreglo sea guardado o no
      $this->db->query("START TRANSACTION");
      foreach($receivings as $receiving){
       //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
       $hazmat      =      $receiving["hazmat"];
       $receivingid = $receiving["receivingid"];
       //Se ejecuta la sentencia preparada
       $sentence->execute();
      }
      if($sentence->close()){
       //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
       $this->db->query("COMMIT");
       //Se devuelve status correcto
       return array(
         "status"  => TRUE
       );
      }else{
       //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
       $this->db->query("ROLLBACK");
       //Se devuelve el resultado con el mensaje de error
       return array(
         "status"  =>            FALSE,
         "message" => $this->db->error
       );
      }
    }
  }
  /*
  **************************************************************************************************
  Este metodo actualiza bound
  **************************************************************************************************
  */
  public function updateAllBound($receivings){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("update receiving set NorthOrSouth = ? where receivingid = ?")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("si", $bound, $receivingid);
      //Se declara un bloque de transaccion para que se asegure que "todos" los datos del arreglo sea guardado o no
      $this->db->query("START TRANSACTION");
      foreach($receivings as $receiving){
       //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
       $bound       =       $receiving["bound"];
       $receivingid = $receiving["receivingid"];
       //Se ejecuta la sentencia preparada
       $sentence->execute();
      }
      if($sentence->close()){
       //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
       $this->db->query("COMMIT");
       //Se devuelve status correcto
       return array(
         "status"  => TRUE
       );
      }else{
       //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
       $this->db->query("ROLLBACK");
       //Se devuelve el resultado con el mensaje de error
       return array(
         "status"  =>            FALSE,
         "message" => $this->db->error
       );
      }
    }
  }
  /*
  **************************************************************************************************
  Este metodo actualiza bound
  **************************************************************************************************
  */
  public function updateAllHazmatAndBound($receivings){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("update receiving set hazmat = ?, NorthOrSouth = ? where receivingid = ?")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("ssi", $hazmat, $bound, $receivingid);
      //Se declara un bloque de transaccion para que se asegure que "todos" los datos del arreglo sea guardado o no
      $this->db->query("START TRANSACTION");
      foreach($receivings as $receiving){
       //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
       $bound       =       $receiving["bound"];
       $hazmat      =      $receiving["hazmat"];
       $receivingid = $receiving["receivingid"];
       //Se ejecuta la sentencia preparada
       $sentence->execute();
      }
      if($sentence->close()){
       //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
       $this->db->query("COMMIT");
       //Se devuelve status correcto
       return array(
         "status"  => TRUE
       );
      }else{
       //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
       $this->db->query("ROLLBACK");
       //Se devuelve el resultado con el mensaje de error
       return array(
         "status"  =>            FALSE,
         "message" => $this->db->error
       );
      }
    }
  }
  public function updateRec($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("update receiving set autoid = NULL where autoid = ?")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
       if( $this->db->affected_rows == 0 ){
         return  TRUE;
       }else{
         return FALSE;
       }
     }
   }
   //Se imprime el error y se retorna falso de que no se completo la operacion
   print_r($this->db->error);
   return FALSE;
  }



  public function searchReceivingBy($numberPage, $params, $filter){
    $pages          =       0;
    $query          =    null;
    $empty          =    TRUE;
    $unique         =    TRUE;
    $numberPages    =       0;
    $numberEntries  =       0;
    $elementsByPage =      15;
    $result         = array();
    $bounds = array('SouthBound', 'NorthBound');
    $shipped = array('F', 'T');
    //Se calcula la posicion en el que se mostrara la informacion del resultado de la consulta
    $offset = ($numberPage - 1) * $elementsByPage;
    //Query Count and Search
    $where          = "where ";
    $queryCheckPreshippingList = "(select count(*) as pslist from checktopreship cpl where r.receivingid = cpl.receivingid) as preshipping";
    $queryTrucking = "(select truckingno from truckingnumber tn where tn.receivingid = r.receivingid limit 1) as truckingno";
    $queryInbondDate = "DATE_ADD(r.createddate, INTERVAL  15 day) as inbondDate";
    $queryTotals = "(SELECT ifnull(sum(originalquantity),'0') FROM receivingdetail WHERE receivingdetail.receivingid = r.receivingid) as originalqty, (SELECT ifnull(sum(currentquantity),'0') FROM receivingdetail WHERE receivingdetail.receivingid = r.receivingid) as currentqty, (SELECT ifnull(sum(quantitytoship),'0') FROM receivingdetail WHERE receivingdetail.receivingid = r.receivingid) as shipqty";
    $queryCount     = "select count(*) as numberEntries from receiving r";
    $queryJoin = " inner join customer c on c.customerid = r.customerid inner join vendor v on v.vendorid = r.vendorid inner join carrier ca on ca.carrierid = r.carrierid inner join user u on u.userid = r.userid";
    //autoid section where
    $result          = $this->checkFragment((Integer)$params["receivingNumber"]  > 0, "r.receivingno = '$params[receivingNumber]'", $unique);
    $unique          =   $result["unique"];
    $empty           =    $result["empty"];
    $receivingNumber       = $result["fragment"];
    //customer_id section where
    $result          = $this->checkFragment(strcmp($params["internalNumber"], "") != 0, "r.freightbill = '$params[internalNumber]'", $unique);
    $unique          =   $result["unique"];
    $empty           =    $result["empty"];
    $internalNumber      = $result["fragment"];
    //customerid section where
    $result          = $this->checkFragment((Integer)$params["customerId"] > 0, "r.customerid = '$params[customerId]'", $unique);
    $unique          =   $result["unique"];
    $empty           =    $result["empty"];
    $customerId      = $result["fragment"];
    //typeInvoice section where
    $result          = $this->checkFragment((Integer)$params["vendorId"] > 0, "r.vendorid = '$params[vendorId]'", $unique);
    $unique          =   $result["unique"];
    $empty           =    $result["empty"];
    $vendorId      = $result["fragment"];
    //typeInvoice section where
    $result          = $this->checkFragment(strcmp($params["freightbill"], "") != 0, "(select count(*) from freightbill where fr='$params[freightbill]' and receivingid = r.receivingid) > 0", $unique);
    $unique          =   $result["unique"];
    $empty           =    $result["empty"];
    $freightbill      = $result["fragment"];
    //typeInvoice section where
    $result          = $this->checkFragment(strcmp($params["truckingNumber"], "") != 0, "(select count(*) from truckingnumber where truckingno='$params[truckingNumber]' and receivingid = r.receivingid) > 0", $unique);
    $unique          =   $result["unique"];
    $empty           =    $result["empty"];
    $truckingNumber      = $result["fragment"];
    //typeInvoice section where
    $result          = $this->checkFragment(strcmp($params["inbond"], "") != 0, "(select count(*) from tande where tande='$params[inbond]' and receivingid = r.receivingid) > 0", $unique);
    $unique          =   $result["unique"];
    $empty           =    $result["empty"];
    $tande      = $result["fragment"];
    //typeInvoice section where
    $result          = $this->checkFragment(strcmp($params["purchaseOrder"], "") != 0, "(select count(*) from po where po='$params[purchaseOrder]' and receivingid = r.receivingid) > 0", $unique);
    $unique          =   $result["unique"];
    $empty           =    $result["empty"];
    $po      = $result["fragment"];
    //typeInvoice section where
    $bound = "";
    if((Integer)$params["bound"] > 0 && (Integer)$params["bound"] <= 2){
        $result          = $this->checkFragment(((Integer)$params["bound"] > 0 && (Integer)$params["bound"] <= 2), "r.NorthOrSouth = '".$bounds[(Integer)$params["bound"] - 1]."'", $unique);
        $unique          =   $result["unique"];
        $empty           =    $result["empty"];
        $bound      = $result["fragment"];
    }
    //typeInvoice section where
    $shippedF = "";
    if((Integer)$params["isShipped"] > 0 && (Integer)$params["isShipped"] <= 2){
        $result          = $this->checkFragment((Integer)$params["isShipped"] > 0 && (Integer)$params["isShipped"] <= 2, "r.isshipped = '".$shipped[(Integer)$params["isShipped"] - 1]."'", $unique);
        $unique          =   $result["unique"];
        $empty           =    $result["empty"];
        $shippedF      = $result["fragment"];
    }
    //typeInvoice section where
    $result          = $this->checkFragment(((Integer)$params["priority"] > 0), "r.priority = '$params[priority]'", $unique);
    $unique          =   $result["unique"];
    $empty           =    $result["empty"];
    $priority      = $result["fragment"];
    //typeInvoice section where
    $result          = $this->checkFragment((Integer)$params["productId"] > 0, "r.receivingid IN (SELECT receivingid FROM receivingdetail WHERE productid = '$params[productId]' or receivingdetailid in (select receivingdetailid from receivinglines where productid = '$params[productId]'))", $unique);
    $unique          =   $result["unique"];
    $empty           =    $result["empty"];
    $product      = $result["fragment"];
    //Date section where
    $dateTo          = (strcmp($params["dateTo"], "") != 0) ? "r.createddate <= '$params[dateTo]'" : "";
    $dateFrom        = (strcmp($params["dateFrom"], "") != 0) ? "r.createddate >= '$params[dateFrom]'" : "";
    $result          = $this->checkDate($dateFrom, $dateTo, $unique);
    $unique          = $result["unique"];
    $empty           = $result["empty"];
    $dateSection     = $result["fragment"];
    // //Status section where
    // $statusPaid      =                                       ((Boolean)$params["statusInvoice"]["paid"]) ? "isPaid = 1" : "";
    // $statusActive    =                                    ((Boolean)$params["statusInvoice"]["active"]) ? "isAllow = 1" : "";
    // $statusCancelled =                             ((Boolean)$params["statusInvoice"]["cancelled"]) ? "isCancelled = 1" : "";
    // $result          =                             $this->checkStatus($statusActive, $statusPaid, $statusCancelled, $unique);
    // $unique          =   $result["unique"];
    // $empty           =    $result["empty"];
    // $statusSection   = $result["fragment"];


    if($empty){
      $where = " where status = 1 and receivingno";
    }else{
      $where .= "$receivingNumber $internalNumber $customerId $vendorId $dateSection $freightbill $truckingNumber $tande $po $bound $shippedF $priority $product and r.status = 1";
    }
    //Despues del filtro realiza las consultas de contar y de busqueda
    //Se realiza una consulta para contar los elementos resultado de la consultas a realizar
    if($query = $this->db->query("$queryCount $queryJoin $where;")){
      //Se obtiene el numero de resultados que arrojo la consulta
      $query         =            $query->fetch_assoc();
      $numberEntries = (Integer)$query["numberEntries"];
    }
    //Se calcula el numero de paginas que tendra la consulta
    $pages         =                                           $numberEntries / $elementsByPage;
    $numberPages   =                (Integer)( $pages > (Integer)$pages ? $pages + 1 : $pages );

    $querySearch    = "select r.*, c.name as customer, v.name as vendor, ca.name as carrier, u.roleid, $queryTrucking, $queryInbondDate, $queryTotals, $queryCheckPreshippingList, concat(u.firstname, '', u.lastname) as creator from (select * from receiving r $where order by receivingid desc limit $offset,$elementsByPage) r ";

    if($query = $this->db->query("$querySearch $queryJoin order by r.receivingid desc;")){
      //Se devuelve el resultado con la sentencia y los resultados de paginacion
      return array(
        "status"        =>                           TRUE,
        "query"         => $query->fetch_all(MYSQL_ASSOC),
        "numberPages"   =>                   $numberPages,
        "numberEntries" =>                 $numberEntries
      );

    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status" =>             FALSE,
      "message" => $this->db->error
    );
  }

  /*
   **************************************************************************************************
   Este metodo obtiene todos las facturas y con su estado (pagada, cancelada, total, monto pagado, monto
   que se debe) de los clientes del sistema
   **************************************************************************************************
   */
  private function checkDate($startDate, $endDate, $unique){
    $fragment = "";
    $separator = "and";
    if( $unique ){
      $separator = "";
    }
    if( strcmp($startDate, "") != 0 && strcmp($endDate, "") != 0 ){
      $fragment = "$separator ($startDate and $endDate)";
      $unique = FALSE;
    }else if( strcmp($startDate, "") != 0 ){
      $fragment =                "$separator $startDate";
      $unique = FALSE;
    }else if( strcmp($endDate, "") != 0 ){
      $fragment =                  "$separator $endDate";
      $unique = FALSE;
    }
    return array(
      "empty"    =>   $unique,
      "unique"   =>   $unique,
      "fragment" => $fragment
    );
  }

  /*
   **************************************************************************************************
   Este metodo obtiene todos las facturas y con su estado (pagada, cancelada, total, monto pagado, monto
   que se debe) de los clientes del sistema
   **************************************************************************************************
   */
  private function checkStatus($isAllow, $isPaid, $isCancelled, $unique){
    $fragment = "";
    $separator = "and";
    if( $unique ){
      $separator = "";
    }
    if( strcmp($isAllow, "") != 0 && strcmp($isPaid, "") != 0 && strcmp($isCancelled, "") != 0 ){
      $fragment = "$separator ($isAllow or $isPaid or $isCancelled)";
      $unique = FALSE;
    }else if( strcmp($isAllow, "") != 0 && strcmp($isPaid, "") != 0 ){
      $fragment = "$separator ($isAllow or $isPaid)";
      $unique = FALSE;
    }else if( strcmp($isAllow, "") != 0 && strcmp($isCancelled, "") != 0 ){
      $fragment = "$separator ($isAllow or $isCancelled)";
      $unique = FALSE;
    }else if( strcmp($isPaid, "") != 0 && strcmp($isCancelled, "") != 0 ){
      $fragment = "$separator ($isPaid or $isCancelled)";
      $unique = FALSE;
    }else if( strcmp($isAllow, "") != 0 ){
      $fragment = "$separator $isAllow";
      $unique = FALSE;
    }else if( strcmp($isPaid, "") != 0 ){
      $fragment = "$separator $isPaid";
      $unique = FALSE;
    }else if( strcmp($isCancelled, "") != 0 ){
      $fragment = "$separator $isCancelled";
      $unique = FALSE;
    }
    return array(
      "empty"    =>   $unique,
      "unique"   =>   $unique,
      "fragment" => $fragment
    );
  }

  /*
   **************************************************************************************************
   Este metodo obtiene todos las facturas y con su estado (pagada, cancelada, total, monto pagado, monto
   que se debe) de los clientes del sistema
   **************************************************************************************************
   */
  private function checkFragment($condition, $fragment, $unique){
    if( $condition ){
      return array(
        "empty"    =>                                 FALSE,
        "unique"   =>                                 FALSE,
        "fragment" => $this->checkEmpty($fragment, $unique)
      );
    }
    return array(
      "empty"    => $unique,
      "unique"   => $unique,
      "fragment" =>      ""
    );
  }
  private function checkEmpty($fragment, $unique){
    $separator = "and";
    if( $unique ){
      $separator = "";
      $unique = FALSE;
    }
    return "$separator $fragment";
  }
}
?>

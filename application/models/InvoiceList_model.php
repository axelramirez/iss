<?php
 require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: Customer Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla InvoiceList
 *********************************** que es la que gestiona los List de facturas de los clientes
 **************************************************************************************************
*/
class InvoiceList_model{
  private $db;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }
  /*
  **************************************************************************************************
  Este metodo obtiene todos los clientes del sistema
  **************************************************************************************************
  */
  public function get(){
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select * from invoicelist;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>   TRUE,
        "query"  => $query
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene un cliente del sistema por id
   **************************************************************************************************
  */
  public function getById($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from invoicelist where invoicelistid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>   TRUE,
         "query"  => $query
       );
     }
     //Se cierra la sentencia
     $sentence->close();
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene un cliente del sistema por id
   **************************************************************************************************
  */
  public function getByIdAndType($id, $type = 0){
    $where  = "where invoiceid = $id";
    switch($type){
      case 1:
      $where += "and extra = 1";
      break;
      case 2:
      $where += "and extra != 1";
      break;
    }
    if($query = $this->db->query("select * from invoicelist $where and qty > 0 order by invoicelistid;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>                            TRUE,
        "query"  => $query->fetch_all(MYSQLI_ASSOC)
      );
    }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
  **************************************************************************************************
  Este metodo inserta una nueva list de factura
  **************************************************************************************************
  */
  public function save($invoicelist){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("insert into invoicelist(invoiceid, qty, description, receivingid, receivingno, unitprice, total, extra, shippingdetailid) values(?, ?, ?, ?, ?, ?, ?, ? , ?)")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("iisiiddii", $invoicelist["invoiceid"], $invoicelist["qty"], $invoicelist["description"], $invoicelist["receivingid"], $invoicelist["receivingno"], $invoicelist["unitprice"], $invoicelist["total"], $invoicelist["extra"], $invoicelist["shippingdetailid"]);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>   TRUE,
          "invoicelistid"  => $this->db->insert_id
        );
      }
      //Se cierra la sentencia
      $sentence->close();
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo inserta una multiples list de factura
   **************************************************************************************************
  */
  public function saveAll($invoiceid, $invoicelists){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("insert into invoicelist(invoiceid, qty, description, receivingid, receivingno, unitprice, total, extra, shippingdetailid) values(?, ?, ?, ?, ?, ?, ?, ? , ?)")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("iisiiddii", $invoiceid, $qty, $description, $receivingid, $receivingno, $unitprice, $total, $extra, $shippingdetailid);
      //Se declara un bloque de transaccion para que se asegure que "todos" los datos del arreglo sea guardado o no
      $this->db->query("START TRANSACTION");
      foreach($invoicelists as $invoicelist){
       //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
       $qty              =              $invoicelist["qty"];
       $total            =            $invoicelist["total"];
       $extra            =            $invoicelist["extra"];
       $unitprice        =        $invoicelist["unitprice"];
       $description      =      $invoicelist["description"];
       $receivingid      =      $invoicelist["receivingid"];
       $receivingno      =      $invoicelist["receivingno"];
       $shippingdetailid = $invoicelist["shippingdetailid"];
       //Se ejecuta la sentencia preparada
       $sentence->execute();
      }
      if($sentence->close()){
       //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
       $this->db->query("COMMIT");
       //Se devuelve status correcto
       return array(
         "status"  => TRUE
       );
      }else{
       //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
       $this->db->query("ROLLBACK");
       //Se devuelve el resultado con el mensaje de error
       return array(
         "status"  =>            FALSE,
         "message" => $this->db->error
       );
      }
    }
  }
  /*
  **************************************************************************************************
  Este metodo actualiza una list de factura
  **************************************************************************************************
  */
  public function update($invoicelist){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("update invoicelist set description = ?, unitprice = ?, total = ? where invoicelistid = ?")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("sdd", $invoicelist["description"], $invoicelist["unitprice"], $invoicelist["total"], $invoicelist["invoicelistid"]);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>   TRUE,
          "invoicelistid"  => $this->db->insert_id
        );
      }
      //Se cierra la sentencia
      $sentence->close();
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo inserta una multiples list de factura
   **************************************************************************************************
  */
  public function updateAll($invoicelists){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("update invoicelist set description = ?, unitprice = ?, total = ? where invoicelistid = ?")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("sddi", $description, $unitprice, $total, $invoicelistid);
      //Se declara un bloque de transaccion para que se asegure que "todos" los datos del arreglo sea guardado o no
      $this->db->query("START TRANSACTION");
      foreach($invoicelists as $invoicelist){
       //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
       $total            =         $invoicelist["total"];
       $unitprice        =     $invoicelist["unitprice"];
       $description      =   $invoicelist["description"];
       $invoicelistid    = $invoicelist["invoicelistid"];
       //Se ejecuta la sentencia preparada
       $sentence->execute();
      }
      if($sentence->close()){
       //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
       $this->db->query("COMMIT");
       //Se devuelve status correcto
       return array(
         "status"  => TRUE
       );
      }else{
       //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
       $this->db->query("ROLLBACK");
       //Se devuelve el resultado con el mensaje de error
       return array(
         "status"  =>            FALSE,
         "message" => $this->db->error
       );
      }
    }
  }
  /*
   **************************************************************************************************
   Este metodo elimina invoicelist por su invoiceid
   **************************************************************************************************
  */
  public function deleteByInvoiceId($invoiceid){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("delete from invoicelist where invoiceid = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $invoiceid);
       //Se ejecuta la sentencia preparada
       if($sentence->execute()){
         //Se cierra la sentencia
         $sentence->close();
         if($this->db->affected_rows != 0){
           return TRUE;
         }else{
           return FALSE;
         }
       }
       $sentence->close();
    }
    return FALSE;
  }
}
?>

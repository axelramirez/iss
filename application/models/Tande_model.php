<?php
require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: Unit Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla po
 **************************************************************************************************
 */
class Tande_model{
  private $db;
  private $model;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
    $this->model = array(
      "tandeid" => array("type" => "Integer"),
      "tande" => array("type" => "String"),
      "tandeexpires" => array("type" => "String"),
      "receivingid" => array("type" => "Integer"),
      "stat" => array("type" => "Integer"),
      "Canceldate" => array("type" => "String"),
      "isShipped" => array("type" => "Integer")
    );
  }
  public function getById($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from tande where tandeid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_assoc()
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
   **************************************************************************************************
   Este metodo elimina un un purchase order de receiving details
   **************************************************************************************************
  */
  public function delete($arrays, $receivingId){
    $status = false;
    foreach($arrays as $key => $array){
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("delete from tande where tandeid = ? and receivingid = ?")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        $sentence->bind_param("ii", $array['tandeid'], $receivingId);
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
          $status = ( $sentence->affected_rows > 0 ) ? TRUE : FALSE;
          $sentence->close();
        }
      }
    }
    return $status;
  }
  /*
   **************************************************************************************************
   Este metodo inserta un nuevo purchase Order
   **************************************************************************************************
  */
  public function insert($ntds, $receivingId){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("INSERT INTO tande(tande, tandeexpires, receivingid) VALUES(?, ?, ?)")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("ssi", $tandename, $tandeexpires, $receivingId);

      $this->db->query("START TRANSACTION");
      foreach($ntds as $td){
       //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
       $tandename = $td["po"];
       $tandeexpires = $td["poDate"];
       //Se ejecuta la sentencia preparada
       $sentence->execute();
      }
      if($sentence->close()){
       //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
       $this->db->query("COMMIT");
       //Se devuelve status correcto
       return TRUE;
      }else{
       //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
       $this->db->query("ROLLBACK");
       //Se devuelve el resultado con el mensaje de error
       return FALSE;
      }
  }
}
  public function update($tds, $receivingid){
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("UPDATE tande set tande = ?, tandeexpires = ? where tandeid = ? and receivingid = ?;")){
        //Se asignan parametros al '?'
        $sentence->bind_param("ssii", $tdname, $tandeexpires, $tandeid, $receivingid);
        //Se declara un bloque de transaccion para que se asegure que "todos" los datos del arreglo sea guardado o no
        $this->db->query("START TRANSACTION");
        foreach($tds as $td){
         //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
         $tdname = $td["tande"];
         $tandeexpires = $td["tandeexpires"];
         //Se ejecuta la sentencia preparada
         $sentence->execute();
        }
        if($sentence->close()){
         //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
         $this->db->query("COMMIT");
         //Se devuelve status correcto
         return TRUE;
        }else{
         //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
         $this->db->query("ROLLBACK");
         //Se devuelve el resultado con el mensaje de error
         return FALSE;
        }
      }
  }
  public function updateMain($fields, $arrays){
    $status = FALSE;
    foreach($arrays as $key => $array){
      $tande = $this->getById($array["tandeid"]);
      $tande = $tande["query"];
      list($haveRegister, $params_type, $query_string, $params) = generateUpdateQuery($fields, $array, $tande, $this->model);
      if($haveRegister){
        $params_type .= "i";
        array_unshift($params, $params_type);
        array_push($params, $array["tandeid"]);
        //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
        if($sentence = $this->db->prepare("update tande set $query_string where tandeid = ?")){
          //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
          call_user_func_array(array($sentence, 'bind_param'), referencesValue($params));
          //Se ejecuta la sentencia preparada
          if($sentence->execute()){
            //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
            $status = ( $sentence->affected_rows > 0 ) ? TRUE : FALSE;
            $sentence->close();
          }
        }
      }
    }
  }

  /*
   **************************************************************************************************
   Este metodo inserta un nuevo T&E
   **************************************************************************************************
  */
  public function save($arrays, $receivingId){
    $status = FALSE;
    foreach($arrays as $key => $array){
      $this->db->query("START TRANSACTION");
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("INSERT INTO tande(tande, tandeexpires, receivingid) values(?, ?, ?);")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        $sentence->bind_param("ssi", $array["tande"], $array["tandeexpires"], $receivingId);
        $sentence->execute();
        if($sentence->close()){
          $this->db->query("COMMIT");
          $status = TRUE;
        }else{
          $this->db->query("ROLLBACK");
          $status = FALSE;
          break;
        }
      }
    }
    return $status;
  }
    public function getByReceivingId($id){
      //Se ejecuta la consulta y se obtiene el resultado
      if($sentence = $this->db->prepare("SELECT tandeid, tande, date_format(tandeexpires, '%Y-%m-%d') AS tandeexpires , receivingid, stat, Canceldate, isShipped FROM tande WHERE receivingid = ?;")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        $sentence->bind_param("i", $id);
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
        $query = $sentence->get_result();
          //Se cierra la sentencia
          $sentence->close();
          //Se devuelve el resultado con la sentencia
          return array(
            "status" =>                     TRUE,
            "query"  => $query->fetch_all(MYSQLI_ASSOC)
          );
        }
      }
      //Se devuelve el resultado con el mensaje de error
      return array(
        "status"  =>            FALSE,
        "message" => $this->db->error
      );
    }
}
?>

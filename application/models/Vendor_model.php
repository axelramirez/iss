<?php
require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: Unit Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla po
 **************************************************************************************************
 */
class Vendor_model{
  private $db;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }

  /*
   **************************************************************************************************
   Este metodo inserta un nuevo vendedor
   **************************************************************************************************
  */
  public function insert($newvendor){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("INSERT INTO vendor(name) values(?)")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("s", $name["name"]);

      $this->db->query("START TRANSACTION");

      if($sentence->close()){
       //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
       $this->db->query("COMMIT");
       //Se devuelve status correcto
       return TRUE;
      }else{
       //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
       $this->db->query("ROLLBACK");
       //Se devuelve el resultado con el mensaje de error
       return FALSE;
      }
  }
}
/*
 **************************************************************************************************
 Este metodo trrae los vendedores ordenados por nombre
 **************************************************************************************************
*/
  public function get(){
      //Se ejecuta la consulta y se obtiene el resultado
      if($sentence = $this->db->prepare("SELECT * FROM vendor ORDER BY name;")){
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
        $query = $sentence->get_result();
          //Se cierra la sentencia
          $sentence->close();
          //Se devuelve el resultado con la sentencia
          return array(
            "status" =>                     TRUE,
            "query"  => $query->fetch_all(MYSQLI_ASSOC)
          );
        }
      }
      //Se devuelve el resultado con el mensaje de error
      return array(
        "status"  =>            FALSE,
        "message" => $this->db->error
      );
  }

  public function getByOptions(){
      //Se ejecuta la consulta y se obtiene el resultado
      if($sentence = $this->db->prepare("SELECT vendorid as value, name FROM vendor ORDER BY name;")){
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
        $query = $sentence->get_result();
          //Se cierra la sentencia
          $sentence->close();
          //Se devuelve el resultado con la sentencia
          return array(
            "status" =>                     TRUE,
            "query"  => $query->fetch_all(MYSQLI_ASSOC)
          );
        }
      }
      //Se devuelve el resultado con el mensaje de error
      return array(
        "status"  =>            FALSE,
        "message" => $this->db->error
      );
  }

  public function saveSimple($name){
      //Se ejecuta la consulta y se obtiene el resultado
      if($sentence = $this->db->prepare("insert into vendor(name) values(?);")){
        $sentence->bind_param('s', $name);
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
        $query = $sentence->get_result();
          //Se cierra la sentencia
          $sentence->close();
          //Se devuelve el resultado con la sentencia
          return array(
            "status" =>                     TRUE,
            "last_id"  => $this->db->insert_id
          );
        }
      }
      //Se devuelve el resultado con el mensaje de error
      return array(
        "status"  =>            FALSE,
        "message" => $this->db->error
      );
  }

  public function getByName($name){
      //Se ejecuta la consulta y se obtiene el resultado
      if($sentence = $this->db->prepare("select vendorid, name from vendor where name like ?")){
        $sentence->bind_param('s', $name);
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
        $query = $sentence->get_result();
          //Se cierra la sentencia
          $sentence->close();
          //Se devuelve el resultado con la sentencia
          return array(
            "status" =>                     TRUE,
            "query"  => $query->fetch_assoc()
          );
        }
      }
      //Se devuelve el resultado con el mensaje de error
      return array(
        "status"  =>            FALSE,
        "message" => $this->db->error
      );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todas las descripciones de vendors por limit
   **************************************************************************************************
   */
   public function getbypagination($numberPage){
       //Se define cuantos elementos se van a consultar
      $elementsByPage = 5;
      //Se define desde donde se va a consultar
      $offset = ($numberPage - 1) * $elementsByPage;
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
     $queryCount = "SELECT COUNT(*) AS entries FROM vendor";
     if($query = $this->db->query("$queryCount;")){
       //Se obtiene el numero de resultados que arrojo la consulta
       $query         =            $query->fetch_assoc();
       $numberEntries = (Integer)$query["entries"];
     }
     //Se calcula el numero de paginas que tendra la consulta
     $pages         =                                           $numberEntries / $elementsByPage;
     $numberPages   =                (Integer)( $pages > (Integer)$pages ? $pages + 1 : $pages );
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("SELECT * FROM vendor ORDER BY name LIMIT $offset,$elementsByPage;")){
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>                  TRUE,
          "query"  => $query->fetch_all(MYSQLI_ASSOC),
          "numberEntries" => $numberEntries,
          "numberPages" => $numberPages
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
   }
   /*
    **************************************************************************************************
    Este metodo inserta un nuevo vendor con todos sus campos
    **************************************************************************************************
   */
   public function insertVendor($data){
       $vendorNo = $data["vendorNo"];
       $vendorName = $data["vendorName"];
       $vendorAddress = $data["vendorAddress"];
       $vendorCity = $data["vendorCity"];
       $vendorCountry = $data["vendorCountry"];
       $vendorState = $data["vendorState"];
       $vendorZipcode = $data["vendorZipcode"];
       $vendorPhone = $data["vendorPhone"];
       $vendorFax = $data["vendorFax"];
       $vendorContactname = $data["vendorContactname"];
       $vendorEmail = $data["vendorEmail"];
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
       if($sentence = $this->db->prepare("INSERT INTO vendor (vendorno, name, address, city, state, zipcode, country, phone, fax, contact, contactemail) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);")){
         //Se asignan parametros al '?'
         $sentence->bind_param("issssisiiss", $vendorNo, $vendorName, $vendorAddress, $vendorCity, $vendorState, $vendorZipcode, $vendorCountry, $vendorPhone, $vendorFax, $vendorContactname, $vendorEmail);
         $this->db->query("START TRANSACTION");
         //Se ejecuta la sentencia preparada
         if ($sentence->execute()) {
             if($sentence->close()){
              //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
              $this->db->query("COMMIT");
              //Se devuelve status correcto
              return TRUE;
             }else{
              //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
              $this->db->query("ROLLBACK");
              //Se devuelve el resultado con el mensaje de error
              return FALSE;
             }
         }
     }
 }
 /*
  **************************************************************************************************
  Este metodo actualiza los campos de un vendor
  **************************************************************************************************
 */
 public function updateVendor($data, $vendorId){
     $vendorNo = $data["vendorNo"];
     $vendorName = $data["vendorName"];
     $vendorAddress = $data["vendorAddress"];
     $vendorCity = $data["vendorCity"];
     $vendorCountry = $data["vendorCountry"];
     $vendorState = $data["vendorState"];
     $vendorZipcode = $data["vendorZipcode"];
     $vendorPhone = $data["vendorPhone"];
     $vendorFax = $data["vendorFax"];
     $vendorContactname = $data["vendorContactname"];
     $vendorEmail = $data["vendorEmail"];
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
     if($sentence = $this->db->prepare("UPDATE vendor SET vendorno = ?, name = ?, address = ?, city = ?, state = ?, zipcode = ?, country = ?, phone = ?, fax = ?, contact = ?, contactemail = ? WHERE vendorid = ?;")){
       //Se asignan parametros al '?'
       $sentence->bind_param("issssisiissi", $vendorNo, $vendorName, $vendorAddress, $vendorCity, $vendorState, $vendorZipcode, $vendorCountry, $vendorPhone, $vendorFax, $vendorContactname, $vendorEmail, $vendorId);
       $this->db->query("START TRANSACTION");
       //Se ejecuta la sentencia preparada
       if ($sentence->execute()) {
           if($sentence->close()){
            //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
            $this->db->query("COMMIT");
            //Se devuelve status correcto
            return TRUE;
           }else{
            //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
            $this->db->query("ROLLBACK");
            //Se devuelve el resultado con el mensaje de error
            return FALSE;
           }
       }
   }
}
/*
 **************************************************************************************************
 Este metodo obtiene los resultado de la busqueda
 **************************************************************************************************
 */
 public function searchbypagination($numberPage, $filter){
     //Se define cuantos elementos se van a consultar
    $elementsByPage = 5;
    $filterLike = '%' . $filter . '%';
    // print($filterLike);
    //Se define desde donde se va a consultar
    $offset = ($numberPage - 1) * $elementsByPage;
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   $queryCount = "SELECT COUNT(*) AS entries FROM vendor WHERE name LIKE '$filterLike' OR vendorno LIKE '$filterLike'";
   if($query = $this->db->query("$queryCount;")){
     //Se obtiene el numero de resultados que arrojo la consulta
     $query         =            $query->fetch_assoc();
     $numberEntries = (Integer)$query["entries"];
   }
   //Se calcula el numero de paginas que tendra la consulta
   $pages         =                                           $numberEntries / $elementsByPage;
   $numberPages   =                (Integer)( $pages > (Integer)$pages ? $pages + 1 : $pages );
  //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
  if($sentence = $this->db->prepare("SELECT * FROM vendor WHERE name LIKE '$filterLike' OR vendorno LIKE '$filterLike' LIMIT $offset,$elementsByPage;")){
    //Se ejecuta la sentencia preparada
    if($sentence->execute()){
      //Se obtiene el resultado de la ejecucion de la sentencia
      $query = $sentence->get_result();
      //Se cierra la sentencia
      $sentence->close();
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>                  TRUE,
        "query"  => $query->fetch_all(MYSQLI_ASSOC),
        "numberEntries" => $numberEntries,
        "numberPages" => $numberPages
      );
    }
  }
  //Se devuelve el resultado con el mensaje de error
  return array(
    "status"  =>            FALSE,
    "message" => $this->db->error
  );
 }
}
?>

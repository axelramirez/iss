<?php

require_once("adapters/MySQLConnection.php");


class Seals_model{
    private $db;

    public function __construct(){
        $this->db = MySQLConnection::getInstance()->getConnection();
    }
    public function get($shippingId,$customerId){
      // print($shippingId);
      if($query = $this->db->query("SELECT idSeal AS value, seal AS text FROM sealsview
        WHERE idCustomer IS NULL AND (status = 'New' || shippingid = $shippingId);")) {
        //se obtiene el numero de resultados de la consulta
        $query         =       $query->fetch_all(MYSQL_ASSOC);
        $internalSeals =       $query;
      }
      if($query = $this->db->query("SELECT idSeal AS value, seal AS text FROM sealsview
        WHERE idCustomer = $customerId AND (status = 'New' || shippingid = $shippingId);")){
        //Se devuelve el resultado con la sentencia y los resultados de paginacion
        return array(
          "status"        =>                           TRUE,
          "query"         => $query->fetch_all(MYSQL_ASSOC),
          "internal"      => $internalSeals
        );
      }
      //Se devuelve el resultado con el mensaje de error
      return array(
        "status" =>             FALSE,
        "message" => $this->db->error
      );
    }
}

?>

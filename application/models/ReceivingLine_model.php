<?php
 require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: Receiving Line Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla Receiving
 *********************************** Line que es la que gestiona los receiving detail de los clientes
 **************************************************************************************************
*/
class ReceivingLine_model{
  private $db;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
  */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
    $this->model = array(
      "receivinglinesid" => array("type" => "Integer"),
      "receivingdetailid" => array("type" => "Integer"),
      "productid" => array("type" => "Integer"),
      "unitid" => array("type" => "Integer"),
      "idLocation" => array("type" => "Integer"),
      "originalQty" => array("type" => "Integer"),
      "partQty" => array("type" => "Integer"),
      "currentQty" => array("type" => "Integer"),
      "lotnumber" => array("type" => "String"),
      "expDate" => array("type" => "String"),
      "po" => array("type" => "String"),
      "podate" => array("type" => "String"),
      "active" => array("type" => "Integer")
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todos los receiving detail
   **************************************************************************************************
  */
  public function get(){
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select * from receivinglines;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>                            TRUE,
        "query"  => $query->fetch_all(MYSQLI_ASSOC)
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene un receiving detail por su id
   **************************************************************************************************
  */
  public function getById($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from receivinglines where receivinglinesid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_assoc()
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
   **************************************************************************************************
   Este metodo actualiza un receiving line
   **************************************************************************************************
  */
  public function update($receivingLine){
   $currentQty       =       $receivingLine["currentQty"];
   $receivinglinesid = $receivingLine["receivinglinesid"];
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("update receivinglines set currentQty = ? where receivinglinesid = ?")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("ii", $currentQty, $receivinglinesid);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
       if( $this->db->affected_rows == 0 ){
         return  TRUE;
       }else{
         return FALSE;
       }
     }
   }
   //Se imprime el error y se retorna falso de que no se completo la operacion
   print_r($this->db->error);
   return FALSE;
  }
  /*
   **************************************************************************************************
   Este metodo actualiza multiples receiving lines (currentQty)
   **************************************************************************************************
  */
  public function updateAll($receivingsLines){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("update receivinglines set currentQty = ? where receivinglinesid = ?")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("ii", $currentQty, $receivinglinesid);
      //Se declara un bloque de transaccion para que se asegure que "todos" los datos del arreglo sea guardado o no
      $this->db->query("START TRANSACTION");
      foreach($receivingsLines as $receivingLine){
       //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
       $currentQty       =       $receivingLine["currentQty"];
       $receivinglinesid = $receivingLine["receivinglinesid"];
       //Se ejecuta la sentencia preparada
       $sentence->execute();
      }
      if($sentence->close()){
       //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
       $this->db->query("COMMIT");
       //Se devuelve status correcto
       return array(
         "status"  => TRUE
       );
      }else{
       //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
       $this->db->query("ROLLBACK");
       //Se devuelve el resultado con el mensaje de error
       return array(
         "status"  =>            FALSE,
         "message" => $this->db->error
       );
      }
    }
  }

  public function updateMain($fields, $arrays, $receivingDetails, $products){
    $status = FALSE;
    $product = array();
    foreach($arrays as $key=>$array){
      $array["currentQty"] = $array["originalQty"];
      $array["receivingdetailid"] = $receivingDetails[$array["receivingdetailno"] - 1]["receivingdetailid"];
      if($array["newProduct"]){
        $array["productid"] = $products[$array["product"]];
      }
      $receivingLine = $this->getById($array["receivinglinesid"]);
      $receivingLine = $receivingLine["query"];
      list($haveRegister, $params_type, $query_string, $params) = generateUpdateQuery($fields, $array, $receivingLine, $this->model);
      if($haveRegister){
        $params_type .= "i";
        array_unshift($params, $params_type);
        array_push($params, $array["receivinglinesid"]);
        //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
        if($sentence = $this->db->prepare("update receivinglines set $query_string where receivinglinesid = ?")){
          //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
          call_user_func_array(array($sentence, 'bind_param'), referencesValue($params));
          //Se ejecuta la sentencia preparada
          if($sentence->execute()){
            //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
            $status = ( $sentence->affected_rows > 0 ) ? TRUE : FALSE;
            //Se cierra la sentencia
            $sentence->close();
          }
        }
      }
    }
    return $status;
  }
  
  public function save($arrays, $receivingDetails, $products){
    $status = FALSE;
    $product = array();
    foreach($arrays as $key => $array){
      $array["currentQty"] = $array["originalQty"];
      $this->db->query("START TRANSACTION");
      $array["receivingdetailid"] = $receivingDetails[$array["receivingdetailno"] - 1]["receivingdetailid"];

      if($array["newProduct"]){
        $array["productid"] = $products[$array["product"]];
      }
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("INSERT INTO receivinglines(receivingdetailid, productid, unitid, originalQty, partQty, currentQty, lotnumber, expDate, po, podate) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        $sentence->bind_param("iiidiissss", $array["receivingdetailid"], $array["productid"], $array["unitid"], $array["originalQty"], $array["partQty"], $array["currentQty"], $array["lotnumber"], $array["expDate"], $array["po"], $array["podate"]);
        $sentence->execute();
        if($sentence->close()){
          $this->db->query("COMMIT");
          $status = TRUE;
        }else{
          $this->db->query("ROLLBACK");
          $status = FALSE;
          break;
        }
      }
    }
    return $status;
  }


  public function delete($arrays){
    $status = false;
    foreach($arrays as $key => $array){
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("delete from receivinglines where receivinglinesid = ?")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        $sentence->bind_param("i", $array['receivinglinesid']);
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
          $status = ( $sentence->affected_rows > 0 ) ? TRUE : FALSE;
          $sentence->close();
        }
      }
    }
    return $status;
  }

  public function deleteByReceivingDetailId($arrays){
    $status = false;
    foreach($arrays as $key => $array){
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("delete from receivinglines where receivingdetailid = ?")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        $sentence->bind_param("i", $array['receivingdetailid']);
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
          $status = ( $sentence->affected_rows > 0 ) ? TRUE : FALSE;
          $sentence->close();
        }
      }
    }
    return $status;
  }
}
?>

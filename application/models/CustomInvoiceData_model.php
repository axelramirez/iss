<?php
   require_once("adapters/MySQLConnection.php");
   /*
    **************************************************************************************************
    ***********************Name: PaymentReceived Class Model
    ***********************Description: Realiza las consultas correspondientes a la tabla payment_received
    *********************************** que es la que gestiona los pagos de facturas de un cliente
    **************************************************************************************************
   */
  class CustomInvoiceData_model{
    private $db;
    /*
     **************************************************************************************************
     Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
     **************************************************************************************************
     */
    public function __construct(){
      $this->db = MySQLConnection::getInstance()->getConnection();
    }
    /*
     **************************************************************************************************
     Este metodo inserta un nuevo pago a una autoinvoice
     **************************************************************************************************
    */
    public function save($pedimentoNumber, $invoiceId){
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("insert into custominvoicedata(autoid, PedimentNo) values(?, ?)")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        $sentence->bind_param("ii", $invoiceId, $pedimentoNumber);
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
          //Se cierra la sentencia
          $sentence->close();
          //Se devuelve el resultado con la sentencia
          return array(
            "status" =>                     TRUE,
            "autoinvoice_id"  => $this->db->insert_id
          );
        }
      }
      //Se devuelve el resultado con el mensaje de error
      return array(
        "status"  =>            FALSE,
        "message" => $this->db->error
      );
    }
    /*
     **************************************************************************************************
     Este metodo elimina un pago sobre una autoinvoice
     **************************************************************************************************
    */
    public function delete($id){
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("delete from custominvoicedata where autoid = ?")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        $sentence->bind_param("i", $id);
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
          //Se cierra la sentencia
          $sentence->close();
          //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
          if( $this->db->affected_rows != 0 ){
            return  TRUE;
          }else{
            return FALSE;
          }
        }
      }
      //Se devuelve el resultado con el mensaje de error
      return array(
        "status"  =>            FALSE,
        "message" => $this->db->error
      );
    }
    /*
     **************************************************************************************************
     Este metodo actualiza un pago a un autoinvoice
     **************************************************************************************************
    */
    public function update($pedimentoNumber, $invoiceId){
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("update custominvoicedata set PedimentNo = ? where autoid = ?")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        $sentence->bind_param("ii", $pedimentoNumber, $invoiceId);
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
          //Se cierra la sentencia
          $sentence->close();
          //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
          if( $this->db->affected_rows != 0 ){
            return  TRUE;
          }else{
            return FALSE;
          }
        }
      }
      //Se devuelve el resultado con el mensaje de error
      return array(
        "status"  =>            FALSE,
        "message" => $this->db->error
      );
    }
  }
  ?>

<?php
require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: Unit Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla unit que
 *********************************** es la que gestiona las unidades de mercancia que maneja el
 *********************************** sistema
 **************************************************************************************************
 */
class Unit_model{
  private $db;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }

  /*
   **************************************************************************************************
   Este metodo obtiene todas las descripciones de las mercancias
   **************************************************************************************************
   */
   public function get(){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("SELECT * FROM unit;")){
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>                  TRUE,
          "query"  => $query->fetch_all(MYSQLI_ASSOC)
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
   }
   /*
    **************************************************************************************************
    Este metodo obtiene todas las descripciones de las mercancias por limit
    **************************************************************************************************
    */
    public function getbypagination($numberPage){
        //Se define cuantos elementos se van a consultar
       $elementsByPage = 5;
       //Se define desde donde se va a consultar
       $offset = ($numberPage - 1) * $elementsByPage;
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      $queryCount = "SELECT COUNT(*) AS entries FROM unit";
      if($query = $this->db->query("$queryCount;")){
        //Se obtiene el numero de resultados que arrojo la consulta
        $query         =            $query->fetch_assoc();
        $numberEntries = (Integer)$query["entries"];
      }
      //Se calcula el numero de paginas que tendra la consulta
      $pages         =                                           $numberEntries / $elementsByPage;
      $numberPages   =                (Integer)( $pages > (Integer)$pages ? $pages + 1 : $pages );
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
     if($sentence = $this->db->prepare("SELECT * FROM unit LIMIT $offset,$elementsByPage;")){
       //Se ejecuta la sentencia preparada
       if($sentence->execute()){
         //Se obtiene el resultado de la ejecucion de la sentencia
         $query = $sentence->get_result();
         //Se cierra la sentencia
         $sentence->close();
         //Se devuelve el resultado con la sentencia
         return array(
           "status" =>                  TRUE,
           "query"  => $query->fetch_all(MYSQLI_ASSOC),
           "numberEntries" => $numberEntries,
           "numberPages" => $numberPages
         );
       }
     }
     //Se devuelve el resultado con el mensaje de error
     return array(
       "status"  =>            FALSE,
       "message" => $this->db->error
     );
    }
  /*
   **************************************************************************************************
   Este metodo obtiene una descripcion de mercancias por su id
   **************************************************************************************************
   */
  public function getById($id){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("select * from unit where unitid = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $id);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>   TRUE,
          "query"  => $query
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo inserta un nuevo unit
   **************************************************************************************************
  */
  public function insert($unitName){
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("INSERT INTO unit (description) VALUES (?);")){
        //Se asignan parametros al '?'
        $sentence->bind_param("s", $unitName);
        $this->db->query("START TRANSACTION");
        //Se ejecuta la sentencia preparada
        if ($sentence->execute()) {
            if($sentence->close()){
             //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
             $this->db->query("COMMIT");
             //Se devuelve status correcto
             return TRUE;
            }else{
             //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
             $this->db->query("ROLLBACK");
             //Se devuelve el resultado con el mensaje de error
             return FALSE;
            }
        }
    }
}
/*
 **************************************************************************************************
 Este metodo actualiza un uom
 **************************************************************************************************
*/
public function update($unitName, $unitId){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("UPDATE unit SET description = ? WHERE unitid = ?;")){
      //Se asignan parametros al '?'
      $sentence->bind_param("si", $unitName, $unitId);
      $this->db->query("START TRANSACTION");
      //Se ejecuta la sentencia preparada
      if ($sentence->execute()) {
          if($sentence->close()){
           //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
           $this->db->query("COMMIT");
           //Se devuelve status correcto
           return TRUE;
          }else{
           //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
           $this->db->query("ROLLBACK");
           //Se devuelve el resultado con el mensaje de error
           return FALSE;
          }
      }
  }
}
}
?>

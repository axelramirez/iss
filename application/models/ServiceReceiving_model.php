<?php
require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: Service Shipping Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla shipping que
 *********************************** es la que gestiona las salidas demercancia de los clientes
 **************************************************************************************************
 */
class ServiceReceiving_model{
  private $db;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }

  /*
   **************************************************************************************************
   Este metodo obtiene los extra cargos de todas las salidas
   **************************************************************************************************
   */
  public function get(){
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select * from servicereceiving;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>   TRUE,
        "query"  => $query
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene los extra cargos para una salida por su receivingid
   **************************************************************************************************
   */
  public function getByShippingId($id){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("select * from servicereceiving where receivingid = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $id);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>   TRUE,
          "query"  => $query
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }

  /*
   **************************************************************************************************
   Este metodo obtiene los extra cargos para una salida por su receiving
   **************************************************************************************************
   */
  public function getByReceivingIdAndAutoInvoiceId($id, $autoInvoiceId){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("select * from servicesReceivingView where receiving_id = ? and autoinvoice_id = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("ii", $id, $autoInvoiceId);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>                           TRUE,
          "query"  => $query->fetch_all(MYSQLI_ASSOC)
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene los extra cargos para una salida por su shippingid
   **************************************************************************************************
   */
  public function getByAutoInvoiceId($id){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("select * from servicesReceivingView where autoinvoice_id = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $id);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>                           TRUE,
          "query"  => $query->fetch_all(MYSQLI_ASSOC)
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
  **************************************************************************************************
  Este metodo inserta un nuevo extra cargo
  **************************************************************************************************
  */
  public function save($charge){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("insert into servicereceiving(serviceid, receivingid, cost, idAutoinvoice, information) values(?, ?, ?, ?, ?)")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("iidis", $charge["serviceid"], $charge["receivingid"], $charge["cost"], $charge["idAutoinvoice"], $charge["information"]);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>   TRUE,
          "id"  => $this->db->insert_id
        );
      }
      //Se cierra la sentencia
      $sentence->close();
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo inserta una multiples list de factura
   **************************************************************************************************
  */
  public function saveAll($invoiceId, $charges){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("insert into servicereceiving(serviceid, receivingid, cost, idAutoinvoice, information) values(?, ?, ?, ?, ?)")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("iidis", $serviceId, $receivingId, $cost, $invoiceId, $information);
      //Se declara un bloque de transaccion para que se asegure que "todos" los datos del arreglo sea guardado o no
      $this->db->query("START TRANSACTION");
      foreach($charges as $charge){
       //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
       $cost          =           $charge["cost"];
       $serviceId     =      $charge["serviceid"];
       $receivingId   =    $charge["receivingid"];
       $information   =    $charge["information"];
       //Se ejecuta la sentencia preparada
       $sentence->execute();
      }
      if($sentence->close()){
       //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
       $this->db->query("COMMIT");
       //Se devuelve status correcto
       return array(
         "status"  => TRUE
       );
      }else{
       //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
       $this->db->query("ROLLBACK");
       //Se devuelve el resultado con el mensaje de error
       return array(
         "status"  =>            FALSE,
         "message" => $this->db->error
       );
      }
    }
  }
  /*
   **************************************************************************************************
   Este metodo elimina un service por su id
   **************************************************************************************************
  */
  public function delete($serviceReceivingId){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("delete from servicereceiving where servicereceivingid = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $serviceReceivingId);
       //Se ejecuta la sentencia preparada
       if($sentence->execute()){
         //Se cierra la sentencia
         $sentence->close();
         if($this->db->affected_rows != 0){
           return TRUE;
         }else{
           return FALSE;
         }
       }
       $sentence->close();
    }
    return FALSE;
  }
  /*
   **************************************************************************************************
   Este metodo inserta una multiples list de factura
   **************************************************************************************************
  */
  public function deleteAll($charges){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("delete from servicereceiving where servicereceivingid = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $serviceId);
      //Se declara un bloque de transaccion para que se asegure que "todos" los datos del arreglo sea guardado o no
      $this->db->query("START TRANSACTION");
      foreach($charges as $charge){
       //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
       $serviceId     =      $charge["servicereceivingid"];
       //Se ejecuta la sentencia preparada
       $sentence->execute();
      }
      if($sentence->close()){
       //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
       $this->db->query("COMMIT");
       //Se devuelve status correcto
       return array(
         "status"  => TRUE
       );
      }else{
       //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
       $this->db->query("ROLLBACK");
       //Se devuelve el resultado con el mensaje de error
       return array(
         "status"  =>            FALSE,
         "message" => $this->db->error
       );
      }
    }
  }

  public function update($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("update servicereceiving set idAutoInvoice = NULL where idAutoInvoice = ?")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
       if( $this->db->affected_rows == 0 ){
         return  TRUE;
       }else{
         return FALSE;
       }
     }
   }
   //Se imprime el error y se retorna falso de que no se completo la operacion
   print_r($this->db->error);
   return FALSE;
  }
}
?>

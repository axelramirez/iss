<?php
 require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: Shipping Detail Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla Shipping
 *********************************** Detail que es la que gestiona los shipping detail
 **************************************************************************************************
*/
class ShippingDetail_model{
  private $db;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
  */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todos los shipping detail
   **************************************************************************************************
  */
  public function get(){
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select * from shippingdetail;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>                            TRUE,
        "query"  => $query->fetch_all(MYSQLI_ASSOC)
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene un shipping detail por su id
   **************************************************************************************************
  */
  public function getById($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from shippingdetail where shippingdetailid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_assoc()
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todos los shipping detail de un shipping
   **************************************************************************************************
  */
  public function getByShippingId($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from shippingdetail where shippingid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                            TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC)
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene la suma de la cantidad total de mercancia por su receivingdetailid
   **************************************************************************************************
  */
  public function getTotalByReceivingDetailId($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select sum(quantity) as total from shippingdetail where receivingdetailid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                            TRUE,
         "query"  => $query->fetch_assoc()
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene la suma de la cantidad total de mercancia por su receivingdlinesid
   **************************************************************************************************
  */
  public function getTotalByReceivingLineId($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select sum(quantity) as total from shippingdetail where receivinglinesid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                            TRUE,
         "query"  => $query->fetch_assoc()
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
   **************************************************************************************************
   Este metodo actualiza un shipping detail
   **************************************************************************************************
   */
   public function update($shippingDetail){
     $weight           =           $shippingDetail["weight"];
     $quantity         =         $shippingDetail["quantity"];
     $shippingdetailid = $shippingDetail["shippingdetailid"];
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
     if($sentence = $this->db->prepare("update shippingdetail set quantity = ?, weight = ? where shippingdetailid = ?")){
       //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
       $sentence->bind_param("idi", $quantity, $weight, $shippingdetailid);
       //Se ejecuta la sentencia preparada
       if($sentence->execute()){
         //Se cierra la sentencia
         $sentence->close();
         //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
         if( $this->db->affected_rows == 0 ){
           return  TRUE;
         }else{
           return FALSE;
         }
       }
     }
     //Se imprime el error y se retorna falso de que no se completo la operacion
     print_r($this->db->error);
     return FALSE;
   }
   /*
    **************************************************************************************************
    Este metodo actualiza multiples shipping details (quantity y weight)
    **************************************************************************************************
   */
   public function updateAll($shippingsDetails){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
     if($sentence = $this->db->prepare("update shippingdetail set quantity = ?, weight = ? where shippingdetailid = ?")){
       //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
       $sentence->bind_param("iii", $quantity, $weight, $shippingdetailid);
       //Se declara un bloque de transaccion para que se asegure que "todos" los datos del arreglo sea guardado o no
       $this->db->query("START TRANSACTION");
       foreach($shippingsDetails as $shippingDetail){
        //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
        $weight           =           $shippingDetail["weight"];
        $quantity         =         $shippingDetail["quantity"];
        $shippingdetailid = $shippingDetail["shippingdetailid"];
        //Se ejecuta la sentencia preparada
        $sentence->execute();
       }
       if($sentence->close()){
        //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
        $this->db->query("COMMIT");
        //Se devuelve status correcto
        return array(
          "status"  => TRUE
        );
       }else{
        //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
        $this->db->query("ROLLBACK");
        //Se devuelve el resultado con el mensaje de error
        return array(
          "status"  =>            FALSE,
          "message" => $this->db->error
        );
       }
     }
   }
}
?>

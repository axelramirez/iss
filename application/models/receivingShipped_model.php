<?php
 require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: Receiving Shipped Class Model
 ***********************Description: Realiza las consultas correspondientes a la vista Receiving
 *********************************** Shipped que es la que gestiona cuantos isShipped son 'T' o 'F'
 **************************************************************************************************
*/
class ReceivingShipped_model{
  private $db;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
  */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }
  /*
   **************************************************************************************************
   Este metodo obtiene el estado en current quantity de todos los receiving, receivingdetail y
   receivinglines
   **************************************************************************************************
  */
  public function get(){
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select * from receivingShippedView;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>                            TRUE,
        "query"  => $query->fetch_all(MYSQLI_ASSOC)
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene el estado en current quantity de un receiving, receivingdetail y
   receivinglines por su receivingdetailid
   **************************************************************************************************
  */
  public function getByReceivingDetailId($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from receivingShippedView where receivingdetailid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_assoc()
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
}
?>

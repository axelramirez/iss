<?php
require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: Unit Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla po
 **************************************************************************************************
 */
class PurchaseOrder_model{
  private $db;
  private $model;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
    $this->model = array(
      "poid" => array("type" => "Integer"),
      "po" => array("type" => "String"),
      "poDate" => array("type" => "String"),
      "receivingid" => array("type" => "Integer"),
      "receivingdetailid" => array("type" => "Integer")
    );
  }

  public function getById($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from po where poid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_assoc()
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }

  /*
   **************************************************************************************************
   Este metodo elimina un un purchase order de receiving details
   **************************************************************************************************
  */
  public function delete($arrays, $receivingId){
    $status = false;
    foreach($arrays as $key => $array){
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("delete from po where poid = ? and receivingid = ?")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        $sentence->bind_param("ii", $array['poid'], $receivingId);
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
          $status = ( $sentence->affected_rows > 0 ) ? TRUE : FALSE;
          $sentence->close();
        }
      }
    }
    return $status;
  }
  /*
   **************************************************************************************************
   Este metodo inserta un nuevo purchase Order
   **************************************************************************************************
  */
  public function insert($npos, $receivingId){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("insert into po(po, poDate, receivingid, receivingdetailid) values(?, ?, ? , ?)")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("ssii", $poname, $podate, $receivingId, $receivingdetailid);

      $this->db->query("START TRANSACTION");
      foreach($npos as $po){
       //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
       $poname = $po["po"];
       $podate = $po["podate"];
       $receivingdetailid = $po["receivingdetailid"];
       //Se ejecuta la sentencia preparada
       $sentence->execute();
      }
      if($sentence->close()){
       //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
       $this->db->query("COMMIT");
       //Se devuelve status correcto
       return TRUE;
      }else{
       //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
       $this->db->query("ROLLBACK");
       //Se devuelve el resultado con el mensaje de error
       return FALSE;
      }
  }
}
  public function update($pso, $receivingid){
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("update po set po = ?, poDate = ?, receivingdetailid = ? where poid = ? and receivingid = ?;")){
        //Se asignan parametros al '?'
        $sentence->bind_param("ssiii", $pos, $podate, $receivingdetailid, $poid, $receivingid);
        //Se declara un bloque de transaccion para que se asegure que "todos" los datos del arreglo sea guardado o no
        $this->db->query("START TRANSACTION");
        foreach($pso as $po){
         //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
         $poid = $po["poid"];
         $pos = $po["po"];
         $podate = $po["podate"];
         $receivingdetailid = $po["receivingdetailid"];
         //Se ejecuta la sentencia preparada
         $sentence->execute();
        }
        if($sentence->close()){
         //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
         $this->db->query("COMMIT");
         //Se devuelve status correcto
         return TRUE;
        }else{
         //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
         $this->db->query("ROLLBACK");
         //Se devuelve el resultado con el mensaje de error
         return FALSE;
        }
      }
  }
  public function updateMain($fields, $arrays, $receivingDetails){
    $status = FALSE;
    foreach($arrays as $key => $array){
      $po = $this->getById($array["poid"]);
      $po = $po["query"];
      if((Integer)$array["receivingdetailno"] == 0){
        continue;
      }else{
        $array["receivingdetailid"] = $receivingDetails[$array["receivingdetailno"] - 1]["receivingdetailid"];
      }
      list($haveRegister, $params_type, $query_string, $params) = generateUpdateQuery($fields, $array, $po, $this->model);
      if($haveRegister){
        $params_type .= "i";
        array_unshift($params, $params_type);
        array_push($params, $array["poid"]);
        //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
        if($sentence = $this->db->prepare("update po set $query_string where poid = ?")){
          //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
          call_user_func_array(array($sentence, 'bind_param'), referencesValue($params));
          //Se ejecuta la sentencia preparada
          if($sentence->execute()){
            //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
            $status = ( $sentence->affected_rows > 0 ) ? TRUE : FALSE;
            $sentence->close();
          }
        }
      }
    }
    return $status;
  }
  public function getByReceivingId($id){
      //Se ejecuta la consulta y se obtiene el resultado
      if($sentence = $this->db->prepare("SELECT po.poid, po.po, date_format(po.poDate, '%Y-%m-%d') AS poDate, po.receivingid, po.receivingdetailid, ifnull(rd.rowno, 0) as rowno FROM po LEFT JOIN (SELECT @numrow:=@numrow+1 AS rowno, rd2.* FROM receivingdetail rd2, (SELECT @numrow:=0) r WHERE rd2.receivingid = ? ORDER BY rd2.receivingdetailid) rd ON po.receivingdetailid = rd.receivingdetailid WHERE po.receivingid = ? ORDER BY po.poid;")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        $sentence->bind_param("ii", $id, $id);
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
        $query = $sentence->get_result();
          //Se cierra la sentencia
          $sentence->close();
          //Se devuelve el resultado con la sentencia
          return array(
            "status" =>                     TRUE,
            "query"  => $query->fetch_all(MYSQLI_ASSOC)
          );
        }
      }
      //Se devuelve el resultado con el mensaje de error
      return array(
        "status"  =>            FALSE,
        "message" => $this->db->error
      );
  }

  /*
   **************************************************************************************************
   Este metodo inserta un nuevo Purchase Order
   **************************************************************************************************
  */
  public function save($arrays, $receivingId, $receivingDetails){
    $status = FALSE;
    foreach($arrays as $key => $array){
      $receivingDetailId = $receivingDetails[$array["receivingdetailno"] - 1]["receivingdetailid"];
      $this->db->query("START TRANSACTION");
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("INSERT INTO po(po, poDate, receivingid, receivingdetailid) values(?, ?, ?, ?);")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        $sentence->bind_param("ssii", $array["po"], $array["poDate"], $receivingId, $receivingDetailId);
        $sentence->execute();
        if($sentence->close()){
          $this->db->query("COMMIT");
          $status = TRUE;
        }else{
          $this->db->query("ROLLBACK");
          $status = FALSE;
          break;
        }
      }
    }
    return $status;
  }
}
?>

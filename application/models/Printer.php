<?php

require_once("adapters/MySQLConnection.php");


class Printer{
    private $db;
    private $model;
    
    

    public function __construct(){
        $this->db = MySQLConnection::getInstance()->getConnection();
    }

    private function cmpStr($printername,$printerip){
       return strcmp($printername,"") == 0 &&  strcmp($printerip,"") == 0; 
    } 

    public function get($printername,$printerip,$numberPage){

        //Se define cuantos elementos se van a consultar
       $elementsByPage = 5;

       $isData =  $this->cmpStr($printername,$printerip);
     
       //Se define desde donde se va a consultar
       $offset = ($numberPage - 1) * $elementsByPage;
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'

      $queryCount = $isData == 1 ? "SELECT COUNT(*) AS entries FROM printers" : "SELECT COUNT(*) AS entries FROM printers WHERE printername=? OR ip=?";
      

      if($sentence1 = $this->db->prepare("$queryCount;")){
       if($isData != 1){
            $sentence1->bind_param("ss",$printername,$printerip);
            $this->db->query("START TRANSACTION");
        }
         if($sentence1->execute()){
              $sentence1->bind_result($numberEntries);
              $sentence1->fetch();
              $sentence1->close();
          }

      }
    
      //Se calcula el numero de paginas que tendra la consulta
      $pages         =                                           $numberEntries / $elementsByPage;
      $numberPages   =                (Integer)( $pages > (Integer)$pages ? $pages + 1 : $pages );
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
       
     $command = $isData == 1 ? "SELECT * FROM printers ORDER BY printerid DESC LIMIT $offset,$elementsByPage;;" : "SELECT * FROM printers WHERE printername=? OR ip=? LIMIT $offset,$elementsByPage;;";
     if($sentence = $this->db->prepare($command)){
        //si hay datos que filtrar pasa la parametrizacion
        if($isData != 1){
          $sentence->bind_param("ss",$printername,$printerip);
          $this->db->query("START TRANSACTION");
        }
       //Se ejecuta la sentencia preparada
       if($sentence->execute()){
         //Se obtiene el resultado de la ejecucion de la sentencia
         $query = $sentence->get_result();
         //Se cierra la sentencia
         $sentence->close();
         //Se devuelve el resultado con la sentencia
         return array(
           "status" =>                  TRUE,
           "query"  => $query->fetch_all(MYSQLI_ASSOC),
           "numberEntries" => $numberEntries,
           "numberPages" => $numberPages
         );
       }
     }
     //Se devuelve el resultado con el mensaje de error
     return array(
       "status"  =>            FALSE,
       "message" => $this->db->error
     );
    }


    public function insert($printername,$printerip,$description,$default){
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
       if($sentence = $this->db->prepare("INSERT INTO printers (printername,ip,des,def) VALUES (?,?,?,?);")){
         //Se asignan parametros al '?'
         $sentence->bind_param("sssi", $printername,$printerip,$description,$default);
         $this->db->query("START TRANSACTION");
         //Se ejecuta la sentencia preparada
         if ($sentence->execute()) {
             if($sentence->close()){
              //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
              $this->db->query("COMMIT");
              //Se devuelve status correcto
              return TRUE;
             }else{
              //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
              $this->db->query("ROLLBACK");
              //Se devuelve el resultado con el mensaje de error
              return FALSE;
             }
         }
     }
 }

    public function update($printername,$printerip,$description,$default, $printerid){
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
       if($sentence = $this->db->prepare("UPDATE printers SET printername = ?, ip = ?,des = ?, def = ? WHERE printerid = ?;")){
         //Se asignan parametros al '?'
         $sentence->bind_param("sssii", $printername,$printerip,$description,$default, $printerid);
         $this->db->query("START TRANSACTION");
         //Se ejecuta la sentencia preparada
         if ($sentence->execute()) {
             if($sentence->close()){
              //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
              $this->db->query("COMMIT");
              //Se devuelve status correcto
              return TRUE;
             }else{
              //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
              $this->db->query("ROLLBACK");
              //Se devuelve el resultado con el mensaje de error
              return FALSE;
             }
         }
     }
   }

  
   public function validateIP($printerip){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
     $ipResult = "";
     if($sentence = $this->db->prepare("SELECT ip printers FROM user WHERE ip = ?;")){
       //Se asignan parametros al '?'
       $sentence->bind_param("s", $printerip);
       $this->db->query("START TRANSACTION");
       //Se ejecuta la sentencia preparada
       if ($sentence->execute()) {
           $sentence->bind_result($ipResult);
           $sentence->fetch();
           if($sentence->close() && strcmp($ipResult,$printerip) == 0){
            //Se devuelve status correcto

            return array(
              "status" =>                  TRUE,
            );
           }else{
            //Se devuelve el resultado con el mensaje de error
            return array(
              "status"  =>            FALSE,
              "message" => $this->db->error
            );
           }
       }
   }
 }

   public function delete($printerid){
        $status = false;
        //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
        if($sentence = $this->db->prepare("DELETE FROM printers WHERE printerid = ?;")){
          //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
          $sentence->bind_param("i", $printerid);
          //Se ejecuta la sentencia preparada
          if($sentence->execute()){
            $status = ( $sentence->affected_rows > 0 ) ? TRUE : FALSE;
            $sentence->close();
          }
        }
        return $status;
    }

}

?>
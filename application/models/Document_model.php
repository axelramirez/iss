<?php
require_once("adapters/MySQLConnection.php");

require_once("helpers/referencesValue_helper.php");
/*
 **************************************************************************************************
 ***********************Name: Unit Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla po
 **************************************************************************************************
 */
class Document_model{
  private $db;
  private $model;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }
  public function getById($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from document where documentid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_assoc()
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }

  public function getByIn($ids, $type, $type_key, $id){
    $params = $ids;
    $params_type = str_repeat("i", count($ids));
    $params_type .= "ssi";
    $documents = "?" . str_repeat(", ?", count($ids) - 1);
    array_unshift($params, $params_type);
    array_push($params, $type);
    array_push($params, $type_key);
    array_push($params, $id);
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from document where documentid in ($documents) and type = ? and type_key = ? and type_recid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     call_user_func_array(array($sentence, 'bind_param'), referencesValue($params));
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC)
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }

  public function getByReceivingId($type, $type_key, $id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from document where type = ? and type_key = ? and type_recid = ? order by documentid;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("ssi", $type, $type_key, $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC)
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
   **************************************************************************************************
   Este metodo elimina un un purchase order de receiving details
   **************************************************************************************************
  */
  public function delete($arrays, $type, $type_key, $receivingId){
    $status = false;
    foreach($arrays as $key => $array){
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("delete from document where documentid = ? and type = ? and type_key = ? and type_recid = ?")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        $sentence->bind_param("issi", $array['documentid'], $type, $type_key, $receivingId);
        //Se ejecuta la sentencia preparada
        if($sentence->execute()){
          $status = ( $sentence->affected_rows > 0 ) ? TRUE : FALSE;
          $sentence->close();
        }
      }
    }
    return $status;
  }
  /*
   **************************************************************************************************
   Este metodo inserta un nuevo purchase Order
   **************************************************************************************************
  */

  public function updateMain($fields, $arrays){
    $status = FALSE;
    foreach($arrays as $key => $array){
      $tande = $this->getById($array["tandeid"]);
      $tande = $tande["query"];
      list($haveRegister, $params_type, $query_string, $params) = generateUpdateQuery($fields, $array, $tande, $this->model);
      if($haveRegister){
        $params_type .= "i";
        array_unshift($params, $params_type);
        array_push($params, $array["tandeid"]);
        //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
        if($sentence = $this->db->prepare("update tande set $query_string where tandeid = ?")){
          //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
          call_user_func_array(array($sentence, 'bind_param'), referencesValue($params));
          //Se ejecuta la sentencia preparada
          if($sentence->execute()){
            //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
            $status = ( $sentence->affected_rows > 0 ) ? TRUE : FALSE;
            $sentence->close();
          }
        }
      }
    }
  }

  /*
   **************************************************************************************************
   Este metodo inserta un nuevo T&E
   **************************************************************************************************
  */
  public function save($arrays, $receivingId){
    $status = FALSE;
    foreach($arrays as $key => $array){
      $this->db->query("START TRANSACTION");
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("INSERT INTO document(type, type_recid, type_key, description, name, file_path, original_name, mime_type) VALUES(?, ?, ?, ?, ?, ?, ?, ?)")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        $sentence->bind_param("sissssss", $array["type"], $receivingId, $array["type_key"], $array["description"], $array["name"], $array["file_path"], $array["original_name"], $array["mime_type"]);
        $sentence->execute();
        if($sentence->close()){
          $this->db->query("COMMIT");
          $status = TRUE;
        }else{
          $this->db->query("ROLLBACK");
          $status = FALSE;
          break;
        }
      }
    }
    return $status;
  }
}
?>

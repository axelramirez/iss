<?php
require_once("adapters/MySQLConnection.php");
include('mailnotification.php');
/*
 **************************************************************************************************
 ***********************Name: Unit Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla docks
 **************************************************************************************************
 */
class Inventory_model{
  private $db;
  private $model;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }

  public function get(){
    $sqlColors = "SELECT * FROM color;";
    if($query = $this->db->query("$sqlColors;")){
      //Se obtiene el numero de resultados que arrojo la consulta
      $queryColors = $query->fetch_all(MYSQLI_ASSOC);
    }
    $sqlTornillos = "SELECT * FROM tornillos ORDER BY name;";
    if($query = $this->db->query("$sqlTornillos;")){
      //Se obtiene el numero de resultados que arrojo la consulta
      $sqlTornillos = $query->fetch_all(MYSQLI_ASSOC);
    }
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("SELECT * FROM producto ORDER BY name;")){
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC),
         "colors" => $queryColors,
         "tornillos" => $sqlTornillos

       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  public function getset(){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("SELECT * FROM setproduct ORDER BY name;")){
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC)

       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  public function search($filter){
    $filterLike = '%' . $filter . '%';
    $sqlSUM = "SELECT SUM(quantity) AS total FROM producto;";
    if($query = $this->db->query("$sqlSUM;")){
      //Se obtiene el numero de resultados que arrojo la consulta
      $query         =            $query->fetch_assoc();
      $totalProducts = (Integer)$query["total"];
    }

   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("SELECT * FROM producto WHERE name LIKE '$filterLike' OR description LIKE '$filterLike';")){
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC),
         "totalProducts" => $totalProducts
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  public function update($data, $productId){
      $productName = $data["productName"];
      $productImage = $data["productImage"];
      $productDescription = $data["productDescription"];
      $productQuantity = $data["productQuantity"];
      $productColor = $data["productColor"];
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("UPDATE producto SET name = ?, description = ?, image = ?, quantity = ?, idcolor = ? WHERE id = ?;")){
        //Se asignan parametros al '?'
        $sentence->bind_param("sssiii", $productName, $productDescription, $productImage, $productQuantity, $productColor, $productId);
        $this->db->query("START TRANSACTION");
        //Se ejecuta la sentencia preparada
        if ($sentence->execute()) {
            if($sentence->close()){
             //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
             $this->db->query("COMMIT");
             //Se devuelve status correcto
             return TRUE;
            }else{
             //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
             $this->db->query("ROLLBACK");
             //Se devuelve el resultado con el mensaje de error
             return FALSE;
            }
        }
    }
 }
 public function insert($data){
     $productName = $data["productName"];
     $productDescription = $data["productDescription"];
     $productImage = $data["productImage"];
     $productQuantity = $data["productQuantity"];
     $productColor = $data["productColor"];
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
     if($sentence = $this->db->prepare("INSERT INTO producto(name, description, image, quantity, idcolor) VALUES(?,?,?,?,?);")){
       //Se asignan parametros al '?'
       $sentence->bind_param("sssii", $productName, $productDescription, $productImage, $productQuantity, $productColor);
       $this->db->query("START TRANSACTION");
       //Se ejecuta la sentencia preparada
       if ($sentence->execute()) {
           if($sentence->close()){
            //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
            $this->db->query("COMMIT");
            //Se devuelve status correcto
            return TRUE;
           }else{
            //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
            $this->db->query("ROLLBACK");
            //Se devuelve el resultado con el mensaje de error
            return FALSE;
           }
       }
   }
}
public function delete($idProduct){
    $status = false;
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("DELETE FROM producto WHERE id = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $idProduct);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        $status = ( $sentence->affected_rows > 0 ) ? TRUE : FALSE;
        $sentence->close();
      }
    }
  return $status;
}
public function receipt($data, $id){
    $qty = 0;
    $productQuantity = $data["productQuantity"];
    $sqlQty = "SELECT quantity FROM producto WHERE id = $id;";
    if($query = $this->db->query("$sqlQty;")){
      //Se obtiene el numero de resultados que arrojo la consulta
      $query         =            $query->fetch_assoc();
      $itemsQty = (Integer)$query["quantity"];
    }
    $qty = ($productQuantity + $itemsQty);
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("UPDATE producto SET quantity = ? WHERE id = ?;")){
      //Se asignan parametros al '?'
      $sentence->bind_param("ii", $qty, $id);
      $this->db->query("START TRANSACTION");
      //Se ejecuta la sentencia preparada
      if ($sentence->execute()) {
          if($sentence->close()){
           //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
           $this->db->query("COMMIT");
           //Se devuelve status correcto
           return TRUE;
          }else{
           //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
           $this->db->query("ROLLBACK");
           //Se devuelve el resultado con el mensaje de error
           return FALSE;
          }
      }
  }
}
public function shipping($data, $id){
    $qty = 0;
    $productQuantity = $data["productQuantity"];
    $sqlQty = "SELECT quantity FROM producto WHERE id = $id;";
    if($query = $this->db->query("$sqlQty;")){
      //Se obtiene el numero de resultados que arrojo la consulta
      $query         =            $query->fetch_assoc();
      $itemsQty = (Integer)$query["quantity"];
    }
    $qty = ($itemsQty - $productQuantity);
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("UPDATE producto SET quantity = ? WHERE id = ?;")){
      //Se asignan parametros al '?'
      $sentence->bind_param("ii", $qty, $id);
      $this->db->query("START TRANSACTION");
      //Se ejecuta la sentencia preparada
      if ($sentence->execute()) {
          if($sentence->close()){
           //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
           $this->db->query("COMMIT");
           //Se devuelve status correcto
           return TRUE;
          }else{
           //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
           $this->db->query("ROLLBACK");
           //Se devuelve el resultado con el mensaje de error
           return FALSE;
          }
      }
  }
}
public function getcolors(){
 //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
 if($sentence = $this->db->prepare("SELECT * FROM color ORDER BY name;")){
   //Se ejecuta la sentencia preparada
   if($sentence->execute()){
     //Se obtiene el resultado de la ejecucion de la sentencia
     $query = $sentence->get_result();
     //Se cierra la sentencia
     $sentence->close();
     //Se devuelve el resultado con la sentencia
     return array(
       "status" =>                  TRUE,
       "query"  => $query->fetch_all(MYSQLI_ASSOC)

     );
   }
 }
 //Se devuelve el resultado con el mensaje de error
 return array(
   "status"  =>            FALSE,
   "message" => $this->db->error
 );
}
public function insertcolor($data){
    $colorName = $data["colorName"];
    $colorDescription = $data["colorDescription"];
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("INSERT INTO color(name, description) VALUES(?,?);")){
      //Se asignan parametros al '?'
      $sentence->bind_param("ss", $colorName, $colorDescription);
      $this->db->query("START TRANSACTION");
      //Se ejecuta la sentencia preparada
      if ($sentence->execute()) {
          if($sentence->close()){
           //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
           $this->db->query("COMMIT");
           //Se devuelve status correcto
           return TRUE;
          }else{
           //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
           $this->db->query("ROLLBACK");
           //Se devuelve el resultado con el mensaje de error
           return FALSE;
          }
      }
  }
}
public function updatecolor($data, $colorId){
    $colorName = $data["colorName"];
    $colorDescription = $data["colorDescription"];
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("UPDATE color SET name = ?, description = ? WHERE id = ?;")){
      //Se asignan parametros al '?'
      $sentence->bind_param("ssi", $colorName, $colorDescription, $colorId);
      $this->db->query("START TRANSACTION");
      //Se ejecuta la sentencia preparada
      if ($sentence->execute()) {
          if($sentence->close()){
           //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
           $this->db->query("COMMIT");
           //Se devuelve status correcto
           return TRUE;
          }else{
           //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
           $this->db->query("ROLLBACK");
           //Se devuelve el resultado con el mensaje de error
           return FALSE;
          }
      }
  }
}
public function deletecolor($idColor){
    $status = false;
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("DELETE FROM color WHERE id = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $idColor);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        $status = ( $sentence->affected_rows > 0 ) ? TRUE : FALSE;
        $sentence->close();
      }
    }
  return $status;
}
public function insertset($data){
    $setName = $data["setName"];
    $setDescription = $data["setDescription"];
    $setImage = $data["setImage"];
    $setItems = $data["setItems"];
    $setquantityBolt = $data["quantityBolt"];
    $setboltId = $data["boltId"];
    $setqtyNut = $data["qtyNut"];
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("INSERT INTO setproduct(name, image, description, idproducts, idbolt, bolt, nut) VALUES(?,?,?,?,?,?,?);")){
      //Se asignan parametros al '?'
      $sentence->bind_param("ssssiii", $setName, $setImage, $setDescription, $setItems, $setboltId, $setquantityBolt, $setqtyNut);
      $this->db->query("START TRANSACTION");
      //Se ejecuta la sentencia preparada
      if ($sentence->execute()) {
          if($sentence->close()){
           //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
           $this->db->query("COMMIT");
           //Se devuelve status correcto
           return TRUE;
          }else{
           //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
           $this->db->query("ROLLBACK");
           //Se devuelve el resultado con el mensaje de error
           return FALSE;
          }
      }
  }
}
}
?>

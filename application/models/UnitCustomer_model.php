<?php
require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: UnitCustomer Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla unitcustomer
 *********************************** que es la que gestiona el costo de handling y CPD por unidad
 *********************************** de mercancia de cada cliente
 **************************************************************************************************
 */
class UnitCustomer_model{
  private $db;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }

  /*
   **************************************************************************************************
   Este metodo obtiene el costo CPD y Handling de todos los clientes de cada unidad de mercancia
   **************************************************************************************************
   */
  public function get(){
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select * from unitcustomer;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>   TRUE,
        "query"  => $query
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene el costo CPD y Handling de un cliente de cada unidad de mercancia
   **************************************************************************************************
   */
  public function getById($id){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("select * from unitcustomer where customerid = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $id);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>   TRUE,
          "query"  => $query
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
}
?>

<?php
 require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: AutoInvoice Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla AutoInvoice
 *********************************** que es la que gestiona las facturas de los clientes
 **************************************************************************************************
*/
class AutoInvoice_model{
  private $db;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
  */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todos las autoinvoice de los clientes
   **************************************************************************************************
  */
  public function get(){
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select * from autoinvoice;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>   TRUE,
        "query"  => $query
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene un autoinvoice por su id
   **************************************************************************************************
  */
  public function getById($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from autoinvoice where autoid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>   TRUE,
         "query"  => $query
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
   **************************************************************************************************
   Este metodo actualiza una autoinvoice existente
   **************************************************************************************************
  */
  public function update($autoInvoice){
   $paid_amount  = $autoInvoice["paid_amount"];
   $autoid       =      $autoInvoice["autoid"];
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("update autoinvoice set paid_amount = paid_amount + ? where autoid = ?")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("di", $paid_amount, $autoid);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
       if( $this->db->affected_rows == 0 ){
         return  TRUE;
       }else{
         return FALSE;
       }
     }
   }
   //Se imprime el error y se retorna falso de que no se completo la operacion
   print_r($this->db->error);
   return FALSE;
  }
  /*
   **************************************************************************************************
   Este metodo actualiza el estado pagado de un autoinvoice existente
   **************************************************************************************************
  */
  public function updatePaid($autoInvoice){
   $ispaid = $autoInvoice["ispaid"];
   $autoid = $autoInvoice["autoid"];
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("update autoinvoice set isPaid = ? where autoid = ?")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("ii", $ispaid, $autoid);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
       if( $this->db->affected_rows == 0 ){
         return  TRUE;
       }else{
         return FALSE;
       }
     }
   }
   //Se imprime el error y se retorna falso de que no se completo la operacion
   print_r($this->db->error);
   return FALSE;
  }

  /*
   **************************************************************************************************
   Este metodo actualiza una autoinvoice existente
   **************************************************************************************************
  */
  public function updateAll($autoInvoice){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("update autoinvoice set total = ?, date = ?, date_start = ? where autoid = ?")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("dssi", $autoInvoice["total"], $autoInvoice["date"], $autoInvoice["startDate"],$autoInvoice["autoinvoice_id"]);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
       if( $this->db->affected_rows == 0 ){
         return  TRUE;
       }else{
         return FALSE;
       }
     }
   }
   //Se imprime el error y se retorna falso de que no se completo la operacion
   print_r($this->db->error);
   return FALSE;
  }

  public function updateInvoice($autoInvoice){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("update autoinvoice set total = ? where autoid = ?")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("di", $autoInvoice["total"], $autoInvoice["autoinvoice_id"]);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
       if( $this->db->affected_rows == 0 ){
         return  TRUE;
       }else{
         return FALSE;
       }
     }
   }
   //Se imprime el error y se retorna falso de que no se completo la operacion
   print_r($this->db->error);
   return FALSE;
  }

  public function cancelAutoInvoice($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("update autoinvoice set isCancelled = 1 where autoid = ?")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
       if( $this->db->affected_rows == 0 ){
         return  TRUE;
       }else{
         return FALSE;
       }
     }
   }
   //Se imprime el error y se retorna falso de que no se completo la operacion
   print_r($this->db->error);
   return FALSE;
  }
}
?>

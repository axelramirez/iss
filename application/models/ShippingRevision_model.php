<?php
require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: Service Shipping Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla shipping que
 *********************************** es la que gestiona las salidas demercancia de los clientes
 **************************************************************************************************
 */
class ShippingRevision_model{
  private $db;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }

  /*
   **************************************************************************************************
   Este metodo obtiene los extra cargos de todas las salidas
   **************************************************************************************************
   */
  public function get(){
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select * from shippingrevisions;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>   TRUE,
        "query"  => $query
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
  **************************************************************************************************
  Este metodo actualiza una list de factura
  **************************************************************************************************
  */
  public function update($serviceReceivingId){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("update shippingrevisions set servicereceivingid = null where servicereceivingid = ?")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $serviceReceivingId);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>   TRUE,
          "invoicelistid"  => $this->db->insert_id
        );
      }
      //Se cierra la sentencia
      $sentence->close();
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
}
?>

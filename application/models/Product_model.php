<?php
require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: Unit Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla po
 **************************************************************************************************
 */
class Product_model{
  private $db;
  private $model;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }
  public function getById($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from product where productid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_assoc()
       );
     }
   }
 }
   public function get(){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("select productid, productno, name, description from product;")){
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>                  TRUE,
          "query"  => $query->fetch_all(MYSQLI_ASSOC)
        );
      }
    }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  public function getByCustomerId($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from product where customerid = ? order by customerid;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC)
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todas las descripciones de proucts por limit
   **************************************************************************************************
   */
   public function getbypagination($numberPage){
       //Se define cuantos elementos se van a consultar
      $elementsByPage = 5;
      //Se define desde donde se va a consultar
      $offset = ($numberPage - 1) * $elementsByPage;
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
     $queryCount = "SELECT COUNT(*) AS entries FROM product";
     if($query = $this->db->query("$queryCount;")){
       //Se obtiene el numero de resultados que arrojo la consulta
       $query         =            $query->fetch_assoc();
       $numberEntries = (Integer)$query["entries"];
     }
     //Se calcula el numero de paginas que tendra la consulta
     $pages         =                                           $numberEntries / $elementsByPage;
     $numberPages   =                (Integer)( $pages > (Integer)$pages ? $pages + 1 : $pages );
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("SELECT product.*, customer.name AS customername FROM product LEFT JOIN customer on customer.customerid = product.customerid ORDER BY product.productno LIMIT $offset,$elementsByPage;")){
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>                  TRUE,
          "query"  => $query->fetch_all(MYSQLI_ASSOC),
          "numberEntries" => $numberEntries,
          "numberPages" => $numberPages
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
   }
   /*
    **************************************************************************************************
    Este metodo inserta un nuevo product con todos sus campos
    **************************************************************************************************
   */
   public function insertProduct($data){
       $productno = $data["productNo"];
       $name = $data["name"];
       $description = $data["description"];
       $description2 = $data["description2"];
       $customerid = $data["customerId"];
       $supplier = $data["supplier"];
       $date = $data["date"];

      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
       if($sentence = $this->db->prepare("INSERT INTO product (productno, name, description,
         description2, customerid, supplier, `date`) VALUES (?, ?, ?, ?, ?, ?, ?);")){
         //Se asignan parametros al '?'
         $sentence->bind_param("ssssiss", $productno, $name, $description, $description2, $customerid, $supplier, $date);
         $this->db->query("START TRANSACTION");
         //Se ejecuta la sentencia preparada
         if ($sentence->execute()) {
             if($sentence->close()){
              //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
              $this->db->query("COMMIT");
              //Se devuelve status correcto
              return TRUE;
             }else{
              //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
              $this->db->query("ROLLBACK");
              //Se devuelve el resultado con el mensaje de error
              return FALSE;
             }
         }
     }
     //Se imprime el error y se retorna falso de que no se completo la operacion
     print_r($this->db->error);
 }
  /*
   **************************************************************************************************
   Este metodo inserta un nuevo Product
   **************************************************************************************************
  */
  public function save($arrays){
    $ids = array();
    $result = array();
    $status = TRUE;
    foreach($arrays as $key => $array){
      $this->db->query("START TRANSACTION");
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("INSERT INTO product(productno, name, description, description2, customerid, supplier, date) VALUES(?, ?, ?, ?, ?, ?, ?)")){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        $sentence->bind_param("ssssiss", $array["productno"], $array["name"], $array["description"], $array["serial"], $array["customerid"], $array["supplier"], $array["date"]);
        $sentence->execute();
        if($sentence->close()){
          array_push($ids, $this->db->insert_id);
        }else{
          $this->db->query("ROLLBACK");
          $status = FALSE;
          break;
        }
      }
    }
    if($status){
      $this->db->query("COMMIT");
      $result = array(
        'status' => TRUE,
        'last_id' => $ids
      );
    }else{
      $result = array(
        'status' => FALSE
      );
    }
    return $result;
  }

  public function insert($array){
    $result = array();
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("INSERT INTO product(productno, name, description, description2, customerid, supplier, date) VALUES(?, ?, ?, ?, ?, ?, ?)")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("ssssiss", $array["productno"], $array["name"], $array["description"], $array["serial"], $array["customerid"], $array["supplier"], $array["date"]);
      $sentence->execute();
      if($sentence->close()){
        $result = array(
          'status' => TRUE,
          'last_id' => $this->db->insert_id
        );
      }else{
        $result = array(
          'status' => FALSE
        );
        break;
      }
    }
    return $result;
  }
  /*
   **************************************************************************************************
   Este metodo inserta un nuevo product con todos sus campos
   **************************************************************************************************
  */
  public function updateProduct($data, $partId){
      // print_r($data);
      $productno = $data["productNo"];
      $name = $data["name"];
      $description = $data["description"];
      $description2 = $data["description2"];
      $customerid = $data["customerId"];
      $supplier = $data["supplier"];
      $date = $data["date"];

     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("UPDATE product SET productno = ?, name = ?, description = ?,
        description2 = ?, customerid = ?, supplier = ?, date = ? WHERE productid = ?;")){
        //Se asignan parametros al '?'
        $sentence->bind_param("ssssissi", $productno, $name, $description, $description2, $customerid, $supplier, $date, $partId);
        $this->db->query("START TRANSACTION");
        //Se ejecuta la sentencia preparada
        if ($sentence->execute()) {
            if($sentence->close()){
             //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
             $this->db->query("COMMIT");
             //Se devuelve status correcto
             return TRUE;
            }else{
             //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
             $this->db->query("ROLLBACK");
             //Se devuelve el resultado con el mensaje de error
             return FALSE;
            }
        }
    }
    //Se imprime el error y se retorna falso de que no se completo la operacion
    print_r($this->db->error);
 }
 /*
  **************************************************************************************************
  Este metodo obtiene los resultado de la busqueda
  **************************************************************************************************
  */
  public function searchbypagination($numberPage, $filter){
      //Se define cuantos elementos se van a consultar
     $elementsByPage = 5;
     $filterLike = '%' . $filter . '%';
     // print($filterLike);
     //Se define desde donde se va a consultar
     $offset = ($numberPage - 1) * $elementsByPage;
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    $queryCount = "SELECT COUNT(*) AS entries FROM product LEFT JOIN customer on customer.customerid = product.customerid
      WHERE product.productno LIKE '$filterLike' OR product.name LIKE '$filterLike' OR product.description LIKE '$filterLike'
      OR product.description2 LIKE '$filterLike' OR customer.name LIKE '$filterLike' OR product.supplier LIKE '$filterLike'";
    if($query = $this->db->query("$queryCount;")){
      //Se obtiene el numero de resultados que arrojo la consulta
      $query         =            $query->fetch_assoc();
      $numberEntries = (Integer)$query["entries"];
    }
    //Se calcula el numero de paginas que tendra la consulta
    $pages         =                                           $numberEntries / $elementsByPage;
    $numberPages   =                (Integer)( $pages > (Integer)$pages ? $pages + 1 : $pages );
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("SELECT product.*, customer.name AS customername FROM product LEFT JOIN customer on customer.customerid = product.customerid
     WHERE product.productno LIKE '$filterLike' OR product.name LIKE '$filterLike' OR product.description LIKE '$filterLike'
     OR product.description2 LIKE '$filterLike' OR customer.name LIKE '$filterLike' OR product.supplier LIKE '$filterLike'
     ORDER BY product.productno LIMIT $offset,$elementsByPage;")){
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC),
         "numberEntries" => $numberEntries,
         "numberPages" => $numberPages
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
}
?>

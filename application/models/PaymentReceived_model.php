<?php
 require_once("adapters/MySQLConnection.php");
 /*
  **************************************************************************************************
  ***********************Name: PaymentReceived Class Model
  ***********************Description: Realiza las consultas correspondientes a la tabla payment_received
  *********************************** que es la que gestiona los pagos de facturas de un cliente
  **************************************************************************************************
 */
class PaymentReceived_model{
  private $db;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todos los pagos
   **************************************************************************************************
  */
  public function get(){
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select * from payments_received;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>                           TRUE,
        "query"  => $query->fetch_all(MYSQLI_ASSOC)
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene un pago por su id
   **************************************************************************************************
  */
  public function getById($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select pr.*, ai.customer_id from payments_received pr inner join autoinvoice ai on ai.autoid = pr.autoinvoice_id where payment_number = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_assoc()
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene un pago por su autoid
   **************************************************************************************************
  */
  public function getByAutoInvoiceId($id, $numberPage){
    $pages          =    0;
    $query          = null;
    $numberPages    =    0;
    $numberEntries  =    0;
    $elementsByPage =    5;
    //Se calcula la posicion en el que se mostrara la informacion del resultado de la consulta
    $offset         = ($numberPage - 1) * $elementsByPage;
    //Se realiza una consulta para contar los elementos resultado de la consultas a realizar
    if($query = $this->db->query("select count(*) as numberEntries from payments_received where autoinvoice_id = $id;")){
      //Se obtiene el numero de resultados que arrojo la consulta
      $query         =            $query->fetch_assoc();
      $numberEntries = (Integer)$query["numberEntries"];
    }
    //Se calcula el numero de paginas que tendra la consulta
    $pages         =                                           $numberEntries / $elementsByPage;
    $numberPages   =                (Integer)( $pages > (Integer)$pages ? $pages + 1 : $pages );
    if($query = $this->db->query("select * from payments_received where autoinvoice_id = $id order by payment_number desc limit $elementsByPage offset $offset;")){
      //Se devuelve el resultado con la sentencia y los resultados de paginacion
      return array(
        "status"        =>                           TRUE,
        "query"         => $query->fetch_all(MYSQL_ASSOC),
        "numberPages"   =>                   $numberPages,
        "numberEntries" =>                 $numberEntries
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status" =>             FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene un pago por su autoid
   **************************************************************************************************
  */
  public function getByIdAndAutoInvoiceId($id, $invoiceId){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("select pr.*, ai.customer_id from payments_received pr inner join autoinvoice ai on ai.autoid = pr.autoinvoice_id where payment_number = ? and autoinvoice_id = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("ii", $id, $invoiceId);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>                  TRUE,
          "query"  => $query->fetch_assoc()
        );
      }
    }
  }
  /*
   **************************************************************************************************
   Este metodo inserta un nuevo pago a una autoinvoice
   **************************************************************************************************
  */
  public function save($payment){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("insert into payments_received(amount, payment_date, payment_type_id, references_number, autoinvoice_id) values(?, ?, ?, ?, ?)")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("dsiii", $payment["amount"], $payment["payment_date"], $payment["payment_type_id"], $payment["references_number"], $payment["autoinvoice_id"]);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>                     TRUE,
          "payment_number"  => $this->db->insert_id
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo elimina un pago sobre una autoinvoice
   **************************************************************************************************
  */
  public function delete($id){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("delete from payments_received where payment_number = ?")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $id);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
        if( $this->db->affected_rows != 0 ){
          return  TRUE;
        }else{
          return FALSE;
        }
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo actualiza un pago a un autoinvoice
   **************************************************************************************************
  */
  public function update($paymentReceived){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("update payments_received set amount = ? where payment_number = ?")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("di", $paymentReceived["amount"], $paymentReceived["payment_number"]);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
        if( $this->db->affected_rows != 0 ){
          return  TRUE;
        }else{
          return FALSE;
        }
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo actualizar todos los datos de un pago a un autoinvoice
   **************************************************************************************************
  */
  public function updateById($paymentReceived){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("update payments_received set amount = ?, payment_date = ?, payment_type_id = ?, references_number = ? where payment_number = ?")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("dsiii", $paymentReceived["amount"], $paymentReceived["payment_date"], $paymentReceived["payment_type_id"], $paymentReceived["references_number"], $paymentReceived["payment_number"]);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
        if( $this->db->affected_rows != 0 ){
          return  TRUE;
        }else{
          return FALSE;
        }
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
}
?>

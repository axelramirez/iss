<?php
 require_once("adapters/MySQLConnection.php");
/*
 **************************************************************************************************
 ***********************Name: Receiving Detail Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla Receiving
 *********************************** Detail que es la que gestiona los receiving detail
 **************************************************************************************************
*/
class ReceivingDetail_model{
  private $db;
  private $model;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
  */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
    $this->model = array(
      "receivingdetailid" => array("type" => "Integer"),
      "receivingid" => array("type" => "Integer"),
      "productid" => array("type" => "Integer"),
      "unitid" => array("type" => "Integer"),
      "idLocation" => array("type" => "Integer"),
      "originalquantity" => array("type" => "Integer"),
      "currentquantity" => array("type" => "Integer"),
      "quantitytoship" => array("type" => "Integer"),
      "weight" => array("type" => "Double"),
      "weightunitid" => array("type" => "Integer"),
      "location" => array("type" => "String"),
      "lotnumber" => array("type" => "String"),
      "createddate" => array("type" => "String"),
      "isshipped" => array("type" => "String"),
      "qtypart" => array("type" => "Integer"),
      "origen" => array("type" => "String"),
      "truckingid" => array("type" => "Integer"),
      "no" => array("type" => "Integer"),
      "lastStorageDate" => array("type" => "String"),
      "po" => array("type" => "String"),
      "podate" => array("type" => "String"),
      "expdate" => array("type" => "String")
    );
  }

  /*
   **************************************************************************************************
   Este metodo obtiene todos los receiving detail
   **************************************************************************************************
  */
  public function find($fields = array("*"), $receivingid){
    if(count($fields) > 0){
      $fields = implode(",", $fields);
    }else{
      $fields = "*";
    }
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select $fields from receivingdetail where receivingid = $receivingid;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>                            TRUE,
        "query"  => $query->fetch_all(MYSQLI_ASSOC)
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene todos los receiving detail
   **************************************************************************************************
  */
  public function get(){
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select * from receivingdetail;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>                            TRUE,
        "query"  => $query->fetch_all(MYSQLI_ASSOC)
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene un receiving detail por su id
   **************************************************************************************************
  */
  public function getById($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from receivingdetail where receivingdetailid = ?;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_assoc()
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }

  public function getByReceivingId($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("SELECT @numrow:=@numrow+1 AS rowno, rd.*, pr.description AS description, pr.productno AS productNo, l.location AS refLocation, s.quantity AS shippedQty, u.description AS unitName, tn.truckingno, un.description AS packageName FROM receivingdetail rd LEFT JOIN locations l
       ON (l.idLocation = rd.idLocation) LEFT JOIN product pr ON rd.productid = pr.productid LEFT JOIN shippingdetail s ON rd.receivingdetailid = s.receivingdetailid LEFT JOIN uom u ON rd.weightunitid = u.uomid LEFT JOIN truckingnumber tn ON tn.truckingid = rd.truckingid LEFT JOIN unit un ON rd.unitid = un.unitid, (SELECT @numrow:=0) r
       WHERE rd.receivingid = ? AND s.receivinglinesid IS NULL ORDER BY createdDate;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC)
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  public function getDetailById($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("SELECT rl.*, p.productno, p.description, u.description AS unitdescription FROM receivinglines rl LEFT JOIN product p ON(p.productid = rl.productid) LEFT JOIN unit u ON(u.unitid = rl.unitid) WHERE rl.active <> 0 AND rl.receivingdetailid = ?;")){
       //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC)
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  public function getQtyParts($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("SELECT COUNT(*) AS quantity FROM (SELECT IF(rl.productid > 0, 'YES', 'NO') AS Products
   FROM receivinglines rl LEFT JOIN product p ON p.productid = rl.productid WHERE rl.receivingdetailid = ?) receivinglines WHERE Products = 'YES';")){
       //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC)
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  public function getPackageTypeById($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("SELECT * FROM unitcustomer uc INNER JOIN unit u ON(u.unitid=uc.unitid) WHERE uc.customerid= ? ORDER BY u.description;")){
       //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC)
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  public function getAllPackagetype(){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("SELECT * FROM unit ORDER BY description;")){
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC)
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  public function getUnitMesurament(){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("select * from uom;")){
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC)
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  public function getLocationById(){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("SELECT idLocation, location FROM locations;")){
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_all(MYSQLI_ASSOC)
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  /*
   **************************************************************************************************
   Este metodo elimina un receiving detail
   **************************************************************************************************
   */
   public function deleteReceivingDetail($id){
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
     if($sentence = $this->db->prepare("delete from receivingdetail where receivingdetailid = ?")){
       //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
       $sentence->bind_param("i", $id);
       //Se ejecuta la sentencia preparada
       if($sentence->execute()){
         //Se cierra la sentencia
         $sentence->close();
         //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
         if( $this->db->affected_rows != 0 ){
           return  TRUE;
         }else{
           return FALSE;
         }
       }
     }
     //Se devuelve el resultado con el mensaje de error
     return array(
       "status"  =>            FALSE,
       "message" => $this->db->error
     );
   }
   public function delete($arrays, $receivingId){
     $status = false;
     foreach($arrays as $key => $array){
       //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
       if($sentence = $this->db->prepare("delete from receivingdetail where receivingdetailid = ? and receivingid = ?")){
         //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
         $sentence->bind_param("ii", $array['receivingdetailid'], $receivingId);
         //Se ejecuta la sentencia preparada
         if($sentence->execute()){
           $status = ( $sentence->affected_rows > 0 ) ? TRUE : FALSE;
           $sentence->close();
         }
       }
     }
     return $status;
   }
  /*
   **************************************************************************************************
   Este metodo actualiza un receiving detail
   **************************************************************************************************
   */
   public function update($receivingDetail){
     $quantitytoship    =    $receivingDetail["quantitytoship"];
     $currentquantity   =   $receivingDetail["currentquantity"];
     $receivingdetailid = $receivingDetail["receivingdetailid"];
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
     if($sentence = $this->db->prepare("update receivingdetail set quantitytoship = ?, currentquantity = ? where receivingdetailid = ?")){
       //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
       $sentence->bind_param("iii", $quantitytoship, $currentquantity, $receivingdetailid);
       //Se ejecuta la sentencia preparada
       if($sentence->execute()){
         //Se cierra la sentencia
         $sentence->close();
         //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
         if( $this->db->affected_rows == 0 ){
           return  TRUE;
         }else{
           return FALSE;
         }
       }
     }
     //Se imprime el error y se retorna falso de que no se completo la operacion
     print_r($this->db->error);
     return FALSE;
   }
   public function inserttracking($ntids){
       $lastTracking = array();
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
       if($sentence = $this->db->prepare("INSERT INTO truckingnumber (receivingid) VALUES (?);")){
         //Se asignan parametros al '?'
         $sentence->bind_param("i", $receivingid);
         //Se declara un bloque de transaccion para que se asegure que "todos" los datos del arreglo sea guardado o no
         $this->db->query("START TRANSACTION");
         foreach($ntids as $ntr){
          //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
          $receivingid = $ntr["receivingid"];

          //Se ejecuta la sentencia preparada
          $sentence->execute();
          array_push($lastTracking, $this->db->insert_id);
         }
         if($sentence->close()){
          //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
          $this->db->query("COMMIT");
          //Se devuelve status correcto
          return array(
              "status" => TRUE,
              "last_id" => $lastTracking
          );
         }else{
          //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
          $this->db->query("ROLLBACK");
          //Se devuelve el resultado con el mensaje de error
          return FALSE;
         }
       }
   }
   public function insertreceivingdetail($nrds, $receivingId){
       $lastTracking = array();
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
       if($sentence = $this->db->prepare("INSERT INTO receivingdetail (no, receivingid, unitid, idLocation, originalquantity, weight, weightunitid, truckingid, po, podate) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);")){
         //Se asignan parametros al '?'
         $sentence->bind_param("iiiiisiiss", $no, $receivingId, $unitid, $idLocation, $originalquantity, $weight, $weightunitid, $truckingid, $po, $podate);
         //Se declara un bloque de transaccion para que se asegure que "todos" los datos del arreglo sea guardado o no
         $this->db->query("START TRANSACTION");
         foreach($nrds as $nrd){
          //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
          $no = $nrd["receivingdetaillineno"];
          $receivingid = $nrd["receivingid"];
          $unitid = $nrd["unitid"];
          $idLocation = $nrd["idLocation"];
          $originalquantity = $nrd["originalquantity"];
          $weight = $nrd["weight"];
          $weightunitid = $nrd["weightunitid"];
          $truckingid = $nrd["truckingid"];
          $po = $nrd["po"];
          $podate = $nrd["poDate"];

          //Se ejecuta la sentencia preparada
          $sentence->execute();
          array_push($lastTracking, $this->db->insert_id);
         }
         if($sentence->close()){
          //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
          $this->db->query("COMMIT");
          //Se devuelve status correcto
          return array(
              "status" => TRUE,
              "last_id" => $lastTracking
          );
         }else{
          //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
          $this->db->query("ROLLBACK");
          //Se devuelve el resultado con el mensaje de error
          return FALSE;
         }
       }
   }
   public function updatereceiving($rds, $receivingid){

      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
       if($sentence = $this->db->prepare("UPDATE receivingdetail rd, truckingnumber tr SET rd.unitid = ?, rd.idLocation = ?, rd.originalquantity = ?, rd.weight = ?, rd.weightunitid = ?, rd.po = ?, rd.podate = ?, tr.truckingno = ? WHERE tr.truckingid = rd.truckingid AND rd.receivingdetailid = ? AND rd.receivingid = ?;")){
         //Se asignan parametros al '?'
         $sentence->bind_param("iiiiisssii", $unitid, $idlocation, $originalquantity, $weight, $weightunitid, $po, $podate, $truckingno, $receivingdetailid, $receivingid);
         //Se declara un bloque de transaccion para que se asegure que "todos" los datos del arreglo sea guardado o no
         $this->db->query("START TRANSACTION");
         foreach($rds as $rd){
          //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
          $receivingdetailid = $rd["receivingdetailid"];
          $unitid = $rd["unitid"];
          $idlocation = $rd["idLocation"];
          $originalquantity = $rd["originalquantity"];
          $weight = $rd["weight"];
          $weightunitid = $rd["weightunitid"];
          $truckingno = $rd["truckingno"];
          $po = $rd["po"];
          $podate = $rd["podate"];
          //Se ejecuta la sentencia preparada
          $sentence->execute();
         }
         if($sentence->close()){
          //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
          $this->db->query("COMMIT");
          //Se devuelve status correcto
          return TRUE;
         }else{
          //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
          $this->db->query("ROLLBACK");
          //Se devuelve el resultado con el mensaje de error
          return FALSE;
         }
       }
   }
  /*
    **************************************************************************************************
    Este metodo actualiza el estado de isShipped de un receiving detail
    **************************************************************************************************
  */
  public function updateShipped($receivingDetail){
    $isShipped         =         $receivingDetail["isShipped"];
    $receivingdetailid = $receivingDetail["receivingdetailid"];
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("update receivingdetail set isshipped = ? where receivingdetailid = ?")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("si", $isShipped, $receivingdetailid);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
        if( $this->db->affected_rows == 0 ){
          return  TRUE;
        }else{
          return FALSE;
        }
      }
    }
    //Se imprime el error y se retorna falso de que no se completo la operacion
    print_r($this->db->error);
    return FALSE;
  }
  /*
   **************************************************************************************************
   Este metodo actualiza multiples receiving details (quantitytoship y currentquantity)
   **************************************************************************************************
  */
  public function updateAll($receivingsDetails){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("update receivingdetail set quantitytoship = ?, currentquantity = ? where receivingdetailid = ?")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("iii", $quantitytoship, $currentquantity, $receivingdetailid);
      //Se declara un bloque de transaccion para que se asegure que "todos" los datos del arreglo sea guardado o no
      $this->db->query("START TRANSACTION");
      foreach($receivingsDetails as $receivingDetail){
       //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
       $quantitytoship    =    $receivingDetail["quantitytoship"];
       $currentquantity   =   $receivingDetail["currentquantity"];
       $receivingdetailid = $receivingDetail["receivingdetailid"];
       //Se ejecuta la sentencia preparada
       $sentence->execute();
      }
      if($sentence->close()){
       //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
       $this->db->query("COMMIT");
       //Se devuelve status correcto
       return array(
         "status"  => TRUE
       );
      }else{
       //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
       $this->db->query("ROLLBACK");
       //Se devuelve el resultado con el mensaje de error
       return array(
         "status"  =>            FALSE,
         "message" => $this->db->error
       );
      }
    }
  }

  public function updateByAutoInvoiceRecdeets($details){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
     if($sentence = $this->db->prepare("UPDATE  receivingdetail SET lastStorageDate = ? WHERE receivingdetailid = ?;")){
       //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
       $sentence->bind_param("si", $newDate, $receivingdetailid);
       //Se declara un bloque de transaccion para que se asegure que "todos" los datos del arreglo sea guardado o no
       $this->db->query("START TRANSACTION");
       foreach($details as $detail){
        //Se asignara el valor para cada variable que se require en el bind_param de cada arreglo
        $newDate    = $detail["newDate"];
        $receivingdetailid = $detail["receivingdetailid"];
        //Se ejecuta la sentencia preparada
        $sentence->execute();
       }
       if($sentence->close()){
        //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
        $this->db->query("COMMIT");
        //Se devuelve status correcto
        return array(
          "status"  => TRUE
        );
       }else{
        //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
        $this->db->query("ROLLBACK");
        //Se devuelve el resultado con el mensaje de error
        return array(
          "status"  =>            FALSE,
          "message" => $this->db->error
        );
       }
     }
  }

  public function updateMain($fields, $arrays, $receivingId, $truckingNumberModel, $products){
    $status = FALSE;

    foreach($arrays as $key=>$array){
      $receivingDetail = $this->getById($array["receivingdetailid"]);
      $receivingDetail = $receivingDetail["query"];
      $trucking = $truckingNumberModel->save(array(
        "truckingno" => $array["truckingno"],
        "receivingid" => $receivingId
      ));
      $array["truckingid"] = $trucking["last_id"];
      if((Integer)$array["mode"] == 0){
        if($array["newProduct"]){
          $array["productid"] = $products[$array["product"]];
        }
      }
      $array["qtypart"] = $array["receivedqtypart"];
      list($haveRegister, $params_type, $query_string, $params) = generateUpdateQuery($fields, $array, $receivingDetail, $this->model);

      if($haveRegister){
        $params_type .= "i";
        array_unshift($params, $params_type);
        array_push($params, $array["receivingdetailid"]);
        //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
        if($sentence = $this->db->prepare("UPDATE receivingdetail SET $query_string WHERE receivingdetailid = ?;")){
          //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
          call_user_func_array(array($sentence, 'bind_param'), referencesValue($params));
          //Se ejecuta la sentencia preparada
          if($sentence->execute()){
            //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
            $status = ( $sentence->affected_rows > 0 ) ? TRUE : FALSE;
            //Se cierra la sentencia
            $sentence->close();
          }
        }
      }
    }
    return $status;
  }

  /*
   **************************************************************************************************
   Este metodo inserta un nuevo Receiving Detail
   **************************************************************************************************
  */
  public function save($arrays, $receivingId, $truckingNumberModel, $products){
    $status = FALSE;

    foreach($arrays as $key => $array){
      $this->db->query("START TRANSACTION");
      $trackingid = 0;
      if ((Integer)$array["trackinginsert"] == 1) {
          $trucking = $truckingNumberModel->save(array(
              "truckingno" => $array["truckingno"],
              "receivingid" => $receivingId
          ));
          $trackingid = $trucking["last_id"];
      }
      $currentquantity = $array["originalquantity"];
      $current_timestamp = date('Y-m-d H:m:s');
      if((Integer)$array["mode"] == 0){
        if($array["newProduct"]){
          $array["productid"] = $products[$array["product"]];
        }
        //Params Type for Receving Detail Query
        $params_query = "iiiiidisiisssii";
        //Receving Detail Query
        $saveQuery = "INSERT INTO receivingdetail(receivingid, truckingid, idLocation, originalquantity, weightunitid, weight, unitid, createddate, currentquantity, quantitytoship, location, lotnumber, origen, productid, qtypart) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        //Params for Receving Detail Query
        $params = array($params_query, $receivingId, $trackingid, $array["idLocation"], $array["originalquantity"], $array["weightunitid"], $array["weight"], $array["unitid"], $current_timestamp, $currentquantity, 0, "", $array['lotnumber'], $array['origen'], $array["productid"], $array["receivedqtypart"]);
      }else if((Integer)$array["mode"] == 1){
        //Params Type for Receving Detail Query
        $params_query = "iiiiidisssii";
        //Receving Detail Query
        $saveQuery = "INSERT INTO receivingdetail(receivingid, truckingid, idLocation, originalquantity, weightunitid, weight, unitid, po, podate, createddate, currentquantity, quantitytoship) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        print_r($array["podate"]);
        //Params for Receving Detail Query
        $params = array($params_query, $receivingId, $trackingid, $array["idLocation"], $array["originalquantity"], $array["weightunitid"], $array["weight"], $array["unitid"], $array["po"], $array["podate"], $current_timestamp, $currentquantity, 0);
      }
      //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare($saveQuery)){
        //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
        // $sentence->bind_param("iiiiidisssiisss", $receivingId, $trucking["last_id"], $array["idLocation"], $array["originalquantity"], $array["weightunitid"], $array["weight"], $array["unitid"], $array["po"], $array["podate"], $current_timestamp, $currentquantity, $zero, "", "", "");
        call_user_func_array(array($sentence, 'bind_param'), referencesValue($params));
        $sentence->execute();
        if($sentence->close()){
          $this->db->query("COMMIT");
          $status = TRUE;
        }else{
          $this->db->query("ROLLBACK");
          $status = FALSE;
          break;
        }
      }
    }
    return $status;
  }
}
?>

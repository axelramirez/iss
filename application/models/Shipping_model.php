<?php
require_once("adapters/MySQLConnection.php");

require_once("helpers/generateUpdateQuery_helper.php");
require_once("helpers/selectTypeField_helper.php");
require_once("helpers/referencesValue_helper.php");
/*
 **************************************************************************************************
 ***********************Name: Shipping Class Model
 ***********************Description: Realiza las consultas correspondientes a la tabla shipping que
 *********************************** es la que gestiona las salidas demercancia de los clientes
 **************************************************************************************************
 */
class Shipping_model{
  private $db;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo crea una instancia de la conexion a MySQL
   **************************************************************************************************
   */
  public function __construct(){
    $this->db = MySQLConnection::getInstance()->getConnection();
  }

  /*
   **************************************************************************************************
   Este metodo obtiene todas las salidas de mercancias de los clientes
   **************************************************************************************************
   */
  public function get(){
    //Se ejecuta la consulta y se obtiene el resultado
    if($query = $this->db->query("select *, DATE_FORMAT(createddate,'%Y-%m-%d') AS shippdate from shipping;")){
      //Se devuelve el resultado con la sentencia
      return array(
        "status" =>   TRUE,
        "query"  => $query
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene un salida por su id
   **************************************************************************************************
   */
  public function getById($id){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("select *, DATE_FORMAT(createddate,'%Y-%m-%d') AS shippdate from shipping where shippingid = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $id);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>   TRUE,
          "query"  => $query
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }

  public function update($id){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("UPDATE shipping SET isbilled = 'F' WHERE shippingid IN (SELECT ref_id FROM autoinv_list WHERE type = 'S' AND auto_id = ?);")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $id);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con true o false dependiendo si hubo campos afectados o no
       if( $this->db->affected_rows == 0 ){
         return  TRUE;
       }else{
         return FALSE;
       }
     }
   }
   //Se imprime el error y se retorna falso de que no se completo la operacion
   print_r($this->db->error);
   return FALSE;
  }

  public function searchShippingBy($numberPage, $params, $filter){
    $pages          =       0;
    $query          =    null;
    $empty          =    TRUE;
    $unique         =    TRUE;
    $numberPages    =       0;
    $numberEntries  =       0;
    $elementsByPage =      15;
    $result         = array();
    // print_r($params);
    //Se calcula la posicion en el que se mostrara la informacion del resultado de la consulta
    $offset = ($numberPage - 1) * $elementsByPage;
    $where = "WHERE";
    $queryJoin = "LEFT JOIN billsoflading bol ON a.shippingid = bol.shippingid LEFT JOIN carrier b ON a.carrierid = b.carrierid LEFT JOIN user u ON a.userid = u.userid LEFT JOIN tracking tr ON tr.shippingid = a.shippingid LEFT JOIN preshipping ps ON ps.shippingid = a.shippingid LEFT JOIN customer c ON c.customerid = a.customerid LEFT JOIN user us ON us.userid = a.userid";
    $queryCount = "select count(*) AS numberEntries FROM shipping a";
    //shippingno section where
    $result        = $this->checkFragment(strcmp($params["shippingNumber"], "") != 0, "a.shippingno = '$params[shippingNumber]'", $unique);
    $unique          = $result["unique"];
    $empty           = $result["empty"];
    $shippingNo     = $result["fragment"];
    //customer section where
    $result          = $this->checkFragment((Integer)$params["customerId"] > 0, "a.customerid = '$params[customerId]'", $unique);
    $unique          = $result["unique"];
    $empty           = $result["empty"];
    $customer    = $result["fragment"];
    //Product section where
    $result          = $this->checkFragment((Integer)$params["productId"] > 0, "a.shippingid IN (SELECT shippingid FROM shippingdetail WHERE receivingdetailid IN (SELECT receivingdetailid FROM receivingdetail WHERE productid = (SELECT productid FROM product WHERE productid = '$params[productId]')))", $unique);
    $unique          = $result["unique"];
    $empty           = $result["empty"];
    $product     = $result["fragment"];
    //carrier section where
    $result          = $this->checkFragment((Integer)$params["carrierId"] > 0, "a.carrierid = '$params[carrierId]'", $unique);
    $unique          = $result["unique"];
    $empty           = $result["empty"];
    $carrier     = $result["fragment"];
    //driver section where
    $result        = $this->checkFragment(strcmp($params["driver"], "") != 0, "driver = '$params[driver]'", $unique);
    $unique          = $result["unique"];
    $empty           = $result["empty"];
    $driver     = $result["fragment"];
    //trucknumber section where
    $result        = $this->checkFragment(strcmp($params["truckNumber"], "") != 0, "trucknumber = '$params[truckNumber]'", $unique);
    $unique          = $result["unique"];
    $empty           = $result["empty"];
    $trucknumber     = $result["fragment"];
    //receivingno section where
    if (strcmp($params["receivingNo"], "") != 0) {
      $querySearch .= ', shippingdetail shd, receivingdetail rd, receiving r';
    }
    $result        = $this->checkFragment(strcmp($params["receivingNo"], "") != 0, "a.shippingid = shd.shippingid AND shd.receivingdetailid = rd.receivingdetailid AND rd.receivingid = r.receivingid AND r.receivingno = '$params[receivingNo]'", $unique);
    $unique          = $result["unique"];
    $empty           = $result["empty"];
    $receivingno     = $result["fragment"];
    //inbond section where
    if (strcmp($params["inbond"], "") != 0) {
      if (strcmp($params["receivingNo"], "") != 0) {
        $result        = $this->checkFragment(strcmp($params["inbond"], "") != 0, "r.receivingid IN(SELECT receivingid FROM tande WHERE tande = '$params[inbond]')", $unique);
        $unique          = $result["unique"];
        $empty           = $result["empty"];
        $inbond     = $result["fragment"];
      }else {
        $querySearch .= ', shippingdetail shd, receivingdetail rd, receiving r';
        $result        = $this->checkFragment(strcmp($params["inbond"], "") != 0, "a.shippingid = shd.shippingid AND shd.receivingdetailid = rd.receivingdetailid AND rd.receivingid = r.receivingid AND r.receivingid IN (SELECT receivingid FROM tande WHERE tande = '$params[inbond]')", $unique);
        $unique          = $result["unique"];
        $empty           = $result["empty"];
        $inbond     = $result["fragment"];
      }
    }
    //tracking section where
    $result        = $this->checkFragment(strcmp($params["trackingNo"], "") != 0, "tr.trackingnumber = '$params[trackingNo]'", $unique);
    $unique          = $result["unique"];
    $empty           = $result["empty"];
    $tracking     = $result["fragment"];
    //pedimento section where
    $result        = $this->checkFragment(strcmp($params["pedimento"], "") != 0, "a.pedimento = '$params[pedimento]'", $unique);
    $unique          = $result["unique"];
    $empty           = $result["empty"];
    $pedimento     = $result["fragment"];
    //Date section where
    $dateFrom        = (strcmp($params["dateFrom"], "") != 0) ? "createddate >= '$params[dateFrom]'" : "";
    $dateTo          = (strcmp($params["dateTo"], "") != 0) ? "createddate <= '$params[dateTo]'" : "";
    $result          = $this->checkDate($dateFrom, $dateTo, $unique);
    $unique          = $result["unique"];
    $empty           = $result["empty"];
    $dateSection     = $result["fragment"];
    // print($empty);
    if ($empty == 1) {
      $where = "WHERE a.status = 1";
    }else {
      $where .= "$dateSection $shippingNo $customer $product $carrier $driver $trucknumber $receivingno $inbond $tracking $pedimento AND a.status = 1";
    }

    //Despues del filtro realiza las consultas de contar y de busqueda
    //Se realiza una consulta para contar los elementos resultado de la consultas a realizar
    if($query = $this->db->query("$queryCount $queryJoin $where;")) {
      //se obtiene el numero de resultados de la consulta
      $query         =            $query->fetch_assoc();
      $numberEntries = (Integer)$query["numberEntries"];
    }
    //Se calcula el numero de paginas que tendra la consulta
    $pages         =                                           $numberEntries / $elementsByPage;
    $numberPages   =                (Integer)( $pages > (Integer)$pages ? $pages + 1 : $pages );
    $querySearch = "select a.*, (SELECT d.documentid FROM shipping s LEFT JOIN document d ON s.shippingid = d.type_recid AND d.type = 'shipping'
    AND d.description = 'EDI' WHERE s.shippingid = 1) as docId, ps.shippingno as preshippingno, c.name as customername, b.name as carriername, CONCAT(us.firstname,' ',us.lastname) as createdby from (select * from shipping a $where order by shippingid desc limit $offset,$elementsByPage) a ";

    if($query = $this->db->query("$querySearch $queryJoin order by a.shippingid desc;")){
      //Se devuelve el resultado con la sentencia y los resultados de paginacion
      return array(
        "status"        =>                           TRUE,
        "query"         => $query->fetch_all(MYSQL_ASSOC),
        "numberPages"   =>                   $numberPages,
        "numberEntries" =>                 $numberEntries
      );
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status" =>             FALSE,
      "message" => $this->db->error
    );
  }
  public function deleteShipment($idShipping){
    $query          =    null;
    $result         = array();
    $receivingDetail = $this->getRD($idShipping);
    $queryReceivingD          = $receivingDetail["query"];
    $quantity          = (Integer)$queryReceivingD["quantity"];
    $receivinglinesid  = $queryReceivingD["receivinglinesid"];
    $receivingdetailid = $queryReceivingD["receivingdetailid"];

    if ($quantity > 0) {
      if ($quantity == '' || $quantity == null) {
        $quantity = 0;
      }
      if (is_null($receivinglinesid)) {
        $this->updateReceivingDetail($receivingdetailid, $quantity);
      }else {
        $this->updateReceivingLines($quantity, $receivinglinesid);
      }

      $rec = "SELECT receivingid from receivingdetail where receivingdetailid = $receivingdetailid";
      if($query = $this->db->query("$rec;")) {
        $query         =            $query->fetch_assoc();
        $receivingid = (Integer)$query['receivingid'];
      }
      $this->updateReceiving($receivingid);
    }
    //Se llama la funcion para eliminar el shipping detail
    $this->deleteShippingDetail($idShipping);

    if($sentence = $this->db->prepare("DELETE FROM shipping WHERE shippingid = ?")){
      //Se asignan parametros al '?'
      $sentence->bind_param("i", $idShipping);
      $this->db->query("START TRANSACTION");
      //Se ejecuta la sentencia preparada
      if ($sentence->execute()) {
          if($sentence->close()){
           //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
           $this->db->query("COMMIT");
           //Se devuelve status correcto
           return TRUE;
          }else{
           //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
           $this->db->query("ROLLBACK");
           //Se devuelve el resultado con el mensaje de error
           return FALSE;
          }
      }
    }
  }
  public function getByShippingId($id){
    $sqlConcat = ', GROUP_CONCAT(IF(se.customerid IS NULL, se.seal, NULL)) AS internalSeals,
    GROUP_CONCAT(IF(se.customerid IS NOT NULL, se.seal, NULL)) AS customerSeals';
    //Se ejecuta la consulta y se obtiene el resultado
    if($sentence = $this->db->prepare("SELECT s.*, c.ownInventory, TIME_FORMAT(s.createddate, '%H:%i') AS createdtime, DATE_FORMAT(s.createddate,'%Y-%m-%d') AS shippdate $sqlConcat FROM shipping s LEFT JOIN seals se ON(se.shippingid=s.shippingid) LEFT JOIN customer c ON(c.customerid = s.customerid) WHERE s.shippingid = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $id);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
      $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>                     TRUE,
          "query"  => $query->fetch_all(MYSQLI_ASSOC)
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  public function getRD($idShipping){
   //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
   if($sentence = $this->db->prepare("SELECT a.*, b.receivingdetailid, b.receivingid, d.receivinglinesid,
   IF(d.receivinglinesid IS NULL, b.no, '') AS no,
   IF(d.receivinglinesid IS NULL, b.productid, d.productid) AS productid,
   IF(d.receivinglinesid IS NULL, b.unitid, d.unitid) AS unitid,
   IF(d.receivinglinesid IS NULL, b.originalquantity, d.originalQty) AS originalquantity,
   IF(d.receivinglinesid IS NULL, b.currentquantity, d.currentQty) AS currentquantity,
   IF(d.receivinglinesid IS NULL, 1, d.active) AS active, c.receivingno, uom.description AS unitMesurament,
   b.weight AS totalWeight, a.weight, b.weightunitid, b.location, p.po,
   IF(d.receivinglinesid IS NULL, b.lotnumber, d.lotnumber) AS lotnumber,
   DATE_FORMAT(b.createddate,'%Y-%m-%d') AS date, c.freightbill,
   tr.truckingno AS tracking, l.idLocation, l.location AS refLocation,
   DATE_FORMAT(b.lastStorageDate,'%Y-%m-%d') AS lastStorageDate
   FROM (shippingdetail a, receivingdetail b, receiving c)
   LEFT JOIN receivinglines d
   ON (b.receivingdetailid = d.receivingdetailid AND a.receivinglinesid = d.receivinglinesid)
   LEFT JOIN truckingnumber tr ON (b.truckingid=tr.truckingid)
   LEFT JOIN locations l ON (b.idLocation=l.idLocation)
   LEFT JOIN po p ON b.receivingdetailid=p.receivingdetailid
   LEFT JOIN uom ON (uom.uomid = b.weightunitid)
   WHERE a.receivingdetailid = b.receivingdetailid
   AND b.receivingid = c.receivingid
   AND (d.active = 1 OR d.receivinglinesid IS NULL)
   AND a.shippingid =  ?
   ORDER BY receivingid, b.receivingdetailid, d.receivinglinesid;")){
     //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
     $sentence->bind_param("i", $idShipping);
     //Se ejecuta la sentencia preparada
     if($sentence->execute()){
       //Se obtiene el resultado de la ejecucion de la sentencia
       $query = $sentence->get_result();
       //Se cierra la sentencia
       $sentence->close();
       //Se devuelve el resultado con la sentencia
       return array(
         "status" =>                  TRUE,
         "query"  => $query->fetch_assoc()
       );
     }
   }
   //Se devuelve el resultado con el mensaje de error
   return array(
     "status"  =>            FALSE,
     "message" => $this->db->error
   );
  }
  public function updateReceivingDetail($receivingdetailid, $quantity){
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("UPDATE receivingdetail SET quantitytoship = quantitytoship - $quantity, currentquantity=currentquantity + $quantity, isshipped='F'
      WHERE receivingdetailid = ?;")){
        //Se asignan parametros al '?'
        $sentence->bind_param("i", $receivingdetailid);
        $this->db->query("START TRANSACTION");
        //Se ejecuta la sentencia preparada
        if ($sentence->execute()) {
            if($sentence->close()){
             //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
             $this->db->query("COMMIT");
             //Se devuelve status correcto
             return TRUE;
            }else{
             //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
             $this->db->query("ROLLBACK");
             //Se devuelve el resultado con el mensaje de error
             return FALSE;
            }
        }
      }
  }
  public function updateReceivingLines($quantity, $receivinglinesid){
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("UPDATE receivinglines SET currentQty=currentQty + ? WHERE receivinglinesid = ?;")){
        //Se asignan parametros al '?'
        $sentence->bind_param("ii", $quantity, $receivinglinesid);
        $this->db->query("START TRANSACTION");
        //Se ejecuta la sentencia preparada
        if ($sentence->execute()) {
            if($sentence->close()){
             //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
             $this->db->query("COMMIT");
             //Se devuelve status correcto
             return TRUE;
            }else{
             //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
             $this->db->query("ROLLBACK");
             //Se devuelve el resultado con el mensaje de error
             return FALSE;
            }
        }
      }
  }
  public function updateReceiving($receivingid){
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("UPDATE receiving SET isshipped = 'F' WHERE receivingid = ?;")){
        //Se asignan parametros al '?'
        $sentence->bind_param("i", $receivingid);
        $this->db->query("START TRANSACTION");
        //Se ejecuta la sentencia preparada
        if ($sentence->execute()) {
            if($sentence->close()){
             //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
             $this->db->query("COMMIT");
             //Se devuelve status correcto
             return TRUE;
            }else{
             //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
             $this->db->query("ROLLBACK");
             //Se devuelve el resultado con el mensaje de error
             return FALSE;
            }
        }
      }
  }
  public function deleteShippingDetail($idShipping){
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("DELETE FROM shippingdetail WHERE shippingid = ?")){
        //Se asignan parametros al '?'
        $sentence->bind_param("i", $idShipping);
        $this->db->query("START TRANSACTION");
        //Se ejecuta la sentencia preparada
        if ($sentence->execute()) {
            if($sentence->close()){
             //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
             $this->db->query("COMMIT");
             //Se devuelve status correcto
             return TRUE;
            }else{
             //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
             $this->db->query("ROLLBACK");
             //Se devuelve el resultado con el mensaje de error
             return FALSE;
            }
        }
      }
  }
  /*
   **************************************************************************************************
   Este metodo obtiene los cargos de un cliente
   **************************************************************************************************
   */
  public function getChargesBy($customerId){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("SELECT servicecustomer.servicecustomerid,
    servicecustomer.customerid, services.serviceid, servicecustomer.cost, services.description, services.`code`,
    services.variant, revisionrates.increment, revisionrates.baserate FROM services
    LEFT JOIN servicecustomer ON services.serviceid = servicecustomer.serviceid AND servicecustomer.customerid = ?
    LEFT JOIN revisionrates ON revisionrates.customerid = servicecustomer.customerid
    WHERE (servicecustomerid IS NOT NULL
    AND (services.system = 0 OR (services.system = '1' AND services.description = 'revision'))) OR services.variant = 1
    ORDER BY services.description;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $customerId);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>   TRUE,
          "query"  => $query->fetch_all(MYSQL_ASSOC)
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene los cargos aplicados en un shipping
   **************************************************************************************************
   */
  public function getChargesAppliedBy($shippingId){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("SELECT sr.serviceshippingid, s.variant, s.serviceid, s.code, s.description, sr.cost
      FROM services s, serviceshipping sr WHERE sr.serviceid = s.serviceid AND sr.shippingid = ?;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $shippingId);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>   TRUE,
          "query"  => $query->fetch_all(MYSQL_ASSOC)
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo obtiene los detalles de un shipping
   **************************************************************************************************
   */
  public function getDetailsBy($shippingId){
    //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
    if($sentence = $this->db->prepare("SELECT a.*, b.receivingdetailid, b.receivingid, d.receivinglinesid,
    IF(d.receivinglinesid IS NULL, b.no, '') AS no,
    IF(d.receivinglinesid IS NULL, b.productid, d.productid) AS productid,
    IF(d.receivinglinesid IS NULL, b.unitid, d.unitid) AS unitid,
    IF(d.receivinglinesid IS NULL, b.originalquantity, d.originalQty) AS originalquantity,
    IF(d.receivinglinesid IS NULL, b.currentquantity, d.currentQty) AS currentquantity,
    IF(d.receivinglinesid IS NULL, 1, d.active) AS active,
    c.receivingno, uom.description AS unitMesurament, b.weight AS totalWeight, a.weight, b.weightunitid, b.location, p.po,
    IF(d.receivinglinesid IS NULL, b.lotnumber, d.lotnumber) AS lotnumber,
    DATE_FORMAT(b.createddate,'%Y-%m-%d') AS date, c.freightbill,
    tr.truckingno AS tracking, l.idLocation, l.location AS refLocation,
    DATE_FORMAT(b.lastStorageDate,'%Y-%m-%d') AS lastStorageDate,
    product.description AS descriptionpart, product.productno, product.productno, unit.description AS nameunit
    FROM (shippingdetail a, receivingdetail b, receiving c)
    LEFT JOIN receivinglines d
    ON (b.receivingdetailid = d.receivingdetailid AND a.receivinglinesid = d.receivinglinesid)
    LEFT JOIN truckingnumber tr ON (b.truckingid=tr.truckingid)
    LEFT JOIN locations l ON (b.idLocation=l.idLocation)
    LEFT JOIN po p ON b.receivingdetailid=p.receivingdetailid
    LEFT JOIN uom ON (uom.uomid = b.weightunitid)
    LEFT JOIN product ON (product.productid = b.productid)
    LEFT JOIN unit ON (b.unitid = unit.unitid)
    WHERE a.receivingdetailid = b.receivingdetailid
    AND b.receivingid = c.receivingid
    AND (d.active = 1 OR d.receivinglinesid IS NULL)
    AND a.shippingid =  ?
    ORDER BY receivingid, b.receivingdetailid, d.receivinglinesid;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      $sentence->bind_param("i", $shippingId);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
        //Se obtiene el resultado de la ejecucion de la sentencia
        $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>   TRUE,
          "query"  => $query->fetch_all(MYSQL_ASSOC)
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  /*
   **************************************************************************************************
   Este metodo agrega entradas a un shipping
   **************************************************************************************************
   */
  public function insertReceivinginShipping($idShipping, $data){
    $insertReceivings = $data["receivingList"];
    $receivingId = $data["receivingId"];
      $rec = "(SELECT a.*, null as receivinglinesid FROM receivingdetail a
      WHERE receivingid IN ($insertReceivings) AND isshipped !='T' AND receivingdetailid not in ($receivingId))
      UNION (SELECT c.*, d.receivinglinesid FROM receivingdetail c
      INNER JOIN receivinglines d on c.receivingdetailid = d.receivingdetailid
      WHERE receivingid IN ($insertReceivings) AND isshipped !='T' AND c.receivingdetailid not in ($receivingId))
      ORDER BY receivingdetailid;";
      if($query = $this->db->query("$rec;")) {
        $receivingdetails = $query->fetch_all(MYSQLI_ASSOC);
      }
      $valueAry = array();
      for ($i=0; $i < count($receivingdetails) ; $i++) {
        $receivingdetailid = $receivingdetails[$i]['receivingdetailid'];
        if ($receivingdetails[$i]['weight'] == "") {
          $weight = 0;
        }else {
          $weight = $receivingdetails[$i]['weight'];
        }
        $receivinglineid = (is_null($receivingdetails[$i]['receivinglinesid'])) ? "default" : $receivingdetails[$i]['receivinglinesid'];
        array_push($valueAry, "($idShipping, $receivingdetailid,  $weight, $receivinglineid)");
      }
      $sqlQuery = "INSERT INTO shippingdetail(shippingid, receivingdetailid, weight, receivinglinesid) VALUES" . implode(',', $valueAry);
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("$sqlQuery;")){
        //Se asignan parametros al '?'
        // $sentence->bind_param("i", $receivingdetailid);
        $this->db->query("START TRANSACTION");
        //Se ejecuta la sentencia preparada
        if ($sentence->execute()) {
            if($sentence->close()){
             //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
             $this->db->query("COMMIT");
             //Se devuelve status correcto
             return TRUE;
            }else{
             //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
             $this->db->query("ROLLBACK");
             //Se devuelve el resultado con el mensaje de error
             return FALSE;
            }
        }
      }
  }
  /*
   **************************************************************************************************
   Este metodo agrega entradas a un shipping
   **************************************************************************************************
   */
  public function insertProductinShipping($idShipping, $data){
      $ids = 0;
      $rdids = 0;
      $idsf = 0;
      $tid = 0;
      $dates = array();
      $name = array();
      $id = array();
      $IDS = array();
      $aids = array();
      $idss = array();
      $DetailRelation = array();

      for ($i=0; $i < count($data); $i++) {
        if ($ids == 0) {
          $ids = $data[$i]['receivingdetailids'];
        }else {
          $ids .= ',' . $data[$i]['receivingdetailids'];
        }
        $rdids = explode(',', $ids);
        for ($j=0; $j < count($rdids) ; $j++) {
          $rdidsDates = explode(':', $rdids[$j]);
          $dates[$i][$j][0] = $rdidsDates[0];
          $dates[$i][$j][1] = $rdidsDates[1];
          array_push($name, $dates[$i][$j][0]);
        }
        for ($ij=0; $ij < count($dates[$i]); $ij++) {
          for ($ji=0; $ji < count($dates[$i]); $ji++) {
            $result = $this->earlierdate(date($dates[$i][$ij][1]), date($dates[$i][$ji][1]));
            if (!$result) {
              $temp[0] = $dates[$i][$ij][0];
              $temp[1] = $dates[$i][$ij][1];

              $dates[$i][$ij][0] = $dates[$i][$ji][0];
              $dates[$i][$ij][1] = $dates[$i][$ji][1];

              $dates[$i][$ji][0] = $temp[0];
              $dates[$i][$ji][1] = $temp[1];
            }
          }
        }
        foreach ($dates[$i] as $var) {
          array_push($id, $var[0]);
        }
        // $names[$i] = implode('', $name); //
        $aids[$i] = implode(',', $id);
        $id = (array) null;
        $name = (array) null;
      }
      for ($j=0; $j < count($rdids) ; $j++) {
        $teids = explode(':', $rdids[$j]);
        array_push($idss, $teids[0]);
      }
      $iids = implode(',', $aids);
      $idsl = implode(',', $idss);

      $rec = "SELECT receivingdetailid, weight, currentquantity FROM receivingdetail WHERE receivingdetailid in ($iids) AND isshipped !='T';";
      if($query = $this->db->query("$rec;")) {
        $receivingdetails = $query->fetch_all(MYSQLI_ASSOC);
      }


      for ($i=0; $i < count($receivingdetails) ; $i++) {
        $receivingdetailid = $receivingdetails[$i]['receivingdetailid'];
        if ($receivingdetails[$i]['weight'] == "") {
          $weight = 0;
        }else {
          $weight = $receivingdetails[$i]['weight'];
        }
        $sqlInsert= "INSERT INTO shippingdetail (shippingid, receivingdetailid, weight) VALUES ($idShipping, $receivingdetailid, $weight)";
        if($query = $this->db->query("$sqlInsert;")) {
          $last_id = $this->db->insert_id;
        }

        $DetailRelation[$receivingdetailid] = $last_id;
        $currentquantity = $receivingdetails[$i]['currentquantity'];
      }
      // $sqlQuery = "CALL UpdateInbondsisShipped($idShipping, 1);";
      $varId = explode(',', $idsl);
      $pdetails = array();
      for ($i=0; $i < count($varId); $i++) {
        $sqlSelect = "SELECT receivingdetailid, currentquantity FROM receivingdetail WHERE receivingdetailid = $varId[$i] AND isshipped !='T';";
        if($query = $this->db->query("$sqlSelect;")) {
          $details = $query->fetch_all(MYSQLI_ASSOC);
          $pdetails[$i] = $details;
        }
      }
      // print_r($data);
      $outerarray = array();
      $nulos = array();
      for ($l=0; $l < count($pdetails); $l++) {
        $currentquantity = $pdetails[$l][0]["currentquantity"];
        $receivingdetailid = $pdetails[$l][0]["receivingdetailid"];
        for ($m=0; $m < count($data); $m++) {
          $rid = explode(',', $data[$m]['ids']);
          for ($n=0; $n < count($rid); $n++) {
            if ($rid[$n] == $receivingdetailid) {
              $outerValue = $data[$m]['outerQty'];
              if ($outerValue <= $currentquantity && $outerValue > 0) {
                $jsonA['shippingdetail'] = $DetailRelation[$receivingdetailid];
                $jsonA['value'] = $outerValue;
                array_push($outerarray, $jsonA);
              }elseif($outerValue < $currentquantity){
                $jsonA['shippingdetail'] = $DetailRelation[$receivingdetailid];
                $jsonA['value'] = $currentquantity;
                $outerValue = $outerValue - $currentquantity;
                array_push($outerarray, $jsonA);
              }elseif ($outerValue == 0) {
                $jsonA['shippingdetail'] = $DetailRelation[$receivingdetailid];
                array_push($nulos, $jsonA['shippingdetail']);
              }
            }
          }
        }
      }
      print_r($outerarray);
      return;
     //Se crea una sentencia preparada, la cual permite asignarle parametros siempre y cuando tenga '?'
      if($sentence = $this->db->prepare("$sqlQuery;")){
        //Se asignan parametros al '?'
        // $sentence->bind_param("i", $receivingdetailid);
        $this->db->query("START TRANSACTION");
        //Se ejecuta la sentencia preparada
        if ($sentence->execute()) {
            if($sentence->close()){
             //Se cierra el bloque de transaccion que sera verificado que se cumpla que se guarde todo o nada
             $this->db->query("COMMIT");
             //Se devuelve status correcto
             return TRUE;
            }else{
             //Se cierra el bloque de transaccion y se borran todos los datos que se insertaron antes de que ocurriera el error
             $this->db->query("ROLLBACK");
             //Se devuelve el resultado con el mensaje de error
             return FALSE;
            }
        }
      }
  }
  public function getPartsD($shippingId, $filter, $customerId){
    $c = 0;
    $receivingids = 0;
    $ids = 0;
    $rec = "SELECT DISTINCT receivingdetailid AS id FROM shippingdetail WHERE shippingid= $shippingId;";
    if($query = $this->db->query("$rec;")) {
      $receivingids = $query->fetch_all(MYSQLI_ASSOC);
    }
    $lenght = count($receivingids);
    for ($i=0; $i < $lenght; $i++) {
      if ($c == 0) {
        $c++;
        $ids = $receivingids[$i]['id'];
      }
      else {
        $ids.= ',' . $receivingids[$i]['id'];
      }
    }
    $parList = "SELECT DISTINCT productid AS partid, productno AS part FROM product WHERE customerid=$customerId ORDER BY productno;";
    if($query = $this->db->query("$parList;")) {
      $productsSearch = $query->fetch_all(MYSQLI_ASSOC);
    }
    $where = "WHERE a.customerid = b.customerid AND a.isshipped = 'F' AND a.status = 1 AND rd.receivingdetailid NOT IN ($ids) AND a.receivingid=rd.receivingid";
    if ($customerId != '') {
      $where.= " AND a.customerid = $customerId";
    }
    if ($filter != '') {
      $filter = '%' . $filter . '%';
      $where.= " AND rd.productid LIKE '$filter'";
    }
    //Se ejecuta la consulta y se obtiene el resultado
    if($sentence = $this->db->prepare("SELECT DISTINCT GROUP_CONCAT(rd.receivingdetailid) AS ids, GROUP_CONCAT(rd.receivingdetailid,':', date(a.createddate)) AS receivingdetailids, pr.productid,
    pr.productno,pr.name,sum(currentquantity)AS currentquantity FROM receiving a, customer b, receivingdetail rd LEFT JOIN product pr ON (rd.productid=pr.productid) $where GROUP BY pr.productid ORDER BY a.createddate DESC;")){
      //Se asignan parametros al '?' donde i recibira un entero el cual obtiene de la variable $id
      // $sentence->bind_param("i", $id);
      //Se ejecuta la sentencia preparada
      if($sentence->execute()){
      $query = $sentence->get_result();
        //Se cierra la sentencia
        $sentence->close();
        //Se devuelve el resultado con la sentencia
        return array(
          "status" =>                     TRUE,
          "query"  => $query->fetch_all(MYSQLI_ASSOC),
          "productsSearch" => $productsSearch
        );
      }
    }
    //Se devuelve el resultado con el mensaje de error
    return array(
      "status"  =>            FALSE,
      "message" => $this->db->error
    );
  }
  private function checkFragment($condition, $fragment, $unique){
    if( $condition ){
      return array(
        "empty"    =>                                 FALSE,
        "unique"   =>                                 FALSE,
        "fragment" => $this->checkEmpty($fragment, $unique)
      );
    }
    return array(
      "empty"    => $unique,
      "unique"   => $unique,
      "fragment" =>      ""
    );
  }
  private function checkEmpty($fragment, $unique){
    $separator = "and";
    if( $unique ){
      $separator = "";
      $unique = FALSE;
    }
    return "$separator $fragment";
  }
  private function checkDate($startDate, $endDate, $unique){
    $fragment = "";
    $separator = "and";
    if( $unique ){
      $separator = "";
    }
    if( strcmp($startDate, "") != 0 && strcmp($endDate, "") != 0 ){
      $fragment = "$separator ($startDate and $endDate)";
      $unique = FALSE;
    }else if( strcmp($startDate, "") != 0 ){
      $fragment =                "$separator $startDate";
      $unique = FALSE;
    }else if( strcmp($endDate, "") != 0 ){
      $fragment =                  "$separator $endDate";
      $unique = FALSE;
    }
    return array(
      "empty"    =>   $unique,
      "unique"   =>   $unique,
      "fragment" => $fragment
    );
  }
  function earlierdate($primera, $segunda) {
      $valoresPrimera = explode("-", $primera);
      $valoresSegunda = explode("-", $segunda);

      $anyoPrimera = $valoresPrimera[0];
      $mesPrimera = $valoresPrimera[1];
      $diaPrimera = $valoresPrimera[2];

      $anyoSegunda = $valoresSegunda[0];
      $mesSegunda = $valoresSegunda[1];
      $diaSegunda = $valoresSegunda[2];

      if (checkdate($mesPrimera, $diaPrimera, $anyoPrimera) &&
              checkdate($mesSegunda, $diaSegunda, $anyoSegunda)) {
          $primera = strtotime($primera);
          $segunda = strtotime($segunda);
          if ($primera == $segunda)
              return 0;
          elseif ($primera > $segunda)
              return false;
          else
              return true;
      } else
          return 0;
  }
}
?>

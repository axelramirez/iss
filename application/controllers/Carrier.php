<?php
//Models
require_once(          "models/Carrier_model.php");
/*
 **************************************************************************************************
 *************************Name: Purchase Order Class Controller
 *************************Description: Se encarga de agregar purchase orders
 **************************************************************************************************
*/
class Carrier{
  private           $carrierModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->carrierModel = new Carrier_model();
  }
  /*
   **************************************************************************************************

   **************************************************************************************************
  */
  public function insertCarrier(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $newcarrier = $params["newCarrier"];
     if($this->carrierModel->insert($newcarrier)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $newcarrier
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function getCarrier(){
      $params = $_GET;
      $resultQuery = array();
      $resultQuery = $this->carrierModel->get();
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "carriers" => $resultQuery["query"]
          )
      )));
  }
  public function getCarrierLimit(){
      $params = $_GET;
      $resultQuery = array();
      $numberPage = (Integer)$params["page"];
      $resultQuery = $this->carrierModel->getbypagination($numberPage);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "carriers" => $resultQuery["query"],
              "numberEntries" => $resultQuery["numberEntries"],
              "numberPages" => $resultQuery["numberPages"]
          )
      )));
  }
  public function updateCarrier(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $carrierId = $params["carrierId"];
      $data = $params["updateCarrier"];
     if($this->carrierModel->updateCarrier($data, $carrierId)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $data
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function insertCarrierAll(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $data = $params["insertCarrier"];
     if($this->carrierModel->insertCarrier($data)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $data
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  //Search carriers
  public function searchCarrier(){
      $params = $_GET;
      $resultQuery = array();
      $numberPage = (Integer)$params["page"];
      $filter = $params["filter"];
      $resultQuery = $this->carrierModel->searchbypagination($numberPage, $filter);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "carriers" => $resultQuery["query"],
              "numberEntries" => $resultQuery["numberEntries"],
              "numberPages" => $resultQuery["numberPages"]
          )
      )));
  }
}
?>

<?php
//Models
require_once(          "models/Document_model.php");
/*
 **************************************************************************************************
 *************************Name: Documents Class Controller
 *************************Description: Se encarga de documents
 **************************************************************************************************
*/
class Document{
  private           $documentModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->documentModel = new Document_model();
  }
  /*
   **************************************************************************************************

   **************************************************************************************************
  */
  public function getDocuments(){
      $params = $_GET;
      $resultQuery = array();
      $type = $params["type"];
      $type_key = $params["typekey"];
      $id = (Integer)$params["type_recid"];
      $resultQuery = $this->documentModel->getByReceivingId($type, $type_key, $id);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "documents" => $resultQuery["query"]
          )
      )));
  }
}
?>

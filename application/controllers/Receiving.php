<?php
//Models
require_once("models/Receiving_model.php");
require_once("models/ReceivingDetail_model.php");
require_once("models/PurchaseOrder_model.php");
require_once("models/Freightbill_model.php");
require_once("models/Tande_model.php");
require_once("models/TruckingNumber_model.php");
require_once("models/Document_model.php");
require_once("models/ServiceReceiving_model.php");
require_once("models/ReceivingLine_model.php");
require_once("models/Product_model.php");
require_once("models/Carrier_model.php");
require_once("models/Vendor_model.php");
//Helpers
require_once("helpers/validityStructureArray_helper.php");
require_once("helpers/validityPositiveNumber_helper.php");
require_once("helpers/simpleRequestURI_helper.php");
require_once("helpers/validityNumber_helper.php");
require_once("helpers/sanitizeString_helper.php");
require_once("helpers/formatDateYMD_helper.php");
require_once("helpers/validityDate_helper.php");
require_once("helpers/validityData_helper.php");
require_once("helpers/filterBy_helper.php");
/*
 **************************************************************************************************
 *************************Name: Purchase Order Class Controller
 *************************Description: Se encarga de agregar purchase orders
 **************************************************************************************************
*/
class Receiving{
  private $receivingModel, $receivingDetailModel, $purchaseOrderModel, $freightbillModel, $tandeModel, $truckingNumberModel, $documentModel, $serviceReceivingModel, $receivingLineModel, $productModel, $vendorModel, $carrierModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->receivingModel = new Receiving_model();
    $this->receivingDetailModel = new ReceivingDetail_model();
    $this->purchaseOrderModel = new PurchaseOrder_model();
    $this->freightbillModel = new Freightbill_model();
    $this->tandeModel = new Tande_model();
    $this->truckingNumberModel = new TruckingNumber_model();
    $this->documentModel = new Document_model();
    $this->serviceReceivingModel = new ServiceReceiving_model();
    $this->receivingLineModel = new ReceivingLine_model();
    $this->productModel = new Product_model();
    $this->vendorModel = new Vendor_model();
    $this->carrierModel = new Carrier_model();
  }

  public function getReceiving(){
      $params = $_GET;
      $resultQuery = array();
      $receivingId = (Integer)$params["receivingId"];
      $resultQuery = $this->receivingModel->getById($receivingId);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "receiving" => $resultQuery["query"]
          )
      )));
  }
  /* Get shippings by receiving detail */
  public function getShipping(){
      $params = $_GET;
      $resultQuery = array();
      $receivingdetaildId = (Integer)$params["receivingdetailId"];
      $resultQuery = $this->receivingModel->getByReceivingDetailId($receivingdetaildId);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "shippings" => $resultQuery["query"]
          )
      )));
  }
  /*

  */
  public function getCustomServices(){
      $params = $_GET;
      $resultQuery = array();
      $customerId = (Integer)$params["customerId"];
      $resultQuery = $this->receivingModel->getByCustomer($customerId);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "services" => $resultQuery["query"]
          )
      )));
  }
  /*

  */
  public function getExtraCargos(){
      $params = $_GET;
      $resultQuery = array();
      $receivingId = (Integer)$params["receivingId"];
      $resultQuery = $this->receivingModel->getByreceivingId($receivingId);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "extracargos" => $resultQuery["query"]
          )
      )));
  }
  /*
    Get Receivings distinct
  */
  public function getReceivingsDistinct(){
      $params = $_GET;
      $resultQuery = array();
      $shippingId = (Integer)$params["shippingId"];
      $filter = $params["filter"];
      $resultQuery = $this->receivingModel->getByShippingId($shippingId, $filter);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "receivings" => $resultQuery["query"],
              "receivingsids" => $resultQuery["receivingsid"]
          )
      )));
  }
  /*
   **************************************************************************************************

   **************************************************************************************************
  */
  public function saveReceiving(){

  }

  public function updateReceiving(){
    $params = json_decode(file_get_contents('php://input'), true);
    $receivingDetail = array();
    $purchaseOrder = array();
    $extraCharges = array();
    $freightbill = array();
    $receiving = array();
    $receivingId = 0;
    $document = array();
    $tande = array();
    $receivingLine =  array();
    $product = array();
    $receivingDetailInsert = array();
    $receivingDetailUpdate = array();
    $receivingDetailDelete = array();
    $purchaseOrderInsert = array();
    $purchaseOrderUpdate = array();
    $purchaseOrderDelete = array();
    $extraChargesInsert = array();
    $extraChargesDelete = array();
    $freightbillInsert = array();
    $freightbillUpdate = array();
    $receivingLineInsert = array();
    $receivingLineUpdate = array();
    $receivingLineDelete = array();
    $productInsert = array();
    $productUpdate = array();
    $productDelete = array();
    $freightbillDelete = array();
    $receivingUpdate = array();
    $documentInsert = array();
    $documentDelete = array();
    $tandeInsert = array();
    $tandeUpdate = array();
    $tandeDelete = array();
    $resultQuery = array();

    //Get each parameter
    $receivingUpdate = (array_key_exists("receiving", $params)) ? $params["receiving"] : array();
    $receivingDetail = (array_key_exists("receivingDetail", $params)) ? $params["receivingDetail"] : array();
    $purchaseOrder = (array_key_exists("purchaseOrder", $params)) ? $params["purchaseOrder"] : array();
    $extraCharges = (array_key_exists("extraCargos", $params)) ? $params["extraCargos"] : array();
    $freightbill = (array_key_exists("freightbill", $params)) ? $params["freightbill"] : array();
    $tande = (array_key_exists("tande", $params)) ? $params["tande"] : array();
    $document = (array_key_exists("document", $params)) ? $params["document"] : array();
    $receivingId = (Integer) (array_key_exists("receivingid", $params)) ? $params["receivingid"] : 0;
    $receivingLine = (array_key_exists("receivingLine", $params)) ? $params["receivingLine"] : array();
    $product = (array_key_exists("product", $params)) ? $params["product"] : array();

    //Accept Headers
    header('Content-Type : application/json');
    //Validity status type Integer in Receiving Detail
    if(count($receivingDetail) > 0){
        if(!validityCompleteArray(array(
            "status" => "Integer"
        ), $receivingDetail)){
            echo(json_encode(array(
                'code' => 400,
                'response' => array(
                    'message' => 'Error en el Receiving Detail, revisalo'
                )
            )));
            return;
        }
    }
    //Validity status type Integer in Purchase Order
    if(count($purchaseOrder) > 0){
        if(!validityCompleteArray(array(
          "status" => "Integer"
        ), $purchaseOrder)){
            echo(json_encode(array(
                'code' => 400,
                'response' => array(
                    'message' => 'Error en el Purchase Order, revisalo'
                )
            )));
            return;
        }
    }
    //Validity status type Integer in ExtraCharges
    if(count($extraCharges) > 0){
        if(!validityCompleteArray(array(
          "status" => "Integer"
        ), $extraCharges)){
            echo(json_encode(array(
                'code' => 400,
                'response' => array(
                    'message' => 'Error en el Extra Charges, revisalo'
                )
            )));
            return;
        }
    }
    //Validity status type Integer in Freightbill
    if(count($freightbill) > 0){
        if(!validityCompleteArray(array(
            "status" => "Integer"
        ), $freightbill)){
            echo(json_encode(array(
                'code' => 400,
                'response' => array(
                    'message' => 'Error en el Freightbill, revisalo'
                )
            )));
            return;
        }
    }
    //Validity status type Integer in T and E | Inbonds
    if(count($tande) > 0){
        if(!validityCompleteArray(array(
            "status" => "Integer"
        ), $tande)){
            echo(json_encode(array(
                'code' => 400,
                'response' => array(
                    'message' => 'Error en el T and E, revisalo'
                )
            )));
            return;
        }
    }
    //Validity status type Integer in Documents
    if(count($document) > 0){
        if(!validityCompleteArray(array(
            "status" => "Integer"
        ), $document)){
            echo(json_encode(array(
                'code' => 400,
                'response' => array(
                    'message' => 'Error en el Document, revisalo'
                )
            )));
            return;
        }
    }
    //Validity status type Integer in Receiving Lines
    if(count($receivingLine) > 0){
        if(!validityCompleteArray(array(
            "status" => "Integer"
        ), $receivingLine)){
            echo(json_encode(array(
                'code' => 400,
                'response' => array(
                    'message' => 'Error en el Receiving Line, revisalo'
                )
            )));
            return;
        }
    }
    //Validity status type Integer in Products
    if(count($products) > 0){
        if(!validityCompleteArray(array(
            "status" => "Integer"
        ), $products)){
            echo(json_encode(array(
                'code' => 400,
                'response' => array(
                    'message' => 'Error en el Product, revisalo'
                )
            )));
            return;
        }
    }
    //Validity status
    if(validityStructureArray(array(
      'status'  => 'Integer', $receivingUpdate['status'] == 1))){
        //Validity Data
        if((Integer)$receivingUpdate['status'] != 0){
            if(!validityStructureArray(array(
                'NorthOrSouth'  => 'String',
                'carrier' => 'String',
                'carrierid' => 'UInteger',
                'comments' => 'String',
                'customerid' => 'UInteger',
                'damaged' => 'String',
                'freightbill' => 'String',
                'hazmat' => 'String',
                'newcarrier' => 'Integer',
                'newvendor' => 'Integer',
                'priority' => 'UInteger',
                'receivingid' => 'UInteger',
                'status' => 'UInteger',
                'vendor' => 'String',
                'vendorid' => 'UInteger',
                'insert' => 'UInteger',
                'receivingno' => 'String',
                'user' => 'UInteger'
            ), $receivingUpdate)){
                echo(json_encode(array(
                    'code' => 400,
                    'response' => array(
                        'message' => 'Error en el Receiving, revisalo'
                    )
                )));
                return;
            }
        }
    }



    //Filter data for Receiving Detail, Purchase Order, Freight Bill, T & E, Extra Charges, Documents, Receiving Lines and Products
    list($receivingDetailDelete, $receivingDetailUpdate, $receivingDetailInsert) = array(filterBy($receivingDetail, "status", 0), filterBy($receivingDetail, "status", 1), filterBy($receivingDetail, "status", 2));
    list($purchaseOrderDelete, $purchaseOrderUpdate, $purchaseOrderInsert) = array(filterBy($purchaseOrder, "status", 0), filterBy($purchaseOrder, "status", 1), filterBy($purchaseOrder, "status", 2));
    list($extraChargesDelete, $extraChargesInsert) = array(filterBy($extraCharges, "status", 0), filterBy($extraCharges, "status", 2));
    list($freightbillDelete, $freightbillUpdate, $freightbillInsert) = array(filterBy($freightbill, "status", 0), filterBy($freightbill, "status", 1), filterBy($freightbill, "status", 2));
    list($tandeDelete, $tandeUpdate, $tandeInsert) = array(filterBy($tande, "status", 0), filterBy($tande, "status", 1), filterBy($tande, "status", 2));
    list($documentDelete, $documentInsert) = array(filterBy($document, "status", 0), filterBy($document, "status", 2));
    list($receivingLineDelete, $receivingLineUpdate, $receivingLineInsert) = array(filterBy($receivingLine, "status", 0), filterBy($receivingLine, "status", 1), filterBy($receivingLine, "status", 2));

    $receivingDetailInsertM0 = filterBy($receivingDetailInsert, "mode", 0);
    $receivingDetailInsertM1 = filterBy($receivingDetailInsert, "mode", 1);
    $receivingDetailUpdateM0 = filterBy($receivingDetailUpdate, "mode", 0);
    $receivingDetailUpdateM1 = filterBy($receivingDetailUpdate, "mode", 1);

    //Validation elements of receving details
    if(count($receivingDetail) > 0){
      $isValid = TRUE;
        //validate insert receiving detail | delete
      if(count($receivingDetailDelete) > 0){
        if(!validityCompleteArray(array(
          "receivingdetailid" => "UInteger"
        ), $receivingDetailDelete)){
          $isValid = FALSE;
        }
      }
      //validate insert receiving detail | receipt mode default
      if(count($receivingDetailInsertM0) > 0){
        if(!validityCompleteArray(array(
          "truckingno" => "String",
          "productid" => "UInteger",
          "receivedqtypart" => "UInteger",
          "origen" => "String",
          "originalquantity" => "UInteger",
          "unitid" => "UInteger",
          "weight" => "UFloat",
          "weightunitid" => "UInteger",
          "idLocation" => "UInteger",
          "lotnumber" => "String",
          "newProduct" => "UInteger",
          "product" => "UInteger"
        ), $receivingDetailInsertM0)){
          $isValid = FALSE;
        }
      }
      //validate insert receiving detail | receipt mode pick and pack
      if(count($receivingDetailInsertM1) > 0){
        if(!validityCompleteArray(array(
          "truckingno" => "String",
          "originalquantity" => "UInteger",
          "unitid" => "UInteger",
          "weight" => "UFloat",
          "weightunitid" => "UInteger",
          "idLocation" => "UInteger",
          "po" => "String",
          "podate" => "String",
          "trackinginsert" => "UInteger"
        ), $receivingDetailInsertM1)){
          $isValid = FALSE;
        }
      }

        //validate update receiving detail | receipt mode default
        if(count($receivingDetailUpdateM0) > 0){
          if(!validityCompleteArray(array(
            "receivingdetailid" => "UInteger",
            "truckingid" => "UInteger",
            "truckingno" => "String",
            "originalquantity" => "UInteger",
            "unitid" => "UInteger",
            "weight" => "UFloat",
            "weightunitid" => "UInteger",
            "idLocation" => "UInteger",
            "lotnumber" => "String",
            "receivedqtypart" => "UInteger",
            "productid" => "UInteger",
            "origen" => "String",
            "newProduct" => "UInteger",
            "product" => "UInteger"
          ), $receivingDetailUpdateM0)){
            $isValid = FALSE;
          }
        }
        //validate update receiving detail | receipt mode pick and pack
        if(count($receivingDetailUpdateM1) > 0){
          if(!validityCompleteArray(array(
            "receivingdetailid" => "UInteger",
            "truckingid" => "UInteger",
            "truckingno" => "String",
            "originalquantity" => "UInteger",
            "unitid" => "UInteger",
            "weight" => "UFloat",
            "weightunitid" => "UInteger",
            "po" => "String",
            "podate" => "String",
            "idLocation" => "UInteger"
          ), $receivingDetailUpdateM1)){
            $isValid = FALSE;
          }
        }
        if(!$isValid){
          echo(json_encode(array(
            "code" => 400,
            "message" => "Ingrese correctamente los datos para cada receiving detail porfavor, si percisten los errores recargue la vista porfavor o avise al administrador."
          )));
          return;
        }
    }
    //Validate receiving lines
    if (count($receivingLine) > 0) {
        $isValid = TRUE;
        //validate delete detail
        if (count($receivingLineDelete) > 0) {
            if(!validityCompleteArray(array(
                "receivinglinesid" => "UInteger"
            ), $receivingLineDelete)) {
                $isValid = FALSE;
            }
        }
        //validate update detail
        if (count($receivingLineUpdate) > 0) {
            if(!validityCompleteArray(array(
                "receivingdetailno" => "UInteger",
                "productid" => "UInteger",
                "unitid" => "UInteger",
                "lotnumber" => "String",
                "originalQty" => "UInteger",
                "partQty" => "UInteger",
                "currentQty" => "UInteger",
                "expDate" => "String",
                "po" => "String",
                "podate" => "String"
            ), $receivingLineUpdate)) {
                $isValid = FALSE;
            }
        }
        //validate insert detail
        if (count($receivingLineInsert) > 0) {
            if(!validityCompleteArray(array(
                "receivingdetailno" => "UInteger",
                "productid" => "UInteger",
                "unitid" => "UInteger",
                "lotnumber" => "String",
                "originalQty" => "UInteger",
                "partQty" => "UInteger",
                "currentQty" => "UInteger",
                "expDate" => "String",
                "po" => "String",
                "podate" => "String"
            ), $receivingLineInsert)) {
                $isValid = FALSE;
            }
        }
        if(!$isValid){
          echo(json_encode(array(
            "code" => 400,
            "message" => "Ingrese correctamente los datos para cada Detail porfavor, si percisten los errores recargue la vista porfavor o avise al administrador."
          )));
          return;
        }
    }
    //Validate purchase orders
    if (count($purchaseOrder) > 0) {
        $isValid = TRUE;
        //validate delete purchase order
        if (count($purchaseOrderDelete) > 0) {
            if(!validityCompleteArray(array(
                "poid" => "UInteger"
            ), $purchaseOrderDelete)) {
                $isValid = FALSE;
            }
        }
        //validate update purchase order
        if (count($purchaseOrderUpdate) > 0) {
            if(!validityCompleteArray(array(
                "poid" => "UInteger",
                "receivingdetailid" => "UInteger",
                "po" => "String",
                "poDate" => "String"
            ), $purchaseOrderUpdate)) {
                $isValid = FALSE;
            }
        }
        //validate insert purcha order
        if (count($purchaseOrderInsert) > 0) {
            if(!validityCompleteArray(array(
                "receivingdetailno" => "UInteger",
                "po" => "String",
                "poDate" => "String"
            ), $purchaseOrderInsert)) {
                $isValid = FALSE;
            }
        }
        if(!$isValid){
          echo(json_encode(array(
            "code" => 400,
            "message" => "Ingrese correctamente los datos para cada purchase order porfavor, si percisten los errores recargue la vista porfavor o avise al administrador."
          )));
          return;
        }
    }
    //Validate freightbill
    if (count($freightbill) > 0) {
        $isValid = TRUE;
        //validate delete freightbill
        if (count($freightbillDelete) > 0) {
            if(!validityCompleteArray(array(
                "frid" => "UInteger"
            ), $freightbillDelete)) {
                $isValid = FALSE;
            }
        }
        //validate update freightbill
        if (count($freightbillUpdate) > 0) {
            if(!validityCompleteArray(array(
                "frid" => "UInteger",
                "fr" => "String"
            ), $freightbillUpdate)) {
                $isValid = FALSE;
            }
        }
        //validate insert freightbill
        if (count($freightbillInsert) > 0) {
            if(!validityCompleteArray(array(
                "fr" => "String",
            ), $freightbillInsert)) {
                $isValid = FALSE;
            }
        }
        if(!$isValid){
          echo(json_encode(array(
            "code" => 400,
            "message" => "Ingrese correctamente los datos para cada freightbill porfavor, si percisten los errores recargue la vista porfavor o avise al administrador."
          )));
          return;
        }
    }
    //Validate T and E Inbonds
    if (count($tande) > 0) {
        $isValid = TRUE;
        //validate delete t and e
        if (count($tandeDelete) > 0) {
            if(!validityCompleteArray(array(
                "tandeid" => "UInteger"
            ), $tandeDelete)) {
                $isValid = FALSE;
            }
        }
        //validate update t and e
        if (count($tandeUpdate) > 0) {
            if(!validityCompleteArray(array(
                "tandeid" => "UInteger",
                "tande" => "String",
                "tandeexpires" => "String"
            ), $tandeUpdate)) {
                $isValid = FALSE;
            }
        }
        //validate insert t and e
        if (count($tandeInsert) > 0) {
            if(!validityCompleteArray(array(
                "tande" => "String",
                "tandeexpires" => "String"
            ), $tandeInsert)) {
                $isValid = FALSE;
            }
        }
        if(!$isValid){
          echo(json_encode(array(
            "code" => 400,
            "message" => "Ingrese correctamente los datos para cada T and E Inbonds porfavor, si percisten los errores recargue la vista porfavor o avise al administrador."
          )));
          return;
        }
    }
    //Insert new receiving
    if ($receivingId == '') {
        $created = $receivingUpdate['user'];
        $receivingInsert = $this->receivingModel->insertreceiving($created);
        $receivingId = $receivingInsert["receivingid"];
    }
    //Map for Insert Extra Charges
    $extraChargesInsert = array_map(function($extraCharge) use($receivingId){
      return array(
        'serviceid' => $extraCharge['serviceid'],
        'cost' => $extraCharge['cost'],
        'information' => $extraCharge['information'],
        'receivingid' => $receivingId
      );
    }, $extraChargesInsert);

    //Delete Receiving Detail, Purchase Order, Freight Bill, T & E, Extra Charges, Documents, Receiving Lines
    $this->truckingNumberModel->deleteByReceivingDetailId($receivingDetailDelete);
    $this->receivingLineModel->deleteByReceivingDetailId($receivingDetailDelete);
    $this->receivingDetailModel->delete($receivingDetailDelete, $receivingId);
    $this->purchaseOrderModel->delete($purchaseOrderDelete, $receivingId);
    $this->freightbillModel->delete($freightbillDelete, $receivingId);
    $this->tandeModel->delete($tandeDelete, $receivingId);
    $this->deleteDocuments($documentDelete, 'receiving', 'receivingid', $receivingId);
    $this->serviceReceivingModel->deleteAll($extraChargesDelete);
    $this->receivingLineModel->delete($receivingLineDelete);

    //Insert Receiving Detail, Purchase Order, Freight Bill, T & E, Extra Charges, Documents, Receiving Lines
    $products = $this->productModel->save($product);
    $this->receivingDetailModel->save($receivingDetailInsertM0, $receivingId, $this->truckingNumberModel, $products["last_id"]);
    $this->receivingDetailModel->save($receivingDetailInsertM1, $receivingId, $this->truckingNumberModel, $products["last_id"]);
    $resultQuery = $this->receivingDetailModel->find(array('receivingdetailid'), $receivingId);
    $receivingDetails = $resultQuery['query'];
    $this->purchaseOrderModel->save($purchaseOrderInsert, $receivingId, $receivingDetails);
    $this->freightbillModel->save($freightbillInsert, $receivingId);
    $this->tandeModel->save($tandeInsert, $receivingId);
    $this->documentModel->save($this->uploadDocuments($documentInsert, 'receiving', 'receivingid', $receivingId), $receivingId);
    $this->serviceReceivingModel->saveAll('null', $extraChargesInsert);
    $this->receivingLineModel->save($receivingLineInsert, $receivingDetails, $products['last_id']);

    //Generate receivingno
    if ((Integer)$receivingUpdate['insert'] != 0) {
        $receivingnoYear = date('Y');
        $receivingnoMonth = date('m');
        $receivingnoDay = date('d');
        //year // month // day // receivingid
        $receivingno = (String)$receivingnoYear . (String)$receivingnoMonth . (String)$receivingnoDay . (String)$receivingId;
        $receivingUpdate['receivingno'] = $receivingno;
    }
    //Update Receiving, Receiving Detail, Purchase Order, Freightbill, T & E, Extra Charges, Documents, Receiving Lines
    if((Integer)$receivingUpdate['status'] != 0){
      if((Integer)$receivingUpdate['newvendor'] == 1){
        $resultQuery = $this->vendorModel->getByName(sanitizeString($receivingUpdate['vendor']));
        $vendor = $resultQuery['query'];
        if(count($vendor) > 0){
          $receivingUpdate['vendorid'] = $vendor['vendorid'];
        }else{
          $resultQuery = $this->vendorModel->saveSimple(sanitizeString($receivingUpdate['vendor']));
          $receivingUpdate['vendorid'] = $resultQuery['last_id'];
        }
      }
      if((Integer)$receivingUpdate['newcarrier'] == 1){
        $resultQuery = $this->carrierModel->getByName(sanitizeString($receivingUpdate['carrier']));
        $carrier = $resultQuery['query'];
        if(count($carrier) > 0){
          $receivingUpdate['carrierid'] = $carrier['carrierid'];
        }else{
          $resultQuery = $this->carrierModel->saveSimple(sanitizeString($receivingUpdate['carrier']));
          $receivingUpdate['carrierid'] = $resultQuery['last_id'];
        }
      }
      $this->receivingModel->update(array("customerid", "vendorid", "carrierid", "freightbill", "comments", "priority", "NorthOrSouth", "hazmat", "damaged", "receivingno"), $receivingUpdate, $receivingId);
    }

    $this->receivingDetailModel->updateMain(array("truckingid", "originalquantity", "unitid", "weight", "weightunitid", "idLocation", "lotnumber", "qtypart", "productid", "origen"), $receivingDetailUpdateM0, $receivingId, $this->truckingNumberModel, $products["last_id"]);
    $this->receivingDetailModel->updateMain(array("truckingid", "originalquantity", "unitid", "weight", "weightunitid", "po", "podate", "idLocation"), $receivingDetailUpdateM1, $receivingId, $this->truckingNumberModel);

    $this->purchaseOrderModel->updateMain(array("po", "poDate", "receivingdetailid"), $purchaseOrderUpdate, $receivingDetails);
    $this->freightbillModel->updateMain(array("fr"), $freightbillUpdate);
    $this->tandeModel->updateMain(array("tande", "tandeexpires"), $tandeUpdate);
    $this->receivingLineModel->updateMain(array("productid", "unitid", "lotnumber", "originalQty", "partQty", "currentQty", "expDate", "po", "podate"), $receivingLineUpdate, $receivingDetails, $products["last_id"]);

    //Response
    echo(json_encode(array(
      'code' => 200,
      'response' => array(
        'receivingId' => $receivingId,
        'receivingno' => $receivingUpdate['receivingno']
      )
    )));
  }

  private function uploadDocuments($files, $type, $type_key, $id){
    $documents = array();
    date_default_timezone_set('UTC');
    foreach($files as $key=>$fileInfo){
      $data = explode(",", $fileInfo["fileBase64"]);
      $data = $data[1];
      $ext = explode('.', $fileInfo["name"]);
      $ext = $ext[1];
      $nameFile = date('Ymdhisu');
      $path = "/home/kawatron/Images/$type/$id/";
      if (!is_dir($path)) {
          mkdir($path, 777, TRUE);
      }
      $file = fopen($path.$nameFile.'.tmp', "wb");
      fwrite( $file, base64_decode($data) );
      fclose( $file);
      array_push($documents, array(
        'type' => $type,
        'type_recid' => $id,
        'type_key' => $type_key,
        'description' => 'Upload File',
        'name' => "$nameFile.tmp",
        'file_path' => $path,
        'original_name' => $fileInfo["name"],
        'mime_type' => $fileInfo["type"]
      ));
      sleep(1);
    }
    return $documents;
  }

  private function deleteDocuments($files, $type, $receiving, $receivingId){
    $documents = array();
    if(count($files) > 0 ){
      foreach($files as $key=>$file){
        array_push($documents, $file["documentid"]);
      }
      $documents = $this->documentModel->getByIn($documents, $type, $receiving, $receivingId);
      $documents = $documents['query'];
      foreach($documents as $file){
        if(file_exists($file['file_path'] . $file['name'])){
          unlink($file['file_path'] . $file['name']);
        }
      }
      return $this->documentModel->delete($files, $type, $receiving, $receivingId);
    }else{
      return false;
    }

  }


  public function getDocuments(){
    $params = $_GET;
    $resultQuery = array();
    $receivingId = (Integer)$params["receivingId"];
    $resultQuery = $this->documentModel->getByReceivingId('receiving', 'receivingid', $receivingId);
    header("content-type: application/json");
    echo(json_encode(array(
        "code" => 200,
        "response" => array(
            "documents" => $resultQuery["query"]
        )
    )));
  }

public function viewOrDownloadDocument(){
    $params = $_GET;
    $resultQuery = array();
    $document = array();
    $documentId = (Integer)$params['documentId'];
    $resultQuery = $this->documentModel->getById($documentId);
    $document = $resultQuery['query'];
    header('Content-Type: application/json');
    if(is_null($document) || count($document) == 0){
        echo(json_encode(array(
          'code' => 404
        )));
        return;
    }
    $systemFile = $document['file_path'] . $document['name'];
    $temporalFile = $document['file_path'] . $document['original_name'];
    $mimeTypeFile = $document['mime_type'];
    copy($systemFile, $temporalFile);
    header('Content-Type: ' . $mimeTypeFile);
    if (!($mimeTypeFile == 'image/jpeg' || $mimeTypeFile == 'image/png' || $mimeTypeFile == 'image/gif' || $mimeTypeFile == 'image/bmp' || $mimeTypeFile == 'application/pdf')) {
        header('Content-Disposition: attachment; filename="' . $document['original_name'] . '"');
    }
    header('Cache-Control: max-age=0');
    readfile($temporalFile);
    if (strcmp($document['description'], 'Upload File') == 0) {
        unlink($temporalFile);
    }
}

  public function getReceivingSearchData(){
    $resultQuery = $this->carrierModel->getByOptions();
    $carriers = $resultQuery['query'];
    $resultQuery = $this->vendorModel->getByOptions();
    $vendors = $resultQuery['query'];
    $resultQuery = $this->productModel->get();
    $products = $resultQuery['query'];
    header('Content-Type: application/json');
    echo(json_encode(array(
      'code' => 200,
      'response' => array(
        'carriers' => $carriers,
        'vendors' => $vendors,
        'products' => $products
      )
    )));
  }

  public function searchReceiving(){
    $params     =               array();
    $params     =                 $_GET;
    //Se da formato a las fechas
    $isValid = TRUE;
    if(validityStructureArray(array(
      'receivingNumber'  => 'UInteger',
      'internalNumber' => 'String',
      'freightbill' => 'String',
      'truckingNumber' => 'String',
      'inbond' => 'String',
      'customerId' => 'UInteger',
      'purchaseOrder' => 'String',
      'vendorId' => 'UInteger',
      'carrierId' => 'UInteger',
      'bound' => 'UInteger',
      'vendorId' => 'UInteger',
      'isShipped' => 'UInteger',
      'priority' => 'UInteger',
      'productId' => 'UInteger',
      'dateFrom' => 'Date',
      'dateTo' => 'Date',
      'numberPage' => 'UInteger'
    ), $params)){
      $numberPage = (Integer)$params['numberPage'];
      $params['dateFrom'] = ( strcmp($params['dateFrom'], '') != 0 ) ? formatDateYMD($params['dateFrom']) : $params['dateFrom'];
      $params['dateTo']   =       ( strcmp($params['dateTo'], '') != 0 ) ? formatDateYMD($params['dateTo']) : $params['dateTo'];
    }else{
      $isValid = FALSE;
    }

    // header('content-type: application/json');
    if(!$isValid || $numberPage <= 0){
      echo(json_encode(array(
        'code'     => 404
      )));
      return;
    }

    // Se llama a la funcion del modelo que realiza la busqueda de facturas por un determinado filtro
    $resultQuery = $this->receivingModel->searchReceivingBy($numberPage, $params, null);
    //Si la consulta arroja resultados, se regresan todas las facturas con su informacion de paginacion correspondiente
    if( count($resultQuery["query"]) > 0 ){
      echo(json_encode(array(
        "code"     =>   200,
        "response" => array(
          "numberPages"   =>   $resultQuery["numberPages"],
          "numberEntries" => $resultQuery["numberEntries"],
          "receivings"      =>         $resultQuery["query"],
          "nextPage"      =>               $numberPage + 1,
          "previousPage"  =>               $numberPage - 1,
          "numberPage" => $numberPage
        )
      )));
      return;
    }else{
      echo(json_encode(array(
        "code"     =>   404
      )));
    }
  }
}
?>

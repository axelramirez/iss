<?php
//Models
require_once(          "models/Dock_model.php");
/*
 **************************************************************************************************
 *************************Name: Purchase Order Class Controller
 *************************Description: Se encarga de agregar purchase orders
 **************************************************************************************************
*/
class Dock{
  private           $dockModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->dockModel = new Dock_model();
  }
  /*
   **************************************************************************************************

   **************************************************************************************************
  */
  public function insertDock(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
     $dockName = $params["dockName"];
     if($this->dockModel->insert($dockName)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $dockName
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function updateDock(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $dockId = $params["dockId"];
      $dockName = $params["dockName"];
     if($this->dockModel->update($dockName, $dockId)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $dockName
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function getDocks(){
      $params = $_GET;
      $resultQuery = array();
      $numberPage = (Integer)$params["page"];
      $resultQuery = $this->dockModel->get($numberPage);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "docks" => $resultQuery["query"],
              "numberEntries" => $resultQuery["numberEntries"],
              "numberPages" => $resultQuery["numberPages"]
          )
      )));
  }
}
?>

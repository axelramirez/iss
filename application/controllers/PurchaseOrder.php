<?php
//Models
require_once(          "models/PurchaseOrder_model.php");
/*
 **************************************************************************************************
 *************************Name: Purchase Order Class Controller
 *************************Description: Se encarga de agregar purchase orders
 **************************************************************************************************
*/
class PurchaseOrder{
  private           $purchaseOrderModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->purchaseOrderModel = new PurchaseOrder_model();
  }
  /*
   **************************************************************************************************

   **************************************************************************************************
  */
  public function insertPO(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $receivingId = $params["receivingId"];
      $npos = $params["newPurchasesOrders"];
     if($this->purchaseOrderModel->insert($npos, $receivingId)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $npos
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function updatePO(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $receivingId = $params["receivingId"];
      $pso = $params["purchasesOrder"];
     if($this->purchaseOrderModel->update($pso, $receivingId)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $pso
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function getPO(){
      $params = $_GET;
      $resultQuery = array();
      $receivingId = (Integer)$params["receivingId"];
      $resultQuery = $this->purchaseOrderModel->getByReceivingId($receivingId);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "purchaseOrders" => $resultQuery["query"]
          )
      )));
  }
  public function deletePO(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $purchaseOrderId = (Integer)$params["purchaseOrder"]["purchaseOrderId"];
     if($this->purchaseOrderModel->delete($purchaseOrderId)){
         echo(json_encode(array(
             "code" => 200
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
}
?>

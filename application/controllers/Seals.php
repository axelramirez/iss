<?php
require_once(          "models/Seals_model.php");

class Seals{
    private $sealsModel;

    public function __construct(){
        $this->sealsModel = new Seals_model();
    }
    public function getSeals(){
        $params = $_GET;
        $resultQuery = array();
        $shippingId = (Integer)$params["idShipping"];
        $customerId = (Integer)$params["customerId"];
        $resultQuery = $this->sealsModel->get($shippingId,$customerId);
        header("content-type: application/json");
        echo(json_encode(array(
            "code" => 200,
            "response" => array(
                "customer" => $resultQuery["query"],
                "internal" => $resultQuery["internal"]
            )
        )));
    }
}
?>

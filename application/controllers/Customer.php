<?php
//Models
require_once(              "models/Tax_model.php");
require_once(             "models/Unit_model.php");
require_once(          "models/Invoice_model.php");
require_once(         "models/Shipping_model.php");
require_once(         "models/Customer_model.php");
require_once(        "models/Receiving_model.php");
require_once(      "models/InvoiceList_model.php");
require_once(      "models/AutoInvoice_model.php");
require_once(     "models/statusAmount_model.php");
require_once(     "models/UnitCustomer_model.php");
require_once(    "models/ReceivingLine_model.php");
require_once(   "models/ShippingDetail_model.php");
require_once(  "models/ServiceShipping_model.php");
require_once(  "models/ReceivingDetail_model.php");
require_once( "models/ReceivingShipped_model.php");
require_once("models/statusAutoInvoice_model.php");
require_once("models/PreCreateShipping_model.php");
//Helpers
require_once(    "helpers/daysElapsed_helper.php");
require_once(    "helpers/daysStorage_helper.php");
require_once(     "helpers/formatDate_helper.php");
require_once(     "helpers/arrayByKey_helper.php");
/*
 **************************************************************************************************
 *************************Name: Customer Class Controller
 *************************Description: Permite verificar el credito de un cliente y lo que debe
 **************************************************************************************************
*/
class Customer{
  private               $taxModel;
  private              $unitModel;
  private           $invoiceModel;
  private          $shippingModel;
  private          $customerModel;
  private         $receivingModel;
  private       $invoiceListModel;
  private       $autoInvoiceModel;
  private      $statusAmountModel;
  private      $unitCustomerModel;
  private     $receivingLineModel;
  private    $shippingDetailModel;
  private   $serviceShippingModel;
  private   $receivingDetailModel;
  private  $receivingShippedModel;
  private $statusAutoInvoiceModel;
  private $preCreateShippingModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->taxModel               = new               Tax_model();
    $this->unitModel              = new              Unit_model();
    $this->invoiceModel           = new           Invoice_model();
    $this->shippingModel          = new          Shipping_model();
    $this->customerModel          = new          Customer_model();
    $this->receivingModel         = new         Receiving_model();
    $this->invoiceListModel       = new       InvoiceList_model();
    $this->autoInvoiceModel       = new       AutoInvoice_model();
    $this->statusAmountModel      = new      StatusAmount_model();
    $this->unitCustomerModel      = new      UnitCustomer_model();
    $this->receivingLineModel     = new     ReceivingLine_model();
    $this->shippingDetailModel    = new    ShippingDetail_model();
    $this->receivingDetailModel   = new   ReceivingDetail_model();
    $this->serviceShippingModel   = new   ServiceShipping_model();
    $this->receivingShippedModel  = new  ReceivingShipped_model();
    $this->statusAutoInvoiceModel = new StatusAutoInvoice_model();
    $this->preCreateShippingModel = new PreCreateShipping_model();
  }
  /*
   **************************************************************************************************
   Esta funcion obtiene todos los clientes del sistema
   **************************************************************************************************
  */
  public function getCustomers(){
    $resultQuery = $this->customerModel->get();
    header("content-type: application/json");
    if($resultQuery["status"]){
     echo json_encode(array(
       "code"     =>   200,
       "response" => array(
         "customers"   =>   $resultQuery["query"]->fetch_all(MYSQLI_ASSOC)
       )
     ));
     return;
   }
   echo json_encode(array(
     "code"     =>   500
   ));
  }
  /*
   **************************************************************************************************
   Esta funcion obtiene todos los datos de los clientes
   **************************************************************************************************
  */
  public function getCustomerAll(){
      $params = $_GET;
      $resultQuery = array();
      $numberPage = (Integer)$params["page"];
      $resultQuery = $this->customerModel->getbypagination($numberPage);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "customers" => $resultQuery["query"],
              "numberEntries" => $resultQuery["numberEntries"],
              "numberPages" => $resultQuery["numberPages"]
          )
      )));
  }
  /*
   **************************************************************************************************
   Esta funcion obtiene la busqueda de clientes
   **************************************************************************************************
  */
  public function searchCustomer(){
      $params = $_GET;
      $resultQuery = array();
      $numberPage = (Integer)$params["page"];
      $filter = $params["filter"];
      $resultQuery = $this->customerModel->searchbypagination($numberPage, $filter);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "customers" => $resultQuery["query"],
              "numberEntries" => $resultQuery["numberEntries"],
              "numberPages" => $resultQuery["numberPages"]
          )
      )));
  }
  /*
   **************************************************************************************************
   Esta funcion inserta nuevos clientes
   **************************************************************************************************
  */
  public function insertCustomerAll(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $data = $params["insertCustomer"];
     if($this->customerModel->insertCustomer($data)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $data
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  /*
   **************************************************************************************************
   Esta funcion actualiza un cliente
   **************************************************************************************************
  */
  public function updateCustomer(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $customerId = $params["customerId"];
      $data = $params["updateCustomer"];
     if($this->customerModel->updateCustomer($data, $customerId)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $data
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  /*
   **************************************************************************************************
   Esta funcion obtiene todos los productos por cliente
   **************************************************************************************************
  */
  public function getProducts(){
      $params = $_GET;
      $resultQuery = array();
      $customerId = (Integer)$params["customerId"];
      $resultQuery = $this->customerModel->getByCustomerId($customerId);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "products" => $resultQuery["query"]
          )
      )));
  }
  /*
   **************************************************************************************************
   Esta funcion obtiene un cliente por su Id
   **************************************************************************************************
  */
  public function getCustomer($id){
    $resultQuery = $this->customerModel->getById($id);
    if($resultQuery["status"]){
      return $resultQuery["query"]->fetch_assoc();
    }
    print_r($resultQuery["message"]);
    return null;
  }
  /*
   **************************************************************************************************
   Esta funcion obtiene un cliente por su Id
   **************************************************************************************************
  */
  public function getCustomerBy(){
    $params = $_GET;
    $resultQuery = array();
    $customerId = (Integer)$params["customerId"];
    $resultQuery = $this->customerModel->getById($customerId);
    header("content-type: application/json");
    echo(json_encode(array(
        "code" => 200,
        "response" => array(
            "customers" => $resultQuery["query"]
        )
    )));
  }
  /*
   **************************************************************************************************
   Esta funcion obtiene todos los clientes excepto el cliente con ese id
   **************************************************************************************************
  */
  public function getCustomersExcept($id){
    $resultQuery = $this->customerModel->getWithoutId($id);
    if($resultQuery["status"])
      return $resultQuery["query"]->fetch_all(MYSQLI_ASSOC);
    print_r($resultQuery["message"]);
    return null;
  }
  /*
   **************************************************************************************************
   Esta funcion obtiene todos los clientes que aun pueden realizar una salida excepto el cliente del id
   **************************************************************************************************
  */
  public function getCustomersExceptBill(){
    $params = $_GET;
    $resultQuery = array();
    $idCustomer = (Integer)$params["idCustomer"];
    $resultQuery = $this->customerModel->getCustomersExcept($idCustomer);
    header("content-type: application/json");
    echo(json_encode(array(
        "code" => 200,
        "response" => array(
            "customers" => $resultQuery["query"]
        )
    )));
  }
  public function getCustomersAllowExcept($id, $billId){
    $resultQuery = $this->statusAmountModel->getAllowClientExcept($id, $billId);
    if($resultQuery["status"])
      return $resultQuery["query"]->fetch_all(MYSQLI_ASSOC);
    print_r($resultQuery["message"]);
    return null;
  }
  /*
   **************************************************************************************************
   Esta funcion verifica si el cliente cuenta con suficiente credito para generar una nueva salida
   con su factura o actualiza el total de una factura ya existente
   **************************************************************************************************
  */
  public function createOrUpdateInvoice($shippingId, $isCharge){
   $tax                =    NULL;       //Almacena el impuesto
   $units              = array();       //Almacena la descripcion de las unidades de mercancias que se manejan en el sistema
   $total              =       0;       //Almacena el costo total de la nueva salida
   $details            = array();       //Almacena todos los elemrntos de preCreateShipping que sean details
   $invoice            = array();       //Almacena la informacion de la consulta que encuentra el invoice para un shipping
   $shipping           =    NULL;       //Almacena la informacion de la salida
   $customer           =    NULL;       //Almacena la informacion del cliente al que pertenece la salida
   $resultQuery        =    NULL;       //Almacena las consultas a los modelos
   $detailLines        = array();       //Almacena todos los elemrntos de preCreateShipping que sean detailLines
   $invoicelists       = array();       //Almacena los items de invoicelist
   $unitsCustomer      = array();       //Almacena el costo de CPD y Handling de todos las unidades de mercancia del cliente
   $servicesShipping   = array();       //Almacena la consulta de los cargos extra de la salida
   $receivingDetails   =    NULL;       //Almacena todos los id de receivingdetail como indices y su estado (detail o line)
   $preCreateShippings =    NULL;       //Almacena todos preCreateShipping para la salida
   //Se obtiene el invoice correspondiente a la salida en caso de existir
   $invoice = $this->invoiceModel->getByShippingId($shippingId);
   //Obtiene el impuesto que se aplicara a la factura
   $resultQuery = $this->taxModel->get();
   $tax = $resultQuery["query"]->fetch_assoc();
   //Obtiene todas las preCreateShipping de un shipping
   $resultQuery = $this->preCreateShippingModel->getByShippingId($shippingId);
   $preCreateShippings = $resultQuery["query"]->fetch_all(MYSQLI_ASSOC);
   //Obtiene la informacion de una salida
   $resultQuery = $this->shippingModel->getById($shippingId);
   $shipping = $resultQuery["query"]->fetch_assoc();
   //Obtiene la informacion del cliente al que le pertenece esa salida
   $resultQuery = $this->customerModel->getById($shipping["customerid"]);
   $customer = $resultQuery["query"]->fetch_assoc();
   //Obtiene el costo de CPD y Handling de mercancia de cada unidad que maneja el cliente y almacena un array asociativo a partir de las unitid
   $resultQuery = $this->unitCustomerModel->getById($customer["customerid"]);
   $unitsCustomer = $resultQuery["query"]->fetch_all(MYSQLI_ASSOC);
   $unitsCustomer = arrayByKey("unitid", $unitsCustomer);
   //Obtiene las unidades que se manejan en el sistema (descripciones) y almacena un array asociativo a partir de las unitid
   $resultQuery = $this->unitModel->get();
   $units = $resultQuery["query"]->fetch_all(MYSQLI_ASSOC);
   $units = arrayByKey("unitid", $units);
   //Obtiene los extra cargos que el cliente coloco en la salida que se aplicaran a la factura, y almacena en invoicelist los que se encuentren y suma al total el costo de esos extra cargos
   $resultQuery = $this->serviceShippingModel->getByShippingId($shippingId);
   $servicesShipping = $resultQuery["query"]->fetch_all(MYSQLI_ASSOC);
   if( count($servicesShipping) > 0 ){
     foreach( $servicesShipping as $serviceShipping ){
       $total += $serviceShipping["cost"];
     }
   }
   //Itera todos los elementos en preCreateShippings y almacena los que son lineas en el arreglo detailLines
   if( count($preCreateShippings) > 0 ){
     foreach( $preCreateShippings as $element ){
       //Verifica si el elemento tiene como estado "line", si si lo tiene almacena el receivingdetailid en el arreglo detailLines
       switch($element["state"]){
         case "line":
         if( !in_array($element["receivingdetailid"], $detailLines) ){
           array_push($detailLines, $element["receivingdetailid"]);
         }
         break;
         case "detail":
         if( !in_array($element["receivingdetailid"], $details) ){
           array_push($details, $element["receivingdetailid"]);
         }
         break;
       }
     }
   }
   //Se verifica que los detalles cuenten con elementos y se almacenan en el arreglo recevingDetails si son solo detalles o lineas
   if( count($details) == 0 ){
     foreach($detailsLines as $detailline){
       $receivingDetails[$detailline] =   "line";
     }
   }else{
     foreach( $details      as     $detail ){
       if( in_array($detail, $detailLines) )
         $receivingDetails[$detail]   =   "line";
       else
         $receivingDetails[$detail]   = "detail";
     }
   }
   //Verifica que receivingDetail cuente con elementos y si si cuenta lo itera y almacena en un arreglo los invoicelists de handling, CPD
   if( count($receivingDetails) > 0 ){
     //Se iteran todos los receiving elements
     foreach( $preCreateShippings as $element ){
       if( array_key_exists($element['receivingdetailid'], $receivingDetails) ){
         if( (strcmp($element["state"], "detail") == 0 && $receivingDetails[$element["receivingdetailid"]] == "detail") || (strcmp($element["state"], "line") == 0 && $receivingDetails[$element["receivingdetailid"]] == "line") ){
           $days = 0;       //Almacena la cantidad de dias de almacenaje y los transcurridos
           $invoicelist = array(
             "qty"              =>                                                             $element["quantity"],
             "description"      => $units[$element['unitid']]." $element[productDescription] Storage: $days Day(s)",
             "receivingid"      =>                                                          $element["receivingid"],
             "receivingno"      =>                                                          $element["receivingno"],
             "unitprice"        =>                                                                               -1,
             "total"            =>                                                                               -1,
             "extra"            =>                                                                                2,
             "shippingdetailid" =>                                                     $element["shippingdetailid"]
           );               //Almacena el invoicelist que se almacenera en el arreglo de invoicelists para posteriormente insertarla en la base de datos
           //Si no hay localStorage es porque no se ha cobrado el almacenaje, y se aplicara el freeday del cliente
           if( !$isCharge ){
             if( is_null($element["lastStorageDate"]) ){
               $days =  daysStorage(formatDate($element["date"]), $customer["freedays"]);
             }else{
               $days = daysElapsed($element["lastStorageDate"], $shipping["shippdate"]);
             }
           }
           //CPD
           //Si el CPD del unitcustomer no es nulo le asigna un determinado valor a unitprice (CPD (Costo por dia de almacenamiento)) y total del invoicelist
           if( !is_null( $CPD = $unitsCustomer[$element["unitid"]]["CPD"]) ){
             $invoicelist["unitprice"] =                               $CPD;
             $invoicelist["total"]     = $invoicelist["qty"] * $days * $CPD;
             $total                   += $invoicelist["qty"] * $days * $CPD;
           }
           //Se almacena invoicelist con precio de CPD en invoicelists
           array_push($invoicelists, $invoicelist);
           //Handling
           //Se asigna una nueva descripcion para handling y un nuevo valor en extra
           $invoicelist["description"] = $units[$element['unitid']]." $element[productDescription] Handling";
           $invoicelist["extra"]       =                                                                   3;
           //Si el Handling del unitcustomer no es nulo le asigna un determinado valor a unitprice (CPD (Costo por dia de almacenamiento)) y total del invoicelist
           if( !is_null( $handling = $unitsCustomer[$element["unitid"]]["handling"]) ){
             $invoicelist["unitprice"] =                          $handling;
             $invoicelist["total"]     =    $invoicelist["qty"] * $handling;
             $total                   +=    $invoicelist["qty"] * $handling;
           }
           //Se almacena invoicelist con precio de Handling en invoicelists
           array_push($invoicelists, $invoicelist);
           //Hazmat, Southbound o Northbound
           //Suma al total el costo de hazmat, south o north que el cliente tiene asignado
           if( strcmp($element["hazmat"], "yes") == 0 ){
             $total += $customer["hazmat"];
           }
           if( strcmp($element["NorthOrSouth"], "Northbound") == 0 ){
             $total += $customer["north"];
           }else{
             $total += $customer["south"];
           }
         }
       }
     }
     //Verificacion del monto del cliente
     switch($invoice["status"]){
       case TRUE:
       //Esta variable almacena el total de la factura
       $ptotal =                                 $total;
       //Esta variable almacena la diferencia entre el subtotal de la factura almacenada y el total calculado para la misma factura
       $total  = $total - $invoice["query"]["subtotal"];
       //Si el monto no alcanza para el total de dinero que cuesta la salida de toda la mercancia, no crea la factura
       if( !$this->checkAmount($customer["customerid"], $total) ){
         echo "No fue posible realizar la salida.";
         return FALSE;
       }
       //Si si cuenta con suficiente saldo se actualiza el total de la factura nuevo
       $this->invoiceModel->update(array(
         "invoiceid" => $invoice["query"]["invoiceid"],
         "subtotal"  =>                        $ptotal
       ));
       //Este metodo llama al metodo del modelo customer que permite actualizar el credito ocupado del cliente
       $this->customerModel->updateDueAmount($customer["customerid"], $total);
       //Elimina todos los list de factura para posteriormente agregar nuevos
       $this->invoiceListModel->deleteByInvoiceId($invoice["query"]["invoiceid"]);
       //Se crean los list de factura
       $this->invoiceListModel->saveAll($invoice["query"]["invoiceid"], $invoicelists);
       break;
       case FALSE:
       //Si el monto no alcanza para el total de dinero que cuesta la salida de toda la mercancia, no crea la factura
       if( !$this->checkAmount($customer["customerid"], $total) || $total == 0 ){
         echo "No fue posible realizar la salida.";
         return FALSE;
       }
       //Si si cuenta con suficiente saldo crea la factura
       $resultQuery = $this->invoiceModel->save(array(
         "shippingid" => $shippingId,
         "total"      =>      $total,
         "tax"        => $tax["tax"]
       ));
       //Este metodo llama al metodo del modelo customer que permite actualizar el credito ocupado del cliente
       $this->customerModel->updateDueAmount($customer["customerid"], $total);
       //Se crean los list de factura
       $this->invoiceListModel->saveAll($resultQuery["invoiceid"], $invoicelists);
       break;
     }
     echo "Si fue posible realizar la salida.";
     return TRUE;
   }
   echo "No fue posible realizar la salida.";
   return FALSE;
  }
  /*
   **************************************************************************************************
   Esta funcion verifica si el cliente cuenta con credito suficiente disponible en el sistema para
   solicitar una nueva salida y verifica que el monto de lo que se realizara la salida sea igual o
   menor al credito que el cliente tiene disponible
   **************************************************************************************************
  */
  private function checkAmount($customerId, $amount){
   //Nota: Se recibiran las variables en el arreglo $_GET, $_POST o $_REQUEST de PHP
   $resultQuery    = array();
   $customerAmount = array();
   //Se obtiene el estado de cuenta del cliente
   $resultQuery = $this->statusAmountModel->getById($customerId);
   //Se verifica que la consulta hay tenido exito
   if( $resultQuery["status"] ){
     //Se declara una variable temporal para recibir todos los registros de la consulta
     $customerAmount = $resultQuery["query"]->fetch_assoc();
     //Se verifica que el cliente cuenta con sufiente saldo para recibir tal monto
     if( $customerAmount['isAllow'] && $amount <= $customerAmount['current_amount'] ){
       return TRUE;
     }else{
       return FALSE;
     }
   }else{
     //Se imprime el mensaje de error
     print_r($resultQuery["message"]);
     return FALSE;
   }
  }
  /*
   **************************************************************************************************
   Esta funcion verifica que una factura aun puede recibir pagos (solo si no esta pagada o cancelada)
   y varifica que el monto que se desea pagar es exactamente igual o menor al monto que se debe de la
   factura
   **************************************************************************************************
  */
  private function checkAutoInvoiceAmount(){
   //Nota: Se recibiran las variables en el arreglo $_GET, $_POST o $_REQUEST de PHP
   $resultQuery         = array();
   $customerAutoInvoice = array();
   $amount              =     100;
   $customerId          =      37;
   $autoInvoiceId       =       1;
   //Se obtiene el estado de la factura del cliente
   $resultQuery = $this->statusAutoInvoiceModel->getByAutoInvoiceClient($autoInvoiceId, $customerId);
   if( $resultQuery["status"] ){
     $customerAutoInvoice = $resultQuery["query"]->fetch_assoc();
     //Se verifica que el cliente cuenta con sufiente saldo para recibir tal monto
     if( $customerAutoInvoice['isAllow'] && $amount <= $customerAutoInvoice['dueInvoice'] ){
       echo "Si se pagara la factura.";
     }else{
       echo "No se pago la factura: la factura esta pagada, cancelada o el monto que quiere pagar es mayor al que se debe de la factura.";
     }
   }else{
     //Se imprime el mensaje de error
     print_r($resultQuery["message"]);
   }
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de verificar si un cliente cuenta con suficiente saldo para realizar una
   nueva salida de mercancia
   **************************************************************************************************
  */
  public function createOrUpdate($shippingId, $isCharge, $receivingQuantities){
    //Declaracion de variables y arreglos a utilizar
    $resultQuery             = array();        //Almacena las consultas a los modelos
    $shippingDetails         = array();        //Almacena la consulta de los shipping details de un shipping
    $receivingsLines         = array();        //Almacena los cambios que se realizaran a los receiving lines
    $shippingsDetails        = array();        //Almacena los cambios que se realizaran a los shipping details de un shipping
    $updateReceivings        =   FALSE;        //Almacena un boleano que indica si se realizaran actualizaciones en shipping detail, receiving detail y lines
    $receivingsDetails       = array();        //Almacena los cambios que se realizaran a los receiving details
    $shippingsDetailsShipped = array();        //Almacena un arreglo
    //Almacena un array asociativo a partir de los shippingdetailid
    $receivingQuantities = arrayByKey("shippingdetailid", $receivingQuantities);
    //Se obtienen los shipping details de la base de datos a partir de el shippingid
    $resultQuery      =                   $this->shippingDetailModel->getByShippingId($shippingId);
    $shippingDetails  =                                                      $resultQuery["query"];
    //Se iteran todos los shipping details obtenidos
    for($c = 0; $c < count($shippingDetails); $c++){
      //Se declaran las variables locales dentro del ciclo para el total de cantidad de mercancia para un detalle o linea, para shipping detail (shipping detail en la base de datos) y para receiving quantity (cantidad a actualizarse de la salida de mercancia)
      $shippingDetail    =                                      $shippingDetails[$c];
      $receivingQuantity = $receivingQuantities[$shippingDetail["shippingdetailid"]];
      $valid             =                                                      true;
      $totalShippingQty  =                                                         0;
      //Esta condidicion verifica si shippingdetail cuenta con receivinglinesid (si es nula entonces es receivingdetail sino lo es en receivinglines)
      if( is_null($shippingDetail["receivinglinesid"]) ){
        $receivingShippedStatus =                                                                                       array();
        //Se realiza una consulta al modelo para obtener un receivingdetail a partir del receivingdetailid de shippingdetail
        $resultQuery            =                    $this->receivingDetailModel->getById($shippingDetail["receivingdetailid"]);
        $receivingDetail        =                                                                         $resultQuery["query"];
        //Se obtiene la cantidad total de mercancia para un receivingdetail
        $resultQuery            = $this->shippingDetailModel->getTotalByReceivingDetailId($shippingDetail["receivingdetailid"]);
        $totalShippingQty       =                                                                $resultQuery["query"]["total"];
        //Verifica que la cantidad a actualizar sea mayor a la suma del total de mercancia para un receivingdetail y la cantidad disponible de mercancia
        if( $receivingQuantity["quantitytoship"] > ($totalShippingQty + $receivingDetail["currentquantity"]) ){
          $valid = false;
        }
        //Verifica que la cantidad de mercancia a sacar sea mayor o igual a 0 y que la cantidad del shippingdetail sea diferente la cantidad de mercancia a sacar
        if( $receivingQuantity["quantitytoship"] >= 0 && $shippingDetail["quantity"] != $receivingQuantity["quantitytoship"] ){
          //Veficica que la cantidad de mercancia a sacar sea menor a la cantidad de mercancia a sacar
          if( $receivingQuantity["quantitytoship"] < $shippingDetail["quantity"] ){
            $receivingDetail["currentquantity"] += $shippingDetail["quantity"] - $receivingQuantity["quantitytoship"];
            $receivingDetail["quantitytoship"]  -= $shippingDetail["quantity"] - $receivingQuantity["quantitytoship"];
          }else{
            $shippingDetail["quantity"]          = $receivingQuantity["quantitytoship"] - $shippingDetail["quantity"];
            $receivingDetail["currentquantity"] -=                                        $shippingDetail["quantity"];
          }
          //Verifica que la cantidad disponible sea menor a 0
          if( $receivingDetail["currentquantity"] < 0 ){
            $valid                               =                                                                      false;
            $receivingDetail["currentquantity"] -= $receivingQuantity["quantitytoship"] + $receivingDetail["currentquantity"];
            $receivingQuantity["quantitytoship"] =                                                                         -1;
          }
          //Verifica que la cantidad de mercancia a sacar sea diferente de -1 y que $valid sea verdadero
          if( $receivingQuantity["quantitytoship"] != -1 || $valid ){
            //Se almacena receiving detail para despues actualizar en receiving detail la cantidad para shipping y la cantidad disponible, a partir del receivingdetailid
            array_push($receivingsDetails, array(
              "quantitytoship"    =>  $receivingQuantity["quantitytoship"],
              "currentquantity"   =>   $receivingDetail["currentquantity"],
              "receivingdetailid" => $receivingDetail["receivingdetailid"]
            ));
            //Se almacena en shipping detail para despues actualizar en shipping detail, la cantidad para shipping y el peso, a partir del shippingdetailid
            array_push($shippingsDetails, array(
              "quantity"         => $receivingQuantity["quantitytoship"],
              "weight"           =>            $shippingDetail["weight"],
              "shippingdetailid" =>  $shippingDetail["shippingdetailid"]
            ));
            $updateReceivings = TRUE;
          }
        }
      }else{
        //Se realiza una consulta al modelo para obtener un receivinglines a partir del receivingdetailid de shippingdetail
        $resultQuery      =                    $this->receivingLineModel->getById($shippingDetail["receivinglinesid"]);
        $receivingLine    =                                                                      $resultQuery["query"];
        //Se obtiene la cantidad total de mercancia para un receivinglines
        $resultQuery      = $this->shippingDetailModel->getTotalByReceivingLineId($shippingDetail["receivinglinesid"]);
        $totalShippingQty =                                                             $resultQuery["query"]["total"];
        //Verifica que la cantidad a actualizar sea mayor a la suma del total de mercancia para un receivingdetail y la cantidad disponible de mercancia
        if( $receivingQuantity["quantitytoship"] > ($totalShippingQty + $receivingLine["currentQty"]) ){
          $valid = false;
        }
        //Verifica que la cantidad de mercancia a sacar sea mayor o igual a 0 y que la cantidad del shippingdetail sea diferente la cantidad de mercancia a sacar
        if( $receivingQuantity["quantitytoship"] >= 0 && $shippingDetail["quantity"] != $receivingQuantity["quantitytoship"] ){
          //Veficica que la cantidad de mercancia a sacar sea menor a la cantidad de mercancia a sacar
          if( $receivingQuantity["quantitytoship"] < $shippingDetail["quantity"] ){
            $receivingLine["currentQty"] += $shippingDetail["quantity"] - $receivingQuantity["quantitytoship"];
          }else{
            $shippingDetail["quantity"]   = $receivingQuantity["quantitytoship"] - $shippingDetail["quantity"];
            $receivingLine["currentQty"] -=                                        $shippingDetail["quantity"];
          }
          //Verifica que la cantidad disponible sea menor a 0
          if( $receivingLine["currentQty"] < 0 ){
            $valid                               =                                                               false;
            $receivingLine["currentQty"]        -= $receivingQuantity["quantitytoship"] + $receivingLine["currentQty"];
            $receivingQuantity["quantitytoship"] =                                                                  -1;
          }
          //Verifica que la cantidad de mercancia a sacar sea diferente de -1 y que $valid sea verdadero
          if( $receivingQuantity["quantitytoship"] != -1 || $valid ){
            //Se almacenara el receiving lines para despues actualizar la cantidad disponible, a partir del receivinglinesid
            array_push($receivingsLines, array(
              "currentQty"       =>       $receivingLine["currentQty"],
              "receivinglinesid" => $receivingLine["receivinglinesid"]
            ));
            //Se almacenara en shipping detail para despues actualizar en shipping detail, la cantidad para shipping y el peso, a partir del shippingdetailid
            array_push($shippingsDetails, array(
              "quantity"         => $receivingQuantity["quantitytoship"],
              "weight"           =>            $shippingDetail["weight"],
              "shippingdetailid" =>  $shippingDetail["shippingdetailid"]
            ));
            $updateReceivings = TRUE;
          }
        }
      }
    }
    if($updateReceivings){
      //Actualiza todos los shipping detail, receiving detail y receiving line que se encuentran en los arreglos
      $this->receivingDetailModel->updateAll($receivingsDetails);
      $this->receivingLineModel->updateAll($receivingLines);
      $this->shippingDetailModel->updateAll($shippingsDetails);
      //Se iteran los receiving detail para posteriormente actualizar el estado en isShipped
      foreach($receivingsDetails as $receivingDetail){
        //Se obtiene el estado en shipped de los receiving (normal, detail, line)
        $resultQuery = $this->receivingShippedModel->getByReceivingDetailId($receivingDetail["receivingdetailid"]);
        $receivingShippedStatus = $resultQuery["query"];
        //Verifica que los receivinglines o receivingdetail tengan cantidad disponible mayor a cero, esto significa que currentQty en alguna linea o currentquantity en algun detalle aun no es 0
        //Llama a la funcion para actualizar el estado de isShipped del modelo receivingDetail, donde el estado depende si se cumple la condicion ('T') y no se cumple 'F'
        if( $receivingShippedStatus["receivinglines"] > 0 || $receivingShippedStatus["receivingdetails"] > 0 ){
          $this->receivingDetailModel->updateShipped(array(
            "isShipped"          =>                                   'F',
            "receivingdetailid"  => $receivingDetail["receivingdetailid"]
          ));
        }else{
          $this->receivingDetailModel->updateShipped(array(
            "isShipped"          =>                                   'T',
            "receivingdetailid"  => $receivingDetail["receivingdetailid"]
          ));
        }
        //Verifica que el receiving de un receivings tenga cantidad disponible mayor a cero, esto significa que currentquantity en algun detalle aun no es 0
        //Llama a la funcion para actualizar el estado de isShipped del modelo receiving, donde el estado depende si se cumple la condicion ('T') y no se cumple 'F'
        if( $receivingShippedStatus["receivings"] > 0 ){
          $this->receivingModel->updateShipped(array(
            "isShipped"    =>                             'F',
            "receivingid"  => $receivingDetail["receivingid"]
          ));
        }else{
          $this->receivingModel->updateShipped(array(
            "isShipped"    =>                             'T',
            "receivingid"  => $receivingDetail["receivingid"]
          ));
        }
      }
      echo "Si fue posible realizar la salida.";
    }else{
      echo "No fue posible realizar la salida.";
    }
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de verificar si un cliente cuenta con suficiente saldo para realizar una
   nueva salida de mercancia
   **************************************************************************************************
  */
  function createOrUpdateInvoiceFinal($shippingId, $isCharge, $receivingQuantities){
    //Declaracion de variables y arreglos a utilizar
    $tax                =    NULL;       //Almacena el impuesto
    $units              = array();       //Almacena la descripcion de las unidades de mercancias que se manejan en el sistema
    $total              =       0;       //Almacena el costo total de la nueva salida
    $details            = array();       //Almacena todos los elemrntos de preCreateShipping que sean details
    $invoice            = array();       //Almacena la informacion de la consulta que encuentra el invoice para un shipping
    $shipping           =    NULL;       //Almacena la informacion de la salida
    $customer           =    NULL;       //Almacena la informacion del cliente al que pertenece la salida
    $resultQuery        =    NULL;       //Almacena las consultas a los modelos
    $detailLines        = array();       //Almacena todos los elemrntos de preCreateShipping que sean detailLines
    $invoicelists       = array();       //Almacena los items de invoicelist
    $unitsCustomer      = array();       //Almacena el costo de CPD y Handling de todos las unidades de mercancia del cliente
    $receivingsLines    = array();       //Almacena los cambios que se realizaran a los receiving lines
    $shippingDetails    = array();       //Almacena la consulta de los shipping details de un shipping
    $shippingsDetails   = array();       //Almacena los cambios que se realizaran a los shipping details de un shipping
    $servicesShipping   = array();       //Almacena la consulta de los cargos extra de la salida
    $updateReceivings   =   FALSE;       //Almacena un boleano que indica si se realizaran actualizaciones en shipping detail, receiving detail y lines
    $receivingDetails   =    NULL;       //Almacena todos los id de receivingdetail como indices y su estado (detail o line)
    $receivingsDetails  = array();       //Almacena los cambios que se realizaran a los receiving details
    $preCreateShippings =    NULL;       //Almacena todos preCreateShipping para la salida
    //Almacena un array asociativo a partir de los shippingdetailid
    $receivingQuantities = arrayByKey("shippingdetailid", $receivingQuantities);
    //Se obtienen los shipping details de la base de datos a partir de el shippingid
    $resultQuery      =                   $this->shippingDetailModel->getByShippingId($shippingId);
    $shippingDetails  =                                                      $resultQuery["query"];
    //Se iteran todos los shipping details obtenidos
    for($c = 0; $c < count($shippingDetails); $c++){
      //Se declaran las variables locales dentro del ciclo para el total de cantidad de mercancia para un detalle o linea, para shipping detail (shipping detail en la base de datos) y para receiving quantity (cantidad a actualizarse de la salida de mercancia)
      $shippingDetail    =                                      $shippingDetails[$c];
      $receivingQuantity = $receivingQuantities[$shippingDetail["shippingdetailid"]];
      $valid             =                                                      true;
      $totalShippingQty  =                                                         0;
      //Esta condidicion verifica si shippingdetail cuenta con receivinglinesid (si es nula entonces es receivingdetail sino lo es en receivinglines)
      if( is_null($shippingDetail["receivinglinesid"]) ){
        $receivingShippedStatus =                                                                                       array();
        //Se realiza una consulta al modelo para obtener un receivingdetail a partir del receivingdetailid de shippingdetail
        $resultQuery            =                    $this->receivingDetailModel->getById($shippingDetail["receivingdetailid"]);
        $receivingDetail        =                                                                         $resultQuery["query"];
        //Se obtiene la cantidad total de mercancia para un receivingdetail
        $resultQuery            = $this->shippingDetailModel->getTotalByReceivingDetailId($shippingDetail["receivingdetailid"]);
        $totalShippingQty       =                                                                $resultQuery["query"]["total"];
        //Verifica que la cantidad a actualizar sea mayor a la suma del total de mercancia para un receivingdetail y la cantidad disponible de mercancia
        if( $receivingQuantity["quantitytoship"] > ($totalShippingQty + $receivingDetail["currentquantity"]) ){
          $valid = false;
        }
        //Verifica que la cantidad de mercancia a sacar sea mayor o igual a 0 y que la cantidad del shippingdetail sea diferente la cantidad de mercancia a sacar
        if( $receivingQuantity["quantitytoship"] >= 0 && $shippingDetail["quantity"] != $receivingQuantity["quantitytoship"] ){
          //Veficica que la cantidad de mercancia a sacar sea menor a la cantidad de mercancia a sacar
          if( $receivingQuantity["quantitytoship"] < $shippingDetail["quantity"] ){
            $receivingDetail["currentquantity"] += $shippingDetail["quantity"] - $receivingQuantity["quantitytoship"];
            $receivingDetail["quantitytoship"]  -= $shippingDetail["quantity"] - $receivingQuantity["quantitytoship"];
          }else{
            $shippingDetail["quantity"]          = $receivingQuantity["quantitytoship"] - $shippingDetail["quantity"];
            $receivingDetail["currentquantity"] -=                                        $shippingDetail["quantity"];
          }
          //Verifica que la cantidad disponible sea menor a 0
          if( $receivingDetail["currentquantity"] < 0 ){
            $valid                               =                                                                      false;
            $receivingDetail["currentquantity"] -= $receivingQuantity["quantitytoship"] + $receivingDetail["currentquantity"];
            $receivingQuantity["quantitytoship"] =                                                                         -1;
          }
          //Verifica que la cantidad de mercancia a sacar sea diferente de -1 y que $valid sea verdadero
          if( $receivingQuantity["quantitytoship"] != -1 || $valid ){
            //Se almacena receiving detail para despues actualizar en receiving detail la cantidad para shipping y la cantidad disponible, a partir del receivingdetailid
            array_push($receivingsDetails, array(
              "quantitytoship"    =>  $receivingQuantity["quantitytoship"],
              "currentquantity"   =>   $receivingDetail["currentquantity"],
              "receivingdetailid" => $receivingDetail["receivingdetailid"]
            ));
            //Se almacena en shipping detail para despues actualizar en shipping detail, la cantidad para shipping y el peso, a partir del shippingdetailid
            array_push($shippingsDetails, array(
              "quantity"         => $receivingQuantity["quantitytoship"],
              "weight"           =>            $shippingDetail["weight"],
              "shippingdetailid" =>  $shippingDetail["shippingdetailid"]
            ));
            $updateReceivings = TRUE;
          }
        }
      }else{
        //Se realiza una consulta al modelo para obtener un receivinglines a partir del receivingdetailid de shippingdetail
        $resultQuery      =                    $this->receivingLineModel->getById($shippingDetail["receivinglinesid"]);
        $receivingLine    =                                                                      $resultQuery["query"];
        //Se obtiene la cantidad total de mercancia para un receivinglines
        $resultQuery      = $this->shippingDetailModel->getTotalByReceivingLineId($shippingDetail["receivinglinesid"]);
        $totalShippingQty =                                                             $resultQuery["query"]["total"];
        //Verifica que la cantidad a actualizar sea mayor a la suma del total de mercancia para un receivingdetail y la cantidad disponible de mercancia
        if( $receivingQuantity["quantitytoship"] > ($totalShippingQty + $receivingLine["currentQty"]) ){
          $valid = false;
        }
        //Verifica que la cantidad de mercancia a sacar sea mayor o igual a 0 y que la cantidad del shippingdetail sea diferente la cantidad de mercancia a sacar
        if( $receivingQuantity["quantitytoship"] >= 0 && $shippingDetail["quantity"] != $receivingQuantity["quantitytoship"] ){
          //Veficica que la cantidad de mercancia a sacar sea menor a la cantidad de mercancia a sacar
          if( $receivingQuantity["quantitytoship"] < $shippingDetail["quantity"] ){
            $receivingLine["currentQty"] += $shippingDetail["quantity"] - $receivingQuantity["quantitytoship"];
          }else{
            $shippingDetail["quantity"]   = $receivingQuantity["quantitytoship"] - $shippingDetail["quantity"];
            $receivingLine["currentQty"] -=                                        $shippingDetail["quantity"];
          }
          //Verifica que la cantidad disponible sea menor a 0
          if( $receivingLine["currentQty"] < 0 ){
            $valid                               =                                                               false;
            $receivingLine["currentQty"]        -= $receivingQuantity["quantitytoship"] + $receivingLine["currentQty"];
            $receivingQuantity["quantitytoship"] =                                                                  -1;
          }
          //Verifica que la cantidad de mercancia a sacar sea diferente de -1 y que $valid sea verdadero
          if( $receivingQuantity["quantitytoship"] != -1 || $valid ){
            //Se almacenara el receiving lines para despues actualizar la cantidad disponible, a partir del receivinglinesid
            array_push($receivingsLines, array(
              "currentQty"       =>       $receivingLine["currentQty"],
              "receivinglinesid" => $receivingLine["receivinglinesid"]
            ));
            //Se almacenara en shipping detail para despues actualizar en shipping detail, la cantidad para shipping y el peso, a partir del shippingdetailid
            array_push($shippingsDetails, array(
              "quantity"         => $receivingQuantity["quantitytoship"],
              "weight"           =>            $shippingDetail["weight"],
              "shippingdetailid" =>  $shippingDetail["shippingdetailid"]
            ));
            $updateReceivings = TRUE;
          }
        }
      }
    }
    if($updateReceivings){
      //Se obtiene el invoice correspondiente a la salida en caso de existir
      $invoice = $this->invoiceModel->getByShippingId($shippingId);
      //Obtiene el impuesto que se aplicara a la factura
      $resultQuery = $this->taxModel->get();
      $tax = $resultQuery["query"]->fetch_assoc();
      //Obtiene todas las preCreateShipping de un shipping
      $resultQuery = $this->preCreateShippingModel->getByShippingId($shippingId);
      $preCreateShippings = $resultQuery["query"]->fetch_all(MYSQLI_ASSOC);
      //Obtiene la informacion de una salida
      $resultQuery = $this->shippingModel->getById($shippingId);
      $shipping = $resultQuery["query"]->fetch_assoc();
      //Obtiene la informacion del cliente al que le pertenece esa salida
      $resultQuery = $this->customerModel->getById($shipping["customerid"]);
      $customer = $resultQuery["query"]->fetch_assoc();
      //Obtiene el costo de CPD y Handling de mercancia de cada unidad que maneja el cliente y almacena un array asociativo a partir de las unitid
      $resultQuery = $this->unitCustomerModel->getById($customer["customerid"]);
      $unitsCustomer = $resultQuery["query"]->fetch_all(MYSQLI_ASSOC);
      $unitsCustomer = arrayByKey("unitid", $unitsCustomer);
      //Obtiene las unidades que se manejan en el sistema (descripciones) y almacena un array asociativo a partir de las unitid
      $resultQuery = $this->unitModel->get();
      $units = $resultQuery["query"]->fetch_all(MYSQLI_ASSOC);
      $units = arrayByKey("unitid", $units);
      //Obtiene los extra cargos que el cliente coloco en la salida que se aplicaran a la factura, y almacena en invoicelist los que se encuentren y suma al total el costo de esos extra cargos
      $resultQuery = $this->serviceShippingModel->getByShippingId($shippingId);
      $servicesShipping = $resultQuery["query"]->fetch_all(MYSQLI_ASSOC);
      if( count($servicesShipping) > 0 ){
        foreach( $servicesShipping as $serviceShipping ){
          $total += $serviceShipping["cost"];
        }
      }
      //Itera todos los elementos en preCreateShippings y almacena los que son lineas en el arreglo detailLines
      if( count($preCreateShippings) > 0 ){
        foreach( $preCreateShippings as $element ){
          //Verifica si el elemento tiene como estado "line", si si lo tiene almacena el receivingdetailid en el arreglo detailLines
          switch($element["state"]){
            case "line":
            if( !in_array($element["receivingdetailid"], $detailLines) ){
              array_push($detailLines, $element["receivingdetailid"]);
            }
            break;
            case "detail":
            if( !in_array($element["receivingdetailid"], $details) ){
              array_push($details, $element["receivingdetailid"]);
            }
            break;
          }
        }
      }
      //Se verifica que los detalles cuenten con elementos y se almacenan en el arreglo recevingDetails si son solo detalles o lineas
      if( count($details) == 0 ){
        foreach($detailsLines as $detailline){
          $receivingDetails[$detailline] =   "line";
        }
      }else{
        foreach( $details      as     $detail ){
          if( in_array($detail, $detailLines) )
            $receivingDetails[$detail]   =   "line";
          else
            $receivingDetails[$detail]   = "detail";
        }
      }
      //Verifica que receivingDetail cuente con elementos y si si cuenta lo itera y almacena en un arreglo los invoicelists de handling, CPD
      if( count($receivingDetails) > 0 ){
        //Se iteran todos los receiving elements
        foreach( $preCreateShippings as $element ){
          if( array_key_exists($element['receivingdetailid'], $receivingDetails) ){
            if( (strcmp($element["state"], "detail") == 0 && $receivingDetails[$element["receivingdetailid"]] == "detail") || (strcmp($element["state"], "line") == 0 && $receivingDetails[$element["receivingdetailid"]] == "line") ){
              $days = 0;       //Almacena la cantidad de dias de almacenaje y los transcurridos
              $invoicelist = array(
                "qty"              =>             $receivingQuantities[$element["shippingdetailid"]]["quantitytoship"],
                "description"      => $units[$element['unitid']]." $element[productDescription] Storage: $days Day(s)",
                "receivingid"      =>                                                          $element["receivingid"],
                "receivingno"      =>                                                          $element["receivingno"],
                "unitprice"        =>                                                                               -1,
                "total"            =>                                                                               -1,
                "extra"            =>                                                                                2,
                "shippingdetailid" =>                                                     $element["shippingdetailid"]
              );               //Almacena el invoicelist que se almacenera en el arreglo de invoicelists para posteriormente insertarla en la base de datos
              //Si no hay localStorage es porque no se ha cobrado el almacenaje, y se aplicara el freeday del cliente
              if( !$isCharge ){
                if( is_null($element["lastStorageDate"]) ){
                  $days =  daysStorage(formatDate($element["date"]), $customer["freedays"]);
                }else{
                  $days = daysElapsed($element["lastStorageDate"], $shipping["shippdate"]);
                }
              }
              //CPD
              //Si el CPD del unitcustomer no es nulo le asigna un determinado valor a unitprice (CPD (Costo por dia de almacenamiento)) y total del invoicelist
              if( !is_null( $CPD = $unitsCustomer[$element["unitid"]]["CPD"]) ){
                $invoicelist["unitprice"] =                               $CPD;
                $invoicelist["total"]     = $invoicelist["qty"] * $days * $CPD;
                $total                   += $invoicelist["qty"] * $days * $CPD;
              }
              //Se almacena invoicelist con precio de CPD en invoicelists
              array_push($invoicelists, $invoicelist);
              //Handling
              //Se asigna una nueva descripcion para handling y un nuevo valor en extra
              $invoicelist["description"] = $units[$element['unitid']]." $element[productDescription] Handling";
              $invoicelist["extra"]       =                                                                   3;
              //Si el Handling del unitcustomer no es nulo le asigna un determinado valor a unitprice (CPD (Costo por dia de almacenamiento)) y total del invoicelist
              if( !is_null( $handling = $unitsCustomer[$element["unitid"]]["handling"]) ){
                $invoicelist["unitprice"] =                          $handling;
                $invoicelist["total"]     =    $invoicelist["qty"] * $handling;
                $total                   +=    $invoicelist["qty"] * $handling;
              }
              //Se almacena invoicelist con precio de Handling en invoicelists
              array_push($invoicelists, $invoicelist);
              //Hazmat, Southbound o Northbound
              //Suma al total el costo de hazmat, south o north que el cliente tiene asignado
              if( strcmp($element["hazmat"], "yes") == 0 ){
                $total += $customer["hazmat"];
              }
              if( strcmp($element["NorthOrSouth"], "Northbound") == 0 ){
                $total += $customer["north"];
              }else{
                $total += $customer["south"];
              }
            }
          }
        }
        //Verificacion del monto del cliente
        switch($invoice["status"]){
          case TRUE:
          //Esta variable almacena el total de la factura
          $ptotal =                                 $total;
          //Esta variable almacena la diferencia entre el subtotal de la factura almacenada y el total calculado para la misma factura
          $total  = $total - $invoice["query"]["subtotal"];
          //Si el monto no alcanza para el total de dinero que cuesta la salida de toda la mercancia, no crea la factura
          if( !$this->checkAmount($customer["customerid"], $total) ){
            echo "No fue posible realizar la salida.";
            return FALSE;
          }
          //Si si cuenta con suficiente saldo se actualiza el total de la factura nuevo
          $this->invoiceModel->update(array(
            "invoiceid" => $invoice["query"]["invoiceid"],
            "subtotal"  =>                        $ptotal
          ));
          //Este metodo llama al metodo del modelo customer que permite actualizar el credito ocupado del cliente
          $this->customerModel->updateDueAmount($customer["customerid"], $total);
          //Elimina todos los list de factura para posteriormente agregar nuevos
          $this->invoiceListModel->deleteByInvoiceId($invoice["query"]["invoiceid"]);
          //Se crean los list de factura
          $this->invoiceListModel->saveAll($invoice["query"]["invoiceid"], $invoicelists);
          break;
          case FALSE:
          //Si el monto no alcanza para el total de dinero que cuesta la salida de toda la mercancia, no crea la factura
          if( !$this->checkAmount($customer["customerid"], $total) || $total == 0 ){
            echo "No fue posible realizar la salida.";
            return FALSE;
          }
          //Si si cuenta con suficiente saldo crea la factura
          $resultQuery = $this->invoiceModel->save(array(
            "shippingid" => $shippingId,
            "total"      =>      $total,
            "tax"        => $tax["tax"]
          ));
          //Este metodo llama al metodo del modelo customer que permite actualizar el credito ocupado del cliente
          $this->customerModel->updateDueAmount($customer["customerid"], $total);
          //Se crean los list de factura
          $this->invoiceListModel->saveAll($resultQuery["invoiceid"], $invoicelists);
          break;
        }
        //Actualiza todos los shipping detail, receiving detail y receiving line que se encuentran en los arreglos
        $this->receivingDetailModel->updateAll($receivingsDetails);
        $this->receivingLineModel->updateAll($receivingLines);
        $this->shippingDetailModel->updateAll($shippingsDetails);
        //Se iteran los receiving detail para posteriormente actualizar el estado en isShipped
        foreach($receivingsDetails as $receivingDetail){
          //Se obtiene el estado en shipped de los receiving (normal, detail, line)
          $resultQuery = $this->receivingShippedModel->getByReceivingDetailId($receivingDetail["receivingdetailid"]);
          $receivingShippedStatus = $resultQuery["query"];
          //Verifica que los receivinglines o receivingdetail tengan cantidad disponible mayor a cero, esto significa que currentQty en alguna linea o currentquantity en algun detalle aun no es 0
          //Llama a la funcion para actualizar el estado de isShipped del modelo receivingDetail, donde el estado depende si se cumple la condicion ('T') y no se cumple 'F'
          if( $receivingShippedStatus["receivinglines"] > 0 || $receivingShippedStatus["receivingdetails"] > 0 ){
            $this->receivingDetailModel->updateShipped(array(
              "isShipped"          =>                                   'F',
              "receivingdetailid"  => $receivingDetail["receivingdetailid"]
            ));
          }else{
            $this->receivingDetailModel->updateShipped(array(
              "isShipped"          =>                                   'T',
              "receivingdetailid"  => $receivingDetail["receivingdetailid"]
            ));
          }
          //Verifica que el receiving de un receivings tenga cantidad disponible mayor a cero, esto significa que currentquantity en algun detalle aun no es 0
          //Llama a la funcion para actualizar el estado de isShipped del modelo receiving, donde el estado depende si se cumple la condicion ('T') y no se cumple 'F'
          if( $receivingShippedStatus["receivings"] > 0 ){
            $this->receivingModel->updateShipped(array(
              "isShipped"    =>                             'F',
              "receivingid"  => $receivingDetail["receivingid"]
            ));
          }else{
            $this->receivingModel->updateShipped(array(
              "isShipped"    =>                             'T',
              "receivingid"  => $receivingDetail["receivingid"]
            ));
          }
        }
        echo "Si fue posible realizar la salida.";
        return TRUE;
      }
    }
    echo "No fue posible realizar la salida.";
    return FALSE;
  }
}
?>

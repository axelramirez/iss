<?php


require_once(          "models/Printer.php");

class Printers{
    private $printer;

    public function __construct(){
        $this->printer = new Printer();
    }

    public function insertPrinter(){
        $params = array();
        parse_str(file_get_contents('php://input'), $params);
        header("content-type: application/json");
      
       $printername = $params["printer_name"];
       $printerip = $params["printer_ip"];
       $description = $params["description"];
       $default = $params["default"];
       if($this->printer->insert($printername,$printerip,$description,$default)){
           echo(json_encode(array(
               "code" => 200,
               "params" => $printername
           )));
       }else{
           echo(json_encode(array(
               "code" => 500
           )));
       }
    }

    public function updatePrinter(){
        $params = array();
        parse_str(file_get_contents('php://input'), $params);
        header("content-type: application/json");
        $printerid = $params["printer_id"];
        $printername = $params["printer_name"];
        $printerip = $params["printer_ip"];
        $description = $params["description"];
        $default = $params["default"];
        if($this->printer->update($printername,$printerip,$description,$default,$printerid)){
            echo(json_encode(array(
                "code" => 200,
                "params" => $printername
            )));
        }else{
            echo(json_encode(array(
                "code" => 500
            )));
        }
    }

    public function getPrinter(){
        $params = $_GET;
        $resultQuery = array();
        $numberPage = (Integer)$params["page"];
        $printername = $params["printer_name"];
        $printerip = $params["printer_ip"];
        $resultQuery = $this->printer->get($printername,$printerip,$numberPage);
        header("content-type: application/json");
        echo(json_encode(array(
            "code" => 200,
            "response" => array(
                "printer" => $resultQuery["query"],
                "numberEntries" => $resultQuery["numberEntries"],
                "numberPages" => $resultQuery["numberPages"]
            )
        )));
    }

  

    public function getUserResult(){
        $params = $_GET;
        $resultQuery = array();
        $userid = (Integer)$params["userid"];
        $pass = $params["pass"];
        $resultQuery = $this->user->getResult($userid,$pass);
        header("content-type: application/json");
        echo(json_encode(array(
            "code" => 200,
            "response" => array(
                "result" => $resultQuery["status"],
            )
        )));
    }


    public function updateUserResult(){
        $params = array();
        parse_str(file_get_contents('php://input'), $params);
        header("content-type: application/json");
      
        $userid = (Integer)$params["userID"];
        $pass = $params["pass"];
       if($this->user->updateResult($userid,$pass)){
           echo(json_encode(array(
               "code" => 200,
               "result" => $userid
           )));
       }else{
           echo(json_encode(array(
               "code" => 500
           )));
       }
    }




    public function deletePrinter(){
        $params = array();
        parse_str(file_get_contents('php://input'), $params);
        header("content-type: application/json");
        $printerid = $params["printer_id"];
        if($this->printer->delete($printerid)){
            echo(json_encode(array(
                "code" => 200,
                "params" => $printerid
            )));
        }else{
            echo(json_encode(array(
                "code" => 500
            )));
        }
      }

}
?>
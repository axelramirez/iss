<?php
//Models
require_once(          "models/Invoice_model.php");
require_once(         "models/Customer_model.php");
require_once(      "models/AutoInvoice_model.php");
require_once(      "models/PaymentType_model.php");
require_once(  "models/PaymentReceived_model.php");
require_once("models/statusAutoInvoice_model.php");
//Helpers
require_once("helpers/validityStructureArray_helper.php");
require_once("helpers/validityPositiveNumber_helper.php");
require_once(        "helpers/sanitizeString_helper.php");
require_once(         "helpers/formatDateYMD_helper.php");
require_once(          "helpers/validityDate_helper.php");
require_once(          "helpers/validityData_helper.php");
/*
 **************************************************************************************************
 *************************Name: Payment Class Controller
 *************************Description: Se encarga de las cuentas por cobrar de cada cliente, donde
 ************************************* cada cliente tiene la posibilidad de pagar sus facturas en
 ************************************* abonos y asi disminuir su cuenta de credito
 **************************************************************************************************
*/
class Payment{
  private           $invoiceModel;
  private          $customerModel;
  private       $autoInvoiceModel;
  private       $paymentTypeModel;
  private   $paymentReceivedModel;
  private $statusAutoInvoiceModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->invoiceModel           = new           Invoice_model();
    $this->customerModel          = new          Customer_model();
    $this->autoInvoiceModel       = new       AutoInvoice_model();
    $this->paymentTypeModel       = new       PaymentType_model();
    $this->paymentReceivedModel   = new   PaymentReceived_model();
    $this->statusAutoInvoiceModel = new StatusAutoInvoice_model();
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de realizar un pago de una factura en AutoInvoice y de disminuir el estado
   de credito de un cliente
   **************************************************************************************************
  */
  public function payInvoice(){
    $params     = array();
    $invoice    = array();
    $payment    = array();
    $newPayment = array();
    $resultQuery         =             array();       //Almacena el resultado de la consulta sobre un modelo
    $customerAutoInvoice =             array();       //Almacena la consulta sobre el modelo statusAutoInvoice
    parse_str(file_get_contents('php://input'), $params);
    $isValid = TRUE;
    if(validityStructureArray(array(
      "payment" => "Array"
    ), $params)){
      $payment = $params["payment"];
      if(validityStructureArray(array(
        "amount" => "UFloat",
        "autoinvoice_id" => "UInteger",
        "payment_type_id" => "UInteger",
        "references_number" => "UInteger",
        "payment_date" => "Date"
      ), $payment)){
        $payment["amount"]            =     round((Float)$payment["amount"], 2);
        $payment["autoinvoice_id"]    =     (Integer)$payment["autoinvoice_id"];
        $payment["payment_type_id"]   =    (Integer)$payment["payment_type_id"];
        $payment["references_number"] =  (Integer)$payment["references_number"];
        $payment["payment_date"]      = (strcmp($payment["payment_date"], "") != 0) ? formatDateYMD($payment["payment_date"]) : '';
      }else{
        $isValid = FALSE;
      }
    }else{
      $isValid = FALSE;
    }
    header("content-type: application/json");
    if(!$isValid){
      echo(json_encode(array(
        "code"     => 400,
        "response" => array(
          "message"  => "Error al llenar los campos, se encontraron campos vacios y datos no validos, porfavor ingreselos correctamente y vuelva a enviar el formulario"
        )
      )));
      return;
    }else if($payment["amount"] == 0){
      echo(json_encode(array(
        "code"     => 400,
        "response" => array(
          "message"  => "Error al llenar el campo de monto, no puede ser 0, por favor ingresa una cantidad valida y vuelva a enviar el formulario."
        )
      )));
      return;
    }

    $resultQuery  = $this->statusAutoInvoiceModel->getById($payment["autoinvoice_id"]);
    $invoice      =               $resultQuery["query"]->fetch_assoc();

    $resultQuery = $this->paymentTypeModel->getById($payment["payment_type_id"]);
    $paymentType = $resultQuery["query"];

    //Se verifica si existe la factura
    if( count($invoice) == 0 ){
      echo(json_encode(array(
        "code" => 404
      )));
      return;
    }

    if(count($paymentType) == 0){
      echo json_encode(array(
        "code" => 400,
        "response" => array(
          "message" => "Error: el tipo de pago no existe, porfavor ingrese un tipo de pago valido."
        )
      ));
      return;
    }

    if((Integer)$invoice["isCancelled"] == 1){
      echo json_encode(array(
        "code" => 400,
        "response" => array(
          "message" => "La factura esta cancelada, no es posible insertar un nuevo pago."
        )
      ));
      return;
    }

    if(strcmp($invoice["dateInvoice"], $payment["payment_date"] ) == 1){
      echo json_encode(array(
        "code" => 400,
        "response" => array(
          "message" => "Error: debe ingresar una fecha con el formato YYYY-MM-DD, que se mayor o igual a la fecha de la factura y que esta fecha exista en el calendario."
        )
      ));
      return;
    }

    //Verifica que el monto sea mayor a 0, que sea menor o igual al monto que se debe de la autoinvoice y que la autoinvoice este actica (no pagada, no cancelada)
    if( $this->checkAutoInvoiceAmount($payment["autoinvoice_id"], $payment["amount"]) ){
      //Realiza una consulta al modelo PaymentReceived y lo almacena en customerAutoInvoice
      $customerAutoInvoice     =                                                           $invoice;
      //Actualiza el monto pagado de la autoinvoice
      $this->autoInvoiceModel->update(array(
        "paid_amount" =>         $payment["amount"],
        "autoid"      => $payment["autoinvoice_id"]
      ));
      //Si aun no ah sido pagada la factura y el monto pagado de la factura es igual al total a pagar de la factura
      if( ((Integer)$customerAutoInvoice['paidInvoice'] + (Integer)$payment["amount"]) === (Integer)$customerAutoInvoice['totalInvoice'] ){
        //Se actualiza la columna isPaid a TRUE para la AutoInvoice
        $this->autoInvoiceModel->updatePaid(array(
          "ispaid" =>                       TRUE,
          "autoid" => $payment["autoinvoice_id"]
        ));
      }else{
        //Se actualiza la columna isPaid a FALSE para la AutoInvoice
        $this->autoInvoiceModel->updatePaid(array(
          "ispaid" =>                       FALSE,
          "autoid" => $payment["autoinvoice_id"]
        ));
      }
      //Se actualiza el credito disponible para el cliente
      $this->customerModel->updateDueAmount($customerAutoInvoice["customer_id"], -$payment["amount"]);
      //Se almacena el pago en el modelo de cuentas por cobrar (PaymentReceived)
      $newPayment = $this->paymentReceivedModel->save($payment);
      //Obtiene los datos de el pago realizado
      $resultQuery =   $this->paymentReceivedModel->getById($newPayment["payment_number"]);
      $newPayment  =                                                 $resultQuery["query"];
      $resultQuery = $this->statusAutoInvoiceModel->getById($newPayment["autoinvoice_id"]);
      $invoice     =                                  $resultQuery["query"]->fetch_assoc();
      //Como todo salio bien se devuelve un codigo 200
      echo(json_encode(array(
        "code"     => 201,
        "response" => array(
          "payment" => $newPayment,
          "invoice" =>    $invoice
        )
      )));
      return;
    }
    //Se devuelve codigo 500 porque no fue posible realizar el pago
    echo(json_encode(array(
      "code"     => 400,
      "response" => array(
        "message" => "Error: el monto que se desea pagar excede el monto que se debe de la factura, porfavor ingrese un monto menor o igual al que se debe."
      )
    )));
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de eliminar una autoinvoice por su id
   **************************************************************************************************
  */
  public function deletePayment(){
    $params  = array();
    $invoice = array();
    parse_str(file_get_contents('php://input'), $params);

    $isValid = TRUE;
    if(validityStructureArray(array(
      "paymentNumber" => "UInteger",
      "autoinvoice_id" => "UInteger"
    ), $params)){
      $paymentNumber = (Integer) $params["paymentNumber"];
      $invoiceId = (Integer) $params["autoinvoice_id"];
    }else{
      $isValid = FALSE;
    }

    if(!$isValid){
      echo(json_encode(array(
        "code"     => 400,
        "response" => array(
          "message"  => "Error al eliminar el pago, el numero de pago o de factura no es valido."
        )
      )));
      return;
    }
    $resultQuery         = $this->paymentReceivedModel->getById($paymentNumber);        //Almacena el resultado de la consulta sobre un modelo
    $paymentReceived     =                                $resultQuery["query"];        //Almacena la consulta sobre el modelo PaymentReceived a partir de resultQuery

    $resultQuery  = $this->statusAutoInvoiceModel->getById($invoiceId);
    $invoice      =               $resultQuery["query"]->fetch_assoc();

    if(count($paymentReceived) == 0 || count($invoice) == 0 ){
      echo(json_encode(array(
        "code"     => 400,
        "response" => array(
          "message"  => "Error al eliminar el pago, el numero de pago o de factura, no existen."
        )
      )));
      return;
    }

    if($paymentReceived["autoinvoice_id"] != $invoice["autoid"]){
      echo(json_encode(array(
        "code"     => 400,
        "response" => array(
          "message"  => "Error al eliminar el pago, el pago no pertecene a la factura."
        )
      )));
      return;
    }

    header("content-type: application/json");
    //Llama a la funcion delete del modelo PaymentReceived y verifica que se elimine el payment
    if( count($paymentReceived) > 0 ){
      $this->paymentReceivedModel->delete($paymentNumber);
      //Actualiza el credito del cliente
      $this->customerModel->updateDueAmount($paymentReceived["customer_id"], $paymentReceived["amount"]);
      //Actualiza el monto pagado de la autoinvoice
      $this->autoInvoiceModel->update(array(
        "paid_amount" => -$paymentReceived["amount"],
        "autoid"      =>  $paymentReceived["autoinvoice_id"]
      ));
      //Actualiza el estado de la autoinvoice a estado no pagado (FALSE)
      $this->autoInvoiceModel->updatePaid(array(
        "ispaid" =>                              FALSE,
        "autoid" => $paymentReceived["autoinvoice_id"]
      ));
      //
      $resultQuery = $this->statusAutoInvoiceModel->getById($paymentReceived["autoinvoice_id"]);
      $invoice     =                                       $resultQuery["query"]->fetch_assoc();
      //Se devuelve codigo 200 si se realizo correctamente la eliminacion del pago
      echo(json_encode(array(
        "code"     => 200,
        "response" => array(
          "paymentNumber" => $paymentNumber,
          "invoice"       =>       $invoice
        )
      )));
      return;
    }
    //Se devuelve codigo 500 si no se realizo la eliminacion del pago
    echo(json_encode(array(
      "code"     => 404
    )));
    return;
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de actualizar una autoinvoice por su id
   **************************************************************************************************
  */
  public function updatePayment(){
    $params              = array();
    $invoice             = array();
    $payment             = array();
    $complete            =   FALSE;
    $customerAutoInvoice = array();        //Almacena la consulta sobre el modelo statusAutoInvoice
    parse_str(file_get_contents('php://input'), $params);
    $isValid = TRUE;
    if(validityStructureArray(array(
      "payment" => "Array"
    ), $params)){
      $payment = $params["payment"];
      if(validityStructureArray(array(
        "payment_number" => "UInteger",
        "amount" => "UFloat",
        "autoinvoice_id" => "UInteger",
        "payment_type_id" => "UInteger",
        "references_number" => "UInteger",
        "payment_date" => "Date"
      ), $payment)){
        $payment["payment_number"]    =     (Integer)$payment["payment_number"];
        $payment["amount"]            =     round((Float)$payment["amount"], 2);
        $payment["autoinvoice_id"]    =     (Integer)$payment["autoinvoice_id"];
        $payment["payment_type_id"]   =    (Integer)$payment["payment_type_id"];
        $payment["references_number"] =  (Integer)$payment["references_number"];
        $payment["payment_date"]      = (strcmp($payment["payment_date"], "") != 0) ? formatDateYMD($payment["payment_date"]) : '';
      }else{
        $isValid = FALSE;
      }
    }else{
      $isValid = FALSE;
    }
    header("content-type: application/json");
    if(!$isValid){
      echo(json_encode(array(
        "code"     => 400,
        "response" => array(
          "message"  => "Error al llenar los campos, se encontraron campos vacios y datos no validos, porfavor ingreselos correctamente y vuelva a enviar el formulario"
        )
      )));
      return;
    }else if($payment["amount"] == 0){
      echo(json_encode(array(
        "code"     => 400,
        "response" => array(
          "message"  => "Error al llenar el campo de monto, no puede ser 0, por favor ingresa una cantidad valida y vuelva a enviar el formulario."
        )
      )));
      return;
    }
    $resultQuery         = $this->paymentReceivedModel->getById($payment["payment_number"]);        //Almacena el resultado de la consulta sobre un modelo
    $paymentReceived     =                                            $resultQuery["query"];        //Almacena la consulta sobre el modelo PaymentReceived a partir de resultQuery

    $resultQuery  = $this->statusAutoInvoiceModel->getById($payment["autoinvoice_id"]);
    $invoice      =               $resultQuery["query"]->fetch_assoc();

    //Se verifica si existe la factura
    if( count($invoice) == 0 || count($paymentReceived) == 0){
      echo(json_encode(array(
        "code"     => 400,
        "response" => array(
          "message"  => "Error al actualizar el pago, el numero de pago o de factura, no existen."
        )
      )));
      return;
    }

    if($paymentReceived["autoinvoice_id"] != $invoice["autoid"]){
      echo(json_encode(array(
        "code"     => 400,
        "response" => array(
          "message"  => "Error al actualizar el pago, el pago no pertecene a la factura."
        )
      )));
      return;
    }

    if(strcmp($invoice["dateInvoice"], $payment["payment_date"] ) == 1){
      echo json_encode(array(
        "code" => 400,
        "response" => array(
          "message" => "Error: debe ingresar una fecha con el formato YYYY-MM-DD, que se mayor o igual a la fecha de la factura y que esta fecha exista en el calendario."
        )
      ));
      return;
    }

    if((Integer)$invoice["isCancelled"] == 1){
      echo json_encode(array(
        "code" => 400,
        "response" => array(
          "message" => "La factura esta cancelada, no es posible actualizar el pago."
        )
      ));
      return;
    }

    $amount              =                  $payment["amount"] - $paymentReceived["amount"];        //Almacena el amount total entre la resta del amount nuevo con el amount almacenado en el sistema
    header("content-type: application/json");
    //Se verifica que el monto a actualizar de un pago sea diferente al almacenado y que sea mayor a 0
    if( $payment["amount"] != ((Integer)$paymentReceived["amount"]) && $amount > 0 && $payment["amount"] > 0 ){
      $customerAutoInvoice     =                               $invoice;
      if( $this->checkAutoInvoiceAmount($payment["autoinvoice_id"], $amount) ){
        //Llama a la funcion updateById del modelo PaymentReceived y verifica que se actualize el payment
        if( $this->paymentReceivedModel->updateById($payment) ){
          //Actualiza el credito del cliente
          $this->customerModel->updateDueAmount($paymentReceived["customer_id"], -$amount);
          //Actualiza el monto pagado de la autoinvoice
          $this->autoInvoiceModel->update(array(
            "paid_amount" =>                            $amount,
            "autoid"      => $paymentReceived["autoinvoice_id"]
          ));
          //Si aun no ah sido pagada la factura y el monto pagado de la factura es igual al total a pagar de la factura
          if( ((Integer)$customerAutoInvoice['paidInvoice'] + (Integer)$amount) == (Integer)$customerAutoInvoice['totalInvoice'] ){
            //Se actualiza la columna isPaid a TRUE para la AutoInvoice
            $this->autoInvoiceModel->updatePaid(array(
              "ispaid" =>                       TRUE,
              "autoid" => $payment["autoinvoice_id"]
            ));
          }else{
            //Se actualiza la columna isPaid a FALSE para la AutoInvoice
            $this->autoInvoiceModel->updatePaid(array(
              "ispaid" =>                       FALSE,
              "autoid" => $payment["autoinvoice_id"]
            ));
          }
        }
      }
      $complete = TRUE;
    }else{
      if( $this->paymentReceivedModel->updateById($payment) ){
        //Actualiza el credito del cliente
        $this->customerModel->updateDueAmount($paymentReceived["customer_id"], -$amount);
        //Actualiza el monto pagado de la autoinvoice
        $this->autoInvoiceModel->update(array(
          "paid_amount" =>                            $amount,
          "autoid"      => $paymentReceived["autoinvoice_id"]
        ));
        //Actualiza el estado de la autoinvoice a estado no pagado (FALSE)
        $this->autoInvoiceModel->updatePaid(array(
          "ispaid" =>                              FALSE,
          "autoid" => $paymentReceived["autoinvoice_id"]
        ));
        $complete = TRUE;
      }
    }
    if( $complete ){
      $resultQuery =           $this->paymentReceivedModel->getById($payment["payment_number"]);
      $payment     =                                                      $resultQuery["query"];
      $resultQuery = $this->statusAutoInvoiceModel->getById($payment["autoinvoice_id"]);
      $invoice     =                                       $resultQuery["query"]->fetch_assoc();
      //Se devuelve codigo 200 si se realizo correctamente la actualizacion del pago
      echo(json_encode(array(
        "code"     => 200,
        "response" => array(
          "payment" => $payment,
          "invoice" => $invoice
        )
      )));
      return;
    }
    //Se devuelve codigo 404 si no se realizo la actualizacion del pago
    echo(json_encode(array(
      "code"     => 404
    )));
  }
  /*
   **************************************************************************************************
   Esta funcion verifica que una factura aun puede recibir pagos (solo si no esta pagada o cancelada)
   y varifica que el monto que se desea pagar es exactamente igual o menor al monto que se debe de la
   factura
   **************************************************************************************************
  */
  private function checkAutoInvoiceAmount($autoInvoiceId, $amount){
  $resultQuery         = array();
  $customerAutoInvoice = array();
  //Se obtiene el estado de la factura del cliente
  $resultQuery = $this->statusAutoInvoiceModel->getById($autoInvoiceId);
  if( $resultQuery["status"] ){
    $customerAutoInvoice = $resultQuery["query"]->fetch_assoc();
    //Se verifica que el cliente cuenta con sufiente saldo para recibir tal monto
    if( ((Boolean)$customerAutoInvoice['isAllow']) && $amount <= ((Double)$customerAutoInvoice['dueInvoice']) && $amount > 0 ){
      return TRUE;
    }else{
      return FALSE;
    }
  }else{
    //Se imprime el mensaje de error
    print_r($resultQuery["message"]);
  }
 }
 /*
  **************************************************************************************************
  Esta funcion devuelve todos los metodos de pago de facturas
  **************************************************************************************************
 */
  public function getPaymentType(){
    $resultQuery = array();
    //Se obtiene el estado de la factura del cliente
    header("content-type: application/json");

    $resultQuery = $this->paymentTypeModel->get();
    if( $resultQuery["status"] ){
     echo(json_encode(array(
       "code"     =>   200,
       "response" => array(
         "paymentType" => $resultQuery["query"]
       )
     )));
     return;
    }else{
     //Se imprime el mensaje de error
     echo(json_encode(array(
       "code"     =>   500
     )));
     return;
    }
  }
  /*
   **************************************************************************************************
   Esta funcion devuelve la información de un pago
   **************************************************************************************************
  */
   public function getPayment(){
     $resultQuery = array();              //Almacena el resultado de la consulta sobre un modelo
     $paymentNumber = (Integer)$_GET["paymentNumber"];
     $invoiceId      =     (Integer)$_GET["invoiceId"];
     if( !$this->validateNumber($paymentNumber) || !$this->validateNumber($invoiceId) ){
         echo(json_encode(array(
           "code"     => 400,
           "response" => array(
             "message"  => "Error al enviar los datos, se esperaban valores de numeros enteros, porfavor recargue la pagina y vuelva intentar."
           )
         )));
         return;
     }
     //Se obtiene la informacion del pago
     $resultQuery = $this->paymentReceivedModel->getByIdAndAutoInvoiceId($paymentNumber, $invoiceId);
     header("content-type: application/json");
     if( $resultQuery["status"] ){
       if( count($resultQuery["query"]) > 0 ){
         echo(json_encode(array(
           "code"     =>   200,
           "response" => array(
             "payment" => $resultQuery["query"]
           )
         )));
       }else{
         echo(json_encode(array(
           "code"     =>   404
         )));
       }
     }else{
      //Se imprime el mensaje de error
      echo(json_encode(array(
        "code"     =>   500
      )));
      return;
     }
   }

   private function validateNumber($value){
     $isValidate = FALSE;
     if( preg_match('/^[0-9]+((\.|\,)[0-9]+)?$/', (string)$value) ){
       $isValidate = TRUE;
     }
     return $isValidate;
   }
}
?>

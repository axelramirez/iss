<?php
//Models
require_once(          "models/Vendor_model.php");
/*
 **************************************************************************************************
 *************************Name: Purchase Order Class Controller
 *************************Description: Se encarga de agregar purchase orders
 **************************************************************************************************
*/
class Vendor{
  private           $vendorModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->vendorModel = new Vendor_model();
  }
  /*
   **************************************************************************************************

   **************************************************************************************************
  */
  public function insertVendor(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $newvendor = $params["newVendor"];
     if($this->vendorModel->insert($newvendor)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $newvendor
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function getVendor(){
      $params = $_GET;
      $resultQuery = array();
      $resultQuery = $this->vendorModel->get();
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "vendors" => $resultQuery["query"]
          )
      )));
  }
  public function getVendorLimit(){
      $params = $_GET;
      $resultQuery = array();
      $numberPage = (Integer)$params["page"];
      $resultQuery = $this->vendorModel->getbypagination($numberPage);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "vendors" => $resultQuery["query"],
              "numberEntries" => $resultQuery["numberEntries"],
              "numberPages" => $resultQuery["numberPages"]
          )
      )));
  }
  //Search vendors
  public function searchVendor(){
      $params = $_GET;
      $resultQuery = array();
      $numberPage = (Integer)$params["page"];
      $filter = $params["filter"];
      $resultQuery = $this->vendorModel->searchbypagination($numberPage, $filter);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "vendors" => $resultQuery["query"],
              "numberEntries" => $resultQuery["numberEntries"],
              "numberPages" => $resultQuery["numberPages"]
          )
      )));
  }
  public function insertVendorAll(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $data = $params["insertVendor"];
     if($this->vendorModel->insertVendor($data)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $data
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function updateVendorAll(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $vendorId = $params["vendorId"];
      $data = $params["updateVendor"];
     if($this->vendorModel->updateVendor($data, $vendorId)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $data
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
}
?>

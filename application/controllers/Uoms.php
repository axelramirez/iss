<?php
//Models
require_once(          "models/Uom_model.php");
/*
 **************************************************************************************************
 *************************Name: UOM Class Controller
 *************************Description: Se encarga de agregar uom
 **************************************************************************************************
*/
class Uom{
  private           $uomModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->uomModel = new Uom_model();
  }
  /*
   **************************************************************************************************

   **************************************************************************************************
  */
  public function insertUOM(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
     $uomName = $params["uomName"];
     if($this->uomModel->insert($uomName)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $uomName
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function updateUOM(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $uomName = $params["uomName"];
      $uomId = $params["uomId"];
     if($this->uomModel->update($uomName, $uomId)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $uomName
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function getUOM(){
      $params = $_GET;
      $resultQuery = array();
      $numberPage = (Integer)$params["page"];
      $resultQuery = $this->uomModel->get($numberPage);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "uom" => $resultQuery["query"],
              "numberEntries" => $resultQuery["numberEntries"],
              "numberPages" => $resultQuery["numberPages"]
          )
      )));
  }
}
?>

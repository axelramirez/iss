<?php
//Models
require_once(          "models/Tande_model.php");
/*
 **************************************************************************************************
 *************************Name: Purchase Order Class Controller
 *************************Description: Se encarga de agregar purchase orders
 **************************************************************************************************
*/
class Tande{
  private           $tandeModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->tandeModel = new Tande_model();
  }
  /*
   **************************************************************************************************

   **************************************************************************************************
  */

  public function insertTD(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $receivingId = $params["receivingId"];
      $ntds = $params["newTandE"];
     if($this->tandeModel->insert($ntds, $receivingId)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $ntds
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function updateTD(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $receivingId = $params["receivingId"];
      $tds = $params["tandE"];
     if($this->tandeModel->update($tds, $receivingId)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $tds
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function getTD(){
      $params = $_GET;
      $resultQuery = array();
      $receivingId = (Integer)$params["receivingId"];
      $resultQuery = $this->tandeModel->getByReceivingId($receivingId);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "tande" => $resultQuery["query"]
          )
      )));
  }
  public function deleteTD(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $purchaseOrderId = (Integer)$params["TandE"]["tandeId"];
     if($this->tandeModel->delete($purchaseOrderId)){
         echo(json_encode(array(
             "code" => 200
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
}
?>

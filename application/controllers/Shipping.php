<?php
//Models
require_once("models/Shipping_model.php");
require_once("models/Product_model.php");
require_once("models/Carrier_model.php");
//Helpers
require_once("helpers/validityStructureArray_helper.php");
require_once("helpers/validityPositiveNumber_helper.php");
require_once("helpers/simpleRequestURI_helper.php");
require_once("helpers/validityNumber_helper.php");
require_once("helpers/sanitizeString_helper.php");
require_once("helpers/formatDateYMD_helper.php");
require_once("helpers/validityDate_helper.php");
require_once("helpers/validityData_helper.php");
require_once("helpers/filterBy_helper.php");
/*
 **************************************************************************************************
 *************************Name: Shipping Class Controller
 *************************Description: Se encarga de agregar purchase orders
 **************************************************************************************************
*/
class Shipping{
  private $shippingModel, $productModel, $carrierModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->shippingModel = new Shipping_model();
    $this->productModel = new Product_model();
    $this->carrierModel = new Carrier_model();
  }

  public function getShippingSearchData(){
    $resultQuery = $this->carrierModel->getByOptions();
    $carriers = $resultQuery['query'];
    $resultQuery = $this->productModel->get();
    $products = $resultQuery['query'];
    header('Content-Type: application/json');
    echo(json_encode(array(
      'code' => 200,
      'response' => array(
        'carriers' => $carriers,
        'products' => $products
      )
    )));
  }
  //Function search shipping / validate array strcuture
   public function searchShipping(){
     $params     =               array();
     $params     =                 $_GET;
     //Se da formato a las fechas
     $isValid = TRUE;
     // if(validityStructureArray(array(
     //   'shippingNumber' => 'String',
     //   'pedimento' => 'String',
     //   'customerId' => 'UInteger',
     //   'receivingNo' => 'String',
     //   'trackingNo' => 'String',
     //   'inbond' => 'String',
     //   'productId' => 'UInteger',
     //   'carrierId' => 'UInteger',
     //   'driver' => 'String',
     //   'truckNumber' => 'String',
     //   'dateForm' => 'Date',
     //   'dateTo' => 'Date',
     //   'numberPage' => 'UInteger'
     // ), $params)){
     //   $numberPage = (Integer)$params['numberPage'];
     //   $params['dateFrom'] = (strcmp($params['dateFrom'], '') != 0 ) ? formatDateYMD($params['dateFrom']) : $params['dateFrom'];
     //   $params['dateTo'] = (strcmp($params['dateTo'], '') != 0 ) ? formatDateYMD($params['dateTo']) : $params['dateTo'];
     // }else{
     //   $isValid = FALSE;
     // }
     // header('content-type: application/json');
     // if(!$isValid || $numberPage <= 0){
     //   echo(json_encode(array(
     //     'code'     => 404
     //   )));
     //   return;
     // }
     $numberPage = (Integer)$params['numberPage'];
     // Se llama a la funcion del modelo que realiza la busqueda de shippings por un determinado filtro
     $resultQuery = $this->shippingModel->searchShippingBy($numberPage, $params, null);
     //Si la consulta arroja resultados, se regresan todas los shippings con su informacion de paginacion correspondiente
     if( count($resultQuery["query"]) > 0 ){
       echo(json_encode(array(
         "code"     =>   200,
         "response" => array(
           "numberPages"   =>   $resultQuery["numberPages"],
           "numberEntries" => $resultQuery["numberEntries"],
           "shippings"      =>         $resultQuery["query"],
           "nextPage"      =>               $numberPage + 1,
           "previousPage"  =>               $numberPage - 1,
           "numberPage" => $numberPage
         )
       )));
       return;
     }else{
       echo(json_encode(array(
         "code"     =>   404
       )));
     }
   }
   public function deleteShipping(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $idShipping = $params["idShipping"];
      if($this->shippingModel->deleteShipment($idShipping)){
          echo(json_encode(array(
              "code" => 200,
              "params" => $idShipping
          )));
      }else{
          echo(json_encode(array(
              "code" => 500
          )));
      }
   }
   public function getShipping(){
       $params = $_GET;
       $resultQuery = array();
       $shippingId = (Integer)$params["idShipping"];
       $resultQuery = $this->shippingModel->getByShippingId($shippingId);
       header("content-type: application/json");
       echo(json_encode(array(
           "code" => 200,
           "response" => array(
               "shippings" => $resultQuery["query"]
           )
       )));
   }
   public function getCharges(){
       $params = $_GET;
       $resultQuery = array();
       $customerId = (Integer)$params["customerId"];
       $resultQuery = $this->shippingModel->getChargesBy($customerId);
       header("content-type: application/json");
       echo(json_encode(array(
           "code" => 200,
           "response" => array(
               "charges" => $resultQuery["query"]
           )
       )));
   }
   public function getChargesApplied(){
       $params = $_GET;
       $resultQuery = array();
       $shippingId = (Integer)$params["shippingId"];
       $resultQuery = $this->shippingModel->getChargesAppliedBy($shippingId);
       header("content-type: application/json");
       echo(json_encode(array(
           "code" => 200,
           "response" => array(
               "charges" => $resultQuery["query"]
           )
       )));
   }
   public function getShippingDetails(){
       $params = $_GET;
       $resultQuery = array();
       $shippingId = (Integer)$params["idShipping"];
       $resultQuery = $this->shippingModel->getDetailsBy($shippingId);
       header("content-type: application/json");
       echo(json_encode(array(
           "code" => 200,
           "response" => array(
               "details" => $resultQuery["query"]
           )
       )));
   }
   public function insertReceivings(){
       $params = array();
       parse_str(file_get_contents('php://input'), $params);
       header("content-type: application/json");
       $idShipping = (Integer)$params["idShipping"];
       $data = $params["insertReceiving"];
      if($this->shippingModel->insertReceivinginShipping($idShipping, $data)){
          echo(json_encode(array(
              "code" => 200,
              "params" => $data
          )));
      }else{
          echo(json_encode(array(
              "code" => 500
          )));
      }
   }
   public function insertProducts(){
       $params = array();
       parse_str(file_get_contents('php://input'), $params);
       header("content-type: application/json");
       $idShipping = (Integer)$params["idShipping"];
       $data = $params["insertProducts"];
      if($this->shippingModel->insertProductinShipping($idShipping, $data)){
          echo(json_encode(array(
              "code" => 200,
              "params" => $data
          )));
      }else{
          echo(json_encode(array(
              "code" => 500
          )));
      }
   }
   public function getPartsDistinct(){
       $params = $_GET;
       $resultQuery = array();
       $shippingId = (Integer)$params["shippingId"];
       $filter = $params["filter"];
       $customerId = $params["customerId"];
       $resultQuery = $this->shippingModel->getPartsD($shippingId, $filter, $customerId);
       header("content-type: application/json");
       echo(json_encode(array(
           "code" => 200,
           "response" => array(
               "parts" => $resultQuery["query"],
               "listparts" => $resultQuery["productsSearch"]
           )
       )));
   }
   public function updateShipping(){
     $params = json_decode(file_get_contents('php://input'), true);
     $shippingId = 0;
     $shippingUpdate = array();
     $document = array();
     $documentInsert = array();
     $documentDelete = array();
     $trackings = array();
     $receivings = array();
     $products = array();

     //Get each parameter
     $shippingId = (Integer) (array_key_exists("shippingId", $params)) ? $params["shippingId"] : 0;
     $shippingUpdate = (array_key_exists("shipping", $params)) ? $params["shipping"] : array();
     $document = (array_key_exists("documents", $params)) ? $params["documents"] : array();
     $trackings = (array_key_exists("trackings", $params)) ? $params["trackings"] : array();
     $receivings = (array_key_exists("insertReceiving", $params)) ? $params["insertReceiving"] : array();
     $products = (array_key_exists("insertProducts", $params)) ? $params["insertProducts"] : array();

     //Accept Headers
     header('Content-Type : application/json');
     //Validity status type Integer in Trackings
     if(count($trackings) > 0){
         if(!validityCompleteArray(array(
             "status" => "Integer"
         ), $trackings)){
             echo(json_encode(array(
                 'code' => 406,
                 'response' => array(
                     'ident'   => 'tracking',
                     'message' => 'Error en el Status de Trackings, revisalo'
                 )
             )));
             return;
         }
     }
     //Validity status type Integer in Documents
     if(count($document) > 0){
         if(!validityCompleteArray(array(
             "status" => "Integer"
         ), $document)){
             echo(json_encode(array(
                 'code' => 406,
                 'response' => array(
                    'ident'   => 'document',
                    'message' => 'Error en el Status de Document, revisalo'
                 )
             )));
             return;
         }
     }
     //Validity status type Integer in Receivings
     if(count($receivings) > 0){
         if(!validityCompleteArray(array(
             "status" => "Integer"
         ), $receivings)){
             echo(json_encode(array(
                 'code' => 406,
                 'response' => array(
                    'ident'   => 'receivings',
                    'message' => 'Error en el Status de Receivings, revisalo'
                 )
             )));
             return;
         }
     }
     //Validity status type Integer in Products
     if(count($products) > 0){
         if(!validityCompleteArray(array(
             "status" => "Integer"
         ), $products)){
             echo(json_encode(array(
                 'code' => 406,
                 'response' => array(
                    'ident'   => 'products',
                    'message' => 'Error en el Status de Products, revisalo'
                 )
             )));
             return;
         }
     }

   }
}
?>

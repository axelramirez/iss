<?php
//Models
require_once(          "models/Uom_model.php");
/*
 **************************************************************************************************
 *************************Name: UOM Class Controller
 *************************Description: Se encarga de agregar uom
 **************************************************************************************************
*/
class Unit{
  private           $unitModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->unitModel = new Unit_model();
  }
  /*
   **************************************************************************************************

   **************************************************************************************************
  */
  public function insertUnit(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
     $unitName = $params["unitName"];
     if($this->unitModel->insert($unitName)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $unitName
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function updateUnit(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $unitName = $params["unitName"];
      $unitId = $params["unitId"];
     if($this->unitModel->update($unitName, $unitId)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $unitName
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function getUnit(){
      $params = $_GET;
      $resultQuery = array();
      $numberPage = (Integer)$params["page"];
      $resultQuery = $this->unitModel->getbypagination($numberPage);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "units" => $resultQuery["query"],
              "numberEntries" => $resultQuery["numberEntries"],
              "numberPages" => $resultQuery["numberPages"]
          )
      )));
  }
}
?>

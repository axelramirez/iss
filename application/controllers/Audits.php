<?php


require_once(          "models/Audit.php");

class Audits{
    private $audit;

    public function __construct(){
        $this->audit = new Audit();
    }
    public function getAudit(){
        $params = $_GET;
        $resultQuery = array();
        $numberPage = (Integer)$params["page"];
        $type = $params["type"];
        $action  = $params["action"];
        $startDate = $params["startDate"];
        $endDate = $params["endDate"];
        $number = $params["number"];
        $idcustomer = $params["idcustomer"];
        $iduser = $params["iduser"];
        $resultQuery = $this->audit->get($type,$action,$startDate,$endDate,$number,$idcustomer,$iduser,$numberPage);
        header("content-type: application/json");
        echo(json_encode(array(
            "code" => 200,
            "response" => array(
                "audit" => $resultQuery["query"],
                "numberEntries" => $resultQuery["numberEntries"],
                "numberPages" => $resultQuery["numberPages"]
            )
        )));
    }
}
?>

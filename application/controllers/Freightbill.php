<?php
//Models
require_once(          "models/Freightbill_model.php");
/*
 **************************************************************************************************
 *************************Name: Purchase Order Class Controller
 *************************Description: Se encarga de agregar purchase orders
 **************************************************************************************************
*/
class Freightbill{
  private           $freightbillModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->freightbillModel = new Freightbill_model();
  }
  /*
   **************************************************************************************************

   **************************************************************************************************
  */

  public function insertFB(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $receivingId = $params["receivingId"];
      $nfbs = $params["newFreightbill"];
     if($this->freightbillModel->insert($nfbs, $receivingId)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $nfbs
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function updateFB(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $receivingId = $params["receivingId"];
      $fbs = $params["freightBill"];
     if($this->freightbillModel->update($fbs, $receivingId)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $fbs
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function getFB(){
      $params = $_GET;
      $resultQuery = array();
      $receivingId = (Integer)$params["receivingId"];
      $resultQuery = $this->freightbillModel->getByReceivingId($receivingId);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "freightbill" => $resultQuery["query"]
          )
      )));
  }
  public function deletePO(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $purchaseOrderId = (Integer)$params["freightBill"]["freightBillId"];
     if($this->freightbillModel->delete($purchaseOrderId)){
         echo(json_encode(array(
             "code" => 200
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
}
?>

<?php


require_once(          "models/User.php");

class Users{
    private $user;

    public function __construct(){
        $this->user = new User();
    }

    public function insertUser(){
        $params = array();
        parse_str(file_get_contents('php://input'), $params);
        header("content-type: application/json");
      
       $login = $params["login"];
       $password = $params["password"];
       $roleid = $params["roleid"];
       $firstname = $params["firstname"];
       $lastname = $params["lastname"];
       if($this->user->insert($login,$password,$roleid,$firstname,$lastname)){
           echo(json_encode(array(
               "code" => 200,
               "params" => $login
           )));
       }else{
           echo(json_encode(array(
               "code" => 500
           )));
       }
    }

    public function updateUser(){
        $params = array();
        parse_str(file_get_contents('php://input'), $params);
        header("content-type: application/json");
        $userid = $params["userid"];
        $login = $params["login"];
        $roleid = $params["roleid"];
        $firstname = $params["firstname"];
        $lastname = $params["lastname"];
        if($this->user->update($login,$roleid,$firstname,$lastname,$userid)){
            echo(json_encode(array(
                "code" => 200,
                "params" => $login
            )));
        }else{
            echo(json_encode(array(
                "code" => 500
            )));
        }
    }

    public function getUser(){
        $params = $_GET;
        $resultQuery = array();
        $numberPage = (Integer)$params["page"];
        $firstname = $params["firstname"];
        $lastname = $params["lastname"];
        $roleid = $params["roleid"];
        $resultQuery = $this->user->get($numberPage,$firstname,$lastname,$roleid);
        header("content-type: application/json");
        echo(json_encode(array(
            "code" => 200,
            "response" => array(
                "user" => $resultQuery["query"],
                "numberEntries" => $resultQuery["numberEntries"],
                "numberPages" => $resultQuery["numberPages"]
            )
        )));
    }

  

    public function getUserResult(){
        $params = $_GET;
        $resultQuery = array();
        $userid = (Integer)$params["userid"];
        $pass = $params["pass"];
        $resultQuery = $this->user->getResult($userid,$pass);
        header("content-type: application/json");
        echo(json_encode(array(
            "code" => 200,
            "response" => array(
                "result" => $resultQuery["status"],
            )
        )));
    }


    public function updateUserResult(){
        $params = array();
        parse_str(file_get_contents('php://input'), $params);
        header("content-type: application/json");
      
        $userid = (Integer)$params["userID"];
        $pass = $params["pass"];
       if($this->user->updateResult($userid,$pass)){
           echo(json_encode(array(
               "code" => 200,
               "result" => $userid
           )));
       }else{
           echo(json_encode(array(
               "code" => 500
           )));
       }
    }



    public function checkLogin(){
        $params = $_GET;
        $resultQuery = array();
        $login = $params["login"];
      
        $resultQuery = $this->user->validateLogin($login);
        header("content-type: application/json");
        echo(json_encode(array(
            "code" => 200,
            "response" => array(
                "result" => $resultQuery["status"],
            )
        )));
    }


    public function deleteUser(){
        $params = array();
        parse_str(file_get_contents('php://input'), $params);
        header("content-type: application/json");
        $userid = $params["userid"];
        if($this->user->delete($userid)){
            echo(json_encode(array(
                "code" => 200,
                "params" => $userid
            )));
        }else{
            echo(json_encode(array(
                "code" => 500
            )));
        }
      }

}
?>
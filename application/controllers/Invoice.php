<?php
//Models
require_once(              "models/Invoice_model.php");
require_once(             "models/Customer_model.php");
require_once(            "models/Receiving_model.php");
require_once(             "models/Shipping_model.php");
require_once(          "models/InvoiceList_model.php");
require_once(          "models/AutoInvoice_model.php");
require_once(         "models/BillOfLading_model.php");
require_once(         "models/statusAmount_model.php");
require_once(      "models/PaymentReceived_model.php");
require_once(      "models/ShippingInvoice_model.php");
require_once(      "models/ServiceCustomer_model.php");
require_once(      "models/ServiceShipping_model.php");
require_once(      "models/ReceivingDetail_model.php");
require_once(     "models/ReceivingInvoice_model.php");
require_once(     "models/ServiceReceiving_model.php");
require_once(    "models/statusAutoInvoice_model.php");
require_once(    "models/CustomInvoiceData_model.php");
require_once(  "models/AutoinvoiceRecdeets_model.php");
require_once( "models/ServiceCustomInvoice_model.php");
require_once("models/ServiceStorageInvoice_model.php");
//Helpers
require_once("helpers/validityStructureArray_helper.php");
require_once("helpers/validityPositiveNumber_helper.php");
require_once(      "helpers/simpleRequestURI_helper.php");
require_once(        "helpers/validityNumber_helper.php");
require_once(        "helpers/sanitizeString_helper.php");
require_once(         "helpers/formatDateYMD_helper.php");
require_once(          "helpers/validityDate_helper.php");
require_once(          "helpers/validityData_helper.php");
/*
 **************************************************************************************************
 *************************Name: Invoice Class Controller
 *************************Description: Este controlador gestionara las peticiones para obtener
 ************************* Invoices y su informacion a detalle, entre otras operaciones
 **************************************************************************************************
*/
class Invoice{
  private $invoiceModel;
  private $customerModel;
  private $shippingModel;
  private $receivingModel;
  private $invoiceListModel;
  private $autoInvoiceModel;
  private $statusAmountModel;
  private $billOfLadingModel;
  private $paymentReceivedModel;
  private $shippingInvoiceModel;
  private $serviceCustomerModel;
  private $serviceShippingModel;
  private $receivingDetailModel;
  private $receivingInvoiceModel;
  private $serviceReceivingModel;
  private $statusAutoInvoiceModel;
  private $customInvoiceDataModel;
  private $autoInvoiceRecdeetsModel;
  private $serviceCustomInvoiceModel;
  private $serviceStorageInvoiceModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->invoiceModel                = new               Invoice_model();
    $this->customerModel               = new              Customer_model();
    $this->shippingModel               = new              Shipping_model();
    $this->receivingModel              = new             Receiving_model();
    $this->invoiceListModel            = new           InvoiceList_model();
    $this->autoInvoiceModel            = new           AutoInvoice_model();
    $this->statusAmountModel           = new          StatusAmount_model();
    $this->billOfLadingModel           = new          BillOfLading_model();
    $this->paymentReceivedModel        = new       PaymentReceived_model();
    $this->shippingInvoiceModel        = new       ShippingInvoice_model();
    $this->serviceCustomerModel        = new       ServiceCustomer_model();
    $this->serviceShippingModel        = new       ServiceShipping_model();
    $this->receivingDetailModel        = new       ReceivingDetail_model();
    $this->serviceReceivingModel       = new      ServiceReceiving_model();
    $this->receivingInvoiceModel       = new      ReceivingInvoice_model();
    $this->statusAutoInvoiceModel      = new     StatusAutoInvoice_model();
    $this->customInvoiceDataModel      = new     CustomInvoiceData_model();
    $this->autoInvoiceRecdeetsModel    = new   AutoinvoiceRecdeets_model();
    $this->serviceCustomInvoiceModel   = new  ServiceCustomInvoice_model();
    $this->serviceStorageInvoiceModel  = new ServiceStorageInvoice_model();
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de regresar todos los invoices y regresar su informacion organizada en
   paginacion
   **************************************************************************************************
  */
  public function getInvoices(){
    $resultQuery =                      array();
    $isValid = TRUE;
    if(validityStructureArray(array(
      "numberPage" => "UInteger"
    ), $_GET)){
      $numberPage  = (Integer)$_GET["numberPage"];
    }else{
      $isValid = FALSE;
    }
    header("content-type: application/json");
    if(!$isValid){
      echo(json_encode(array(
        "code"     => 404
      )));
      return;
    }
    //Verifica que el numero de paginas sea mayor a 0
    if($numberPage > 0){
      //Obtiene todas las facturas
      $resultQuery = $this->statusAutoInvoiceModel->getByDesc($numberPage);
      if(count($resultQuery["query"]) > 0){
        echo json_encode(array(
          "code"     =>   200,
          "response" => array(
            "numberPages"   =>   $resultQuery["numberPages"],
            "numberEntries" => $resultQuery["numberEntries"],
            "invoices"      =>         $resultQuery["query"],
            "nextPage"      =>               $numberPage + 1,
            "previousPage"  =>               $numberPage - 1
          )
        ));
      }else{
        echo json_encode(array(
          "code" => 404
        ));
      }
    }else{
      echo json_encode(array(
        "code" => 500
      ));
    }
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de regresar  un invoice y regresar toda su informacion organizada en
   paginacion
   **************************************************************************************************
  */
  public function getInvoice(){
    $resultQuery     =                      array();
    $invoiceDetail   =                      array();
    $invoicePayments =                      array();
    //Validity Number Page
    $isValid = TRUE;
    if(validityStructureArray(array(
      "numberPage" => "UInteger",
      "invoiceId"  => "UInteger"
    ), $_GET)){
      $numberPage  = (Integer)$_GET["numberPage"];
      $invoiceId       =  (Integer)$_GET["invoiceId"];
    }else{
      $isValid = FALSE;
    }
    header("content-type: application/json");
    if(!$isValid){
      echo(json_encode(array(
        "code"     => 404
      )));
      return;
    }
    //Revisa que el invoiceId sea mayor a 0
    if($invoiceId > 0){
      //Obtiene una factura por su id
      $resultQuery           =                       $this->statusAutoInvoiceModel->getById($invoiceId);
      $invoiceDetail         =                                     $resultQuery["query"]->fetch_assoc();
      if(count($invoiceDetail) > 0){
        //Obtiene los pagos de una factura por su id
        $resultQuery           = $this->paymentReceivedModel->getByAutoInvoiceId($invoiceId, $numberPage);
        $invoicePayments       =                                                    $resultQuery["query"];
        echo json_encode(array(
          "code"     =>   200,
          "response" => array(
            "numberPages"   =>   $resultQuery["numberPages"],
            "numberEntries" => $resultQuery["numberEntries"],
            "invoice"       =>                $invoiceDetail,
            "payments"      =>              $invoicePayments,
            "nextPage"      =>               $numberPage + 1,
            "previousPage"  =>               $numberPage - 1
          )
        ));
      }else{
        echo json_encode(array(
          "code" => 404
        ));
      }
    }else{
      echo json_encode(array(
        "code" => 500
      ));
    }
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de regresar todos los invoices que coincidan con los parametros de busqueda
   obtenidos y los regresa en forma de paginacion
   **************************************************************************************************
  */
  public function searchInvoice(){
    $params     =               array();
    $params     =                 $_GET;
    //Se da formato a las fechas
    $isValid = TRUE;
    if(validityStructureArray(array(
      "invoiceId"  => "UInteger",
      "customerId" => "UInteger",
      "invoiceType" => "Integer",
      "pedimentNo" => "UInteger",
      "dateTo" => "Date",
      "dateFrom" => "Date",
      "statusInvoice" => "Array",
      "numberPage" => "UInteger"
    ), $params)){
      if(validityStructureArray(array(
        "active" => "UInteger",
        "paid"  => "UInteger",
        "cancelled" => "UInteger"
      ), $params["statusInvoice"])){
        $numberPage = $params["numberPage"];
        $params["dateFrom"] = ( strcmp($params["dateFrom"], "") != 0 ) ? formatDateYMD($params["dateFrom"]) : $params["dateFrom"];
        $params["dateTo"]   =       ( strcmp($params["dateTo"], "") != 0 ) ? formatDateYMD($params["dateTo"]) : $params["dateTo"];
      }else{
        $isValid = FALSE;
      }
    }else{
      $isValid = FALSE;
    }

    header("content-type: application/json");
    if(!$isValid){
      echo(json_encode(array(
        "code"     => 404
      )));
      return;
    }

    //Se llama a la funcion del modelo que realiza la busqueda de facturas por un determinado filtro
    $resultQuery = $this->statusAutoInvoiceModel->searchInvoiceBy($numberPage, $params);
    //Si la consulta arroja resultados, se regresan todas las facturas con su informacion de paginacion correspondiente
    if( count($resultQuery["query"]) > 0 ){
      echo(json_encode(array(
        "code"     =>   200,
        "response" => array(
          "numberPages"   =>   $resultQuery["numberPages"],
          "numberEntries" => $resultQuery["numberEntries"],
          "invoices"      =>         $resultQuery["query"],
          "nextPage"      =>               $numberPage + 1,
          "previousPage"  =>               $numberPage - 1
        )
      )));
      return;
    }else{
      echo(json_encode(array(
        "code"     =>   404
      )));
    }
  }
  /*
   **************************************************************************************************
   Muestra la vista de busqueda de facturas
   **************************************************************************************************
  */
  public function viewInvoices(){
    $URI = simpleRequestURI($_SERVER["REQUEST_URI"]);
    //Se renderiza la vista de busque de facturas
    require_once("views/Invoice/payments_invoices.php");
  }

  /*
   **************************************************************************************************
   Esta funcion se encarga de regresar la vista de edicion de invoice dependiendo el tipo de invoice
   **************************************************************************************************
  */
  public function editInvoiceByType(){
    $params      =            array();
    $params      =              $_GET;
    $invoice     =            array();
    $resultQuery =            array();
    //Se valida que el datode invoiceId sea numerico
    $isValid = TRUE;
    if(validityStructureArray(array(
      "invoiceId"  => "UInteger"
    ), $params)){
      $invoiceId   = $_GET["invoiceId"];
    }else{
      $isValid = FALSE;
    }
    if(!$isValid){
      require_once("views/Errors/404.php");
      return;
    }
    $URI         = simpleRequestURI($_SERVER["REQUEST_URI"]);
    //Se obtiene la factura a partir de su id
    $resultQuery = $this->statusAutoInvoiceModel->getById($invoiceId);
    //Se obtiene la factura para ese id
    $resultQuery  =                                $this->statusAutoInvoiceModel->getById($invoiceId);
    $invoice      =                                              $resultQuery["query"]->fetch_assoc();

    //Se verifica si existe la factura
    if( count($invoice) == 0 ){
      $message = "";
      require_once("views/Errors/404.php");
      return;
    }
    if((Integer)$invoice["isCancelled"] == 1){
      $message = "La factura esta cancelada, no es posible vizualizarla.";
      require_once("views/Errors/403.php");
      return;
    }

    if((Integer)$invoice["havePayments"] == 1 || (Integer)$invoice["isPaid"] == 1){
      $message = "La factura ya tiene pagos o ya esta pagada, no es posible actualizarla.";
      require_once("views/Errors/404.php");
      return;
    }
    //Se verifica el tipo de factura y renderiza una vista apropiada dependiendo del tipo de factura
    switch((Integer)$invoice["typeInvoice"]){
      case 0:
      require_once("views/Invoice/Edit/in_out_invoice.php");
      break;
      case 1:
      require_once("views/Invoice/Edit/custom_invoice.php");
      break;
      case 2:
      require_once("views/Invoice/Edit/storage_invoice.php");
      break;
      case 3:
      require_once("views/Invoice/Edit/p&p_invoice.php");
      break;
      default:
      $message = "No existe una vista para mostrar ese tipo de factura";
      require_once("views/Errors/404.php");
      return;
    }
  }

  /*
   **************************************************************************************************
   Esta funcion se encarga de regresar la vista de edicion de invoice dependiendo el tipo de invoice
   **************************************************************************************************
  */
  public function viewInvoiceByType(){
    $params      =            array();
    $params      =              $_GET;
    $invoice     =            array();
    $resultQuery =            array();
    //Se valida que el datode invoiceId sea numerico
    $isValid = TRUE;
    if(validityStructureArray(array(
      "invoiceId"  => "UInteger"
    ), $params)){
      $invoiceId   = $_GET["invoiceId"];
    }else{
      $isValid = FALSE;
    }
    if(!$isValid){
      require_once("views/Errors/404.php");
      return;
    }
    $URI         = simpleRequestURI($_SERVER["REQUEST_URI"]);
    //Se obtiene la factura para ese id
    $resultQuery  =                                $this->statusAutoInvoiceModel->getById($invoiceId);
    $invoice      =                                              $resultQuery["query"]->fetch_assoc();

    //Se verifica si existe la factura
    if( count($invoice) == 0 ){
      $message = "";
      require_once("views/Errors/404.php");
      return;
    }
    if((Integer)$invoice["isCancelled"] == 1){
      $message = "La factura esta cancelada, no es posible vizualizarla.";
      require_once("views/Errors/403.php");
      return;
    }
    //Se verifica el tipo de factura y renderiza una vista apropiada dependiendo del tipo de factura
    switch((Integer)$invoice["typeInvoice"]){
      case 0:
      require_once("views/Invoice/Detail/in_out_invoice.php");
      break;
      case 1:
      require_once("views/Invoice/Detail/custom_invoice.php");
      break;
      case 2:
      require_once("views/Invoice/Detail/storage_invoice.php");
      break;
      case 3:
      require_once("views/Invoice/Detail/p&p_invoice.php");
      break;
      default:
      $message = "No existe una vista para mostrar ese tipo de factura";
      require_once("views/Errors/404.php");
      return;
    }
  }

  public function cancelInvoice(){
    $isValid = TRUE;
    if(validityStructureArray(array(
      "invoiceId"  => "UInteger"
    ), $_GET)){
      $invoiceId   = $_GET["invoiceId"];
    }else{
      $isValid = FALSE;
    }
    header("content-type: application/json");
    if(!$isValid){
      echo(json_encode(array(
        "code" => 500
      )));
      return;
    }
    //Se obtiene la factura para ese id
    $resultQuery  =                                $this->statusAutoInvoiceModel->getById($invoiceId);
    $invoice      =                                              $resultQuery["query"]->fetch_assoc();
    //Se verifica si existe la factura
    if( count($invoice) == 0 ){
      echo(json_encode(array(
        "code" => 404
      )));
      return;
    }
    if((Integer)$invoice["isCancelled"] == 1){
      echo json_encode(array(
        "code" => 400,
        "message" => "La factura ya esta cancelada."
      ));
      return;
    }
    if((Integer)$invoice["havePayments"] == 1 || (Integer)$invoice["isPaid"] == 1){
      echo json_encode(array(
        "code" => 400,
        "message" => "La factura ya tiene pagos o ya esta pagada, no es posible cancelarla."
      ));
      return;
    }
    //Se verifica el tipo de factura y renderiza una vista apropiada dependiendo del tipo de factura
    switch((Integer)$invoice["typeInvoice"]){
      case 0:
      $this->autoInvoiceModel->cancelAutoInvoice($invoiceId);
      $this->shippingModel->update($invoiceId);
      $this->receivingModel->updateRec($invoiceId);
      $this->serviceShippingModel->update($invoiceId);
      $this->serviceReceivingModel->update($invoiceId);
      $this->customerModel->updateDueAmount($invoice["customer_id"], $invoice["dueInvoice"]);
      break;
      case 1:
      $this->autoInvoiceModel->cancelAutoInvoice($invoiceId);
      $this->customInvoiceDataModel->delete($invoiceId);
      $this->serviceCustomInvoiceModel->deleteServices($invoiceId);
      break;
      case 2:
      $this->autoInvoiceModel->cancelAutoInvoice($invoiceId);
      $resultQuery = $this->autoInvoiceRecdeetsModel->getReceivingDetailToUpdate($invoiceId);
      $resultQuery = $resultQuery["query"];
      $this->receivingDetailModel->updateByAutoInvoiceRecdeets($resultQuery);
      $this->autoInvoiceRecdeetsModel->updateRecdeets($invoiceId);
      $this->serviceStorageInvoiceModel->deleteServices($invoiceId);
      $this->customerModel->updateDueAmount($invoice["customer_id"], $invoice["dueInvoice"]);
      break;
      case 3:
      $this->autoInvoiceModel->cancelAutoInvoice($invoiceId);
      $this->shippingModel->update($invoiceId);
      $this->serviceShippingModel->update($invoiceId);
      $this->customerModel->updateDueAmount($invoice["customer_id"], $invoice["dueInvoice"]);
      break;
      default:
      echo json_encode(array(
        "code" => 400,
        "message" => "La factura no tiene un tipo valido para el sistema, no se pudo concretar la accion."
      ));
      return;
    }
    echo json_encode(array(
      "code" => 200
    ));
  }

  /*
   **************************************************************************************************
   Esta funcion se encarga de regresar la informacion de edicion para un determinado tipo de factura.
   **************************************************************************************************
  */
  public function getInvoiceInfoInOut(){
    $self              =    null;
    $params            =   $_GET;
    $invoice           = array();
    $invoiceId         =       0;
    $resultQuery       = array();
    $creditInvoice     =       0;
    $shippingsInvoice  = array();
    $receivingsInvoice = array();
    header("Content-Type: application/json");
    $isValid = TRUE;
    if(validityStructureArray(array(
      "invoiceId"  => "UInteger"
    ), $params)){
      $invoiceId   = (Integer)$params["invoiceId"];
    }else{
      $isValid = FALSE;
    }
    header("content-type: application/json");
    if(!$isValid){
      echo(json_encode(array(
        "code" => 500
      )));
      return;
    }
    $invoiceId         =                                           (Integer)$params["invoiceId"];
    //Se obtiene la factura a partir de su id
    $resultQuery       =                      $this->statusAutoInvoiceModel->getById($invoiceId);
    //Se verifica que se obtengan resultados
    if( $resultQuery["query"]->num_rows == 0 ){
      echo json_encode(array(
        "code" => 404
      ));
      return;
    }
    //Se obtienen los datos de la factura
    $invoice           =                         $resultQuery["query"]->fetch_assoc();

    if((Integer)$invoice["isCancelled"] == 1){
      echo json_encode(array(
        "code" => 400,
        "message" => "La factura esta cancelada."
      ));
      return;
    }
    if((Integer)$invoice["typeInvoice"] !== 0){
      echo json_encode(array(
        "code" => 400,
        "message" => "Not found"
      ));
      return;
    }
    //Se obtienen los servicios de extra cargos del cliente y se verifica que tenga servicios el customer
    $resultQuery                 = $this->serviceCustomerModel->getByCustomerId($invoice["customer_id"]);
    $invoice["servicesCustomer"] =                                                 $resultQuery["query"];
    $invoice["haveServicesCustomer"] = count($invoice["servicesCustomer"]) > 0;
    //Se declara en el arreglo de invoice el subtotal de la factura igualandolo al total de invoice
    $invoice["subTotalInvoice"] = $invoice["totalInvoice"];
    //Se obtienen los receiving de la factura
    $resultQuery       = $this->receivingInvoiceModel->getByAutoInvoiceId($invoiceId);
    $receivingsInvoice =                                        $resultQuery["query"];

    //Se recorre todos los receivings de invoice para calcular el total del receivingy crear un nuevo arreglo
    $receivingsInvoice = array_map(function($receiving){
      $totalReceiving  =                                                                                                    0;
      $totalReceiving +=                                                     (Double)$receiving["totalCostServiceReceiving"];
      $totalReceiving +=                    (Double)( (Boolean)$receiving["hazmatReceiving"] ? $receiving["hazmatCost"] : 0);
      $totalReceiving += (Double)((Boolean)$receiving["boundReceiving"] ? $receiving["northCost"] : $receiving["southCost"]);
      $receiving["totalReceiving"] =                                                                          $totalReceiving;
      return $receiving;
    }, $receivingsInvoice);

    //Se obtienen los shipping de la factura
    $resultQuery = $this->shippingInvoiceModel->getByAutoInvoiceId($invoiceId);
    $shippingsInvoice = $resultQuery["query"];
    //Se define el valor de self como invoiceListModel, ya que se utilizara dentro de una funcion anonima
    $self = $this->invoiceListModel;

    //Se recorren los shippings de invoice para calcular el total del shipping, obtener el tipo de cada invoicelist y los dias de almacenaje de estos, asi como obtener el credito total
    $shippingsInvoice = array_map(function($shipping) use ($self, & $creditInvoice){
      $haveCredit = $shipping["haveCredit"];
      //Se calcula el total del shipping
      $shipping["totalShipping"] = $shipping["subTotalShipping"] + $shipping["totalCostServiceShipping"];
      //se obtienen los invocielist de el invoice_id del shipping
      $resultQuery = $self->getByIdAndType($shipping["invoice_id"]);
      $shipping["invoiceslist"] = $resultQuery["query"];

      //Se recorren los invoicelist para obtener el tipo de invoicelist, el tipo de paquete y sus dias de almacenaje, si es tipo storage el invoicelist
      $shipping["invoiceslist"] = array_map(function($invoiceList){
        //Se obtiene un arreglo a partir de la descripcion y los espacios que contenga
        $description = explode(" ", $invoiceList["description"]);
        //Se almacena el tipo de paquete
        $invoiceList["typePackage"] = $description[0];
        //Se verifica que tipo es el invoicelist, y almacena el correspondiente
        if( strcmp($description[1], "Handling") == 0 ){
          $invoiceList["typeInvoiceList"] = "Handling";
        }else{
          $invoiceList["typeInvoiceList"] = "Storage";
          //Se obtiene un arreglo a partir de el valor en la segunda posicion del arreglo $description y los : que contenga
          $storageInfo = explode(":", $description[1]);
          //Se almacenan los dias de almacenaje de invoicelist
          $invoiceList["daysStorage"]     = $storageInfo[1];
        }
        return $invoiceList;
      }, $shipping["invoiceslist"]);

      if($haveCredit){
        $invoicelists = array();
        foreach($shipping["invoiceslist"] as $invoicelist){
          if(strcmp($invoicelist["typeInvoiceList"], "Storage") != 0){
            array_push($invoicelists, $invoicelist);
          }
        }
        $shipping["invoiceslist"] = $invoicelists;
      }

      //Se suma al credito el credito del shipping, esta variable es de referencia
      $creditInvoice += $shipping["creditShipping"];
      return $shipping;
    }, $shippingsInvoice);

    //Se almacena el credito total en la factura y al total de la factura se le resta el credito total
    $invoice["creditInvoice"]  = $creditInvoice;
    // $invoice["totalInvoice"]  -= $creditInvoice;
    echo(json_encode(array(
      "code" => 200,
      "response" => array(
        "invoice"           =>           $invoice,
        "receivingsInvoice"  =>  (is_null($receivingsInvoice)!=TRUE) ? $receivingsInvoice : array(),
        "shippingsInvoice"  =>  (is_null($shippingsInvoice)!=TRUE) ? $shippingsInvoice : array()
        )
    )));
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de regresar la informacion de edicion para un determinado tipo de factura.
   **************************************************************************************************
  */
  public function getInvoiceInfoPAndP(){
    $self              =    null;
    $params            =   $_GET;
    $invoice           = array();
    $invoiceId         =       0;
    $resultQuery       = array();
    $creditInvoice     =       0;
    $shippingsInvoice  = array();
    $receivingsInvoice = array();
    header("Content-Type: application/json");
    //Se valida que el datode invoiceId sea numerico
    $isValid = TRUE;
    if(validityStructureArray(array(
      "invoiceId"  => "UInteger"
    ), $params)){
      $invoiceId   = (Integer)$params["invoiceId"];
    }else{
      $isValid = FALSE;
    }
    header("content-type: application/json");
    if(!$isValid){
      echo(json_encode(array(
        "code" => 500
      )));
      return;
    }
    //Se obtiene la factura a partir de su id
    $resultQuery       =                      $this->statusAutoInvoiceModel->getById($invoiceId);
    //Se verifica que se obtengan resultados
    if( $resultQuery["query"]->num_rows == 0 ){
      echo json_encode(array(
        "code" => 404
      ));
      return;
    }
    //Se obtienen los datos de la factura
    $invoice           =                         $resultQuery["query"]->fetch_assoc();

    if((Integer)$invoice["isCancelled"] == 1){
      echo json_encode(array(
        "code" => 400,
        "message" => "La factura esta cancelada."
      ));
      return;
    }

    if((Integer)$invoice["typeInvoice"] !== 3){
      echo json_encode(array(
        "code" => 400,
        "message" => "Not found"
      ));
      return;
    }
    //Se obtienen los servicios de extra cargos del cliente y se verifica que tenga servicios el customer
    $resultQuery                 = $this->serviceCustomerModel->getByCustomerId($invoice["customer_id"]);
    $invoice["servicesCustomer"] =                                                 $resultQuery["query"];
    $invoice["haveServicesCustomer"] = count($invoice["servicesCustomer"]) > 0;
    //Se declara en el arreglo de invoice el subtotal de la factura igualandolo al total de invoice
    $invoice["subTotalInvoice"] = $invoice["totalInvoice"];

    //Se obtienen los shipping de la factura
    $resultQuery = $this->shippingInvoiceModel->getByAutoInvoiceId($invoiceId);
    $shippingsInvoice = $resultQuery["query"];
    //Se define el valor de self como invoiceListModel, ya que se utilizara dentro de una funcion anonima
    $self = $this->invoiceListModel;

    //Se recorren los shippings de invoice para calcular el total del shipping, obtener el tipo de cada invoicelist y los dias de almacenaje de estos, asi como obtener el credito total
    $shippingsInvoice = array_map(function($shipping) use ($self, & $creditInvoice){
      $haveCredit = $shipping["haveCredit"];
      //Se calcula el total del shipping
      $shipping["totalShipping"] = $shipping["subTotalShipping"] + $shipping["totalCostServiceShipping"];
      //se obtienen los invocielist de el invoice_id del shipping
      $resultQuery = $self->getByIdAndType($shipping["invoice_id"]);
      $shipping["invoiceslist"] = $resultQuery["query"];

      //Se recorren los invoicelist para obtener el tipo de invoicelist, el tipo de paquete y sus dias de almacenaje, si es tipo storage el invoicelist
      $shipping["invoiceslist"] = array_map(function($invoiceList){
        //Se obtiene un arreglo a partir de la descripcion y los espacios que contenga
        $description = explode(" ", $invoiceList["description"]);
        //Se almacena el tipo de paquete
        $invoiceList["typePackage"] = $description[0];
        //Se verifica que tipo es el invoicelist, y almacena el correspondiente
        if( strcmp($description[1], "Handling") == 0 ){
          $invoiceList["typeInvoiceList"] = "Handling";
        }else{
          $invoiceList["typeInvoiceList"] = "Storage";
          //Se obtiene un arreglo a partir de el valor en la segunda posicion del arreglo $description y los : que contenga
          $storageInfo = explode(":", $description[1]);
          //Se almacenan los dias de almacenaje de invoicelist
          $invoiceList["daysStorage"]     = $storageInfo[1];
        }
        return $invoiceList;
      }, $shipping["invoiceslist"]);

      $invoicelists = array();
      foreach($shipping["invoiceslist"] as $invoicelist){
        if(strcmp($invoicelist["typeInvoiceList"], "Storage") != 0){
          array_push($invoicelists, $invoicelist);
        }
      }
      $shipping["invoiceslist"] = $invoicelists;

      //Se suma al credito el credito del shipping, esta variable es de referencia
      $creditInvoice += $shipping["creditShipping"];
      return $shipping;
    }, $shippingsInvoice);

    //Se almacena el credito total en la factura y al total de la factura se le resta el credito total
    $invoice["creditInvoice"]  = $creditInvoice;
    // $invoice["totalInvoice"]  -= $creditInvoice;
    echo(json_encode(array(
      "code" => 200,
      "response" => array(
        "invoice"           =>           $invoice,
        "shippingsInvoice"  =>  (is_null($shippingsInvoice)!=TRUE) ? $shippingsInvoice : array()
        )
    )));
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de regresar la informacion de edicion para un determinado tipo de factura.
   **************************************************************************************************
  */
  public function getInvoiceInfoStorage(){
    $self              =    null;
    $params            =   $_GET;
    $invoice           = array();
    $invoiceId         =       0;
    $resultQuery       = array();
    $creditInvoice     =       0;
    $receivingsDetailInvoice = array();
    header("Content-Type: application/json");
    //Se valida que el datode invoiceId sea numerico
    $isValid = TRUE;
    if(validityStructureArray(array(
      "invoiceId"  => "UInteger"
    ), $params)){
      $invoiceId   = (Integer)$params["invoiceId"];
    }else{
      $isValid = FALSE;
    }
    header("content-type: application/json");
    if(!$isValid){
      echo(json_encode(array(
        "code" => 500
      )));
      return;
    }
    //Se obtiene la factura a partir de su id
    $resultQuery       =                      $this->statusAutoInvoiceModel->getById($invoiceId);
    //Se verifica que se obtengan resultados
    if( $resultQuery["query"]->num_rows == 0 ){
      echo json_encode(array(
        "code" => 404
      ));
      return;
    }
    //Se obtienen los datos de la factura
    $invoice           =                         $resultQuery["query"]->fetch_assoc();

    if((Integer)$invoice["isCancelled"] == 1){
      echo json_encode(array(
        "code" => 400,
        "message" => "La factura esta cancelada."
      ));
      return;
    }

    if((Integer)$invoice["typeInvoice"] !== 2){
      echo json_encode(array(
        "code" => 400,
        "message" => "Not found"
      ));
      return;
    }
    //Se obtienen los servicios de extra cargos del cliente y se verifica que tenga servicios el customer
    $resultQuery                 = $this->serviceCustomerModel->getByCustomerId($invoice["customer_id"]);
    $invoice["servicesCustomer"] =                                                 $resultQuery["query"];
    $invoice["haveServicesCustomer"] = count($invoice["servicesCustomer"]) > 0;
    $invoice["subTotalInvoice"] = $invoice["totalInvoice"];
    //
    $resultQuery = $this->autoInvoiceRecdeetsModel->getByAutoInvoiceId($invoiceId);
    $receivingsDetailInvoice = $resultQuery["query"];
    $resultQuery = $this->serviceStorageInvoiceModel->getTotalByAutoInvoiceId($invoiceId);
    $invoice["totalServices"] = $resultQuery["query"]["totalServices"];
    echo(json_encode(array(
      "code" => 200,
      "response" => array(
        "invoice"           =>           $invoice,
        "receivingsDetails"  =>  (is_null($receivingsDetailInvoice) != TRUE) ? $receivingsDetailInvoice : array()
        )
    )));
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de regresar la informacion de edicion para un determinado tipo de factura.
   **************************************************************************************************
  */
  public function getInvoiceInfoCustom(){
    $self              =    null;
    $params            =   $_GET;
    $invoice           = array();
    $invoiceId         =       0;
    $resultQuery       = array();
    $creditInvoice     =       0;
    $receivingsDetailInvoice = array();
    header("Content-Type: application/json");
    //Se valida que el datode invoiceId sea numerico
    $isValid = TRUE;
    if(validityStructureArray(array(
      "invoiceId"  => "UInteger"
    ), $params)){
      $invoiceId   = (Integer)$params["invoiceId"];
    }else{
      $isValid = FALSE;
    }
    if(!$isValid){
      echo(json_encode(array(
        "code" => 500
      )));
      return;
    }
    //Se obtiene la factura a partir de su id
    $resultQuery       =                      $this->statusAutoInvoiceModel->getById($invoiceId);
    //Se verifica que se obtengan resultados
    if( $resultQuery["query"]->num_rows == 0 ){
      echo json_encode(array(
        "code" => 404
      ));
      return;
    }
    //Se obtienen los datos de la factura
    $invoice           =                         $resultQuery["query"]->fetch_assoc();

    if((Integer)$invoice["isCancelled"] == 1){
      echo json_encode(array(
        "code" => 400,
        "message" => "La factura esta cancelada."
      ));
      return;
    }

    if((Integer)$invoice["typeInvoice"] !== 1){
      echo json_encode(array(
        "code" => 400,
        "message" => "Not found"
      ));
      return;
    }
    //Se obtienen los servicios de extra cargos del cliente y se verifica que tenga servicios el customer
    $resultQuery                 = $this->serviceCustomerModel->getByCustomerId($invoice["customer_id"]);
    $invoice["servicesCustomer"] =                                                 $resultQuery["query"];
    $invoice["haveServicesCustomer"] = count($invoice["servicesCustomer"]) > 0;
    $invoice["subTotalInvoice"] = $invoice["totalInvoice"];
    $resultQuery = $this->serviceCustomInvoiceModel->getTotalByAutoInvoiceId($invoiceId);
    $invoice["totalServices"] = $resultQuery["query"]["totalServices"];
    echo(json_encode(array(
      "code" => 200,
      "response" => array(
        "invoice" => $invoice
        )
    )));
  }
  /*
   **************************************************************************************************
   Obtiene los extra cargos de shipping para una factura
   **************************************************************************************************
  */
  public function getExtraChargesByShipping(){
    header('Content-Type: application/json');
    $isValid = TRUE;
    if(validityStructureArray(array(
      "invoiceId"  => "UInteger",
      "shippingId" => "UInteger"
    ), $_GET)){
      $invoiceId  = $_GET["invoiceId"];
      $shippingId = $_GET["shippingId"];
    }else{
      $isValid = FALSE;
    }
    if(!$isValid){
      echo(json_encode(array(
        "code" => 500
      )));
      return;
    }
    //Se obtiene la factura a partir de su id
    $resultQuery = $this->statusAutoInvoiceModel->getById($invoiceId);
    //Se verifica que se obtengan resultados
    if( $resultQuery["query"]->num_rows == 0 ){
      echo json_encode(array(
        "code" => 500
      ));
      return;
    }
    //Se obtienen los datos de la factura
    $invoice = $resultQuery["query"]->fetch_assoc();

    if((Integer)$invoice["typeInvoice"] !== 3 && (Integer)$invoice["typeInvoice"] !== 0){
      echo json_encode(array(
        "code" => 500
      ));
      return;
    }

    if((Integer)$invoice["isCancelled"] == 1){
      echo json_encode(array(
        "code" => 500
      ));
      return;
    }

    $resultQuery = $this->serviceShippingModel->getByShippingIdAndAutoInvoiceId($shippingId, $invoiceId);
    if( count($resultQuery["query"]) != 0 ){
      echo(json_encode(
        array(
          "code"     =>                   200,
          "response" => array(
            "charges" => $resultQuery["query"]
          )
        )
      ));
    }else{
      echo(json_encode(
        array(
          "code"     => 404
        )
      ));
    }
  }
  /*
   **************************************************************************************************
   Obtiene los extra cargos de shipping para una factura
   **************************************************************************************************
  */
  public function getExtraChargesByStorage(){
    header('Content-Type: application/json');
    $isValid = TRUE;
    if(validityStructureArray(array(
      "invoiceId"  => "UInteger"
    ), $_GET)){
      $invoiceId  = $_GET["invoiceId"];
    }else{
      $isValid = FALSE;
    }
    if(!$isValid){
      echo(json_encode(array(
        "code" => 500
      )));
      return;
    }

    $resultQuery = $this->statusAutoInvoiceModel->getById($invoiceId);
    //Se verifica que se obtengan resultados
    if( $resultQuery["query"]->num_rows == 0 ){
      echo json_encode(array(
        "code" => 500
      ));
      return;
    }
    //Se obtienen los datos de la factura
    $invoice = $resultQuery["query"]->fetch_assoc();

    if((Integer)$invoice["typeInvoice"] !== 2){
      echo json_encode(array(
        "code" => 500
      ));
      return;
    }

    if((Integer)$invoice["isCancelled"] == 1){
      echo json_encode(array(
        "code" => 500
      ));
      return;
    }

    $resultQuery = $this->serviceStorageInvoiceModel->getByAutoInvoiceId($invoiceId);
    if( count($resultQuery["query"]) != 0 ){
      echo(json_encode(
        array(
          "code"     =>                   200,
          "response" => array(
            "charges" => $resultQuery["query"]
          )
        )
      ));
    }else{
      echo(json_encode(
        array(
          "code"     => 404
        )
      ));
    }
  }

  /*
   **************************************************************************************************
   Obtiene los extra cargos de shipping para una factura
   **************************************************************************************************
  */
  public function getExtraChargesByCustom(){
    header('Content-Type: application/json');
    $isValid = TRUE;
    if(validityStructureArray(array(
      "invoiceId"  => "UInteger"
    ), $_GET)){
      $invoiceId  = $_GET["invoiceId"];
    }else{
      $isValid = FALSE;
    }
    if(!$isValid){
      echo(json_encode(array(
        "code" => 500
      )));
      return;
    }

    $resultQuery = $this->statusAutoInvoiceModel->getById($invoiceId);
    //Se verifica que se obtengan resultados
    if( $resultQuery["query"]->num_rows == 0 ){
      echo json_encode(array(
        "code" => 500
      ));
      return;
    }
    //Se obtienen los datos de la factura
    $invoice = $resultQuery["query"]->fetch_assoc();

    if((Integer)$invoice["typeInvoice"] !== 1){
      echo json_encode(array(
        "code" => 500
      ));
      return;
    }

    if((Integer)$invoice["isCancelled"] == 1){
      echo json_encode(array(
        "code" => 500
      ));
      return;
    }

    $resultQuery = $this->serviceCustomInvoiceModel->getByAutoInvoiceId($invoiceId);

    if( count($resultQuery["query"]) != 0 ){
      echo(json_encode(
        array(
          "code"     =>                   200,
          "response" => array(
            "charges" => $resultQuery["query"]
          )
        )
      ));
    }else{
      echo(json_encode(
        array(
          "code"     => 404
        )
      ));
    }
  }
  /*
   **************************************************************************************************
   Obtiene los extra cargos de receiving para una factura
   **************************************************************************************************
  */
  public function getExtraChargesByReceiving(){
    header('Content-Type: application/json');
    $isValid = TRUE;
    if(validityStructureArray(array(
      "invoiceId"  => "UInteger",
      "receivingId" => "UInteger"
    ), $_GET)){
      $invoiceId  = $_GET["invoiceId"];
      $receivingId = $_GET["receivingId"];
    }else{
      $isValid = FALSE;
    }
    if(!$isValid){
      echo(json_encode(array(
        "code" => 500
      )));
      return;
    }
    //Se obtiene la factura a partir de su id
    $resultQuery = $this->statusAutoInvoiceModel->getById($invoiceId);
    //Se verifica que se obtengan resultados
    if( $resultQuery["query"]->num_rows == 0 ){
      echo json_encode(array(
        "code" => 500
      ));
      return;
    }
    //Se obtienen los datos de la factura
    $invoice = $resultQuery["query"]->fetch_assoc();

    if((Integer)$invoice["typeInvoice"] !== 0){
      echo json_encode(array(
        "code" => 500
      ));
      return;
    }

    if((Integer)$invoice["isCancelled"] == 1){
      echo json_encode(array(
        "code" => 500
      ));
      return;
    }

    $resultQuery = $this->serviceReceivingModel->getByReceivingIdAndAutoInvoiceId($receivingId, $invoiceId);

    if( count($resultQuery["query"]) != 0 ){
      echo(json_encode(
        array(
          "code"     =>                   200,
          "response" => array(
            "charges" => $resultQuery["query"]
          )
        )
      ));
    }else{
      echo(json_encode(
        array(
          "code"     => 404
        )
      ));
    }
  }
  /*
   **************************************************************************************************
   Obtiene los extra cargos de receiving para una factura
   **************************************************************************************************
  */
  public function saveInvoiceInOut(){
    $params                      = array();
    $invoice                     = array();
    $invoices                    = array();
    $invoiceId                   =       0;
    $shippings                   = array();
    $receiving                   = array();
    $extraTotal                  =       0;
    $invoiceIds                  = array();
    $shippingEC                  = array();
    $receivingEC                 = array();
    $resultQuery                 = array();
    $creditTotal                 =       0;
    $extraCharges                = array();
    $receivingAll                = array();
    $invoiceUpdate               = array();
    $invoicesUpdate              = array();
    $invoicelistAll              = array();
    $receivingBound              = array();
    $subTotalInvoice             =       0;
    $shippingsUpdate             = array();
    $receivingHazmat             = array();
    $servicesCustomer            = array();
    $shippingsInvoice            = array();
    $receivingsUpdate            = array();
    $receivingsInvoice           = array();
    $invoiceListUpdate           = array();
    $invoicelistStorage          = array();
    $billOfLadingUpdate          = array();
    $invoicelistHandling         = array();
    $extraChargesShippingInsert  = array();
    $extraChargesShippingDelete  = array();
    $extraChargesReceivingInsert = array();
    $extraChargesReceivingDelete = array();
    //Se obtienen los datos mediante la peticion PUT
    parse_str(file_get_contents('php://input'), $params);
    //Se obtiene cada dato
    if(array_key_exists("invoiceId", $params)){
      if(!checkValidity("UInteger", $params["invoiceId"])){
        echo(json_encode(array(
          "code" => 404
        )));
        return;
      }
      $invoiceId = (Integer)$params["invoiceId"];
    }else{
      echo(json_encode(array(
        "code" => 404
      )));
      return;
    }
    $dateStart    = ( array_key_exists("invoiceStartDate", $params) ) ? $params["invoiceStartDate"] : "";
    $dateEnd    = ( array_key_exists("invoiceEndDate", $params) ) ? $params["invoiceEndDate"] : "";
    $shippings    =       ( array_key_exists("shippings", $params) ) ? $params["shippings"] : array();
    $receivings   =     ( array_key_exists("receivings", $params) ) ? $params["receivings"] : array();
    $extraCharges = ( array_key_exists("extraCharges", $params) ) ? $params["extraCharges"] : array();

    //Se obtiene la factura para ese id
    $resultQuery  =                                $this->statusAutoInvoiceModel->getById($invoiceId);
    $invoice      =                                              $resultQuery["query"]->fetch_assoc();
    $subTotalInvoice = $invoice["totalInvoice"];
    header('Content-Type: application/json');

    //Se verifica si existe la factura
    if( count($invoice) == 0 ){
      echo(json_encode(array(
        "code" => 404
      )));
      return;
    }
    if((Integer)$invoice["typeInvoice"] !== 0){
      echo json_encode(array(
        "code" => 400,
        "message" => "Not found"
      ));
      return;
    }
    if((Integer)$invoice["isCancelled"] == 1){
      echo json_encode(array(
        "code" => 400,
        "message" => "La factura esta cancelada, no es posible actualizarla."
      ));
      return;
    }

    if((Integer)$invoice["havePayments"] == 1 || (Integer)$invoice["isPaid"] == 1){
      echo json_encode(array(
        "code" => 400,
        "message" => "La factura ya tiene pagos o ya esta pagada, no es posible actualizarla."
      ));
      return;
    }

    //Validality of each array with elements
    $isValid = TRUE;
    if(strcmp($dateStart, "") != 0){
      if(!checkValidity("Date", $dateStart)){
        $isValid = FALSE;
      }
    }
    if(strcmp($dateEnd, "") != 0){
      if(!checkValidity("Date", $dateEnd)){
        $isValid = FALSE;
      }
    }
    if(!$isValid){
      echo(json_encode(array(
        "code" => 400,
        "message" => "Ingrese correctamente el formato YYYY-MM-DD para las fechas porfavor, si percisten los errores recargue la vista porfavor o avise al administrador."
      )));
      return;
    }
    if(count($extraCharges) > 0){
      $extraChargesS0 = array_filter($extraCharges, function($element){
        return $element["status"] == 0;
      });
      $extraChargesS1 = array_filter($extraCharges, function($element){
        return $element["status"] == 1;
      });
      if(count($extraChargesS1) > 0){
        if(!validityCompleteArray(array(
          "type" => "String",
          "status" => "UInteger",
          "descriptionServiceId" => "UInteger",
          "costService" => "UFloat",
          "id" => "UInteger",
          "extraInformationService" => "String"
        ), $extraChargesS1)){
          $isValid = FALSE;
        }
      }
      if(count($extraChargesS0) > 0){
        if(!validityCompleteArray(array(
          "type" => "String",
          "status" => "UInteger",
          "serviceId" => "UInteger"
        ), $extraChargesS0)){
          $isValid = FALSE;
        }
      }
    }
    if(!$isValid){
      echo(json_encode(array(
        "code" => 400,
        "message" => "Ingrese correctamente los datos para cada extra cargo porfavor, si percisten los errores recargue la vista porfavor o avise al administrador."
      )));
      return;
    }
    if(count($receivings) > 0){
      $receivingsS1 = array_filter($receivings, function($element){
        return $element["status"] == 1;
      });
      $receivingsS2 = array_filter($receivings, function($element){
        return $element["status"] == 2;
      });
      $receivingsS3 = array_filter($receivings, function($element){
        return $element["status"] == 3;
      });
      if(count($receivingsS3) > 0){
        if(!validityCompleteArray(array(
          "id" => "UInteger",
          "status" => "UInteger",
          "hazmat" => "UInteger",
          "bound" => "UInteger"
        ), $receivingsS3)){
          $isValid = FALSE;
        }
      }
      if(count($receivingsS2) > 0){
        if(!validityCompleteArray(array(
          "id" => "UInteger",
          "status" => "UInteger",
          "bound" => "UInteger"
        ), $receivingsS2)){
          $isValid = FALSE;
        }
      }
      if(count($receivingsS1) > 0){
        if(!validityCompleteArray(array(
          "id" => "UInteger",
          "status" => "UInteger",
          "hazmat" => "UInteger"
        ), $receivingsS1)){
          $isValid = FALSE;
        }
      }
    }
    if(!$isValid){
      echo(json_encode(array(
        "code" => 400,
        "message" => "Ingrese correctamente los datos para cada receiving porfavor, si percisten los errores recargue la vista porfavor o avise al administrador."
      )));
      return;
    }
    if(count($shippings) > 0){
      $shippingsS1 = array_filter($shippings, function($element){
        return $element["status"] == 1;
      });
      $shippingsS2 = array_filter($shippings, function($element){
        return $element["status"] == 2;
      });
      $shippingsS3 = array_filter($shippings, function($element){
        return $element["status"] == 3;
      });
      if(count($shippingsS3) > 0){
        if(validityCompleteArray(array(
          "id" => "UInteger",
          "status" => "UInteger",
          "credit" => "UFloat",
          "invoicelists" => "Array"
        ), $shippingsS3)){
          foreach($shippings as $shipping){
            $invoicelistsS1 = array_filter($shipping["invoicelists"], function($element){
              return $element["status"] == 1;
            });
            $invoicelistsS2 = array_filter($shipping["invoicelists"], function($element){
              return $element["status"] == 2;
            });
            $invoicelistsS3 = array_filter($shipping["invoicelists"], function($element){
              return $element["status"] == 3;
            });
            if(count($invoicelistsS3) > 0){
              if(!validityCompleteArray(array(
                "id" => "UInteger",
                "status" => "UInteger",
                "days" => "UInteger",
                "unitprice" => "UFloat"
              ), $invoicelistsS3)){
                $isValid = FALSE;
                break;
              }
            }
            if(count($invoicelistsS2) > 0){
              if(!validityCompleteArray(array(
                "id" => "UInteger",
                "status" => "UInteger",
                "days" => "UInteger"
              ), $invoicelistsS2)){
                $isValid = FALSE;
                break;
              }
            }
            if(count($invoicelistsS1) > 0){
              if(!validityCompleteArray(array(
                "id" => "UInteger",
                "status" => "UInteger",
                "unitprice" => "UFloat"
              ), $invoicelistsS1)){
                $isValid = FALSE;
                break;
              }
            }
          }
        }
      }
      if(count($shippingsS2) > 0){
        if(!validityCompleteArray(array(
          "id" => "UInteger",
          "status" => "UInteger",
          "credit" => "UFloat"
        ), $shippingsS2)){
          $isValid = FALSE;
        }
      }
      if(count($shippingsS1) > 0){
        if(validityCompleteArray(array(
          "id" => "UInteger",
          "status" => "UInteger",
          "invoicelists" => "Array"
        ), $shippingsS3)){
          foreach($shippings as $shipping){
            $invoicelistsS1 = array_filter($shipping["invoicelists"], function($element){
              return $element["status"] == 1;
            });
            $invoicelistsS2 = array_filter($shipping["invoicelists"], function($element){
              return $element["status"] == 2;
            });
            $invoicelistsS3 = array_filter($shipping["invoicelists"], function($element){
              return $element["status"] == 3;
            });
            if(count($invoicelistsS3) > 0){
              if(!validityCompleteArray(array(
                "id" => "UInteger",
                "status" => "UInteger",
                "days" => "UInteger",
                "unitprice" => "UFloat"
              ), $invoicelistsS3)){
                $isValid = FALSE;
                break;
              }
            }
            if(count($invoicelistsS2) > 0){
              if(!validityCompleteArray(array(
                "id" => "UInteger",
                "status" => "UInteger",
                "days" => "UInteger"
              ), $invoicelistsS2)){
                $isValid = FALSE;
                break;
              }
            }
            if(count($invoicelistsS1) > 0){
              if(!validityCompleteArray(array(
                "id" => "UInteger",
                "status" => "UInteger",
                "unitprice" => "UFloat"
              ), $invoicelistsS1)){
                $isValid = FALSE;
                break;
              }
            }
          }
        }
      }
    }
    if(!$isValid){
      echo(json_encode(array(
        "code" => 400,
        "message" => "Ingrese correctamente los datos para cada shipment correctamente porfavor, si percisten los errores recargue la vista porfavor o avise al administrador."
      )));
      return;
    }
    $resultQuery = $this->serviceCustomerModel->getByCustomerId($invoice["customer_id"]);
    $servicesCustomer = $resultQuery["query"];
    $resultQuery = $this->serviceReceivingModel->getByAutoInvoiceId($invoiceId);
    $receivingEC = $resultQuery["query"];
    $resultQuery = $this->serviceShippingModel->getByAutoInvoiceId($invoiceId);
    $shippingEC = $resultQuery["query"];
    $resultQuery       = $this->receivingInvoiceModel->getByAutoInvoiceId($invoiceId);
    $receivingsInvoice =                                        $resultQuery["query"];
    $resultQuery = $this->shippingInvoiceModel->getByAutoInvoiceId($invoiceId);
    $shippingsInvoice = $resultQuery["query"];
    $self = $this->invoiceListModel;
    $shippingsInvoice = array_map(function($shipping) use ($self, & $creditTotal, & $invoiceIds){
      $haveCredit = $shipping["haveCredit"];
      //se obtienen los invocielist de el invoice_id del shipping
      $resultQuery = $self->getByIdAndType($shipping["invoice_id"]);
      $shipping["invoiceslist"] = $resultQuery["query"];

      //Se recorren los invoicelist para obtener el tipo de invoicelist, el tipo de paquete y sus dias de almacenaje, si es tipo storage el invoicelist
      $shipping["invoiceslist"] = array_map(function($invoiceList){
        //Se obtiene un arreglo a partir de la descripcion y los espacios que contenga
        $description = explode(" ", $invoiceList["description"]);
        //Se almacena el tipo de paquete
        $invoiceList["typePackage"] = $description[0];
        //Se verifica que tipo es el invoicelist, y almacena el correspondiente
        if( strcmp($description[1], "Handling") == 0 ){
          $invoiceList["typeInvoiceList"] = "Handling";
        }else{
          $invoiceList["typeInvoiceList"] = "Storage";
          //Se obtiene un arreglo a partir de el valor en la segunda posicion del arreglo $description y los : que contenga
          $storageInfo = explode(":", $description[1]);
          //Se almacenan los dias de almacenaje de invoicelist
          $invoiceList["daysStorage"]     = $storageInfo[1];
        }
        return $invoiceList;
      }, $shipping["invoiceslist"]);

      if($haveCredit){
        $invoicelists = array();
        foreach($shipping["invoiceslist"] as $invoicelist){
          if(strcmp($invoicelist["typeInvoiceList"], "Storage") != 0){
            array_push($invoicelists, $invoicelist);
          }
        }
        $shipping["invoiceslist"] = $invoicelists;
      }
      array_push($invoiceIds, $shipping["invoice_id"]);
      $creditTotal += $shipping["creditShipping"];
      return $shipping;
    }, $shippingsInvoice);
    if( count($invoiceIds) > 0 && count($shippings) > 0){
      $resultQuery = $this->invoiceModel->getAllByIds($invoiceIds);
      $invoices    =                         $resultQuery["query"];
      foreach($invoices as $inv){
        array_push($invoicesUpdate, array(
          "invoiceid" => (Integer)$inv["invoiceid"],
          "subtotal"  =>  (Double)$inv["subtotal"]
        ));
      }
    }
    foreach($extraCharges as $charge){
      switch($charge["type"]){
        case "receiving":
        switch($charge["status"]){
          case 0:
          foreach($receivingEC as $receivingCharge){
            if($charge["serviceId"] == $receivingCharge["service_receiving_id"]){
              $subTotalInvoice -= $receivingCharge["costService"];
              array_push($extraChargesReceivingDelete, array(
                "servicereceivingid"   => (Integer)$charge["serviceId"]
              ));
              break;
            }
          }
          break;
          case 1:
          foreach($servicesCustomer as $serviceCustomer){
            if($charge["descriptionServiceId"] == $serviceCustomer["service_id"] && $invoice["customer_id"] == $serviceCustomer["customer_id"]){
              $cost = 0;
              if($serviceCustomer["variantService"]){
                $cost = (Double)$charge["costService"];
                if($cost === $serviceCustomer["costService"]){
                  $cost = $serviceCustomer["costService"];
                }
              }else{
                $cost = $serviceCustomer["costService"];
              }
              $extraTotal += $cost;
              array_push($extraChargesReceivingInsert, array(
                "cost"        =>          $cost,
                "serviceid"   => (Integer)$charge["descriptionServiceId"],
                "receivingid"  =>                   (Integer)$charge["id"],
                "information" =>       sanitizeString($charge["extraInformationService"])
              ));
              break;
            }
          }
          break;
        }
        break;
        case "shipping":
        switch($charge["status"]){
          case 0:
          foreach($shippingEC as $shippingCharge){
            if( $charge["serviceId"] == $shippingCharge["service_shipping_id"] ){
              $subTotalInvoice -= $shippingCharge["costService"];
              array_push($extraChargesShippingDelete, array(
                "serviceshippingid"   => (Integer)$charge["serviceId"]
              ));
              break;
            }
          }
          break;
          case 1:
          foreach($servicesCustomer as $serviceCustomer){
            if($charge["descriptionServiceId"] == $serviceCustomer["service_id"] && $invoice["customer_id"] == $serviceCustomer["customer_id"]){
              $cost = 0;
              if($serviceCustomer["variantService"]){
                $cost = (Double)$charge["costService"];
                if($cost === $serviceCustomer["costService"]){
                  $cost = $serviceCustomer["costService"];
                }
              }else{
                $cost = $serviceCustomer["costService"];
              }
              $extraTotal += $cost;
              array_push($extraChargesShippingInsert, array(
                "cost"        =>          $cost,
                "serviceid"   => (Integer)$charge["descriptionServiceId"],
                "shippingid"  =>                   (Integer)$charge["id"],
                "information" =>       sanitizeString($charge["extraInformationService"])
              ));
              break;
            }
          }
          break;
        }
        break;
      }
    }
    foreach($receivings as $receiving){
      foreach($receivingsInvoice as $receivingInvoice){
        if($receivingInvoice["receiving_id"] == $receiving["id"]){
          $hazmat = (Double)((array_key_exists("hazmat", $receiving)) ? $receiving["hazmat"] : $receivingInvoice["hazmatReceiving"]);
          $bound  =    (Double)((array_key_exists("bound", $receiving)) ? $receiving["bound"] : $receivingInvoice["boundReceiving"]);
          if($receivingInvoice["hazmatReceiving"] != $hazmat){
            switch($hazmat){
              case 0:
              $subTotalInvoice -= $receivingInvoice["hazmatCost"];
              break;
              case 1:
              $extraTotal += $receivingInvoice["hazmatCost"];
              break;
            }
          }
          if($receivingInvoice["boundReceiving"] != $bound){
            switch($hazmat){
              case 0:
              $subTotalInvoice -= $receivingInvoice["northCost"];
              $extraTotal += $receivingInvoice["southCost"];
              break;
              case 1:
              $subTotalInvoice -= $receivingInvoice["southCost"];
              $extraTotal += $receivingInvoice["northCost"];
              break;
            }
          }
          switch((Integer)$receiving["status"]){
            case 1:
            array_push($receivingHazmat, array(
              "receivingid" =>     (Integer)$receiving["id"],
              "hazmat"      =>      ($hazmat) ? "yes" : "no"
            ));
            break;
            case 2:
            array_push($receivingBound, array(
              "receivingid" =>              (Integer)$receiving["id"],
              "bound"       => ($bound) ? "NorthBound" : "SouthBound"
            ));
            break;
            case 3:
            array_push($receivingAll, array(
              "receivingid" =>              (Integer)$receiving["id"],
              "hazmat"      =>               ($hazmat) ? "yes" : "no",
              "bound"       => ($bound) ? "NorthBound" : "SouthBound"
            ));
            break;
          }
          break;
        }
      }
    }
    foreach($shippings as $shipping){
      foreach($shippingsInvoice as $shippingInvoice){
        if($shipping["id"] == $shippingInvoice["shipping_id"]){
          switch($shipping["status"]){
            case 1:
            $invoicelists = $shipping["invoicelists"];
            foreach($invoicelists as $invoicelist){
              $invoicelistsI = $shippingInvoice["invoiceslist"];
              foreach($invoicelistsI as $invoicelistI){
                $isChange            = FALSE;
                $newTotalInvoiceList =     0;
                if($invoicelist["id"] == $invoicelistI["invoicelistid"]){
                  switch($invoicelist["status"]){
                    case 1:
                    if($invoicelistI["daysStorage"] != $invoicelist["days"]){
                      $qty  =       (Integer)$invoicelistI["qty"];
                      $CPD  =     (Double)$invoicelistI["unitprice"];
                      $days =  (Integer)$invoicelist["days"];
                      $newTotalInvoiceList = $qty * $days * $CPD;
                      $typePackage = $invoicelistI['typePackage'];
                      $description = "$typePackage Storage:$days Day(s)";
                      $invoicesUpdate = array_map(function($invoice) use ($shippingInvoice, $invoicelistI, $newTotalInvoiceList) {
                        if($shippingInvoice["invoice_id"] == $invoice["invoiceid"]){
                          $invoice["subtotal"] +=   $newTotalInvoiceList - $invoicelistI["total"];
                        }
                        return $invoice;
                      }, $invoicesUpdate);
                      array_push($invoiceListUpdate, array(
                        "invoicelistid" => $invoicelist["id"],
                        "unitprice"   => $CPD,
                        "description" => $description,
                        "total"       => $newTotalInvoiceList
                      ));
                      $isChange = TRUE;
                    }
                    break;
                    case 2:
                    switch($invoicelistI["typeInvoiceList"]){
                      case "Storage":
                      if($invoicelistI["unitprice"] != $invoicelist["unitprice"]){
                        $qty  =       (Integer)$invoicelistI["qty"];
                        $CPD  =     (Double)$invoicelist["unitprice"];
                        $days =  (Integer)$invoicelistI["daysStorage"];
                        $newTotalInvoiceList = $qty * $days * $CPD;
                        $typePackage = $invoicelistI['typePackage'];
                        $description = "$typePackage Storage:$days Day(s)";
                        $invoicesUpdate = array_map(function($invoice) use ($shippingInvoice, $invoicelistI, $newTotalInvoiceList) {
                          if($shippingInvoice["invoice_id"] == $invoice["invoiceid"]){
                            $invoice["subtotal"] +=   $newTotalInvoiceList - $invoicelistI["total"];
                          }
                          return $invoice;
                        }, $invoicesUpdate);
                        array_push($invoiceListUpdate, array(
                          "invoicelistid" => $invoicelist["id"],
                          "unitprice"   => $CPD,
                          "description" => $description,
                          "total"       => $newTotalInvoiceList
                        ));
                        $isChange = TRUE;
                      }
                      break;
                      case "Handling":
                      if($invoicelistI["unitprice"] != $invoicelist["unitprice"]){
                        $qty  =       (Integer)$invoicelistI["qty"];
                        $unitprice  =     (Double)$invoicelist["unitprice"];
                        $newTotalInvoiceList = $qty * $unitprice;
                        $typePackage = $invoicelistI['typePackage'];
                        $description = "$typePackage Handling";
                        $invoicesUpdate = array_map(function($invoice) use ($shippingInvoice, $invoicelistI, $newTotalInvoiceList) {
                          if($shippingInvoice["invoice_id"] == $invoice["invoiceid"]){
                            $invoice["subtotal"] +=   $newTotalInvoiceList - $invoicelistI["total"];
                          }
                          return $invoice;
                        }, $invoicesUpdate);
                        array_push($invoiceListUpdate, array(
                          "invoicelistid" => $invoicelist["id"],
                          "unitprice"   => $unitprice,
                          "description" => $description,
                          "total"       => $newTotalInvoiceList
                        ));
                        $isChange = TRUE;
                      }
                      break;
                    }
                    break;
                    case 3:
                    if($invoicelistI["daysStorage"] != $invoicelist["days"] && $invoicelistI["unitprice"] != $invoicelist["unitprice"]){
                      $qty  =       (Integer)$invoicelistI["qty"];
                      $CPD  =     (Double)$invoicelist["unitprice"];
                      $days =  (Integer)$invoicelist["days"];
                      $newTotalInvoiceList = $qty * $days * $CPD;
                      $typePackage = $invoicelistI['typePackage'];
                      $description = "$typePackage Storage:$days Day(s)";
                      $invoicesUpdate = array_map(function($invoice) use ($shippingInvoice, $invoicelistI, $newTotalInvoiceList) {
                        if($shippingInvoice["invoice_id"] == $invoice["invoiceid"]){
                          $invoice["subtotal"] +=   $newTotalInvoiceList - $invoicelistI["total"];
                        }
                        return $invoice;
                      }, $invoicesUpdate);
                      array_push($invoiceListUpdate, array(
                        "invoicelistid" => $invoicelist["id"],
                        "unitprice"   => $CPD,
                        "description" => $description,
                        "total"       => $newTotalInvoiceList
                      ));
                      $isChange = TRUE;
                    }
                    break;
                  }
                  if($isChange){
                    $subTotalInvoice -= $invoicelistI["total"];
                    $extraTotal   +=   $newTotalInvoiceList;
                  }
                }
              }
            }
            break;
            case 2:
            if($shippingInvoice["haveCredit"]){
              if( $shipping["credit"] != $shippingInvoice["creditShipping"] ){
                $creditTotal += $shipping["credit"] - $shippingInvoice["creditShipping"];
                array_push($billOfLadingUpdate, array(
                  "billofladingid" => $shippingInvoice["bill_of_lading_id"],
                  "credit"          => $shipping["credit"]
                ));
              }
            }
            break;
            case 3:
            $invoicelists = $shipping["invoicelists"];
            if($shippingInvoice["haveCredit"]){
              if( $shipping["credit"] != $shippingInvoice["creditShipping"] ){
                $creditTotal += $shipping["credit"] - $shippingInvoice["creditShipping"];
                array_push($billOfLadingUpdate, array(
                  "billofladingid" => $shippingInvoice["bill_of_lading_id"],
                  "credit"          => $shipping["credit"]
                ));
              }
            }
            foreach($invoicelists as $invoicelist){
              $invoicelistsI = $shippingInvoice["invoiceslist"];
              foreach($invoicelistsI as $invoicelistI){
                $isChange            = FALSE;
                $newTotalInvoiceList =     0;
                if($invoicelist["id"] == $invoicelistI["invoicelistid"]){
                  switch($invoicelist["status"]){
                    case 1:
                    if($invoicelistI["daysStorage"] != $invoicelist["days"]){
                      $qty  =       (Integer)$invoicelistI["qty"];
                      $CPD  =     (Double)$invoicelistI["unitprice"];
                      $days =  (Integer)$invoicelist["days"];
                      $newTotalInvoiceList = $qty * $days * $CPD;
                      $typePackage = $invoicelistI['typePackage'];
                      $description = "$typePackage Storage:$days Day(s)";
                      $invoicesUpdate = array_map(function($invoice) use ($shippingInvoice, $invoicelistI, $newTotalInvoiceList) {
                        if($shippingInvoice["invoice_id"] == $invoice["invoiceid"]){
                          $invoice["subtotal"] +=   $newTotalInvoiceList - $invoicelistI["total"];
                        }
                        return $invoice;
                      }, $invoicesUpdate);
                      array_push($invoiceListUpdate, array(
                        "invoicelistid" => $invoicelist["id"],
                        "unitprice"   => $CPD,
                        "description" => $description,
                        "total"       => $newTotalInvoiceList
                      ));
                      $isChange = TRUE;
                    }
                    break;
                    case 2:
                    switch($invoicelistI["typeInvoiceList"]){
                      case "Storage":
                      if($invoicelistI["unitprice"] != $invoicelist["unitprice"]){
                        $qty  =       (Integer)$invoicelistI["qty"];
                        $CPD  =     (Double)$invoicelist["unitprice"];
                        $days =  (Integer)$invoicelistI["daysStorage"];
                        $newTotalInvoiceList = $qty * $days * $CPD;
                        $typePackage = $invoicelistI['typePackage'];
                        $description = "$typePackage Storage:$days Day(s)";
                        $invoicesUpdate = array_map(function($invoice) use ($shippingInvoice, $invoicelistI, $newTotalInvoiceList) {
                          if($shippingInvoice["invoice_id"] == $invoice["invoiceid"]){
                            $invoice["subtotal"] +=   $newTotalInvoiceList - $invoicelistI["total"];
                          }
                          return $invoice;
                        }, $invoicesUpdate);
                        array_push($invoiceListUpdate, array(
                          "invoicelistid" => $invoicelist["id"],
                          "unitprice"   => $CPD,
                          "description" => $description,
                          "total"       => $newTotalInvoiceList
                        ));
                        $isChange = TRUE;
                      }
                      break;
                      case "Handling":
                      if($invoicelistI["unitprice"] != $invoicelist["unitprice"]){
                        $qty  =       (Integer)$invoicelistI["qty"];
                        $unitprice  =     (Double)$invoicelist["unitprice"];
                        $newTotalInvoiceList = $qty * $unitprice;
                        $typePackage = $invoicelistI['typePackage'];
                        $description = "$typePackage Handling";
                        $invoicesUpdate = array_map(function($invoice) use ($shippingInvoice, $invoicelistI, $newTotalInvoiceList) {
                          if($shippingInvoice["invoice_id"] == $invoice["invoiceid"]){
                            $invoice["subtotal"] +=   $newTotalInvoiceList - $invoicelistI["total"];
                          }
                          return $invoice;
                        }, $invoicesUpdate);
                        array_push($invoiceListUpdate, array(
                          "invoicelistid" => $invoicelist["id"],
                          "unitprice"   => $unitprice,
                          "description" => $description,
                          "total"       => $newTotalInvoiceList
                        ));
                        $isChange = TRUE;
                      }
                      break;
                    }
                    break;
                    case 3:
                    if($invoicelistI["daysStorage"] != $invoicelist["days"] && $invoicelistI["unitprice"] != $invoicelist["unitprice"]){
                      $qty  =       (Integer)$invoicelistI["qty"];
                      $CPD  =     (Double)$invoicelist["unitprice"];
                      $days =  (Integer)$invoicelist["days"];
                      $newTotalInvoiceList = $qty * $days * $CPD;
                      $typePackage = $invoicelistI['typePackage'];
                      $description = "$typePackage Storage:$days Day(s)";
                      $invoicesUpdate = array_map(function($invoice) use ($shippingInvoice, $invoicelistI, $newTotalInvoiceList) {
                        if($shippingInvoice["invoice_id"] == $invoice["invoiceid"]){
                          $invoice["subtotal"] +=   $newTotalInvoiceList - $invoicelistI["total"];
                        }
                        return $invoice;
                      }, $invoicesUpdate);
                      array_push($invoiceListUpdate, array(
                        "invoicelistid" => $invoicelist["id"],
                        "unitprice"   => $CPD,
                        "description" => $description,
                        "total"       => $newTotalInvoiceList
                      ));
                      $isChange = TRUE;
                    }
                    break;
                  }
                  if($isChange){
                    $subTotalInvoice -= $invoicelistI["total"];
                    $extraTotal   +=   $newTotalInvoiceList;
                  }
                }
              }
            }
            break;
          }
        }
      }
    }
    if($this->checkAmount($invoice["customer_id"], ($subTotalInvoice + $extraTotal) - $invoice["totalInvoice"], $invoice["totalInvoice"] - $subTotalInvoice)){
      if( count($extraChargesReceivingInsert) > 0 ){
        $this->serviceReceivingModel->saveAll($invoiceId, $extraChargesReceivingInsert);
      }
      if( count($extraChargesShippingInsert) > 0 ){
        $this->serviceShippingModel->saveAll($invoiceId, $extraChargesShippingInsert);
      }
      if( count($extraChargesReceivingDelete) > 0 ){
        $this->serviceReceivingModel->deleteAll($extraChargesReceivingDelete);
      }
      if( count($extraChargesShippingDelete) > 0 ){
        $this->serviceShippingModel->deleteAll($extraChargesShippingDelete);
      }
      if( count($receivingAll) > 0 ){
        $this->receivingModel->updateAllHazmatAndBound($receivingAll);
      }
      if( count($receivingHazmat) > 0 ){
        $this->receivingModel->updateAllHazmat($receivingHazmat);
      }
      if( count($receivingBound) > 0 ){
        $this->receivingModel->updateAllBound($receivingBound);
      }
      if( count($invoiceListUpdate) > 0 ){
        $this->invoiceListModel->updateAll($invoiceListUpdate);
        $this->invoiceModel->updateAll($invoicesUpdate);
      }
      if( count($billOfLadingUpdate) > 0 ){
        $this->billOfLadingModel->updateAll($billOfLadingUpdate);
      }
      if($invoice["totalInvoice"] != ($subTotalInvoice + $extraTotal) || array_key_exists("invoiceStartDate", $params) || array_key_exists("invoiceEndDate", $params)){
        if($invoice["totalInvoice"] != ($subTotalInvoice + $extraTotal)){
          $this->customerModel->updateDueAmount($invoice["customer_id"], ($subTotalInvoice + $extraTotal) - $invoice["totalInvoice"]);
        }
        if(array_key_exists("invoiceStartDate", $params) && array_key_exists("invoiceEndDate", $params)){
          if(strcmp($params["invoiceStartDate"], $params["invoiceEndDate"]) == -1){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $params["invoiceStartDate"],
              "date"           => $params["invoiceEndDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else{
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $invoice["dateStartInvoice"],
              "date"           => $invoice["dateInvoice"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }
        }else if(array_key_exists("invoiceStartDate", $params)){
          if(strcmp($params["invoiceStartDate"], $invoice["dateInvoice"]) == -1){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $params["invoiceStartDate"],
              "date"           => $params["invoiceStartDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else if(strcmp($params["invoiceStartDate"], $invoice["dateInvoice"]) == 1 && strcmp($invoice["dateStartInvoice"], $invoice["dateInvoice"]) == 0){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $params["invoiceStartDate"],
              "date"           => $params["invoiceStartDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else{
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $invoice["dateStartInvoice"],
              "date"           => $invoice["dateInvoice"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }
        }else if(array_key_exists("invoiceEndDate", $params)){
          if(strcmp($params["invoiceEndDate"], $invoice["dateStartInvoice"]) != -1 && strcmp($invoice["dateStartInvoice"], $invoice["dateInvoice"]) != 0){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $invoice["dateStartInvoice"],
              "date"           => $params["invoiceEndDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else if(strcmp($params["invoiceEndDate"], $invoice["dateStartInvoice"]) != -1 && strcmp($invoice["dateStartInvoice"], $invoice["dateInvoice"]) == 0){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $params["invoiceEndDate"],
              "date"           => $params["invoiceEndDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else{
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $invoice["dateStartInvoice"],
              "date"           => $invoice["dateInvoice"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }
        }else{
          $this->autoInvoiceModel->updateAll(array(
            "autoinvoice_id" => $invoice["autoid"],
            "startDate"      => $invoice["dateStartInvoice"],
            "date"           => $invoice["dateInvoice"],
            "total"          => $subTotalInvoice + $extraTotal
          ));
        }
      }
      echo(json_encode(
        array(
          "code"     =>   200
        )
      ));
    }else{
      echo(json_encode(
        array(
          "code"     =>   500
        )
      ));
    }
  }

  /*
   **************************************************************************************************
   Obtiene los extra cargos de receiving para una factura
   **************************************************************************************************
  */
  public function saveInvoicePAndP(){
    $params                      = array();
    $invoice                     = array();
    $invoices                    = array();
    $invoiceId                   =       0;
    $shippings                   = array();
    $receiving                   = array();
    $extraTotal                  =       0;
    $invoiceIds                  = array();
    $shippingEC                  = array();
    $receivingEC                 = array();
    $resultQuery                 = array();
    $creditTotal                 =       0;
    $extraCharges                = array();
    $receivingAll                = array();
    $invoiceUpdate               = array();
    $invoicesUpdate              = array();
    $invoicelistAll              = array();
    $receivingBound              = array();
    $subTotalInvoice             =       0;
    $shippingsUpdate             = array();
    $receivingHazmat             = array();
    $servicesCustomer            = array();
    $shippingsInvoice            = array();
    $receivingsUpdate            = array();
    $receivingsInvoice           = array();
    $invoiceListUpdate           = array();
    $invoicelistStorage          = array();
    $billOfLadingUpdate          = array();
    $invoicelistHandling         = array();
    $extraChargesShippingInsert  = array();
    $extraChargesShippingDelete  = array();
    $extraChargesReceivingInsert = array();
    $extraChargesReceivingDelete = array();
    //Se obtienen los datos mediante la peticion PUT
    parse_str(file_get_contents('php://input'), $params);
    if(array_key_exists("invoiceId", $params)){
      if(!checkValidity("UInteger", $params["invoiceId"])){
        echo(json_encode(array(
          "code" => 404
        )));
        return;
      }
      $invoiceId = (Integer)$params["invoiceId"];
    }else{
      echo(json_encode(array(
        "code" => 404
      )));
      return;
    }

    //Se obtiene la factura para ese id
    $resultQuery  =                                $this->statusAutoInvoiceModel->getById($invoiceId);
    $invoice      =                                              $resultQuery["query"]->fetch_assoc();
    $subTotalInvoice = $invoice["totalInvoice"];
    header('Content-Type: application/json');
    //Se verifica si existe la factura
    if( count($invoice) == 0 ){
      echo(json_encode(array(
        "code" => 404
      )));
      return;
    }
    if((Integer)$invoice["typeInvoice"] !== 3){
      echo json_encode(array(
        "code" => 400,
        "message" => "Not found"
      ));
      return;
    }

    if((Integer)$invoice["isCancelled"] == 1){
      echo json_encode(array(
        "code" => 400,
        "message" => "La factura esta cancelada, no es posible actualizarla."
      ));
      return;
    }

    if((Integer)$invoice["havePayments"] == 1 || (Integer)$invoice["isPaid"] == 1){
      echo json_encode(array(
        "code" => 400,
        "message" => "La factura ya tiene pagos o ya esta pagada, no es posible actualizarla."
      ));
      return;
    }
    //Se obtiene cada dato
    $dateStart    = ( array_key_exists("invoiceStartDate", $params) ) ? $params["invoiceStartDate"] : "";
    $dateEnd    = ( array_key_exists("invoiceEndDate", $params) ) ? $params["invoiceEndDate"] : "";
    $shippings    =       ( array_key_exists("shippings", $params) ) ? $params["shippings"] : array();
    $extraCharges = ( array_key_exists("extraCharges", $params) ) ? $params["extraCharges"] : array();

    $isValid = TRUE;
    if(strcmp($dateStart, "") != 0){
      if(!checkValidity("Date", $dateStart)){
        $isValid = FALSE;
      }
    }
    if(strcmp($dateEnd, "") != 0){
      if(!checkValidity("Date", $dateEnd)){
        $isValid = FALSE;
      }
    }
    if(!$isValid){
      echo(json_encode(array(
        "code" => 400,
        "message" => "Ingrese correctamente el formato YYYY-MM-DD para las fechas porfavor, si percisten los errores recargue la vista porfavor o avise al administrador."
      )));
      return;
    }
    if(count($extraCharges) > 0){
      $extraChargesS0 = array_filter($extraCharges, function($element){
        return $element["status"] == 0;
      });
      $extraChargesS1 = array_filter($extraCharges, function($element){
        return $element["status"] == 1;
      });
      if(count($extraChargesS1) > 0){
        if(!validityCompleteArray(array(
          "type" => "String",
          "status" => "UInteger",
          "descriptionServiceId" => "UInteger",
          "costService" => "UFloat",
          "id" => "UInteger",
          "extraInformationService" => "String"
        ), $extraChargesS1)){
          $isValid = FALSE;
        }
      }
      if(count($extraChargesS0) > 0){
        if(!validityCompleteArray(array(
          "type" => "String",
          "status" => "UInteger",
          "serviceId" => "UInteger"
        ), $extraChargesS0)){
          $isValid = FALSE;
        }
      }
    }
    if(!$isValid){
      echo(json_encode(array(
        "code" => 400,
        "message" => "Ingrese correctamente los datos para cada extra cargo porfavor, si percisten los errores recargue la vista porfavor o avise al administrador."
      )));
      return;
    }
    if(count($shippings) > 0){
      $shippingsS1 = array_filter($shippings, function($element){
        return $element["status"] == 1;
      });
      $shippingsS2 = array_filter($shippings, function($element){
        return $element["status"] == 2;
      });
      $shippingsS3 = array_filter($shippings, function($element){
        return $element["status"] == 3;
      });
      if(count($shippingsS3) > 0){
        if(validityCompleteArray(array(
          "id" => "UInteger",
          "status" => "UInteger",
          "credit" => "UFloat",
          "invoicelists" => "Array"
        ), $shippingsS3)){
          foreach($shippings as $shipping){
            $invoicelistsS1 = array_filter($shipping["invoicelists"], function($element){
              return $element["status"] == 1;
            });
            $invoicelistsS2 = array_filter($shipping["invoicelists"], function($element){
              return $element["status"] == 2;
            });
            $invoicelistsS3 = array_filter($shipping["invoicelists"], function($element){
              return $element["status"] == 3;
            });
            if(count($invoicelistsS3) > 0){
              if(!validityCompleteArray(array(
                "id" => "UInteger",
                "status" => "UInteger",
                "days" => "UInteger",
                "unitprice" => "UFloat"
              ), $invoicelistsS3)){
                $isValid = FALSE;
                break;
              }
            }
            if(count($invoicelistsS2) > 0){
              if(!validityCompleteArray(array(
                "id" => "UInteger",
                "status" => "UInteger",
                "days" => "UInteger"
              ), $invoicelistsS2)){
                $isValid = FALSE;
                break;
              }
            }
            if(count($invoicelistsS1) > 0){
              if(!validityCompleteArray(array(
                "id" => "UInteger",
                "status" => "UInteger",
                "unitprice" => "UFloat"
              ), $invoicelistsS1)){
                $isValid = FALSE;
                break;
              }
            }
          }
        }
      }
      if(count($shippingsS2) > 0){
        if(!validityCompleteArray(array(
          "id" => "UInteger",
          "status" => "UInteger",
          "credit" => "UFloat"
        ), $shippingsS2)){
          $isValid = FALSE;
        }
      }
      if(count($shippingsS1) > 0){
        if(validityCompleteArray(array(
          "id" => "UInteger",
          "status" => "UInteger",
          "invoicelists" => "Array"
        ), $shippingsS3)){
          foreach($shippings as $shipping){
            $invoicelistsS1 = array_filter($shipping["invoicelists"], function($element){
              return $element["status"] == 1;
            });
            $invoicelistsS2 = array_filter($shipping["invoicelists"], function($element){
              return $element["status"] == 2;
            });
            $invoicelistsS3 = array_filter($shipping["invoicelists"], function($element){
              return $element["status"] == 3;
            });
            if(count($invoicelistsS3) > 0){
              if(!validityCompleteArray(array(
                "id" => "UInteger",
                "status" => "UInteger",
                "days" => "UInteger",
                "unitprice" => "UFloat"
              ), $invoicelistsS3)){
                $isValid = FALSE;
                break;
              }
            }
            if(count($invoicelistsS2) > 0){
              if(!validityCompleteArray(array(
                "id" => "UInteger",
                "status" => "UInteger",
                "days" => "UInteger"
              ), $invoicelistsS2)){
                $isValid = FALSE;
                break;
              }
            }
            if(count($invoicelistsS1) > 0){
              if(!validityCompleteArray(array(
                "id" => "UInteger",
                "status" => "UInteger",
                "unitprice" => "UFloat"
              ), $invoicelistsS1)){
                $isValid = FALSE;
                break;
              }
            }
          }
        }
      }
    }
    if(!$isValid){
      echo(json_encode(array(
        "code" => 400,
        "message" => "Ingrese correctamente los datos para cada shipment correctamente porfavor, si percisten los errores recargue la vista porfavor o avise al administrador."
      )));
      return;
    }

    $resultQuery = $this->serviceCustomerModel->getByCustomerId($invoice["customer_id"]);
    $servicesCustomer = $resultQuery["query"];
    $resultQuery = $this->serviceShippingModel->getByAutoInvoiceId($invoiceId);
    $shippingEC = $resultQuery["query"];
    $resultQuery = $this->shippingInvoiceModel->getByAutoInvoiceId($invoiceId);
    $shippingsInvoice = $resultQuery["query"];
    $self = $this->invoiceListModel;
    $shippingsInvoice = array_map(function($shipping) use ($self, & $creditTotal, & $invoiceIds){
      $haveCredit = $shipping["haveCredit"];
      //se obtienen los invocielist de el invoice_id del shipping
      $resultQuery = $self->getByIdAndType($shipping["invoice_id"]);
      $shipping["invoiceslist"] = $resultQuery["query"];

      //Se recorren los invoicelist para obtener el tipo de invoicelist, el tipo de paquete y sus dias de almacenaje, si es tipo storage el invoicelist
      $shipping["invoiceslist"] = array_map(function($invoiceList){
        //Se obtiene un arreglo a partir de la descripcion y los espacios que contenga
        $description = explode(" ", $invoiceList["description"]);
        //Se almacena el tipo de paquete
        $invoiceList["typePackage"] = $description[0];
        //Se verifica que tipo es el invoicelist, y almacena el correspondiente
        if( strcmp($description[1], "Handling") == 0 ){
          $invoiceList["typeInvoiceList"] = "Handling";
        }else{
          $invoiceList["typeInvoiceList"] = "Storage";
          //Se obtiene un arreglo a partir de el valor en la segunda posicion del arreglo $description y los : que contenga
          $storageInfo = explode(":", $description[1]);
          //Se almacenan los dias de almacenaje de invoicelist
          $invoiceList["daysStorage"]     = $storageInfo[1];
        }
        return $invoiceList;
      }, $shipping["invoiceslist"]);
      $invoicelists = array();
      foreach($shipping["invoiceslist"] as $invoicelist){
        if(strcmp($invoicelist["typeInvoiceList"], "Storage") != 0){
          array_push($invoicelists, $invoicelist);
        }
      }
      $shipping["invoiceslist"] = $invoicelists;
      array_push($invoiceIds, $shipping["invoice_id"]);
      $creditTotal += $shipping["creditShipping"];
      return $shipping;
    }, $shippingsInvoice);
    if( count($invoiceIds) > 0 && count($shippings) > 0){
      $resultQuery = $this->invoiceModel->getAllByIds($invoiceIds);
      $invoices    =                         $resultQuery["query"];
      foreach($invoices as $inv){
        array_push($invoicesUpdate, array(
          "invoiceid" => (Integer)$inv["invoiceid"],
          "subtotal"  =>  (Double)$inv["subtotal"]
        ));
      }
    }
    if( count($servicesCustomer) > 0 ){
      foreach($extraCharges as $charge){
        switch($charge["type"]){
          case "shipping":
          switch($charge["status"]){
            case 0:
            foreach($shippingEC as $shippingCharge){
              if( $charge["serviceId"] == $shippingCharge["service_shipping_id"] ){
                $subTotalInvoice -= $shippingCharge["costService"];
                array_push($extraChargesShippingDelete, array(
                  "serviceshippingid"   => (Integer)$charge["serviceId"]
                ));
                break;
              }
            }
            break;
            case 1:
            foreach($servicesCustomer as $serviceCustomer){
              if($charge["descriptionServiceId"] == $serviceCustomer["service_id"] && $invoice["customer_id"] == $serviceCustomer["customer_id"]){
                $cost = 0;
                if($serviceCustomer["variantService"]){
                  $cost = (Double)$charge["costService"];
                  if($cost === $serviceCustomer["costService"]){
                    $cost = $serviceCustomer["costService"];
                  }
                }else{
                  $cost = $serviceCustomer["costService"];
                }
                $extraTotal += $cost;
                array_push($extraChargesShippingInsert, array(
                  "cost"        =>          $cost,
                  "serviceid"   => (Integer)$charge["descriptionServiceId"],
                  "shippingid"  =>                   (Integer)$charge["id"],
                  "information" =>       sanitizeString($charge["extraInformationService"])
                ));
                break;
              }
            }
            break;
          }
          break;
        }
      }
    }
    if(count($shippings) > 0){
      foreach($shippings as $shipping){
        foreach($shippingsInvoice as $shippingInvoice){
          if($shipping["id"] == $shippingInvoice["shipping_id"]){
            switch($shipping["status"]){
              case 1:
              $invoicelists = $shipping["invoicelists"];
              foreach($invoicelists as $invoicelist){
                $invoicelistsI = $shippingInvoice["invoiceslist"];
                foreach($invoicelistsI as $invoicelistI){
                  $isChange            = FALSE;
                  $newTotalInvoiceList =     0;
                  if($invoicelist["id"] == $invoicelistI["invoicelistid"]){
                    switch($invoicelist["status"]){
                      case 2:
                      switch($invoicelistI["typeInvoiceList"]){
                        case "Handling":
                        if($invoicelistI["unitprice"] != $invoicelist["unitprice"]){
                          $qty  =       (Integer)$invoicelistI["qty"];
                          $unitprice  =     (Double)$invoicelist["unitprice"];
                          $newTotalInvoiceList = $qty * $unitprice;
                          $typePackage = $invoicelistI['typePackage'];
                          $description = "$typePackage Handling";
                          $invoicesUpdate = array_map(function($invoice) use ($shippingInvoice, $invoicelistI, $newTotalInvoiceList) {
                            if($shippingInvoice["invoice_id"] == $invoice["invoiceid"]){
                              $invoice["subtotal"] +=   $newTotalInvoiceList - $invoicelistI["total"];
                            }
                            return $invoice;
                          }, $invoicesUpdate);
                          array_push($invoiceListUpdate, array(
                            "invoicelistid" => $invoicelist["id"],
                            "unitprice"   => $unitprice,
                            "description" => $description,
                            "total"       => $newTotalInvoiceList
                          ));
                          $isChange = TRUE;
                        }
                        break;
                      }
                      break;
                    }
                    if($isChange){
                      $subTotalInvoice -= $invoicelistI["total"];
                      $extraTotal   +=   $newTotalInvoiceList;
                    }
                  }
                }
              }
              break;
              case 2:
              if($shippingInvoice["haveCredit"]){
                if( $shipping["credit"] != $shippingInvoice["creditShipping"] ){
                  $creditTotal += $shipping["credit"] - $shippingInvoice["creditShipping"];
                  array_push($billOfLadingUpdate, array(
                    "billofladingid" => $shippingInvoice["bill_of_lading_id"],
                    "credit"          => $shipping["credit"]
                  ));
                }
              }
              break;
              case 3:
              $invoicelists = $shipping["invoicelists"];
              if($shippingInvoice["haveCredit"]){
                if( $shipping["credit"] != $shippingInvoice["creditShipping"] ){
                  $creditTotal += $shipping["credit"] - $shippingInvoice["creditShipping"];
                  array_push($billOfLadingUpdate, array(
                    "billofladingid" => $shippingInvoice["bill_of_lading_id"],
                    "credit"          => $shipping["credit"]
                  ));
                }
              }
              foreach($invoicelists as $invoicelist){
                $invoicelistsI = $shippingInvoice["invoiceslist"];
                foreach($invoicelistsI as $invoicelistI){
                  $isChange            = FALSE;
                  $newTotalInvoiceList =     0;
                  if($invoicelist["id"] == $invoicelistI["invoicelistid"]){
                    switch($invoicelist["status"]){
                      case 2:
                      switch($invoicelistI["typeInvoiceList"]){
                        case "Handling":
                        if($invoicelistI["unitprice"] != $invoicelist["unitprice"]){
                          $qty  =       (Integer)$invoicelistI["qty"];
                          $unitprice  =     (Double)$invoicelist["unitprice"];
                          $newTotalInvoiceList = $qty * $unitprice;
                          $typePackage = $invoicelistI['typePackage'];
                          $description = "$typePackage Handling";
                          $invoicesUpdate = array_map(function($invoice) use ($shippingInvoice, $invoicelistI, $newTotalInvoiceList) {
                            if($shippingInvoice["invoice_id"] == $invoice["invoiceid"]){
                              $invoice["subtotal"] +=   $newTotalInvoiceList - $invoicelistI["total"];
                            }
                            return $invoice;
                          }, $invoicesUpdate);
                          array_push($invoiceListUpdate, array(
                            "invoicelistid" => $invoicelist["id"],
                            "unitprice"   => $unitprice,
                            "description" => $description,
                            "total"       => $newTotalInvoiceList
                          ));
                          $isChange = TRUE;
                        }
                        break;
                      }
                      break;
                    }
                    if($isChange){
                      $subTotalInvoice -= $invoicelistI["total"];
                      $extraTotal   +=   $newTotalInvoiceList;
                    }
                  }
                }
              }
              break;
            }
          }
        }
      }
    }
    if($this->checkAmount($invoice["customer_id"], ($subTotalInvoice + $extraTotal) - $invoice["totalInvoice"], $invoice["totalInvoice"] - $subTotalInvoice)){
      if( count($extraChargesShippingInsert) > 0 ){
        $this->serviceShippingModel->saveAll($invoiceId, $extraChargesShippingInsert);
      }
      if( count($extraChargesShippingDelete) > 0 ){
        $this->serviceShippingModel->deleteAll($extraChargesShippingDelete);
      }
      if( count($invoiceListUpdate) > 0 ){
        $this->invoiceListModel->updateAll($invoiceListUpdate);
        $this->invoiceModel->updateAll($invoicesUpdate);
      }
      if( count($billOfLadingUpdate) > 0 ){
        $this->billOfLadingModel->updateAll($billOfLadingUpdate);
      }
      if($invoice["totalInvoice"] != ($subTotalInvoice + $extraTotal) || array_key_exists("invoiceStartDate", $params) || array_key_exists("invoiceEndDate", $params)){
        if($invoice["totalInvoice"] != ($subTotalInvoice + $extraTotal)){
          $this->customerModel->updateDueAmount($invoice["customer_id"], ($subTotalInvoice + $extraTotal) - $invoice["totalInvoice"]);
        }
        if(array_key_exists("invoiceStartDate", $params) && array_key_exists("invoiceEndDate", $params)){
          if(strcmp($params["invoiceStartDate"], $params["invoiceEndDate"]) == -1){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $params["invoiceStartDate"],
              "date"           => $params["invoiceEndDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else{
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $invoice["dateStartInvoice"],
              "date"           => $invoice["dateInvoice"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }
        }else if(array_key_exists("invoiceStartDate", $params)){
          if(strcmp($params["invoiceStartDate"], $invoice["dateInvoice"]) == -1){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $params["invoiceStartDate"],
              "date"           => $params["invoiceStartDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else if(strcmp($params["invoiceStartDate"], $invoice["dateInvoice"]) == 1 && strcmp($invoice["dateStartInvoice"], $invoice["dateInvoice"]) == 0){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $params["invoiceStartDate"],
              "date"           => $params["invoiceStartDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else{
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $invoice["dateStartInvoice"],
              "date"           => $invoice["dateInvoice"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }
        }else if(array_key_exists("invoiceEndDate", $params)){
          if(strcmp($params["invoiceEndDate"], $invoice["dateStartInvoice"]) != -1 && strcmp($invoice["dateStartInvoice"], $invoice["dateInvoice"]) != 0){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $invoice["dateStartInvoice"],
              "date"           => $params["invoiceEndDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else if(strcmp($params["invoiceEndDate"], $invoice["dateStartInvoice"]) != -1 && strcmp($invoice["dateStartInvoice"], $invoice["dateInvoice"]) == 0){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $params["invoiceEndDate"],
              "date"           => $params["invoiceEndDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else{
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $invoice["dateStartInvoice"],
              "date"           => $invoice["dateInvoice"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }
        }else{
          $this->autoInvoiceModel->updateAll(array(
            "autoinvoice_id" => $invoice["autoid"],
            "startDate"      => $invoice["dateStartInvoice"],
            "date"           => $invoice["dateInvoice"],
            "total"          => $subTotalInvoice + $extraTotal
          ));
        }
      }
      echo(json_encode(
        array(
          "code"     =>   200
        )
      ));
    }else{
      echo(json_encode(
        array(
          "code"     =>   500
        )
      ));
    }
  }

  /*
   **************************************************************************************************
   Obtiene los extra cargos de receiving para una factura
   **************************************************************************************************
  */
  public function saveInvoiceStorage(){
    $params                      = array();
    $invoice                     = array();
    $invoices                    = array();
    $invoiceId                   =       0;
    $storageEC                   = array();
    $extraTotal                  =       0;
    $invoiceIds                  = array();
    $resultQuery                 = array();
    $extraCharges                = array();
    $invoiceUpdate               = array();
    $subTotalInvoice             =       0;
    $servicesCustomer            = array();
    $receivingsDetails           = array();
    $extraChargesSInsert         = array();
    $extraChargesSDelete         = array();
    $receivingsDetailsUpdate     = array();
    $receivingsDetailsStorage    = array();
    //Se obtienen los datos mediante la peticion PUT
    parse_str(file_get_contents('php://input'), $params);
    //Se obtiene cada dato
    if(array_key_exists("invoiceId", $params)){
      if(!checkValidity("UInteger", $params["invoiceId"])){
        echo(json_encode(array(
          "code" => 404
        )));
        return;
      }
      $invoiceId = (Integer)$params["invoiceId"];
    }else{
      echo(json_encode(array(
        "code" => 404
      )));
      return;
    }
    //Se obtiene la factura para ese id
    $resultQuery  =                                $this->statusAutoInvoiceModel->getById($invoiceId);
    $invoice      =                                              $resultQuery["query"]->fetch_assoc();
    $subTotalInvoice = $invoice["totalInvoice"];
    header('Content-Type: application/json');
    //Se verifica si existe la factura
    if( count($invoice) == 0 ){
      echo(json_encode(array(
        "code" => 404
      )));
      return;
    }
    if((Integer)$invoice["typeInvoice"] !== 2){
      echo json_encode(array(
        "code" => 400,
        "message" => "Not found"
      ));
      return;
    }
    if((Integer)$invoice["isCancelled"] == 1){
      echo json_encode(array(
        "code" => 400,
        "message" => "La factura esta cancelada, no es posible actualizarla."
      ));
      return;
    }

    if((Integer)$invoice["havePayments"] == 1 || (Integer)$invoice["isPaid"] == 1){
      echo json_encode(array(
        "code" => 400,
        "message" => "La factura ya tiene pagos o ya esta pagada, no es posible actualizarla."
      ));
      return;
    }

    $dateStart    = ( array_key_exists("invoiceStartDate", $params) ) ? $params["invoiceStartDate"] : "";
    $dateEnd    = ( array_key_exists("invoiceEndDate", $params) ) ? $params["invoiceEndDate"] : "";
    $receivingsDetails = ( array_key_exists("receivingsDetails", $params) ) ? $params["receivingsDetails"] : array();
    $extraCharges      =           ( array_key_exists("extraCharges", $params) ) ? $params["extraCharges"] : array();
    //Validality of each array with elements
    $isValid = TRUE;
    if(strcmp($dateStart, "") != 0){
      if(!checkValidity("Date", $dateStart)){
        $isValid = FALSE;
      }
    }
    if(strcmp($dateEnd, "") != 0){
      if(!checkValidity("Date", $dateEnd)){
        $isValid = FALSE;
      }
    }
    if(!$isValid){
      echo(json_encode(array(
        "code" => 400,
        "message" => "Ingrese correctamente el formato YYYY-MM-DD para las fechas porfavor, si percisten los errores recargue la vista porfavor o avise al administrador."
      )));
      return;
    }
    if(count($extraCharges) > 0){
      $extraChargesS1 = array_filter($extraCharges, function($element){
        return $element["status"] == 1;
      });
      $extraChargesS0 = array_filter($extraCharges, function($element){
        return $element["status"] == 0;
      });
      if(count($extraChargesS1) > 0){
        if(!validityCompleteArray(array(
          "status" => "UInteger",
          "descriptionServiceId" => "UInteger",
          "costService" => "UFloat",
          "extraInformationService" => "String"
        ), $extraChargesS1)){
          $isValid = FALSE;
        }
      }
      if(count($extraChargesS0) > 0){
        if(!validityCompleteArray(array(
          "serviceId" => "UInteger",
          "status" => "UInteger"
        ), $extraChargesS0)){
          $isValid = FALSE;
        }
      }
    }
    if(!$isValid){
      echo(json_encode(array(
        "code" => 400,
        "message" => "Ingrese correctamente los datos para cada extra cargo porfavor, si percisten los errores recargue la vista porfavor o avise al administrador."
      )));
      return;
    }
    if(count($receivingsDetails) > 0){
      $receivingsDetailsS1 = array_filter($receivingsDetails, function($element){
        $status = (array_key_exists("status", $element)) ? $element["status"] : -1;
        return $status == 1;
      });
      $receivingsDetailsS2 = array_filter($receivingsDetails, function($element){
        $status = (array_key_exists("status", $element)) ? $element["status"] : -1;
        return $status == 2;
      });
      $receivingsDetailsS3 = array_filter($receivingsDetails, function($element){
        $status = (array_key_exists("status", $element)) ? $element["status"] : -1;
        return $status == 3;
      });
      if(count($receivingsDetailsS3) > 0){
        if(!validityCompleteArray(array(
          "id" => "UInteger",
          "status" => "UInteger",
          "unitprice" => "UFloat",
          "days" => "UInteger"
        ), $receivingsDetailsS3)){
          $isValid = FALSE;
        }
      }
      if(count($receivingsDetailsS2) > 0){
        if(!validityCompleteArray(array(
          "id" => "UInteger",
          "status" => "UInteger",
          "unitprice" => "UFloat"
        ), $receivingsDetailsS2)){
          $isValid = FALSE;
        }
      }
      if(count($receivingsDetailsS1) > 0){
        if(!validityCompleteArray(array(
          "id" => "UInteger",
          "status" => "UInteger",
          "days" => "UInteger"
        ), $receivingsDetailsS1)){
          $isValid = FALSE;
        }
      }
    }
    if(!$isValid){
      echo(json_encode(array(
        "code" => 400,
        "message" => "Ingrese correctamente los datos para cada receivings details porfavor, si percisten los errores recargue la vista porfavor o avise al administrador."
      )));
      return;
    }

    $resultQuery = $this->serviceCustomerModel->getByCustomerId($invoice["customer_id"]);
    $servicesCustomer = $resultQuery["query"];
    $resultQuery = $this->serviceStorageInvoiceModel->getByAutoInvoiceId($invoiceId);
    $storageEC = $resultQuery["query"];
    $resultQuery = $this->autoInvoiceRecdeetsModel->getByAutoInvoiceId($invoiceId);
    $receivingsDetailsStorage = $resultQuery["query"];
    foreach($extraCharges as $charge){
      switch($charge["status"]){
        case 0:
        foreach($storageEC as $storageCharge){
          if( $charge["serviceId"] == $storageCharge["service_storage_invoice_id"] ){
            $subTotalInvoice -= $storageCharge["costService"];
            array_push($extraChargesSDelete, array(
              "service_storage_invoice_id"   => (Integer)$charge["serviceId"]
            ));
            break;
          }
        }
        break;
        case 1:
        foreach($servicesCustomer as $serviceCustomer){
          if($charge["descriptionServiceId"] == $serviceCustomer["service_id"] && $invoice["customer_id"] == $serviceCustomer["customer_id"]){
            $cost = 0;
            if($serviceCustomer["variantService"]){
              $cost = (Double)$charge["costService"];
              if($cost === $serviceCustomer["costService"]){
                $cost = $serviceCustomer["costService"];
              }
            }else{
              $cost = $serviceCustomer["costService"];
            }
            $extraTotal += $cost;
            array_push($extraChargesSInsert, array(
              "cost"        =>          $cost,
              "serviceid"   => (Integer)$charge["descriptionServiceId"],
              "information" => sanitizeString($charge["extraInformationService"])
            ));
            break;
          }
        }
        break;
      }
    }
    foreach($receivingsDetails as $receivingDetail){
      foreach($receivingsDetailsStorage as $receivingDetailStorage){
        if($receivingDetail["id"] == $receivingDetailStorage["receiving_detail_id"] && $invoiceId == $receivingDetailStorage["autoinvoice_id"]){
          $newTotalReceivingDetail = 0;
          switch($receivingDetail["status"]){
            case 1:
            $isChange            = FALSE;
            if($receivingDetail["days"] != $receivingDetailStorage["chargedDays"]){
              $qty  =       (Integer)$receivingDetailStorage["quantity"];
              $unitprice  =     (Double)$receivingDetailStorage["unitPrice"];
              $days  =       (Integer)$receivingDetail["days"];
              $newTotalReceivingDetail = $qty * $unitprice * $days;
              array_push($receivingsDetailsUpdate, array(
                "receiving_detail_id" => $receivingDetail["id"],
                "UnitPrice"   => $unitprice,
                "ChargedDays" => $days
              ));
              $isChange = TRUE;
            }
            if($isChange){
              $subTotalInvoice -= $receivingDetailStorage["total"];
              $extraTotal   +=   $newTotalReceivingDetail;
            }
            break;
            case 2:
            $isChange            = FALSE;
            if($receivingDetail["unitprice"] != $receivingDetailStorage["unitPrice"]){
              $qty  =       (Integer)$receivingDetailStorage["quantity"];
              $unitprice  =     (Double)$receivingDetail["unitprice"];
              $days  =       (Integer)$receivingDetailStorage["chargedDays"];
              $newTotalReceivingDetail = $qty * $unitprice * $days;
              array_push($receivingsDetailsUpdate, array(
                "receiving_detail_id" => $receivingDetail["id"],
                "UnitPrice"   => $unitprice,
                "ChargedDays" => $days
              ));
              $isChange = TRUE;
            }
            if($isChange){
              $subTotalInvoice -= $receivingDetailStorage["total"];
              $extraTotal   +=   $newTotalReceivingDetail;
            }
            break;
            case 3:
            $isChange            = FALSE;
            if($receivingDetail["unitprice"] != $receivingDetailStorage["unitPrice"] || $receivingDetail["days"] != $receivingDetailStorage["chargedDays"]){
              $qty  =       (Integer)$receivingDetailStorage["quantity"];
              $unitprice  =     (Double)$receivingDetail["unitprice"];
              $days  =       (Integer)$receivingDetail["days"];
              $newTotalReceivingDetail = $qty * $unitprice * $days;
              array_push($receivingsDetailsUpdate, array(
                "receiving_detail_id" => $receivingDetail["id"],
                "UnitPrice"   => $unitprice,
                "ChargedDays" => $days
              ));
              $isChange = TRUE;
            }
            if($isChange){
              $subTotalInvoice -= $receivingDetailStorage["total"];
              $extraTotal   +=   $newTotalReceivingDetail;
            }
            break;
          }
        }
      }
    }
    if($this->checkAmount($invoice["customer_id"], ($subTotalInvoice + $extraTotal) - $invoice["totalInvoice"], $invoice["totalInvoice"] - $subTotalInvoice)){
      if( count($extraChargesSInsert) > 0 ){
        $this->serviceStorageInvoiceModel->saveAll($invoiceId, $extraChargesSInsert);
      }
      if( count($extraChargesSDelete) > 0 ){
        $this->serviceStorageInvoiceModel->deleteAll($invoiceId, $extraChargesSDelete);
      }
      if( count($receivingsDetailsUpdate) > 0 ){
        $this->autoInvoiceRecdeetsModel->updateAll($invoiceId, $receivingsDetailsUpdate);
      }
      if($invoice["totalInvoice"] != ($subTotalInvoice + $extraTotal) || array_key_exists("invoiceStartDate", $params) || array_key_exists("invoiceEndDate", $params)){
        if($invoice["totalInvoice"] != ($subTotalInvoice + $extraTotal)){
          $this->customerModel->updateDueAmount($invoice["customer_id"], ($subTotalInvoice + $extraTotal) - $invoice["totalInvoice"]);
        }
        if(array_key_exists("invoiceStartDate", $params) && array_key_exists("invoiceEndDate", $params)){
          if(strcmp($params["invoiceStartDate"], $params["invoiceEndDate"]) == -1){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $params["invoiceStartDate"],
              "date"           => $params["invoiceEndDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else{
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $invoice["dateStartInvoice"],
              "date"           => $invoice["dateInvoice"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }
        }else if(array_key_exists("invoiceStartDate", $params)){
          if(strcmp($params["invoiceStartDate"], $invoice["dateInvoice"]) == -1){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $params["invoiceStartDate"],
              "date"           => $params["invoiceStartDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else if(strcmp($params["invoiceStartDate"], $invoice["dateInvoice"]) == 1 && strcmp($invoice["dateStartInvoice"], $invoice["dateInvoice"]) == 0){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $params["invoiceStartDate"],
              "date"           => $params["invoiceStartDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else{
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $invoice["dateStartInvoice"],
              "date"           => $invoice["dateInvoice"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }
        }else if(array_key_exists("invoiceEndDate", $params)){
          if(strcmp($params["invoiceEndDate"], $invoice["dateStartInvoice"]) != -1 && strcmp($invoice["dateStartInvoice"], $invoice["dateInvoice"]) != 0){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $invoice["dateStartInvoice"],
              "date"           => $params["invoiceEndDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else if(strcmp($params["invoiceEndDate"], $invoice["dateStartInvoice"]) != -1 && strcmp($invoice["dateStartInvoice"], $invoice["dateInvoice"]) == 0){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $params["invoiceEndDate"],
              "date"           => $params["invoiceEndDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else{
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $invoice["dateStartInvoice"],
              "date"           => $invoice["dateInvoice"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }
        }else{
          $this->autoInvoiceModel->updateAll(array(
            "autoinvoice_id" => $invoice["autoid"],
            "startDate"      => $invoice["dateStartInvoice"],
            "date"           => $invoice["dateInvoice"],
            "total"          => $subTotalInvoice + $extraTotal
          ));
        }
      }
      echo(json_encode(
        array(
          "code"     =>   200
        )
      ));
    }else{
      echo(json_encode(
        array(
          "code"     =>   500
        )
      ));
    }
  }

  /*
   **************************************************************************************************
   Obtiene los extra cargos de receiving para una factura
   **************************************************************************************************
  */
  public function saveInvoiceCustom(){
    $params                      = array();
    $invoice                     = array();
    $invoices                    = array();
    $invoiceId                   =       0;
    $customEC                    = array();
    $extraTotal                  =       0;
    $invoiceIds                  = array();
    $resultQuery                 = array();
    $extraCharges                = array();
    $invoiceUpdate               = array();
    $subTotalInvoice             =       0;
    $pedimentoNumber             =      -1;
    $servicesCustomer            = array();
    $extraChargesCInsert         = array();
    $extraChargesCDelete         = array();
    //Se obtienen los datos mediante la peticion PUT
    parse_str(file_get_contents('php://input'), $params);
    if(array_key_exists("invoiceId", $params)){
      if(!checkValidity("UInteger", $params["invoiceId"])){
        echo(json_encode(array(
          "code" => 404
        )));
        return;
      }
      $invoiceId = (Integer)$params["invoiceId"];
    }else{
      echo(json_encode(array(
        "code" => 404
      )));
      return;
    }
    if(array_key_exists("pedimentoNumber", $params)){
      if(!checkValidity("Integer", $params["pedimentoNumber"])){
        echo(json_encode(array(
          "code" => 400,
          "message" => "Ingresa correctamente el numero de pedimento porfavor."
        )));
        return;
      }
      $pedimentoNumber = (!is_null($params["pedimentoNumber"])) ? (Integer)$params["pedimentoNumber"] : -1;
    }
    //Se obtiene cada dato
    $dateStart = ( array_key_exists("invoiceStartDate", $params) ) ? $params["invoiceStartDate"] : "";
    $dateEnd = ( array_key_exists("invoiceEndDate", $params) ) ? $params["invoiceEndDate"] : "";
    $extraCharges = ( array_key_exists("extraCharges", $params) ) ? $params["extraCharges"] : array();

    //Se obtiene la factura para ese id
    $resultQuery  =                                $this->statusAutoInvoiceModel->getById($invoiceId);
    $invoice      =                                              $resultQuery["query"]->fetch_assoc();
    $subTotalInvoice = $invoice["totalInvoice"];
    header('Content-Type: application/json');
    //Se verifica si existe la factura
    if( count($invoice) == 0 ){
      echo(json_encode(array(
        "code" => 404
      )));
      return;
    }
    if((Integer)$invoice["typeInvoice"] !== 1){
      echo json_encode(array(
        "code" => 400,
        "message" => "Not found"
      ));
      return;
    }
    if((Integer)$invoice["isCancelled"] == 1){
      echo json_encode(array(
        "code" => 400,
        "message" => "La factura esta cancelada, no es posible actualizarla."
      ));
      return;
    }

    if((Integer)$invoice["havePayments"] == 1 || (Integer)$invoice["isPaid"] == 1){
      echo json_encode(array(
        "code" => 400,
        "message" => "La factura ya tiene pagos o ya esta pagada, no es posible actualizarla."
      ));
      return;
    }
    //Validality of each array with elements
    $isValid = TRUE;
    if(strcmp($dateStart, "") != 0){
      if(!checkValidity("Date", $dateStart)){
        $isValid = FALSE;
      }
    }
    if(strcmp($dateEnd, "") != 0){
      if(!checkValidity("Date", $dateEnd)){
        $isValid = FALSE;
      }
    }
    if(!$isValid){
      echo(json_encode(array(
        "code" => 400,
        "message" => "Ingrese correctamente el formato YYYY-MM-DD para las fechas porfavor, si percisten los errores recargue la vista porfavor o avise al administrador."
      )));
      return;
    }
    if(count($extraCharges) > 0){
      $extraChargesS1 = array_filter($extraCharges, function($element){
        return $element["status"] == 1;
      });
      $extraChargesS0 = array_filter($extraCharges, function($element){
        return $element["status"] == 0;
      });
      if(count($extraChargesS1) > 0){
        if(!validityCompleteArray(array(
          "status" => "UInteger",
          "descriptionServiceId" => "UInteger",
          "costService" => "UFloat",
          "extraInformationService" => "String"
        ), $extraChargesS1)){
          $isValid = FALSE;
        }
      }
      if(count($extraChargesS0) > 0){
        if(!validityCompleteArray(array(
          "serviceId" => "UInteger",
          "status" => "UInteger"
        ), $extraChargesS0)){
          $isValid = FALSE;
        }
      }
    }
    if(!$isValid){
      echo(json_encode(array(
        "code" => 400,
        "message" => "Ingrese correctamente los datos para cada extra cargo porfavor, si percisten los errores recargue la vista porfavor o avise al administrador."
      )));
      return;
    }

    $resultQuery = $this->serviceCustomerModel->getByCustomerId($invoice["customer_id"]);
    $servicesCustomer = $resultQuery["query"];
    $resultQuery = $this->serviceCustomInvoiceModel->getByAutoInvoiceId($invoiceId);
    $customEC = $resultQuery["query"];
    if( count($servicesCustomer) > 0 ){
      foreach($extraCharges as $charge){
        switch($charge["status"]){
          case 0:
          foreach($customEC as $customCharge){
            if( $charge["serviceId"] == $customCharge["service_custom_invoice_id"] ){
              $subTotalInvoice -= $customCharge["costService"];
              array_push($extraChargesCDelete, array(
                "service_custom_invoice_id"   => (Integer)$charge["serviceId"]
              ));
              break;
            }
          }
          break;
          case 1:
          foreach($servicesCustomer as $serviceCustomer){
            if($charge["descriptionServiceId"] == $serviceCustomer["service_id"] && $invoice["customer_id"] == $serviceCustomer["customer_id"]){
              $cost = 0;
              if($serviceCustomer["variantService"]){
                $cost = (Double)$charge["costService"];
                if($cost === $serviceCustomer["costService"]){
                  $cost = $serviceCustomer["costService"];
                }
              }else{
                $cost = $serviceCustomer["costService"];
              }
              $extraTotal += $cost;
              array_push($extraChargesCInsert, array(
                "cost"        =>          $cost,
                "serviceid"   => (Integer)$charge["descriptionServiceId"],
                "information" =>       sanitizeString($charge["extraInformationService"])
              ));
              break;
            }
          }
          break;
        }
      }
    }
    if($this->checkAmount($invoice["customer_id"], ($subTotalInvoice + $extraTotal) - $invoice["totalInvoice"], $invoice["totalInvoice"] - $subTotalInvoice)){
      if( count($extraChargesCInsert) > 0 ){
        $this->serviceCustomInvoiceModel->saveAll($invoiceId, $extraChargesCInsert);
      }
      if( count($extraChargesCDelete) > 0 ){
        $this->serviceCustomInvoiceModel->deleteAll($invoiceId, $extraChargesCDelete);
      }
      if($invoice["totalInvoice"] != ($subTotalInvoice + $extraTotal) || $pedimentoNumber > -1 || array_key_exists("invoiceStartDate", $params) || array_key_exists("invoiceEndDate", $params)){
        if($invoice["totalInvoice"] != ($subTotalInvoice + $extraTotal)){
          $this->customerModel->updateDueAmount($invoice["customer_id"], ($subTotalInvoice + $extraTotal) - $invoice["totalInvoice"]);
        }
        if($pedimentoNumber > 0){
          $this->customInvoiceDataModel->delete($invoiceId);
          $this->customInvoiceDataModel->save($pedimentoNumber, $invoiceId);
        }else if($pedimentoNumber == 0){
          $this->customInvoiceDataModel->delete($invoiceId);
        }
        if(array_key_exists("invoiceStartDate", $params) && array_key_exists("invoiceEndDate", $params)){
          if(strcmp($params["invoiceStartDate"], $params["invoiceEndDate"]) == -1){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $params["invoiceStartDate"],
              "date"           => $params["invoiceEndDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else{
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $invoice["dateStartInvoice"],
              "date"           => $invoice["dateInvoice"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }
        }else if(array_key_exists("invoiceStartDate", $params)){
          if(strcmp($params["invoiceStartDate"], $invoice["dateInvoice"]) == -1){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $params["invoiceStartDate"],
              "date"           => $params["invoiceStartDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else if(strcmp($params["invoiceStartDate"], $invoice["dateInvoice"]) == 1 && strcmp($invoice["dateStartInvoice"], $invoice["dateInvoice"]) == 0){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $params["invoiceStartDate"],
              "date"           => $params["invoiceStartDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else{
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $invoice["dateStartInvoice"],
              "date"           => $invoice["dateInvoice"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }
        }else if(array_key_exists("invoiceEndDate", $params)){
          if(strcmp($params["invoiceEndDate"], $invoice["dateStartInvoice"]) != -1 && strcmp($invoice["dateStartInvoice"], $invoice["dateInvoice"]) != 0){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $invoice["dateStartInvoice"],
              "date"           => $params["invoiceEndDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else if(strcmp($params["invoiceEndDate"], $invoice["dateStartInvoice"]) != -1 && strcmp($invoice["dateStartInvoice"], $invoice["dateInvoice"]) == 0){
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $params["invoiceEndDate"],
              "date"           => $params["invoiceEndDate"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }else{
            $this->autoInvoiceModel->updateAll(array(
              "autoinvoice_id" => $invoice["autoid"],
              "startDate"      => $invoice["dateStartInvoice"],
              "date"           => $invoice["dateInvoice"],
              "total"          => $subTotalInvoice + $extraTotal
            ));
          }
        }else{
          $this->autoInvoiceModel->updateAll(array(
            "autoinvoice_id" => $invoice["autoid"],
            "startDate"      => $invoice["dateStartInvoice"],
            "date"           => $invoice["dateInvoice"],
            "total"          => $subTotalInvoice + $extraTotal
          ));
        }
      }
      echo(json_encode(
        array(
          "code"     =>   200
        )
      ));
    }else{
      echo(json_encode(
        array(
          "code"     =>   500
        )
      ));
    }
  }

  /*
   **************************************************************************************************
   Esta funcion verifica si el cliente cuenta con credito suficiente disponible en el sistema para
   solicitar una nueva salida y verifica que el monto de lo que se realizara la salida sea igual o
   menor al credito que el cliente tiene disponible
   **************************************************************************************************
  */
  private function checkAmount($customerId, $amount, $currentTotalInvoice){
   //Nota: Se recibiran las variables en el arreglo $_GET, $_POST o $_REQUEST de PHP
   $resultQuery    = array();
   $customerAmount = array();
   //Se obtiene el estado de cuenta del cliente
   $resultQuery = $this->statusAmountModel->getById($customerId);
   //Se verifica que la consulta hay tenido exito
   if( $resultQuery["status"] ){
     //Se declara una variable temporal para recibir todos los registros de la consulta
     $customerAmount = $resultQuery["query"]->fetch_assoc();
     //Se verifica que el cliente cuenta con sufiente saldo para recibir tal monto
     if( (Boolean)$customerAmount['isAllow'] && $amount <= $customerAmount['current_amount'] + $currentTotalInvoice ){
       return TRUE;
     }else{
       return FALSE;
     }
   }else{
     //Se imprime el mensaje de error
     print_r($resultQuery["message"]);
     return FALSE;
   }
  }

}
?>

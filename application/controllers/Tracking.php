<?php
//Models
require_once(          "models/Tracking_model.php");
/*
 **************************************************************************************************
 *************************Name: Tracking Class Controller
 *************************Description:
 **************************************************************************************************
*/
class Tracking{
  private           $trackingModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->trackingModel = new Tracking_model();
  }
  /*
   **************************************************************************************************

   **************************************************************************************************
  */
  public function getTracking(){
      $params = $_GET;
      $resultQuery = array();
      $shippingId = $params["shippingId"];
      $resultQuery = $this->trackingModel->get($shippingId);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "trackings" => $resultQuery["query"],
          )
      )));
  }
}
?>

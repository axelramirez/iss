<?php
//Models
require_once(          "models/Inventory_model.php");
/*
 **************************************************************************************************
 *************************Name: Dashboard Class Controller
 **************************************************************************************************
*/
class Inventory{
  private           $inventoryModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->inventoryModel = new Inventory_model();
  }

  public function getProducts(){
      $resultQuery = array();
      $resultQuery = $this->inventoryModel->get();
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "products" => $resultQuery["query"],
              "colors" => $resultQuery["colors"],
              "tornillos" => $resultQuery["tornillos"]
          )
      )));
  }
  public function getSet(){
      $resultQuery = array();
      $resultQuery = $this->inventoryModel->getset();
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "sets" => $resultQuery["query"]
          )
      )));
  }
  public function updateProduct(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $productId = $params["productId"];
      $data = $params["updateProduct"];
     if($this->inventoryModel->update($data, $productId)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $data
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function insertProduct(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $data = $params["insertProduct"];
     if($this->inventoryModel->insert($data)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $data
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function getProductsByFilter(){
      $params = $_GET;
      $resultQuery = array();
      $filter = $params["filter"];
      $resultQuery = $this->inventoryModel->search($filter);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "items" => $resultQuery["query"],
              "totalProducts" => $resultQuery["totalProducts"]
          )
      )));
  }
  public function deleteProduct(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
     $idProduct = $params["idProduct"];
     if($this->inventoryModel->delete($idProduct)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $idProduct
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function insertReceipt(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $id = $params["id"];
      $data = $params["insertProduct"];
     if($this->inventoryModel->receipt($data, $id)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $data
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function insertShipping(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $id = $params["id"];
      $data = $params["insertProduct"];
     if($this->inventoryModel->shipping($data, $id)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $data
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function getColors(){
      $resultQuery = array();
      $resultQuery = $this->inventoryModel->getcolors();
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "colors" => $resultQuery["query"]
          )
      )));
  }
  public function insertColor(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $data = $params["insertColors"];
     if($this->inventoryModel->insertcolor($data)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $data
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function updateColor(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $colorId = $params["colorId"];
      $data = $params["updateColor"];
     if($this->inventoryModel->updatecolor($data, $colorId)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $data
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function deleteColor(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
     $idColor = $params["idColor"];
     if($this->inventoryModel->deletecolor($idColor)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $idProduct
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function insertSet(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $data = $params["insertSet"];
     if($this->inventoryModel->insertset($data)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $data
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
}
?>

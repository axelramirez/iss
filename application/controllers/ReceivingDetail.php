<?php
//Models
require_once(          "models/ReceivingDetail_model.php");
/*
 **************************************************************************************************
 *************************Name: Receiving Detail Class Controller
 *************************Description: Se encarga de agregar purchase orders
 **************************************************************************************************
*/
class ReceivingDetail{
  private           $receivingDetailModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->receivingDetailModel = new ReceivingDetail_model();
  }
  /*
   **************************************************************************************************

   **************************************************************************************************
  */
  public function insertTR(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $ntids = $params["newTrackingsId"];
      $resultQuery = $this->receivingDetailModel->inserttracking($ntids);
     if($resultQuery[status]){
         echo(json_encode(array(
             "code" => 200,
             "params" => $ntids,
             "ids" => $resultQuery["last_id"]
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function insertRD(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $receivingId = $params["receivingId"];
      $nrds = $params["newPurchasesOrders"];
     if($this->receivingDetailModel->insertreceivingdetail($nrds, $receivingId)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $nrds
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function getRD(){
      $params = $_GET;
      $resultQuery = array();
      $receivingId = (Integer)$params["receivingId"];
      $resultQuery = $this->receivingDetailModel->getByReceivingId($receivingId);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "receivingDetail" => $resultQuery["query"]
          )
      )));
  }
  public function getDetails(){
      $params = $_GET;
      $resultQuery = array();
      $receivingdetailId = (Integer)$params["receivingdetailId"];
      $resultQuery = $this->receivingDetailModel->getDetailById($receivingdetailId);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "details" => $resultQuery["query"]
          )
      )));
  }
  public function getPartsQTY(){
      $params = $_GET;
      $resultQuery = array();
      $receivingdetailId = (Integer)$params["receivingdetailId"];
      $resultQuery = $this->receivingDetailModel->getQtyParts($receivingdetailId);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "qtyParts" => $resultQuery["query"]
          )
      )));
  }
  public function getPT(){
      $params = $_GET;
      $resultQuery = array();
      $customerId = (Integer)$params["customerId"];
      $resultQuery = $this->receivingDetailModel->getPackageTypeById($customerId);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "packageType" => $resultQuery["query"]
          )
      )));
  }
  public function getAllPT(){
      $params = $_GET;
      $resultQuery = array();
      $resultQuery = $this->receivingDetailModel->getAllPackagetype();
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "packageAllType" => $resultQuery["query"]
          )
      )));
  }
  public function getUM(){
      $params = $_GET;
      $resultQuery = array();
      $resultQuery = $this->receivingDetailModel->getUnitMesurament();
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "unitMesurament" => $resultQuery["query"]
          )
      )));
  }
  public function getLO(){
      $params = $_GET;
      $resultQuery = array();
      $resultQuery = $this->receivingDetailModel->getLocationById();
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "Locations" => $resultQuery["query"]
          )
      )));
  }
  public function updateRD(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $receivingId = $params["receivingId"];
      $rds = $params["receivingsDetails"];
     if($this->receivingDetailModel->updatereceiving($rds, $receivingId)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $rds
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function deleteRD(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $receivingdetaildid = (Integer)$params["receivingDetail"]["receivingDetailId"];
     if($this->receivingDetailModel->deleteReceivingDetail($receivingdetaildid)){
         echo(json_encode(array(
             "code" => 200
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
}
?>

<?php
//Models
require_once(          "models/Location_model.php");
/*
 **************************************************************************************************
 *************************Name: Purchase Order Class Controller
 *************************Description: Se encarga de agregar purchase orders
 **************************************************************************************************
*/
class Location{
  private           $locationModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->locationModel = new Location_model();
  }
  /*
   **************************************************************************************************

   **************************************************************************************************
  */
  public function insertLocation(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
     $locationName = $params["locationName"];
     if($this->locationModel->insert($locationName)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $locationName
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function getLocations(){
      $params = $_GET;
      $resultQuery = array();
      $numberPage = (Integer)$params["page"];
      $resultQuery = $this->locationModel->get($numberPage);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "locations" => $resultQuery["query"],
              "numberEntries" => $resultQuery["numberEntries"],
              "numberPages" => $resultQuery["numberPages"]
          )
      )));
  }
  /*
   **************************************************************************************************
   Esta funcion obtiene la busqueda de locations
   **************************************************************************************************
  */
  public function searchLocation(){
      $params = $_GET;
      $resultQuery = array();
      $numberPage = (Integer)$params["page"];
      $filter = $params["filter"];
      $resultQuery = $this->locationModel->searchbypagination($numberPage, $filter);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "locations" => $resultQuery["query"],
              "numberEntries" => $resultQuery["numberEntries"],
              "numberPages" => $resultQuery["numberPages"]
          )
      )));
  }
  public function deleteLocation(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
     $idLocation = $params["idLocation"];
     if($this->locationModel->delete($idLocation)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $idLocation
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
}
?>

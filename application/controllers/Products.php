<?php
//Models
require_once(          "models/Product_model.php");
/*
 **************************************************************************************************
 *************************Name: Product Class Controller
 *************************Description: Se encarga de agregar product
 **************************************************************************************************
*/
class Product{
  private           $productModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->productModel = new Product_model();
  }
  /*
   **************************************************************************************************

   **************************************************************************************************
  */
  public function getProduct(){
      $params = $_GET;
      $resultQuery = array();
      $numberPage = (Integer)$params["page"];
      $resultQuery = $this->productModel->getbypagination($numberPage);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "parts" => $resultQuery["query"],
              "numberEntries" => $resultQuery["numberEntries"],
              "numberPages" => $resultQuery["numberPages"]
          )
      )));
  }
  public function insertProduct(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $data = $params["insertPart"];
     if($this->productModel->insertProduct($data)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $data
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function updateProductAll(){
    $params = array();
    parse_str(file_get_contents('php://input'), $params);
    header("content-type: application/json");
    $partId = $params["partId"];
    $data = $params["updatePart"];
    if($this->productModel->updateProduct($data, $partId)){
      echo(json_encode(array(
        "code" => 200,
        "params" => $data
      )));
    }else{
      echo(json_encode(array(
        "code" => 500
      )));
    }
  }
  // Search parts
  public function searchProduct(){
      $params = $_GET;
      $resultQuery = array();
      $numberPage = (Integer)$params["page"];
      $filter = $params["filter"];
      $resultQuery = $this->productModel->searchbypagination($numberPage, $filter);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "parts" => $resultQuery["query"],
              "numberEntries" => $resultQuery["numberEntries"],
              "numberPages" => $resultQuery["numberPages"]
          )
      )));
  }
}
?>

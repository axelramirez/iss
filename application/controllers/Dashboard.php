<?php
//Models
require_once(          "models/Dashboard_model.php");
/*
 **************************************************************************************************
 *************************Name: Dashboard Class Controller
 **************************************************************************************************
*/
class Dashboard{
  private           $dashboardModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->dashboardModel = new Dashboard_model();
  }

  public function getInfoDashboard(){
      $resultQuery = array();
      $resultQuery = $this->dashboardModel->get();
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "products" => $resultQuery["query"]
          )
      )));
  }
}
?>

<?php
//Models
require_once(          "models/Service_model.php");
/*
 **************************************************************************************************
 *************************Name: Purchase Order Class Controller
 *************************Description: Se encarga de agregar purchase orders
 **************************************************************************************************
*/
class Service{
  private           $serviceModel;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->serviceModel = new Service_model();
  }
  /*
   **************************************************************************************************

   **************************************************************************************************
  */
  public function insertServices(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $data = $params["insertService"];
     if($this->serviceModel->insert($data)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $data
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function updateServices(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $serviceId = $params["serviceId"];
      $data = $params["updateServices"];
     if($this->serviceModel->update($data, $serviceId)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $data
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function updateSystemCodes(){
      $params = array();
      parse_str(file_get_contents('php://input'), $params);
      header("content-type: application/json");
      $data = $params["updateCodes"];
     if($this->serviceModel->updateCode($data)){
         echo(json_encode(array(
             "code" => 200,
             "params" => $data
         )));
     }else{
         echo(json_encode(array(
             "code" => 500
         )));
     }
  }
  public function getServices(){
      $params = $_GET;
      $resultQuery = array();
      $numberPage = (Integer)$params["page"];
      $resultQuery = $this->serviceModel->getAll($numberPage);
      header("content-type: application/json");
      echo(json_encode(array(
          "code" => 200,
          "response" => array(
              "services" => $resultQuery["query"],
              "numberEntries" => $resultQuery["numberEntries"],
              "numberPages" => $resultQuery["numberPages"]
          )
      )));
  }
}
?>

<?php
/*
 **************************************************************************************************
 ***********************Name: MySQL Connection
 ***********************Gestiona una conexion unica a la base de datos de MySQL
 **************************************************************************************************
 */
class MySQLConnection{
  /*
   **************************************************************************************************
   Variables de configuracion para conectarse a la base de datos y de las instancias de conexion y
   de la misma clase de conexion
   **************************************************************************************************
   */
  private        $connection;
  private static $instance        =            null;
  private        $host            =     "localhost";
  private        $user            =          "root";
  private        $password        =    "holacrayola";
  private        $database        =    "solosynergy";

  /*
   **************************************************************************************************
   Es el constructor de la clase de conexion, crea una instancia de la conexion con mysql
   **************************************************************************************************
   */
  private function __construct(){
    $this->connection = new mysqli($this->host, $this->user, $this->password, $this->database);
  }

  /*
   **************************************************************************************************
   Es el metodo encargado de crear una sola instancia de la clase, devuelve la instancia de la clase
   **************************************************************************************************
   */
  public static function getInstance(){
    if(self::$instance == null){
      self::$instance = new MySQLConnection();
    }
    return self::$instance;
  }

  /*
   **************************************************************************************************
   Este metodo devuelve la conexion de la base de datos
   **************************************************************************************************
   */
  public function getConnection(){
    return $this->connection;
  }

  /*
   **************************************************************************************************
   Este metodo verifica que la base de datos este conectado, devuelve un boleano
   **************************************************************************************************
   */
  public function isConnect(){
    return !$this->connection->connect_error;
  }

  /*
   **************************************************************************************************
   Este metodo evita que la clase sea clonada, para evitar romper la implementacion del singleton
   **************************************************************************************************
   */
  public function __clone(){
    trigger_error("No se puede clonar la clase MySQLConnection.", E_USER_ERROR);
  }
}
?>

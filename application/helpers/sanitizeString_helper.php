<?php
function sanitizeString($data){
  return trim(filter_var(quotemeta($data), FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES |  FILTER_FLAG_STRIP_HIGH));
}
 ?>

<?php
function checkValidity($typeData, $data){
  switch($typeData){
    case "Integer":
    return validityNumber($data);
    case "UInteger":
    return validityPositiveNumber($data);
    case "UFloat":
    return validityPositiveNumber($data, TRUE);
    case "Date":
    return validityDate($data);
    case "String":
    return settype($data, "string");
    case "Array":
    return is_array($data);
  }
}
 ?>

<?php
function validateDate($data){
  return preg_match('/^[0-9]{4}\-[0-1][0-9]\-[0-3][0-9]$/', $data);
}

function validityDate($data){
  $dataArray = array();
  $month = 0;
  $day = 0;
  $year = 0;
  $isValid = TRUE;
  if(!validateDate($data)){
    $isValid = FALSE;
  }else{
    $dateArray = explode("-", $data);
    $month = (Integer) $dateArray[1];
    $day = (Integer) $dateArray[2];
    $year = (Integer) $dateArray[0];
    if(!checkdate($month, $day, $year)){
      $isValid = FALSE;
    }
  }

  if(strcmp($data, "") == 0){
    $isValid = TRUE;
  }
  return $isValid;
}
?>

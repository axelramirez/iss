<?php
/*
 **************************************************************************************************
 ***********************Name: Array By Key Function Helper
 ***********************Description: Esta funcion asigna en el indice de un arreglo de arreglos
 *********************************** asociativos el nombre de llave que se da como parametro y
 *********************************** asigna a ese indice todo el elemento para una mejor
 *********************************** manipulacion
 **************************************************************************************************
 */
function arrayByKey($nameKey, $array){
  $newArray = array();
  foreach( $array as $element ){
    if( array_key_exists($nameKey, $element) ){
      $newArray[$element[$nameKey]] = $element;
    }else{
      return NULL;
    }
  }
  return $newArray;
}
?>

<?php
function validateInteger($data){
  return preg_match('/^\-?[0-9]+$/', $data);
}

function validateFloat($data){
  return preg_match('/^\-?[0-9]+\.[0-9]+$/', $data);
}
function validityNumber($data, $isFloat = FALSE){
  $isValid = TRUE;
  if(is_nan((Integer)$data)){
    $isValid = false;
  }else{
    if($isFloat){
      if(!validateFloat($data) && !validateInteger($data)){
        $isValid = false;
      }
    }else{
      if(!validateInteger($data)){
        $isValid = false;
      }
    }
  }
  return $isValid;
}
 ?>

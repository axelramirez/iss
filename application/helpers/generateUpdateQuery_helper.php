<?php
function generateUpdateQuery($fields, $array, $data, $structure){
  $params_type = "";
  $query_string = "";
  $params = array();
  $index = 0;
  $haveRegisters = FALSE;
  foreach($fields as $field){
    if($array[$field] != $data[$field] && strcmp((string)$array[$field], (string)$data[$field]) != 0){
      $params_type .= selectTypeField($structure[$field]["type"]);
      array_push($params, $array[$field]);
      if($index > 0 && $haveRegisters){
        $query_string .= ", $field = ?";
      }else{
        $query_string .= "$field = ?";
        $haveRegisters = TRUE;
      }
    }
    $index++;
  }
  return array($haveRegisters, $params_type, $query_string, $params);
}
 ?>

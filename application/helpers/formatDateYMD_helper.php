<?php
/*
 **************************************************************************************************
   ***********************Name: Format Date Function Helper
 ***********************Description: Esta funcion da formato a la fecha
 **************************************************************************************************
 */
function formatDateYMD($date) {
    return date('Y-m-d', strtotime($date));
}
?>

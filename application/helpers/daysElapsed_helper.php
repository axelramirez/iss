<?php
/*
 **************************************************************************************************
 ***********************Name: Days Elapsed Function Helper
 ***********************Description: Esta funcion obtiene el tiempo transcurrido entre la
 *********************************** fecha del ultimo almacenamiento hasta la creacion de la
 *********************************** salida
 **************************************************************************************************
 */
function daysElapsed($startDate, $finishDate){
	$to   = date_create($finishDate);
	$from =  date_create($startDate);
  $diff =    date_diff($from, $to);
  $days =    $diff->format('%r%a');
	if( $days <= 0 ){
		return   0;
	}
	return $days;
}
?>

<?php
function validatePositiveInteger($data){
  return preg_match('/^[0-9]+$/', $data);
}

function validatePositiveFloat($data){
  return preg_match('/^[0-9]+\.[0-9]+$/', $data); 
}
function validityPositiveNumber($data, $isFloat = FALSE){
  $isValid = TRUE;
  if(is_nan((Integer)$data)){
    $isValid = false;
  }else if((Integer)$data < 0){
    $isValid = false;
  }else{
    if($isFloat){
      if(!validatePositiveFloat($data) && !validatePositiveInteger($data)){
        $isValid = false;
      }
    }else{
      if(!validatePositiveInteger($data)){
        $isValid = false;
      }
    }
  }
  return $isValid;
}
 ?>

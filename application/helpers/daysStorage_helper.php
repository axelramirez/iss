<?php
/*
 **************************************************************************************************
 ***********************Name: Days Storage Function Helper
 ***********************Description: Esta funcion obtiene los dias que tiene almacenada la
 *********************************** mercancia
 **************************************************************************************************
 */
function daysStorage($date, $freedays) {
    $start  =           strtotime($date);
    $start += (60 * 60 * 24) * $freedays;
    $now    =   strtotime(date("m/d/Y"));
    $days   =                          0;
    while ($now > $start) {
        $now = $now - (60 * 60 * 24);
        $days++;
    }
    return $days;
}
?>

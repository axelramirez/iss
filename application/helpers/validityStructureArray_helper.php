<?php
  function validityStructureArray($structureArray, $Array){
    $isValid = TRUE;
    if(count($Array) > 0 && count($structureArray) > 0){
      foreach($structureArray as $key => $value){
        if(!array_key_exists($key, $Array)){
          $isValid = FALSE;
          break;
        }else{
          if(!checkValidity($value, $Array[$key])){
            $isValid = FALSE;
            break;
          }
        }
      }
    }else{
      $isValid = FALSE;
    }
    return $isValid;
  }

  function validityCompleteArray($structureArray, $Array){
    $isValid = TRUE;
    if(count($Array) > 0 && count($structureArray) > 0){
      foreach($Array as $element){
        foreach($structureArray as $key => $value){
          if(!array_key_exists($key, $element)){
            $isValid = FALSE;
            break;
          }else{
            if(!checkValidity($value, $element[$key])){
              $isValid = FALSE;
              break;
            }
          }
        }
        if(!$isValid){
          break;
        }
      }
    }else{
      $isValid = FALSE;
    }
    return $isValid;
  }
 ?>

<?php
function filterBy($array, $key, $value){
  return array_filter($array, function($element) use($key, $value){
    return $element[$key] === $value;
  });
}
?>

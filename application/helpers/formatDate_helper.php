<?php
/*
 **************************************************************************************************
   ***********************Name: Format Date Function Helper
 ***********************Description: Esta funcion da formato a la fecha
 **************************************************************************************************
 */
function formatDate($date) {
    return date('m/d/Y', strtotime($date));
}
?>

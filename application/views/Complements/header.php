<?php
date_default_timezone_set('America/Los_Angeles');
if (!isset($_SESSION)) {
    session_start();
}
$URL = preg_replace("/application\//", "", $URI, 1);
ob_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Inventory</title>
        <?php
        if (!isset($charset)) {
            echo "<meta http-equiv = 'content-type' content = 'text/html; charset=iso-8859-1' />";
        } else {
            echo "<meta charset='$charset' />";
        }
        ?>
        <link href="<?= $URI ?>public/assets/style/menu.css" type="text/css" rel="stylesheet">
        <link href="<?= $URI ?>public/assets/style/login.css" type="text/css" rel="stylesheet">
        <link href="<?= $URI ?>public/assets/style/dialogs.css" type="text/css" rel="stylesheet">
        <link href="<?= $URI ?>public/assets/style/print.css" type="text/css" rel="stylesheet" media="print">
        <script type='text/javascript' src='<?= $URI ?>public/assets/javascript/jquery-3.2.1.js'></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" src="<?= $URI ?>public/assets/javascript/menu.js"></script>
        <script language="javascript" type="text/javascript">
            function helpSWI() {
                $('#helpSWI').show(500);
                $('#helpB').show(300);
                document.body.style.overflow = 'hidden';
            }

            function helpExit() {
                $('#helpSWI').hide(300);
                $('#helpB').hide(500);
                document.body.style.overflow = 'scroll';
            }
        </script>
    </head>
    <body>
        <?php
        include('../database.php');
        include('../functions.php');
        include('../functions_qry.php');
        include('../global_vars.php');
        ?>

        <!-- Starts top menu. -->

        <header id="afterHeader">
            <nav id="newHeader" onselectstart="return false;" class="unselectable">
                <ul>
                    <section class="btn-menu">
                        <div></div>
                        <div></div>
                        <div></div>
                    </section>
                    <div>

                    <?php
                    $isadmin = -1;
                    if (checkLogin()) {
                        $isadmin = get_rolename($_SESSION['userid']);
                        $userName = "SELECT firstname FROM user WHERE userid = " . $_SESSION['userid'];
                        $userName = mysql_query("$userName");
                        $userName = mysql_fetch_array($userName);
                        // Si no es tipo employee.

                        if ($isadmin != 4 && $isadmin != 5) {
                            ?>
                            <li class='subnav'>
                                <label>Receipts</label>
                                <span></span>
                                <ul>
                                    <li link="<?= $URL ?>receiving_search.php">Receipt Search</li>
                                    <li link="<?= $URL ?>PreReceipts/">PreReceipt Search</li>
                                </ul>
                            </li>
                            <li class='subnav'>
                                <!-- SPAN tags are used to show the drop down arrow. -->
                                <label>Shipments</label>
                                <span></span>
                                <ul>
                                    <li link="<?= $URL ?>shipping_search.php">
                                        Shipment Search
                                    </li>
                                    <li link="<?= $URL ?>preshipping_search.php">
                                        Pre-Shipment
                                    </li>
                                </ul>
                            </li>
                            <?php
                        }
                        if ($isadmin == 1 || $isadmin == 5) { // Si es tipo admin.
                            ?>
                            <li class='subnav'>
                                <label>Billing</label>
                                <span></span>
                                <ul>
                                    <li link="<?= $URL ?>billing_config.php">
                                        Customer's Quote
                                    </li>
                                    <li link="<?= $URL ?>auto_invoice.php">
                                        Invoice Schedule Setup
                                    </li>
                                    <li link="<?= $URL ?>application/index.php/invoice">
                                        Search Invoices
                                    </li>
                                    <li link="<?= $URL ?>InvoiceCreate.php">
                                        Create Invoices
                                    </li>
                                    <li class="staticText">
                                                View Reports
                                    </li>
                                    <li link="<?= $URL ?>billing_reports.php">Billing Reports</li>
                                    <li link="<?= $URL ?>pediment_reports.php">Pedimento Reports</li>
                                    </li>
                                </ul>
                            </li>
                            <?php
                        } if ($isadmin != 5) {
                            ?>
                            <li class='subnav'>
                                <label>Control</label>
                                <span></span>
                                <ul>
                                    <li class="staticText">
                                        Control
                                    </li>
                                    <li link="<?= $URL ?>control_search.php">
                                        Control Drivers
                                    </li>

                                    <?php if ($isadmin != 4) { ?>
                                        <li link="<?= $URL ?>ArrivalAlertSearch.php">
                                            Notifications
                                        </li>
                                        <?php if ($isadmin == 1 || $isadmin == 3) { ?>
                                            <li link="<?= $URL ?>RevisionSearch.php">
                                                Revisions
                                            </li>
                                            <li class="staticText">
                                                Schedules E-Mail Config
                                            </li>
                                            <li link="<?= $URL ?>schedules.php">
                                                Schedules Config
                                            </li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li>

                            <?php
                        }
                        if ($isadmin >= 1 && $isadmin <= 3) {
                            ?>
                            <li link="<?= $URL ?>CTPAT/SealSearch.php">
                                <label>CTPAT</label>
                            </li>
                            <?php
                        }
                        if ($isadmin != 4 && $isadmin != 5) {  // Si no es tipo employee.
                            ?>
                            <li link="<?= $URL ?>tande_search.php">
                                <label>Inbonds</label>
                            </li>
                            <?php if ($isadmin == 2) : // Si es tipo cliente.    ?>
                                <li class='subnav'>
                                    <label>EDI</label>
                                    <span></span>
                                    <ul>
                                        <li link="<?= $URL ?>PartsCatalogUploadView.php">
                                            EDI File Upload
                                        </li>
                                    </ul>
                                </li>
                            <?php endif; ?>
                            <li class='subnav'>
                                <label>Reports</label>
                                <span></span>
                                <ul>
                                    <li link="<?= $URL ?>rpt_receiving.php">
                                        Receiving Report
                                    </li>
                                    <li link="<?= $URL ?>rpt_receivingtotal.php">
                                        Active Inventory Report
                                    </li>
                                    <li link="<?= $URL ?>rpt_shipping.php">
                                        Shipping Report
                                    </li>
                                    <?php if ($isadmin == 1 || $isadmin == 3) { ?>
                                        <li link="<?= $URL ?>internal_report.php">
                                            Advanced Report
                                        </li>
                                        <?php
                                    }
                                    ?>
                                    <li link="<?= $URL ?>rpt_custom.php?type=1">
                                        Smart Receiving Report
                                    </li>
                                    <li link="<?= $URL ?>rpt_custom.php?type=2">
                                        Smart Shipping Report
                                    </li>
                                </ul>
                            </li>
                            <?php if ($isadmin == 1 || $isadmin == 3) { ?>
                                <li class='subnav'>
                                    <label>Catalog</label>
                                    <span></span>
                                    <ul>
                                        <li link="<?= $URL ?>parts_search.php">
                                            Parts
                                        </li>
                                        <li link="<?= $URL ?>charge_list.php">
                                            Charge
                                        </li>
                                        <li link="<?= $URL ?>carrier_search.php">
                                            Carrier
                                        </li>
                                        <li link="<?= $URL ?>customer_search.php">
                                            Customer
                                        </li>
                                        <li link="<?= $URL ?>vendor_search.php">
                                            Vendor
                                        </li>
                                        <li link="<?= $URL ?>unit_list.php">
                                            Package Type
                                        </li>
                                        <li link="<?= $URL ?>uom_list.php">
                                            Uom
                                        </li>
                                        <li link="<?= $URL ?>Location/LocationSearch.php">
                                            Location
                                        </li>
                                        <li link="<?= $URL ?>docks_list.php">
                                            Docks
                                        </li>
                                        <li link="<?= $URL ?>Services/service_list.php">
                                            Services
                                        </li>
                                    </ul>
                                </li>
                                <li link="<?= $URL ?>dashboardplus.php">
                                    <label>Console Mgr</label>
                                </li>
                                <li class='subnav'>
                                    <label>Admin</label>
                                    <span></span>
                                    <ul>
                                        <?php if ($isadmin == 1) { ?>
                                            <li link="<?= $URL ?>company_form.php">
                                                Company
                                            </li>
                                            <li link="<?= $URL ?>user_search.php">
                                                Users
                                            </li>
                                        <?php } ?>
                                        <li link="<?= $URL ?>past_receiving.php">
                                            Edit Receiving
                                        </li>
                                        <?php if ($isadmin == 1) { ?>
                                            <li link="<?= $URL ?>audit_log.php">
                                                Audit Log
                                            </li>
                                            <li class="staticText">
                                                Devices
                                            </li>
                                            <li link="<?= $URL ?>printers.php">
                                                Label Printers
                                            </li>
                                            <li link="<?= $URL ?>Zebra_Calibration.php">
                                                Zebra Calibration
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                <?php
                            }
                        }
                        ?>
                        </div>
                        <div >
                            <!-- Preshipping & log section. -->
                            <a href="<?= $URL ?>preshipping_search.php"><li class="pre">
                                <?php if ($isadmin == 1 || $isadmin == 3) { ?>
                                    New Preshippings <strong id="pre1"><img src="<?= $URI ?>public/assets/images/loading.gif" alt="Loading" /></strong>
                                <?php } ?>
                            </li></a>
                            <li class="pre">
                                Logged as <strong><?php echo ucfirst($userName['firstname']) ?></strong> <a href="<?= $URL ?>logout.php">Logout</a>
                            </li>
                        </div>
                        <?php
                    } else
                        echo "";
                    ?>
                </ul>
            </nav>
        </header>
        <!-- Ends top menu. -->

        <div class="banner">
            <div>
                <div>
                    <img src="<?= $URI ?>public/assets/images/logo/logo-systems.png" alt="">
                </div>
                <div>
                    <p>Powered by</p>
                    <div class="">
                        <img src="<?= $URI ?>public/assets/images/logo/systems.svg" alt="">
                        <div>
                            <p>Systems</p>
                            <p>Communications</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- <?php
        if ($isadmin == 1 || $isadmin == 3) {
            ?>
            <script type="text/javascript">
                var a;
                $.timer(2000, function() {
                    $.post("/ISS/loadnewpreshipping.php", {nomas: a}, function(data) {
                        a = data;
                        if (a > 0) {
                            $("#pre1").html('<a href="<?= $URL ?>preshipping_search.php">' + a + '</a>');
                        } else {
                            $("#pre1").html(0);
                        }
                    });
                });
            </script>
        <?php } ?> -->
        <div id="col1">

        </div>
    </body>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</html>

</div>
<?php
if (isset($_SESSION['userid'])) {
    $isadmin = get_rolename($_SESSION['userid']);
    if ($isadmin == 4 && basename($_SERVER['PHP_SELF']) != 'control_search.php') {
        echo "<script>"
        . "location.href = '/ISS/control_search.php'"
        . "</script>";
        exit();
    } else if ($isadmin == 5) {
        $urls = array('billing_config.php', 'auto_invoice.php', 'invoice.php'
            , 'InvoiceCreate.php');
        if (!in_array(basename($_SERVER['PHP_SELF']), $urls)) {
            echo "<script>"
            . "location.href = '/ISS/invoice.php'"
            . "</script>";
        }
    }
}
$version = '4.13.14.12';
?>
        <div class="footer">
            <div class="footer-box">
                <div>
                    <h3>Support and help</h3>
                </div>
                <div class="info-footer">
                    <div>
                        <div>
                            <h3>Address</h3>
                            <p>4492 Camino de la Plaza #293</p>
                            <p>San Ysidro CA 92173</p>
                        </div>
                        <div>
                            <h3>Phones</h3>
                            <div>
                                <p>MEX</p>
                                <p>664 - 379 - 7785</p>
                            </div>
                            <div>
                                <p>USA</p>
                                <p>619 - 407 - 7239</p>
                            </div>
                        </div>
                        <div>
                            <h3>Email</h3>
                            <a href="mailto:support@int-systems.net">support@int-systems.net</a>
                        </div>
                    </div>
                    <div>
                        <div>
                            <div>
                                <img src="<?= $URI ?>public/assets/images/logo-swi.png" alt="">
                            </div>
                            <div class="">
                                <p>Version</p>
                                <p><?= $version ?></p>
                            </div>
                        </div>
                        <div>
                            <h3>Support hours</h3>
                            <p>Systems Communications support is available 5 days a week from 8:00 to 17:00, Pacific time.</p>
                        </div>
                    </div>
                </div>
                <div>
                    <p>COPYRIGHT&copy; 2017 | </p><a href="http://int-systems.net/" target="_blank">Systems Communications</a>
                </div>
            </div>
        </div>
    </body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <link type='text/css' href='<?= $URI ?>public/assets/style/box-view.css' rel='stylesheet'/>
  <link type='text/css' href='<?= $URI ?>public/assets/style/table.css' rel='stylesheet'/>
  <link type='text/css' href='<?= $URI ?>public/assets/style/invoice.css' rel='stylesheet'/>
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>View Invoice</title>
</head>
<body data-invoice-id=<?= $invoiceId ?> data-uri="<?= $URI ?>">
  <?php
    include('views/Complements/header.php');
  ?>
  <div class="box-shadow"></div>
  <div class="shadow-notify"></div>
  <div class="shadow-table"></div>
  <div class="box-confirmation-not">
      <span class="message-dialog-not">Are you sure delete this invoice?</span>
      <div>
          <input type="button" class="yes-confirmation" name="" value="Yes">
          <input type="button" class="no-confirmation" name="" value="No">
      </div>
  </div>
  <div class="pop-msj">
      <div class="head-pop-msj">
          <img src="<?= $URI ?>public/assets/images/icons/close.svg" class="close-pop-msj">
      </div>
      <span></span>
      <div class="buttons-pop">
          <div class="confirm-pop">
              <p>Ok</p>
          </div>
      </div>
  </div>
  <div class="pop-notification save-not">
      <span>Saved successfully</span>
  </div>
  <div class="pop-notification edit-not">
      <span>Edited successfully</span>
  </div>
  <div class="pop-notification delete-not">
      <span>Deleted successfully</span>
  </div>
  <!-- Extra Charges -->
  <div class="extra-chargues" data-extra-charges=0>
    <div class="items-applied">
      <div class="title-dialog">
        <h2>Extra Charges</h2>
        <img src="<?= $URI ?>public/assets/images/icons/close.svg" class="close-dialog-shadow">
      </div>
    </div>
    <div class="applied-charges">
      <h1>Applied Charges</h1>
      <div id="services_list">
          <div class="content-loading content-ec">
              <svg class="ldi-83fims" width="200px"  height="200px"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style="background: none;"><!--?xml version="1.0" encoding="utf-8"?--><!--Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)--><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" style="transform-origin: 50px 50px 0px;" xml:space="preserve"><g style="transform-origin: 50px 50px 0px; transform: scale(0.6);"><g style="transform-origin: 50px 50px 0px;"><style type="text/css" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.9s;">.st0{fill:#040000;} .st1{fill:#050000;} .st2{fill:#E74F0C;} .st3{fill:#4D494C;} .st4{fill:#E72E11;} .st5{fill:#241C1D;}</style><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.75385s;"><circle class="st0" cx="72.6" cy="69.575" r="17.4" fill="#e85757" style="fill: rgb(232, 87, 87);"></circle></g><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.60769s;"><circle class="st0" cx="27.4" cy="69.575" r="17.4" fill="#e85757" style="fill: rgb(232, 87, 87);"></circle></g><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.46154s;"><path class="st0" d="M67.4,30.425c0,9.61-7.79,17.4-17.4,17.4s-17.4-7.79-17.4-17.4c0-9.6,7.79-17.4,17.4-17.4 S67.4,20.825,67.4,30.425z" fill="#e85757" style="fill: rgb(232, 87, 87);"></path></g><metadata xmlns:d="https://loading.io/stock/" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.31538s;">
            <d:name class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.16923s;">asana</d:name>
            <d:tags class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.02308s;">asana,brand wb</d:tags>
            <d:license class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.876923s;">cc0</d:license>
            <d:slug class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.730769s;">83fims</d:slug>
            </metadata></g></g><style type="text/css" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.584615s;">
            </style></svg></svg>
          </div>
        <table data-table-extra-chargues=0 id="applied" class="table">
        </table>
        <div class="not-found not-found-ec">
            <img src=" <?= $URI ?>public/assets/images/icons/empty.svg" alt="">
            <span>No payments found</span>
        </div>
      </div>
    </div>
  </div>
  <!-- End Extra Charges -->
  <main class="invoice-box">
    <div class="top-invoice">
      <div class="back-search" id="backButton">
        <img src="<?= $URI ?>public/assets/images/icons/arrow2.svg" alt="">
        <p>Back</p>
      </div>
      <div style="width:100%; display:flex; justify-content:flex-end;">
       <a href="<?= $URI ?>../pdfScripts/print_invoice.php?invoiceid=<?= $invoiceId ?>" target="print">
         <div class="button print">
           <img src="<?= $URI ?>public/assets/images/icons/printw.svg" width="30"  height="22" alt="Print">
           <p>Print Invoice</p>
         </div>
        </a>
        <a href="<?= $URI ?>../excelScripts/invoice_to_csv.php?invoiceid=<?= $invoiceId ?>" target="_blank">
          <div class="button excel-export">
            <img src="<?= $URI ?>public/assets/images/icons/excel.svg" width="22" height="22" alt="Excel">
            <p>Excel Report</p>
          </div>
        </a>
      </div>
    </div>
    <!-- Information Invoice -->
    <div class="invoice-head">
          <div class="title-invoice">
              <h1>In/Out Invoice</h1>
              <div class="tag-title view-invoice">
                  <span>View</span>
              </div>
          </div>
      <div class="info-invoice">
        <div>
            <p>Invoice #</p>
            <p data-info-invoice-id=0></p>
        </div>
        <div>
            <p>Status</p>
            <p data-info-status=0></p>
        </div>
        <div>
            <p>Customer</p>
            <p data-info-customer=0></p>
        </div>
        <div>
            <p>Start Date</p>
            <p data-info-start-date=0></p>
        </div>
        <div>
            <p>End Date</p>
            <p data-info-end-date=0></p>
        </div>
        <div class="options-view">
           <input type="button" class="fancyBtn button" value="Edit" onclick="window.location.href='<?= $URI ?>index.php/invoice/edit?invoiceId=<?= $invoiceId ?>'">
       </div>
      </div>
    </div>
    <!-- End Information Invoice -->
      <div class="summary-box">
        <!-- Receiving Table Invoice -->
        <div class="receiving-summary-box">
          <h1>Receiving Summary</h1>
          <div class="data-box" data-table-receiving=0>
            <div class="title-data-box">
              <div>
                <div>
                  <p>Receiving No.</p>
                </div>
                <div>
                  <p>Hazmat</p>
                </div>
                <div>
                  <p>Bound</p>
                </div>
                <div>
                  <p>Date</p>
                </div>
                <div>
                  <p>Total Amount</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- End Receiving Table Invoice -->
        <!-- Shipping Table Invoice -->
        <div class="shipping-summary-box">
          <h1>Shipment Summary</h1>
          <div class="data-box" data-table-shipping=0>
            <div class="title-data-box">
              <div>
                <div>
                  <p>Shipment No.</p>
                </div>
                <div>
                  <p>Date</p>
                </div>
                <div>
                  <p>Total Amount</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- End Shipping Table Invoice -->
      </div>
      <div class="po-view po-view-invoice">
        <div class="table-search hide table-payments">
          <h1>Payments</h1>
          <div class="nav pagination">
            <div>
              <p><span data-entries=0>0</span> entries on <span data-pages=0>0</span> Page(s)</p>
            </div>
            <div>
              <span data-value="0" class="previous"></span>
              <p data-page="0">Page <span data-number-page="0">0</span></p>
              <span data-value="0" class="next"></span>
            </div>
         </div>
         <table class="table receiving-search">
           <tbody>

           </tbody>
         </table>
         <div class="content-loading">
             <svg class="ldi-83fims" width="200px"  height="200px"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style="background: none;"><!--?xml version="1.0" encoding="utf-8"?--><!--Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)--><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" style="transform-origin: 50px 50px 0px;" xml:space="preserve"><g style="transform-origin: 50px 50px 0px; transform: scale(0.6);"><g style="transform-origin: 50px 50px 0px;"><style type="text/css" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.9s;">.st0{fill:#040000;} .st1{fill:#050000;} .st2{fill:#E74F0C;} .st3{fill:#4D494C;} .st4{fill:#E72E11;} .st5{fill:#241C1D;}</style><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.75385s;"><circle class="st0" cx="72.6" cy="69.575" r="17.4" fill="#e85757" style="fill: rgb(232, 87, 87);"></circle></g><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.60769s;"><circle class="st0" cx="27.4" cy="69.575" r="17.4" fill="#e85757" style="fill: rgb(232, 87, 87);"></circle></g><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.46154s;"><path class="st0" d="M67.4,30.425c0,9.61-7.79,17.4-17.4,17.4s-17.4-7.79-17.4-17.4c0-9.6,7.79-17.4,17.4-17.4 S67.4,20.825,67.4,30.425z" fill="#e85757" style="fill: rgb(232, 87, 87);"></path></g><metadata xmlns:d="https://loading.io/stock/" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.31538s;">
           <d:name class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.16923s;">asana</d:name>
           <d:tags class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.02308s;">asana,brand wb</d:tags>
           <d:license class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.876923s;">cc0</d:license>
           <d:slug class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.730769s;">83fims</d:slug>
           </metadata></g></g><style type="text/css" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.584615s;">
           </style></svg></svg>
         </div>
         <div class="not-found">
             <img src="<?= $URI ?>public/assets/images/icons/empty.svg" alt="">
             <span>No payments found</span>
         </div>
        </div>
        <div class="content-new-payment">
          <input type="button" id="buttonAddPayment" value="New Payment" class="button new-payment"/>
        </div>
      </div>
    <div class="total-box">
        <div class="total-invoice-box-view">
            <h1>Totals</h1>
            <div class="data-total">
                <div>
                    <p>Paid amount</p>
                    <div  class="paid-total-invoice">
                        <p>$</p>
                        <span data-info-paid-amount=0></span>
                    </div>
                </div>
                <div>
                    <p>Due amount</p>
                    <div class="due-amount-invoice">
                        <p>$</p>
                        <span data-info-due-amount=0></span>
                    </div>
                </div>
                <div class="total-invoice">
                    <p>Total amount</p>
                    <div>
                        <p>$</p>
                        <span data-info-total-amount=0></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </main>
    <!-- Insert Pop Up -->
<div class="dialog-box insert-box hide">
    <div class="title-dialog">
        <h2 >New Payment</h2>
        <img src="<?= $URI ?>public/assets/images/icons/close.svg" class="close-dialog-shadow">
    </div>
    <div class="component-box">
      <label for="textPaymentDate" class="label-component">Payment Date</label>
      <input id="textPaymentDate" type="text" class="input-component datepicker">
    </div>
    <div class="group-component-box">
        <div class="component-box">
          <label for="textReferences" class="label-component">Reference Number</label>
          <input id="textReferences" type="text" class="input-component" value="">
        </div>
          <div class="component-box">
            <label for="textAmount" class="label-component">Amount</label>
            <input id="textAmount" type="number" step="0.01" class="input-component" value="0" placeholder="$0.00">
          </div>
          <div class="component-box">
            <label for="selectType" class="label-component">Payment Type</label>
            <select id="selectType" class="input-component">
              <option value="0">None</option>
            </select>
          </div>
    </div>
    <div class="content-buttons">
      <span id="buttonInsertPayment" class="button save-btn save-payment-btn">
        Save
      </span>
      <span id="buttonCancelPayment" class="button cancel">
        Cancel
      </span>
    </div>
  </div>

</div>
    <!-- End Insert Pop Up -->
    <!-- Update Pop Up -->
    <div class="dialog-box update-box hide">
        <div class="title-dialog">
            <h2>Update Payment #<span data-update-payment=0></span></h2>
            <img src="<?= $URI ?>public/assets/images/icons/close.svg" class="close-dialog-update">
        </div>
        <div class="component-box">
          <label for="textUPaymentDate" class="label-component">Payment Date</label>
          <input id="textUPaymentDate" type="text" class="input-component datepicker">
        </div>
        <div class="group-component-box">
          <div class="component-box">
               <label for="textUReferences" class="label-component">Reference Number</label>
               <input id="textUReferences" type="text" class="input-component" value="">
          </div>
          <div class="component-box">
            <label for="textUAmount" class="label-component">Amount</label>
            <input id="textUAmount" type="number" step="0.01" class="input-component" value="0">
          </div>
          <div class="component-box">
            <label for="selectUType" class="label-component">Payment Type</label>
            <select id="selectUType" class="input-component">
              <option value="0">None</option>
            </select>
          </div>
        </div>
      <div class="content-buttons">
        <span id="buttonUpdatePayment" class="button search">
          Update
        </span>
        <span id="buttonCancelUPayment" class="button cancel">
          Cancel
        </span>
      </div>
    </div>
    <div class="loading-box">
        <svg class="ldi-83fims" width="200px"  height="200px"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style="background: none;"><!--?xml version="1.0" encoding="utf-8"?--><!--Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)--><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" style="transform-origin: 50px 50px 0px;" xml:space="preserve"><g style="transform-origin: 50px 50px 0px; transform: scale(0.6);"><g style="transform-origin: 50px 50px 0px;"><style type="text/css" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.9s;">.st0{fill:#040000;} .st1{fill:#050000;} .st2{fill:#E74F0C;} .st3{fill:#4D494C;} .st4{fill:#E72E11;} .st5{fill:#241C1D;}</style><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.75385s;"><circle class="st0" cx="72.6" cy="69.575" r="17.4" fill="#e85757" style="fill: rgb(232, 87, 87);"></circle></g><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.60769s;"><circle class="st0" cx="27.4" cy="69.575" r="17.4" fill="#e85757" style="fill: rgb(232, 87, 87);"></circle></g><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.46154s;"><path class="st0" d="M67.4,30.425c0,9.61-7.79,17.4-17.4,17.4s-17.4-7.79-17.4-17.4c0-9.6,7.79-17.4,17.4-17.4 S67.4,20.825,67.4,30.425z" fill="#e85757" style="fill: rgb(232, 87, 87);"></path></g><metadata xmlns:d="https://loading.io/stock/" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.31538s;">
      <d:name class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.16923s;">asana</d:name>
      <d:tags class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.02308s;">asana,brand wb</d:tags>
      <d:license class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.876923s;">cc0</d:license>
      <d:slug class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.730769s;">83fims</d:slug>
      </metadata></g></g><style type="text/css" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.584615s;">
      </style></svg></svg>
    </div>
    <!-- End Update Pop Up -->
  <?php
    include('views/Complements/footer.php');
  ?>
  <script src="<?= $URI ?>public/assets/javascript/Invoice/Detail/in_out_invoice.js"></script>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <link type='text/css' href='<?= $URI ?>public/assets/style/table.css' rel='stylesheet'/>
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Payments Receiving</title>
</head>
<body data-uri="<?= $URI ?>">
  <?php
    include('views/Complements/header.php');
  ?>
  <div class="box-shadow"></div>
  <div class="shadow-notify"></div>
  <div class="box-confirmation-not">
      <span class="message-dialog-not">Are you sure delete this invoice?</span>
      <div>
          <input type="button" class="yes-confirmation" name="" value="Yes">
          <input type="button" class="no-confirmation" name="" value="No">
      </div>
  </div>
  <div class="pop-msj">
      <div class="head-pop-msj">
          <img src="<?= $URI ?>public/assets/images/icons/close.svg" class="close-pop-msj">
      </div>
      <span></span>
      <div class="buttons-pop">
          <div class="confirm-pop">
              <p>Ok</p>
          </div>
      </div>
  </div>
  <div class="pop-notification save-not">
      <span>Cancelled successfully</span>
  </div>
  <div class="pop-notification edit-not">
      <span>Cancelled successfully</span>
  </div>
  <div class="pop-notification delete-not">
      <span>Cancelled successfully</span>
  </div>
  <main>
    <div class="content-box">
      <div class="section-box">
        <h1>Invoice</h1>
        <form id="invoiceSearch">
          <div>
            <div>
              <p>Invoice #</p>
              <input type="text" id="invoiceIdText" name="invoiceIdText" value="">
            </div>
            <div class="advanced-s">
              <p>Pedimento #</p>
              <input type="text" name="pedimentText" id="pedimentText" value="">
            </div>
            <div class="simple-search-customer advanced-s">
              <p>Customer</p>
              <select name="customerid" id="customerid">
                <option value="0">All</option>
              </select>
            </div>
            <div class="advanced-s">
              <p>Type</p>
              <select name="invoiceType" id="invoiceType">
                <option value="-1">All</option>
                <option value="0">In/Out Invoice</option>
                <option value="1">Custom Invoice</option>
                <option value="2">Storage Invoice</option>
                <option value="3">P&P Invoice</option>
              </select>
            </div>
            <div class="title-option advanced-s">
              <h2>Payment Date</h2>
            </div>
            <div class="section-date advanced-s">
              <div>
                <p>From</p>
                <input type="text" id="dateFrom" name="dateFrom" size="12" value="" class="datepicker">
              </div>
              <div>
                <p>To</p>
                <input type="text" id="dateTo" name="dateTo" size="12" value="" class="datepicker">
              </div>
            </div>
            <div class="bound-option advanced-s">
              <p>Status</p>
              <div>
                <div>
                  <input type="checkbox" id="statusActive" value="1">
                  <label for="statusActive">Active</label>
                </div>
                <div>
                  <input type="checkbox" id="statusPaid" value="2">
                  <label for="statusPaid">Paid</label>
                </div>
                <div>
                  <input type="checkbox" id="statusCancelled" value="3">
                  <label for="statusCancelled">Cancelled</label>
                </div>
              </div>
            </div>
          </div>
          <div class="advanced-search">
            <div class="option-advanced">
              <p class="text-search">Advance Search</p>
            </div>
          </div>
          <div class="section-options" style='display:flex;justify-content:flex-end;'>
            <button type="reset" name="button" class="button clear-search">Clear search</button>
            <input type="button" id="buttonSearch" value="Search" class="button search">
          </div>
        </form>
      </div>
    </div>
    <div class="content-result-box hide">
      <div class="options-search">
        <div>
          <div id="backButton" class="back-search">
            <img src="<?= $URI ?>public/assets/images/icons/arrow2.svg" alt="">
            <p>Back</p>
          </div>
          <div>
           <a href="#"target="print" class="hide">
             <div class="button print">
               <img src="<?= $URI ?>public/assets/images/icons/printw.svg" width="30"  height="22" alt="Print">
               <p>Print Result</p>
             </div>
            </a>
            <a href="#" target="_blank" class="hide">
              <div class="button excel-export">
                <img src="<?= $URI ?>public/assets/images/icons/excel.svg" width="22" height="22" alt="Excel">
                <p>Excel Report</p>
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="table-search hide">
        <div class="nav pagination">
          <div>
            <p><span data-entries=0>0</span> entries on <span data-pages=0>0</span> Page(s)</p>
          </div>
          <div>
            <span data-value="0" class="previous"></span>
            <p data-page="0">Page <span data-number-page="0">0</span></p>
            <span data-value="0" class="next"></span>
          </div>
       </div>
        <table class="table receiving-search">
          <tbody>
          </tbody>
        </table>
      </div>
      <div class="not-found">
          <img src="<?= $URI ?>public/assets/images/icons/empty.svg" alt="">
          <span>No results found</span>
      </div>
    </div>
  </main>
  <div class="loading-box">
      <svg class="ldi-83fims" width="200px"  height="200px"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style="background: none;"><!--?xml version="1.0" encoding="utf-8"?--><!--Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)--><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" style="transform-origin: 50px 50px 0px;" xml:space="preserve"><g style="transform-origin: 50px 50px 0px; transform: scale(0.6);"><g style="transform-origin: 50px 50px 0px;"><style type="text/css" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.9s;">.st0{fill:#040000;} .st1{fill:#050000;} .st2{fill:#E74F0C;} .st3{fill:#4D494C;} .st4{fill:#E72E11;} .st5{fill:#241C1D;}</style><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.75385s;"><circle class="st0" cx="72.6" cy="69.575" r="17.4" fill="#e85757" style="fill: rgb(232, 87, 87);"></circle></g><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.60769s;"><circle class="st0" cx="27.4" cy="69.575" r="17.4" fill="#e85757" style="fill: rgb(232, 87, 87);"></circle></g><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.46154s;"><path class="st0" d="M67.4,30.425c0,9.61-7.79,17.4-17.4,17.4s-17.4-7.79-17.4-17.4c0-9.6,7.79-17.4,17.4-17.4 S67.4,20.825,67.4,30.425z" fill="#e85757" style="fill: rgb(232, 87, 87);"></path></g><metadata xmlns:d="https://loading.io/stock/" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.31538s;">
    <d:name class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.16923s;">asana</d:name>
    <d:tags class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.02308s;">asana,brand wb</d:tags>
    <d:license class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.876923s;">cc0</d:license>
    <d:slug class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.730769s;">83fims</d:slug>
    </metadata></g></g><style type="text/css" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.584615s;">
    </style></svg></svg>
  </div>
  <?php
    include('views/Complements/footer.php');
  ?>
  <script src="<?= $URI ?>public/assets/javascript/Invoice/app.js"></script>
</body>
</html>

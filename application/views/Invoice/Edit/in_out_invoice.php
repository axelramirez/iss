<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <link type='text/css' href='<?= $URI ?>public/assets/style/box-view.css' rel='stylesheet'/>
  <link type='text/css' href='<?= $URI ?>public/assets/style/table.css' rel='stylesheet'/>
  <link type='text/css' href='<?= $URI ?>public/assets/style/invoice.css' rel='stylesheet'/>
  <link type='text/css' href='<?= $URI ?>public/assets/style/dialogs.css' rel='stylesheet'/>
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Invoice Edit</title>
</head>
<body data-invoice-id=<?= $invoiceId ?> data-uri="<?= $URI ?>">
  <?php
    include('views/Complements/header.php');
  ?>
  <div class="loading-box">
      <svg class="ldi-83fims" width="200px"  height="200px"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style="background: none;"><!--?xml version="1.0" encoding="utf-8"?--><!--Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)--><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" style="transform-origin: 50px 50px 0px;" xml:space="preserve"><g style="transform-origin: 50px 50px 0px; transform: scale(0.6);"><g style="transform-origin: 50px 50px 0px;"><style type="text/css" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.9s;">.st0{fill:#040000;} .st1{fill:#050000;} .st2{fill:#E74F0C;} .st3{fill:#4D494C;} .st4{fill:#E72E11;} .st5{fill:#241C1D;}</style><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.75385s;"><circle class="st0" cx="72.6" cy="69.575" r="17.4" fill="#e85757" style="fill: rgb(232, 87, 87);"></circle></g><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.60769s;"><circle class="st0" cx="27.4" cy="69.575" r="17.4" fill="#e85757" style="fill: rgb(232, 87, 87);"></circle></g><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.46154s;"><path class="st0" d="M67.4,30.425c0,9.61-7.79,17.4-17.4,17.4s-17.4-7.79-17.4-17.4c0-9.6,7.79-17.4,17.4-17.4 S67.4,20.825,67.4,30.425z" fill="#e85757" style="fill: rgb(232, 87, 87);"></path></g><metadata xmlns:d="https://loading.io/stock/" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.31538s;">
    <d:name class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.16923s;">asana</d:name>
    <d:tags class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.02308s;">asana,brand wb</d:tags>
    <d:license class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.876923s;">cc0</d:license>
    <d:slug class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.730769s;">83fims</d:slug>
    </metadata></g></g><style type="text/css" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.584615s;">
    </style></svg></svg>
  </div>
  <div class="box-shadow"></div>
  <div class="shadow-notify"></div>
  <div class="box-confirmation-not">
      <span class="message-dialog-not">Are you sure delete this invoice?</span>
      <div>
          <input type="button" class="yes-confirmation" name="" value="Yes">
          <input type="button" class="no-confirmation" name="" value="No">
      </div>
  </div>
  <div class="pop-msj">
      <div class="head-pop-msj">
          <img src="<?= $URI ?>public/assets/images/icons/close.svg" class="close-pop-msj">
      </div>
      <span></span>
      <div class="buttons-pop">
          <div class="confirm-pop">
              <p>Ok</p>
          </div>
      </div>
  </div>
  <div class="pop-notification save-not">
      <span>Saved successfully</span>
  </div>
  <div class="pop-notification edit-not">
      <span>Edited successfully</span>
  </div>
  <div class="pop-notification delete-not">
      <span>Deleted successfully</span>
  </div>
  <!-- Extra Charges -->
  <div class="extra-chargues" data-extra-charges=0>
    <div class="items-applied">
      <div class="title-dialog">
        <h2>Extra Charges</h2>
        <img src="<?= $URI ?>public/assets/images/icons/close.svg" class="close-dialog-shadow">
      </div>
      <div class="content-service-items">
        <div id="select_service">
          <p>Add New Charge</p>
          <select id="selectServicesCustomer" class="service">
            <option value="0">None</option>
          </select>
        </div>
        <div class="add_service">
          <div class="items-add-service">
            <div>
              <p>Code</p>
              <span data-extra-code=0></span>
            </div>
            <div>
              <p>Description</p>
              <span data-extra-description=0></span>
            </div>
            <div>
              <p>Cost</p>
              <div>
                <span id="service_cost">Set</span>
              </div>
            </div>
            <div id="titleBaseRate" style="display:none">
              <p>Base Rate</p>
              <div>
                <p>$</p>
                <span id="service_baserate"></span>
              </div>
            </div>
            <div data-variant-input=0>
              <p>Variant Cost</p>
              <input type="number" id="otherCost" value="0" step="0.01">
            </div>
            <div>
              <p>How many?</p>
              <input type="number" id="serviceQty" size="4" value="1">
            </div>
            <div class="additional-info">
              <p>Additional Information</p>
              <input type="text" id="extraInformation" maxlength="36" size="18">
            </div>
          </div>
          <div class="button-add-service">
            <input type="button" id="buttonAddCharge" class="button save-btn" value="Add Charge"/>
            <input type="button" class="button cancel-btn" value="Cancel" />
          </div>
        </div>
      </div>
    </div>
    <div class="applied-charges">
      <h1>Applied Charges</h1>
      <div id="services_list">
          <div class="content-loading">
              <svg class="ldi-83fims" width="200px"  height="200px"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style="background: none;"><!--?xml version="1.0" encoding="utf-8"?--><!--Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)--><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" style="transform-origin: 50px 50px 0px;" xml:space="preserve"><g style="transform-origin: 50px 50px 0px; transform: scale(0.6);"><g style="transform-origin: 50px 50px 0px;"><style type="text/css" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.9s;">.st0{fill:#040000;} .st1{fill:#050000;} .st2{fill:#E74F0C;} .st3{fill:#4D494C;} .st4{fill:#E72E11;} .st5{fill:#241C1D;}</style><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.75385s;"><circle class="st0" cx="72.6" cy="69.575" r="17.4" fill="#e85757" style="fill: rgb(232, 87, 87);"></circle></g><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.60769s;"><circle class="st0" cx="27.4" cy="69.575" r="17.4" fill="#e85757" style="fill: rgb(232, 87, 87);"></circle></g><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.46154s;"><path class="st0" d="M67.4,30.425c0,9.61-7.79,17.4-17.4,17.4s-17.4-7.79-17.4-17.4c0-9.6,7.79-17.4,17.4-17.4 S67.4,20.825,67.4,30.425z" fill="#e85757" style="fill: rgb(232, 87, 87);"></path></g><metadata xmlns:d="https://loading.io/stock/" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.31538s;">
            <d:name class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.16923s;">asana</d:name>
            <d:tags class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.02308s;">asana,brand wb</d:tags>
            <d:license class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.876923s;">cc0</d:license>
            <d:slug class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.730769s;">83fims</d:slug>
            </metadata></g></g><style type="text/css" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.584615s;">
            </style></svg></svg>
          </div>
        <table data-table-extra-chargues=0 id="applied" class="table">
        </table>
        <div class="not-found">
            <img src=" <?= $URI ?>public/assets/images/icons/empty.svg" alt="">
            <span>No payments found</span>
        </div>
      </div>
    </div>
  </div>
  <!-- End Extra Charges -->
  <main class="invoice-box">
    <form id="invoiceForm" action="#">
      <div class="top-invoice">
        <div class="back-search" id="backButton">
          <img src="<?= $URI ?>public/assets/images/icons/arrow2.svg" alt="">
          <p>Back</p>
        </div>
      </div>
      <!-- Information Invoice -->
      <div class="invoice-head">
        <div class="title-invoice">
            <h1>In/Out Invoice</h1>
            <div class="tag-title edit-invoice">
                <span>Edit</span>
            </div>
        </div>
        <div class="info-invoice">
          <div>
            <p>Invoice #</p>
            <p data-info-autoinvoice-id=0></p>
          </div>
          <div>
            <p>Customer</p>
            <p data-info-autoinvoice-customer=0></p>
          </div>
          <div>
            <p>Start Date</p>
            <input type="text" id="invoiceStartDate" name="invoiceStartDate" data-info-value="" value="" size="10" required pattern="[0-9]{4}-[0-1]?[0-9]-[0-3]?[0-9]" class="datepicker">
          </div>
          <div>
            <p>End Date</p>
            <input type="text" id="invoiceDate" name="invoiceDate" data-info-value="" value="" size="10" required pattern="[0-9]{4}-[0-1]?[0-9]-[0-3]?[0-9]" class="datepicker">
          </div>
        </div>
      </div>
      <!-- End Information Invoice -->
      <!-- Receiving Table and Shipping Table Invoice -->
      <div class="summary-box">
        <!-- Receiving Table Invoice -->
        <div class="receiving-summary-box">
          <h1>Receiving Summary</h1>
          <div class="data-box" data-table-receiving=0>
            <div class="title-data-box">
              <div>
                <div>
                  <p>Receiving No.</p>
                </div>
                <div>
                  <p>Date</p>
                </div>
                <div>
                  <p>Total Amount</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- End Receiving Table Invoice -->
        <!-- Shipping Table Invoice -->
        <div class="shipping-summary-box">
          <h1>Shipment Summary</h1>
          <div class="data-box" data-table-shipping=0>
            <div class="title-data-box">
              <div>
                <div>
                  <p>Shipment No.</p>
                </div>
                <div>
                  <p>Date</p>
                </div>
                <div>
                  <p>Total Amount</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- End Shipping Table Invoice -->
        <!-- Totals Invoice -->
        <div class="total-invoice-box">
          <h1>Totals</h1>
          <div class="data-total">
            <div>
              <p>Subtotal</p>
              <div>
                <span data-info-autoinvoice-subtotal=0></span>
              </div>
            </div>
            <div>
              <p>Credit</p>
              <div>
                <span data-info-autoinvoice-credit=0></span>
              </div>
            </div>
            <div class="total-invoice">
              <p>Total</p>
              <div>
                <span data-info-autoinvoice-total=0></span>
              </div>
            </div>
          </div>
        </div>
        <!-- End Totals Invoice -->
      </div>
      <div class="bottom-invoice">
        <input type="button" class="button save-btn" id="btn-save-invoice" name="" value="Save">
      </div>
    </form>
  </main>
  <?php
    include('views/Complements/footer.php');
  ?>
</body>
<script type="text/javascript" src="<?= $URI ?>public/assets/javascript/Invoice/Edit/in_out_invoice.js"></script>
</html>

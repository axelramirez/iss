<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <link type='text/css' href='<?= $URI ?>public/assets/style/box-view.css' rel='stylesheet'/>
  <link type='text/css' href='<?= $URI ?>public/assets/style/table.css' rel='stylesheet'/>
  <link type='text/css' href='<?= $URI ?>public/assets/style/invoice.css' rel='stylesheet'/>
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Invoice Edit</title>
</head>
<body data-invoice-id=<?= $invoiceId ?> data-uri="<?= $URI ?>">
  <?php
    include('views/Complements/header.php');
  ?>
  <div class="box-shadow"></div>
  <div class="extra-chargues">
      <div class="items-applied">
          <div class="title-dialog">
              <h2>Extra Chargues</h2>
              <img src="<?= $URI ?>public/assets/images/icons/close.svg" class="close-dialog-shadow">
          </div>
          <div class="info-manual-extra">
              <div>
                  <p>Date</p>
                  <input type="date" name="" value="">
              </div>
              <div>
                  <p>Pediment No.</p>
                  <input type="text" name="" value="">
              </div>
          </div>
          <div class="content-service-items">
              <div id="select_service">
                  <p>Add New Chargue</p>
                  <select class="service">
                      <option value=""></option>
                      <option value="">Chargue 1</option>
                      <option value="">Chargue 2</option>
                      <option value="">Chargue 3</option>
                      <option value="">Chargue 4</option>
                  </select>
              </div>
              <div class="add_service">
                  <div class="items-add-service">
                      <div>
                          <p>Code</p>
                          <span>1234</span>
                      </div>
                      <div>
                          <p>Description</p>
                          <span>Cargo description</span>
                      </div>
                      <div>
                          <p>Cost</p>
                          <div>
                              <p>$</p>
                              <span id="service_cost">0.00</span>
                          </div>
                      </div>
                      <div id="titleBaseRate">
                          <p>Base Rate</p>
                          <div>
                              <p>$</p>
                              <span id="service_baserate"></span>
                          </div>
                      </div>
                      <div>
                          <p>Variant Cost</p>
                          <input type="text" id="other_cost">
                      </div>
                      <div>
                          <p>How many?</p>
                          <input type="text" id="serviceQty" size="4" value="1">
                      </div>
                      <div class="additional-info">
                          <p>Additional Information</p>
                          <input type="text" id="information" maxlength="36" size="18">
                      </div>
                  </div>
                  <div class="button-add-service">
                      <input type="button" class="button save-btn" value="Add Charge" disabled="disabled"/>
                      <input type="button" class="button cancel-btn" value="Cancel" />
                  </div>
              </div>
          </div>
      </div>
      <div class="applied-charges">
          <h1>Applied Charges</h1>
          <div id="services_list">
              <table id="applied" class="table">
                  <tr>
                      <td>Delete</td>
                      <td>Code</td>
                      <td>Description</td>
                      <td>Cost</td>
                  </tr>
                  <tr>
                      <td class="delete-chargue">
                          <img src="<?= $URI ?>public/assets/images/icons/delete.svg" alt="">
                      </td>
                      <td>1234</td>
                      <td>Description</td>
                      <td>$ <span>0.00</span></td>
                  </tr>
              </table>
          </div>
      </div>
  </div>
  <main class="invoice-box">
      <div class="top-invoice">
        <div class="back-search">
            <img src="<?= $URI ?>public/assets/images/icons/arrow2.svg" alt="">
            <p>Back to Invoices Search</p>
        </div>
      </div>
      <div class="invoice-head">
          <h1>Invoice Creation</h1>
          <div class="invoice-creation-box">
              <div>
                  <p>Please choose a Customer</p>
                  <select class="" name="">
                      <option value="">
                          Customer 1
                      </option>
                  </select>
              </div>
              <div>
                <p>Generate a new Warehouse Invoice</p>
                <input type="button" class="button create-btn" name="" value="Warehouse Invoice">
              </div>
              <div>
                <p>Generate a new Storage Invoice</p>
                <input type="button" class="button create-btn storage-invoice" name="" value="Storage Invoice">
              </div>
              <div>
                <p>Generate a new Manual Invoice</p>
                <input type="button" class="button create-btn manual-invoice-btn" name="" value="Manual Invoice">
              </div>
          </div>
      </div>
  </main>
  <?php
    include('views/Complements/footer.php');
  ?>
</body>
<script src="<?= $URI ?>public/assets/javascript/Invoice/Detail/app.js"></script>
<script src="<?= $URI ?>public/assets/javascript/Invoice/invoice.js"></script>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <link type='text/css' href='<?= $URI ?>public/assets/style/box-view.css' rel='stylesheet'/>
  <link type='text/css' href='<?= $URI ?>public/assets/style/table.css' rel='stylesheet'/>
  <link type='text/css' href='<?= $URI ?>public/assets/style/invoice.css' rel='stylesheet'/>
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Invoice Edit</title>
</head>
<body data-invoice-id=<?= $invoiceId ?> data-uri="<?= $URI ?>">
  <?php
    include('views/Complements/header.php');
  ?>
  <div class="loading-box">
      <svg class="ldi-83fims" width="200px"  height="200px"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style="background: none;"><!--?xml version="1.0" encoding="utf-8"?--><!--Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)--><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" style="transform-origin: 50px 50px 0px;" xml:space="preserve"><g style="transform-origin: 50px 50px 0px; transform: scale(0.6);"><g style="transform-origin: 50px 50px 0px;"><style type="text/css" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.9s;">.st0{fill:#040000;} .st1{fill:#050000;} .st2{fill:#E74F0C;} .st3{fill:#4D494C;} .st4{fill:#E72E11;} .st5{fill:#241C1D;}</style><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.75385s;"><circle class="st0" cx="72.6" cy="69.575" r="17.4" fill="#e85757" style="fill: rgb(232, 87, 87);"></circle></g><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.60769s;"><circle class="st0" cx="27.4" cy="69.575" r="17.4" fill="#e85757" style="fill: rgb(232, 87, 87);"></circle></g><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.46154s;"><path class="st0" d="M67.4,30.425c0,9.61-7.79,17.4-17.4,17.4s-17.4-7.79-17.4-17.4c0-9.6,7.79-17.4,17.4-17.4 S67.4,20.825,67.4,30.425z" fill="#e85757" style="fill: rgb(232, 87, 87);"></path></g><metadata xmlns:d="https://loading.io/stock/" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.31538s;">
    <d:name class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.16923s;">asana</d:name>
    <d:tags class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.02308s;">asana,brand wb</d:tags>
    <d:license class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.876923s;">cc0</d:license>
    <d:slug class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.730769s;">83fims</d:slug>
    </metadata></g></g><style type="text/css" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.584615s;">
    </style></svg></svg>
  </div>
  <div class="box-shadow"></div>
  <div class="shadow-notify"></div>
  <div class="box-confirmation-not">
      <span class="message-dialog-not">Are you sure delete this invoice?</span>
      <div>
          <input type="button" class="yes-confirmation" name="" value="Yes">
          <input type="button" class="no-confirmation" name="" value="No">
      </div>
  </div>
  <div class="pop-msj">
      <div class="head-pop-msj">
          <img src="<?= $URI ?>public/assets/images/icons/close.svg" class="close-pop-msj">
      </div>
      <span></span>
      <div class="buttons-pop">
          <div class="confirm-pop">
              <p>Ok</p>
          </div>
      </div>
  </div>
  <div class="pop-notification save-not">
      <span>Saved successfully</span>
  </div>
  <div class="pop-notification edit-not">
      <span>Edited successfully</span>
  </div>
  <div class="pop-notification delete-not">
      <span>Deleted successfully</span>
  </div>
  <!-- Extra Charges -->
  <div class="extra-chargues" data-extra-charges=0>
    <div class="items-applied">
      <div class="title-dialog">
        <h2>Extra Charges</h2>
        <img src="<?= $URI ?>public/assets/images/icons/close.svg" class="close-dialog-shadow">
      </div>
      <div class="content-service-items">
        <div id="select_service">
          <p>Add New Charge</p>
          <select id="selectServicesCustomer" class="service">
            <option value="0">None</option>
          </select>
        </div>
        <div class="add_service">
          <div class="items-add-service">
            <div>
              <p>Code</p>
              <span data-extra-code=0></span>
            </div>
            <div>
              <p>Description</p>
              <span data-extra-description=0></span>
            </div>
            <div>
              <p>Cost</p>
              <div>
                <span id="service_cost">Set</span>
              </div>
            </div>
            <div id="titleBaseRate" style="display:none">
              <p>Base Rate</p>
              <div>
                <p>$</p>
                <span id="service_baserate"></span>
              </div>
            </div>
            <div data-variant-input=0>
              <p>Variant Cost</p>
              <input type="number" id="otherCost" value="0" step="0.01">
            </div>
            <div>
              <p>How many?</p>
              <input type="number" id="serviceQty" size="4" value="1">
            </div>
            <div class="additional-info">
              <p>Additional Information</p>
              <input type="text" id="extraInformation" maxlength="36" size="18">
            </div>
          </div>
          <div class="button-add-service">
            <input type="button" id="buttonAddCharge" class="button save-btn" value="Add Charge"/>
            <input type="button" class="button cancel-btn" value="Cancel" />
          </div>
        </div>
      </div>
    </div>
    <div class="applied-charges">
      <h1>Applied Charges</h1>
      <div id="services_list">
          <div class="content-loading">
              <svg class="ldi-83fims" width="200px"  height="200px"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style="background: none;"><!--?xml version="1.0" encoding="utf-8"?--><!--Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)--><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" style="transform-origin: 50px 50px 0px;" xml:space="preserve"><g style="transform-origin: 50px 50px 0px; transform: scale(0.6);"><g style="transform-origin: 50px 50px 0px;"><style type="text/css" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.9s;">.st0{fill:#040000;} .st1{fill:#050000;} .st2{fill:#E74F0C;} .st3{fill:#4D494C;} .st4{fill:#E72E11;} .st5{fill:#241C1D;}</style><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.75385s;"><circle class="st0" cx="72.6" cy="69.575" r="17.4" fill="#e85757" style="fill: rgb(232, 87, 87);"></circle></g><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.60769s;"><circle class="st0" cx="27.4" cy="69.575" r="17.4" fill="#e85757" style="fill: rgb(232, 87, 87);"></circle></g><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.46154s;"><path class="st0" d="M67.4,30.425c0,9.61-7.79,17.4-17.4,17.4s-17.4-7.79-17.4-17.4c0-9.6,7.79-17.4,17.4-17.4 S67.4,20.825,67.4,30.425z" fill="#e85757" style="fill: rgb(232, 87, 87);"></path></g><metadata xmlns:d="https://loading.io/stock/" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.31538s;">
            <d:name class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.16923s;">asana</d:name>
            <d:tags class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.02308s;">asana,brand wb</d:tags>
            <d:license class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.876923s;">cc0</d:license>
            <d:slug class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.730769s;">83fims</d:slug>
            </metadata></g></g><style type="text/css" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.584615s;">
            </style></svg></svg>
          </div>
        <table data-table-extra-chargues=0 id="applied" class="table">
        </table>
        <div class="not-found">
            <img src=" <?= $URI ?>public/assets/images/icons/empty.svg" alt="">
            <span>No payments found</span>
        </div>
      </div>
    </div>
  </div>
  <!-- End Extra Charges -->
  <main class="invoice-box">
    <form id="invoiceForm" action="#">
      <div class="top-invoice">
        <div class="back-search" id="backButton">
          <img src="<?= $URI ?>public/assets/images/icons/arrow2.svg" alt="">
          <p>Back</p>
        </div>
      </div>
      <!-- Information Invoice -->
      <div class="invoice-head">
            <div class="title-invoice">
                <h1>Storage Invoice</h1>
                <div class="tag-title edit-invoice">
                    <span>Edit</span>
                </div>
            </div>
        <div class="info-invoice">
          <div>
            <p>Invoice #</p>
            <p data-info-autoinvoice-id=0></p>
          </div>
          <div>
            <p>Customer</p>
            <p data-info-autoinvoice-customer=0></p>
          </div>
          <div>
            <p>Start Date</p>
            <input type="text" id="invoiceStartDate" name="invoiceStartDate" data-info-value="" value="" size="10" required pattern="[0-9]{4}-[0-1]?[0-9]-[0-3]?[0-9]" class="datepicker">
          </div>
          <div>
            <p>End Date</p>
            <input type="text" id="invoiceDate" name="invoiceDate" data-info-value="" value="" size="10" required pattern="[0-9]{4}-[0-1]?[0-9]-[0-3]?[0-9]" class="datepicker">
          </div>
        </div>
      </div>
      <!-- End Information Invoice -->
      <div class="storage-box">
          <div class="storage-box-head">
              <div class="select-columns">
                  <p>Show / Hide Columns</p>
                  <ul>
                      <div id="2" class="item-column">
                          <img src="<?= $URI ?>public/assets/images/icons/view-w.svg" class="select-view">
                          <img src="<?= $URI ?>public/assets/images/icons/no-view.svg" class="select-no-view">
                          <li>Date</li>
                      </div>
                      <div id="3" class="item-column">
                          <img src="<?= $URI ?>public/assets/images/icons/view-w.svg" class="select-view">
                          <img src="<?= $URI ?>public/assets/images/icons/no-view.svg" class="select-no-view">
                          <li>Int. # Rec.</li>
                      </div>
                      <div id="4" class="item-column">
                          <img src="<?= $URI ?>public/assets/images/icons/view-w.svg" class="select-view">
                          <img src="<?= $URI ?>public/assets/images/icons/no-view.svg" class="select-no-view">
                          <li>Bill of lading</li>
                      </div>
                      <div id="5" class="item-column">
                          <img src="<?= $URI ?>public/assets/images/icons/view-w.svg" class="select-view">
                          <img src="<?= $URI ?>public/assets/images/icons/no-view.svg" class="select-no-view">
                          <li>Inbond</li>
                      </div>
                  </ul>
              </div>
              <div>
                  <div class="open-extra-chargue">
                      <div class="btn-extra-chargue">
                          <img src="<?= $URI ?>public/assets/images/icons/dollar.svg">
                      </div>
                      <p>Manage Extra Charges</p>
                  </div>
                  <div class="total-storage-invoice">
                      <p>Amount</p>
                      <div class="total-storage-span">
                          <p>$</p>
                          <span data-total-services=0>0.00</span>
                      </div>
                  </div>
              </div>
          </div>
          <div class="data-storage-box">
              <div class="title-storage-box">
                  <div class="column-1">
                      <p>Receiving</p>
                  </div>
                  <div class="column-2">
                      <p>Date</p>
                  </div>
                  <div class="column-3">
                      <p>Int. # Rec.</p>
                  </div>
                  <div class="column-4">
                      <p>Bill of lading</p>
                  </div>
                  <div class="column-5">
                      <p>Inbond</p>
                  </div>
                  <div class="column-6">
                      <p>Qty</p>
                  </div>
                  <div class="column-7">
                      <p>UOM</p>
                  </div>
                  <div class="column-8">
                      <p>Days</p>
                  </div>
                  <div class="column-9">
                      <p>CPD</p>
                  </div>
                  <div class="column-10">
                      <p>Amount</p>
                  </div>
                  <div class="column-11">
                      <p>Revert</p>
                  </div>
              </div>
              <div class="content-data-storage-box" data-table-receiving-detail=0>

              </div>
          </div>
      </div>
      <!-- Totals Invoice -->
      <div class="total-invoice-box">
        <h1>Totals</h1>
        <div class="data-total">
          <div>
            <p>Subtotal</p>
            <div>
              <span data-info-autoinvoice-subtotal=0></span>
            </div>
          </div>
          <div class="total-invoice">
            <p>Total</p>
            <div>
              <span data-info-autoinvoice-total=0></span>
            </div>
          </div>
        </div>
      </div>
      <!-- End Totals Invoice -->
      <div class="bottom-invoice">
          <input type="button" class="button save-btn" id="btn-save-invoice" name="" value="Save">
      </div>
    </form>
  </main>
  <?php
    include('views/Complements/footer.php');
  ?>
  <script src="<?= $URI ?>public/assets/javascript/Invoice/Edit/storage_invoice.js"></script>
</body>
</html>

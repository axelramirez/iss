<?php
require_once("app.php");
function main(){
    try{
        $app = new App();
        $app->render();

    } catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
    }
}

main();
?>

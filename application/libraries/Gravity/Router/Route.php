<?php
/*
 **************************************************************************************************
 *************************Name: Route Class
 *************************Description: Mantiene una ruta, y contiene los metodos para validar,
 ************************* Que los datos de la url den match con la ruta
 **************************************************************************************************
*/
class Route{
  private      $method;
  private    $callback;
  private   $route_uri;
  private $route_alias;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct($method, $route, $callback, $alias = null){
    $this->method      =   $method;
    $this->callback    = $callback;
    $this->route_uri   =    $route;
    $this->route_alias =    $alias;
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de validar que una ruta dada como parametro tenga coincidencia con la uri,
   de la clase
   **************************************************************************************************
  */
  public function match($method, $route){
    if( strcmp( $method, $this->method ) == 0 && strcmp( $route, $this->route_uri ) == 0 ){
      call_user_func($this->callback);
      return TRUE;
    }
    return FALSE;
  }
}
?>

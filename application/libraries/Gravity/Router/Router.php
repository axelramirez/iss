<?php
//Import Class
require_once("Route.php");
/*
 **************************************************************************************************
 *************************Name: Router Class
 *************************Description: Gestiona un conjunto de rutas http del servidor
 **************************************************************************************************
*/
class Router{
  private     $routes;
  private $routerName;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct($name){
    $this->routes      = array(
      "GET"    => array(),
      "POST"   => array(),
      "PUT"    => array(),
      "DELETE" => array()
    );
    $this->routerName = $name;
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de insertar al arreglo de rutas, una nueva ruta tipo get
   **************************************************************************************************
  */
  public function get($uri, $callback, $alias=null){
    if( count($this->routes) > 0 ){
      $this->matchRoute("GET", "/".$this->routerName.$uri);
    }
    $this->routes["GET"]["/".$this->routerName.$uri] = new Route("GET", "/".$this->routerName.$uri, $callback, $alias);
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de insertar al arreglo de rutas, una nueva ruta tipo post
   **************************************************************************************************
  */
  public function post($uri, $callback, $alias=null){
    if( count($this->routes) > 0 ){
      if( $this->matchRoute("POST", $uri) )
        return;
    }
    $this->routes["POST"]["/".$this->routerName.$uri] = new Route("POST", "/".$this->routerName.$uri, $callback, $alias);
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de insertar al arreglo de rutas, una nueva ruta tipo put
   **************************************************************************************************
  */
  public function put($uri, $callback, $alias=null){
    if( count($this->routes) > 0 ){
      if( $this->matchRoute("PUT", $uri) )
        return;
    }
    $this->routes["PUT"]["/".$this->routerName.$uri] = new Route("PUT", "/".$this->routerName.$uri, $callback, $alias);
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de insertar al arreglo de rutas, una nueva ruta tipo delete
   **************************************************************************************************
  */
  public function delete($uri, $callback, $alias=null){
    if( count($this->routes) > 0 ){
      if( $this->matchRoute("DELETE", $uri) )
        return;
    }
    $this->routes["DELETE"]["/".$this->routerName.$uri] = new Route("DELETE", "/".$this->routerName.$uri, $callback, $alias);
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de verificar si la ruta que se desea insertar ya existe en el arreglo de
   rutas
   **************************************************************************************************
  */
  public function matchRoute($method, $uri){
    if( !empty($this->routes[$method][$uri]) ){
      if( $this->routes[$method][$uri]->match($method, $uri) ){
        return TRUE;
      }
    }
    return FALSE;
  }
}
?>

<?php
//Import Class
require_once("Router/Router.php");
/*
 **************************************************************************************************
 *************************Name: Router Class
 *************************Description: Gestiona un conjunto de rutas http del servidor
 **************************************************************************************************
*/
class Gravity{
  private $routers;
  /*
   **************************************************************************************************
   Es el constructor de la clase, solo instancia los modelos a utilizar
   **************************************************************************************************
  */
  public function __construct(){
    $this->routers = array();
    array_push($this->routers, new Router(""));
    $this->get("", function(){
      require_once("views/index.html");
    });
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de insertar al arreglo de rutas, una nueva ruta tipo get
   **************************************************************************************************
  */
  public function get($uri, $callback, $alias=null){
    $this->routers[0]->get($uri, $callback, $alias);
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de insertar al arreglo de rutas, una nueva ruta tipo post
   **************************************************************************************************
  */
  public function post($uri, $callback, $alias=null){
    $this->routers[0]->post($uri, $callback, $alias);
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de insertar al arreglo de rutas, una nueva ruta tipo put
   **************************************************************************************************
  */
  public function put($uri, $callback, $alias=null){
    $this->routers[0]->put($uri, $callback, $alias);
  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de insertar al arreglo de rutas, una nueva ruta tipo delete
   **************************************************************************************************
  */
  public function delete($uri, $callback, $alias=null){
    $this->routers[0]->delete($uri, $callback, $alias);
  }
  /*
   **************************************************************************************************
   Este metodo se encarga de agregar un nuevo objeto router al arreglo de routers
   **************************************************************************************************
  */
  public function useRouter($router){

  }
  /*
   **************************************************************************************************
   Esta funcion se encarga de verificar la url que se ha recibido desde el navegador
   **************************************************************************************************
  */
  public function listen(){
    foreach($this->routers as $router){
      if( !$router->matchRoute($_SERVER["REQUEST_METHOD"], $_SERVER["PATH_INFO"]) ){
        echo "Error 404: Pagina no encontrada";
      }
    }
  }
}
?>

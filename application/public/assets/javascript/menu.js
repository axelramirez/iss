$(document).ready(function() {
	//Menu
	var sw = screen.width;
	var cm = 0;

	$('.subnav').click(function(){
		if(cm == 0){
			$(this).find('ul').css({"display":"block"});
			if(sw <= 1295){
				$(this).find('span').css({"transform":"rotate(-90deg)","transition":"0.5s"});
			}
			else{
				$(this).find('span').css({"transform":"rotatex(180deg)","transition":"0.5s"});
			}
			cm = 1;
		}
		else{
			cm = 0;
			$('#newHeader ul li ul').css({"display":"none"});
            $('#newHeader ul li span').css({"transform":"rotatex(0deg)","transition":"0.5s"});
		}

	});
	$('#newHeader li').click(function() {
		if ($(this).attr('link'))
			window.location.href = $(this).attr('link');
	});
    //Responsive menu
    var rm = 0;
	$('.btn-menu').click(function(){
		if(rm == 0){
			$('#newHeader ul > div:nth-child(2)').css({"display":"block"});
			$('#newHeader ul > .btn-menu').css({"justify-content":"center"});
			$('#newHeader ul > .btn-menu > div:nth-child(2)').css({"display":"none","transition":"0.5s"});
			$('#newHeader ul > .btn-menu > div:nth-child(1)').css({"transform":"rotate(-45deg)","margin-top":"0px", "transition":"0.5s"});
			$('#newHeader ul > .btn-menu > div:nth-child(3)').css({"transform":"rotate(45deg)","margin-bottom":"0px","transition":"0.5s"});

			rm = 1;
		}
		else{
			rm = 0;
			$('#newHeader ul > div:nth-child(2)').css({"display":"none"});
			$('#newHeader ul > .btn-menu').css({"justify-content":"space-around"});
			$('#newHeader ul > .btn-menu > div:nth-child(2)').css({"display":"flex","transition":"0.5s"});
			$('#newHeader ul > .btn-menu > div:nth-child(1)').css({"transform":"rotate(0deg)","margin-top":"5px", "transition":"0.5s"});
			$('#newHeader ul > .btn-menu > div:nth-child(3)').css({"transform":"rotate(0deg)","margin-bottom":"5px","transition":"0.5s"});
		}
	});
	$(function(){
		$(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' }).val();
	});
});
/************************************Loading**************************************/
function openloading(){
	$('body').css({"overflow":"hidden"});
	$('.loading-box').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"loading-in","animation-duration":"0.01s"});
    $('.loading-box svg').css({"display":"block","animation":"pop-in","animation-duration":"0.25s"});
	setTimeout(function(){
		$('.loading-box svg').css({"animation":"pop-out","animation-duration":"0.25s"});
		setTimeout(function(){
			$('.loading-box svg').css({"display":"none"});
		},220);
		$('.loading-box').css({"animation":"loading-out","animation-duration":"0.45s"});
		setTimeout(function(){
			$('.loading-box').css({"display":"none"});
		},450);

	},1000);
	$('body').css({"overflow":"auto"});
}
function openloadingcontent(){
	$('.content-loading').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"loading-in","animation-duration":"0.01s"});
    $('.content-loading svg').css({"display":"block","animation":"content-pop-in","animation-duration":"0.25s"});
	setTimeout(function(){
		$('.content-loading svg').css({"animation":"content-pop-out","animation-duration":"0.25s"});
		setTimeout(function(){
			$('.content-loading svg').css({"display":"none"});
		},220);
		$('.content-loading').css({"animation":"loading-out","animation-duration":"0.45s"});
		setTimeout(function(){
			$('.content-loading').css({"display":"none"});
		},450);
	},500);
}

function openloadingcontentEC(){
	$('.content-loading.content-ec').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"loading-in","animation-duration":"0.01s"});
    $('.content-loading.content-ec svg').css({"display":"block","animation":"content-pop-in","animation-duration":"0.25s"});
	setTimeout(function(){
		$('.content-loading.content-ec svg').css({"animation":"content-pop-out","animation-duration":"0.25s"});
		setTimeout(function(){
			$('.content-loading.content-ec svg').css({"display":"none"});
		},220);
		$('.content-loading.content-ec').css({"animation":"loading-out","animation-duration":"0.45s"});
		setTimeout(function(){
			$('.content-loading.content-ec').css({"display":"none"});
		},450);
	},500);
}

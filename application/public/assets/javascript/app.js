//App JS Main
(()=>{
  
})()

/*
 ************************************************************************************************************
 Esta funcion se encarga de realizar una peticion al servidor a la ruta que se encarga de calcular e insertar
 los datos de cantidades de mercancias de salida, asi tambien como de actualizar las cantidades de salida
 solo si el precio total de la cantidad a sacar para un cliente entra en el monto debido
 ************************************************************************************************************
*/
function requestNewShipping(){
  fetch("application/controllers/Customer.php",
  {
    body: { data : false }
  })
  .then(res => res.json())
  .then((data)=>{
    console.log(data);
  });
}

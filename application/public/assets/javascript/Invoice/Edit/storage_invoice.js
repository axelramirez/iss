var ec        =  0;
var det       =  0;
var sdet      =  0;
var invoiceId =  0;
var serverURI = "";
var lengthEC  =  0;
var lengthIndex = -1;
var dateStartInvoice = "";
var dateEndInvoice = "";
var statusDateInvoice = false;
var notFoundCharges = false;
var selectServicesCustomer =  $('#selectServicesCustomer');
var totalServices = 0;
var totalInvoice  = 0;
var statusAction = 0;
//Variables to Update Invoice
var receivingsDetails   = [];
var extraCharges        = [];

openloading();

(()=>{
  initialize()
  document.querySelector("#invoiceForm").addEventListener("submit", (e)=>{
    e.preventDefault()
  })
  window.addEventListener('beforeunload', (e)=>{
    if((extraCharges.length != 0 || receivingsDetails.length != 0 || dateStartInvoice.localeCompare("") != 0 || dateEndInvoice.localeCompare("") != 0) && statusAction == 0){
      e.returnValue = "Cambios detectados, estas seguro que deseas salir?";
      return "Cambios detectados, estas seguro que deseas salir?";
    }
    e.preventDefault();
  })
  getInvoice()
})()

function getInvoice(){
  $.ajax({
    url: `${serverURI}index.php/invoice/storage/edit/get`,
    type: "GET",
    dataType: 'json',
    data: {
      invoiceId
    },
    success: data=>{
      switch(data.code){
        case 200:
        totalInvoice = parseFloat(data.response.invoice.totalInvoice)
        totalServices = parseFloat(data.response.invoice.totalServices)
        var receivingDetailTable         = $('[data-table-receiving-detail=0]')
        $('[data-info-autoinvoice-id=0]').html(data.response.invoice.autoinvoice_number)
        $('[data-info-autoinvoice-customer=0]').html(data.response.invoice.name)
        $('[data-total-services=0]').html(money_format(data.response.invoice.totalServices))
        if(data.response.invoice.dateInvoice == data.response.invoice.dateStartInvoice){
          $('#invoiceDate').parent().hide()
          $('#invoiceStartDate').prev().html("Date")
          statusDateInvoice = false
        }else{
          statusDateInvoice = true
        }
        $('#invoiceDate').val(data.response.invoice.dateInvoice)
        $('#invoiceStartDate').val(data.response.invoice.dateStartInvoice)
        $('#invoiceDate').data("value", data.response.invoice.dateInvoice)
        $('#invoiceStartDate').data("value", data.response.invoice.dateStartInvoice)
        data.response.receivingsDetails.forEach(receivingInvoice=>{
          receivingDetailTable.append(putReceivingDetail(receivingInvoice))
        })
        $('[data-info-autoinvoice-subtotal=0]').html(`$ ${money_format(data.response.invoice.subTotalInvoice)}`)
        $('[data-info-autoinvoice-total=0]').html(`$ ${money_format(data.response.invoice.totalInvoice)}`)
        if(data.response.invoice.haveServicesCustomer){
          data.response.invoice.servicesCustomer.forEach(serviceCustomer=>{
            selectServicesCustomer.append(putServiceCustomer(serviceCustomer))
          })
        }
        break
        case 404:
        console.log("Error 404: No existe esa autoinvoice")
        break
        case 500:
        console.log("Error 500: Al recibir los parametros")
        break
      }
    },
    error: (request, msg, error) => {
        console.log(error)
    }
  })
}

function addExtraCharge(descriptionText, descriptionId, code, cost, variant, extras){
  extraCharges.push({
    index: lengthIndex,
    serviceId: 0,
    status: 1,
    descriptionService: descriptionText,
    descriptionServiceId: descriptionId,
    codeService: code,
    costService: cost,
    variantService: variant,
    extraInformationService:extras
  })
  lengthEC++
}

function removeExtraCharge(serviceId, indexCharge=null){
  var isSaved   = false
  extraCharges.forEach((charge, index, array)=>{
    if(charge.serviceId == 0 && serviceId == 0 && charge.status == 1 && charge.index == indexCharge){
      array.splice(index, 1)
      isSaved = true
      lengthEC--
      return
    }else if(charge.serviceId != 0 && serviceId != 0 && charge.serviceId == serviceId && charge.status == 0){
      isSaved = true
      return
    }
  })
  if(!isSaved){
    extraCharges.push({
      serviceId: serviceId,
      status: 0
    })
    lengthEC--
  }
}

function addReceivingDetail(id, status, value){
  var isSaved = false
  receivingsDetails.forEach((receivingDetail, index, array)=>{
    if(receivingDetail.id == id){
      array[index].status = (array[index].status == status) ? status : 3
      switch(status){
        case 1:
        array[index].days = value
        break
        case 2:
        array[index].unitprice = value
        break
      }
      isSaved = true
      return
    }
  })
  if(!isSaved){
    switch(status){
      case 1:
      receivingsDetails.push({id, status, days: value})
      break
      case 2:
      receivingsDetails.push({id, status, unitprice: value})
      break
    }
  }
}

function removeReceivingDetail(id, status){
  receivingsDetails.forEach((receivingDetail, index, array)=>{
    if(receivingDetail.id == id){
      switch(receivingDetail.status){
        case 1:
        if(status == 1){
          array.splice(index, 1)
        }
        break
        case 2:
        if(status == 2){
          array.splice(index, 1)
        }
        break
        case 3:
        switch(status){
          case 1:
          array[index].status = 2
          break
          case 2:
          array[index].status = 1
          break
        }
        break
      }
      return
    }
  })
}

function saveInvoice(){
  var bodyJson = {}
  statusAction = 1
  if(!document.querySelector("#invoiceForm").checkValidity()){
    showErrorNotify("Error: favor de llenar todos los campos correctamente")
    return
  }
  if(extraCharges.length != 0 || receivingsDetails.length != 0 || dateStartInvoice.localeCompare("") != 0 || dateEndInvoice.localeCompare("") != 0){
    showdialog(()=>{
      if(extraCharges.length > 0){
        bodyJson.extraCharges = extraCharges
      }
      if(receivingsDetails.length > 0){
        bodyJson.receivingsDetails = receivingsDetails
      }
      if(dateStartInvoice.localeCompare("") != 0){
        bodyJson.invoiceStartDate = dateStartInvoice.toString()
      }
      if(dateEndInvoice.localeCompare("") != 0){
        bodyJson.invoiceEndDate = dateEndInvoice.toString()
      }
      bodyJson.invoiceId = invoiceId
      $.ajax({
        url: `${serverURI}index.php/invoice/storage/save`,
        type: "PUT",
        dataType: "json",
        data:bodyJson,
        success: data =>{
          switch(data.code){
            case 200:
            shownotify(0)
            window.location.replace(`${serverURI}index.php/invoice/detail?invoiceId=${invoiceId}`)
            break
            case 400:
            showErrorNotify(data.message)
            break
            case 500:
            showErrorNotify("Error en el servidor, por favor recargue la pagina y si los problemas percisten avise al administrador.")
            break
          }
        },
        error: (request, msg, error) => {
            console.log(error)
        }
      })
    }, "Are you sure save?")
  }else{
    showErrorNotify("Error: It is not update, it is not found changes in the data invoice")
  }
}

function putReceivingDetail(receivingDetail){
  return `
  <div data-receiving-detail-id=${receivingDetail.receiving_detail_id} data-receiving-quantity=${receivingDetail.quantity} data-total-receiving=${receivingDetail.total}>
      <div class="column-1">
          <p>${receivingDetail.receiving_number}</p>
      </div>
      <div class="column-2">
          <p>${receivingDetail.createdDate}</p>
      </div>
      <div class="column-3">
          <p>${receivingDetail.freightBill}</p>
      </div>
      <div class="column-4">
          <p>${receivingDetail.fr}</p>
      </div>
      <div class="column-5">
          <p>${receivingDetail.tande}</p>
      </div>
      <div class="column-6">
          <p>${receivingDetail.quantity}</p>
      </div>
      <div class="column-7">
          <p>${receivingDetail.description}</p>
      </div>
      <div class="column-8">
          <input type="number" name="" min=0 required value=${receivingDetail.chargedDays} data-value=${receivingDetail.chargedDays} data-charged-days=0>
      </div>
      <div class="column-9">
          <p>$</p>
          <input type="number" step="0.01" min=0 required name="" value=${receivingDetail.unitPrice} data-value=${receivingDetail.unitPrice} data-unitprice=0>
      </div>
      <div class="column-10">
          <p>$</p>
          <span data-total-receiving=0>${money_format(receivingDetail.total)}</span>
      </div>
      <div class="column-11">
          <button class="button revert clear-search">Revert</button>
      </div>
  </div>`;
}

function putExtraChargue(extraChargue){
  return `
  <tr>
    <td class="delete-chargue" data-service-id=${extraChargue.service_storage_invoice_id} data-index=${extraChargue.index} data-total-extra=${extraChargue.costService}>
      <img src="${serverURI}public/assets/images/icons/delete.svg" alt="">
    </td>
    <td>${extraChargue.codeService}</td>
    <td>${extraChargue.descriptionService}</td>
    <td>$ <span>${money_format(extraChargue.costService)}</span></td>
  </tr>
  `
  $('.content-loading').addClass('loading-dialog');
  tableExtraChargues.removeClass('hide');
  $('.not-found').css({"display":"none"});
}

function putServiceCustomer(serviceCustomer){
  return `
  <option
  value="${serviceCustomer.service_id}"
  data-service-code="${serviceCustomer.codeService}"
  data-service-variant=${serviceCustomer.variantService}
  data-service-cost=${serviceCustomer.costService}>
    ${serviceCustomer.descriptionService}
  </option>
  `
}

function money_format(quantity_money){
  var quantity_final   =                                           ""
  var quantity_integer =                     parseInt(quantity_money)
  var quantity_decimal = quantity_money - quantity_integer
  var quantity_string  =                   quantity_integer.toString()
  var symbol = 0
  for( var c = quantity_string.length - 1; c >= 0; c-- ){
    if( symbol == 3 ){
      quantity_final += ","
      symbol = 0
    }
    symbol++
    quantity_final += quantity_string[c]
  }
  quantity_string = quantity_final
  quantity_final  = ""
  for( var c = quantity_string.length - 1; c >= 0; c-- ){
    quantity_final += quantity_string[c]
  }
  quantity_string = quantity_decimal.toFixed(2).toString().substring(1, 5)
  return `${quantity_final + quantity_string}`
}

function initialize(){
  var bodyElement = $('body')
  //Define global vars
  serverURI =                  bodyElement.data("uri")
  invoiceId = parseInt(bodyElement.data("invoice-id"))
  $('#btn-save-invoice').click(saveInvoice)
  $("#invoiceStartDate").change((e)=>{
    var dateInvoice = $(e.currentTarget)
    var EdateEndInvoice = $('#invoiceDate')
    var previousValue= dateInvoice.data("value")
    var currentValue = dateInvoice.val()
    var isValid = true
    if(("").localeCompare(currentValue) == 0){
      isValid = false
    }else{
      if(!validateAllDate(currentValue)){
        isValid = false
      }
    }
    if(!isValid){
      showErrorNotify("Las fechas deben tener el formato YYYY-MM-DD, y deben existir, favor de ingresar la fecha correctamente correctamente")
      dateStartInvoice = ""
      EdateEndInvoice.change()
      return
    }
    if(currentValue !== previousValue){
      if(statusDateInvoice){
        if(currentValue.localeCompare(EdateEndInvoice.val()) === -1 || currentValue.localeCompare(EdateEndInvoice.val()) === 0){
          dateStartInvoice = currentValue.toString()
        }else if(currentValue.localeCompare(EdateEndInvoice.val()) === 1){
          dateStartInvoice = ""
          dateInvoice.val(dateInvoice.data("value"))
          showErrorNotify("No puede existir una fecha de inicio mayor a la fecha final.")
        }
      }else{
        dateStartInvoice = currentValue.toString()
      }
    }else{
      dateStartInvoice = ""
    }
  })
  $("#invoiceDate").change((e)=>{
    var dateInvoice = $(e.currentTarget)
    var EdateStartInvoice = $('#invoiceStartDate')
    var previousValue= dateInvoice.data("value")
    var currentValue = dateInvoice.val()
    var isValid = true
    if(("").localeCompare(currentValue) == 0){
      isValid = false
    }else{
      if(!validateAllDate(currentValue)){
        isValid = false
      }
    }
    if(!isValid){
      showErrorNotify("Las fechas deben tener el formato YYYY-MM-DD, y deben existir, favor de ingresar la fecha correctamente correctamente")
      dateStartInvoice = ""
      EdateEndInvoice.change()
      return
    }
    if(currentValue !== previousValue){
      if(statusDateInvoice){
        if(currentValue.localeCompare(EdateStartInvoice.val()) === 1 || currentValue.localeCompare(EdateStartInvoice.val()) === 0){
          dateEndInvoice = currentValue.toString()
        }else if(currentValue.localeCompare(EdateStartInvoice.val()) === -1){
          dateEndInvoice = ""
          dateInvoice.val(dateInvoice.data("value"))
          showErrorNotify("No puede existir una fecha final menor a la fecha de inicio.")
        }
      }else{
        dateEndInvoice = currentValue.toString()
      }
    }else{
      dateEndInvoice = ""
    }
  })

  $('.open-extra-chargue').click(function(){
    var tableExtraChargues = $('[data-table-extra-chargues=0]')
    var extraChargesAdded = []
    openloadingcontent()
    $('.content-loading').addClass('loading-dialog');
    $('.not-found').css({"height":"240px"});
    tableExtraChargues.html("")
    selectServicesCustomer.val("0")
    $('#buttonAddCharge').unbind("click")
    $.ajax({
      url: `${serverURI}index.php/invoice/get/extra/storage?invoiceId=${invoiceId}`,
      type: "GET",
      dataType: "json",
      success: data=>{
        switch(data.code){
          case 200:
          data.response.charges = Array.from(data.response.charges).filter((charge)=>{
            serviceId = 0
            extraCharges.filter((chargeInF)=>{
              return (chargeInF.status == 0)
            }).forEach((chargeIn)=>{
              if(chargeIn.serviceId == charge.service_storage_invoice_id && chargeIn.status == 0){
                serviceId = charge.service_storage_invoice_id
              }
            })
            return charge.service_storage_invoice_id != serviceId
          })
          extraChargesAdded = extraCharges.filter((charge)=>{
            return (charge.status == 1)
          }).map((charge)=>{
            return {
              index: charge.index,
              service_storage_invoice_id: charge.serviceId,
              codeService: charge.codeService,
              descriptionService: charge.descriptionService,
              costService: charge.costService
            }
          })
          data.response.charges = data.response.charges.concat(extraChargesAdded)
          lengthEC = data.response.charges.length
          if(lengthEC == 0){
            notFoundCharges =  true;
            tableExtraChargues.addClass('hide');
            $('.not-found').css({"display":"flex"});
          }else{
            notFoundCharges = false
            tableExtraChargues.html(`
            <tr>
              <td>Delete</td>
              <td>Code</td>
              <td>Description</td>
              <td>Cost</td>
            </tr>`)
            tableExtraChargues.removeClass('hide');
            $('.not-found').css({"display":"none"});
            data.response.charges.forEach(charge=>{
              tableExtraChargues.append(putExtraChargue(charge, true))
            })
            $('.table').removeClass('hide');
          }
          break
          case 404:
              console.log("Error 404")
              var extraChargesAdded = extraCharges.filter((charge)=>{
                return (charge.status == 1)
              }).map((charge)=>{
                return {
                  index: charge.index,
                  service_storage_invoice_id: charge.serviceId,
                  codeService: charge.codeService,
                  descriptionService: charge.descriptionService,
                  costService: charge.costService
                }
              })
              lengthEC = extraChargesAdded.length
              if(lengthEC == 0){
                notFoundCharges =  true;
                tableExtraChargues.addClass('hide');
                $('.not-found').css({"display":"flex"});
              }else{
                notFoundCharges = false
                tableExtraChargues.html(`
                <tr>
                  <td>Delete</td>
                  <td>Code</td>
                  <td>Description</td>
                  <td>Cost</td>
                </tr>`)
                tableExtraChargues.removeClass('hide');
                $('.not-found').css({"display":"none"});
                extraChargesAdded.forEach(charge=>{
                  tableExtraChargues.append(putExtraChargue(charge, true))
                })
              }
          break
          case 500:
          console.log("Error 500")
          break
        }
      },
      error: (request, msg, error) => {
          console.log(error)
      }
    })
    .done(()=>{
      $('.delete-chargue > img').click((e)=>{
        var buttonDelete =                       $(e.currentTarget)
        var serviceId    = buttonDelete.parent().data("service-id")
        var indexCharge  = 0
        var totalExtra  = buttonDelete.parent().data("total-extra")
        buttonDelete.parent().parent().remove()
        totalServices -= totalExtra
        totalInvoice -= totalExtra
        $('[data-info-autoinvoice-subtotal=0]').html(`$ ${money_format(totalInvoice)}`)
        $('[data-info-autoinvoice-total=0]').html(`$ ${money_format(totalInvoice)}`)
        $('[data-total-services=0]').html(money_format(totalServices))
        if(serviceId == 0){
          indexCharge = buttonDelete.parent().data("index")
          removeExtraCharge(0, indexCharge)
        }else{
          removeExtraCharge(serviceId, 0)
        }
        if(lengthEC == 0){
          openloadingcontent()
          notFoundCharges =  true;
          tableExtraChargues.addClass('hide');
          $('.not-found').css({"display":"flex"});
        }
      })
    })
    if(ec == 0){
      $('.box-shadow').css({"display":"block"});
      $('.extra-chargues').css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
      ec = 1;
    }
    else{
      $('.box-shadow').css({"display":"none"});
      $('.extra-chargues').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
      ec = 0;
    }
    $('#buttonAddCharge').bind("click", ()=>{
      $('.delete-chargue > img').unbind("click")
      var txtCost = $('#otherCost')
      var txtInformation = $('#extraInformation')
      var txtServiceQty  = $('#serviceQty')
      var registerExtraCharge = {}
      var tableExtraChargues = $('[data-table-extra-chargues=0]')
      var optionSelected = selectServicesCustomer.find(":selected")

      var valueCost = parseFloat(txtCost.val())
      var valueQty = parseFloat(txtServiceQty.val())
      var validCost = true
      var validQty = true

      if(parseInt(optionSelected.data("service-variant")) != 0){
        if(isNaN(valueCost)){
          validCost = false
        }else if(valueCost < 0){
          validCost = false
        }
      }

      if(isNaN(valueQty)){
        validQty = false
      }else if(valueQty < 0){
        validQty = false
      }else if(!validatePositiveFloat(valueCost)){
        validCost = false
      }else if(!validatePositiveInteger(valueQty)){
        validQty = false
      }

      if(!validCost){
        showErrorNotify("El costo debe ser numerico y mayor o igual a 0, ingrese los datos correctamente")
        return
      }
      if(!validQty){
        showErrorNotify("La cantidad debe ser numerica con o sin punto decimal mayor a 0, ingrese los datos correctamente")
        return
      }

      if(notFoundCharges)
      {
          tableExtraChargues.html(`
          <tr>
            <td>Delete</td>
            <td>Code</td>
            <td>Description</td>
            <td>Cost</td>
          </tr>`)
          openloadingcontent()
          tableExtraChargues.removeClass('hide')
          $('.not-found').css({"display":"none"});
          notFoundCharges = false
      }
      for(var i = 0; i < parseInt(txtServiceQty.val()); i++){
        lengthIndex++
        registerExtraCharge.index = lengthIndex
        registerExtraCharge.service_storage_invoice_id = 0
        if(parseInt(optionSelected.data("service-variant")) != 0){
          if(valueCost === parseFloat(optionSelected.data("service-cost"))){
            registerExtraCharge.costService = valueCost
          }else{
            registerExtraCharge.costService = valueCost
          }
        }else{
          registerExtraCharge.costService = parseFloat(optionSelected.data("service-cost"))
        }
        registerExtraCharge.codeService = optionSelected.data("service-code")
        registerExtraCharge.descriptionService = optionSelected.html()
        addExtraCharge(registerExtraCharge.descriptionService, selectServicesCustomer.val(), registerExtraCharge.codeService, registerExtraCharge.costService, optionSelected.data("service-variant"), txtInformation.val())
        tableExtraChargues.append(putExtraChargue(registerExtraCharge))
        totalServices += registerExtraCharge.costService
        totalInvoice += registerExtraCharge.costService
      }
      $('[data-total-services=0]').html(money_format(totalServices))
      $('[data-info-autoinvoice-subtotal=0]').html(`$ ${money_format(totalInvoice)}`)
      $('[data-info-autoinvoice-total=0]').html(`$ ${money_format(totalInvoice)}`)
      $('.delete-chargue > img').bind("click", (e)=>{
        var buttonDelete =                       $(e.currentTarget)
        var indexCharge = buttonDelete.parent().data("index")
        var totalExtra  = buttonDelete.parent().data("total-extra")
        var serviceId    = buttonDelete.parent().data("service-id")
        buttonDelete.parent().parent().remove()
        totalServices -= totalExtra
        totalInvoice -= totalExtra
        $('[data-info-autoinvoice-subtotal=0]').html(`$ ${money_format(totalInvoice)}`)
        $('[data-info-autoinvoice-total=0]').html(`$ ${money_format(totalInvoice)}`)
        $('[data-total-services=0]').html(money_format(totalServices))
        if(serviceId == 0){
          indexCharge = buttonDelete.parent().data("index")
          removeExtraCharge(0, indexCharge)
        }else{
          removeExtraCharge(serviceId, 0)
        }
        if(lengthEC == 0){
          openloadingcontent()
          notFoundCharges =  true;
          tableExtraChargues.addClass('hide');
          $('.not-found').css({"display":"flex"});
        }
      })
      selectServicesCustomer.val(0)
      txtCost.val(0)
      txtInformation.val("")
      txtServiceQty.val(1)
      $('.add_service').hide();
      $('.applied-charges').show();
    })
  })

  $('.add_service').hide();

  $('.service').change(function(){
    var optionSelected = $(this).find(":selected")
    $('[data-extra-code=0]').html(optionSelected.data("service-code"))
    $('[data-extra-description=0]').html(optionSelected.html())
    $('#extraInformation').val("")
    $('#serviceQty').val(1)
    $('#otherCost').val(0)
    if(parseInt(optionSelected.data("service-variant")) == 0){
      $('[data-variant-input=0]').hide()
    }else{
      $('[data-variant-input=0]').show()
      $('#otherCost').val(optionSelected.data("service-cost"))
    }
    if( (parseInt(optionSelected.val())) === 0 ){
      $('.add_service').hide();
      $('.applied-charges').show();
    }else{
      $('.add_service').show();
      $('.applied-charges').hide();
    }
  });

  $(".cancel-btn").click(function() {
    selectServicesCustomer.val(0)
    $('.add_service').hide();
    $('.applied-charges').show();
    $('#buttonAddCharge').unbind("click");
  });
  /***********************Close box-shadow y dialogos***********************/
  $('.box-shadow').click(function(){
    sdet = 0;
    det = 0;
    ec = 0;
    $('.add_service').hide();
    $('.applied-charges').show();
    $('.box-shadow').css({"display":"none"});
    $('.shipping-details').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
    $('.extra-chargues').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
    $('.receiving-details').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
    $('#buttonAddCharge').unbind("click");
  });
  $('.close-dialog-shadow').click(function(){
      sdet = 0;
      det = 0;
      ec = 0;
      $('.add_service').hide();
      $('.applied-charges').show();
      $('.box-shadow').css({"display":"none"});
      $('.shipping-details').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
      $('.extra-chargues').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
      $('.receiving-details').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
      $('#buttonAddCharge').unbind("click");
  });

  /******************** Days Storage Change ***********************/
  $('body').on('change', '[data-charged-days=0]', (e)=>{
    var newTotalReceiving = 0
    var daysElement = $(e.currentTarget)
    var row         = daysElement.parent().parent()
    var totalReceiving = parseFloat(row.data('total-receiving'))
    var unitPrice = parseFloat(row.find('[data-unitprice=0]').val())
    var totalReceivingElement = row.find('[data-total-receiving=0]')
    var quantity = parseInt(row.data("receiving-quantity"))
    var receivingDetailId = row.data("receiving-detail-id")
    var previousValue = parseInt(daysElement.data("value"))
    var currentValue = parseFloat(daysElement.val())
    var isValidDays = true
    var isValidUnitPrice = true

    if(isNaN(currentValue)){
      isValidDays = false
    }else if(currentValue < 0){
      isValidDays = false
    }else if(isNaN(unitPrice)){
      isValidUnitPrice = false
    }else if(unitPrice < 0){
      isValidUnitPrice = false
    }else if(!validatePositiveInteger(currentValue)){
      isValidDays = false
    }else if(!validatePositiveFloat(unitPrice)){
      isValidUnitPrice = false
    }

    if(!isValidDays){
      showErrorNotify("Error: el dato de dias de almacenaje debe de ser numerico y debe ser 0 o mayor.")
      removeReceivingDetail(receivingDetailId, 1)
      totalReceivingElement.html(money_format(0))
      row.data('total-receiving', 0)
      totalInvoice -= totalReceiving
      $('[data-info-autoinvoice-subtotal]').html(`$ ${money_format(totalInvoice)}`)
      $('[data-info-autoinvoice-total]').html(`$ ${money_format(totalInvoice)}`)
      return
    }
    if(!isValidUnitPrice){
      showErrorNotify("Error: el dato de precio por unidad debe de ser numerico y debe ser 0 o mayor.")
      removeReceivingDetail(receivingDetailId, 2)
      unitPrice = 0
      totalReceivingElement.html(money_format(0))
      row.data('total-receiving', 0)
      totalInvoice -= totalReceiving
      $('[data-info-autoinvoice-subtotal]').html(`$ ${money_format(totalInvoice)}`)
      $('[data-info-autoinvoice-total]').html(`$ ${money_format(totalInvoice)}`)
    }

    if(previousValue !== currentValue){
      addReceivingDetail(receivingDetailId, 1, currentValue)
    }else{
      removeReceivingDetail(receivingDetailId, 1)
    }
    newTotalReceiving = quantity * currentValue * unitPrice
    if(newTotalReceiving != totalReceiving){
      totalReceivingElement.html(`${money_format(newTotalReceiving)}`)
      row.data('total-receiving', newTotalReceiving)
      newTotalReceiving -= totalReceiving
      totalInvoice += newTotalReceiving
      $('[data-info-autoinvoice-subtotal]').html(`$ ${money_format(totalInvoice)}`)
      $('[data-info-autoinvoice-total]').html(`$ ${money_format(totalInvoice)}`)
    }
  })
  /******************** Unit Price Change ***********************/
  $('body').on('change', '[data-unitprice=0]',(e)=>{
    var newTotalReceiving = 0
    var unitPriceElement = $(e.currentTarget)
    var row         = unitPriceElement.parent().parent()
    var totalReceiving = parseFloat(row.data('total-receiving'))
    var days = parseFloat(row.find('[data-charged-days=0]').val())
    var totalReceivingElement = row.find('[data-total-receiving=0]')
    var quantity = parseInt(row.data("receiving-quantity"))
    var receivingDetailId = row.data("receiving-detail-id")
    var previousValue = parseFloat(unitPriceElement.data("value"))
    var currentValue = parseFloat(unitPriceElement.val())
    var isValidDays =true
    var isValidUnitPrice = true

    if(isNaN(currentValue)){
      isValidUnitPrice = false
    }else if(currentValue < 0){
      isValidUnitPrice = false
    }else if(isNaN(days)){
      isValidDays = false
    }else if(days < 0){
      isValidDays = false
    }else if(!validatePositiveFloat(currentValue)){
      isValidUnitPrice = false
    }else if(!validatePositiveInteger(days)){
      isValidDays = false
    }

    if(!isValidUnitPrice){
      showErrorNotify("Error: el dato de precio por unidad debe de ser numerico y debe ser 0 o mayor.")
      removeReceivingDetail(receivingDetailId, 2)
      totalReceivingElement.html(money_format(0))
      row.data('total-receiving', 0)
      totalInvoice -= totalReceiving
      $('[data-info-autoinvoice-subtotal]').html(`$ ${money_format(totalInvoice)}`)
      $('[data-info-autoinvoice-total]').html(`$ ${money_format(totalInvoice)}`)
      return
    }
    if(!isValidDays){
      showErrorNotify("Error: el dato de dias de almacenaje debe de ser numerico y debe ser 0 o mayor.")
      removeReceivingDetail(receivingDetailId, 1)
      days = 0
      totalReceivingElement.html(money_format(0))
      row.data('total-receiving', 0)
      totalInvoice -= totalReceiving
      $('[data-info-autoinvoice-subtotal]').html(`$ ${money_format(totalInvoice)}`)
      $('[data-info-autoinvoice-total]').html(`$ ${money_format(totalInvoice)}`)
    }

    if(previousValue !== currentValue){
      addReceivingDetail(receivingDetailId, 2, currentValue)
    }else{
      removeReceivingDetail(receivingDetailId, 2)
    }
    newTotalReceiving = quantity * currentValue * days
    if(newTotalReceiving != totalReceiving){
      totalReceivingElement.html(money_format(newTotalReceiving))
      row.data('total-receiving', newTotalReceiving)
      newTotalReceiving -= totalReceiving
      totalInvoice += newTotalReceiving
      $('[data-info-autoinvoice-subtotal]').html(`$ ${money_format(totalInvoice)}`)
      $('[data-info-autoinvoice-total]').html(`$ ${money_format(totalInvoice)}`)
    }
  })
  $('body').on('click', '.revert', (e)=>{
    var newTotalReceiving = 0
    var revertButton = $(e.currentTarget)
    var row         = revertButton.parent().parent()
    row.find('[data-unitprice=0]').val(row.find('[data-unitprice=0]').data('value'))
    row.find('[data-charged-days=0]').val(row.find('[data-charged-days=0]').data('value'))
    var totalReceiving = parseFloat(row.data('total-receiving'))
    var unitPrice = parseFloat(row.find('[data-unitprice=0]').val())
    var days = parseFloat(row.find('[data-charged-days=0]').val())
    var totalReceivingElement = row.find('[data-total-receiving=0]')
    var quantity = parseInt(row.data("receiving-quantity"))
    var receivingDetailId = row.data("receiving-detail-id")
    removeReceivingDetail(receivingDetailId, 1)
    removeReceivingDetail(receivingDetailId, 2)
    newTotalReceiving = quantity * unitPrice * days
    if(newTotalReceiving != totalReceiving){
      totalReceivingElement.html(money_format(newTotalReceiving))
      row.data('total-receiving', newTotalReceiving)
      newTotalReceiving -= totalReceiving
      totalInvoice += newTotalReceiving
      $('[data-info-autoinvoice-subtotal]').html(`$ ${money_format(totalInvoice)}`)
      $('[data-info-autoinvoice-total]').html(`$ ${money_format(totalInvoice)}`)
    }
  })
  $('.pop-msj span').html("")
  $('.close-pop-msj').click(function(){
      quitMessageAlert()
  });
  $('.confirm-pop').click(function(){
      quitMessageAlert()
  })
  $('#backButton').click(()=>{
    statusAction = 1
    if(extraCharges.length != 0 || receivingsDetails.length != 0 || dateStartInvoice.localeCompare("") != 0 || dateEndInvoice.localeCompare("") != 0){
      showdialog(()=>{
        location.href = `${serverURI}index.php/invoice`
      }, "Are you sure exit without save?")
    }else{
      location.href = `${serverURI}index.php/invoice`
    }
  })
}

function showdialog(callback, message){
    $('.message-dialog-not').html(message);

    $('.box-confirmation-not').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"pop-in", "animation-duration":"0.25s"});
    $('.shadow-notify').css({"display":"block"});


    $('.yes-confirmation').bind("click", function(){
        callback();
        $('.box-confirmation-not').css({"display":"none"});
        $('.shadow-notify').css({"display":"none"});
        $('.yes-confirmation').unbind("click");
    });
    $('.no-confirmation').click(function(){
        $('.box-confirmation-not').css({"display":"none"});
        $('.shadow-notify').css({"display":"none"});
        $('.yes-confirmation').unbind("click");
        statusAction = 0
    });
}
function shownotify(type, time = 3000){
    selectnotify(type, "notify-in");
    setTimeout(function(){
        selectnotify(type, "notify-out");
        setTimeout(function(){
            selectnotify(type, "notify-in", false);
            selectnotify(type, "notify-out", false);
        }, 370);
    }, time);
}
function selectnotify(type, className, isAdd=true){
    switch (type) {
        case 0:
            if(isAdd){
                $('.save-not').addClass(className);
            }else{
                $('.save-not').removeClass(className);
            }
            break;
        case 1:
            if(isAdd){
                $('.delete-not').addClass(className);
            }else{
                $('.delete-not').removeClass(className);
            }
            break;
        case 2:
            if(isAdd){
                $('.edit-not').addClass(className);
            }else{
                $('.edit-not').removeClass(className);
            }
            break;
        default:
        break;
    }
}

function showErrorNotify(message){
  $('.pop-msj span').html(message);
  $('.shadow-notify').toggleClass('visible')
  $('.pop-msj').css({"animation":"notify-in","animation-duration":"0.25s", "display":"block"});
}

function quitMessageAlert(){
    $('.shadow-notify').toggleClass('visible')
    $('.pop-msj').css({"animation":"pop-out","animation-duration":"0.25s", "display":"block"});
    setTimeout(function(){
        $('.pop-msj').css({"display":"none"});
    },220);
}

function validateDate(data){
  var regExp = /^[0-9]{4}\-[0-1]?[0-9]\-[0-3]?[0-9]$/
  return regExp.test(data)
}

function validatePositiveInteger(data){
  var regExp = /^[0-9]+$/
  return regExp.test(data)
}

function validatePositiveFloat(data){
  var regExp = /^[0-9]+(\.[0-9]+)?$/
  return regExp.test(data)
}

function validateAllDate(data){
  var isValid = true
  if(!validateDate(data)){
    isValid = false
  }else{
    var partDate = data.split("-")
    var year = parseInt(partDate[0])
    var month = parseInt(partDate[1]) - 1
    var day = parseInt(partDate[2])
    var dateObject = new Date(year, month, day)
    if(!((day==dateObject.getDate()) && (month==dateObject.getMonth()) && (year==dateObject.getFullYear()))){
      isValid = false
    }
  }
  return isValid
}

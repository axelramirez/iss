var ec        =  0;
var det       =  0;
var sdet      =  0;
var invoiceId =  0;
var serverURI = "";
var lengthEC  =  0;
var lengthIndex = -1;
var dateStartInvoice = "";
var dateEndInvoice = "";
var statusDateInvoice = false;
var notFoundCharges = false;
var selectServicesCustomer =  $('#selectServicesCustomer');
var totalInvoice  = 0;
var creditTotal = 0;
var statusAction = 0;
//Variables to Update Invoice
var shippings    = [];
var extraCharges = [];

//Main Function
openloading();
(()=>{
  initialize()
  $('#btn-save-invoice').click(saveInvoice)
  document.querySelector("#invoiceForm").addEventListener("submit", (e)=>{
    e.preventDefault()
  })
  window.addEventListener('beforeunload', (e)=>{
    if((extraCharges.length != 0 || shippings.length != 0 || dateStartInvoice.localeCompare("") != 0 || dateEndInvoice.localeCompare("") != 0) && statusAction == 0){
      e.returnValue = "Cambios detectados, estas seguro que deseas salir?";
      return "Cambios detectados, estas seguro que deseas salir?";
    }
    e.preventDefault();
  })
  getInvoice()
})()

function saveInvoice(){
  var bodyJson = {}
  statusAction = 1
  if(!document.querySelector("#invoiceForm").checkValidity()){
    showErrorNotify("Error: favor de llenar todos los campos correctamente")
    return
  }
  if(extraCharges.length != 0 || shippings.length != 0 || dateStartInvoice.localeCompare("") != 0 || dateEndInvoice.localeCompare("") != 0){
    showdialog(()=>{
      if(extraCharges.length > 0){
        bodyJson.extraCharges = extraCharges
      }
      if(shippings.length > 0){
        bodyJson.shippings = shippings
      }
      if(dateStartInvoice.localeCompare("") != 0){
        bodyJson.invoiceStartDate = dateStartInvoice.toString()
      }
      if(dateEndInvoice.localeCompare("") != 0){
        bodyJson.invoiceEndDate = dateEndInvoice.toString()
      }
      bodyJson.invoiceId = invoiceId
      $.ajax({
        url: `${serverURI}index.php/invoice/p_and_p/save`,
        type: "PUT",
        dataType: "json",
        data:bodyJson,
        success: data =>{
          switch(data.code){
            case 200:
            shownotify(0)
            window.location.replace(`${serverURI}index.php/invoice/detail?invoiceId=${invoiceId}`)
            break
            case 400:
            showErrorNotify(data.message)
            break
            case 500:
            showErrorNotify("Error en el servidor, por favor recargue la pagina y si los problemas percisten avise al administrador.")
            break
          }
        },
        error: (request, msg, error) => {
            console.log(error)
        }
      })
    }, "Are you sure save?")
  }else{
    showErrorNotify("Error: It is not update, it is not found changes in the data invoice")
  }
}

function addShippingDetail(id, status, value){
  var isSaved = false
  var isListSaved = false
  shippings.forEach((shipping, index, array)=>{
    if(shipping.id == id){
      switch(status){
        case 1:
        array[index].status = (array[index].status == status) ? status : 3
        addShippingInvoiceList(index, value.id, value.status, value.type, value.days, value.unitprice)
        break
        case 2:
        array[index].status = (array[index].status == status) ? status : 3
        array[index].credit = value
        break
      }
      isSaved = true
    }
  })
  if(!isSaved){
    switch(status){
      case 1:
      shippings.push({id, status, invoicelists: [{
        id: value.id,
        status: value.status,
        type: value.type,
        days: value.days,
        unitprice: value.unitprice
      }]})
      break
      case 2:
      shippings.push({id, status, invoicelists: [], credit:value})
      break
    }
  }
}
function addShippingInvoiceList(index, id, status, type, days, unitprice){
  var isSaved = false
  shippings[index].invoicelists.forEach((invoicelist, index, array)=>{
    if(invoicelist.id == id && invoicelist.type == type){
      isSaved = true
      array[index].status = (array[index].status == status) ? status : 3
      switch(status){
        case 1:
        array[index].days = days
        break
        case 2:
        array[index].unitprice = unitprice
        break
      }
      return
    }
  })
  if(!isSaved){
    shippings[index].invoicelists.push({id, type, status, days, unitprice})
  }
}
function removeShippingInvoiceList(index, id, status, type){
  var isSaved = false
  shippings[index].invoicelists.forEach((invoicelist, index, array)=>{
    if(invoicelist.id == id && invoicelist.type == type){
      switch(status){
        case 1:
        if(array[index].status === status){
          array.splice(index, 1)
        }else if(array[index].status == 3){
          array[index].status = 2
        }
        break
        case 2:
        if(array[index].status === status){
          array.splice(index, 1)
        }else if(array[index].status == 3){
          array[index].status = 1
        }
        break
      }
      return
    }
  })
  if(shippings[index].invoicelists.length == 0 && shippings[index].status == 1){
    shippings.splice(index, 1)
  }else if(shippings[index].invoicelists.length == 0 && shippings[index].status == 3){
    shippings[index].status = 2
  }
}
function removeShippingDetail(id, status, value){
  shippings.forEach((shipping, index, array)=>{
    if(shipping.id == id){
      switch(status){
        case 1:
        if(status == array[index].status){
          removeShippingInvoiceList(index, value.id, value.status, value.type)
        }else if(array[index].status == 3){
          removeShippingInvoiceList(index, value.id, value.status, value.type)
        }
        break
        case 2:
        if(status == array[index].status){
          array.splice(index, 1)
        }else if(array[index].status == 3){
          array[index].status = 1
        }
        break
      }
      isSaved = true
    }
  })
}
function getInvoice(){
  $.ajax({
    url: `${serverURI}index.php/invoice/p_and_p/edit/get`,
    type: "GET",
    dataType: 'json',
    data: {
      invoiceId
    },
    success: data=>{
      switch(data.code){
        case 200:
        var shippingTable          =  $('[data-table-shipping=0]')
        var receivingTable         = $('[data-table-receiving=0]')
        totalInvoice = parseFloat(data.response.invoice.totalInvoice)
        creditTotal  = parseFloat(data.response.invoice.creditInvoice)
        $('[data-info-autoinvoice-id=0]').html(data.response.invoice.autoinvoice_number)
        $('[data-info-autoinvoice-customer=0]').html(data.response.invoice.name)
        if(data.response.invoice.dateInvoice == data.response.invoice.dateStartInvoice){
          $('#invoiceDate').parent().hide()
          $('#invoiceStartDate').prev().html("Date")
          statusDateInvoice = false
        }else{
          statusDateInvoice = true
        }
        $('#invoiceDate').val(data.response.invoice.dateInvoice)
        $('#invoiceStartDate').val(data.response.invoice.dateStartInvoice)
        $('#invoiceDate').data("value", data.response.invoice.dateInvoice)
        $('#invoiceStartDate').data("value", data.response.invoice.dateStartInvoice)
        data.response.shippingsInvoice.forEach(shippingInvoice=>{
          shippingTable.append(putShippingInvoice(shippingInvoice, data.response.invoice.haveServicesCustomer))
        })
        $('[data-info-autoinvoice-subtotal=0]').html(money_format(totalInvoice))
        $('[data-info-autoinvoice-credit=0]').html(money_format(creditTotal))
        $('[data-info-autoinvoice-total=0]').html(money_format(totalInvoice - creditTotal))
        if(data.response.invoice.haveServicesCustomer){
          data.response.invoice.servicesCustomer.forEach(serviceCustomer=>{
            selectServicesCustomer.append(putServiceCustomer(serviceCustomer))
          })
        }
        break
        case 404:
        console.log("Error 404: No existe esa autoinvoice")
        break
        case 500:
        console.log("Error 500: Al recibir los parametros")
        break
      }
    },
    error: (request, msg, error) => {
        console.log(error)
    }
  })
  .done(()=>{
    /******************** Days Storage Change ***********************/
    $('[data-daysstorage=0]').change((e)=>{
      var daysElement = $(e.currentTarget)
      var row         = daysElement.parents('.data-shipping-detail')

      var newTotalShipping        = 0
      var newTotalInvoiceList     = 0
      var totalShippingDialog     = row.parents('.box-data-details').siblings('.shipping-detail-info').find('[data-total=0]')
      var register                = row.parents('.content-data-box')
      var totalShipping           = parseFloat(register.data('total'))
      var totalShippingElement    = register.find("[data-register-total=0]")
      var totalInvoiceList        = parseFloat(row.data('total'))
      var quantity                = parseInt(row.data('quantity'))
      var totalInvoiceListElement = row.find("[data-invoicelist-total=0]")
      var unitPrice               = parseFloat(row.find('[data-unitprice=0]').val())

      var invoiceListId = parseInt(row.data("invoicelist-id"))
      var invoiceListType = row.data("invoicelist-type")
      var shippingId = parseInt(register.data("shipping-id"))
      var previousValue = parseFloat(daysElement.data("value"))
      var currentValue = parseFloat(daysElement.val())
      var isValidDays = true
      var isValidUnitPrice = true

      if(isNaN(currentValue)){
        isValidDays = false
      }else if(currentValue < 0){
        isValidDays = false
      }else if(isNaN(unitPrice)){
        isValidUnitPrice = false
      }else if(unitPrice < 0){
        isValidUnitPrice = false
      }else if(!validatePositiveInteger(currentValue)){
        isValidDays = false
      }else if(!validatePositiveFloat(unitPrice)){
        isValidUnitPrice = false
      }

      if(!isValidDays){
        showErrorNotify("Error: el dato de dias de almacenaje debe de ser numerico y debe ser 0 o mayor.")
        removeShippingDetail(shippingId, 1, {
          id: invoiceListId,
          status: 1,
          type: invoiceListType
        })
        totalInvoiceListElement.html(money_format(0))
        row.data('total', 0)
        newTotalShipping    = totalShipping - totalInvoiceList
        totalInvoice -= totalInvoiceList
        register.data('total', newTotalShipping)
        totalShippingElement.html(money_format(newTotalShipping))
        totalShippingDialog.html(money_format(newTotalShipping))
        $('[data-info-autoinvoice-subtotal]').html(money_format(totalInvoice))
        $('[data-info-autoinvoice-total]').html(money_format(totalInvoice - creditTotal))
        return
      }
      if(!isValidUnitPrice){
        showErrorNotify("Error: el dato de precio por unidad debe de ser numerico y debe ser 0 o mayor.")
        removeShippingDetail(shippingId, 1, {
          id: invoiceListId,
          status: 2,
          type: invoiceListType
        })
        unitPrice = 0
        totalInvoiceListElement.html(money_format(0))
        row.data('total', 0)
        newTotalShipping    = totalShipping - totalInvoiceList
        totalInvoice -= totalInvoiceList
        register.data('total', newTotalShipping)
        totalShippingElement.html(money_format(newTotalShipping))
        totalShippingDialog.html(money_format(newTotalShipping))
        $('[data-info-autoinvoice-subtotal]').html(money_format(totalInvoice))
        $('[data-info-autoinvoice-total]').html(money_format(totalInvoice - creditTotal))
      }

      if(previousValue !== currentValue){
        addShippingDetail(shippingId, 1, {
          id: invoiceListId,
          status: 1,
          type: invoiceListType,
          days: currentValue,
          unitprice: 0
        })
      }else{
        removeShippingDetail(shippingId, 1, {
          id: invoiceListId,
          status: 1,
          type: invoiceListType
        })
      }

      newTotalInvoiceList         = quantity * unitPrice * currentValue
      if(newTotalInvoiceList != totalInvoiceList){
        totalInvoiceListElement.html(money_format(newTotalInvoiceList))
        row.data('total', newTotalInvoiceList)
        newTotalShipping    = totalShipping - totalInvoiceList
        newTotalShipping += newTotalInvoiceList
        newTotalInvoiceList -= totalInvoiceList
        totalInvoice += newTotalInvoiceList
        register.data('total', newTotalShipping)
        totalShippingElement.html(money_format(newTotalShipping))
        totalShippingDialog.html(money_format(newTotalShipping))
        $('[data-info-autoinvoice-subtotal]').html(money_format(totalInvoice))
        $('[data-info-autoinvoice-total]').html(money_format(totalInvoice - creditTotal))
      }

    })
    /******************** Unit Price Change ***********************/
    $('[data-unitprice=0]').change((e)=>{
      var unitPriceElement = $(e.currentTarget)
      var row         = unitPriceElement.parents('.data-shipping-detail')

      var newTotalShipping        = 0
      var newTotalInvoiceList     = 0
      var totalShippingDialog     = row.parents('.box-data-details').siblings('.shipping-detail-info').find('[data-total=0]')
      var register                = row.parents('.content-data-box')
      var totalShipping           = parseFloat(register.data('total'))
      var totalShippingElement    = register.find("[data-register-total=0]")
      var totalInvoiceList        = parseFloat(row.data('total'))
      var quantity                = parseInt(row.data('quantity'))
      var totalInvoiceListElement = row.find("[data-invoicelist-total=0]")
      var days                    = parseFloat(row.find('[data-daysstorage=0]').val())

      var invoiceListId = row.data("invoicelist-id")
      var invoiceListType = row.data("invoicelist-type")
      var shippingId = parseInt(register.data("shipping-id"))
      var previousValue = parseFloat(unitPriceElement.data("value"))
      var currentValue = parseFloat(unitPriceElement.val())
      var isValidDays =true
      var isValidUnitPrice = true

      if(invoiceListType.localeCompare("Handling") == 0){
        if(isNaN(currentValue)){
          isValidUnitPrice = false
        }else if(currentValue < 0){
          isValidUnitPrice = false
        }else if(!validatePositiveFloat(currentValue)){
          isValidUnitPrice = false
        }
      }else{
        if(isNaN(currentValue)){
          isValidUnitPrice = false
        }else if(currentValue < 0){
          isValidUnitPrice = false
        }else if(isNaN(days)){
          isValidDays = false
        }else if(days < 0){
          isValidDays = false
        }else if(!validatePositiveFloat(currentValue)){
          isValidUnitPrice = false
        }else if(!validatePositiveInteger(days)){
          isValidDays = false
        }
      }

      if(!isValidUnitPrice){
        showErrorNotify("Error: el dato de precio por unidad debe de ser numerico y debe ser 0 o mayor.")
        removeShippingDetail(shippingId, 1, {
          id: invoiceListId,
          status: 2,
          type: invoiceListType
        })
        totalInvoiceListElement.html(money_format(0))
        row.data('total', 0)
        newTotalShipping    = totalShipping - totalInvoiceList
        totalInvoice -= totalInvoiceList
        register.data('total', newTotalShipping)
        totalShippingElement.html(money_format(newTotalShipping))
        totalShippingDialog.html(money_format(newTotalShipping))
        $('[data-info-autoinvoice-subtotal]').html(money_format(totalInvoice))
        $('[data-info-autoinvoice-total]').html(money_format(totalInvoice - creditTotal))
        return
      }
      if(!isValidDays){
        showErrorNotify("Error: el dato de dias de almacenaje debe de ser numerico y debe ser 0 o mayor.")
        removeShippingDetail(shippingId, 1, {
          id: invoiceListId,
          status: 1,
          type: invoiceListType
        })
        days = 0
        totalInvoiceListElement.html(money_format(0))
        row.data('total', 0)
        newTotalShipping    = totalShipping - totalInvoiceList
        totalInvoice -= totalInvoiceList
        register.data('total', newTotalShipping)
        totalShippingElement.html(money_format(newTotalShipping))
        totalShippingDialog.html(money_format(newTotalShipping))
        $('[data-info-autoinvoice-subtotal]').html(money_format(totalInvoice))
        $('[data-info-autoinvoice-total]').html(money_format(totalInvoice - creditTotal))
      }

      if(previousValue !== currentValue){
        addShippingDetail(shippingId, 1, {
          id: invoiceListId,
          status: 2,
          type: invoiceListType,
          days: 0,
          unitprice: currentValue
        })
      }else{
        removeShippingDetail(shippingId, 1, {
          id: invoiceListId,
          status: 2,
          type: invoiceListType
        })
      }
      if(invoiceListType.localeCompare("Handling") == 0){
        newTotalInvoiceList         = quantity * currentValue
      }else{
        newTotalInvoiceList         = quantity * days * currentValue
      }
      if(newTotalInvoiceList != totalInvoiceList){
        totalInvoiceListElement.html(money_format(newTotalInvoiceList))
        row.data('total', newTotalInvoiceList)
        newTotalShipping    = totalShipping - totalInvoiceList
        newTotalShipping += newTotalInvoiceList
        newTotalInvoiceList -= totalInvoiceList
        totalInvoice += newTotalInvoiceList
        register.data('total', newTotalShipping)
        totalShippingElement.html(money_format(newTotalShipping))
        totalShippingDialog.html(money_format(newTotalShipping))
        $('[data-info-autoinvoice-subtotal]').html(money_format(totalInvoice))
        $('[data-info-autoinvoice-total]').html(money_format(totalInvoice - creditTotal))
      }

    })
    /******************** Credit Change ***********************/
    $('[data-credit=0]').change((e)=>{
      var creditElement = $(e.currentTarget)
      var row         = creditElement.parent().parent()
      var shippingId = row.parent().parent().parent().parent().data("shipping-id")
      var previousValue = parseFloat(creditElement.data("value"))
      var currentValue = parseFloat(creditElement.val())
      var lastValue = parseFloat(creditElement.data('last-value'))
      var isValid = true

      if(isNaN(currentValue)){
        isValid = false
      }else if(currentValue < 0){
        isValid = false
      }
      else if(!validatePositiveFloat(currentValue)){
        isValid = false
      }

      if(!isValid){
        showErrorNotify("Error: el dato del credito debe de ser numerico y debe ser 0 o mayor.")
        removeShippingDetail(shippingId, 2, 0)
        creditTotal -= lastValue
        creditElement.data('last-value', 0)
        $('[data-info-autoinvoice-subtotal]').html(money_format(totalInvoice))
        $('[data-info-autoinvoice-credit]').html(money_format(creditTotal))
        $('[data-info-autoinvoice-total]').html(money_format(totalInvoice - creditTotal))
        return
      }

      if(previousValue !== currentValue){
        addShippingDetail(shippingId, 2, currentValue)
      }else{
        removeShippingDetail(shippingId, 2, 0)
      }

      creditTotal -= lastValue
      creditTotal += currentValue
      if((totalInvoice - creditTotal) >= 0){
        creditElement.data('last-value', currentValue)
        $('[data-info-autoinvoice-credit]').html(money_format(creditTotal))
        $('[data-info-autoinvoice-total]').html(money_format(totalInvoice - creditTotal))
      }else{
        creditTotal += lastValue
        creditTotal -= currentValue
      }
    })
    /***********Tool tip extra chargues********/
    $(".button-extra-chargue").hover(function(){
        $(this).find('.tooltip').css({"display":"block"});
    });
    $(".button-extra-chargue").mouseleave(function(){
        $(this).find('.tooltip').css({"display":"none"});
    });
    /******************Shipping Details**************************/
    $('.sDetails').click(function(){
      if(det == 0){
        $('.box-shadow').css({"display":"block"});
        $(this).next().css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
        sdet = 1;
      }
      else{
        $('.box-shadow').css({"display":"none"});
        $(this).next().css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
        sdet = 0;
      }
    });
    /**********************************extra cargos*************************/
    $('.button-extra-chargue').click(function(){
      $('#otherCost').val("0")
      $('#extraInformation').val("")
      $('#serviceQty').val("1")
      var registerSelected   = $(this).parents('.content-data-box')
      var registerType       =      registerSelected.data('type')
      var registerId         =                                  0
      var tableExtraChargues = $('[data-table-extra-chargues=0]')
      var totalRegister           = parseFloat(registerSelected.data('total'))
      var totalRegisterElement    = registerSelected.find("[data-register-total=0]")
      if(registerType.localeCompare("shipping") == 0){
        var totalShippingDialog     = registerSelected.find('.shipping-detail-info').find('[data-total=0]')
      }
      var extraChargesAdded = []
      openloadingcontent()
      $('.content-loading').addClass('loading-dialog');
      $('.not-found').css({"height":"240px"});
      tableExtraChargues.html("")
      selectServicesCustomer.val("0")
      $('#buttonAddCharge').unbind("click")
      registerId = registerSelected.data("shipping-id")
      $.ajax({
        url: `${serverURI}index.php/invoice/get/extra/shipping?invoiceId=${invoiceId}&shippingId=${registerId}`,
        type: "GET",
        dataType: "json",
        success: data=>{
          switch(data.code){
            case 200:
            data.response.charges = Array.from(data.response.charges).filter((charge)=>{
              serviceId = 0
              extraCharges.forEach((chargeIn)=>{
                if(registerId == charge.shipping_id && chargeIn.id == charge.shipping_id && chargeIn.type == "shipping" && chargeIn.serviceId == charge.service_shipping_id && chargeIn.status == 0){
                  serviceId = charge.service_shipping_id
                }
              })
              return charge.service_shipping_id != serviceId
            })
            var extraChargesAdded = extraCharges.filter((charge)=>{
              return (charge.type == "shipping" && charge.id == registerId && charge.status == 1)
            }).map((charge)=>{
              return {
                index: charge.index,
                service_shipping_id: charge.serviceId,
                codeService: charge.codeService,
                descriptionService: charge.descriptionService,
                costService: charge.costService
              }
            })
            data.response.charges = data.response.charges.concat(extraChargesAdded)
            lengthEC = data.response.charges.length
            if(lengthEC == 0){
              notFoundCharges =  true;
              tableExtraChargues.addClass('hide');
              $('.not-found').css({"display":"flex"});
            }else{
              notFoundCharges = false
              tableExtraChargues.html(`
              <tr>
                <td>Delete</td>
                <td>Code</td>
                <td>Description</td>
                <td>Cost</td>
              </tr>`)
              $('.table').removeClass('hide');
              tableExtraChargues.removeClass('hide');
              $('.not-found').css({"display":"none"});
              data.response.charges.forEach(charge=>{
                tableExtraChargues.append(putExtraChargue(charge, false))
              })
            }
            break
            case 404:
                console.log("Error 404")
                extraChargesAdded = extraCharges.filter((charge)=>{
                  return (charge.type == "shipping" && charge.id == registerId && charge.status == 1)
                }).map((charge)=>{
                  return {
                    index: charge.index,
                    service_shipping_id: charge.serviceId,
                    codeService: charge.codeService,
                    descriptionService: charge.descriptionService,
                    costService: charge.costService
                  }
                })
                lengthEC = extraChargesAdded.length
                if(lengthEC == 0){
                  notFoundCharges =  true;
                  tableExtraChargues.addClass('hide');
                  $('.not-found').css({"display":"flex"});
                }else{
                  notFoundCharges = false
                  tableExtraChargues.html(`
                  <tr>
                    <td>Delete</td>
                    <td>Code</td>
                    <td>Description</td>
                    <td>Cost</td>
                  </tr>`)
                  tableExtraChargues.removeClass('hide');
                  $('.not-found').css({"display":"none"});
                  extraChargesAdded.forEach(charge=>{
                    tableExtraChargues.append(putExtraChargue(charge, false))
                  })
                }
            break
            case 500:
            console.log("Error 500")
            break
          }
        },
        error: (request, msg, error) => {
            console.log(error)
        }
      })
      .done(()=>{
        $('.delete-chargue > img').click((e)=>{
          var indexCharge  = 0
          var buttonDelete =                       $(e.currentTarget)
          var contentButton = buttonDelete.parents('.delete-chargue')
          var register = contentButton.parents('tr')
          var serviceId    = parseInt(contentButton.data("service-id"))
          var totalExtra  = parseFloat(contentButton.data("total-extra"))
          var totalRegisterEC = parseFloat(registerSelected.data('total'))
          var totalRegisterElementEC = registerSelected.find('[data-register-total=0]')
          register.remove()
          totalInvoice -= totalExtra
          totalRegisterEC -= totalExtra
          totalRegisterElementEC.html(money_format(totalRegisterEC))
          totalRegisterElementEC.data('total', totalRegisterEC)
          $('[data-info-autoinvoice-subtotal=0]').html(money_format(totalInvoice))
          $('[data-info-autoinvoice-total=0]').html(money_format(totalInvoice-creditTotal))
          registerSelected.data('total', totalRegisterEC)
          if(serviceId == 0){
            indexCharge = parseInt(contentButton.data("index"))
            removeExtraCharge(registerId, registerType, 0, indexCharge)
          }else{
            removeExtraCharge(registerId, registerType, serviceId, 0)
          }
          if(lengthEC == 0){
            openloadingcontent()
            notFoundCharges =  true;
            tableExtraChargues.addClass('hide');
            $('.not-found').css({"display":"flex"});
          }
        })
      })
      if(ec == 0){
        $('.box-shadow').css({"display":"block"});
        $('.extra-chargues').css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
        ec = 1;
      }
      else{
        $('.box-shadow').css({"display":"none"});
        $('.extra-chargues').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
        ec = 0;
      }
      $('#buttonAddCharge').bind("click", ()=>{
        $('.delete-chargue > img').unbind("click")
        var txtCost = $('#otherCost')
        var txtInformation = $('#extraInformation')
        var txtServiceQty  = $('#serviceQty')
        var registerExtraCharge = {}
        var tableExtraChargues = $('[data-table-extra-chargues=0]')
        var optionSelected = selectServicesCustomer.find(":selected")
        var totalRegisterElement = registerSelected.find("[data-register-total=0]")
        var newTotalRegister = 0
        var totalRegister = registerSelected.data('total')

        var valueCost = parseFloat(txtCost.val())
        var valueQty = parseFloat(txtServiceQty.val())
        var validCost = true
        var validQty = true

        if(parseInt(optionSelected.data("service-variant")) != 0){
          if(isNaN(valueCost)){
            validCost = false
          }else if(valueCost < 0){
            validCost = false
          }
        }

        if(isNaN(valueQty)){
          validQty = false
        }else if(valueQty < 0){
          validQty = false
        }else if(!validatePositiveFloat(valueCost)){
          validCost = false
        }else if(!validatePositiveInteger(valueQty)){
          validQty = false
        }

        if(!validCost){
          showErrorNotify("El costo debe ser numerico y mayor o igual a 0, ingrese los datos correctamente")
          return
        }
        if(!validQty){
          showErrorNotify("La cantidad debe ser numerica con o sin punto decimal mayor a 0, ingrese los datos correctamente")
          return
        }

        if(notFoundCharges)
        {
            tableExtraChargues.html(`
            <tr>
              <td>Delete</td>
              <td>Code</td>
              <td>Description</td>
              <td>Cost</td>
            </tr>`)
            openloadingcontent()
            tableExtraChargues.removeClass('hide')
            $('.not-found').css({"display":"none"});
            notFoundCharges = false
        }
        for(var i = 0; i < parseInt(txtServiceQty.val()); i++){
          lengthIndex++
          registerExtraCharge.index = lengthIndex
          if(parseInt(optionSelected.data("service-variant")) != 0){
            if(valueCost === parseFloat(optionSelected.data("service-cost"))){
              registerExtraCharge.costService = valueCost
            }else{
              registerExtraCharge.costService = valueCost
            }
          }else{
            registerExtraCharge.costService = parseFloat(optionSelected.data("service-cost"))
          }
          if(registerType.localeCompare("receiving") == 0){
            registerExtraCharge.service_receiving_id = 0,
            registerExtraCharge.type = true
          }else{
            registerExtraCharge.service_shipping_id = 0,
            registerExtraCharge.type = false
          }
          registerExtraCharge.codeService = optionSelected.data("service-code")
          registerExtraCharge.descriptionService = optionSelected.html()
          addExtraCharge(registerId, registerType, registerExtraCharge.descriptionService, selectServicesCustomer.val(), registerExtraCharge.codeService, registerExtraCharge.costService, optionSelected.data("service-variant"), txtInformation.val())
          tableExtraChargues.append(putExtraChargue(registerExtraCharge, registerExtraCharge.type))
          totalRegister += registerExtraCharge.costService
          totalInvoice += registerExtraCharge.costService
        }
        totalRegisterElement.html(money_format(totalRegister))
        registerSelected.data('total', totalRegister)
        if(registerType.localeCompare("shipping") == 0){
          totalShippingDialog.html(money_format(totalRegister))
        }
        $('[data-info-autoinvoice-subtotal=0]').html(money_format(totalInvoice))
        $('[data-info-autoinvoice-total=0]').html(money_format(totalInvoice - creditTotal))
        $('.delete-chargue > img').bind("click", (e)=>{
          var buttonDelete =                       $(e.currentTarget)
          var contentButton = buttonDelete.parents('.delete-chargue')
          var register = contentButton.parents('tr')
          var indexCharge = parseInt(contentButton.data("index"))
          var serviceId    = parseInt(contentButton.data("service-id"))
          var totalExtra  = parseFloat(contentButton.data("total-extra"))
          var totalRegisterEC = parseFloat(registerSelected.data('total'))
          var totalRegisterElementEC = registerSelected.find('[data-register-total=0]')
          register.remove()
          totalInvoice -= totalExtra
          totalRegisterEC -= totalExtra
          totalRegisterElementEC.html(money_format(totalRegisterEC))
          totalRegisterElementEC.data('total', totalRegisterEC)
          $('[data-info-autoinvoice-subtotal=0]').html(money_format(totalInvoice))
          $('[data-info-autoinvoice-total=0]').html(money_format(totalInvoice -creditTotal))
          registerSelected.data('total', totalRegisterEC)
          if(serviceId == 0){
            indexCharge = parseInt(contentButton.data("index"))
            removeExtraCharge(registerId, registerType, 0, indexCharge)
          }else{
            removeExtraCharge(registerId, registerType, serviceId, 0)
          }
          if(lengthEC == 0){
            openloadingcontent()
            notFoundCharges =  true;
            tableExtraChargues.addClass('hide');
            $('.not-found').css({"display":"flex"});
          }
        })
        selectServicesCustomer.val(0)
        txtCost.val(0)
        txtInformation.val("")
        txtServiceQty.val(1)
        $('.add_service').hide();
        $('.applied-charges').show();
      })
    });
  })
}

function addExtraCharge(id, type, descriptionText, descriptionId, code, cost, variant, extras){
  extraCharges.push({
    index: lengthIndex,
    serviceId: 0,
    id: id,
    type: type,
    status: 1,
    descriptionService: descriptionText,
    descriptionServiceId: descriptionId,
    codeService: code,
    costService: cost,
    variantService: variant,
    extraInformationService:extras
  })
  lengthEC++
}

function removeExtraCharge(id, type, serviceId, indexCharge=null){
  var isSaved   = false
  extraCharges.forEach((charge, index, array)=>{
    if(charge.id == id && charge.type == type){
      if(charge.serviceId == 0 && serviceId == 0 && charge.status == 1 && charge.index == indexCharge){
        array.splice(index, 1)
        isSaved = true
        lengthEC--
        return
      }else if(charge.serviceId != 0 && serviceId != 0 && charge.serviceId == serviceId && charge.status == 0){
        isSaved = true
        return
      }
    }
  })
  if(!isSaved){
    extraCharges.push({
      serviceId: serviceId,
      id: id,
      type: type,
      status: 0
    })
    lengthEC--
  }
}

function putExtraChargue(extraChargue, type){
  return `
  <tr>
    <td class="delete-chargue" data-service-id=${(type) ?
      `${extraChargue.service_receiving_id} ${(extraChargue.service_receiving_id == 0) ? `data-index=${extraChargue.index}` : '' }`
      : `${extraChargue.service_shipping_id} ${(extraChargue.service_shipping_id == 0) ? `data-index=${extraChargue.index}` : '' }`} data-total-extra=${extraChargue.costService}>
      <img src="${serverURI}public/assets/images/icons/delete.svg" alt="">
    </td>
    <td>${extraChargue.codeService}</td>
    <td>${extraChargue.descriptionService}</td>
    <td><span>${money_format(extraChargue.costService)}</span></td>
  </tr>
  `
  $('.content-loading').addClass('loading-dialog');
  tableExtraChargues.removeClass('hide');
  $('.not-found').css({"display":"none"});
}

function putShippingInvoice(shippingInvoice, haveServicesCustomer){
  return `
  <div class="content-data-box" data-shipping-id=${shippingInvoice.shipping_id} data-type="shipping" data-total=${shippingInvoice.totalShipping}>
      <div class="info-data">
          <div>
              <p>${shippingInvoice.shipping_number}</p>
          </div>
          <div>
              <p>${shippingInvoice.dateShipping}</p>
          </div>
          <div>
              <p data-register-total=0 data-total=${shippingInvoice.totalShipping}>${money_format(shippingInvoice.totalShipping)}</p>
          </div>
      </div>
      <div class="options-data">
          <div>
              <p class="sDetails">Details</p>
              <div class="shipping-details">
                  <div class="title-dialog">
                    <h2>Details</h2>
                    <img src="${serverURI}public/assets/images/icons/close.svg" class="close-dialog-shadow">
                  </div>
                  <div class="shipping-detail-info">
                      <p>${shippingInvoice.shipping_number}</p>
                      <p>${shippingInvoice.dateShipping}</p>
                      <p data-total=0>${money_format(shippingInvoice.totalShipping)}</p>
                  </div>
                  <div class="shipping-detail-head">
                      <div class="qty-shipping-detail">
                          <p>Qty</p>
                      </div>
                      <div class="description-shipping-detail">
                          <p>Description</p>
                      </div>
                      <div class="type-price-detail">
                          <p>Price Per Package</p>
                      </div>
                      <div class="total-shipping-detail">
                          <p>Amount</p>
                      </div>
                  </div>
                  <div class="box-data-details">
                    ${ (shippingInvoice.invoiceslist.length > 1) ? (shippingInvoice.invoiceslist.reduce((previous, current, index)=>{
                      if( typeof previous != 'string' ){
                        previous = putInvoiceList(previous)
                      }
                      return previous + putInvoiceList(current)
                    }))
                    : (shippingInvoice.invoiceslist.length == 1) ? putInvoiceList(shippingInvoice.invoiceslist[0]) : '' }
                  </div>
                  ${ (shippingInvoice.haveCredit) ?
                    `<div class="box-credit">
                      <p>Credit</p>
                      <div>
                        <p>$</p>
                        <input type="number" required data-credit=0 data-value=${shippingInvoice.creditShipping} data-last-value=${shippingInvoice.creditShipping} step="0.01" name="" value="${shippingInvoice.creditShipping}">
                      </div>
                    </div>`
                  : ''}
              </div>
          </div>
          ${ (haveServicesCustomer) ?
            `<div>
              <div class="aExtraR aExtra button-extra-chargue">
                <img src="${serverURI}public/assets/images/icons/dollar.svg" alt="">
                <div class="tooltip" style="display: none;">
                  <p>Manage extra charges</p>
                </div>
              </div>
            </div>`
            :
            '' }
      </div>
  </div>
  `
}

function putInvoiceList(invoiceList){
  return `
  <div class="data-shipping-detail" data-invoicelist-id=${invoiceList.invoicelistid} data-invoicelist-type=${invoiceList.typeInvoiceList} data-quantity=${invoiceList.qty} data-total=${invoiceList.total}>
      <div class="qty-shipping-detail">
          <p>${invoiceList.qty}</p>
      </div>
      <div class="description-shipping-detail">
      ${ (("Handling").localeCompare(invoiceList.typeInvoiceList) == 0) ?
          `<p>${invoiceList.description}</p>`
          :
          `<p>Storage</p>
          <input type="number" required data-daysstorage=0 step=0.01 data-value=${invoiceList.daysStorage} value="${invoiceList.daysStorage}">
          <p>Day(s)</p>`
       }
      </div>
      <div class="type-price-detail">
          <div>
            <span>$</span>
            <input type="number" required data-unitprice=0 step=0.01 data-value=${invoiceList.unitprice} value="${invoiceList.unitprice}">
          </div>
      </div>
      <div class="total-shipping-detail">
          <p data-invoicelist-total=0>${money_format(invoiceList.total)}</p>
      </div>
  </div>
  `
}

function putServiceCustomer(serviceCustomer){
  return `
  <option
  value="${serviceCustomer.service_id}"
  data-service-code="${serviceCustomer.codeService}"
  data-service-variant=${serviceCustomer.variantService}
  data-service-cost=${serviceCustomer.costService}>
    ${serviceCustomer.descriptionService}
  </option>
  `
}

function money_format(quantity_money){
  var quantity_final   =                                           ""
  var quantity_integer =                     parseInt(quantity_money)
  var quantity_decimal = quantity_money - quantity_integer
  var quantity_string  =                   quantity_integer.toString()
  var symbol = 0
  for( var c = quantity_string.length - 1; c >= 0; c-- ){
    if( symbol == 3 ){
      quantity_final += ","
      symbol = 0
    }
    symbol++
    quantity_final += quantity_string[c]
  }
  quantity_string = quantity_final
  quantity_final  = ""
  for( var c = quantity_string.length - 1; c >= 0; c-- ){
    quantity_final += quantity_string[c]
  }
  quantity_string = quantity_decimal.toFixed(2).toString().substring(1, 5)
  return `$ ${quantity_final + quantity_string}`
}
//Initialize event to each component in the view
function initialize(){
  var bodyElement = $('body')
  //Define global vars
  serverURI =                  bodyElement.data("uri")
  invoiceId = parseInt(bodyElement.data("invoice-id"))

  $("#invoiceStartDate").change((e)=>{
    var dateInvoice = $(e.currentTarget)
    var EdateEndInvoice = $('#invoiceDate')
    var previousValue= dateInvoice.data("value")
    var currentValue = dateInvoice.val()
    var isValid = true
    if(("").localeCompare(currentValue) == 0){
      isValid = false
    }else{
      if(!validateAllDate(currentValue)){
        isValid = false
      }
    }
    if(!isValid){
      showErrorNotify("Las fechas deben tener el formato YYYY-MM-DD, y deben existir, favor de ingresar la fecha correctamente correctamente")
      dateStartInvoice = ""
      EdateEndInvoice.change()
      return
    }
    if(currentValue !== previousValue){
      if(statusDateInvoice){
        if(currentValue.localeCompare(EdateEndInvoice.val()) === -1 || currentValue.localeCompare(EdateEndInvoice.val()) === 0){
          dateStartInvoice = currentValue.toString()
        }else if(currentValue.localeCompare(EdateEndInvoice.val()) === 1){
          dateStartInvoice = ""
          dateInvoice.val(dateInvoice.data("value"))
          showErrorNotify("No puede existir una fecha de inicio mayor a la fecha final.")
        }
      }else{
        dateStartInvoice = currentValue.toString()
      }
    }else{
      dateStartInvoice = ""
    }
  })
  $("#invoiceDate").change((e)=>{
    var dateInvoice = $(e.currentTarget)
    var EdateStartInvoice = $('#invoiceStartDate')
    var previousValue= dateInvoice.data("value")
    var currentValue = dateInvoice.val()
    var isValid = true
    if(("").localeCompare(currentValue) == 0){
      isValid = false
    }else{
      if(!validateAllDate(currentValue)){
        isValid = false
      }
    }
    if(!isValid){
      showErrorNotify("Las fechas deben tener el formato YYYY-MM-DD, y deben existir, favor de ingresar la fecha correctamente correctamente")
      dateStartInvoice = ""
      EdateEndInvoice.change()
      return
    }
    if(currentValue !== previousValue){
      if(statusDateInvoice){
        if(currentValue.localeCompare(EdateStartInvoice.val()) === 1 || currentValue.localeCompare(EdateStartInvoice.val()) === 0){
          dateEndInvoice = currentValue.toString()
        }else if(currentValue.localeCompare(EdateStartInvoice.val()) === -1){
          dateEndInvoice = ""
          dateInvoice.val(dateInvoice.data("value"))
          showErrorNotify("No puede existir una fecha final menor a la fecha de inicio.")
        }
      }else{
        dateEndInvoice = currentValue.toString()
      }
    }else{
      dateEndInvoice = ""
    }
  })

  /**********************Receiving Details***************************/
  $('.aDetails').click(function(){
    if(det == 0){
      $('.box-shadow').css({"display":"block"});
      $(this).next().css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
      det = 1;
    }
    else{
      $('.box-shadow').css({"display":"none"});
      $(this).next().css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
      det = 0;
    }
  });
  /******************Shipping Details**************************/
  $('.sDetails').click(function(){
    if(det == 0){
      $('.box-shadow').css({"display":"block"});
      $(this).next().css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
      sdet = 1;
    }
    else{
      $('.box-shadow').css({"display":"none"});
      $(this).next().css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
      sdet = 0;
    }
  });
  /******************************* Extra cargo dialogo**************************************/
  $('.add_service').hide();

  $('.service').change(function(){
    var optionSelected = $(this).find(":selected")
    $('[data-extra-code=0]').html(optionSelected.data("service-code"))
    $('[data-extra-description=0]').html(optionSelected.html())
    $('#extraInformation').val("")
    $('#serviceQty').val(1)
    $('#otherCost').val(0)
    if(parseInt(optionSelected.data("service-variant")) == 0){
      $('[data-variant-input=0]').hide()
    }else{
      $('[data-variant-input=0]').show()
      $('#otherCost').val(optionSelected.data("service-cost"))
    }
    if( (parseInt(optionSelected.val())) === 0 ){
      $('.add_service').hide();
      $('.applied-charges').show();
    }else{
      $('.add_service').show();
      $('.applied-charges').hide();
    }
  });

  $(".cancel-btn").click(function() {
    selectServicesCustomer.val(0)
    $('.add_service').hide();
    $('.applied-charges').show();
    $('#buttonAddCharge').unbind("click");
  });
  /***********************Close box-shadow y dialogos***********************/
  $('.box-shadow').click(function(){
    sdet = 0;
    det = 0;
    ec = 0;
    $('.add_service').hide();
    $('.applied-charges').show();
    $('.box-shadow').css({"display":"none"});
    $('.shipping-details').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
    $('.extra-chargues').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
    $('.receiving-details').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
    $('#buttonAddCharge').unbind("click");
  });
  $('.close-dialog-shadow').click(function(){
      sdet = 0;
      det = 0;
      ec = 0;
      $('.add_service').hide();
      $('.applied-charges').show();
      $('.box-shadow').css({"display":"none"});
      $('.shipping-details').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
      $('.extra-chargues').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
      $('.receiving-details').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
      $('#buttonAddCharge').unbind("click");
  });
  $('.pop-msj span').html("")
  $('.close-pop-msj').click(function(){
      quitMessageAlert()
  });
  $('.confirm-pop').click(function(){
      quitMessageAlert()
  })
  $('#backButton').click(()=>{
    statusAction = 1
    if(extraCharges.length != 0 || shippings.length != 0 || dateStartInvoice.localeCompare("") != 0 || dateEndInvoice.localeCompare("") != 0){
      showdialog(()=>{
        location.href = `${serverURI}index.php/invoice`
      }, "Are you sure exit without save?")
    }else{
      location.href = `${serverURI}index.php/invoice`
    }
  })
}

function showdialog(callback, message){
    $('.message-dialog-not').html(message);

    $('.box-confirmation-not').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"pop-in", "animation-duration":"0.25s"});
    $('.shadow-notify').css({"display":"block"});


    $('.yes-confirmation').bind("click", function(){
        callback();
        $('.box-confirmation-not').css({"display":"none"});
        $('.shadow-notify').css({"display":"none"});
        $('.yes-confirmation').unbind("click");
    });
    $('.no-confirmation').click(function(){
        $('.box-confirmation-not').css({"display":"none"});
        $('.shadow-notify').css({"display":"none"});
        $('.yes-confirmation').unbind("click");
        statusAction = 0
    });
}
function shownotify(type, time = 3000){
    selectnotify(type, "notify-in");
    setTimeout(function(){
        selectnotify(type, "notify-out");
        setTimeout(function(){
            selectnotify(type, "notify-in", false);
            selectnotify(type, "notify-out", false);
        }, 370);
    }, time);
}
function selectnotify(type, className, isAdd=true){
    switch (type) {
        case 0:
            if(isAdd){
                $('.save-not').addClass(className);
            }else{
                $('.save-not').removeClass(className);
            }
            break;
        case 1:
            if(isAdd){
                $('.delete-not').addClass(className);
            }else{
                $('.delete-not').removeClass(className);
            }
            break;
        case 2:
            if(isAdd){
                $('.edit-not').addClass(className);
            }else{
                $('.edit-not').removeClass(className);
            }
            break;
        default:
        break;
    }
}

function showErrorNotify(message){
  $('.pop-msj span').html(message);
  $('.shadow-notify').toggleClass('visible')
  $('.pop-msj').css({"animation":"notify-in","animation-duration":"0.25s", "display":"block"});
}
function quitMessageAlert(){
    $('.shadow-notify').toggleClass('visible')
    $('.pop-msj').css({"animation":"pop-out","animation-duration":"0.25s", "display":"block"});
    setTimeout(function(){
        $('.pop-msj').css({"display":"none"});
    },220);
}

function validateDate(data){
  var regExp = /^[0-9]{4}\-[0-1]?[0-9]\-[0-3]?[0-9]$/
  return regExp.test(data)
}

function validatePositiveInteger(data){
  var regExp = /^[0-9]+$/
  return regExp.test(data)
}

function validatePositiveFloat(data){
  var regExp = /^[0-9]+(\.[0-9]+)?$/
  return regExp.test(data)
}

function validateAllDate(data){
  var isValid = true
  if(!validateDate(data)){
    isValid = false
  }else{
    var partDate = data.split("-")
    var year = parseInt(partDate[0])
    var month = parseInt(partDate[1]) - 1
    var day = parseInt(partDate[2])
    var dateObject = new Date(year, month, day)
    if(!((day==dateObject.getDate()) && (month==dateObject.getMonth()) && (year==dateObject.getFullYear()))){
      isValid = false
    }
  }
  return isValid
}

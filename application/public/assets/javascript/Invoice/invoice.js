var ec        =  0;
openloading();

/*********************************************************************View Invoice********************************************************/
var spi = 0;
$('.save-payment-btn').click(function(){
    setTimeout(function(){
        $('.due-amount-invoice').css({"animation":"pop-in","animation-duration":"0.25s"});
        $('.paid-total-invoice').css({"animation":"pop-in","animation-duration":"0.25s"});
    },500);
    setTimeout(function(){
        $('.due-amount-invoice').css({"animation":"unset"});
        $('.paid-total-invoice').css({"animation":"unset"});
    },1000);
});
/**********************************************************************End view invoice*******************************************************/
var det       =  0;
var sdet      =  0;
var invoiceId =  0;
var serverURI = "";
var extraCReceiving = {};
var extraCShipping  = {};
var selectServicesCustomer =  $('#selectServicesCustomer');

(()=>{
  // initialize()
  // getInvoice()
})()

// function getInvoice(){
//   $.ajax({
//     url: `${serverURI}index.php/invoice/edit/get`,
//     type: "GET",
//     dataType: 'json',
//     data: {
//       invoiceId
//     },
//     success: data=>{
//       switch(data.code){
//         case 200:
//         var shippingTable          =  $('[data-table-shipping=0]')
//         var receivingTable         = $('[data-table-receiving=0]')
//         $('[data-info-autoinvoice-id=0]').html(data.response.invoice.autoid)
//         $('[data-info-autoinvoice-customer=0]').html(data.response.invoice.name)
//         $('#invoiceDate').val(data.response.invoice.dateInvoice)
//         $('#invoiceStartDate').val(data.response.invoice.dateInvoice)
//         data.response.receivingsInvoice.forEach(receivingInvoice=>{
//           receivingTable.append(putReceivingInvoice(receivingInvoice, data.response.invoice.haveServicesCustomer))
//         })
//         data.response.shippingsInvoice.forEach(shippingInvoice=>{
//           shippingTable.append(putShippingInvoice(shippingInvoice, data.response.invoice.haveServicesCustomer))
//         })
//         $('[data-info-autoinvoice-subtotal=0]').html(money_format(data.response.invoice.subTotalInvoice))
//         $('[data-info-autoinvoice-credit=0]').html(money_format(data.response.invoice.creditInvoice))
//         $('[data-info-autoinvoice-total=0]').html(money_format(data.response.invoice.totalInvoice))
//         if(data.response.invoice.haveServicesCustomer){
//           data.response.invoice.servicesCustomer.forEach(serviceCustomer=>{
//             selectServicesCustomer.append(putServiceCustomer(serviceCustomer))
//           })
//         }
//         break
//         case 404:
//         console.log("Error 404: No existe esa autoinvoice")
//         break
//         case 500:
//         console.log("Error 500: Al recibir los parametros")
//         break
//       }
//     },
//     error: (request, msg, error) => {
//         console.log(error)
//     }
//   })
//   .done(()=>{
//     /******************Receiving Details**************************/
//     $('.aDetails').click(function(){
//       if(det == 0){
//         $('.box-shadow').addClass('visible');
//         $(this).next().css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
//         det = 1;
//       }
//       else{
//         $('.box-shadow').removeClass('visible');
//         $(this).next().css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
//         det = 0;
//     }
// });
// /******************Shipping Details**************************/
// var sdet = 0;
// $('.sDetails').click(function(){
//     if(det == 0){
//         $('.box-shadow').addClass('visible');
//         $(this).next().css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
//         sdet = 1;
//       }
//       else{
//         $('.box-shadow').removeClass('visible');
//         $(this).next().css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
//         sdet = 0;
//     }
// });
// /**********************************extra cargos*************************/
// var ec = 0;
// /***********Tool tip extra chargues********/
// $(".button-extra-chargue").hover(function(){
//     $(this).find('.tooltip').css({"display":"block"});
// });
// $(".button-extra-chargue").mouseleave(function(){
//     $(this).find('.tooltip').css({"display":"none"});
// });
//     $('.button-extra-chargue').click(function(){
//       var registerSelected   = $(this).parent().parent().parent()
//       var registerType       =      registerSelected.data('type')
//       var registerId         =                                  0
//       var tableExtraChargues = $('[data-table-extra-chargues=0]')
//       tableExtraChargues.html("")
//       selectServicesCustomer.val("0")
//       if( registerType.localeCompare("receiving") == 0 ){
//         registerId = registerSelected.data("receiving-id")
//         $.ajax({
//           url: `/ISS/application/index.php/invoice/get/extra/receiving?invoiceId=${invoiceId}&receivingId=${registerId}`,
//           type: "GET",
//           dataType: "json",
//           success: data=>{
//             switch(data.code){
//               case 200:
//               tableExtraChargues.data("type", "receiving")
//               tableExtraChargues.html(`
//               <tr>
//                 <td>Delete</td>
//                 <td>Code</td>
//                 <td>Description</td>
//                 <td>Cost</td>
//               </tr>`)
//               data.response.chargues.forEach(chargue=>{
//                 tableExtraChargues.append(putExtraChargue(chargue, true))
//               })
//               break
//               case 404:
//               console.log("Error 404")
//               tableExtraChargues.html("<tr><td>Error 404: No se encontraron elementos</td></tr>")
//               break
//               case 500:
//               console.log("Error 500")
//               break
//             }
//           },
//           error: (request, msg, error) => {
//               console.log(error)
//           }
//         })
//       }else{
//         registerId = registerSelected.data("shipping-id")
//         tableExtraChargues.data("type", "receiving")
//         $.ajax({
//           url: `/ISS/application/index.php/invoice/get/extra/shipping?invoiceId=${invoiceId}&shippingId=${registerId}`,
//           type: "GET",
//           dataType: "json",
//           success: data=>{
//             switch(data.code){
//               case 200:
//               tableExtraChargues.html(`
//               <tr>
//                 <td>Delete</td>
//                 <td>Code</td>
//                 <td>Description</td>
//                 <td>Cost</td>
//               </tr>`)
//               data.response.chargues.forEach(chargue=>{
//                 tableExtraChargues.append(putExtraChargue(chargue, false))
//               })
//               break
//               case 404:
//               console.log("Error 404");
//               tableExtraChargues.html("<tr><td>Error 404: No se encontraron elementos</td></tr>")
//               break
//               case 500:
//               console.log("Error 500")
//               break
//             }
//           },
//           error: (request, msg, error) => {
//               console.log(error)
//           }
//         })
//       }
//       if(ec == 0){
//         $('.box-shadow').addClass('visible');
//         $('.extra-chargues').css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
//         ec = 1;
//       }
//       else{
//         $('.box-shadow').removeClass('visible');
//         $('.extra-chargues').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
//         ec = 0;
//       }
//     });
//   })
// }

function putExtraChargue(extraChargue, type){
  return `
  <tr>
    <td class="delete-chargue" data-service-id=${(type) ? extraChargue.receiving_id : extraChargue.shipping_id}>
      <img src="${serverURI}public/assets/images/icons/delete.svg" alt="">
    </td>
    <td>${extraChargue.codeService}</td>
    <td>${extraChargue.descriptionService}</td>
    <td>$ <span>${extraChargue.costService}</span></td>
  </tr>
  `
}
function putReceivingInvoice(receivingInvoice, haveServicesCustomer){
  return `
  <div class="content-data-box" data-receiving-id=${receivingInvoice.receiving_id} data-type="receiving">
    <div class="info-data">
        <div>
            <p>${receivingInvoice.receiving_number}</p>
        </div>
        <div>
            <p>${receivingInvoice.dateReceiving}</p>
        </div>
        <div>
            <p data-receiving-total=0>${money_format(receivingInvoice.totalReceiving)}</p>
        </div>
    </div>
    <div class="options-data">
      <div>
        <p class="aDetails">Details</p>
        <div class="receiving-details">
          <div>
            <input id="check-hazmat${receivingInvoice.receiving_number}" type="checkbox" class="hazmat" name="hazmat${receivingInvoice.receiving_number}" value="" ${(receivingInvoice.hazmatReceiving) ? 'checked' : ''}>
            <label for="check-hazmat${receivingInvoice.receiving_number}">Hazmat</label>
          </div>
          <div>
            <input id="nbound${receivingInvoice.receiving_number}" type="radio" class="bound" name="bound${receivingInvoice.receiving_number}" value="" ${(receivingInvoice.boundReceiving) ? 'checked' : ''}>
            <label for="nbound${receivingInvoice.receiving_number}">Northbound</label>
          </div>
          <div>
            <input id="sbound${receivingInvoice.receiving_number}" type="radio" class="bound" name="bound${receivingInvoice.receiving_number}" value="" ${(!receivingInvoice.boundReceiving) ? 'checked' : ''}>
            <label for="sbound${receivingInvoice.receiving_number}">Southbound</label>
          </div>
          <div class="total-detail">
            <p data-bound-status=0>${(receivingInvoice.boundReceiving) ? 'NorthBound' : 'SouthBound'}</p>
            <p data-bound-cost=0>${money_format((receivingInvoice.boundReceiving) ? receivingInvoice.northCost : receivingInvoice.southCost)}</p>
          </div>
        </div>
      </div>
      ${ (haveServicesCustomer) ?
        `<div>
          <div class="aExtraR aExtra button-extra-chargue">
            <img src="${serverURI}public/assets/images/icons/dollar.svg" alt="">
          </div>
        </div>`
        :
        '' }
    </div>
  </div>
  `
}
function putShippingInvoice(shippingInvoice, haveServicesCustomer){
  return `
  <div class="content-data-box" data-shipping-id=${shippingInvoice.shipping_id} data-type="shipping">
      <div class="info-data">
          <div>
              <p>${shippingInvoice.shipping_number}</p>
          </div>
          <div>
              <p>${shippingInvoice.dateShipping}</p>
          </div>
          <div>
              <p>${money_format(shippingInvoice.totalShipping)}</p>
          </div>
      </div>
      <div class="options-data">
          <div>
              <p class="sDetails">Details</p>
              <div class="shipping-details">
                  <div class="shipping-detail-info">
                      <p>${shippingInvoice.shipping_number}</p>
                      <p>${shippingInvoice.dateShipping}</p>
                      <p>${money_format(shippingInvoice.totalShipping)}</p>
                  </div>
                  <div class="shipping-detail-head">
                      <div class="qty-shipping-detail">
                          <p>Qty</p>
                      </div>
                      <div class="description-shipping-detail">
                          <p>Description</p>
                      </div>
                      <div class="type-price-detail">
                          <p>Package Type Price</p>
                      </div>
                      <div class="total-shipping-detail">
                          <p>Total</p>
                      </div>
                  </div>
                  <div class="box-data-details">
                    ${(shippingInvoice.invoiceslist.reduce((previous, current, index)=>{
                      if( typeof previous != 'string' ){
                        previous = putInvoiceList(previous)
                      }
                      return previous + putInvoiceList(current)
                    }))}
                  </div>
              </div>
          </div>
          ${ (haveServicesCustomer) ?
            `<div>
              <div class="aExtraR aExtra button-extra-chargue">
                <img src="${serverURI}public/assets/images/icons/dollar.svg" alt="">
              </div>
            </div>`
            :
            '' }
      </div>
  </div>
  `
}

function putInvoiceList(invoiceList){
  return `
  <div class="data-shipping-detail" data-invoicelist-id=${invoiceList.invoicelistid}>
      <div class="qty-shipping-detail">
          <p>${invoiceList.qty}</p>
      </div>
      <div class="description-shipping-detail">
      ${ (("Handling").localeCompare(invoiceList.typeInvoiceList) == 0) ?
          `<p>${invoiceList.description}</p>`
          :
          `<p>Storage</p>
          <input type="number" name="" value="${invoiceList.daysStorage}">
          <p>Day(s)</p>`
       }
      </div>
      <div class="type-price-detail">
          <p>Package Type Price</p>
          <input type="number" name="" value="${invoiceList.unitprice}">
      </div>
      <div class="total-shipping-detail">
          <p>${money_format(invoiceList.total)}</p>
      </div>
  </div>
  `
}

function putServiceCustomer(serviceCustomer){
  return `
  <option
  value="${serviceCustomer.service_id}"
  data-service-code="${serviceCustomer.codeService}"
  data-service-variant=${serviceCustomer.variantService}
  data-service-cost=${serviceCustomer.costService}>
    ${serviceCustomer.descriptionService}
  </option>
  `
}

function money_format(quantity_money){
  var quantity_final   =                                           ""
  var quantity_integer =                     parseInt(quantity_money)
  var quantity_decimal = quantity_money - quantity_integer
  var quantity_string  =                   quantity_integer.toString()
  var symbol = 0
  for( var c = quantity_string.length - 1; c >= 0; c-- ){
    if( symbol == 3 ){
      quantity_final += ","
      symbol = 0
    }
    symbol++
    quantity_final += quantity_string[c]
  }
  quantity_string = quantity_final
  quantity_final  = ""
  for( var c = quantity_string.length - 1; c >= 0; c-- ){
    quantity_final += quantity_string[c]
  }
  quantity_string = quantity_decimal.toFixed(2).toString().substring(1, 5)
  return `$ ${quantity_final + quantity_string}`
}
//Initialize event to each component in the view
function initialize(){
  var bodyElement = $('body')
  //Define global vars
  serverURI =                  bodyElement.data("uri")
  invoiceId = parseInt(bodyElement.data("invoice-id"))
  /**********************Receiving Details***************************/
  $('.aDetails').click(function(){
    if(det == 0){
      $('.box-shadow').css({"display":"block"});
      $(this).next().css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
      det = 1;
    }
    else{
      $('.box-shadow').css({"display":"none"});
      $(this).next().css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
      det = 0;
    }
  });
  /******************Shipping Details**************************/
  $('.sDetails').click(function(){
    if(det == 0){
      $('.box-shadow').css({"display":"block"});
      $(this).next().css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
      sdet = 1;
    }
    else{
      $('.box-shadow').css({"display":"none"});
      $(this).next().css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
      sdet = 0;
    }
  });
  /******************************* Extra cargo dialogo**************************************/
  $('.add_service').hide();

  $('.service').change(function(){
    var optionSelected = $(this).find(":selected")
    $('[data-extra-code=0]').html(optionSelected.data("service-code"))
    $('[data-extra-description=0]').html(optionSelected.html())
    if( (parseInt(optionSelected.val())) === 0 ){
      $('.add_service').hide();
      $('.applied-charges').show();
    }else{
      $('.add_service').show();
      $('.applied-charges').hide();
    }
  });

  $(".cancel-btn").click(function() {
    $('.add_service').hide();
    $('.applied-charges').show();
});
/*************************Close box-shadow y dialogos***********************/
$('.box-shadow').click(function(){
    sdet = 0;
    det = 0;
    ec = 0;
    $('.add_service').hide();
    $('.applied-charges').show();
    $('.box-shadow').removeClass('visible');
    $('.shipping-details').css({"display":"none"});
    $('.extra-chargues').css({"display":"none"});
    $('.receiving-details').css({"display":"none"});
    $('.insert-box').addClass('hide');
    $('.update-box').addClass('hide');
  });
  $('.close-dialog-shadow').click(function(){
      sdet = 0;
      det = 0;
      ec = 0;
      $('.add_service').hide();
      $('.applied-charges').show();
    $('.box-shadow').removeClass('visible');
    $('.shipping-details').css({"display":"none"});
    $('.extra-chargues').css({"display":"none"});
    $('.receiving-details').css({"display":"none"});
    $('.insert-box').addClass('hide');
});
$('.close-dialog-update').click(function(){
    $('.update-box').addClass('hide');
    $('.box-shadow').removeClass('visible');
});
/*********************************************Storage invoice***********************************************/
/***********Select columns**************/
var por = 7;
$('.column-2').css({"display":"none"});
$('.column-3').css({"display":"none"});
$('.column-4').css({"display":"none"});
$('.column-5').css({"display":"none"});
$('.content-data-storage-box div div').css({"width":`calc(100% / ${por})`});
$('.title-storage-box div').css({"width":`calc(100% / ${por})`});

$('.select-columns ul > div').click(function(){
    console.log("Des");
    var idcol = this.id;
    console.log(idcol);
    var classcol = this.className;
    console.log(classcol);

    if(classcol == 'item-column'){
        $(this).removeClass('item-column');
        $(this).addClass('select-item');
        $(this).find('.select-view').css({"display":"block"});
        $(this).find('.select-no-view').css({"display":"none"});
        $(`.column-${idcol}`).css({"display":"block"});
        por++;
        console.log(por);
    }
    else if(classcol == 'select-item'){
        $(this).removeClass('select-item');
        $(this).addClass('item-column');
        $(this).find('.select-no-view').css({"display":"block"});
        $(this).find('.select-view').css({"display":"none"});
        $(`.column-${idcol}`).css({"display":"none"});
        por--;
        console.log(por);
    }
});
/***********Open extra chargues************/
$('.open-extra-chargue').click(function(){
    $('.box-shadow').addClass('visible');
    $('.extra-chargues').css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
});
/********************************************************************Manual Invoice*****************************************************/
$('.manual-invoice-btn').click(function(){
    $('.box-shadow').removeClass('visible');
    $('.extra-chargues').css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
});
$('.btn-action-save').click(function(){
    showdialog(()=>{
        shownotify(0);
    }, 'Are you sure save?');
});
$('.btn-action-delete').click(function(){
    showdialog(()=>{
        shownotify(1);
    }, 'Are you sure delete?');
});
$('.btn-action-edit').click(function(){
    showdialog(()=>{
        shownotify(2);
    }, 'Are you sure update?');
  });
}
/*****************************************************************************Dialogo de confirmacion********************************************/


function showdialog(callback, message){
    $('.message-dialog-not').html(message);

    $('.box-confirmation-not').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"pop-in", "animation-duration":"0.25s"});
    $('.shadow-notify').css({"display":"block"});


    $('.yes-confirmation').bind("click", function(){
        callback();
        $('.box-confirmation-not').css({"display":"none"});
        $('.shadow-notify').css({"display":"none"});
        $('.yes-confirmation').unbind("click");
    });
    $('.no-confirmation').click(function(){
        $('.box-confirmation-not').css({"display":"none"});
        $('.shadow-notify').css({"display":"none"});
        $('.yes-confirmation').unbind("click");
    });
}
function shownotify(type, time = 3000){
    selectnotify(type, "notify-in");
    setTimeout(function(){
        selectnotify(type, "notify-out");
        setTimeout(function(){
            selectnotify(type, "notify-in", false);
            selectnotify(type, "notify-out", false);
        }, 370);
    }, time);
}
function selectnotify(type, className, isAdd=true){
    switch (type) {
        case 0:
            if(isAdd){
                $('.save-not').addClass(className);
            }else{
                $('.save-not').removeClass(className);
            }
            break;
        case 1:
            if(isAdd){
                $('.delete-not').addClass(className);
            }else{
                $('.delete-not').removeClass(className);
            }
            break;
        case 2:
            if(isAdd){
                $('.edit-not').addClass(className);
            }else{
                $('.edit-not').removeClass(className);
            }
            break;
        default:
        break;
    }
}

function showErrorNotify(message){
  $('.pop-msj span').html(message);
  $('.shadow-notify').toggleClass('visible')
  $('.pop-msj').css({"animation":"notify-in","animation-duration":"0.25s", "display":"block"});

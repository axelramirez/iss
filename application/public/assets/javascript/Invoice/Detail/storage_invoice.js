/*
 **************************************************************************************************
 *************************Name: Logic Invoice Detail File HTML
 *************************Description: Mantiene la logica de la aplicacion para la vista Invoice
 ************************************* Detail
 **************************************************************************************************
*/
//Globals var
var ec        =  0;
var det       =  0;
var sdet      =  0;
var numberPayments = 0;
var optionst    =  0;
var invoiceId   =  0;
var serverURI   = "";
var invoiceData = {};
var numberPg    =  0;
var numberPageElement    = $('[data-number-page=0]');
var numberPagesElement   =       $('[data-pages=0]');
var previousPageElement  =            $('.previous');
var numberEntriesElement =     $('[data-entries=0]');
var dateInvoice = '';
$('.pop-msj span').html("");
$('.close-pop-msj').click(function(){
    quitMessageAlert();
});
$('.confirm-pop').click(function(){
    quitMessageAlert();
});
/*
 **************************************************************************************************
 Es el main del archivo
 **************************************************************************************************
*/
(()=>{

  var buttonCancelUPayment =    $('#buttonCancelUPayment')
  var buttonCancelPayment =      $('#buttonCancelPayment')
  var buttonInsertPayment =      $('#buttonInsertPayment')
  var bodyElement         =                      $('body')
  var navigations         =          $('.previous, .next')
  var buttonAddPayment    =         $('#buttonAddPayment')
  serverURI               =        bodyElement.data("uri")
  invoiceId               = bodyElement.data("invoice-id")
  console.log(invoiceId);
  navigations.click(onNavigation)
  buttonCancelPayment.click(onAddPayment)
  buttonCancelUPayment.click(offUpdatePayment)
  buttonAddPayment.click(onAddPayment)
  buttonInsertPayment.click(insertPayment)
  getPayments(1)
  getPaymentType()
  $("body").on('click', '.button-options', function(){
      $(".shadow-table").css({"display":"block"});
  }, onOptions);

  $("body").on('click', '[data-edit-button=0]',  onUpdatePayment);
  $("body").on('click', '[data-delete-button=0]',  (e)=>{
      var buttonDelete = $(e.currentTarget)
      var rowPayment = buttonDelete.parent().parent().parent()
      showdialog(()=>{
          deletePayment(rowPayment)
      }, 'Are you sure delete?')
  });
  $('.box-shadow').click(function(){
    sdet = 0;
    det = 0;
    ec = 0;
    $('.add_service').hide();
    $('.applied-charges').show();
    $('.box-shadow').css({"display":"none"});
    $('.shipping-details').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
    $('.extra-chargues').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
    $('.receiving-details').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
    $('#buttonAddCharge').unbind("click");
  });
  $('.close-dialog-shadow').click(function(){
      sdet = 0;
      det = 0;
      ec = 0;
      $('.add_service').hide();
      $('.applied-charges').show();
      $('.box-shadow').css({"display":"none"});
      $('.shipping-details').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
      $('.extra-chargues').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
      $('.receiving-details').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
      $('#buttonAddCharge').unbind("click");
  });
  $('#backButton').click(()=>{
    location.href = `${serverURI}index.php/invoice`
  })
  getInvoice()
})()

/*
 **************************************************************************************************
 Se encarga de insertar un nuevo pago a traves de una peticion ajax
 **************************************************************************************************
*/
function insertPayment(){
  var errors           =                                  true
  var amount           =     parseFloat($('#textAmount').val())
  var paymentType      =       parseInt($('#selectType').val())
  var referencesNumber =             $('#textReferences').val()
  var paymentDate      = $('#textPaymentDate').val().toString()
  //Amount
  if(isNaN(amount)){
    errors = false
  }else if(amount <= 0){
    errors = false
  }else if(!validatePositiveInteger(amount)){
    errors = false
  }
  if(!errors){
    showErrorNotify("Error: el dato del monto debe de ser numerico y debe ser 1 o mayor.")
    return
  }
  //References Number
  if(isNaN(referencesNumber)){
    errors = false
  }else if(!validatePositiveInteger(referencesNumber)){
    errors = false
  }
  if(!errors){
    showErrorNotify("Error: el dato del numero de referencia debe de ser numerico y debe ser 1 o mayor.")
    return
  }
  //Type payment
  if(isNaN(paymentType)){
    errors = false
  }else if(paymentType <= 0){
    errors = false
  }else if(!validatePositiveInteger(paymentType)){
    errors = false
  }
  if(!errors){
    showErrorNotify("Error: debe seleccionar un tipo de pago.")
    return
  }
  if(("").localeCompare(paymentDate) == 0){
    errors = false
  }else{
    if(!validateAllDate(paymentDate)){
      errors = false
    }else if(paymentDate.localeCompare(dateInvoice) == -1){
      errors = false
    }
  }
  if(!errors){
    showErrorNotify("Error: debe ingresar una fecha con el formato YYYY-MM-DD, que se mayor o igual a la fecha de la factura y que esta fecha exista en el calendario.")
    return
  }
  if( invoiceData.isAllow === 0 ){
    showErrorNotify("No fue posible insertar el nuevo pago, ya que la factura ya esta pagada.")
    $('#textAmount').val("0")
    $('#selectType').val("0")
    $('#textReferences').val("")
    $('.insert-box').toggleClass('hide');
    $('.box-shadow').toggleClass('visible');
    return
  }
  $.ajax({
    url: `${serverURI}index.php/invoice/payment`,
    type: "POST",
    data: {payment:{
        amount: amount,
        payment_type_id: paymentType,
        references_number: referencesNumber,
        autoinvoice_id: invoiceId,
        payment_date: paymentDate
      }
    },
    dataType: "json",
    success:data=>{
      switch(data.code){
        case 201:
        getPayments(1)
        if(numberPayments < 5){
            numberPayments++
        }
        invoiceData = data.response.invoice
        $('[data-info-status=0]').html(data.response.invoice.statusInvoice)
        $('[data-info-paid-amount=0]').html(money_format(data.response.invoice.paidInvoice))
        $('[data-info-due-amount=0]').html(money_format(data.response.invoice.dueInvoice))
        $('[data-table-column=0]').after(putPayment(data.response.payment))
        shownotify(0);
        if((numberPayments-1) == 0){
            foundPayment()
        }
        numberEntriesElement.html(parseInt(numberEntriesElement.html())+1)
        break
        case 400:
        showErrorNotify(data.response.message)
        break
        case 500:
            showErrorNotify("Error al insertar un nuevo pago");
        break
      }
    },
    error: (request, msg, error) => {
        console.log(error)
    }
  })
  .done(()=>{
    $('#textAmount').val("0")
    $('#selectType').val("0")
    $('#textReferences').val("")
    $('#textPaymentDate').val("")
    $('.insert-box').toggleClass('hide');
    $('.box-shadow').toggleClass('visible');
  })
}

/*
 **************************************************************************************************
 Se encarga de eliminar un pago a traves de una peticion ajax
 **************************************************************************************************
*/
function deletePayment(rowPayment){
  $.ajax({
    url: `${serverURI}index.php/invoice/payment`,
    type: "DELETE",
    data: {paymentNumber: rowPayment.data("payment-number"), autoinvoice_id: invoiceId},
    dataType: "json",
    success:data=>{
      switch(data.code){
        case 200:
        getPayments(numberPg)
        if(numberPayments > 0){
            numberPayments--
        }
        if(numberPayments == 0){
            notFoundPayment();
        }
        invoiceData = data.response.invoice
        shownotify(1)
        rowPayment.remove()
        $('[data-info-status=0]').html(data.response.invoice.statusInvoice)
        $('[data-info-paid-amount=0]').html(money_format(data.response.invoice.paidInvoice))
        $('[data-info-due-amount=0]').html(money_format(data.response.invoice.dueInvoice))
        break
        case 400:
          showErrorNotify(data.response.message)
        break
        case 500:
            showErrorNotify("No fue posible eliminar el pago.")
        break
      }
    },
    error: function(request,msg,error) {
        console.log(error)
    }
  })
}

/*
 **************************************************************************************************
 Se encarga de actualizar un pago a traves de una peticion ajax
 **************************************************************************************************
*/
function updatePayment(paymentNumber){
  var errors           =                                       true
  var amount           =         parseFloat($('#textUAmount').val())
  var paymentType      =           parseInt($('#selectUType').val())
  var referencesNumber =                 $('#textUReferences').val()
  var paymentDate      =                $('#textUPaymentDate').val()
  var rowPayment       = $(`[data-payment-number=${paymentNumber}]`)
  //Amount
  if(isNaN(amount)){
    errors = false
  }else if(amount <= 0){
    errors = false
  }else if(!validatePositiveInteger(amount)){
    errors = false
  }
  if(!errors){
    showErrorNotify("Error: el dato del monto debe de ser numerico y debe ser 1 o mayor.")
    return
  }
  //References Number
  if(isNaN(referencesNumber)){
    errors = false
  }else if(!validatePositiveInteger(referencesNumber)){
    errors = false
  }
  if(!errors){
    showErrorNotify("Error: el dato del numero de referencia debe de ser numerico y debe ser 1 o mayor.")
    return
  }
  //Type payment
  if(isNaN(paymentType)){
    errors = false
  }else if(paymentType <= 0){
    errors = false
  }else if(!validatePositiveInteger(paymentType)){
    errors = false
  }
  if(!errors){
    showErrorNotify("Error: debe seleccionar un tipo de pago.")
    return
  }
  if(("").localeCompare(paymentDate) == 0){
    errors = false
  }else{
    if(!validateAllDate(paymentDate)){
      errors = false
    }else if(paymentDate.localeCompare(dateInvoice) == -1){
      errors = false
    }
  }
  if(!errors){
    showErrorNotify("Error: debe ingresar una fecha con el formato YYYY-MM-DD, que se mayor o igual a la fecha de la factura y que esta fecha exista en el calendario.")
    return
  }
  $.ajax({
    url: `${serverURI}index.php/invoice/payment`,
    type: "PUT",
    data: {payment:{
        amount: amount,
        payment_type_id: paymentType,
        references_number: referencesNumber,
        autoinvoice_id: invoiceId,
        payment_date: paymentDate,
        payment_number: paymentNumber
      }
    },
    dataType: "json",
    success: data => {
      switch(data.code){
        case 200:
        shownotify(2);
        invoiceData = data.response.invoice
        renderColumn(rowPayment, 3, money_format(data.response.payment.amount))
        renderColumn(rowPayment, 4, data.response.payment.payment_type_id)
        renderColumn(rowPayment, 5, data.response.payment.references_number)
        renderColumn(rowPayment, 6, data.response.payment.payment_date)
        $('[data-info-status=0]').html(data.response.invoice.statusInvoice)
        $('[data-info-paid-amount=0]').html(money_format(data.response.invoice.paidInvoice))
        $('[data-info-due-amount=0]').html(money_format(data.response.invoice.dueInvoice))
        break
        case 400:
        showErrorNotify(data.response.message)
        break
        case 404:
        showErrorNotify("Error al actualizar el campo, quiza ha sido eliminado, vuelva actualizar la pagina e intentelo de nuevo")
        break
      }
    },
    error: function(request,msg,error) {
        console.log(error)
    }
  })
  .done(()=>{
    $('#textUAmount').val("0")
    $('#selectUType').val("0")
    $('#textUReferences').val("")
    $('#textUPaymentDate').val("")
    $('.update-box').addClass('hide')
    $('.content-dialog-box').addClass('hide')
    $('.box-shadow').removeClass('visible');
  })
}

/*
 **************************************************************************************************

 **************************************************************************************************
*/
function renderColumn(rowPayment, numberColumn, dataColumn){
  if(numberColumn > 2){
    rowPayment.find(`td:nth-child(${numberColumn})`).html(dataColumn)
  }
}
/*
 **************************************************************************************************
 Obtiene todos los pagos paginados para la factura y su informacion
 **************************************************************************************************
*/
function getPayments(numberPage){
  var tableInvoice         =                $('.receiving-search')
  var nextPageElement      =                $('.next')
  var resultPagination     =        $('.table-search')
  openloadingcontent()
  tableInvoice.empty()
  $.ajax({
    url: `${serverURI}index.php/invoice/get`,
    type: "GET",
    data: {
      invoiceId,
      numberPage
    },
    success: data=>{
      switch(data.code){
        case 200:
        if(data.response.payments.length == 0){
            notFoundPayment();
        }
        else{
            foundPayment();
        }
        if(data.response.numberEntries >= 5){
            numberPayments = 5
        }else{
            numberPayments = data.response.numberEntries
        }
        invoiceData = data.response.invoice
        tableInvoice.append(putPayment({}, true))
        numberPageElement.html(numberPage)
        numberPg = numberPage;
        numberPagesElement.html(data.response.numberPages)
        numberEntriesElement.html(data.response.numberEntries)
        previousPageElement.data("value", data.response.previousPage)
        nextPageElement.data("value", data.response.nextPage)
        //Set Invoice Information
        $('[data-info-invoice-id=0]').html(data.response.invoice.autoinvoice_number)
        $('[data-info-status=0]').html(data.response.invoice.statusInvoice)
        $('[data-info-customer=0]').html(data.response.invoice.name)
        $('[data-info-date=0]').html(data.response.invoice.dateInvoice)
        $('[data-info-total-amount=0]').html(money_format(data.response.invoice.totalInvoice))
        $('[data-info-paid-amount=0]').html(money_format(data.response.invoice.paidInvoice))
        $('[data-info-due-amount=0]').html(money_format(data.response.invoice.dueInvoice))

        if(data.response.invoice.dateInvoice == data.response.invoice.dateStartInvoice){
          $('[data-info-end-date=0]').parent().hide()
          $('[data-info-start-date=0]').prev().html("Date")
        }
        $('[data-info-end-date=0]').html(data.response.invoice.dateInvoice)
        $('[data-info-start-date=0]').html(data.response.invoice.dateStartInvoice)
        dateInvoice = data.response.invoice.dateInvoice
        $('[data-info-type=0]').html(data.response.invoice.typeNameInvoice)
        data.response.payments.forEach(payment=>{
            setTimeout(function(){
                tableInvoice.append(putPayment(payment))
            },500);
        })
        if( data.response.previousPage <= 0 ){
          previousPageElement.addClass("invisible")
        }else{
          previousPageElement.removeClass("invisible")
        }
        if( numberPage == data.response.numberPages ){
          nextPageElement.addClass("invisible")
        }else{
          nextPageElement.removeClass("invisible")
        }
        resultPagination.removeClass("hide")

        if(data.response.invoice.isCancelled == 1 || data.response.invoice.isPaid == 1 || data.response.invoice.havePayments == 1){
          $('.fancyBtn.button').addClass("hide")
        }else{
          $('.fancyBtn.button').removeClass("hide")
        }
        break
        case 500:
        showErrorNotify("Error del servidor, porfavor recargue la pagina, si el problema consiste avise a un administrador.")
        break
      }
    }
  })
}

function getInvoice(){
  $.ajax({
    url: `${serverURI}index.php/invoice/storage/edit/get`,
    type: "GET",
    dataType: 'json',
    data: {
      invoiceId
    },
    success: data=>{
      switch(data.code){
        case 200:
        var receivingDetailTable         = $('[data-table-receiving-detail=0]')
        $('[data-total-services=0]').html(money_format(data.response.invoice.totalServices))
        data.response.receivingsDetails.forEach(receivingInvoice=>{
          receivingDetailTable.append(putReceivingDetail(receivingInvoice))
        })
        break
        case 404:
        console.log("Error 404: No existe esa autoinvoice")
        break
        case 500:
        console.log("Error 500: Al recibir los parametros")
        break
      }
    },
    error: (request, msg, error) => {
        console.log(error)
    }
  })
  .done(()=>{
    /***********Tool tip extra chargues********/
    $(".button-extra-chargue").hover(function(){
        $(this).find('.tooltip').css({"display":"block"});
    });
    $(".button-extra-chargue").mouseleave(function(){
        $(this).find('.tooltip').css({"display":"none"});
    });
    /******************Receiving Details**************************/
    $('.aDetails').click(function(){
      if(det == 0){
        $('.box-shadow').css({"display":"block"});
        $(this).next().css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
        det = 1;
      }
      else{
        $('.box-shadow').css({"display":"none"});
        $(this).next().css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
        det = 0;
      }
    });
    /******************Shipping Details**************************/
    $('.sDetails').click(function(){
      if(det == 0){
        $('.box-shadow').css({"display":"block"});
        $(this).next().css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
        sdet = 1;
      }
      else{
        $('.box-shadow').css({"display":"none"});
        $(this).next().css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
        sdet = 0;
      }
    });
    /**********************************extra cargos*************************/
    $('.open-extra-chargue').click(function(){
      var tableExtraChargues = $('[data-table-extra-chargues=0]')
      var extraChargesAdded = []
      openloadingcontentEC()
      $('.content-loading').addClass('loading-dialog');
      $('.not-found').css({"height":"240px"});
      tableExtraChargues.html("")
      $.ajax({
        url: `${serverURI}index.php/invoice/get/extra/storage?invoiceId=${invoiceId}`,
        type: "GET",
        dataType: "json",
        success: data=>{
          switch(data.code){
            case 200:
            tableExtraChargues.html(`
            <tr>
              <td>Code</td>
              <td>Description</td>
              <td>Cost</td>
            </tr>`)
            tableExtraChargues.removeClass('hide');
            $('.not-found-ec').css({"display":"none"});
            data.response.charges.forEach(charge=>{
              tableExtraChargues.append(putExtraChargue(charge, true))
            })
            $('.table').removeClass('hide');
            break
            case 404:
                console.log("Error 404")
                tableExtraChargues.addClass('hide');
                $('.not-found-ec').css({"display":"flex"});
            break
            case 500:
            console.log("Error 500")
            break
          }
        },
        error: (request, msg, error) => {
            console.log(error)
        }
      })
      if(ec == 0){
        $('.box-shadow').css({"display":"block"});
        $('.extra-chargues').css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
        ec = 1;
      }
      else{
        $('.box-shadow').css({"display":"none"});
        $('.extra-chargues').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
        ec = 0;
      }
    });
  })
}

function putExtraChargue(extraChargue, type){
  return `
  <tr>
    <td>${extraChargue.codeService}</td>
    <td>${extraChargue.descriptionService}</td>
    <td>$ <span>${extraChargue.costService}</span></td>
  </tr>
  `
  $('.content-loading').addClass('loading-dialog');
  tableExtraChargues.removeClass('hide');
  $('.not-found').css({"display":"none"});
}

function putReceivingDetail(receivingDetail){
  return `
  <div data-receiving-detail-id=${receivingDetail.receiving_detail_id} data-receiving-quantity=${receivingDetail.quantity} data-total-receiving=${receivingDetail.total}>
      <div class="column-1">
          <p>${receivingDetail.receiving_number}</p>
      </div>
      <div class="column-2">
          <p>${receivingDetail.createdDate}</p>
      </div>
      <div class="column-3">
          <p>${receivingDetail.freightBill}</p>
      </div>
      <div class="column-4">
          <p>${receivingDetail.fr}</p>
      </div>
      <div class="column-5">
          <p>${receivingDetail.tande}</p>
      </div>
      <div class="column-6">
          <p>${receivingDetail.quantity}</p>
      </div>
      <div class="column-7">
          <p>${receivingDetail.description}</p>
      </div>
      <div class="column-8">
          <p>$ ${receivingDetail.chargedDays}</p>
      </div>
      <div class="column-9">
          <p>$ ${money_format(receivingDetail.unitPrice)}</p>
      </div>
      <div class="column-10">
          <p>$</p>
          <span data-total-receiving=0>${money_format(receivingDetail.total)}</span>
      </div>
  </div>`;
}

/*
 **************************************************************************************************
 Regresa un registro de un pago o las columnas del pago para posteriormente renderizarse en la vista
 **************************************************************************************************
*/
function putPayment(payment, first=false){
  if( first ){
    return `
    <tr data-table-column=0>
      <td>
          Options
      </td>
      <td>
          Payment #
      </td>
      <td>
          Payment Amount
      </td>
      <td>
          Payment Type
      </td>
      <td>
          Reference #
      </td>
      <td>
          Payment Date
      </td>
    </tr>`
  }
  return `
  <tr data-payment-number=${payment.payment_number}>
    <td class="button-options"><img src="${serverURI}public/assets/images/icons/more.svg">
      <div class="options-table">
        <div data-edit-button="0">
          <img src="${serverURI}public/assets/images/icons/edit.svg">
          <p>Edit</p>
        </div>
        <div data-delete-button="0" class="delete-payment">
          <img src="${serverURI}public/assets/images/icons/delete.svg">
          <p>Delete</p>
        </div>
      </div>
    </td>
    <td>
      ${payment.payment_number}
    </td>
    <td>
       ${money_format(payment.amount)}
    </td>
    <td>
      ${payment.payment_type_id}
    </td>
    <td>
      ${payment.references_number}
    </td>
    <td>
      ${payment.payment_date}
    </td>
  </tr>`
}

/*
 **************************************************************************************************
 Se asigna al evento click de el boton de opciones de cada registro
 **************************************************************************************************
*/
function onOptions(){
    if(optionst == 0){
    $(this).find('.options-table').css({"display":"block"});
    optionst = 1;
    }
    else{
    $('.options-table').css({"display":"none"});
    optionst = 0;
    }
}

/*
 **************************************************************************************************
 Se asigna al evento click de los botones de navegacion de la paginacion
 **************************************************************************************************
*/
function onNavigation(){
  getPayments($(this).data("value"))
}

/*
 **************************************************************************************************
 Se encarga de dar formato de dinero a un numero
 **************************************************************************************************
*/
function money_format(quantity_money){
  var quantity_final   =                                           ""
  var quantity_integer =                     parseInt(quantity_money)
  var quantity_decimal = quantity_money - quantity_integer
  var quantity_string  =                   quantity_integer.toString()
  var symbol = 0
  for( var c = quantity_string.length - 1; c >= 0; c-- ){
    if( symbol == 3 ){
      quantity_final += ","
      symbol = 0
    }
    symbol++
    quantity_final += quantity_string[c]
  }
  quantity_string = quantity_final
  quantity_final  = ""
  for( var c = quantity_string.length - 1; c >= 0; c-- ){
    quantity_final += quantity_string[c]
  }
  quantity_string = quantity_decimal.toFixed(2).toString().substring(1, 5)
  return `${quantity_final + quantity_string}`
}

/*
 **************************************************************************************************
 Se asigna al evento click para que se muestre el dialogo de insertar
 **************************************************************************************************
*/
function onAddPayment(){
  $('#textAmount').val("0");
  $('#selectType').val("0");
  $('#textReferences').val("");
  $('.insert-box').toggleClass('hide');
  $('.box-shadow').toggleClass('visible');
  openDialog();
}
function openDialog(){
    $('.dialog-box').css({"animation":"pop-in","animation-duration":"0.25s"});
}
function closeDialog(){
    $('.dialog-box').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.dialog-box').css({"display":"none"});
    },200);
}
/*
 **************************************************************************************************
 Se muestra un mensaje de alerta en caso de un error
 **************************************************************************************************
*/
function showMessageAlert(typeMsg){
    switch (typeMsg) {
        case 500:
            $('.pop-msj span').html("Error al insertar un nuevo pago");
            break;
        case 501:
            $('.pop-msj span').html("Cantidad no aceptada");
            break;
        case 502:
            $('.pop-msj span').html("No fue posible insertar el nuevo pago, ya que la factura ya esta pagada.");
            break;
        case 503:

            break;
        default:

    }
    $('.shadow-notify').toggleClass('visible')
    $('.pop-msj').css({"animation":"pop-in","animation-duration":"0.25s", "display":"block"});
}
function quitMessageAlert(){
    $('.shadow-notify').toggleClass('visible')
    $('.pop-msj').css({"animation":"pop-out","animation-duration":"0.25s", "display":"block"});
    setTimeout(function(){
        $('.pop-msj').css({"display":"none"});
    },220);
}
/*
***************************************************************************************************
Mostrar y ocultar cuadro de Not found
***************************************************************************************************
*/
function notFoundPayment(){
    $('.nav.pagination').css({"display":"none"});
    $('.table').addClass('hide');
    $('.not-found').css({"display":"flex"});
}
function foundPayment(){
    $('.nav.pagination').css({"display":"flex"});
    $('.table').removeClass('hide');
    $('.not-found').css({"display":"none"});
}
/*
 **************************************************************************************************
 Se asigna al evento click para que se muestre el dialogo de actualizar
 **************************************************************************************************
*/
function onUpdatePayment(){
  var rowPayment = $(this).parent().parent().parent()
  $('[data-update-payment=0]').html(rowPayment.data("payment-number"))
  $('#textUAmount').val("0")
  $('#selectUType').val("0")
  $('#textUReferences').val("")
  $('#textUPaymentDate').val("")
  $('.update-box').removeClass('hide')
  $('.box-shadow').addClass('visible');
  getPayment(rowPayment.data('payment-number'))
  openDialog();
  $('#buttonUpdatePayment').bind("click",()=>{
    updatePayment(rowPayment.data('payment-number'))
    $('#buttonUpdatePayment').unbind("click")
  })
}
function offUpdatePayment(){
  $('#textUAmount').val("0")
  $('#selectUType').val("0")
  $('#textUReferences').val("")
  $('#textUPaymentDate').val("")
  $('.update-box').addClass('hide')
  $('.box-shadow').removeClass('visible');
}

/*
 **************************************************************************************************
 Obtiene los tipos de pagos a traves de una peticion ajax
 **************************************************************************************************
*/
function getPaymentType(){
  var selectElement = $('#selectType, #selectUType')
  $.ajax({
    url: `${serverURI}index.php/invoice/payment/type/get`,
    type: "GET",
    dataType: "json",
    success: data=>{
      switch(data.code){
        case 200:
        data.response.paymentType.map((type)=>{
          selectElement.append(putPaymentType(type))
        })
        break
        case 500:
            $('.pop-msj span').html("Error al obtener los tipos de pago");
            $('.shadow-notify').toggleClass('visible')
            $('.pop-msj').css({"animation":"notify-in","animation-duration":"0.25s", "display":"block"});
        break
      }
    },
    error: function(request,msg,error) {
        console.log(error)
    }
  })
}

/*
 **************************************************************************************************
 Obtiene un pago por su payment_number y renderizarlo en el dialogo de update
 **************************************************************************************************
*/
function getPayment(id){
  $.ajax({
    url: `${serverURI}index.php/invoice/payment?paymentNumber=${id}&invoiceId=${invoiceId}`,
    type: "GET",
    dataType: "json",
    success: data=>{
      switch(data.code){
        case 200:
          $('#textUAmount').val(data.response.payment.amount)
          $('#selectUType').val(data.response.payment.payment_type_id)
          $('#textUReferences').val(data.response.payment.references_number)
          $('#textUPaymentDate').val(data.response.payment.payment_date)
        break
        case 400:
            showErrorNotify(data.response.message)
        break
        case 404:
            showErrorNotify("Error pago no encontrado")
        break
        case 500:
            showErrorNotify("Error al obtener el pago")
        break
      }
    },
    error: function(request,msg,error) {
        console.log(error)
    }
  })
}

/*
 **************************************************************************************************
 Retorna un elemento option para que sea posible renderizarlo
 **************************************************************************************************
*/
function putPaymentType(paymentType){
  return `
  <option value="${ paymentType.payment_type_id }">${ paymentType.name }</option>
  `
}


function showErrorNotify(message){
  $('.pop-msj span').html(message);
  $('.shadow-notify').toggleClass('visible')
  $('.pop-msj').css({"animation":"notify-in","animation-duration":"0.25s", "display":"block"});
}

function showdialog(callback, message){
    $('.message-dialog-not').html(message);

    $('.box-confirmation-not').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"pop-in", "animation-duration":"0.25s"});
    $('.shadow-notify').css({"display":"block"});


    $('.yes-confirmation').bind("click", function(){
        callback();
        $('.box-confirmation-not').css({"display":"none"});
        $('.shadow-notify').css({"display":"none"});
        $('.yes-confirmation').unbind("click");
    });
    $('.no-confirmation').click(function(){
        $('.box-confirmation-not').css({"display":"none"});
        $('.shadow-notify').css({"display":"none"});
        $('.yes-confirmation').unbind("click");
    });
}
function shownotify(type, time = 3000){
    selectnotify(type, "notify-in");
    setTimeout(function(){
        selectnotify(type, "notify-out");
        setTimeout(function(){
            selectnotify(type, "notify-in", false);
            selectnotify(type, "notify-out", false);
        }, 370);
    }, time);
}
function selectnotify(type, className, isAdd=true){
    switch (type) {
        case 0:
            if(isAdd){
                $('.save-not').addClass(className);
            }else{
                $('.save-not').removeClass(className);
            }
            break;
        case 1:
            if(isAdd){
                $('.delete-not').addClass(className);
            }else{
                $('.delete-not').removeClass(className);
            }
            break;
        case 2:
            if(isAdd){
                $('.edit-not').addClass(className);
            }else{
                $('.edit-not').removeClass(className);
            }
            break;
        default:
        break;
    }
}

function showErrorNotify(message){
  $('.pop-msj span').html(message);
  $('.shadow-notify').toggleClass('visible')
  $('.pop-msj').css({"animation":"notify-in","animation-duration":"0.25s", "display":"block"});
}
function quitMessageAlert(){
    $('.shadow-notify').toggleClass('visible')
    $('.pop-msj').css({"animation":"pop-out","animation-duration":"0.25s", "display":"block"});
    setTimeout(function(){
        $('.pop-msj').css({"display":"none"});
    },220);
}

function validateAllDate(data){
  var isValid = true
  if(!validateDate(data)){
    isValid = false
  }else{
    var partDate = data.split("-")
    var year = parseInt(partDate[0])
    var month = parseInt(partDate[1]) - 1
    var day = parseInt(partDate[2])
    var dateObject = new Date(year, month, day)
    if(!((day==dateObject.getDate()) && (month==dateObject.getMonth()) && (year==dateObject.getFullYear()))){
      isValid = false
    }
  }
  return isValid
}

function validateDate(data){
  var regExp = /^[0-9]{4}\-[0-1]?[0-9]\-[0-3]?[0-9]$/
  return regExp.test(data)
}

function validatePositiveInteger(data){
  var regExp = /^[0-9]+$/
  return regExp.test(data)
}

function validatePositiveFloat(data){
  var regExp = /^[0-9]+(\.[0-9]+)?$/
  return regExp.test(data)
}

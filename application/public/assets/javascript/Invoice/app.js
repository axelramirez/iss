/*
 **************************************************************************************************
 *************************Name: Logic Invoice File HTML
 *************************Description: Mantiene la logica de la aplicacion para la vista Invoice
 **************************************************************************************************
*/
//Globals var
var optionst     =  0;
var serverURI    = "";
var searchFilter = {};
var sap = 0;
var isBack = false;
/*
 **************************************************************************************************
 Es el main del archivo
 **************************************************************************************************
*/
(()=>{
  var bodyElement =               $('body')
  var navigations =   $('.previous, .next')
  serverURI       = bodyElement.data("uri")
  $("body").on('click', '.button-options',  onOptions);

  //Back Event Detected, execute the function searchInvoice only if one page for pagination or execute the function backSearch only if one page for main
  window.onpopstate = (e)=>{
    isBack = true;
    if(e.state.isSearch){
      searchFilter = e.state.filter;
      $('.content-box').addClass("hide");
      searchInvoice();
    }else{
      backSearch();
    }
    window.onload = null
    // isBack = false;
  };

  window.onload = () => {
    // if(history.state.isSearch){
    //   searchFilter = history.state.filter;
    //   $('.content-box').addClass("hide");
    //   searchInvoice();
    // }else{
    //   backSearch();
    // }
    history.pushState({isSearch: false}, '', '');
  }

	$(".option-advanced").click(function(){
		if(sap == 0){
			$(".option-advanced p").css({"color":"rgb(255, 86, 86)"});
			$(".advanced-s").css({"display":"block","opacity":"1"});
			$(".section-date").css({"display":"flex","opacity":"1"});
			$(".text-search").text('Simple Search');
			sap = 1;
		}
		else{
			sap = 0;
			$(".option-advanced p").css({"color":"#40a3ff"});
			$(".advanced-s").css({"display":"none","opacity":"0"});
			$(".text-search").text('Advanced Search');
		}
	});
  navigations.click(onNavigation)
  getAllCustomers()
  $('#buttonSearch').click(()=>{
    $('.content-box').addClass("hide")
    var dateTo        =                                                    $('#dateTo').val()
    var dateFrom      =                                                  $('#dateFrom').val()
    var invoiceId     = ( $('#invoiceIdText').val() == "" ) ? "0" : $('#invoiceIdText').val()
    var pedimentNo    =   ( $('#pedimentText').val() == "" ) ? "0" : $('#pedimentText').val()
    var customerId    =                                                $('#customerid').val()
    var invoiceType   =                                               $('#invoiceType').val()
    var statusInvoice = {
      active: ( $('#statusActive:checked').val() === undefined ) ? 0 : 1,
      paid: ( $('#statusPaid:checked').val() === undefined ) ? 0 : 1,
      cancelled: ( $('#statusCancelled:checked').val() === undefined ) ? 0 : 1
    }
    searchFilter = {
      invoiceId,
      customerId,
      invoiceType,
      statusInvoice,
      pedimentNo,
      dateTo,
      dateFrom,
      numberPage: 1
    }
    searchInvoice()
  })
  $('#backButton').click(backSearch)
  $('body').on('click', '[data-invoice-cancel=0]', cancelInvoice)
  $('.box-shadow').click(function(){
    sdet = 0;
    det = 0;
    ec = 0;
    $('.add_service').hide();
    $('.applied-charges').show();
    $('.box-shadow').css({"display":"none"});
    $('.shipping-details').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
    $('.extra-chargues').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
    $('.receiving-details').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
    $('#buttonAddCharge').unbind("click");
  });

  $('.close-dialog-shadow').click(function(){
      sdet = 0;
      det = 0;
      ec = 0;
      $('.add_service').hide();
      $('.applied-charges').show();
      $('.box-shadow').css({"display":"none"});
      $('.shipping-details').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
      $('.extra-chargues').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
      $('.receiving-details').css({"animation":"pop-out","animation-duration":"0.25s","display":"none"});
      $('#buttonAddCharge').unbind("click");
  });

  $('.pop-msj span').html("")
  $('.close-pop-msj').click(function(){
      quitMessageAlert()
  });
  $('.confirm-pop').click(function(){
      quitMessageAlert()
  })
})()
/*
 **************************************************************************************************
 Se encarga de realizar una peticion ajax para realizar una busqueda de invoice
 **************************************************************************************************
*/
function backSearch(){
  if(!isBack){
    history.pushState({isSearch: false}, `Invoice Main Page`, '#backButton');
  }else{
    history.replaceState({isSearch: false}, `Invoice Main Page`, '#backButton');
  }
  $('.content-box').removeClass("hide")
  $('.content-result-box').addClass("hide")
}

function cancelInvoice(e){
  var buttonCancel = $(this)
  var autoNumber = buttonCancel.data("auto-number")
  var invoiceId = buttonCancel.data("id")
  showdialog(()=>{
    $.ajax({
      url: `${serverURI}index.php/invoice/cancel?invoiceId=${invoiceId}`,
      type: "DELETE",
      success: (data)=>{
        switch(data.code){
          case 200:
          shownotify(0)
          searchInvoice()
          break
          case 400:
          showErrorNotify(data.message)
          break
        }
      },
      error:(request, message, error)=>{
        console.log(request);
      }
    })
  }, `Are you sure Cancel invoice #${autoNumber}?`)
}
/*
 **************************************************************************************************
 Se encarga de realizar una peticion ajax para realizar una busqueda de invoice
 **************************************************************************************************
*/
function searchInvoice(){
  var tableInvoice         =                $('tbody')
  var contentNotFound      =           $('.not-found')
  var nextPageElement      =                $('.next')
  var resultPagination     =        $('.table-search')
  var numberPageElement    = $('[data-number-page=0]')
  var numberPagesElement   =       $('[data-pages=0]')
  var previousPageElement  =            $('.previous')
  var numberEntriesElement =     $('[data-entries=0]')
  var contentResultElement =  $('.content-result-box')
  openloading();
  tableInvoice.empty()
  contentNotFound.css({"display":"none"});
  optionst = 0;
  $('.options-table').css({"display":"none"});
  $.ajax({
    url: `${serverURI}index.php/invoice/search`,
    type: "GET",
    data: searchFilter,
    success: data=>{
      switch(data.code){
        case 200:
        tableInvoice.append(putInvoice({}, true))
        numberPageElement.html(searchFilter.numberPage)
        numberPagesElement.html(data.response.numberPages)
        numberEntriesElement.html(data.response.numberEntries)
        previousPageElement.data("value", data.response.previousPage)
        nextPageElement.data("value", data.response.nextPage)
        data.response.invoices.forEach(invoice=>{
          tableInvoice.append(putInvoice(invoice))
        })
        if( data.response.previousPage <= 0 ){
          previousPageElement.addClass("invisible")
        }else{
          previousPageElement.removeClass("invisible")
        }
        if( searchFilter.numberPage == data.response.numberPages ){
          nextPageElement.addClass("invisible")
        }else{
          nextPageElement.removeClass("invisible")
        }
        resultPagination.removeClass("hide")
        contentResultElement.removeClass("hide")
        break
        case 404:
            contentResultElement.removeClass("hide")
            contentNotFound.css({"display":"flex"});
            resultPagination.addClass("hide");
        break
        case 500:
        console.log("Error 500")
        break
      }
    },
    error: (request, msg, error) => {
        console.log(error)
    }
  })
  .done(()=>{
    if(!isBack){
      history.pushState({filter: searchFilter, isSearch: true}, `Invoice Search Page ${searchFilter.numberPage}`, `?${convertJsonToURL(searchFilter)}`);
    }else{
      history.replaceState({filter: searchFilter, isSearch: true}, `Invoice Search Page ${searchFilter.numberPage}`, `?${convertJsonToURL(searchFilter)}`);
    }
  })

}

function convertJsonToURL(json){
  var data = JSON.stringify(json);
  data = data.replace(/{/g, "");
  data = data.replace(/}/g, "");
  data = data.replace(/:/g, "=")
  data = data.replace(/,/g, "&");
  data = data.replace(/"/g, "");
  return data;
}

/*
 **************************************************************************************************
 Regresa un registro de una factura o las columnas de las facturas para posteriormente renderizarse en la vista
 **************************************************************************************************
*/
function putInvoice(invoice, first=false){
  if( first ){
    return `
    <tr>
      <td>
          Options
      </td>
      <td>
          Invoice #
      </td>
      <td>
          Status
      </td>
      <td>
          Customer
      </td>
      <td>
          Date
      </td>
      <td>
          Total Amount
      </td>
      <td>
          Paid Amount
      </td>
      <td>Type</td>
    </tr>`
  }
  return `
  <tr>
    <td class="button-options"><img src="${serverURI}public/assets/images/icons/more.svg">
      <div class="options-table">
        ${ putMenuInvoiceBy(invoice) }
      </div>
    </td>
    <td>
      ${invoice.autoinvoice_number}
    </td>
    <td>
      ${invoice.statusInvoice}
    </td>
    <td>
      ${invoice.name}
    </td>
    <td>
      ${invoice.dateInvoice}
    </td>
    <td>
      ${money_format(invoice.totalInvoice)}
    </td>
    <td>
      ${money_format(invoice.paidInvoice)}
    </td>
    <td>
      ${invoice.typeNameInvoice}
    </td>
  </tr>`
}


function putMenuInvoiceBy(invoice){
  var menuString = ""
  if(invoice.isCancelled == 1){
    menuString = `
    <div>
      <p>No tiene opciones.</p>
    </div>
    `
  }else if(invoice.isPaid == 1 || invoice.havePayments == 1){
    menuString = `
    <div>
      <a href="${serverURI}index.php/invoice/detail?invoiceId=${invoice.autoid}">
        <img src="${serverURI}public/assets/images/icons/view.svg">
        <p>View</p>
      </a>
    </div>
    ${menuOptionExcelBy(invoice.autoid, invoice.typeInvoice)}
    <div>
      <a href="${menuOptionPrintBy(invoice.autoid, invoice.typeInvoice)}" target="print">
        <img src="${serverURI}public/assets/images/icons/print.svg">
        <p>Print</p>
      </a>
    </div>
    `
  }else{
    menuString = `
    <div>
      <a href="${serverURI}index.php/invoice/detail?invoiceId=${invoice.autoid}">
        <img src="${serverURI}public/assets/images/icons/view.svg">
        <p>View</p>
      </a>
    </div>
    <div class="">
      <a href="${serverURI}index.php/invoice/edit?invoiceId=${invoice.autoid}">
        <img src="${serverURI}public/assets/images/icons/edit.svg">
        <p>Edit</p>
      </a>
    </div>
    ${menuOptionExcelBy(invoice.autoid, invoice.typeInvoice)}
    <div>
      <a href="${menuOptionPrintBy(invoice.autoid, invoice.typeInvoice)}" target="print">
        <img src="${serverURI}public/assets/images/icons/print.svg">
        <p>Print</p>
      </a>
    </div>
    <div data-invoice-cancel=0 data-id=${invoice.autoid} data-auto-number=${invoice.autoinvoice_number}>
      <img src="${serverURI}public/assets/images/icons/close.svg">
      <p>Cancel</p>
    </div>
    `
  }
  return menuString;
}

function menuOptionPrintBy(invoiceId, statusInvoice){
  var optionPrint = "#"
  switch(parseInt(statusInvoice)){
    case 0:
    optionPrint = `${serverURI}../pdfScripts/print_invoice.php?invoiceid=${invoiceId}`
    break
    case 1:
    optionPrint = `${serverURI}../pdfScripts/print_invoice_custom.php?invoiceid=${invoiceId}`
    break
    case 2:
    optionPrint = `${serverURI}../pdfScripts/print_invoice_storage.php?invoiceid=${invoiceId}`
    break
    case 3:
    optionPrint = `${serverURI}../pdfScripts/new_print_invoice.php?invoiceid=${invoiceId}`
    break
  }
  return optionPrint;
}

function menuOptionExcelBy(invoiceId, statusInvoice){
  var optionExcel = ""
  switch(parseInt(statusInvoice)){
    case 0:
    optionExcel = `
    <div>
      <a href="${serverURI}../excelScripts/invoice_to_csv.php?invoiceid=${invoiceId}" target="_blank">
        <img src="${serverURI}public/assets/images/icons/excel_black.svg">
        <p>Report</p>
      </a>
    </div>`
    break
    case 2:
    optionExcel = `
    <div>
      <a href="${serverURI}../excelScripts/invoiceStorage_to_csv.php?invoiceid=${invoiceId}" target="_blank">
        <img src="${serverURI}public/assets/images/icons/excel_black.svg">
        <p>Report</p>
      </a>
    </div>`
    break
  }
  return optionExcel;
}
/*
 **************************************************************************************************
 Se asigna al evento click de el boton de opciones de cada registro
 **************************************************************************************************
*/
function onOptions(){
  if(optionst == 0){
    $(this).find('.options-table').css({"display":"block"});
    optionst = 1;
  }
  else{
    optionst = 0;
    $('.options-table').css({"display":"none"});
  }
}

/*
 **************************************************************************************************
 Se asigna al evento click de los botones de navegacion de la paginacion
 **************************************************************************************************
*/
function onNavigation(){
  searchFilter.numberPage = $(this).data("value")
  searchInvoice()
}

/*
 **************************************************************************************************
 Se encarga de dar formato de dinero a un numero
 **************************************************************************************************
*/
function money_format(quantity_money){
  var quantity_final   =                                           ""
  var quantity_integer =                     parseInt(quantity_money)
  var quantity_decimal = quantity_money - quantity_integer
  var quantity_string  =                   quantity_integer.toString()
  var symbol = 0
  for( var c = quantity_string.length - 1; c >= 0; c-- ){
    if( symbol == 3 ){
      quantity_final += ","
      symbol = 0
    }
    symbol++
    quantity_final += quantity_string[c]
  }
  quantity_string = quantity_final
  quantity_final  = ""
  for( var c = quantity_string.length - 1; c >= 0; c-- ){
    quantity_final += quantity_string[c]
  }
  quantity_string = quantity_decimal.toFixed(2).toString().substring(1, 5)
  return `${quantity_final + quantity_string}`
}

/*
 **************************************************************************************************
 Obtiene un pago por su payment_number
 **************************************************************************************************
*/
function getAllCustomers(){
  var selectCustomer = $('#customerid')
  $.ajax({
    url: `${serverURI}index.php/customer/get`,
    type: "GET",
    dataType: "json",
    success: data=>{
      switch(data.code){
        case 200:
        data.response.customers.forEach((customer)=>{
          selectCustomer.append(putCustomer(customer))
        })
        break
        case 500:
        console.log("Error 500 al obter los tipos de pago")
        break
      }
    },
    error: function(request,msg,error) {
        console.log(error)
    }
  })
}
/*
 **************************************************************************************************
 Retorna un elemento option para que sea posible renderizarlo
 **************************************************************************************************
*/
function putCustomer(customer){
  return `
  <option value="${ customer.customerid }">${ customer.name }</option>
  `
}

function showdialog(callback, message){
    $('.message-dialog-not').html(message);

    $('.box-confirmation-not').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"pop-in", "animation-duration":"0.25s"});
    $('.shadow-notify').css({"display":"block"});


    $('.yes-confirmation').bind("click", function(){
        callback();
        $('.box-confirmation-not').css({"display":"none"});
        $('.shadow-notify').css({"display":"none"});
        $('.yes-confirmation').unbind("click");
    });
    $('.no-confirmation').click(function(){
        $('.box-confirmation-not').css({"display":"none"});
        $('.shadow-notify').css({"display":"none"});
        $('.yes-confirmation').unbind("click");
        statusAction = 0
    });
}
function shownotify(type, time = 3000){
    selectnotify(type, "notify-in");
    setTimeout(function(){
        selectnotify(type, "notify-out");
        setTimeout(function(){
            selectnotify(type, "notify-in", false);
            selectnotify(type, "notify-out", false);
        }, 370);
    }, time);
}
function selectnotify(type, className, isAdd=true){
    switch (type) {
        case 0:
            if(isAdd){
                $('.save-not').addClass(className);
            }else{
                $('.save-not').removeClass(className);
            }
            break;
        case 1:
            if(isAdd){
                $('.delete-not').addClass(className);
            }else{
                $('.delete-not').removeClass(className);
            }
            break;
        case 2:
            if(isAdd){
                $('.edit-not').addClass(className);
            }else{
                $('.edit-not').removeClass(className);
            }
            break;
        default:
        break;
    }
}

function showErrorNotify(message){
  $('.pop-msj span').html(message);
  $('.shadow-notify').toggleClass('visible')
  $('.pop-msj').css({"animation":"notify-in","animation-duration":"0.25s", "display":"block"});
}

function quitMessageAlert(){
    $('.shadow-notify').toggleClass('visible')
    $('.pop-msj').css({"animation":"pop-out","animation-duration":"0.25s", "display":"block"});
    setTimeout(function(){
        $('.pop-msj').css({"display":"none"});
    },220);
}

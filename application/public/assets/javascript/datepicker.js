$(function() {
    var pathname = window.location.pathname.split("/")[1];
    var pathnameLevel2 = window.location.pathname.split("/")[2];
    if(pathname != "preshipping_summary_edit.php"){
            if (pathnameLevel2 == "pediment_reports.php"){        
                $("#from").datepicker();
                $("#to").datepicker({
                    onSelect: function (selected) {                        
                        var setedDate = new Date(selected);
                        var cday = setedDate.getDate();
                        var cyear = setedDate.getFullYear();    
                        var cmonth = setedDate.getMonth()-12;
                        var before1year = new Date(cyear,cmonth,cday);          
                        $("#from").datepicker("option", "minDate", before1year);             
                    }
                });
            }
            else {
                var dates = $( "#from, #to" ).datepicker({
                    //showOn: "button",
                    //			buttonImage: "images/calendario.png",
                    //			buttonImageOnly: true,
                    //defaultDate: "+1w",
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1,
                    onSelect: function( selectedDate ) {
                        var option = this.id == "from" ? "minDate" : "maxDate",
                        instance = $( this ).data( "datepicker" ),
                        date = $.datepicker.parseDate(
                            instance.settings.dateFormat ||
                            $.datepicker._defaults.dateFormat,
                            selectedDate, instance.settings );
                        dates.not( this ).datepicker( "option", option, date );
                        $(this).change();
                    }
                });
            }
    }
});
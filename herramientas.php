<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <?php include('header.php'); ?>
    <link href="style/herramientas.css" rel="stylesheet" type="text/css" />
    <title>Clientes</title>
  </head>
  <body>
    <?php include('menu.php'); ?>
    <div class="box-shadow" hidden></div>
    <div class="box-confirmation-not">
        <span class="message-dialog-not"></span>
        <div>
            <input type="button" class="yes-confirmation" name="" value="Sí, estoy seguro">
            <input type="button" class="no-confirmation" name="" value="No">
        </div>
    </div>
    <div class="new-color-box" hidden>
      <div class="title-dialog">
        <h1>Crear nuevo Color/Material</h1>
        <img src="images/icons/close.svg" class="close-newcolor">
      </div>
      <form id="inserColor">
        <div class="inputs-color-info">
          <div>
            <p>Nombre del Color</p>
            <input id="color-name" type="text" name="" value="" required>
          </div>
          <div>
            <p>Descripción</p>
            <input id="color-description" type="text" name="" value="" required>
          </div>
        </div>
      </form>
      <input type="button" name="" class="saveColor save-button" value="Guardar">
    </div>
    <main>
      <h1>Herramientas</h1>
      <div class="options-settings">
        <div class="color-settings">
          <img src="images/icons/painter-palette.svg" alt="">
          <p>Administrar colores/material</p>
        </div>
      </div>
      <div id="section-colors">
        <div class="section-control-colors">
          <input type="button" class="createColor create-button" value="Crear nuevo color/material">
        </div>
        <table>
        </table>
      </div>
    </main>
  </body>
  <script src="js/herramientas/app.js"></script>
</html>

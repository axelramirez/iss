var numberpage = 1;
var flag = false;
var msg = "";
var objUser;


$(document).ready(function() {
    chargeTable();
   // objUser = new Template();


    $('div.box-add-unit').on('click', 'input', function(){
        $('#passSection').show();
        $('div.add-uom-box').css({"height":"50%"});
        $('.title-dialog h2').text('Create User');
        $('.username input').val('');
        $('.userlast input').val('');
        $('.login input').val('');
        $('.password input').val('');
        $('#confirm-password').val('');
        $('select.userrole').val("-1");

        $('.pass-user').css("background-color","white");

        $('.save-btn').val('Create');
        $('.save-btn').removeClass('update-user');
        $('.save-btn').addClass('insert-user');
        openUserBox(false);

    });

    /*document.getElementById('confirm-password').addEventListener('keyup',event =>{

        objUser.disableButton();
        changeState();

     });*/



     $('body').on('click', '#idEditUser img', function(){
        $('.title-dialog h2').text('Edit User');

        var id = $(this).parent().parent().attr('data-id');
        var name = $(this).parent().parent().find('.username').text();
        var last = $(this).parent().parent().find('.userlast').text();
        var login = $(this).parent().parent().find('.userlogin').text();
        var roleid  = $(this).parent().parent().find('.userrole').data('roleid');

        $('.save-btn').removeClass('insert-user');
        $('.save-btn').addClass('update-user');
        openUserBox(true);
        $('.username input').val(name);
        $('.userlast input').val(last);
        $('.login input').val(login);
        $('select.userrole').val(roleid);
        $('#passSection').hide();

        //getResult(id,$('.password input'));
        $('.update-uom').attr('data-id', id);
    });



    $('body').on('click', '#idEdit-Pass img', function(){
        $('.title-dialog h2').text('Edit User');
        var id = $(this).parent().parent().attr('data-id');
        editPassBox();
        $('#valedit-password').text("");
        $('#save-pass').attr('data-id',id);
        $('#edit-password').attr('data-id', id);
    });

    $('body').on('click','#save-pass',function(){
        var id = $(this).attr('data-id');
        updateResult(id,$('#new-password').val());

    });

    $('body').on('click', '.edit-user img', function(){
        $('.title-dialog h2').text('Edit User');
        var id = $(this).parent().parent().attr('data-id');
        var name = $(this).parent().parent().find('.username').text();
        var last = $(this).parent().parent().find('.userlast').text();
        var login = $(this).parent().parent().find('.userlogin').text();
        var roleid  = $(this).parent().parent().find('.userrole').data('roleid');
        openUserBox(true);
        $('.username input').val(name);
        $('.userlast input').val(last);
        $('.login input').val(login);
        $('select.userrole').val(roleid);
        $('#passSection').hide();
        $('div.add-uom-box').css({"height":"30%"});
        //getResult(id,$('.password input'));
        $('.update-uom').attr('data-id', id);
    });
    //Cierra el dialogo de service y limpia los inputs
    $('body').on('click', 'input.search-user', function(){
        getUser($('.form-inline .firstname').val(),$('.form-inline .lastname').val(),$('.form-inline .roleid').val());
    });

    $('body').on('click', '.close-uom', function(){
        closeUomBox();
    });

    $('body').on('click', '.close-pass', function(){
        closePassBox();
    });

    $('body').on('click', '.delete-user img', function(){
        var userid = $(this).parent().parent().attr('data-id');
        var login = $(this).parent().parent().find('.userlogin').text();

        showdialog(()=>{
            deleteUser(userid)
        }, 'Are you sure delete user ' + login + '?')
    });


    $('body').on('click', '.cancel-insert-uom', function(){
        closeUomBox();
    });
   $('body').on('click', '.insert-user', function(){

        insertUser();
    });


    $('body').on('click', '.update-user', function(){
        updateUser();
    });
    $('body').on('click', '.next-user-list', function(){
        numberpage++;
        getUser();
    });
    $('body').on('click', '.back-user-list', function(){
        if (numberpage > 1) {
            numberpage--;
            getUser();
        }
    });


});


function chargeTable(){
    getUser();
    setadd();

}

function setadd(){
    // call function validations
       // setEventChar('firstname',3,50);
       // setEventChar('lastname',3,50);
       // setEventCharNumber('login',5,50);
       // setEventCharNumber('password',8,50);

     /*   setMessage('firstname',"Accept minimum characters 3, first capital character");
        setMessage('lastname',"Accept minimum characters 3, first capital character");
        setMessage('login',"Accept minimum 5 characters");
        setMessage('password',"Accept minimum 8 characters");   */
    //
    $('div.add-uom-box').css({"display":"none"});
    $('div.add-uom-box').css({"position":"absolute"});
    $('div.add-uom-box').css({"left":"0"});
    $('div.add-uom-box').css({"right":"0"});
    $('div.add-uom-box').css({"top":"0"});
    $('div.add-uom-box').css({"bottom":"0"});
    $('div.add-uom-box').css({"margin":"auto"});
    $('div.add-uom-box').css({"width":"40%"});
    $('div.add-uom-box').css({"height":"50%"});
    $('div.add-uom-box').css({"background":"#fff"});
    $('div.add-uom-box').css({"z-index":"102"});
    $('div.add-uom-box').css({"padding":"25px"});
    $('div.add-uom-box').css({"border":"1px solid #e4e4e4"});

    $('div.edit-pass-box').css({"display":"none"});
    $('div.edit-pass-box').css({"position":"absolute"});
    $('div.edit-pass-box').css({"left":"0"});
    $('div.edit-pass-box').css({"right":"0"});
    $('div.edit-pass-box').css({"top":"0"});
    $('div.edit-pass-box').css({"bottom":"0"});
    $('div.edit-pass-box').css({"margin":"auto"});
    $('div.edit-pass-box').css({"width":"20%"});
    $('div.edit-pass-box').css({"height":"50%"});
    $('div.edit-pass-box').css({"background":"#fff"});
    $('div.edit-pass-box').css({"z-index":"102"});
    $('div.edit-pass-box').css({"padding":"25px"});
    $('div.edit-pass-box').css({"border":"1px solid #e4e4e4"});

    $('.form-inline input').css("vertical-align","middle");
    $('.form-inline input').css("margin","5px 10px 5px 0");
    $('.form-inline input').css("padding","3px");
    $('.form-inline input').css("background-color","#fff");
    $('.form-inline input').css("border","1px solid #ddd");

    $('.form-inline select').css("vertical-align","middle");
    $('.form-inline select').css("margin","5px 10px 5px 0");
    $('.form-inline select').css("padding","3px");
    $('.form-inline select').css("background-color","#fff");
    $('.form-inline select').css("border","1px solid #ddd");

    $('.form-inline').css (
        "display","flex",
        "flex-flow","row wrap",
        "align-items","center"
      );


    $('.form-inline').css("padding","1px 50px 1px 50px");



    $('div.col-20').css("float","left");
    $('div.col-20').css("height","20%");
    $('div.col-20').css("margin-top","20px");
    $('div.col-20').css("padding","3px 40px 3px 40px");



    $('div.col-40').css("float","left");
    $('div.col-40').css("height","40%");
    $('div.col-40').css("margin-top","20px");
    $('div.col-40').css("padding","3px 70px 3px 70px");


    $('div.col-10').css("float","left");
    $('div.col-10').css("width","10%");
    $('div.col-10').css("margin-top","20px");
    $('div.col-10').css("padding","3px 1px 3px 1px");


    $('div.row:after').css("content","");
    $('div.row:after').css("display","table");
    $('div.row:after').css("clear","both");

    $('.options-receipt').css("display","-webkit-flex");
    $('.options-receipt').css("display","-ms-flex");
    $('.options-receipt').css("display","-o-flex");
    $('.options-receipt').css("display","flex");
    $('.options-receipt').css("flex-direction","row");
    $('.options-receipt').css("justify-content","space-between");
    $('.options-receipt').css("align-items","center");
    $('.options-receipt').css("padding","10px 120px 10px 120px");
    $('.options-receipt').css("box-sizing","border-box");


    $('.entries-user').css(
        "width", "100%",
        "display","-webkit-flex",
        "display","-ms-flex",
        "display","-o-flex",
        "display", "flex",
        "flex-direction"," row",
        "justify-content","space-between",
        "align-items","center"
    );

    $('.total-entries').css(
        "display", "-webkit-flex",
        "display", "-ms-flex",
        "display","-o-flex",
        "display","flex",
        "flex-direction","row",
        "align-items","center"
    );

    $('.navigation').css(
        "display", "-webkit-flex",
        "display", "-ms-flex",
        "display","-o-flex",
        "display","flex",
        "flex-direction", "row",
        "align-items", "center"
    );

    $('.entries-user p').css(
        "font-family", "roboto-r",
        "font-size", "0.9em",
        "color", "#000"
    );

   $('.entries-user p:nth-child(2)').css(
        'padding','0px 5px 0px 5px',
        'box-sizing','border-box'
    );

    $('.back-user-list').css(
        "transform", "rotate(180deg)",
        "padding-left", "10px",
        "display", "none"
    );

    $(".next-user-list").css(
        "padding-left", "10px"
    );

    $(".entries-user img").css(
        "width", "30px",
        "height", "150px",
        "cursor", "pointer"
    );

    $('.add-unit').css(
        "background", "rgba(56, 193, 65, 1)",
        "color","#fff"
    );

    $('.cancel-btn').css(
        "background","#ff7070",
        "color","#fff"
    );

    $('.save-btn').css(
        "background", "#3197ff",
        "color", "#fff"
    );

    $(".loading-catalog").css(
        "position", "absolute",
        "top", "75px",
        "z-index", "1",
        "width","100%",
        "height", "calc(100% - 75px)",
        "background", "#fff",
        "display", "none",
        "justify-content", "center",
        "align-items", "center"
    );

    ///objUser.disableButton();

}

function getForm(idForm){
  var form = "";
  if(idForm == 0)
  form =       `
        <div class="form-inline">
        <div class="row">
            <div class="col-20 password">
                <Label>Password</Label>
                <input id="firstname" type="text" required>
                <div class="data-person"><span id="valfirstname"></span></div>
            </div>
            <div class="col-20 new-password">
                <Label>New Password</Label>
                <input id="lastname" type="text" required>
                <div class="data-person"><span id="vallastname"></span></div>
            </div>
            <div class="col-20 confirm-password">
                <Label>Confirm Password</Label>
                <input id="login" type="text" required>
                <div class="data-person"><span id="vallogin"></span></div>
            </div>
        </div>

        <div class="row">
            <div class="col-40">
                <Label>Role</Label>
                <select name="role" id="frmrole"  class="userrole" onChange="validateSelect()"/>
                <input id="save-user"type="button" class="button save-btn update-uom" value="Update">
                <input type="button" class="button cancel-btn cancel-insert-uom" value="Cancel">
            </div>
        </div>
    `;

   if(idForm == 1)
   form =  `

        <div class="form-inline">
            <div class="row">
                <div class="col-20 username">
                    <Label>First Name</Label>
                    <input id="firstname" type="text" required>
                    <div class="data-person"><span id="valfirstname"></span></div>
                </div>
                <div class="col-20 userlast">
                    <Label>Last Name</Label>
                    <input id="lastname" type="text" required>
                    <div class="data-person"><span id="vallastname"></span></div>
                </div>
                <div class="col-20 login">
                    <Label>Login</Label>
                    <input id="login" type="text" required>
                    <div class="data-person"><span id="vallogin"></span></div>
                </div>
            </div>
            <div class="row">

                <div class="col-20 password">
                    <Label>Password</Label>
                    <input id="password" type="text" required max="8">
                    <div class="data-person"><span id="valpassword"></span></div>
                </div>

                <div class="col-20 confim-password">
                    <Label>Confirm Password</Label>
                    <input id="confirm-password" type="text" required max="8">
                </div>
                <div class="col-20">
                    <div class="pass-user"><span id="valpassword-confirm"></span></div>
                </div>

            </div>


            <div class="row">
                <div class="col-40">
                    <Label>Role</Label>
                    <select name="role" id="frmrole"  class="userrole" onChange="validateSelect()"/>
                    <input id="save-user"type="button" class="button save-btn update-uom" value="Update">
                    <input type="button" class="button cancel-btn cancel-insert-uom" value="Cancel">
                </div>
            </div>
        </div> `;

        openUserBox(form);
}

function openUserBox(idEdit){

    if(idEdit == true){
        $('#firstname').attr("onkeyup","eventChar('firstname',3,50,3)");
        $('#lastname').attr("onkeyup","eventChar('lastname',3,50,3)");
        $('#login').attr("onkeyup","eventChar('login',3,50,3)");
        $('#frmrole').attr("onchange","validateSelect(3)");
        disableButton(3);
    }
    else{

        $('#firstname').attr("onkeyup","eventChar('firstname',3,50)");
        $('#lastname').attr("onkeyup","eventChar('lastname',3,50)");
        $('#login').attr("onkeyup","eventChar('login',3,50)");
        $('#frmrole').attr("onchange","validateSelect(true)");
        disableButton(true);
        setMessage('firstname',"Accept minimum characters 3");
        setMessage('lastname',"Accept minimum characters 3");
        setMessage('login',"Accept minimum 5 characters");
        setMessage('password',"Accept minimum 8 characters");

    }

    $('.box-shadow').css({"display":"block"});
    $('.add-uom-box').css({"display":"block"});
    $('.add-uom-box').css({"animation":"pop-in","animation-duration":"0.25s"});


    //disableButton();
}

function editPassBox(){

    $('.box-shadow').css({"display":"block"});
    $('.edit-pass-box').css({"display":"block"});
    $('.edit-pass-box').css({"animation":"pop-in","animation-duration":"0.25s"});
    document.getElementById("save-pass").disabled = true;
    $('.save-btn').css("background", "#878f99");
    /*setMessage('firstname',"Accept minimum characters 3");
    setMessage('lastname',"Accept minimum characters 3");
    setMessage('login',"Accept minimum 5 characters");
    setMessage('password',"Accept minimum 8 characters");
    disableButton();*/
}

///Cierra el contenedor para insertar/actualizar uom
function closeUomBox(){
    $('.box-shadow').css({"display":"none"});
    $('.add-uom-box').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.add-uom-box').css({"display":"none"});
    },250);
    resetClass();
}



function closePassBox(){
    $('.box-shadow').css({"display":"none"});
    $('.edit-pass-box').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.edit-pass-box').css({"display":"none"});
    },250);

}
//Resetea las clases del contenedor insertar/actualizar uom
function resetClass(){
    $('.save-btn').removeClass('insert-uom');
    $('.save-btn').addClass('update-uom');
}

function putUser(user, index, first = false){

    var roleName = $('.field_input option[value=' + user.roleid + ']').text()
    if (first) {
        return `
                <tr>
                    <td>Edit Pass</td>
                    <td>Edit</td>
                    <td>Delete</td>
                    <td>Role</td>
                    <td>First Name</td>
                    <td>Last Name</td>
                    <td>Login</td>

                </tr>
        `;
    }
    else {
        return `
                <tr id="user_${user.userid}" data-id="${user.userid}">
                    <td id="idEdit-Pass">
                        <img  src="images/icons/edit.svg" alt="Edit-Pass">
                    </td>
                    <td class="edit-user">
                        <img src="images/icons/edit.svg" alt="Edit">
                    </td>
                    <td class="delete-user">
                        <img src="images/icons/delete.svg" alt="Delete">
                    </td>
                    <td class="userrole" data-roleid=${user.roleid}>${roleName}</td>
                    <td class="username">${user.firstname}</td>
                    <td class="userlast">${user.lastname}</td>
                    <td class="userlogin">${user.login}</td>
                </tr>
        `;
    }
}


function insertUser(){
    $.ajax({
      url: '/ISS/application/index.php/user/insert',
      type: 'PUT',
      dataType: 'json',
      data: {
          firstname: $('.username input').val(),
          lastname: $('.userlast input').val(),
          login: $('.login input').val(),
          password: $('.password input').val(),
          roleid: parseInt($('select.userrole').val())
      },
      success: (data)=>{
          getUser();
          closeUomBox();
          shownotify(0);
      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}

function updateUser(){
    $.ajax({
        url: '/ISS/application/index.php/user/updateUser',
        type: 'PUT',
        dataType: 'json',
        data: {
            userid: parseInt($('.update-user').data('id')),
            firstname: $('.username input').val(),
            lastname: $('.userlast input').val(),
            login: $('.login input').val(),
            roleid: parseInt($('select.userrole').val())
        },
        success: (data)=>{
            getUser();
            closeUomBox();
            shownotify(0);
        },
        error: (req, msg, error)=>{
          console.log(req);
          console.log(req.responseText);
        }
      })
}

function deleteUser(userid){
    $.ajax({
      url: '/ISS/application/index.php/user/delete',
      type: 'DELETE',
      dataType: 'json',
      data: {
          userid: userid
      },
      success: (data)=>{
          switch (data.code) {
              case 200:
                  getUser();
                  closeLocationBox();
                  shownotify(1);
                  break;
              case 400:
                console.log("Error 400");
                break;
            case 500:
              console.log("Error 500");
              break;
              default:
          }

      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}


function validateLogin($login){
    msg = "";

    $.ajax({
        url: `/ISS/application/index.php/user/checkLogin`,
        type: "GET",
        dataType: 'json',
        data: {
            login: $login
        },
        async: false,
        success: data=>{
            switch (data.code) {
                case 200:
                    if(data.response.result){
                        msg = 'already exists';
                    }
                    else{
                        msg = 'can use it';
                    }
                    break;
                case 404:
                    console.log(data);
                    break;
                default:
            }
        }, error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{
        loadingcatalogs();
    });

}

function validatePassword($userid,$pass){
    msg = "";

    $.ajax({
        url: `/ISS/application/index.php/user/getResult`,
        type: "GET",
        dataType: 'json',
        data: {
            userid: $userid,
            pass: $pass
        },
        async: false,
        success: data=>{
            switch (data.code) {
                case 200:
                    if(data.response.result){
                        msg = 'correct password';
                    }
                    else{
                        msg = 'incorrect pass';
                    }
                    break;
                case 404:
                    console.log(data);
                    break;
                default:
            }
        }, error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{
        loadingcatalogs();
    });

}

function updateResult(userid,newpass){
    $.ajax({
      url: '/ISS/application/index.php/user/updateResult',
      type: 'PUT',
      dataType: 'json',
      data: {
        userID: userid,
        pass:newpass
      },
      success: data=>{
          getUser();
          closePassBox();
          shownotify(0);
      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}

function getUser(firstname="",lastname="",roleid="-1"){
    var tableUser = $('.tableUser');
    tableUser.empty();
    $.ajax({
        url: `/ISS/application/index.php/user/get`,
        type: "GET",
        dataType: 'json',
        data: {
            page: numberpage,
            firstname: firstname,
            lastname: lastname,
            roleid: roleid
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    tableUser.append(putUser({}, 0, true));
                    data.response.user.forEach((user)=>{
                        tableUser.append(putUser(user));

                    });
                    var entries = data.response.numberEntries;
                    var pages = data.response.numberPages;
                    $('.entries-user .entries').html(entries + ' entries');
                    $('.entries-user .pages').html(pages + ' Page(s)');
                    $('.actual-page p').html('Page ' + numberpage);
                    //Ocultar y mostrar flechas de navegacion
                    if (numberpage == pages) {$('.next-user-list').hide();}
                    else {$('.next-user-list').show();}
                    if (numberpage > 1) {$('.back-user-list').show();}
                    else {$('.back-user-list').hide();}
                    break;
                case 404:
                    console.log(data);
                    break;
                default:
            }
        }, error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{
        loadingcatalogs();
    });
}

function validate(value) {
    var re =/^[a-zA-Z]+$/;
    return re.test(value);
  }

  function validateM(value) {
    var re =/^[A-Z]+$/;
    return re.test(value);
  }

  function validateCN(value) {
    var re =/^[a-zA-Z0-9]+$/;
    return re.test(value);
  }


 function getFlagValidation(idInput,min,max){
    var value = $('#' + idInput).val();


        if(idInput == 'firstname' || idInput == "lastname")
            return (validate(value) && validateM(value[0]) && (value.length >= min && value.length <= max));

        if(idInput == 'login' || idInput == 'password'  || idInput == 'new-password')
            return (validateCN(value) && validate(value[0]) && (value.length >= min && value.length <= max));
        if(idInput == 'edit-password'  )
            return validate(value[0]);

        return false;
 }

 function setMessage(idInput,msg){
    $('#val' + idInput).text(msg);
    $('.data-person').css("background-color","hsl(0, 0%, 71%)");
    $('.data-person').css("color", "white");
    $('.data-person').width($('#' + idInput).width());
    $('.data-person').css("border","none");
    $('.data-person').css("border-radius","50px");
    $('.data-person').css("text-align","center");
    $('#val' + idInput).css("font-size","15px");

 }

 function validateColor(idInput,min,max,insert=true){


    if(getFlagValidation(idInput,min,max)){
        $('#' + idInput).css("background-color","white");
        $('#' + idInput).css("color","black");
        $('#val' + idInput).text("");

        if(idInput == 'edit-password'){
            if($('#edit-password').val() != ''){
                validatePassword($('#edit-password').data('id'),$('#edit-password').val());
                setMessage(idInput,msg);
            }
        }

        if(idInput == 'new-password'){
            changeState(insert);
        }

        if(idInput == 'login'){
            validateLogin($('.login input').val());
            setMessage('login',msg);
        }

    }
    else{
        $('#' + idInput).css("background-color","hsl(0, 100%, 75%)");
        $('#' + idInput).css("color","white");

        if(idInput == 'firstname' || idInput == "lastname")
             setMessage(idInput,"Accept minimum 3 characters");

        if(idInput == 'login')
             setMessage(idInput,"Accept minimum 5 characters");
        if(idInput == 'password'  || idInput == 'new-password')
             setMessage(idInput,"Accept minimum 8 characters");
    }

    disableButton(insert);
 }


 function setButton(activeButton,idButton){
     console.log(activeButton);
    if(activeButton){
        // This part is for when the message is empty, activate create button

        document.getElementById(idButton).disabled = false;
        $('.save-btn').css("background", "#3197ff");
    }
    else{
        document.getElementById(idButton).disabled = true;
        $('.save-btn').css("background", "#878f99");
    }

 }

 function disableButton(insert){

    if(insert == true)
        setButton(flagText($('#valfirstname').text())  &&  flagText($('#vallastname').text()) && flagText($('#vallogin').text(),'can use it')  && flagText($('#valpassword').text()) && $('#password').val() == $('#confirm-password').val() && $('#frmrole').val() != "-1",'save-user');


    if(insert == false)
        setButton(flagText($('#valedit-password').text(),"correct password")  &&  flagText($('#valnew-password').text()) && flagText($('#valeditconfirm-password').text(),'Pass Verified'),'save-pass');

    if(insert == 3){
        setButton(flagText($('#valfirstname').text())  &&  flagText($('#vallastname').text()) && flagText($('#vallogin').text(),'can use it')  && $('#frmrole').val() != "-1",'save-user');
    }

 }



 function flagText(text,comp=""){
    return text == comp;
 }

 function validateSelect(event=false){
        disableButton(event);
 }

 function changeState(insert=true){
     if(insert == true){
        if($('#password').val() == $('#confirm-password').val()){
            $('#valpassword-confirm').text('Pass Verified');
            $('.pass-user').css("background-color","hsl(0, 0%, 71%)");
            $('.pass-user').css("color", "white");
            $('.pass-user').css("border-radius","50px");
            $('.pass-user').css("text-align","center");
        }
        else
            $('.pass-user').css("background-color","white");
    }
    else{
        if($('#new-password').val() == $('#editconfirm-password').val()){
            $('#valeditconfirm-password').text('Pass Verified');
            $('.editpass-user').css("background-color","hsl(0, 0%, 71%)");
            $('.editpass-user').css("color", "white");
            $('.editpass-user').css("border-radius","50px");
            $('.editpass-user').css("text-align","center");
        }
        else{
            $('.editpass-user').css("background-color","white");
            $('#valeditconfirm-password').text('');
        }
    }
 }
    /*document.getElementById('confirm-password').addEventListener('keyup',event =>{

        objUser.disableButton();
        changeState();

     });*/



    function confirmPass(insert=true){

        changeState(insert);
        disableButton(insert);
    }



 function eventChar(idInput,min,max,insert=true){
    //document.getElementById(idInput).addEventListener('keyup',event =>{
        validateColor(idInput,min,max,insert);

    //});
}

function eventChar(idInput,min,max){
    //document.getElementById(idInput).addEventListener('keyup',event =>{
        validateColor(idInput,min,max);

    //});
}

function eventCharNumber(idInput,min,max,insert=true){
    //document.getElementById(idInput).addEventListener('keyup',event =>{
        validateColor(idInput,min,max,insert);

    //});
}


class Template{



    constructor(){
        this.setWorkTemplate();
    }

    setWorkTemplate(){

        var wkTemplate = new Worker('js/users/pass.js');
        wkTemplate.postMessage("hello");

        wkTemplate.onmessage = function(e){
            //console.log(e.data);
            $('#newuser').html(e.data);
        }


    }


}

var numberpage = 1;
var colorsArray = Array;
$(function() {
  $('body').on('click', '.color-settings', function(){
    $('.options-settings').hide();
    $('#section-colors').show();
    getColors();
  });
  $('body').on('click', '.closeInformationItem', function(){
    $('.box-shadow').hide();
    $('.info-product-box').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.info-product-box').css({"display":"none"});
    },250);
  });
  $('body').on('click', '.options-item img', function(){
    $('.box-shadow').show();
    $(this).parent().find('.options-menu-item').css({"display":"flex"});
    $(this).parent().find('.options-menu-item').css({"animation":"pop-in","animation-duration":"0.25s"});
  });
  $('body').on('click', '.box-shadow', function(){
    $('.box-shadow').hide();
    $('.options-menu-item').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.options-menu-item').css({"display":"none"});
    },250);
    closeBox();
    $('.box-confirmation-not').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.box-confirmation-not').css({"display":"none"});
    },250);
  });
  $('body').on('click', '.close-newcolor', function(){
    closeBox();
  })
  $('body').on('click', '.edit-color', function(){
    $('.options-menu-item').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.options-menu-item').css({"display":"none"});
    },250);
    $('.saveColor').attr('data-mode', 'update');
    var id = parseInt($(this).parent().parent().parent().attr('data-id'));
    var name = $(this).parent().parent().parent().attr('data-name');
    var desription = $(this).parent().parent().parent().attr('data-description');
    $('.saveColor').attr('data-id', id);
    $('#color-name').val(name);
    $('#color-description').val(desription);
    $('.new-color-box .title-dialog h1').html('Editar Informacion Color/Material');

    openBox();
  });
  $('body').on('click', '.createColor', function(){
    $('.new-color-box .title-dialog h1').html('Crear nuevo Color/Material');
    $('.saveColor').attr('data-mode', 'insert');
    openBox();
  });
  $('body').on('click', '.saveColor', function(e){
    var mode = $(this).attr('data-mode');
    var colorId = $(this).attr('data-id');
    var inpObj = document.getElementById('inserColor');
    if (!inpObj.checkValidity()) {
      var name = $('#color-name').val();
      var description = $('#color-description').val();
      if (name == '') {
        $('#color-name').addClass('input-error');
        $('#color-name').attr('placeholder', 'Llena este campo');
        $('#color-name').focus();
      }else {
        $('#color-name').removeClass('input-error');
        $('#color-name').removeAttr('placeholder');
      }
      if (description == '') {
        $('#color-description').addClass('input-error');
        $('#color-description').attr('placeholder', 'Llena este campo');
        $('#color-description').focus();
      }else {
        $('#color-description').removeClass('input-error');
        $('#color-description').removeAttr('placeholder');
      }
    }else {
      switch (mode) {
        case 'update':
          updateColors(colorId);
          break;
        case 'insert':
          insertColor();
          break;
        default:

      }
    }
  });
  $('body').on('click', '.delete-color', function(){
    var id = $(this).attr('data-id');
    var name = $(this).attr('data-name');
    $('.options-menu-item').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.options-menu-item').css({"display":"none"});
    },250);
    showdialog(()=>{
      deleteColor(id);
    }, '¿Quieres eliminar el color ' + name + '?');
  });
});
function deleteColor(id){
  $.ajax({
    url: '/ISS/application/index.php/ISS/delete/color',
    type: 'DELETE',
    dataType: 'json',
    data: {
        idColor: id
    },
    success: (data)=>{
        switch (data.code) {
            case 200:
                getColors();
                break;
            case 400:
              console.log("Error 400");
              break;
          case 500:
            console.log("Error 500");
            break;
            default:
        }

    },
    error: (req, msg, error)=>{
      console.log(req);
      console.log(req.responseText);
    }
  })
}
/*****************************************************************************Dialogo de confirmacion********************************************/
function showdialog(callback, message){
    $('.message-dialog-not').html(message);
    $('.box-confirmation-not').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"pop-in", "animation-duration":"0.25s"});

    $('.yes-confirmation').bind("click", function(){
        callback();
				$('.box-confirmation-not').css({"animation":"pop-out","animation-duration":"0.25s"});
		    setTimeout(function(){
		        $('.box-confirmation-not').css({"display":"none"});
		    },250);
        $('.box-shadow').hide();
        $('.yes-confirmation').unbind("click");
    });
    $('.no-confirmation').click(function(){
				$('.box-confirmation-not').css({"animation":"pop-out","animation-duration":"0.25s"});
		    setTimeout(function(){
		        $('.box-confirmation-not').css({"display":"none"});
		    },250);
        $('.box-shadow').hide();
        $('.yes-confirmation').unbind("click");
    });
}
function closeBox(){
  $('.box-shadow').hide();
  $('.new-color-box').css({"animation":"pop-out","animation-duration":"0.25s"});
  setTimeout(function(){
      $('.new-color-box').css({"display":"none"});
  },250);
  $('#color-name').val('');
  $('#color-description').val('');
  $('#color-name').removeClass('input-error');
  $('#color-description').removeClass('input-error');
  $('#color-name').removeAttr('placeholder');
  $('#color-description').removeAttr('placeholder');

}
function openBox(){
  $('.box-shadow').show();
  $('.new-color-box').css({"display":"block"});
  $('.new-color-box').css({"animation":"pop-in","animation-duration":"0.25s"});
  $('#color-name').focus();
}
function insertColor(){
  var name = $('#color-name').val();
  var description = $('#color-description').val();
  var dataInsert = (
      updateColor = {
        colorName: name,
        colorDescription: description
  });
  $.ajax({
    url: '/ISS/application/index.php/ISS/insert/color',
    type: 'PUT',
    dataType: 'json',
    data: {
        insertColors: dataInsert
    },
    success: (data)=>{
      console.log(data);
      switch (data.code) {
        case 200:
          getColors();
          closeBox();
          break;
        case 500:
          break;
        default:

      }
    },
    error: (req, msg, error)=>{
      console.log(req);
      console.log(req.responseText);
    }
  })
}
function updateColors(colorId){
  var name = $('#color-name').val();
  var description = $('#color-description').val();
  var dataInsert = (
      updateColor = {
        colorName: name,
        colorDescription: description
  });
  $.ajax({
    url: '/ISS/application/index.php/ISS/update/color',
    type: 'PUT',
    dataType: 'json',
    data: {
        updateColor: dataInsert,
        colorId: colorId
    },
    success: (data)=>{
      console.log(data);
      switch (data.code) {
        case 200:
          getColors();
          closeBox();
          break;
        case 500:
          break;
        default:

      }
    },
    error: (req, msg, error)=>{
      console.log(req);
      console.log(req.responseText);
    }
  })
}
//Escribe los productos dentro de la tabla
function putColors(color, index, first = false){
    if (first) {
      return `
        <tr>
          <td>Opciones</td>
          <td>Color</td>
          <td>Descripcion</td>
        </tr>
      `;
    }
    else {
      return `
          <tr id="color_${color.id}" class="item-color" data-id="${color.id}" data-name="${color.name}" data-description="${color.description}">
              <td class="options-item">
                <img src="images/icons/options.svg">
                <div class="options-menu-item">
                  <div class="edit-color">
                    <img src="images/icons/edit.svg"/>
                    <p>Editar</p>
                  </div>
                  <div class="delete-color" data-id="${color.id}" data-name="${color.name}">
                    <img src="images/icons/delete.svg"/>
                    <p>Eliminar</p>
                  </div>
                </div>
              </td>
              <td class="name">
                <p>${color.name}</p>
              </td>
              <td class="description">
                <p>${color.description}</p>
              </td>
          </tr>
      `;
    }
}
//Obtiene los colores/material
function getColors(){
    var tableColors = $('#section-colors table');
    tableColors.empty();
    $.ajax({
        url: `/ISS/application/index.php/ISS/get/colors`,
        type: "GET",
        dataType: 'json',
        success: data=>{
            switch (data.code) {
                case 200:
                    console.log(data);
                    tableColors.append(putColors({}, 0, true));
                    data.response.colors.forEach((color)=>{
                        tableColors.append(putColors(color));
                    });
                    break;
                case 404:
                    console.log(data);
                    break;
                default:
            }
        }, error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{
    });
}

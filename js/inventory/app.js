var numberpage = 1;
var colorsArray = Array;
var productsArray = [];
var tornillosArray = [];
var setsArray = [];
$(function() {
  $('body').on('click', '.inventory-control', function(){
    $('.menu-options').hide();
    $('#inventory').show();
    getProducts();
  });
  $('body').on('click', '.search-inventory', function(){
    $('.menu-options').hide();
    $('#search-inventory').show();
    var filter = '';
    var section = 'search';
    getProductByFilter(filter, section);
  });
  $('body').on('click', '.searchItem', function(){
    var section = 'search';
    var filter = $('.filterInput').val();
    if (filter != '') {
      $('.search-section-box').append(`<p class="search-all">Mostrar todo</p>`);
      getProductByFilter(filter, section);
    }
  });
  $('body').on('click', '.search-all', function(){
    $('.search-all').remove();
    $('.filterInput').val('');
    var section = 'search';
    var filter = $('.filterInput').val();
    getProductByFilter(filter, section);
  });
  $('body').on('click', '.receipt-inventory', function(){
    var filter = '';
    var section = 'receipt';
    $('.menu-options').hide();
    $('#receipt-inventory').show();
    getProductByFilter(filter, section);
  });
  $('body').on('click', '.shipping-inventory', function(){
    var filter = '';
    var section = 'shipping';
    $('.menu-options').hide();
    $('#shipping-inventory').show();
    getProductByFilter(filter, section);
  });
  $('body').on('click', '.closeInformationItem', function(){
    $('.box-shadow').hide();
    $('.info-product-box').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.info-product-box').css({"display":"none"});
    },250);
  });
  $('body').on('click', '.box-shadow', function(){
    $('.box-shadow').hide();
    $('.capture-shipping').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.capture-shipping').css({"display":"none"});
    },250);
    $('.capture-receipt').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.capture-receipt').css({"display":"none"});
    },250);
    $('.info-product-box').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.info-product-box').css({"display":"none"});
    },250);
    $('.options-menu-item').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.options-menu-item').css({"display":"none"});
    },250);
    closeBox();
    closeBoxSet();
    $('.box-confirmation-not').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.box-confirmation-not').css({"display":"none"});
    },250);
  });
  $('body').on('click', '.options-item img', function(){
    $('.box-shadow').show();
    $(this).parent().find('.options-menu-item').css({"display":"flex"});
    $(this).parent().find('.options-menu-item').css({"animation":"pop-in","animation-duration":"0.25s"});
  });
  $('body').on('click', '.close-newproduct', function(){
    closeBox();
  })
  $('body').on('click', '.close-newset', function(){
    closeBoxSet();
  })
  $('body').on('click', '.edit-product', function(){
    $('.options-menu-item').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.options-menu-item').css({"display":"none"});
    },250);
    $('.saveProduct').attr('data-mode', 'update');
    var id = parseInt($(this).parent().parent().parent().attr('data-id'));
    var name = $(this).parent().parent().parent().attr('data-name');
    var image = $(this).parent().parent().parent().attr('data-image');
    var desription = $(this).parent().parent().parent().attr('data-description');
    var quantity = $(this).parent().parent().parent().attr('data-quantity');
    var color = $(this).parent().parent().parent().attr('data-color');
    $('.saveProduct').attr('data-id', id);
    $('#product-name').val(name);
    $('#product-image').val(image);
    $('#product-description').val(desription);
    $('#product-quantity').val(quantity);
    $('.new-product-box .title-dialog h1').html('Editar Informacion');

    openBox();
    var selectColor = $('#product-color');
    selectColor.empty();
    for (var i = 0; i < colorsArray.length; i++) {
      selectColor.append(`
        <option value="${colorsArray[i].id}">${colorsArray[i].name} - (${colorsArray[i].description})</option>
        `);
    }
    selectColor.val(color);
  });
  $('body').on('click', '.createProduct', function(){
    $('.new-product-box .title-dialog h1').html('Crear nuevo Producto');
    $('.saveProduct').attr('data-mode', 'insert');
    openBox();

    var selectColor = $('#product-color');
    selectColor.empty();
    for (var i = 0; i < colorsArray.length; i++) {
      selectColor.append(`
        <option value="${colorsArray[i].id}">${colorsArray[i].name} - (${colorsArray[i].description})</option>
        `);
    }
  });
  $('body').on('click', '.createSet', function(){
    $('.new-set-box .title-dialog h1').html('Crear nuevo Set');
    $('.saveSet').attr('data-mode', 'insert');
    openBoxSet();

    var selectTornillo = $('#set-bolt');
    selectTornillo.empty();
    for (var i = 0; i < tornillosArray.length; i++) {
      selectTornillo.append(`
        <option data-mm="${tornillosArray[i].description}" value="${tornillosArray[i].id}">${tornillosArray[i].name} - (${tornillosArray[i].description})</option>
        `);
    }
  });
  $('body').on('click', '.openItemsDB', function(){
    $('#insertSetSection').css({"display":"none"});
    $('.saveProduct').css({"display":"none"});
    $('.set-items-box').show();
    $('#showItemsToSet').empty();
    for (var i = 0; i < productsArray.length; i++) {
      $('#showItemsToSet').append(`
          <div class="itemSet" data-id="${productsArray[i].id}" data-name="${productsArray[i].name}" data-img="${productsArray[i].image}" data-selected="0">
            <img src="${productsArray[i].image}"/>
            <div>
              <p>${productsArray[i].name}</p>
              <input type="number" class="qtyItemSet" value="" hidden/>
            </div>
          </div>
        `);
    }
  });
  $('body').on('click', '.itemSet', function(){
    var beforeSelected = $(this).attr('data-selected');
    if (beforeSelected == 1) {
      $(this).removeClass('selectedItemSet');
      $(this).attr('data-selected', 0);
      $(this).find('input').css({"display":"none"});
      $(this).find('input').val('');
      $(this).find('img').removeClass('imageResizeSet');
    }else {
      $(this).addClass('selectedItemSet');
      $(this).attr('data-selected', 1);
      $(this).find('input').show();
      $(this).find('input').focus();
      $(this).find('img').addClass('imageResizeSet');
    }
  });
  $('body').on('focusout', '.qtyItemSet', function(){
    var value = $(this).val();
    if (value == "" || value == 0 || value == undefined) {
      $(this).val(1);
    }
  });
  $('body').on('click', '.insertSelectItems', function(){
    var items;
    var ids;
    $('#showItemsToSet .itemSet').map(function(i){
        var itemSelected = $(this).attr('data-selected');
        if (itemSelected == 1) {
          var qty = $(this).find('.qtyItemSet').val();
          var nameItem = $(this).data('name');
          var idItem = $(this).data('id');
          if (items == "" || items == undefined) {
            items = nameItem;
            ids = idItem;
            if (qty > 1) {
              for (var i = 1; i < qty; i++) {
                items += "," + nameItem;
                ids += "," + idItem;
              }
            }
          }else {
            items += "," + nameItem;
            ids += "," + idItem;
          }
        }
    });
    $('#insertSetSection').show();
    $('.saveProduct').show();
    $('.set-items-box').css({"display":"none"});
    $('.productsSet').show();
    $('.productsSet').attr('data-id', ids);
    $('.productsSet').val(items);
  });
  $('body').on('click', '.back-set-box', function(){
    $('#insertSetSection').show();
    $('.saveProduct').show();
    $('.set-items-box').css({"display":"none"});
  });
  //Insert Set
  $('body').on('click', '.saveSet', function(e){
    var mode = $(this).attr('data-mode');
    var setId = $(this).attr('data-id');
    var inpObj = document.getElementById('insertSetSection');
    if (!inpObj.checkValidity()) {
      var name = $('#set-name').val();
      var description = $('#set-description').val();
      var qtyBolt = $('.qtyBolt').val();
      if (name == '') {
        $('#set-name').addClass('input-error');
        $('#set-name').attr('placeholder', 'Llena este campo');
        $('#set-name').focus();
      }else {
        $('#set-name').removeClass('input-error');
        $('#set-name').removeAttr('placeholder');
      }
      if (description == '') {
        $('#set-description').addClass('input-error');
        $('#set-description').attr('placeholder', 'Llena este campo');
        $('#set-description').focus();
      }else {
        $('#set-description').removeClass('input-error');
        $('#set-description').removeAttr('placeholder');
      }
      if (qtyBolt == '') {
        $('.qtyBolt').addClass('input-error');
        $('.qtyBolt').attr('placeholder', 'Llena este campo');
        $('.qtyBolt').focus();
      }else {
        $('.qtyBolt').removeClass('input-error');
        $('.qtyBolt').removeAttr('placeholder');
      }
    }else {
      switch (mode) {
        case 'update':
          //updateSet(setId);
          break;
        case 'insert':
          console.log('hi');
          insertSet();
          break;
        default:

      }
    }
  })
  $('body').on('click', '.saveProduct', function(e){
    var mode = $(this).attr('data-mode');
    var productId = $(this).attr('data-id');
    var inpObj = document.getElementById('inserProduct');
    if (!inpObj.checkValidity()) {
      var name = $('#product-name').val();
      var description = $('#product-description').val();
      var quantity = $('#product-quantity').val();
      if (name == '') {
        $('#product-name').addClass('input-error');
        $('#product-name').attr('placeholder', 'Llena este campo');
        $('#product-name').focus();
      }else {
        $('#product-name').removeClass('input-error');
        $('#product-name').removeAttr('placeholder');
      }
      if (description == '') {
        $('#product-description').addClass('input-error');
        $('#product-description').attr('placeholder', 'Llena este campo');
        $('#product-description').focus();
      }else {
        $('#product-description').removeClass('input-error');
        $('#product-description').removeAttr('placeholder');
      }
      if (quantity == '') {
        $('#product-quantity').addClass('input-error');
        $('#product-quantity').attr('placeholder', 'Llena este campo');
        $('#product-quantity').focus();
      }else {
        $('#product-quantity').removeClass('input-error');
        $('#product-quantity').removeAttr('placeholder');
      }
    }else {
      switch (mode) {
        case 'update':
          updateProducts(productId);
          break;
        case 'insert':
          insertProduct();
          break;
        default:

      }
    }
  });
  $('body').on('click', '.delete-product', function(){
    var id = $(this).attr('data-id');
    var name = $(this).attr('data-name');
    $('.options-menu-item').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.options-menu-item').css({"display":"none"});
    },250);
    showdialog(()=>{
      deleteProduct(id);
    }, '¿Quieres eliminar el producto ' + name + '?');
  });
  $('body').on('click', '.item-p', function(){
    var name = $(this).attr('data-name');
    var description = $(this).attr('data-description');
    var quantity = $(this).attr('data-quantity');
    var image = $(this).attr('data-image');
    $('.box-shadow').show();
    $('.info-product-box').css({"display":"block"});
    $('.info-product-box').css({"animation":"pop-in","animation-duration":"0.25s"});
    writeInformationItem(name, description, quantity, image);
  });
  $('body').on('click', '.item-receipt-count', function(){
    var name = $(this).attr('data-name');
    var id = $(this).attr('data-id');
    $('.qtyReceipt').val('');
    $('.name-receipt-product p').html('Producto ' + name);
    $('.insertReceiptProduct').attr('data-id', id);
    $('.box-shadow').show();
    $('.capture-receipt').css({"display":"block"});
    $('.capture-receipt').css({"animation":"pop-in","animation-duration":"0.25s"});
    $('.qtyReceipt').focus();
  });
  $('body').on('click', '.item-shipping-count', function(){
    var name = $(this).attr('data-name');
    var id = $(this).attr('data-id');
    var qty = $(this).attr('data-qty');
    $('.qtyShipping').val('');
    $('.name-shipping-product p').html('Producto ' + name);
    $('.insertShippingProduct').attr('data-id', id);
    $('.insertShippingProduct').attr('data-qty', qty);
    $('.box-shadow').show();
    $('.capture-shipping').css({"display":"block"});
    $('.capture-shipping').css({"animation":"pop-in","animation-duration":"0.25s"});
    $('.qtyShipping').focus();
  });
  $('body').on('click', '.close-capture-receipt', function(){
    $('.box-shadow').hide();
    $('.capture-receipt').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.capture-receipt').css({"display":"none"});
    },250);
  });
  $('body').on('click', '.close-capture-shipping', function(){
    $('.box-shadow').hide();
    $('.capture-shipping').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.capture-shipping').css({"display":"none"});
    },250);
  });
  $('body').on('click', '.insertReceiptProduct', function(){
    var productId = $(this).attr('data-id');
    var inpObj = document.getElementById('insertReceipt');
    if (!inpObj.checkValidity()) {
      var quantity = $('.qtyReceipt').val();
      if (quantity == '') {
        $('.qtyReceipt').addClass('input-error');
        $('.qtyReceipt').attr('placeholder', 'Ingresa una cantidad');
        $('.qtyReceipt').focus();
      }else {
        $('.qtyReceipt').removeClass('input-error');
        $('.qtyReceipt').removeAttr('placeholder');
      }
    }else {
      receiptProduct(productId);
      }
  });
});
function receiptProduct(productId){
  var quantity = $('.qtyReceipt').val();
  var dataInsert = (
      updateProduct = {
        productQuantity: quantity
  });
  $.ajax({
    url: '/ISS/application/index.php/ISS/insertReceipt',
    type: 'PUT',
    dataType: 'json',
    data: {
        insertProduct: dataInsert,
        id: productId
    },
    success: (data)=>{
      console.log(data);
      switch (data.code) {
        case 200:
          var section = "receipt";
          var filter = '';
          getProductByFilter(filter, section);
          $('.box-shadow').hide();
          $('.capture-receipt').css({"animation":"pop-out","animation-duration":"0.25s"});
          setTimeout(function(){
              $('.capture-receipt').css({"display":"none"});
          },250);
          break;
        case 500:
          break;
        default:

      }
    },
    error: (req, msg, error)=>{
      console.log(req);
      console.log(req.responseText);
    }
  })
}
$('body').on('click', '.insertShippingProduct', function(){
  var productId = $(this).attr('data-id');
  var qty = $(this).attr('data-qty');
  var inpObj = document.getElementById('insertShipping');
  var quantity = $('.qtyShipping').val();
  if (!inpObj.checkValidity()) {
    if (quantity == '') {
      $('.qtyShipping').addClass('input-error');
      $('.qtyShipping').attr('placeholder', 'Ingresa una cantidad');
      $('.qtyShipping').focus();
    }else {
      $('.qtyShipping').removeClass('input-error');
      $('.qtyShipping').removeAttr('placeholder');
    }
  }else {
      if (quantity <= qty) {
        shippingProduct(productId);
      }else {
        $('.capture-shipping').css({"height":"250px;"});
        $('#insertShipping > div').append(`
          <p class="error-qty">La cantidad de mercancía no puede ser mayor a ${qty} para este producto.
          `);
      }
    }
});
function shippingProduct(productId){
  var quantity = $('.qtyShipping').val();
  var dataInsert = (
      updateProduct = {
        productQuantity: quantity
  });
  $.ajax({
    url: '/ISS/application/index.php/ISS/insertShipping',
    type: 'PUT',
    dataType: 'json',
    data: {
        insertProduct: dataInsert,
        id: productId
    },
    success: (data)=>{
      console.log(data);
      switch (data.code) {
        case 200:
          var section = "receipt";
          var filter = '';
          getProductByFilter(filter, section);
          $('.box-shadow').hide();
          $('.capture-shipping').css({"animation":"pop-out","animation-duration":"0.25s"});
          setTimeout(function(){
              $('.capture-shipping').css({"display":"none"});
          },250);
          break;
        case 500:
          break;
        default:

      }
    },
    error: (req, msg, error)=>{
      console.log(req);
      console.log(req.responseText);
    }
  })
}
function deleteProduct(id){
  $.ajax({
    url: '/ISS/application/index.php/ISS/delete',
    type: 'DELETE',
    dataType: 'json',
    data: {
        idProduct: id
    },
    success: (data)=>{
        switch (data.code) {
            case 200:
                getProducts();
                break;
            case 400:
              console.log("Error 400");
              break;
          case 500:
            console.log("Error 500");
            break;
            default:
        }

    },
    error: (req, msg, error)=>{
      console.log(req);
      console.log(req.responseText);
    }
  })
}
function writeInformationItem(name, description, quantity, image){
  $('.info-product-box .section-data').html(
    `
      <div>
        <img src="${image}"/>
      </div>
      <div>
        <p class="info-name">${name}</p>
        <label class="info-description">${description}</label>
        <div class="quantity-info">
          <p>Cantidad: </p>
          <label>${quantity}</label>
        </div>
      </div>

    `
  );
}
/*****************************************************************************Dialogo de confirmacion********************************************/
function showdialog(callback, message){
    $('.message-dialog-not').html(message);
    $('.box-confirmation-not').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"pop-in", "animation-duration":"0.25s"});

    $('.yes-confirmation').bind("click", function(){
        callback();
				$('.box-confirmation-not').css({"animation":"pop-out","animation-duration":"0.25s"});
		    setTimeout(function(){
		        $('.box-confirmation-not').css({"display":"none"});
		    },250);
        $('.box-shadow').hide();
        $('.yes-confirmation').unbind("click");
    });
    $('.no-confirmation').click(function(){
				$('.box-confirmation-not').css({"animation":"pop-out","animation-duration":"0.25s"});
		    setTimeout(function(){
		        $('.box-confirmation-not').css({"display":"none"});
		    },250);
        $('.box-shadow').hide();
        $('.yes-confirmation').unbind("click");
    });
}
function closeBox(){
  $('.box-shadow').hide();
  $('.new-product-box').css({"animation":"pop-out","animation-duration":"0.25s"});
  setTimeout(function(){
      $('.new-product-box').css({"display":"none"});
  },250);
  $('#product-name').val('');
  $('#product-description').val('');
  $('#product-image').val('');
  $('#product-quantity').val('');
  $('#product-name').removeClass('input-error');
  $('#product-quantity').removeClass('input-error');
  $('#product-description').removeClass('input-error');
  $('#product-name').removeAttr('placeholder');
  $('#product-quantity').removeAttr('placeholder');
  $('#product-description').removeAttr('placeholder');

}
function closeBoxSet(){
  $('.box-shadow').hide();
  $('.new-set-box').css({"animation":"pop-out","animation-duration":"0.25s"});
  setTimeout(function(){
      $('.new-set-box').css({"display":"none"});
  },250);
  $('#set-name').val('');
  $('#set-description').val('');
  $('#set-image').val('');
  $('#set-name').removeClass('input-error');
  $('#set-description').removeClass('input-error');
  $('.qtyBolt').removeClass('input-error');
  $('#set-name').removeAttr('placeholder');
  $('#set-description').removeAttr('placeholder');
  $('.qtyBolt').attr('placeholder', 'Cantidad');
  $('#insertSetSection').show();
  $('.saveProduct').show();
  $('.set-items-box').css({"display":"none"});

}
function openBox(){
  $('.box-shadow').show();
  $('.new-product-box').css({"display":"block"});
  $('.new-product-box').css({"animation":"pop-in","animation-duration":"0.25s"});
  $('#product-name').focus();
}
function openBoxSet(){
  $('.box-shadow').show();
  $('.new-set-box').css({"display":"block"});
  $('.new-set-box').css({"animation":"pop-in","animation-duration":"0.25s"});
  $('#set-name').focus();
}
function insertProduct(){
  var name = $('#product-name').val();
  var image = $('#product-image').val();
  var description = $('#product-description').val();
  var quantity = $('#product-quantity').val();
  var color = parseInt($('#product-color option:selected').val());
  var dataInsert = (
      updateProduct = {
        productName: name,
        productImage: image,
        productDescription: description,
        productQuantity: quantity,
        productColor: color
  });
  $.ajax({
    url: '/ISS/application/index.php/ISS/insert',
    type: 'PUT',
    dataType: 'json',
    data: {
        insertProduct: dataInsert
    },
    success: (data)=>{
      console.log(data);
      switch (data.code) {
        case 200:
          getProducts();
          closeBox();
          break;
        case 500:
          break;
        default:

      }
    },
    error: (req, msg, error)=>{
      console.log(req);
      console.log(req.responseText);
    }
  })
}
function updateProducts(productId){
  var name = $('#product-name').val();
  var image = $('#product-image').val();
  var description = $('#product-description').val();
  var quantity = $('#product-quantity').val();
  var color = parseInt($('#product-color option:selected').val());
  var dataInsert = (
      updateProduct = {
        productName: name,
        productImage: image,
        productDescription: description,
        productQuantity: quantity,
        productColor: color
  });
  $.ajax({
    url: '/ISS/application/index.php/ISS/update',
    type: 'PUT',
    dataType: 'json',
    data: {
        updateProduct: dataInsert,
        productId: productId
    },
    success: (data)=>{
      console.log(data);
      switch (data.code) {
        case 200:
          getProducts();
          closeBox();
          break;
        case 500:
          break;
        default:

      }
    },
    error: (req, msg, error)=>{
      console.log(req);
      console.log(req.responseText);
    }
  })
}
//Escribe los productos dentro de la tabla
function putProducts(product, index, first = false){
    if (first) {
      return `
        <tr>
          <td>Opciones</td>
          <td>Imagen</td>
          <td>Producto</td>
          <td>Color</td>
          <td>Descripción</td>
          <td>Cantidad</td>
        </tr>
      `;
    }
    else {
      return `
          <tr id="product_${product.id}" class="item-product" data-id="${product.id}" data-name="${product.name}" data-description="${product.description}" data-quantity="${product.quantity}" data-color="${product.idcolor}" data-image="${product.image}">
              <td class="options-item">
                <img src="images/icons/options.svg">
                <div class="options-menu-item">
                  <div class="edit-product">
                    <img src="images/icons/edit.svg"/>
                    <p>Editar</p>
                  </div>
                  <div class="delete-product" data-id="${product.id}" data-name="${product.name}">
                    <img src="images/icons/delete.svg"/>
                    <p>Eliminar</p>
                  </div>
                </div>
              </td>
              <td class="icon">
                <img src="${product.image}">
              </td>
              <td class="name">
                <p>${product.name}</p>
              </td>
              <td class="color" data-idcolor="${product.idcolor}">
                <p></p>
              </td>
              <td class="description">
                <p>${product.description}</p>
              </td>
              <td class="quantity">
                <p>${product.quantity}</p>
              </td>
          </tr>
      `;
    }
}
//Escribe los productos dentro de la tabla
function putProductsT(product, index, first = false){
    if (first) {
      return `
        <tr>
          <td>Opciones</td>
          <td>Imagen</td>
          <td>Producto</td>
          <td>Color</td>
          <td>Descripción</td>
          <td>Cantidad</td>
        </tr>
      `;
    }
    else {
      return `
          <tr id="product_${product.id}" class="item-product" data-id="${product.id}" data-name="${product.name}" data-description="${product.description}" data-quantity="${product.quantity}" data-color="${product.idcolor}" data-image="${product.image}">
              <td class="options-item">
                <img src="images/icons/options.svg">
                <div class="options-menu-item">
                  <div class="edit-product">
                    <img src="images/icons/edit.svg"/>
                    <p>Editar</p>
                  </div>
                  <div class="delete-product" data-id="${product.id}" data-name="${product.name}">
                    <img src="images/icons/delete.svg"/>
                    <p>Eliminar</p>
                  </div>
                </div>
              </td>
              <td class="icon">
                <img src="${product.image}">
              </td>
              <td class="name">
                <p>${product.name}</p>
              </td>
              <td class="color" data-idcolor="${product.idcolor}">
                <p></p>
              </td>
              <td class="description">
                <p>${product.description}</p>
              </td>
              <td class="quantity">
                <p>${product.quantity}</p>
              </td>
          </tr>
      `;
    }
}
//Obtiene los 10 productos con menor cantidad en inventario
function getProducts(){
    var tableProducts = $('#inventory .onlyItems table');
    tableProducts.empty();
    productsArray = [];
    tornillosArray = [];
    $.ajax({
        url: `/ISS/application/index.php/ISS/get`,
        type: "GET",
        dataType: 'json',
        success: data=>{
            switch (data.code) {
                case 200:
                    console.log(data);
                    tableProducts.append(putProducts({}, 0, true));
                    data.response.products.forEach((product)=>{
                        tableProducts.append(putProducts(product));
                    });
                    colorsArray = data.response.colors;
                    productsArray = data.response.products;
                    tornillosArray = data.response.tornillos;
                    data.response.tornillos.forEach((product)=>{
                        tableProducts.append(putProductsT(product));
                    });
                    break;
                case 404:
                    console.log(data);
                    break;
                default:
            }
        }, error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{
      getProductsSet();
      $('#inventory .onlyItems table .item-product td').map(function(i){
        if (i!=0) {
          var classSection = $(this).attr('class');
          if (classSection == "color") {
            var idcolor = $(this).attr('data-idcolor');
            for (var i = 0; i < colorsArray.length; i++) {
              if (idcolor == colorsArray[i].id) {
                console.log(colorsArray[i].name);
                $(this).html(`${colorsArray[i].name} - (${colorsArray[i].description})`);
              }
            }
          }
        }
      });
    });
}
//Escribe los productos dentro de la tabla
function putSetProducts(set, index, first = false){
    if (first) {
      return `
        <tr>
          <td>Opciones</td>
          <td>Imagen</td>
          <td>Producto</td>
          <td>Color</td>
          <td>Descripción</td>
          <td>Cantidad</td>
        </tr>
      `;
    }
    else {
      return `
          <tr id="set_${set.id}" class="item-set" data-id="${set.id}" data-name="${set.name}" data-description="${set.description}"  data-image="${set.image}">
              <td class="options-item">
                <img src="images/icons/options.svg">
                <div class="options-menu-item">
                  <div class="edit-set">
                    <img src="images/icons/edit.svg"/>
                    <p>Editar</p>
                  </div>
                  <div class="delete-set" data-id="${set.id}" data-name="${set.name}">
                    <img src="images/icons/delete.svg"/>
                    <p>Eliminar</p>
                  </div>
                </div>
              </td>
              <td class="icon">
                <img src="${set.image}">
              </td>
              <td class="name">
                <p>${set.name}</p>
              </td>
              <td>
                <p></p>
              </td>
              <td class="description">
                <p>${set.description}</p>
              </td>
              <td class="quantity">
                <p></p>
              </td>
          </tr>
      `;
    }
}
function getProductsSet(){
    var tableSetProducts = $('#inventory .onlySet table');
    tableSetProducts.empty();
    $.ajax({
        url: `/ISS/application/index.php/ISS/get/set`,
        type: "GET",
        dataType: 'json',
        success: data=>{
            switch (data.code) {
                case 200:
                    console.log(data);
                    tableSetProducts.append(putSetProducts({}, 0, true));
                    data.response.sets.forEach((set)=>{
                        tableSetProducts.append(putSetProducts(set));
                    });
                    setsArray = data.response.sets;
                    break;
                case 404:
                    console.log(data);
                    break;
                default:
            }
        }, error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{
      var items = [];
      var sameItem = 1;
      console.log(setsArray);
      //productsArray.id
      for (var i = 0; i < setsArray.length; i++) {
        items.push(setsArray[i].idproducts.split(","));
      }
      console.log(items);
      for (var i = 0; i < items.length; i++) {
        if (items[i].length > 1) {
          for (var j = 0; j < items[i].length; j++) {
            if (items[i][j] == items[i][j + 1]) {
              sameItem++;
              console.log('Same: ' + sameItem + ' : ' + items[i][j]);
            }
          }
        }
      }
    });
}
function putItem(item, index, first = false){
    if (first) {
      return `
        <tr>
          <td>Producto</td>
          <td>Descripción</td>
        </tr>
      `;
    }
    else {
      return `
          <tr id="item_${item.id}" class="item-p" data-id="${item.id}" data-name="${item.name}" data-description="${item.description}" data-quantity="${item.quantity}" data-image="${item.image}">
              <td class="name">
                <p>${item.name}</p>
              </td>
              <td class="description">
                <p>${item.description}</p>
              </td>
          </tr>
      `;
    }
}
function putItemsReceipt(item){
  return `
    <div class="item-receipt-count" data-id="${item.id}" data-name="${item.name}">
      <img src="${item.image}"/>
      <p>${item.name}</p>
    </div>
  `;
}
function putItemsShipping(item){
  return `
    <div class="item-shipping-count" data-id="${item.id}" data-name="${item.name}" data-qty="${item.quantity}">
      <img src="${item.image}"/>
      <p>${item.name}</p>
    </div>
  `;
}
function getProductByFilter(filter, section){
  console.log(section);
  var tableProducts = $('.items-box table');
  tableProducts.empty();
  $.ajax({
      url: `/ISS/application/index.php/ISS/search`,
      type: "GET",
      dataType: 'json',
      data: {
          filter: filter
      },
      success: data=>{
          switch (data.code) {
              case 200:
                console.log(data);
                if (section == 'search') {
                  var total = data.response.totalProducts;
                  $('.info-total-items').find('label').html(total);
                    tableProducts.append(putItem({}, 0, true));
                    data.response.items.forEach((item)=>{
                        tableProducts.append(putItem(item));
                    });
                }
                if (section == 'receipt') {
                  var sectionReceiptBox = $('.items-receipt-box');
                  sectionReceiptBox.empty();
                  data.response.items.forEach((item)=>{
                      sectionReceiptBox.append(putItemsReceipt(item));
                  });
                }
                if (section == 'shipping') {
                  var sectionReceiptBox = $('.items-shipping-box');
                  sectionReceiptBox.empty();
                  data.response.items.forEach((item)=>{
                      sectionReceiptBox.append(putItemsShipping(item));
                  });
                }
                  break;
              case 404:
                  console.log(data);
                  break;
              default:
          }
      }, error: (req, msg, error)=>{
        console.log(req);
      }
  }).done(()=>{
  });
}
function insertSet(){
  var qtynut = 0;
  var name = $('#set-name').val();
  var image = $('#set-image').val();
  var description = $('#set-description').val();
  var items = $('.productsSet').data('id');
  var qtyBolt = $('.qtyBolt').val();
  var boltid = parseInt($('#set-bolt option:selected').val());
  var mm = $('#set-bolt option:selected').attr('data-mm');
  if (mm == "25mm") {
    qtynut = qtyBolt;
  }
  var dataInsert = (
      updateSet = {
        setName: name,
        setImage: image,
        setDescription: description,
        setItems: items,
        quantityBolt: qtyBolt,
        boltId: boltid,
        qtyNut: qtynut
  });
  $.ajax({
    url: '/ISS/application/index.php/ISS/insert/set',
    type: 'PUT',
    dataType: 'json',
    data: {
        insertSet: dataInsert
    },
    success: (data)=>{
      console.log(data);
      switch (data.code) {
        case 200:
          getProductsSet();
          closeBoxSet();
          break;
        case 500:
          break;
        default:

      }
    },
    error: (req, msg, error)=>{
      console.log(req);
      console.log(req.responseText);
    }
  })
}

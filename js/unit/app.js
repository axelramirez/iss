var numberpage = 1;
$(document).ready(function() {
    getUnit();
    //Abre el dialogo de Create Location
    $('body').on('click', '.add-unit', function(){
        $('.title-dialog h2').text('Create Package Type');
        $('.save-btn').val('Create');
        $('.save-btn').removeClass('update-unit');
        $('.save-btn').addClass('insert-unit');
        openUnitBox();
    });
    $('body').on('click', '.edit-unit img', function(){
        $('.title-dialog h2').text('Edit Package Type');
        var id = $(this).parent().parent().attr('data-id');
        var name = $(this).parent().parent().find('.unitdescription').text();

        openUnitBox();
        $('.unitname input').val(name);
        $('.update-unit').attr('data-id', id);
    });
    //Cierra el dialogo de service y limpia los inputs
    $('body').on('click', '.close-unit', function(){
        closeUnitBox();
    });
    $('body').on('click', '.cancel-insert-unit', function(){
        closeUnitBox();
    });
    $('body').on('click', '.insert-unit', function(e){
      var inpObj = document.getElementById('unitName');
      if (!inpObj.checkValidity()) {
        $('.unitname input').addClass('input-error');
        $('.unitname input').attr('placeholder', 'Please fill');
        $('.unitname input').focus();
      }else {
        e.preventDefault();
        var unitname = $('.unitname input').val();
        if (unitname.length <= 50) {
          insertUnit();
        }else {
          console.log('Please not edit the max length');
        }
      }
    });
    $('body').on('click', '.update-unit', function(e){
      var inpObj = document.getElementById('unitName');
      if (!inpObj.checkValidity()) {
        $('.unitname input').addClass('input-error');
        $('.unitname input').attr('placeholder', 'Please fill');
        $('.unitname input').focus();
      }else {
        e.preventDefault();
        var unitname = $('.unitname input').val();
        if (unitname.length <= 50) {
          updateUnit();
        } else {
          console.log('Please not edit the max length');
        }
      }
    });
    $('body').on('click', '.next-unit-list', function(){
        numberpage++;
        getUnit();
    });
    $('body').on('click', '.back-unit-list', function(){
        if (numberpage > 1) {
            numberpage--;
            getUnit();
        }
    });
});
//Abre el contenedor para insertar/actualizar unit
function openUnitBox(){
    $('.box-shadow').css({"display":"block"});
    $('.add-unit-box').css({"display":"block"});
    $('.add-unit-box').css({"animation":"pop-in","animation-duration":"0.25s"});
}
//Cierra el contenedor para insertar/actualizar unit
function closeUnitBox(){
    $('.box-shadow').css({"display":"none"});
    $('.add-unit-box').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.add-unit-box').css({"display":"none"});
    },250);
    $('.unitname input').val('');
    $('.unitname input').removeClass('input-error');
    $('.unitname input').attr('placeholder', '');
    resetClass();
}
//Resetea las clases del contenedor insertar/actualizar unit
function resetClass(){
    $('.save-btn').removeClass('insert-unit');
    $('.save-btn').addClass('update-unit');
}
//Fucnion para insertar unit
function insertUnit(){
    var description = $('.unitname input').val();
    $.ajax({
      url: '/ISS/application/index.php/unit/insert',
      type: 'PUT',
      dataType: 'json',
      data: {
          unitName: description
      },
      success: (data)=>{
          getUnit();
          closeUnitBox();
          shownotify(0);
      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}
//Funcion para actualizar uom
function updateUnit(){
    var description = $('.unitname input').val();
    var id = $('.update-unit').attr('data-id');
    $.ajax({
      url: '/ISS/application/index.php/unit/update',
      type: 'PUT',
      dataType: 'json',
      data: {
          unitName: description,
          unitId: id
      },
      success: (data)=>{
          switch (data.code) {
              case 200:
                  getUnit();
                  closeUnitBox();
                  shownotify(0);
                  break;
              case 400:
                console.log("ERROR");
                  break;
              default:

          }
      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}
//Escribe los uoms dentro de la tabla
function putUnit(unit, index, first = false){
    if (first) {
        return `
            <tr>
                <td>Edit</td>
                <td>Description</td>
            </tr>
        `;
    }
    else {
        return `
            <tr id="unit_${unit.unitid}" data-id="${unit.unitid}">
                <td class="edit-unit">
                    <img src="images/icons/edit.svg" alt="Edit">
                </td>
                <td class="unitdescription">${unit.description}</td>
            </tr>
        `;
    }
}
//Obtiene los uoms con el metodo ajax y los itera en la funcion putUom
function getUnit(){
    var tableUnit = $('.unit-box table');
    tableUnit.empty();
    $.ajax({
        url: `/ISS/application/index.php/unit/get`,
        type: "GET",
        dataType: 'json',
        data: {
            page: numberpage
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    tableUnit.append(putUnit({}, 0, true));
                    data.response.units.forEach((unit)=>{
                        tableUnit.append(putUnit(unit));
                    });
                    var entries = data.response.numberEntries;
                    var pages = data.response.numberPages;
                    $('.entries-unit .entries').html(entries + ' entries');
                    $('.entries-unit .pages').html(pages + ' Page(s)');
                    $('.actual-page p').html('Page ' + numberpage);
                    //Ocultar y mostrar flechas de navegacion
                    if (numberpage == pages) {$('.next-unit-list').hide();}
                    else {$('.next-unit-list').show();}
                    if (numberpage > 1) {$('.back-unit-list').show();}
                    else {$('.back-unit-list').hide();}
                    break;
                case 404:
                    console.log(data);
                    break;
                default:
            }
        }, error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{
        loadingcatalogs();
    });
}

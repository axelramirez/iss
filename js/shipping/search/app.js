var app = angular.module('shippingApp', []);
app.controller('shippingController', ['$scope', '$http', '$timeout', function($scope, $http, $timeout){
  $scope.isSearch = true;
  $scope.withMenu = 0;
  $scope.isOpenMenu = false;
  $scope.isLoading = false;
  $scope.withoutScroll = "";
  $scope.isAdvanced = false;
  $scope.carriers = [];
  $scope.parts = [];
  $scope.isOpenDialog = false;
  $scope.isPartDialog = false;
  $scope.productSelected = 0;
  $scope.buttonOptionSearchText = "Advanced Search";
  $scope.productSelected = {
    productId: 0, productNumber: ''
  };
  $scope.buttonOptionSearchStyle = {
    "color": "#40a3ff"
  };

  $http.get('/ISS/application/index.php/shipping/get/data/search')
  .then(function(response){
    $scope.carriers = response.data.response.carriers;
    $scope.parts = response.data.response.products;
    $scope.carriers.unshift({value:0, name:"All"});
    $scope.parts.splice(0, 1);
  })
  .catch(function(error){
    console.log(error);
  })
  $scope.clearSearchData = function(){
    $scope.searchFilter = {
      shippingNumber: '',
      pedimento: '',
      customerId: 0,
      receivingNo: '',
      trackingNo: '',
      inbond: '',
      productId: 0,
      carrierId: 0,
      driver: '',
      truckNumber: '',
      dateFrom: '',
      dateTo: '',
      numberPage: 1
    };
  };
  $scope.clearSearchData();

  $scope.selectPart = function(productId, productNumber){
    $scope.productSelected = {
      productId, productNumber
    };
  }

  $scope.optionSearch = function(){
    $scope.isAdvanced = !$scope.isAdvanced;
    if(!$scope.isAdvanced){
      $scope.buttonOptionSearchText = "Advanced Search";
      $scope.buttonOptionSearchStyle = {
        "color": "#40a3ff"
      };
    }else{
      $scope.buttonOptionSearchText = "Simple Search";
      $scope.buttonOptionSearchStyle = {
        "color": "rgb(255, 86, 86)"
      };
    }
  }

  $scope.openMenu = function(id){
    if($scope.isOpenMenu){
      $scope.isOpenMenu = false;
    }else{
      $scope.isOpenMenu = true;
      $scope.withMenu = id;
    }
  }

  $scope.searchShippings = function(){
    $scope.isLoading = true;
    $('.button.search').css({"background":"#c9e4ff"});
    $('.button.search').prop('disabled', true);
    $('.loading-search-button').addClass('flex');
    $scope.shippings = [];
    var dateform = $scope.searchFilter.dateFrom = $('.dateFrom').val();
    var dateTo = $scope.searchFilter.dateTo = $('.dateTo').val();
    if (dateform != 0) {
        $scope.searchFilter.dateFrom = (new Date(dateform)).toString('yyyy-MM-dd');
    }else{
        $scope.searchFilter.dateFrom = '';
    }
    if (dateTo != 0) {
        $scope.searchFilter.dateTo = (new Date(dateTo)).toString('yyyy-MM-dd');
    }else{
        $scope.searchFilter.dateTo = '';
    }
    $http.get(
      `/ISS/application/index.php/shipping/search?${toURLParameters($scope.searchFilter)}`,
      {
        header: {
          'Content-Type': 'application/json'
        }
      }
    ).then(function(response){
      console.log(response.data);
      switch(response.data.code){
        case 200:
        // console.log(response.data.response.shippings);
        var length = response.data.response.shippings.length;
        $('.button.search').css({"background":"#3197ff"});
        $('.loading-search-button').removeClass('flex');
        $('.button.search').prop('disabled', false);
        $('.search').attr('data-receipt', length);
        $('.not-found').hide();
        $('.options-search').show();
        $('.table-search').show();
        $('body,html').animate({ scrollTop: 530 }, 700);
        $scope.shippings = response.data.response.shippings;
        $scope.numberEntries = response.data.response.numberEntries;
        $scope.numberPages = response.data.response.numberPages;
        $scope.searchFilter.numberPage = response.data.response.numberPage;
        $scope.nextPage = response.data.response.nextPage;
        $scope.prevPage = response.data.response.previousPage;
        $scope.searchFilter.numberPage = response.data.response.numberPage;
        var array = response.data.response.shippings;
        break;
        case 404:
        console.log('Error');
        var length = 0;
        $('.search').attr('data-receipt', length);
        $('.not-found').css({"display":"flex"});
        $('.options-search').hide();
        $('.table-search').hide();
        $('body,html').animate({ scrollTop: 530 }, 700);
        $scope.numberEntries = 0;
        $scope.numberPages = 1;
        $scope.searchFilter.numberPage = 1;
        $scope.nextPage = 2;
        $scope.prevPage = 0;
        break;
      }
      $timeout(function(){
        $scope.withoutScroll = "";
        $scope.isLoading = false;
        // console.log(response.data.response.shippings);
        // testfunction();
        // for (var i = 0; i < array.length; i++) {
        //     var receivingid = array[i].receivingid;
        //     if (array[i].shipqty > 0) {
        //         $('.editreceiving_'+receivingid).remove();
        //         $('.deletereceiving_'+receivingid).remove();
        //     }
        //     if (array[i].currentqty == 0) {
        //         $('.fastshipping_'+receivingid).remove();
        //     }
        //     if (array[i].isshipped == 'F' && array[i].currentqty > 0) {
        //         if (array[i].preshipping > 0) {
        //             $('.checktopreshipping_'+receivingid).css({'background':'#ff3f3f'});
        //             $('.checktopreshipping_'+receivingid).find('p').css({'color':'#fff'});
        //         }
        //         else{
        //             $('.checktopreshipping_'+receivingid).css({'background':'#fff'});
        //         }
        //     }
        //     if (array[i].isshipped == 'T') {
        //         $('.checktopreshipping_'+receivingid).remove();
        //     }
        //     if (array[i].hasLinkedDocs == 'F') {
        //         $('.document_'+receivingid).find('img').remove();
        //     }
        //     if (array[i].damaged == 'no') {
        //         $('.damaged_'+receivingid).find('img').remove();
        //     }
        // }
      }, 500);
    }).catch(function(error){
      $scope.withoutScroll = "";
      $scope.isLoading = false;
      console.log(error);
    });
  }

  $scope.nextResult = function(){
    if($scope.nextPage <= $scope.numberPages){
      $scope.searchFilter.numberPage++
      $scope.searchShippings()
    }
  }

  $scope.previousResult = function(){
    if($scope.previousPage != 0){
      $scope.searchFilter.numberPage--
      $scope.searchShippings()
    }
  }
}]);

function toURLParameters(json){
  var data=JSON.stringify(json);
  data = data.replace(/{/g, "");
  data = data.replace(/}/g, "");
  data = data.replace(/:/g, "=")
  data = data.replace(/,/g, "&");
  data = data.replace(/"/g, "");
  return data
}

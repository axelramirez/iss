$(document).ready(function() {

	$('#customerid').change(function() {
		customerid = $(this).val();
		if (customerid == 0) {
			$('#part-option').hide();
			// $('#clearP').click();
			// $('#newPreshipBtn').attr('disabled', true)
			// 	.removeClass('create-pre')
			// 	.addClass('disable');
		} else {
			$('#part-option').show();
			// $('#newPreshipBtn').attr('disabled', false)
			// 	.addClass('create-pre')
			// 	.removeClass('disable');
		}
	});
	$('body').on('click', '.delete-shipping', function(){
			var shippingid = $(this).attr('data-id');
			var shippingno = $(this).attr('data-sno');
			$('.invisible-box').hide();
		  $('.options-table').css({"animation":"pop-out","animation-duration":"0.25s"});
		  setTimeout(function(){
			  $('.options-table').css({"display":"none"});
		  },250);
			showdialog(()=>{
					deleteShipping(shippingid);
			}, 'Are you sure delete Shipping No. ' + shippingno + '?')
	});
});
//Funcion para eliminar un shipping
function deleteShipping(shippingid){
    $.ajax({
      url: '/ISS/application/index.php/shipping/delete',
      type: 'DELETE',
      dataType: 'json',
      data: {
          idShipping: shippingid
      },
      success: (data)=>{
					console.log(data);
          switch (data.code) {
              case 200:
                  $('.rowShipping_'+shippingid).remove();
                  shownotify(1);
                  break;
              case 400:
                console.log("Error 400");
                break;
            case 500:
              console.log("Error 500");
              break;
              default:
          }

      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}

var shipping = [];
var details = [];
var insertReceiving = [];
var insertProducts = [];
var shippingdetailsarray = [];
var rIds = [];
var receivingsids = [];
var customer = [];
var productsList = [];
var pIds = [];
var productlistarray = [];
var trackings = [];
var files = [];
var isWarehouse = false;
var indexFile = 0;
$(document).ready(function() {
  var shippingId = $('#main-id').attr('data-id');
  getShipping(shippingId);
  getShipingDetails(shippingId);
  getTrackings(shippingId);
  getDocuments(shippingId);
  $('#receivingFile').change(convertFiles);
  $('body').on('click', '.openSeals', function(){
    openSealsBox();
  });
  $('body').on('click', '.close-seals', function(){
    closeSealsBox();
  });
  $('body').on('click', '.add-charge', function(){
    var preinsertextraCargos = Array();
    var cost = $('#service_cost').text();
    var code = $('#service_code').text();
    var other_cost = $('#service_variant').val();
    var serviceid = $('.selectCharge option:selected').val();
    var description = $('#description-service').text();
    var information = $('#service_information').val();
    var baserate = $('#service_baserate').text();
    var increment = $('.selectCharge option:selected').attr('data-increment');
    var quantity = $('#service_qty').val();

    for (var i = 0; i < quantity; i++) {
      var sizedetails = details.length;
      preinsertextraCargos.push({
        serviceId: serviceid,
        code: code,
        cost: cost,
        variantCot: other_cost,
        description: description,
        information: information,
        baseRate: baserate,
        increment: increment,
        index: sizedetails,
        status: 2
      })
      details.push({
        serviceId: serviceid,
        code: code,
        cost: cost,
        variantCot: other_cost,
        description: description,
        information: information,
        baseRate: baserate,
        increment: increment,
        index: sizedetails,
        status: 2
      })
    }
    writeextracargo(preinsertextraCargos);
  });
  //delete extracargo event
  $('body').on('click', '.delete-charge img', function(){
    preextraCargos = Array();
    var row = $(this).parent().parent();
    var status = row.data('status');
    if (status == 1) {
      row.data('status', 0);
      row.hide();
      details.push({
        serviceshippingid: row.data('id'),
        shippingid: shippingId,
        status: 0
      })
    }
    if (status == 2) {
      indexarray = row.attr('data-indexarray');
      row.remove();
      delete details[indexarray];
      details = details.filter(function(e){ return e === 0 || e });
    }
  });
  $('body').on('keyup', '.qtyshipped input', function(){
    var detailid = $(this).parent().parent().attr('data-id');
    var val = $(this).val();
    if (val.charAt(0) == 0) {
      var l = val.length;
      val = val.substr(1,l)
      $(this).val(val);
    }
    shippedQuantity(detailid);
  });
  $('body').on('keyup', '.outerQty', function(){
    var id = $(this).parent().parent().attr('data-id');
    var value = $(this).val();
    if (value.charAt(0) == 0) {
      var l = value.length;
      value = value.substr(1,l);
      $(this).val(value);
    }
    outerShipping(id);
  });
  $('body').on('click', '.close-receivingS', function(){
    closeReceivingsBox();
  });
  $('body').on('click', '.add-receiving-shippingD', function(){
    getReceivings(shippingId, 0);
    $('.box-shadow').css({"display":"block"});
    $('.inputReceivingS').focus();
    $('#add-receiving-box').css({"display":"block"});
    $('#add-receiving-box').css({"animation":"pop-in","animation-duration":"0.25s"});
  });
  $('body').on('click', '.search-receivingS', function(){
    var filter = $('.inputReceivingS').val();
    getReceivings(shippingId, filter);
  });
  $('body').on('click', '.receiving-box table tr', function(){
    var id = $(this).data('id');
    var idlenght = rIds.length;
    if (id != '' | id != undefined | id != null) {
      $('#receiving_'+id).find('td').addClass('td-color-select');
      if (idlenght != 0) {
        var index = rIds.findIndex(obj => obj.receivingid==id);
        if (index == -1) {
          rIds.push({
            receivingid: id
          })
        }else {
          delete rIds[index];
          rIds = rIds.filter(function(e){ return e === 0 || e });
          $('#receiving_'+id).find('td').removeClass('td-color-select');
        }
      }else {
        rIds.push({
          receivingid: id
        })
      }
    }
  });
  $('body').on('click', '.add-selected-receivingS', function(){
      var receiving_list = 0;
      var c = 0;
      for (var i = 0; i < rIds.length; i++) {
        if (c == 0) {
          c++;
          receiving_list = rIds[i].receivingid;
        }else {
          receiving_list += ',' + rIds[i].receivingid;
        }
      }
      var id_list = receivingsids[0].id;
      if (receiving_list != 0 || receiving_list != '' || receiving_list != undefined) {
        // insertReceiving(shippingId, id_list, receiving_list);
        insertReceiving.push({
          receivingId: id_list,
          receivingList: receiving_list,
          status: 2
        });
        closeReceivingsBox();
      }
  });
  $('body').on('click', '.add-part-shippingD', function(){
    getParts(shippingId, 0);
    $('.box-shadow').css({"display":"block"});
    $('.inputReceivingS').focus();
    $('#add-part-shipping-box').css({"display":"block"});
    $('#add-part-shipping-box').css({"animation":"pop-in","animation-duration":"0.25s"});
  });
  $('body').on('click', '.search-partS', function(){
    var filter = $('.inputPartS').val();
    if (filter == '' || filter == undefined || filter == null || filter == NaN) {
      filter = 0;
    }
    if (filter != 0) {
      var indexPart = productsList.findIndex(obj => obj.part==filter);
      filter = productsList[indexPart].partId;
    }
    getParts(shippingId, filter);
  });
  $('body').on('click', '.close-partS', function(){
    closePartsBox();
  });
  $('body').on('click', '.part-box table tr', function(){
    var id = $(this).data('id');
    var idlenght = pIds.length;

    if (id != '' | id != undefined | id != null) {
      $('#part_'+id).find('td').addClass('td-color-select');
      var indexR = productlistarray.findIndex(obj => obj.productId == id);
      var index = pIds.findIndex(obj => obj.productid==id);
      if (idlenght != 0) {
        if (index == -1) {
          pIds.push({
            productid: id,
            receivingdetailids: productlistarray[indexR].receivingdetailids,
            outerQty: parseInt($('#outerQty_'+id).val()),
            ids: productlistarray[indexR].ids
          })
        }else {
          delete pIds[index];
          pIds = pIds.filter(function(e){ return e === 0 || e });
          $('#part_'+id).find('td').removeClass('td-color-select');
        }
      }else {
        pIds.push({
          productid: id,
          receivingdetailids: productlistarray[indexR].receivingdetailids,
          outerQty: parseInt($('#outerQty_'+id).val()),
          ids: productlistarray[indexR].ids
        })
      }
    }
  });
  $('body').on('click', '.add-selected-partS', function(){
    var idsdate = 0;
    var receivingIds = Array();
    var date = 0;
    var ids = 0;
    var indexChara = 0;
    var qtys = 0;
    /*test*/
    console.log(pIds);
    for (var i = 0; i < pIds.length; i++) {
      pIds[i].outerQty = parseInt($('#outerQty_'+pIds[i].productid).val());
      if (qtys == 0) {
        qtys = pIds[i].outerQty;
      }else {
        qtys += ',' + pIds[i].outerQty;
      }
      if (idsdate == 0) {
        idsdate = pIds[i].receivingdetailids;
      }else {
        idsdate += ',' + pIds[i].receivingdetailids;
      }

    }
    receivingIds = idsdate.split(',');
    for (var j = 0; j < receivingIds.length; j++) {
      indexChara =  receivingIds[j].indexOf(':');
      if (ids == 0) {
        ids = receivingIds[j].substring(0,indexChara);
      }else {
        ids += ',' + receivingIds[j].substring(0,indexChara);
      }
      if (date == 0) {
        date = receivingIds[j].substring(indexChara, 15);
      }else {
        date += ',' + receivingIds[j].split(indexChara,15);
      }
    }
    /*test end*/
    // insertProducts(shippingId, ids, date);
    insertProducts.push({
      insertProducts: pIds,
      status: 2
    });
    closePartsBox();
  });
  $('body').on('click', '.deleteTracking', function(){
    var name = $(this).parent().next().text();
    var id = $(this).parent().parent().attr('data-id');
    var status = $(this).parent().parent().attr('data-status');
    var indexarray = $(this).parent().parent().attr('data-indexarray');
    showdialog(()=>{
        deleteTracking(id, status, indexarray)
    }, 'Are you sure delete tracking ' + name + '?')
  });
  $('body').on('click', '.insert-tracking-rows', function(){
    $('.box-shadow').css({"display":"block"});
    $('.add-tracking-box').css({"display":"block"});
    $('.add-tracking-box').css({"animation":"pop-in","animation-duration":"0.25s"});
    var qty = $('.trackig-rows-qty').val();
    writeTrackingRows(qty);
  });
  $('body').on('click', '.close-add-trackings', function(){
    closeTracking();
  });

  $('body').on('click', '.insert-tracking', function(){
    preinsertTrackings = [];
    var index = 0;
    var sizetrackings = 0;
    $('.add-tracking-box table tr').map(function(i){
      if (i != 0) {
        var length = $(this).find('.trackingVal').val().length;

        if (length > 0 && length != null) {
          sizetrackings = trackings.length;
          preinsertTrackings.push({
            name: $(this).find('.trackingVal').val(),
            index: sizetrackings
          })
          index = trackings.length;
          index > 0 ? index = index : index = 0;
          trackings.push({
            name: $(this).find('.trackingVal').val(),
            status: 2,
            index: index
          })
          $(this).find('.trackingVal').removeClass('input-error');
          $(this).find('.trackingVal').removeAttr('placeholder');
        }else {
          $(this).find('.trackingVal').addClass('input-error');
          $(this).find('.trackingVal').attr('placeholder', 'Please fill');
        }
      }
    });
    var section = "tracking";
    writeTrackings(section);
  });
  $('body').on('click', '.saveShipping', function(){
    var mob = $('input[name=manifest]:checked').attr('val');
    presaveShipping(mob, 'save');
  });
  $('body').on('click', '.nextShipping', function(){
    var mob = $('input[name=manifest]:checked').attr('val');
    presaveShipping(mob, 'next');
  });
});
async function convertFiles(){
  isFileUpload = true;
  console.log(isFileUpload);
  for(var i = 0; i < this.files.length; i++){
    await encodeBase64(this.files[i]);
  }
  isFileUpload = false;
}
function encodeBase64(file){
  return new Promise((resolve, reject) => {
    var reader = new FileReader();
    reader.onload = () => {
      var file64 = {};
      file64.name = file.name;
      file64.type = file.type;
      file64.fileBase64 = reader.result;
      file64.status = 2;
      file64.documentid = 0;
      file64.index = indexFile;
      files.push(file64);
      indexFile++;
      $('#documentList').append(putDocument({
        documentid: file64.documentid,
        original_name: file64.name,
        created: new Date().toString('yyyy-mm-dd hh:mm:ss'),
        description: ''
      }, file64.index));
      resolve();
    }
    if(file)
    reader.readAsDataURL(file);
    else{
      reject();
    }
  });
}
function closeTracking(){
  $('.box-shadow').css({"display":"none"});
  $('.add-tracking-box').css({"animation":"pop-out","animation-duration":"0.25s"});
  setTimeout(function(){
      $('.add-tracking-box').css({"display":"none"});
  },250);
}
function writeTrackings(section){
  switch (section) {
    case 'tracking':
      var length = preinsertTrackings.length;
      var tempCont = 0;
      var notrackings = $('.shipping-tracking-box table tr').length;
      $('.shipping-tracking-box .result-table').show();
      $('.shipping-tracking-box table').show();
      $('.not-found').hide();
      notrackings = notrackings - 1
      console.log(notrackings);
      if (notrackings <= 0) {
        $('.shipping-tracking-box table').append(`
          <tr>
            <td>Delete</td>
            <td>Tracking Number</td>
          </tr>
        `);
      }
      for (var i = 0; i < preinsertTrackings.length; i++) {
        $('.shipping-tracking-box table').append(`
          <tr id="tmpTN_${i}" data-indexarray="${preinsertTrackings[i].index}" data-id="tmpTN_${i}" data-status="2">
            <td>
              <img src="images/icons/delete.svg" class="deleteTracking">
            </td>
            <td>${preinsertTrackings[i].name}</td>
          </tr>
        `);
      }
      closeTracking();
      break;
    default:

  }
}
function insertProducts(shippingId, ids, date){
  $.ajax({
    url: '/ISS/application/index.php/shipping/insert/products',
    type: 'PUT',
    dataType: 'json',
    data: {
        idShipping: shippingId,
        insertProducts: pIds
    },
    success: (data)=>{
        switch (data.code) {
            case 200:
              console.log(data);
              break;
            case 400:
              console.log("Error 400");
              break;
          case 500:
            console.log("Error 500");
            break;
            default:
        }

    },error: (req, msg, error)=>{
      console.log(req);
    }
  }).done(()=>{
    getShipingDetails(shippingId);
    closePartsBox();
  });
}
function insertReceiving(shippingId, id_list, receiving_list){
  var dataInsert = (
      insertReceiving = {
        receivingId: id_list,
        receivingList: receiving_list
  });
  $.ajax({
    url: '/ISS/application/index.php/shipping/insert/receivings',
    type: 'PUT',
    dataType: 'json',
    data: {
        idShipping: shippingId,
        insertReceiving: dataInsert
    },
    success: (data)=>{
        switch (data.code) {
            case 200:
              console.log(data);
              break;
            case 400:
              console.log("Error 400");
              break;
          case 500:
            console.log("Error 500");
            break;
            default:
        }

    },error: (req, msg, error)=>{
      console.log(req);
    }
  }).done(()=>{
    getShipingDetails(shippingId);
    closeReceivingsBox();
  });
}
function shippedQuantity(detailid){
  for (var i = 0; i < shippingdetailsarray.length; i++) {
    if (shippingdetailsarray[i].detailid == detailid) {
      var onhand = shippingdetailsarray[i].currentqty;
      var receivedqty = shippingdetailsarray[i].receivedQty;
      var maxqty = onhand + shippingdetailsarray[i].quantity;
      var shippedqtyinsert = parseInt($('.quantitytoship_'+detailid).val());
      if (shippedqtyinsert >= 0 && shippedqtyinsert <= maxqty) {
        $('.quantitytoship_'+detailid).removeClass('input-error');
        $('.quantitytoship_'+detailid).parent().find('.message-validity').remove();
        $('.quantitytoship_'+detailid).removeAttr('placeholder');

        var weight = (shippingdetailsarray[i].totalWeight / receivedqty) * shippedqtyinsert;
        weight = parseFloat(weight).toFixed(2);
        $('.weight_'+shippingdetailsarray[i].detailid).text(weight);
      }else {
        if (shippedqtyinsert == '' || isNaN(shippedqtyinsert) || shippedqtyinsert == null || shippedqtyinsert == undefined) {
          $('.quantitytoship_'+detailid).val('0');
          $('.weight_'+shippingdetailsarray[i].detailid).text('0.00');
          $('.quantitytoship_'+detailid).removeClass('input-error');
          $('.quantitytoship_'+detailid).parent().find('.message-validity').remove();
          $('.quantitytoship_'+detailid).removeAttr('placeholder');
        }else {
          $('.quantitytoship_'+detailid).focus();
          $('.quantitytoship_'+detailid).attr('placeholder', 'Please fill');
          $('.quantitytoship_'+detailid).addClass('input-error');
          $('.quantitytoship_'+detailid).parent().append(`
            <div class="message-validity">
            <span>Invalid quantity. Max Quantity ${maxqty}</span>
            </div>
            `);
        }
      }
    }
  }
}
function outerShipping(id){
  for (var i = 0; i < productlistarray.length; i++) {
    if (productlistarray[i].productId == id) {
      var currentqty = productlistarray[i].currentQty;
      var insertqty = parseInt($('#outerQty_'+id).val());
      if (insertqty >= 0 && insertqty <= currentqty) {
        $('#outerQty_'+id).removeClass('input-error');
        $('#outerQty_'+id).parent().find('.message-validity').remove();
        $('#outerQty_'+id).removeAttr('placeholder');
      }else {
        if (insertqty == '' || isNaN(insertqty) || insertqty == null || insertqty == undefined) {
          $('#outerQty_'+id).val('0');
          $('#outerQty_'+id).removeClass('input-error');
          $('#outerQty_'+id).parent().find('.message-validity').remove();
          $('#outerQty_'+id).removeAttr('placeholder');
        } else {
          $('#outerQty_'+id).focus();
          $('#outerQty_'+id).attr('placeholder', 'Please fill');
          $('#outerQty_'+id).addClass('input-error');
          $('#outerQty_'+id).parent().append(`
            <div class="message-validity">
            <span>Invalid quantity. Max Quantity ${currentqty}</span>
            </div>
            `);
        }
      }
    }
  }
}
function writeextracargo(preinsertextraCargos){
  var tableCargos = $('#service-list table');
  for (var i = 0; i < preinsertextraCargos.length; i++) {
    if (preinsertextraCargos[i].status == 2) {
      tableCargos.append(`
        <tr data-status="2" id="tmpcharge_${[i]}" data-indexarray="${preinsertextraCargos[i].index}" data-serviceid="${preinsertextraCargos[i].serviceId }" data-cost="${preinsertextraCargos[i].cost}" data-information="${preinsertextraCargos[i].information}">
            <td class="delete-charge">
              <img src="images/icons/delete.svg" class="btn-delete-cargo">
            </td>
            <td>${preinsertextraCargos[i].code}</td>
            <td>${preinsertextraCargos[i].description}</td>
            <td>$ ${preinsertextraCargos[i].cost}</td>
        </tr>
        `);
    }
  }
}
function openSealsBox(){
    $('.box-shadow').css({"display":"block"});
    $('.seals-box').css({"display":"block"});
    $('.seals-box').css({"animation":"pop-in","animation-duration":"0.25s"});
    $('.inputSeal').focus();
}
function closeSealsBox(){
    $('.box-shadow').css({"display":"none"});
    $('.seals-box').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.seals-box').css({"display":"none"});
    },250);
    $('.inputSeal').val('');
}
function closePartsBox(){
  $('.box-shadow').css({"display":"none"});
  $('#add-part-shipping-box').css({"animation":"pop-out","animation-duration":"0.25s"});
  setTimeout(function(){
      $('#add-part-shipping-box').css({"display":"none"});
  },250);
  $('.inputPartS').val('');
}
function closeReceivingsBox(){
  $('.box-shadow').css({"display":"none"});
  $('#add-receiving-box').css({"animation":"pop-out","animation-duration":"0.25s"});
  setTimeout(function(){
      $('#add-receiving-box').css({"display":"none"});
  },250);
  $('.inputReceivingS').val('');
}
function changeButton(bol){
  var section = $('.section-control-shipping .buttons-shipping');
  if (bol == true) {
    $('.saveShipping').remove();
    $('.nextShipping').remove();
    section.append(`<input type="button" class="button save-btn nextShipping" value="Next">`);
  }else if (bol == false) {
    $('.saveShipping').remove();
    $('.nextShipping').remove();
    section.append(`<input type="button" class="button save-btn saveShipping" name="" value="Save">`);
  }
}
function manifestBillofLading(){
  var section = $('.section-control-shipping');
  $('.saveShipping').remove();
  section.append(`
    <div class="radiobuttonsShipping">
      <div>
        <input type="radio" name="manifest" id="manifestShipping" onclick="changeButton(false)" val="Manifest">
        <label for="manifestShipping">Manifest</label>
      </div>
      <div>
        <input type="radio" name="manifest" id="billofladingShipping" onclick="changeButton(true)" val="BOL" checked>
        <label for="billofladingShipping">Bill of Lading</label>
      </div>
    </div>
  `)
  section.find('.buttons-shipping').append(`
    <input type="button" class="button save-btn nextShipping" value="Next">
  `);
}
function getShipping(shippingId){
  $.ajax({
    url: '/ISS/application/index.php/shipping/get',
    type: 'GET',
    dataType: 'json',
    data: {
        idShipping: shippingId
    },
    success: (data)=>{
        switch (data.code) {
            case 200:
              console.log(data);
              customer.push({
                customerId: data.response.shippings[0].whcustomerid
              })
              var shippingData = data.response.shippings;
              var customerid = (shippingData[0].whcustomerid == null) ? shippingData[0].customerid : shippingData[0].whcustomerid;
              if (shippingData[0].whcustomerid != null) {
                isWarehouse = true;
                $('#billtoShipping').attr('data-id', shippingData[0].customerid);
                // $('#billtoShipping').attr('data-whid', shippingData[0].whcustomerid)
              }else if (customerid != ''){
                if (shippingData[0].ownInventory == 1) {
                  isWarehouse = true;
                  manifestBillofLading();
                }
              }
              if (isWarehouse != true) {
                $('.billTo-section').remove();
              }
              $('#shippingNo').text(shippingData[0].shippingno);
              $('#customeridShipping').attr('data-id', customerid);
              $('#carrierShipping').attr('data-id', shippingData[0].carrierid);
              $('#trucknumber').val(shippingData[0].trucknumber);
              $('#driver').val(shippingData[0].driver);
              $('#invoiceno').val(shippingData[0].invoice);
              $('#pedimentono').val(shippingData[0].pedimento);
              $('#creationdate').val(shippingData[0].createddate);
              $('#creationtime').timepicker({
           		 timeFormat: 'H:i:s A'
           	 }).val(shippingData[0].createdtime);
              $('#comments').text(shippingData[0].comments);
              break;
            case 400:
              console.log("Error 400");
              break;
          case 500:
            console.log("Error 500");
            break;
            default:
        }

    },error: (req, msg, error)=>{
      console.log(req);
    }
  }).done(()=>{
      var customerid = $('#customeridShipping').attr('data-id');
      var billto = $('#billtoShipping').attr('data-id');
      getCustomer(customerid);
      if (billto != undefined) {
        getCustomersExcept(customerid, billto);
      }
      getCarriers();
      getSeals(customerid);
  });
}
//Escribe los sellos
function optionSeals(si){
    var optionSeal = `<option value="${si.text}" data-value="${si.value}" title="${si.text}">`;
    return optionSeal;
}
//Escribe los servicios dentro de la tabla
function putSealsCustomer(seal, index, first = false){
    if (first) {
        return `
            <tr>
                <td>Delete</td>
                <td>Customer Seal</td>
            </tr>
        `;
    }
    else {
        return `
            <tr id="seal_${seal.value}" data-id="${seal.value}">
                <td class="delete-seal">
                    <img src="images/icons/delete.svg" alt="Edit">
                </td>
                <td class="sealname">${seal.text}</td>
            </tr>
        `;
    }
}
//Obtiene los sellos de customer e internos
function getSeals(customerid){
  var shippingid = $('#main-id').attr('data-id');
  var selectSeal = $('#sealsList');
  var tableSeals = $('.seals-box table');
  selectSeal.empty();
  $.ajax({
    url: '/ISS/application/index.php/seals/get',
    type: 'GET',
    dataType: 'json',
    data: {
        idShipping: shippingid,
        customerId: customerid
    },
    success: (data)=>{
        switch (data.code) {
            case 200:
              console.log(data);
              data.response.internal.forEach((si)=>{
                  selectSeal.append(optionSeals(si));
              });
              tableSeals.append(putSealsCustomer({}, 0, true));
              data.response.customer.forEach((sc)=>{
                  tableSeals.append(putSealsCustomer(sc));
              });
              break;
            case 400:
              console.log("Error 400");
              break;
          case 500:
            console.log("Error 500");
            break;
            default:
        }

    },error: (req, msg, error)=>{
      console.log(req);
    }
  }).done(()=>{

  });
}
//Selecciona el cliente
function selectCustomerB(){
    var shippingid = $('#main-id').attr('data-id');
    if (shippingid != '') {
        var selectCB = $('.selectBillTo');
        var actualOption = selectCB.attr('data-id');
        selectCB.val(actualOption);
    }
}
//Escribe los clientes
function optionCustomerB(c){
    var optionsCustomer = `<option value="${c.customerid}" data-value="${c.name}" title="${c.name}">${c.name}</option>`;
    return optionsCustomer;
}
function getCustomersExcept(customerid, billto){
  var selectCB = $('.selectBillTo');
  $.ajax({
      url: `/ISS/application/index.php/customer/get/except`,
      type: "GET",
      dataType: 'json',
      data: {
          idCustomer: customerid,
          idBillto: billto
      },
      success: data=>{
          switch (data.code) {
              case 200:
                  console.log(data);
                  data.response.customers.forEach((c)=>{
                      selectCB.append(optionCustomerB(c));
                  });
                  break;
              case 404:
                  console.log(data);
                  break;
              default:
          }
      }, error: (req, msg, error)=>{
        console.log(req);
      }
  }).done(()=>{
      selectCustomerB();
  });
}
function getCustomer(customerid){
  $.ajax({
      url: `/ISS/application/index.php/customer/get/by`,
      type: "GET",
      data: {
        customerId: customerid
      },
      dataType: 'json',
      success: data=>{
          switch (data.code) {
              case 200:
              console.log(data);
                  var customer =  data.response.customers;
                  $('#customeridShipping').text(customer[0].name);
                  break;
              case 404:
                  console.log(data);
                  break;
              default:
          }
      }, error: (req, msg, error)=>{
        console.log(req);
      }
  }).done(()=>{

  });
}
//Selecciona el carrier
function selectCarrierC(){
    var shippingid = $('#main-id').attr('data-id');
    if (shippingid != '') {
        var selectCarrier = $('.selectCarrier');
        var actualOption = selectCarrier.attr('data-id');
        selectCarrier.val(actualOption);
    }
}
//Escribe los carriers
function optionCarrier(carrier){
    var optionsCustomer = `<option value="${carrier.carrierid}" data-value="${carrier.name}" title="${carrier.name}">${carrier.name}</option>`;
    return optionsCustomer;
}
//Obtiene los carriers
function getCarriers(){
  var selectCarrier = $('.selectCarrier');
  $.ajax({
      url: `/ISS/application/index.php/carrier/get`,
      type: "GET",
      dataType: 'json',
      success: data=>{
          switch (data.code) {
              case 200:
                  data.response.carriers.forEach((carrier)=>{
                      selectCarrier.append(optionCarrier(carrier));
                  });
                  break;
              case 404:
                  console.log(data);
                  break;
              default:
          }
      }, error: (req, msg, error)=>{
        console.log(req);
      }
  }).done(()=>{
      selectCarrierC();
  });
}
//function create json with add extra cargos and delete extra cargos
function getExtraCargos(){
  $('#service-list table tr').map(function(i){
    if (i != 0) {
      var status = $(this).data('status');
      if (status == 0) {
        extraCargos.push({
          serviceshippingid: $(this).data('id'),
          status: 0
        })
      }
      if (status == 2) {
        extraCargos.push({
          serviceid: $(this).data('serviceid'),
          cost: $(this).data('cost'),
          information: $(this).data('information'),
          status: 2
        });
      }
    }
  });
}
function optionDetails(sd, index, first = false){
  if (first) {
    return `
    <tr>
      <td>No.</td>
      <td>Receiving No.</td>
      <td>Internal Rec No.</td>
      <td>Tracking No.</td>
      <td>Decription</td>
      <td>Part No.</td>
      <td>Package Type</td>
      <td>Received Qty</td>
      <td>OnHand</td>
      <td>Shipped Qty</td>
      <td>Weight</td>
      <td>Unit of Mesurement</td>
      <td>Location</td>
      <td>Lot Number</td>
      <td>PO</td>
    </tr>
    `
  }else {
    return `
    <tr class="itemsd sdid" data-id="${sd.shippingdetailid}" data-line="${index}" data-status="1" data-change="0">
      <td>${sd.no}</td>
      <td>${sd.receivingno}</td>
      <td>${sd.freightbill}</td>
      <td>${sd.tracking}</td>
      <td>${sd.descriptionpart}</td>
      <td>${sd.productno}</td>
      <td>${sd.nameunit}</td>
      <td>${sd.originalquantity}</td>
      <td class="currentqty">${sd.currentquantity}</td>
      <td class="qtyshipped" data-qty="${sd.quantity}">
        <input type="number" class="quantitytoship_${sd.shippingdetailid}" value="${sd.quantity}" size="8" maxlength="10">
      </td>
      <td class="weight_${sd.shippingdetailid}">${sd.weight}</td>
      <td>${sd.unitMesurament}</td>
      <td>${sd.location}</td>
      <td>${sd.lotnumber}</td>
      <td>${sd.po}</td>
    </tr>
    `;
  }
}
//Function get shipping details
function getShipingDetails(shippingId){
  var tableShippingDetails = $('.shipping-details-box table');
  tableShippingDetails.empty();
  $.ajax({
    url: '/ISS/application/index.php/shipping/get/details',
    type: 'GET',
    dataType: 'json',
    data: {
        idShipping: shippingId
    },
    success: (data)=>{
        switch (data.code) {
            case 200:
              console.log(data);
              tableShippingDetails.append(optionDetails({}, 0, true));
              data.response.details.forEach((sd)=>{
                  tableShippingDetails.append(optionDetails(sd));
                  shippingdetailsarray.push({
                    detailid: sd.shippingdetailid,
                    currentqty: sd.currentquantity,
                    quantity: sd.quantity,
                    totalWeight: sd.totalWeight,
                    receivedQty: sd.originalquantity
                  })
              });
              break;
            case 400:
              console.log("Error 400");
              break;
          case 500:
            console.log("Error 500");
            break;
            default:
        }

    },error: (req, msg, error)=>{
      console.log(req);
    }
  }).done(()=>{
    openloadingcontent();
  });
}
function putReceivings(r, index, first = false){
  if (first) {
    return `
      <tr>
        <td>Rec. No.</td>
        <td>Internal Rec. No.</td>
        <td>Created Date</td>
        <td>Customer</td>
        <td>Vendor</td>
        <td>Carrier</td>
      </tr>
    `;
  }else {
    return `
      <tr id="receiving_${r.receivingid}" data-id="${r.receivingid}">
        <td>${r.receivingno}</td>
        <td>${r.freightbill}</td>
        <td>${r.createddate}</td>
        <td>${r.customername}</td>
        <td>${r.vendorid}</td>
        <td>${r.carrierid}</td>
      </tr>
    `;
  }
}
function getReceivings(shippingId, filter){
  if (filter == 0) {filter = '';}
  console.log(filter);
  var tableReceivings = $('#add-receiving-box table');
  tableReceivings.empty();
  $.ajax({
      url: `/ISS/application/index.php/shipping/get/receivings`,
      type: "GET",
      data: {
        shippingId: shippingId,
        filter: filter
      },
      dataType: 'json',
      success: data=>{
          switch (data.code) {
              case 200:
                  console.log(data);
                  tableReceivings.append(putReceivings({}, 0, true));
                  data.response.receivings.forEach((r)=>{
                      tableReceivings.append(putReceivings(r));
                  });
                  receivingsids.push({
                    id: data.response.receivingsids
                  })
                  break;
              case 404:
                  console.log(data);
                  break;
              default:
          }
      }, error: (req, msg, error)=>{
        console.log(req);
      }
  }).done(()=>{

  });
}
function putParts(p, index, first = false){
  if (first) {
    return `
      <tr>
        <td>Part No.</td>
        <td>Sub Part</td>
        <td>Current Outer</td>
        <td>Outer Qty to Shipping</td>
      </tr>
    `;
  }else {
    return `
      <tr id="part_${p.productid}" data-id="${p.productid}">
        <td>${p.productno}</td>
        <td>${p.name}</td>
        <td>${p.currentquantity}</td>
        <td>
          <input type="number" id="outerQty_${p.productid}" class="outerQty" size="10" value="0">
        </td>
      </tr>
    `;
  }
}
//Escribe los productos en el datalis
function optionsPart(lp){
    var optionsPart = `<option value="${lp.part}" data-value="${lp.partid}" title="${lp.part}">`;
    return optionsPart;
}
function getParts(shippingId, filter){
  if (filter == 0) {filter = '';}
  var tableParts = $('#add-part-shipping-box table');
  tableParts.empty();
  var inputParts = $('#partList');
  inputParts.empty();
  var customerid = customer[0].customerId;
  $.ajax({
      url: `/ISS/application/index.php/shipping/get/parts`,
      type: "GET",
      data: {
        shippingId: shippingId,
        filter: filter,
        customerId: customerid
      },
      dataType: 'json',
      success: data=>{
          switch (data.code) {
              case 200:
                  console.log(data);
                  tableParts.append(putParts({}, 0, true));
                  data.response.parts.forEach((p)=>{
                      tableParts.append(putParts(p));
                      productlistarray.push({
                        productId: p.productid,
                        currentQty: p.currentquantity,
                        receivingdetailids: p.receivingdetailids,
                        ids: p.ids
                      });
                  });
                  data.response.listparts.forEach((lp)=>{
                    inputParts.append(optionsPart(lp));
                    productsList.push({
                      partId: lp.partid,
                      part: lp.part
                    });
                  });
                  break;
              case 404:
                  console.log(data);
                  break;
              default:
          }
      }, error: (req, msg, error)=>{
        console.log(req);
      }
  }).done(()=>{

  });
}
function putTrackings(t, index, first = false){
  if (first) {
    return `
      <tr>
        <td>Delete</td>
        <td>Tracking Number</td>
      </tr>
    `;
  }else {
    return `
      <tr id="tracking_${t.trackingid}" data-id="${t.trackingid}" data-status="1">
        <td>
          <img src="images/icons/delete.svg" class="deleteTracking">
        </td>
        <td>${t.trackingnumber}</td>
      </tr>
    `;
  }
}
function getTrackings(shippingId){
  var tableTrackings = $('.shipping-tracking-box table');
  $.ajax({
      url: `/ISS/application/index.php/get/tracking`,
      type: "GET",
      data: {
        shippingId: shippingId
      },
      dataType: 'json',
      success: data=>{
          switch (data.code) {
              case 200:
                  if (data.response.trackings.length > 0) {
                    tableTrackings.append(putTrackings({}, 0, true));
                    data.response.trackings.forEach((t)=>{
                      tableTrackings.append(putTrackings(t));
                    });
                  } else {
                    $('.shipping-tracking-box .result-table').hide();
                    $('.shipping-tracking-box .not-found').addClass('flex');
                  }
                  break;
              case 404:
                  console.log(data);
                  break;
              default:
          }
      }, error: (req, msg, error)=>{
        console.log(req);
      }
  }).done(()=>{
    openloadingcontent();
  });
}
function deleteTracking(id, status, indexarray){
  if (status == 1) {
    trackings.push({
      status: 0,
      idTracking: id
    })
  }
  if (status == 2) {
    $('#'+id).remove();
    delete trackings[indexarray];
    trackings = trackings.filter(function(e){ return e === 0 || e });
    for (var i = 0; i < trackings.length; i++) {
      trackings[i].index = i;
      var c = 0;
      $('.shipping-tracking-box table tr').each(function(j){
        if (j != 0) {
          var s = $(this).attr('data-status');
          if (s == 2) {
            $(this).attr('data-indexarray', c);
            c++;
          }
        }
      });
    }
  }
  $('#tracking_'+id).remove();
}
function writeTrackingRows(qty){
  if (qty == '' || qty <= 0) {
    qty = 1;
  }
  var tableAddTracking = $('.add-tracking-box table');
  tableAddTracking.empty();
  qty = parseInt(qty) + 1;
  var c = 1;
  tableAddTracking.append(`
      <tr>
          <td>No</td>
          <td>Delete</td>
          <td>Tracking Number</td>
      </tr>`);
  while (c < qty) {
    tableAddTracking.append(`
        <tr>
            <td>${c}</td>
            <td><img src="images/icons/delete.svg" class="delete-tmp-tracking"></td>
            <td><input type="text" class="trackingVal" value="" maxlength="50" placeholder="Tracking Number"/></td>
        </tr>`);
        c++;
  }
}
function putDocument(document, index, first=false){
    if(first){
        return `
        <tr>
            <td>View</td>
            <td>Delete</td>
            <td>File</td>
            <td>Created</td>
        </tr>`;
    }
    return `
    <tr data-documentid=${document.documentid} data-index=${index} id="Doc_${document.documentid}">
        <td>
          <a href="/ISS/application/index.php/document/show?documentId=${document.documentid} " target="upload">
            <img src="/ISS/images/icons/view.svg" width="16" height="16" alt="View">
          </a>
        </td>
        <td>
          <img class="delete-file" src="images/icons/delete${(document.description.localeCompare('EDI') == 0) ? '_disabled' : ''}.svg" alt="Delete">
        </td>
        <td>
          ${(document.description.localeCompare('EDI') == 0) ? 'EDI FILE' : document.original_name}
        </td>
        <td>
          ${document.created}
        </td>
    </tr>`;
}
//Obtiene los documentos de shipping
function getDocuments(shippingId){
  var tableDocuments = $('#documentList');
  tableDocuments.empty();
  $.ajax({
    url: '/ISS/application/index.php/document/shipping/get',
    type: 'GET',
    dataType: 'json',
    data: {
        type: 'shipping',
        typekey: 'shippingid',
        type_recid: shippingId
    },
    success: (data)=>{
        switch (data.code) {
            case 200:
              console.log(data);
              tableDocuments.append(putDocument({}, 0, true));
              data.response.documents.forEach((document)=>{
                  tableDocuments.append(putDocument(document, -1));
              })
              break;
            case 400:
              console.log("Error 400");
              break;
          case 500:
            console.log("Error 500");
            break;
            default:
        }

    },error: (req, msg, error)=>{
      console.log(req);
    }
  }).done(()=>{

  });
}
function presaveShipping(mob, action){
  var error = 0;
  var carrier = $('#carrierShipping option:selected').val();
  if (carrier == 'none' || carrier == NaN || carrier == undefined || carrier == null) {
    $('#carrierShipping').addClass('input-error');
    error = 1;
  }
  if (isWarehouse == true && action == 'save') {
    var billto = $('#billtoShipping option:selected').val();
    if (billto == 'none' || billto == NaN || billto == undefined || billto == null) {
      $('#billtoShipping').addClass('input-error');
      error = 1;
    }
    if (error == 0) {
      showdialog(()=>{
        saveShipping(mob, action)
      }, 'You selected "Manifest" as your document type, this will cause your invoice to be incomplete, are you sure you want to continue?')
    }else if (error == 1) {
      $('body,html').animate({ scrollTop: 200 }, 700);
    }
  } else if (isWarehouse == false && action == 'save') {
    if (error == 0) {
      showdialog(()=>{
        saveShipping(mob, action)
      }, 'Aye you sure save this Shipping?')
    }else if (error == 1) {
      $('body,html').animate({ scrollTop: 200 }, 700);
    }
  }
  else if(action == 'next'){
    if (error == 0) {
      showdialog(()=>{
        saveShipping(mob, action)
      }, 'Aye you sure save this Shipping?')
    }else if (error == 1) {
      $('body,html').animate({ scrollTop: 200 }, 700);
    }
  }
}
function getDataShipping(){
  var carrier = $('#carrierShipping option:selected').val();
  var pedimento = $('#pedimentono').val();
  var creationdate = $('#creationdate').val();
  var creationtime = $('#creationtime').val();
  var trucknumber = $('#trucknumber').val();
  var driver = $('#driver').val();
  var invoiceno = $('#invoiceno').val();
  var comments = $('#comments').val();
  shipping.push({
    carrierid: carrier,
    pedimentono: pedimento,
    creationdate: creationdate,
    creationtime: creationtime,
    trucknumber: trucknumber,
    driver: driver,
    invoiceno: invoiceno,
    comments: comments
  });
}
//funtion save shipping
function saveShipping(mob, action){
  var test = 0;
  extraCargos = [];
  shipping = [];
  getExtraCargos();
  getDataShipping();
  JsonShipping = {
    shippingId: $('#main-id').attr('data-id'),
    documents: files,
    shipping,
    trackings,
    insertReceiving,
    insertProducts
  };
  console.log(JsonShipping);
  if (test != 1) {
    $.ajax({
      url: '/ISS/application/index.php/shipping/update',
      type: 'PUT',
      dataType: 'json',
      data: JSON.stringify(JsonShipping),
      success: (data)=>{
        console.log(data);
        var id = data.response.shippingId;
        // window.location.href = 'http://localhost/ISS/shipping_summary.php?shippingid='+id;
        switch (data.code) {
          case 400:
            if (data.response.ident == 'tracking') {
              alert(data.response.message);
            }
            break;
          case 406:
            if (data.response.ident == 'tracking') {
              alert(data.response.message);
            }
            break;
          default:

        }
      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
  }
}

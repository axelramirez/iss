$(document).ready(function() {
  $('body').on('click', '#btn_services', function(){
    var customerid = $('#customeridShipping').attr('data-id');
    var shippingId = $('#main-id').attr('data-id');
    $('.box-shadow').css({"display":"block"});
    $('.chargesShipping').css({"display":"block"});
    $('.chargesShipping').css({"animation":"pop-in","animation-duration":"0.25s"});
    getAppliedCharges(shippingId);
    getCharges(customerid);
  });
  $('body').on('change', '.selectCharge', function(){
    var datacode = $('.selectCharge option:selected').attr('data-code');
    var description = $('.selectCharge option:selected').attr('data-description');
    var cost = $('.selectCharge option:selected').attr('data-cost');
    var variant = $('.selectCharge option:selected').attr('data-variant');
    $('#service_code').text(datacode);
    $('#description-service').text(description);
    $('#service_cost').text(cost);
    $('#service_variant').val(variant);
    $('.applied-charges').hide();
    $('.description-charges').show();
  });
  $('body').on('click', '.cancel-add-charge', function(){
    $('.selectCharge').prop('selectedIndex',0);
    $('.applied-charges').show();
    $('.description-charges').hide();
    $('#service_information').val('');
    $('#service_variant').val('0');
    $('#service_qty').val('1');
  });
  $('body').on('click', '.add-charge', function(){
    $('.selectCharge').prop('selectedIndex',0);
    $('.applied-charges').show();
    $('.description-charges').hide();
  });
  $('body').on('click', '.close-extra-charges', function(){
    $('.box-shadow').css({"display":"none"});
    $('.chargesShipping').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.chargesShipping').css({"display":"none"});
    },250);
  });
});
//Escribe los cargos
function optionCharges(charge, index, first = false){
  if (first) {
    return `<option value="none">Select service</option>`;
  }
  else {
    return `<option value="${charge.serviceid}" data-increment="${charge.increment}" data-value="${charge.serviceid}" data-code="${charge.code}" data-description="${charge.description}" data-cost="${charge.cost}" data-variant="${charge.variant}" title="${charge.description}">${charge.description}</option>`;
  }
}
function getCharges(customerid){
  var selectCharge = $('.selectCharge');
  selectCharge.empty();
  $.ajax({
    url: '/ISS/application/index.php/shipping/get/charges',
    type: 'GET',
    dataType: 'json',
    data: {
        customerId: customerid
    },
    success: (data)=>{
        switch (data.code) {
            case 200:
              console.log(data);
              selectCharge.append(optionCharges({}, 0, true));
              data.response.charges.forEach((charge)=>{
                  selectCharge.append(optionCharges(charge));
              });
              break;
            case 400:
              console.log("Error 400");
              break;
          case 500:
            console.log("Error 500");
            break;
            default:
        }

    },error: (req, msg, error)=>{
      console.log(req);
    }
  }).done(()=>{

  });
}
//Escribe los cargos aplicados en un shipping
function optionChargesApplied(charges, index, first = false){
  if (first) {
    return `
    <tr>
     <td>Delete</td>
     <td>Code</td>
     <td>Description</td>
     <td>Cost</td>
    </tr>`;
  }
  else {
    return `
    <tr data-id="${charges.serviceid}" data-status="1" id="rowAS_${charges.serviceid}">
      <td class="delete-charge"><img src="images/icons/delete.svg"></td>
      <td>${charges.code}</td>
      <td>${charges.description}</td>
      <td>$${charges.cost}</td>
    </tr>`;
  }
}
function getAppliedCharges(shippingId){
  var tableCargos = $('#service-list table');
  tableCargos.empty();
  $.ajax({
    url: '/ISS/application/index.php/shipping/charges/applied',
    type: 'GET',
    dataType: 'json',
    data: {
        shippingId: shippingId
    },
    success: (data)=>{
        switch (data.code) {
            case 200:
              console.log(data);
              tableCargos.append(optionChargesApplied({}, 0, true));
              data.response.charges.forEach((charges)=>{
                  tableCargos.append(optionChargesApplied(charges));
              });
              console.log(details);
              for (var i = 0; i < details.length; i++) {
                if (details[i].status == 0) {
                  $('#rowAS_' + details[i].serviceshippingid).hide();
                  $('#rowAS_' + details[i].serviceshippingid).data('status', 0);
                }
                if (details[i].status == 2) {
                  tableCargos.append(`
                    <tr data-status="2" id="tmpcharge_${[i]}" data-indexarray=${details[i].index} data-serviceid="${details[i].serviceId}" data-cost="${details[i].cost}" data-information="${details[i].information}">
                      <td class="delete-charge"><img src="images/icons/delete.svg"></td>
                      <td>${details[i].code}</td>
                      <td>${details[i].description}</td>
                      <td>$${details[i].cost}</td>
                    </tr>
                    `);
                }
              }
              break;
            case 400:
              console.log("Error 400");
              break;
          case 500:
            console.log("Error 500");
            break;
            default:
        }

    },error: (req, msg, error)=>{
      console.log(req);
    }
  }).done(()=>{

  });
}

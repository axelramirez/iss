function dump(obj) {
    var out = '';
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }
    var pre = document.createElement('pre');
    pre.innerHTML = out;
    document.body.appendChild(pre)
}

// implement JSON.stringify serialization
JSON.stringify = JSON.stringify || function(obj) {
    var t = typeof (obj);
    if (t != "object" || obj === null) {
        // simple data type
        if (t == "string")
            obj = '"' + obj + '"';
        return String(obj);
    }
    else {
        // recurse array or object
        var n, v, json = [], arr = (obj && obj.constructor == Array);
        for (n in obj) {
            v = obj[n];
            t = typeof (v);
            if (t == "string")
                v = '"' + v + '"';
            else if (t == "object" && v !== null)
                v = JSON.stringify(v);
            json.push((arr ? "" : '"' + n + '":') + String(v));
        }
        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
};
//////////////////////

function LoadSelect(select, json) {
    if (json != null)
        //loading select with json
        $.each(json.result, function(i, v) {
            select.append($("<option/>", {
                value: v.value,
                text: v.text
            }));
        });
    if (typeof select.attr("default") === "string") {
        select.append($("<option/>", {
            value: select.attr("default"),
            text: select.attr("default"),
            selected: "selected"
        }));
    } else {
        select.attr("defaultVal", select.find('option:eq(0)').val());
        select.attr("defaultText", select.find('option:eq(0)').text());
    }
}
(function($) {
    $.widget("ui.comboboxui", {
        // default options
        options: {
            json: null
        },
          _create: function() {
            var self = this;
            var select = this.element.hide();
            var select_id = select.attr('id');
            var json = self.options.json;
            LoadSelect(select, json);
            var selected = select.children(":selected");
            var value = selected.val() ? selected.text() : "";
            var input = $("<input class = 'comboboxui'/>")
                    .attr('id', 'comboboxui_' + select_id)
                    .attr('name', 'comboboxui_' + select_id)
                    .addClass("ui-state-default ui-combobox-input ui-widget ui-widget-content ui-corner-left")
                          .insertAfter(select)
                          .val(value)
                          .autocomplete({
                                delay: 0,
                                minLength: 0,
                                source: function(request, response) {
                                      var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                                      response(select.children("option").map(function() {
                                            var text = $(this).text();
                                            if (this.value && (!request.term || matcher.test(text)))
                                                  return {
                                                        label: text.replace(
                                                                  new RegExp(
                                                                            "(?![^&;]+;)(?!<[^<>]*)(" +
                                                                            $.ui.autocomplete.escapeRegex(request.term) +
                                                                            ")(?![^<>]*>)(?![^&;]+;)", "gi"),
                                                                  "<strong>$1</strong>"),
                                                        value: text,
                                                        option: this
                                                  };
                                      }));
                                },
                                select: function(event, ui) {
                                      ui.item.option.selected = true;
                                      self._trigger("selected", event, {
                                            item: ui.item.option
                                      });
                                },
                                change: function(event, ui) {
                            if (typeof ui == 'undefined') {
                                valid = false;
                            } else if (!ui.item) {
                                var valueLowerCase = $(this).val().trim().toLowerCase(),
                                        valid = false;
                                select.children("option").each(function() {
                                    if ($(this).text().toLowerCase() === valueLowerCase) {
                                        this.selected = valid = true;
                                    }
                                });

                            } else {
                                valid = true;
                            }

                            if (!valid) {
                                // remove invalid value, as it didn't match anything
                                $(this).val(select.attr(
                                        typeof select.attr("default") === "string"
                                        ? "default" : "defaultText"));
                                select.val(select.attr(
                                        typeof select.attr("default") === "string"
                                        ? "default" : "defaultVal"));
                            }

                            select.change();
                            return false;
                                },
                        focus: function(event, ui) {

                        }
                          })
                          .addClass("ui-widget ui-widget-content ui-corner-left");

            input.bind('focus', function() {
                if ($(this).val() == select.attr("default")) {
                    $(this).val('');
                }
            });
               
                input.data("autocomplete")._renderItem = function(ul, item) {
                      return $("<li></li>")
                                .data("item.autocomplete", item)
                                .append("<a>" + item.label + "</a>")
                                .appendTo(ul);
                };
               
                $("<button> </button>")
                        .attr("tabIndex", -1)
                        .attr("title", "Show All Items")
                        .insertAfter(input)
                        .button({
                              icons: {
                                    primary: "ui-icon-triangle-1-s"
                              },
                              text: false
                        })
                        .removeClass("ui-corner-all")
                        .addClass("ui-corner-right ui-button-icon")
                        .click(function(event) {
                        event.preventDefault();
                              // close if already visible
                              if (input.autocomplete("widget").is(":visible")) {
                                    input.autocomplete("close");
                                    return;
                              }
                              // pass empty string as value to search for, displaying all results
                              input.autocomplete("search", "");
                              input.focus();
                        });
          }
    });
})(jQuery);


   
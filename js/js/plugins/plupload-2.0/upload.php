<?php

//Data Base Settings
$mysql = FALSE;
$mysqli = TRUE;

// chdir up to root.
while (!file_exists("database.php"))
    chdir('..');
require_once ('database.php');

function ErrorLog($valuesAry) {
    $date = date("F j, Y, H:i:s");
    $logNumber = (intval(date('W')) % 2) + 1;
    $logStr = "UploadFilesLogError$logNumber.log";
    $weekOfYear = intval(date('W'));
    $exist = file_exists($logStr);
    if ($exist == 1) {
        $handle = fopen($logStr, "rb");
        $weekNumber = intval(fgets($handle));
        fclose($handle);
        $modeStr = $weekNumber == $weekOfYear ? "ab+" : "wb+";
    } else {
        $modeStr = "wb+";
    }
    $fp = fopen($logStr, $modeStr);
    if ($modeStr == "wb+")
        fwrite($fp, $weekOfYear . "\r\n");
    fwrite($fp, "********** $date ********** \r\n");
    foreach ($valuesAry as $key => $value) {
        fwrite($fp, "$key => $value \r\n");
    }
    fclose($fp);
}

function DocumentInsert($mysqli, $type, $type_recid, $type_key, $description, $filename, $file_path, $original_name, $mime_type) {
    $sqlInsert = "INSERT INTO document (type, type_recid, type_key, description"
            . ", name, file_path, original_name, mime_type)
				VALUES ('$type',
				$type_recid,
				'$type_key',
				'$description',
				'$filename',
				'$file_path',
				'$original_name',
				'$mime_type')";
    if (!$mysqli->query($sqlInsert))
        ErrorLog(array('file type' => 'RecShipFile', 'error' => $mysqli->error
            , 'query' => $sqlInsert));
}

function PreAlertFiles($mysqli, $parentId, $name, $tmpName, $path, $documentType, $customerid, $userid) {
    $preAlertFilesInsert = "INSERT INTO prealertfiles"
            . " (`idPreAlert`, `fileNo`, `name`, `tmpName`, `path`, `documentType`)"
            . " SELECT '$parentId', IF(MAX(fileNo) IS NULL, 1 ,MAX(fileNo)+1),"
            . " '$name', '$tmpName', '$path', '$documentType'"
            . " FROM prealertfiles WHERE idPreAlert = '$parentId'";
    if ($mysqli->query($preAlertFilesInsert)) {
        $preAlertNo = 'PA' . (base_convert(substr($parentId, 2), 36, 10));
        $information = "File: $name Was Created";
        $ip = $_SERVER['REMOTE_ADDR'];
        $add_log = "INSERT INTO log (type, action, information"
                . ", idCustomer, idUser, ip)"
                . " VALUES ('PA', 'A', '$information'"
                . ", $customerid, $userid, '$ip') ";
        if ($mysqli->query($add_log)) {
            $idLog = $mysqli->insert_id;
            $add_log = "INSERT INTO log_prealerts (idLog, idPreAlert, number)"
                    . " VALUES ($idLog, '$parentId', '$preAlertNo')";
            if (!$mysqli->query($add_log))
                ErrorLog(array('preAlert' => 'log_prealerts', 'error' => $mysqli->error
                    , 'query' => $add_log));
        } else {
            ErrorLog(array('preAlert' => 'Filelog', 'error' => $mysqli->error
                , 'query' => $add_log));
        }
    } else
        ErrorLog(array('file type' => 'preAlertFile', 'error' => $mysqli->error
            , 'query' => $preAlertFilesInsert));
}

function get_path() {
    $ini_array = parse_ini_file("settings/settings.ini", true);
    return $ini_array['path']['tempdir'];
}

/**
 * upload.php
 *
 * Copyright 2013, Moxiecode Systems AB
 * Released under GPL License.
 *
 * License: http://www.plupload.com/license
 * Contributing: http://www.plupload.com/contributing
 */
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

// 5 minutes execution time
@set_time_limit(5 * 60);

// Settings
$type = $_REQUEST["type"];
$parentId = (isset($_REQUEST["parentId"]) ? $_REQUEST["parentId"] :
                (isset($_REQUEST["idType"]) ? $_REQUEST["idType"] : NULL));

$targetDir = isset($_REQUEST["parentId"]) ?
        realpath(get_path()) . DIRECTORY_SEPARATOR . $parentId : get_path();
$targetDir = str_replace('\\', '/', $targetDir);

// Create target dir
if (!file_exists($targetDir)) {
    @mkdir($targetDir);
}

// Settings
$temp = tempnam($targetDir, "upload_");
$tmpName = basename($temp);
$filePath = $temp;


// Get a file type
if ($type != 'PreAlertBol' && $type != 'PreAlertRegular') {
    if (isset($_REQUEST["mimeType"])) {
        $mimeType = $_REQUEST["mimeType"];
    } elseif (!empty($_FILES)) {
        $mimeType = $_FILES["file"]["type"];
    } else {
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $mimeType = $finfo->buffer(file_get_contents("php://input"));
        unset($finfo);
    }
} else {
    $customerid = $_REQUEST["customerid"];
    $userid = $_REQUEST["userid"];
}

// Get a file name
if (isset($_REQUEST["name"])) {
    $fileName = $_REQUEST["name"];
} elseif (!empty($_FILES)) {
    $fileName = $_FILES["file"]["name"];
} else {
    $fileName = uniqid("file_");
}

// Chunking might be enabled
$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

// Open temp file
if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
    die('{"jsonrpc" : "2.0", "error" : {"code": 102'
            . ', "message": "Failed to open output stream."}, "id" : "id"}');
}

if (!empty($_FILES)) {
    if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
        die('{"jsonrpc" : "2.0", "error" : {"code": 103'
                . ', "message": "Failed to move uploaded file."}, "id" : "id"}');
    }

    // Read binary input stream and append it to temp file
    if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
        die('{"jsonrpc" : "2.0", "error" : {"code": 101'
                . ', "message": "Failed to open input stream."}, "id" : "id"}');
    }
} else {
    if (!$in = @fopen("php://input", "rb")) {
        die('{"jsonrpc" : "2.0", "error" : {"code": 101'
                . ', "message": "Failed to open input stream."}, "id" : "id"}');
    }
}

while ($buff = fread($in, 4096)) {
    fwrite($out, $buff);
}

@fclose($out);
@fclose($in);

if ($type == 'PreAlertBol' || $type == 'PreAlertRegular')
    PreAlertFiles($mysqli, $parentId, $fileName, $tmpName, $targetDir, substr(strtolower($type), 8), $customerid, $userid);
else if ($type == 'receiving' || $type == 'shipping')
    DocumentInsert($mysqli, $type, $parentId, $type . 'id', 'Upload File', $tmpName, $targetDir, $fileName, $mimeType);

// Check if file has been uploaded
if (!$chunks || $chunk == $chunks - 1) {
    // Strip the temp .part suffix off 
    rename("{$filePath}.part", $filePath);
}
// Return Success JSON-RPC response
die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');


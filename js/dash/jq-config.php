<?php

$iniFile = parse_ini_file("settings/settings.ini");
$host = $iniFile['host'];
$user = $iniFile['user'];
$pass = $iniFile['pass'];
$dataBase = $iniFile['db'];
define('DB_DSN',"mysql:host=$host;dbname=$dataBase");
define('DB_USER', $user);
define('DB_PASSWORD', $pass);
define('ABSPATH', dirname(__FILE__).'/');
?>

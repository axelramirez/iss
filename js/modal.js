$(document).ready(function() {

	//*****************Receiving search**********************************************/
	//Cerrar dialogo de partes
	$(".close-dialog-part").click(function(){
		$(".dialog-shadow").css({"display":"none"});
	});


	//Clean inputs search
	$(".clear-search").click(function(){
		$('#part-option').hide();
	});
	//

	$(function() {
		$('#customerid').change();
	});
	//
	$('.newps').click(function(){
		$('.content-box').css({"display":"none"});
		$('.options-search').css({"display":"block"});
	});
	//Ocultar y mostrar mas opciones de busqueda
	// var sap = 0;
	// $(".option-advanced").click(function(){
	// 	if(sap == 0){
	// 		$(".option-advanced p").css({"color":"rgb(255, 86, 86)"});
	// 		$(".advanced-s").css({"display":"block","opacity":"1"});
	// 		$(".section-date").css({"display":"flex","opacity":"1"});
	// 		$(".text-search").text('Simple Search');
	// 		sap = 1;
	// 	}
	// 	else{
	// 		sap = 0;
	// 		$(".option-advanced p").css({"color":"#40a3ff"});
	// 		$(".advanced-s").css({"display":"none","opacity":"0"});
	// 		$(".text-search").text('Advanced Search');
	// 	}
	// });
	//
	$(".close-extra-chargues").click(function(){
		$(".dialog-shadow-add-services").css({"display":"none"});
	});

	var optionst = 0;

	// $('.button-options').click(function(){
	// 	if(optionst == 0){
	// 		$(".shadow-table").css({"display":"block"});
	// 		$(this).find('.options-table').css({"display":"block"});
	// 		optionst = 1;
	// 	}
	// 	else{
	// 		optionst = 0;
	// 		$('.options-table').css({"display":"none"});
	// 		$(".shadow-table").css({"display":"none"});
	// 	}
	//
	// });

	/*******************************End receiving search***************************************/
	//select all the a tag with name equal to modal
	$('a[name=modal]').click(function(e) {
		//Cancel the link behavior
		e.preventDefault();

		//Get the A tag
		var id = $(this).attr('href');

		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();

		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});

		//transition effect
		$('#mask').fadeIn(1000);
		$('#mask').fadeTo("slow",0.8);

		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();

		//Set the popup window to center
		$(id).css('top',  winH/2-$(id).height()/2);
		$(id).css('left', winW/2-$(id).width()/2);

		//transition effect
		$(id).fadeIn(2000);

	});

	//if close button is clicked
	$('.window .close').click(function (e) {
		//Cancel the link behavior
		e.preventDefault();

		$('#mask').hide();
		$('.window').hide();
	});

	//if mask is clicked
	$('#mask').click(function () {
		$(this).hide();
		$('.window').hide();
	});
	function recargar(t){
		console.log("tracking" + t);
	$.post("cargar.php", { track: t }, function(data){ console.log(data); $("#dialog").html(data);});
	}
});

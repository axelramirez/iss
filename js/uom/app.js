var numberpage = 1;
$(document).ready(function() {
    getUOM();
    //Abre el dialogo de Create Location
    $('body').on('click', '.add-uom', function(){
        $('.title-dialog h2').text('Create UOM');
        $('.save-btn').val('Create');
        $('.save-btn').removeClass('update-uom');
        $('.save-btn').addClass('insert-uom');
        openUomBox();
    });
    $('body').on('click', '.edit-uom img', function(){
        $('.title-dialog h2').text('Edit UOM');
        var id = $(this).parent().parent().attr('data-id');
        var name = $(this).parent().parent().find('.uomdescription').text();

        openUomBox();
        $('.uomname input').val(name);
        $('.update-uom').attr('data-id', id);
    });
    //Cierra el dialogo de service y limpia los inputs
    $('body').on('click', '.close-uom', function(){
        closeUomBox();
    });
    $('body').on('click', '.cancel-insert-uom', function(){
        closeUomBox();
    });
    $('body').on('click', '.insert-uom', function(e){
      var inpObj = document.getElementById('uomName');
      if (!inpObj.checkValidity()) {
        $('.uomname input').addClass('input-error');
        $('.uomname input').attr('placeholder', 'Please fill');
        $('.uomname input').focus();
      }else {
        e.preventDefault();
        var uomname = $('.uomname input').val();
        if (uomname.length <= 50) {
          insertUom();
        }else {
          console.log('Please not edit the max length');
        }
      }
    });
    $('body').on('click', '.update-uom', function(e){
      var inpObj = document.getElementById('uomName');
      if (!inpObj.checkValidity()) {
        $('.uomname input').addClass('input-error');
        $('.uomname input').attr('placeholder', 'Please fill');
        $('.uomname input').focus();
      }else {
        e.preventDefault();
        var uomname = $('.uomname input').val();
        if (uomname.length <= 50) {
          updateUom();
        }else {
          console.log('Please not edit the max length');
        }
      }
    });
    $('body').on('click', '.next-uom-list', function(){
        numberpage++;
        getUOM();
    });
    $('body').on('click', '.back-uom-list', function(){
        if (numberpage > 1) {
            numberpage--;
            getUOM();
        }
    });
});
//Abre el contenedor para insertar/actualizar uom
function openUomBox(){
    $('.box-shadow').css({"display":"block"});
    $('.add-uom-box').css({"display":"block"});
    $('.add-uom-box').css({"animation":"pop-in","animation-duration":"0.25s"});
}
//Cierra el contenedor para insertar/actualizar uom
function closeUomBox(){
    $('.box-shadow').css({"display":"none"});
    $('.add-uom-box').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.add-uom-box').css({"display":"none"});
    },250);
    $('.uomname input').val('');
    $('.uomname input').removeClass('input-error');
    $('.uomname input').attr('placeholder', '');
    resetClass();
}
//Resetea las clases del contenedor insertar/actualizar uom
function resetClass(){
    $('.save-btn').removeClass('insert-uom');
    $('.save-btn').addClass('update-uom');
}
//Fucnion para insertar uom
function insertUom(){
    var description = $('.uomname input').val();
    $.ajax({
      url: '/ISS/application/index.php/uom/insert',
      type: 'PUT',
      dataType: 'json',
      data: {
          uomName: description
      },
      success: (data)=>{
          getUOM();
          closeUomBox();
          shownotify(0);
      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}
//Funcion para actualizar uom
function updateUom(){
    var description = $('.uomname input').val();
    var id = $('.update-uom').attr('data-id');
    $.ajax({
      url: '/ISS/application/index.php/uom/update',
      type: 'PUT',
      dataType: 'json',
      data: {
          uomName: description,
          uomId: id
      },
      success: (data)=>{
          switch (data.code) {
              case 200:
                  getUOM();
                  closeUomBox();
                  shownotify(0);
                  break;
              case 400:
                console.log("ERROR");
                  break;
              default:

          }
      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}
//Escribe los uoms dentro de la tabla
function putUom(uom, index, first = false){
    if (first) {
        return `
            <tr>
                <td>Edit</td>
                <td>Description</td>
            </tr>
        `;
    }
    else {
        return `
            <tr id="uom_${uom.uomid}" data-id="${uom.uomid}">
                <td class="edit-uom">
                    <img src="images/icons/edit.svg" alt="Edit">
                </td>
                <td class="uomdescription">${uom.description}</td>
            </tr>
        `;
    }
}
//Obtiene los uoms con el metodo ajax y los itera en la funcion putUom
function getUOM(){
    var tableUOM = $('.uom-box table');
    tableUOM.empty();
    $.ajax({
        url: `/ISS/application/index.php/uom/get`,
        type: "GET",
        dataType: 'json',
        data: {
            page: numberpage
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    tableUOM.append(putUom({}, 0, true));
                    data.response.uom.forEach((uom)=>{
                        tableUOM.append(putUom(uom));
                    });
                    var entries = data.response.numberEntries;
                    var pages = data.response.numberPages;
                    $('.entries-uom .entries').html(entries + ' entries');
                    $('.entries-uom .pages').html(pages + ' Page(s)');
                    $('.actual-page p').html('Page ' + numberpage);
                    //Ocultar y mostrar flechas de navegacion
                    if (numberpage == pages) {$('.next-uom-list').hide();}
                    else {$('.next-uom-list').show();}
                    if (numberpage > 1) {$('.back-uom-list').show();}
                    else {$('.back-uom-list').hide();}
                    break;
                case 404:
                    console.log(data);
                    break;
                default:
            }
        }, error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{
        loadingcatalogs();
    });
}

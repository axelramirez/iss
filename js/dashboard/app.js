var numberpage = 1;
$(document).ready(function() {
  getProductsDashBoard();
});

//Escribe los productos dentro de la tabla
function putProducts(product, index, first = false){
    if (first) {
      return `
        <tr>
          <td>Imagen</td>
          <td>Producto</td>
          <td>Descripción</td>
          <td>Cantidad</td>
        </tr>
      `;
    }
    else {
      return `
          <tr id="product_${product.id}" class="item-dashboard">
              <td class="icon">
                <img src="${product.image}">
              </td>
              <td class="name">
                <p>${product.name}</p>
              </td>
              <td class="description">
                <p>${product.description}</p>
              </td>
              <td class="quantity">
                <p>${product.quantity}</p>
              </td>
          </tr>
      `;
    }
}
//Obtiene los 10 productos con menor cantidad en inventario
function getProductsDashBoard(){
    var sectionProduct = $('.products-quantity table');
    sectionProduct.empty();
    $.ajax({
        url: `/ISS/application/index.php/dashboard/get`,
        type: "GET",
        dataType: 'json',
        success: data=>{
            switch (data.code) {
                case 200:
                    console.log(data);
                    sectionProduct.append(putProducts({}, 0, true));
                    data.response.products.forEach((product)=>{
                        sectionProduct.append(putProducts(product));
                        if (product.quantity <= 99) {
                          $('#product_' + product.id).find('.quantity p').css({"color":"red"});
                        }
                        if (product.quantity >= 100 && product.quantity <= 199) {
                          $('#product_' + product.id).find('.quantity p').css({"color":"orange"});
                        }
                        if (product.quantity >= 200) {
                          $('#product_' + product.id).find('.quantity p').css({"color":"rgb(21, 189, 43)"});
                        }
                    });
                    break;
                case 404:
                    console.log(data);
                    break;
                default:
            }
        }, error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{
    });
}

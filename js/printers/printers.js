
numberpage = 1;
$(document).ready(function()
{

    main_printers();
    getPrinters();

    $('body').on('click','input#save-printer',function(){
           insertPrinter();
    });

    $('body').on('click', '#add-printer', function(){



           //$('div.form-printer').css({"padding":"1px 40% 1px 40%"});

            $('body div.form-printer').html(`
                    <div class="row">
                        <div class="col-40 printer_name">
                            <Label>Printer Name</Label>
                            <input id="printer_name" type="text" required>
                            <div id="frm_printer_name"><span id="val_printer_name"></span></div>
                        </div>
                    </div>
                    <div class="row">
                        <Label>Printer IP</Label>
                    </div>
                    <div class="row">
                        <div class="col-20 printer_ip">
                            <input id="printer_ip1"  type="text" onkeyup="eventChar('printer_ip1','save_printer',1,3,'only numbers')" required>
                            <div id="frm_printer_ip1"><span id="val_printer_ip1"></span></div>
                        </div>
                        <div class="col-20 printer_ip">
                            <input id="printer_ip2"  type="text" onkeyup="eventChar('printer_ip2','save-printer',1,3,'only numbers')" required>
                            <div id="frm_printer_ip2"><span id="val_printer_ip2"></span></div>
                        </div>
                        <div class="col-20 printer_ip">
                            <input id="printer_ip3" type="text" onkeyup="eventChar('printer_ip3','save-printer',1,3,'only numbers')" required>
                            <div id="frm_printer_ip3"><span id="val_printer_ip3"></span></div>
                        </div>
                        <div class="col-20 printer_ip">
                            <input id="printer_ip4" type="text" onkeyup="eventChar('printer_ip4','save-printer',1,3,'only numbers')" required>
                            <div id="frm_printer_ip4"><span id="val_printer_ip4"></span></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-40 printer_description">
                            <Label>Description</Label>
                            <textarea id="description" onkeyup="" rows="4" cols="50">
                            </textarea>
                            <div class="data-printer"><span id="val_printer_description"></span></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-40 printer_default">
                            <Label>Default</Label>
                            <select name="default" id="default" class="field_input roleid">
                                <option value="0">----</option>
                                <option value="1">Default</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-20">
                            <input id="save-printer" type="button" class="button save-btn update-printer" value="Create">
                        </div>
                        <div class="col-20">
                            <input type="button" class="button cancel-btn cancel-insert-uom close-printer" value="Cancel">
                        </div>
                    </div>
                    `);

            openPrinterBox('save-printer');
    });

    $('body').on('click', '#edit-printerid img', function(){



        //$('div.form-printer').css({"padding":"1px 40% 1px 40%"});

         $('body div.form-printer').html(`
                 <div class="row">
                     <div class="col-40 printer_name">
                         <Label>Printer Name</Label>
                         <input id="printer_name" type="text" onkeyup="eventChar('printer_name','update-printer',3,50,'Minimum 3 characters, first character capital letter')" required>
                         <div id="frm_printer_name"><span id="val_printer_name"></span></div>
                     </div>
                 </div>
                 <div class="row">
                     <Label>Printer IP</Label>
                 </div>
                 <div class="row">
                     <div class="col-20 printer_ip">
                        <input id="printer_ip1"  type="text" onkeyup="eventChar('printer_ip1','save_printer',1,3,'only numbers')" required>
                        <div id="frm_printer_ip1"><span id="val_printer_ip1"></span></div>
                     </div>
                     <div class="col-20 printer_ip">
                        <input id="printer_ip2"  type="text" onkeyup="eventChar('printer_ip2','save-printer',1,3,'only numbers')" required>
                        <div id="frm_printer_ip2"><span id="val_printer_ip2"></span></div>
                     </div>
                     <div class="col-20 printer_ip">
                        <input id="printer_ip3" type="text" onkeyup="eventChar('printer_ip3','save-printer',1,3,'only numbers')" required>
                        <div id="frm_printer_ip3"><span id="val_printer_ip3"></span></div>
                     </div>
                     <div class="col-20 printer_ip">
                        <input id="printer_ip4" type="text" onkeyup="eventChar('printer_ip4','save-printer',1,3,'only numbers')" required>
                        <div id="frm_printer_ip4"><span id="val_printer_ip4"></span></div>
                     </div>
                 </div>
                 <div class="row">
                     <div class="col-40 printer_description">
                         <Label>Description</Label>
                         <textarea id="description" onkeyup="" rows="4" cols="50">
                         </textarea>
                         <div class="data-printer"><span id="val_printer_description"></span></div>
                     </div>
                 </div>
                 <div class="row">
                     <div class="col-40 printer_default">
                         <Label>Default</Label>

                         <select name="default" id="default" class="field_input roleid">
                            <option value="0">----</option>
                            <option value="1">Default</option>
                         </select>
                     </div>
                 </div>
                 <div class="row">
                     <div class="col-20">
                         <input id="update-printer" type="button" class="button save-btn update-printer" value="Update">
                     </div>
                     <div class="col-20">
                         <input type="button" class="button cancel-btn cancel-insert-uom close-printer" value="Cancel">
                     </div>
                 </div>
                 `);


                    $('.title-dialog h2').text('Edit Printer');

                    var id = $(this).parent().parent().attr('data-id');
                    var name = $(this).parent().parent().find('.printer-name').text();
                    var ip = $(this).parent().parent().find('.printer-ip').text().split(".");
                    var description = $(this).parent().parent().find('.printer-description').text();
                    var def = $(this).parent().parent().find('.printer-default').text();

                    $('#printer_name').val(name);
                    $('#printer_ip1').val(ip[0]);
                    $('#printer_ip2').val(ip[1]);
                    $('#printer_ip3').val(ip[2]);
                    $('#printer_ip4').val(ip[3]);
                    $('#description').val(description);
                    $('select#default').val(def);
                    $('body input.update-printer').attr('data-id',id);

         openPrinterBox('update-printer');
 });

    $('body').on('click', 'input#update-printer', function(){
        updatePrinter();
    });

    $('body').on('click','.delete-printer img',function(){
        deletePrinter($(this).parent().parent().attr('data-id'));
    });

    $('body').on('click','.button-go',function(){
        getPrinters($('.printername').val(),$('.printerip').val());
    });

    $('body').on('click', '.close-printer', function(){
        closePrinterBox();
    });


    $('body').on('click', '.next-printer-list', function(){
        numberpage++;
        getPrinters();
    });

    $('body').on('click', '.back-printer-list', function(){
        if (numberpage > 1) {
            numberpage--;
            getPrinters();
        }
    });

});

function main_printers(){


    $('div.add-printer-box').css({"display":"none"});
    $('div.add-printer-box').css({"position":"absolute"});
    $('div.add-printer-box').css({"left":"0"});
    $('div.add-printer-box').css({"right":"0"});
    $('div.add-printer-box').css({"top":"0"});
    $('div.add-printer-box').css({"bottom":"0"});
    $('div.add-printer-box').css({"margin":"auto"});
    $('div.add-printer-box').css({"width":"30%"});
    $('div.add-printer-box').css({"height":"70%"});
    $('div.add-printer-box').css({"background":"#fff"});
    $('div.add-printer-box').css({"z-index":"102"});
    $('div.add-printer-box').css({"padding":"25px"});
    $('div.add-printer-box').css({"border":"1px solid #e4e4e4"});


    $('.form-inline').css (
        "display","flex",
        "flex-flow","row wrap",
        "align-items","center"
      );



    $('div.col-20').css("float","left");
    $('div.col-20').css("height","40");
    $('div.col-20').css("margin-top","20px");
    $('div.col-20').css("padding","3px 40px 3px 40px");

    $('div.col-40').css("float","left");
    $('div.col-40').css("height","40");
    $('div.col-40').css("margin-top","20px");
    $('div.col-40').css("padding","3px 40px 3px 40px");

    $('div.col-50').css("float","left");
    $('div.col-50').css("height","50%");
    $('div.col-50').css("margin-top","20px");
    $('div.col-50').css("padding","3px 40px 3px 40px");

    $('body input.button-go').css('width','55px');



    $("td.edit-printer img").css(
        {"width" : "20px"},
        {"height" : "auto"},
        {"cursor" : "pointer"}
    )

    $('.table-printer').css(
        {"table-layout":"fixed"}
    );

    $('.back-printer-list').css(
        {"transform": "rotate(180deg)"},
        {"padding": "3px"},
    );

    $('div.row:after').css(
        {"content" : ""},
        {"display" : "table"},
        {"clear"   : "both"}
    );


}


function openPrinterBox(idButton){
    $('.box-shadow').css({"display":"block"});
    $('.add-printer-box').css({"display":"block"});
    $('.add-printer-box').css({"animation":"pop-in","animation-duration":"0.25s"});
    'Minimum 3 characters, first character capital letter'
    if(idButton == 'save-printer'){
        $('body .printer_name input').attr("data-flag",0);
        $('body .printer_ip input').attr("data-flag",0);
    }
    if(idButton == 'update-printer'){
        $('body .printer_name input').attr("data-flag",1);
        $('body .printer_ip input').attr("data-flag",1);
    }

    setEventInput('printer_name',idButton,3,50,'Minimum 3 characters, first character capital letter');
    setEventInput('printer_ip1',idButton,1,3,'Only Numbers');
    setEventInput('printer_ip2',idButton,1,3,'Only Numbers');
    setEventInput('printer_ip3',idButton,1,3,'Only Numbers');
    setEventInput('printer_ip4',idButton,1,3,'Only Numbers');
    setButton(idButton);

}

function closePrinterBox(){
    $('.box-shadow').css({"display":"none"});
    $('.add-printer-box').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.add-printer-box').css({"display":"none"});
    },250);

    //$('body div.form-printer').html(``);

}

function setImage(def){
    if(def == 1)
     return "default.jpg";
    return  "no-default.png"
}

function putPrinter(printer, index, first = false){


    if (first) {
        return `
                <tr>
                    <td>Edit</td>
                    <td>Delete</td>
                    <td>Printer Name</td>
                    <td>Printer IP</td>
                    <td>Printer Description</td>
                    <td>Default</td>

                </tr>
        `;
    }
    else {
        return `
                <tr id="printer_${printer.printerid}" data-id="${printer.printerid}">
                    <td id="edit-printerid" class="edit-printer" data-id="${printer.printerid}">
                        <img src="images/icons/edit.svg" alt="Edit">
                    </td>
                    <td class="delete-printer">
                        <img src="images/icons/delete.svg" alt="Delete">
                    </td>
                    <td class="printer-name">${printer.printername}</td>
                    <td class="printer-ip">${printer.ip}</td>
                    <td class="printer-description">${printer.des}</td>
                    <td class="printer-default"> <img src="images/${printer.def == 1 ? "default.jpg" : "no_default.png"}"></td>
                </tr>
        `;
    }
}

function getPrinters(printer_name="",printer_ip=""){
    var table = $('.table-printer');
    table.empty();
    $.ajax({
        url: `/ISS/application/index.php/printers/get`,
        type: "GET",
        dataType: 'json',
        data: {
            page: numberpage,
            printer_name: printer_name,
            printer_ip: printer_ip,
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    table.append(putPrinter({}, 0, true));
                    var index = 0;
                    data.response.printer.forEach((printer)=>{
                        table.append(putPrinter(printer,index));
                        index++;
                    });
                    var entries = data.response.numberEntries;
                    var pages = data.response.numberPages;
                    $('.entries-printer .entries').html(entries + ' entries');
                    $('.entries-printer .pages').html(pages + ' Page(s)');
                    $('.actual-page p').html('Page ' + numberpage);
                    //Ocultar y mostrar flechas de navegacion
                    if (numberpage == pages) {$('.next-printer-list').hide();}
                    else {$('.next-printer-list').show();}
                    if (numberpage > 1) {$('.back-printer-list').show();}
                    else {$('.back-printer-list').hide();}
                    break;
                case 404:
                    console.log(data);
                    break;
                default:
            }
        }, error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{
        loadingcatalogs();
    });
}

function insertPrinter(){

    $.ajax({
      url: '/ISS/application/index.php/printers/insert',
      type: 'PUT',
      dataType: 'json',
      data: {
          printer_name: $('input#printer_name').val() ,
          printer_ip:   $('input#printer_ip1').val() + "." + $('input#printer_ip2').val() + "." + $('input#printer_ip3').val() + "." + $('input#printer_ip4').val(),
          description:  $.trim($('textarea#description').val()),
          default:      $('select#default').val()
      },
      success: (data)=>{
          getPrinters();
          closePrinterBox();
          shownotify(0);
      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}

function updatePrinter(){
    $.ajax({
        url: '/ISS/application/index.php/printers/updatePrinter',
        type: 'PUT',
        dataType: 'json',
        data: {
            printer_id:   parseInt($('input#update-printer').data('id')),
            printer_name: $('input#printer_name').val(),
            printer_ip:   $('input#printer_ip1').val() + "." + $('input#printer_ip2').val() + "." + $('input#printer_ip3').val() + "." + $('input#printer_ip4').val(),
            description:  $.trim($('textarea#description').val()),
            default:      $('select#default').val()
        },
        success: (data)=>{
            getPrinters();
            closePrinterBox();
            shownotify(0);
        },
        error: (req, msg, error)=>{
          console.log(req);
          console.log(req.responseText);
        }
      })
}

function deletePrinter(printer_id){
    $.ajax({
      url: '/ISS/application/index.php/printers/delete',
      type: 'DELETE',
      dataType: 'json',
      data: {
          printer_id: printer_id
      },
      success: (data)=>{
          switch (data.code) {
              case 200:
                  getPrinters();
                  closePrinterBox();
                  shownotify(1);
                  break;
              case 400:
                console.log("Error 400");
                break;
            case 500:
              console.log("Error 500");
              break;
              default:
          }

      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}

function validateC(value) {
    var re =/^[a-zA-Z]+$/;
    return re.test(value);
}

function validateCN(value) {
    var re =/^[a-zA-Z0-9-_* ]+$/;
    return re.test(value);
}

function validateM(value) {
    var re =/^[A-Z]+$/;
    return re.test(value);
}

function validateN(value) {
    var re =/^[0-9]+$/;
    return re.test(value);
}

function getFlagValidation(idInput,min,max){
    var value = $('#' + idInput).val();

        if(idInput == 'printer_name')
            return (validateCN(value) && validateM(value[0]) && (value.length >= min && value.length <= max));

        if(idInput == 'printer_ip1' || idInput == 'printer_ip2' || idInput == 'printer_ip3' || idInput == 'printer_ip4')
            return (validateN(value) && (value.length >= min && value.length <= max));

        return false;
 }

 function setMessage(idInput,msg,visible){
    $('div#frm_' + idInput).css("background-color","hsl(0, 0%, 71%)");
    $('div#frm_' + idInput).css("color", "white");
    $('div#frm_' + idInput).width($('#' + idInput).width());
    $('div#frm_' + idInput).css("border","none");
    $('div#frm_' + idInput).css("border-radius","50px");
    $('div#frm_' + idInput).css("text-align","center");
    $('div#frm_' + idInput).css("padding","5px 0px 5px 0px");
    $('div#frm_' + idInput).css("display",visible);
    $('body span#val_' + idInput).text(msg);
    $('body span#val_' + idInput).css("font-size","15px");

 }

 function setButton(idButton){

   if($('input#printer_name').attr('data-flag') == 1 && $('input#printer_ip1').attr('data-flag') == 1 && $('input#printer_ip2').attr('data-flag') == 1 && $('input#printer_ip3').attr('data-flag') == 1 && $('input#printer_ip4').attr('data-flag') == 1 ){
       // This part is for when the message is empty, activate create button

       document.getElementById(idButton).disabled = false;
       $('.save-btn').css("background", "#3197ff");
   }
   if($('input#printer_name').attr('data-flag') == 0 || $('input#printer_ip1').attr('data-flag') == 0 || $('input#printer_ip2').attr('data-flag') == 0 || $('input#printer_ip3').attr('data-flag') == 0 || $('input#printer_ip4').attr('data-flag') == 0){

       document.getElementById(idButton).disabled = true;
       $('.save-btn').css("background", "#878f99");
   }

}

function setEventInput(idInput,idButton,min,max,msg){

    $('body').on('keyup','input#'+ idInput,function(){
         eventChar(idInput,idButton,min,max,msg);

    });
}

 function eventChar(idInput,idButton="",min,max,msg=""){

    if(getFlagValidation(idInput,min,max)){
        $('#' + idInput).attr("data-flag",1);

        $('#' + idInput).css("background-color","white");
        $('#' + idInput).css("color","black");
        setMessage(idInput,msg,"none");
    }
    else{
        $('#' + idInput).attr("data-flag",0);

        $('#' + idInput).css("background-color","hsl(0, 100%, 75%)");
        $('#' + idInput).css("color","white");
        setMessage(idInput,msg,"block");
    }

    setButton(idButton);

 }

$(document).ready(function() {
	jQuery.browser = {};
	(function () {
	    jQuery.browser.msie = false;
	    jQuery.browser.version = 0;
	    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
	        jQuery.browser.msie = true;
	        jQuery.browser.version = RegExp.$1;
	    }
	})();
	let g = "color:#777;font-weight:bold;";
	let b = "color:#0F669D;font-weight:bold;";
	let systemsMessageStyle="font-size:21px; font-weight:200; letter-spacing:0.2em; line-height:1.4em; font-family:helvetica,arial; color:rgba(0,0,25,0.5);";
	console.log("%cSTOP! No edit", "border-radius: 4px; background: #3197ff; font-weight: bold; padding: 5px; box-sizing: border-box; color: white; font-size: x-large");
	console.log("%csystems communications", systemsMessageStyle);

	let textChrome = `%c
%c	XXXXXXXX
%c	XX                          XX
%c	XX                          XX
%c	XXXXXXXX XX    XX XXXXXXX XXXXXXX  XXXXXXX XXXXXXXXXX XXXXXXX
%c	      XX XX    XX XX        XX     XX   XX XX  XX  XX XX
%c	      XX XXXXXXXX XXXXXXX   XX     XXXXXXX XX  XX  XX XXXXXXX
%c	      XX       XX      XX   XX     XX      XX      XX      XX
%c	XXXXXXXX XXXXXXXX XXXXXXX   XXXXXX XXXXXXX XX      XX XXXXXXX
`;
console.log(textChrome, b);
	//Menu
	var sw = screen.width;
	var cm = 0;
	$('.subnav').click(function(){
		if(cm == 0){
			$(this).find('ul').css({"display":"block"});
			if(sw <= 1295){
				$(this).find('span').css({"transform":"rotate(-90deg)","transition":"0.5s"});
			}
			else{
				$(this).find('span').css({"transform":"rotatex(180deg)","transition":"0.5s"});
			}
			cm = 1;
		}
		else{
			cm = 0;
			$('#newHeader ul li ul').css({"display":"none"});
            $('#newHeader ul li span').css({"transform":"rotatex(0deg)","transition":"0.5s"});
		}

	});
	$('#newHeader li').click(function() {
		if ($(this).attr('link'))
			window.location.href = $(this).attr('link');
	});
	$('body').on('click', '.back-view', function(){
		 window.history.back();
	});
    //Responsive menu
    var rm = 0;
	$('.btn-menu').click(function(){
		if(rm == 0){
			$('#newHeader ul > div:nth-child(2)').css({"display":"block"});
			$('#newHeader ul > .btn-menu').css({"justify-content":"center"});
			$('#newHeader ul > .btn-menu > div:nth-child(2)').css({"display":"none","transition":"0.5s"});
			$('#newHeader ul > .btn-menu > div:nth-child(1)').css({"transform":"rotate(-45deg)","margin-top":"0px", "transition":"0.5s"});
			$('#newHeader ul > .btn-menu > div:nth-child(3)').css({"transform":"rotate(45deg)","margin-bottom":"0px","transition":"0.5s"});

			rm = 1;
		}
		else{
			rm = 0;
			$('#newHeader ul > div:nth-child(2)').css({"display":"none"});
			$('#newHeader ul > .btn-menu').css({"justify-content":"space-around"});
			$('#newHeader ul > .btn-menu > div:nth-child(2)').css({"display":"flex","transition":"0.5s"});
			$('#newHeader ul > .btn-menu > div:nth-child(1)').css({"transform":"rotate(0deg)","margin-top":"5px", "transition":"0.5s"});
			$('#newHeader ul > .btn-menu > div:nth-child(3)').css({"transform":"rotate(0deg)","margin-bottom":"5px","transition":"0.5s"});
		}
	});

   $('body').on('mousedown','.datepicker', function(){
	   $(this).datepicker({ dateFormat: 'yy-mm-dd' }).val();
   });
	 // $('body').on('mousedown','.datepickerHour', function(){
		//  console.log('hi');
   // });
   $('body').on('click', '.button-options img', function(){
	  $(this).next().css({"display":"block"});
	   $(this).next().css({"animation":"pop-in","animation-duration":"0.25s"});
	  $('.invisible-box').show();
   });
   $('body').on('click', '.invisible-box', function(){
	  $('.invisible-box').hide();
	  $('.options-table').css({"animation":"pop-out","animation-duration":"0.25s"});
	  setTimeout(function(){
		  $('.options-table').css({"display":"none"});
	  },250);
   });
});
/************************************Loading**************************************/
function openloading(){
	$('body').css({"overflow":"hidden"});
	$('.loading-box').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"loading-in","animation-duration":"0.01s"});
    $('.loading-box svg').css({"display":"block","animation":"pop-in","animation-duration":"0.25s"});
	setTimeout(function(){
		$('.loading-box svg').css({"animation":"pop-out","animation-duration":"0.25s"});
		setTimeout(function(){
			$('.loading-box svg').css({"display":"none"});
		},220);
		$('.loading-box').css({"animation":"loading-out","animation-duration":"0.45s"});
		setTimeout(function(){
			$('.loading-box').css({"display":"none"});
		},450);

	},1000);
	$('body').css({"overflow":"auto"});
}
function openloadingcontent(){
	$('.content-loading').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"loading-in","animation-duration":"0.01s"});
    $('.content-loading svg').css({"display":"block","animation":"content-pop-in","animation-duration":"0.25s"});
	setTimeout(function(){
		$('.content-loading svg').css({"animation":"content-pop-out","animation-duration":"0.25s"});
		setTimeout(function(){
			$('.content-loading svg').css({"display":"none"});
		},220);
		$('.content-loading').css({"animation":"loading-out","animation-duration":"0.45s"});
		setTimeout(function(){
			$('.content-loading').css({"display":"none"});
		},450);
	},500);
}
function loadingpurchasesorders(){
	$('.content-loading-po').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"loading-in","animation-duration":"0.01s"});
    $('.content-loading-po svg').css({"display":"block","animation":"content-pop-in","animation-duration":"0.25s"});
	setTimeout(function(){
		$('.content-loading-po svg').css({"animation":"content-pop-out","animation-duration":"0.25s"});
		setTimeout(function(){
			$('.content-loading-po svg').css({"display":"none"});
		},220);
		$('.content-loading-po').css({"animation":"loading-out","animation-duration":"0.45s"});
		setTimeout(function(){
			$('.content-loading-po').css({"display":"none"});
		},450);
	},500);
}
function loadingreceivingdetails(){
	$('.content-loading-rd').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"loading-in","animation-duration":"0.01s"});
    $('.content-loading-rd svg').css({"display":"block","animation":"content-pop-in","animation-duration":"0.25s"});
	setTimeout(function(){
		$('.content-loading-rd svg').css({"animation":"content-pop-out","animation-duration":"0.25s"});
		setTimeout(function(){
			$('.content-loading-rd svg').css({"display":"none"});
		},220);
		$('.content-loading-rd').css({"animation":"loading-out","animation-duration":"0.45s"});
		setTimeout(function(){
			$('.content-loading-rd').css({"display":"none"});
		},450);
	},500);
}
function loadingdetailsviewreceiving(){
	$('.content-loading-details-view').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"loading-in","animation-duration":"0.01s", "z-index":"1"});
    $('.content-loading-details-view svg').css({"display":"block","animation":"content-pop-in","animation-duration":"0.25s"});
	setTimeout(function(){
		$('.content-loading-details-view svg').css({"animation":"content-pop-out","animation-duration":"0.25s"});
		setTimeout(function(){
			$('.content-loading-details-view svg').css({"display":"none"});
		},220);
		$('.content-loading-details-view').css({"animation":"loading-out","animation-duration":"0.45s"});
		setTimeout(function(){
			$('.content-loading-details-view').css({"display":"none"});
		},450);
	},500);
}
function loadingfreightbills(){
	$('.content-loading-fb').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"loading-in","animation-duration":"0.01s"});
    $('.content-loading-fb svg').css({"display":"block","animation":"content-pop-in","animation-duration":"0.25s"});
	setTimeout(function(){
		$('.content-loading-fb svg').css({"animation":"content-pop-out","animation-duration":"0.25s"});
		setTimeout(function(){
			$('.content-loading-fb svg').css({"display":"none"});
		},220);
		$('.content-loading-fb').css({"animation":"loading-out","animation-duration":"0.45s"});
		setTimeout(function(){
			$('.content-loading-fb').css({"display":"none"});
		},450);
	},500);
}
function loadingtande(){
	$('.content-loading-td').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"loading-in","animation-duration":"0.01s"});
    $('.content-loading-td svg').css({"display":"block","animation":"content-pop-in","animation-duration":"0.25s"});
	setTimeout(function(){
		$('.content-loading-td svg').css({"animation":"content-pop-out","animation-duration":"0.25s"});
		setTimeout(function(){
			$('.content-loading-td svg').css({"display":"none"});
		},220);
		$('.content-loading-td').css({"animation":"loading-out","animation-duration":"0.45s"});
		setTimeout(function(){
			$('.content-loading-td').css({"display":"none"});
		},450);
	},500);
}
function loadingservices(){
	$('.loading-services').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"loading-in","animation-duration":"0.01s"});
    $('.loading-services svg').css({"display":"block","animation":"content-pop-in","animation-duration":"0.25s"});
	setTimeout(function(){
		$('.loading-services svg').css({"animation":"content-pop-out","animation-duration":"0.25s"});
		setTimeout(function(){
			$('.loading-services svg').css({"display":"none"});
		},220);
		$('.loading-services').css({"animation":"loading-out","animation-duration":"0.45s"});
		setTimeout(function(){
			$('.loading-services').css({"display":"none"});
		},450);
	},500);
}
function loadingdocks(){
	$('.loading-dock').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"loading-in","animation-duration":"0.01s"});
    $('.loading-dock svg').css({"display":"block","animation":"content-pop-in","animation-duration":"0.25s"});
	setTimeout(function(){
		$('.loading-dock svg').css({"animation":"content-pop-out","animation-duration":"0.25s"});
		setTimeout(function(){
			$('.loading-dock svg').css({"display":"none"});
		},220);
		$('.loading-dock').css({"animation":"loading-out","animation-duration":"0.45s"});
		setTimeout(function(){
			$('.loading-dock').css({"display":"none"});
		},450);
	},500);
}
function loadinglocations(){
	$('.loading-location').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"loading-in","animation-duration":"0.01s"});
    $('.loading-location svg').css({"display":"block","animation":"content-pop-in","animation-duration":"0.25s"});
	setTimeout(function(){
		$('.loading-location svg').css({"animation":"content-pop-out","animation-duration":"0.25s"});
		setTimeout(function(){
			$('.loading-location svg').css({"display":"none"});
		},220);
		$('.loading-location').css({"animation":"loading-out","animation-duration":"0.45s"});
		setTimeout(function(){
			$('.loading-location').css({"display":"none"});
		},450);
	},500);
}
function loadingcatalogs(){
	$('.loading-catalog').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"loading-in","animation-duration":"0.01s"});
    $('.loading-catalog svg').css({"display":"block","animation":"content-pop-in","animation-duration":"0.25s"});
	setTimeout(function(){
		$('.loading-catalog svg').css({"animation":"content-pop-out","animation-duration":"0.25s"});
		setTimeout(function(){
			$('.loading-catalog svg').css({"display":"none"});
		},220);
		$('.loading-catalog').css({"animation":"loading-out","animation-duration":"0.45s"});
		setTimeout(function(){
			$('.loading-catalog').css({"display":"none"});
		},450);
	},500);
}
/*******************************Mostrar notificaciones**********************************/
$('.btn-action-save').click(function(){
    showdialog(()=>{
        shownotify(0);
    }, 'Are you sure save?');
});
$('.btn-action-delete').click(function(){
    showdialog(()=>{
        shownotify(1);
    }, 'Are you sure delete?');
});
$('.btn-action-edit').click(function(){
    showdialog(()=>{
        shownotify(2);
    }, 'Are you sure update?');
  });
/*****************************************************************************Dialogo de confirmacion********************************************/
function showdialog(callback, message){
    $('.message-dialog-not').html(message);

    $('.box-confirmation-not').css({"-webkit-display":"flex","-moz-display":"flex","-ms-display":"flex","-o-display":"flex","display":"flex", "animation":"pop-in", "animation-duration":"0.25s"});
    $('.shadow-notify').css({"display":"block"});


    $('.yes-confirmation').bind("click", function(){
        callback();
				$('.box-confirmation-not').css({"animation":"pop-out","animation-duration":"0.25s"});
		    setTimeout(function(){
		        $('.box-confirmation-not').css({"display":"none"});
		    },250);
        $('.shadow-notify').css({"display":"none"});
        $('.yes-confirmation').unbind("click");
    });
    $('.no-confirmation').click(function(){
				$('.box-confirmation-not').css({"animation":"pop-out","animation-duration":"0.25s"});
		    setTimeout(function(){
		        $('.box-confirmation-not').css({"display":"none"});
		    },250);
        $('.shadow-notify').css({"display":"none"});
        $('.yes-confirmation').unbind("click");
    });
}
/******************************************************Notifucaciones de acciones*********************************************************************/
function shownotify(type, time = 3000){
    selectnotify(type, "notify-in");
    setTimeout(function(){
        selectnotify(type, "notify-out");
        setTimeout(function(){
            selectnotify(type, "notify-in", false);
            selectnotify(type, "notify-out", false);
        }, 370);
    }, time);
}
function selectnotify(type, className, isAdd=true){
    switch (type) {
        case 0:
            if(isAdd){
                $('.save-not').addClass(className);
            }else{
                $('.save-not').removeClass(className);
            }
            break;
        case 1:
            if(isAdd){
                $('.delete-not').addClass(className);
            }else{
                $('.delete-not').removeClass(className);
            }
            break;
        case 2:
            if(isAdd){
                $('.edit-not').addClass(className);
            }else{
                $('.edit-not').removeClass(className);
            }
            break;
        default:
        break;
    }
}
function notFoundContent(){
    $('.nav.pagination').css({"display":"none"});
    $('.table').addClass('hide');
    $('.not-found').css({"display":"flex"});
}
function foundContent(){
    $('.nav.pagination').css({"display":"flex"});
    $('.table').removeClass('hide');
    $('.not-found').css({"display":"none"});
}

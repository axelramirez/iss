numberpage = 1;
$(document).ready(function(){
    // getAudit();

    $('.auditlogno input').focus();
    $('body').on('click','input#searchid',function(){
      var type = $( "body select[name='type']" ).val();
      var action = $( "body select[name='action']" ).val();
      var startDate = $( "body input[name='fecha1']" ).val();
      var endDate = $( "body input[name='fecha2']" ).val();
      var number = $( "body input[name='no']" ).val();
      var idcustomer = $( "body select[name='customerid']" ).val();
      var iduser = $( "body select[name='user']" ).val();
      getAudit(type,action,startDate,endDate,number,idcustomer,iduser);
    });
    //Clear inputs filter
    $('body').on('click', '.clear-search', function(){
      $('.auditlogno input').focus();
      $('#audit_search')[0].reset();
    });
    $('body').on('click', '.option-advanced p', function(){
      $('.section-date').addClass('section-date-show');
      $('.advanced-s').addClass('show-advanced-s');
      $('.option-advanced p').text('Simple Search');
      $('.option-advanced').addClass('option-simple');
      $('.option-advanced').removeClass('option-advanced');
    });
    $('body').on('click', '.option-simple p', function(){
      $('.section-date').removeClass('section-date-show');
      $('.advanced-s').removeClass('show-advanced-s');
      $('.option-simple p').text('Advanced Search');
      $('.option-simple').addClass('option-advanced');
      $('.option-simple').removeClass('option-simple');
    });
    $('body').on('click', '.action-next-list', function(){
      numberpage++;
      var type = $( "body select[name='type']" ).val();
      var action = $( "body select[name='action']" ).val();
      var startDate = $( "body input[name='fecha1']" ).val();
      var endDate = $( "body input[name='fecha2']" ).val();
      var number = $( "body input[name='no']" ).val();
      var idcustomer = $( "body select[name='customerid']" ).val();
      var iduser = $( "body select[name='user']" ).val();
      getAudit(type,action,startDate,endDate,number,idcustomer,iduser);
    });
    $('body').on('click', '.action-back-list', function(){
        if (numberpage > 1) {
            numberpage--;
            var type = $( "body select[name='type']" ).val();
            var action = $( "body select[name='action']" ).val();
            var startDate = $( "body input[name='fecha1']" ).val();
            var endDate = $( "body input[name='fecha2']" ).val();
            var number = $( "body input[name='no']" ).val();
            var idcustomer = $( "body select[name='customerid']" ).val();
            var iduser = $( "body select[name='user']" ).val();
            getAudit(type,action,startDate,endDate,number,idcustomer,iduser);
        }
    });
});
function getType(code){
  var typeName = "";
  if(code == 'R')
      typeName = "Receiving";
  if(code == 'S')
      typeName = "Shipping";
  if(code == "PA")
      typeName = "PreAlert";
  return typeName;
}
function getAction(code){
  var action = "";
  if(code == 'A')
      action = "#38c141";
  if(code == 'E')
      action = "#3197ff";
  if(code == 'D')
      action = "#d22828";
  return action;
}
function putAudit(audit, index, first = false){
  var customer = $("body select[name='customerid'] option[value=" + audit.idCustomer + "]").text();
  var user = $("body select[name='user'] option[value=" + audit.idUser + "]").text();
  if (first) {
    return `
      <tr>
          <td>Action</td>
          <td>Type</td>
          <td>No.</td>
          <td>Information</td>
          <td>Date/Time</td>
          <td>Customer</td>
          <td>User</td>loc
          <td>IP</td>
          <td>Source</td>
      </tr>
    `;
  }
  else {
      return `
        <tr id="audit_${audit.number}" data-id="${audit.number}">
            <td style='background-color:${getAction(audit.action)}' class="audit-action">
              <img src="images/icons/${audit.action}.svg">
            </td>
            <td class="audit-type">${getType(audit.type)}</td>
            <td class="audit-number">${audit.number}</td>
            <td class="audit-information">${audit.information}</td>
            <td class="audit-date"> ${audit.date}</td>
            <td class="audit-customer"> ${audit.customer}</td>
            <td class="audit-user"> ${audit.user}</td>
            <td class="audit-ip"> ${audit.ip}</td>
            <td class="audit-source"> ${audit.source}</td>
        </tr>
      `;
  }
}
function getAudit(type="",action="",startDate="",endDate="",number="",idcustomer="",iduser=""){
    if (startDate == '') {
      startDate = '0000-00-00'
    }
    if (endDate == '') {
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1; //January is 0!
      var yyyy = today.getFullYear();

      if(dd<10) {
          dd = '0'+dd
      }
      if(mm<10) {
          mm = '0'+mm
      }
      today = yyyy + '-' + mm + '-' + dd;
      endDate = today;
    }
    var table = $('.table-audit');
    table.empty();
    $.ajax({
        url: `/ISS/application/index.php/audit/get`,
        type: "GET",
        dataType: 'json',
        data: {
            page: numberpage,
            type:type,
            action:action,
            startDate:startDate,
            endDate:endDate,
            number:number,
            idcustomer:idcustomer,
            iduser:iduser
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    console.log(data);
                    var nulldata = data.response.audit;
                    var length = data.response.audit.length;
                    var index = 0;
                    table.append(putAudit({}, 0, true));
                    if (nulldata != null && length != 0) {
                      data.response.audit.forEach((audit)=>{
                        table.append(putAudit(audit,index));
                        index++;
                      });
                      var entries = data.response.numberEntries;
                      var pages = data.response.numberPages;
                      $('.not-found').hide();
                      $('.options-search').show();
                      $('.table-search').show();
                      $('.description-action').css({"display":"flex"});
                      $('.entries-audit .entries').html(entries + ' entries');
                      $('.entries-audit .pages').html(pages + ' Page(s)');
                      $('.actual-page p').html('Page ' + numberpage);
                      //Ocultar y mostrar flechas de navegacion
                      if (numberpage == pages) {$('.action-next-list').hide();}
                      else {$('.action-next-list').show();}
                      if (numberpage > 1) {$('.action-back-list').show();}
                      else {$('.action-back-list').hide();}
                      $('body,html').animate({ scrollTop: 530 }, 700);
                    }else {
                      $('.not-found').css({"display":"flex"});
                      $('.options-search').hide();
                      $('.table-search').hide();
                      $('.description-action').hide();
                      $('body,html').animate({ scrollTop: 530 }, 700);
                    }
                    break;
                case 404:
                    console.log(data);
                    break;
                default:
            }
        }, error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{
        openloading();
    });
}

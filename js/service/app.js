var dataCode = [];
var numberpage = 1;
$(document).ready(function() {
    getServices();
    //Abre el dialogo de Add Service
    $('body').on('click', '.add-service', function(){
        $('.title-dialog h2').text('Create Service');
        $('.title-dialog img').removeClass('close-edit');
        $('.title-dialog img').addClass('close-insert');
        $('.add-service-box .save-btn').val('Create');
        $('.add-service-box .save-btn').removeClass('update-service');
        $('.add-service-box .save-btn').addClass('insert-service');
        $('.cancel-btn').removeClass('cancel-update-service');
        $('.cancel-btn').addClass('cancel-insert-service');
        openServiceBox();
    });
    //Abre el dialogo de Edit Service con sus valores
    $('body').on('click', '.edit-service img', function(){
        $('.title-dialog h2').text('Edit Service');
        var id = $(this).parent().parent().attr('data-id');
        var description = $(this).parent().parent().find('.description').text();
        var code =  $(this).parent().parent().find('.code').text();
        var variant = $(this).parent().parent().attr('data-variant');
        openServiceBox();
        $('.edit-description input').val(description);
        $('.update-service').attr('data-id', id);
        if (code != '' || code != NaN || code != undefined || code != null) {
            $('.edit-code input').val(code);
        }
        if (variant == 1) {
            $('.checkvariant').prop('checked', true);
        }
        else {
            $('.checkvariant').prop('checked', false);
        }
    });
    $('body').on('click', '.next-services-list', function(){
        numberpage++;
        getServices();
    });
    $('body').on('click', '.back-services-list', function(){
        if (numberpage > 1) {
            numberpage--;
            getServices();
        }
    });
    //Cierra el dialogo de service y limpia los inputs
    $('body').on('click', '.close-edit', function(){
        closeEditServicesBox();
    });
    $('body').on('click', '.cancel-update-service', function(){
        closeEditServicesBox();
    });
    $('body').on('click', '.close-insert', function(){
        closeEditServicesBox();
    });
    $('body').on('click', '.cancel-insert-service', function(){
        closeEditServicesBox();
    });
    //Inserta los cambios realizados
    $('body').on('click', '.update-service', function(e){
      var inpObj = document.getElementById('descriptionService');
      if (!inpObj.checkValidity()) {
        $('.edit-description input').addClass('input-error');
        $('.edit-description input').attr('placeholder', 'Please fill');
      } else {
        e.preventDefault();
        var description = $('.edit-description input').val();
        var code = $('.edit-code input').val();
        var variant = $('.checkvariant').prop('checked');
        var id = parseInt($('.update-service').attr('data-id'));

        if (variant == false) {
            variant = 0;
        }
        else {
            variant = 1;
        }
        var dataUpdate = (
            updateServices = {
            description: description,
            code: code,
            variant: variant
        });
        $.ajax({
          url: '/ISS/application/index.php/services/update',
          type: 'PUT',
          dataType: 'json',
          data: {
              updateServices: dataUpdate,
              serviceId: id
          },
          success: (data)=>{
            console.log(data);
            shownotify(0);
            getServices();
            closeEditServicesBox();
          },
          error: (req, msg, error)=>{
            console.log(req);
            console.log(req.responseText);
          }
        })
      }
    });
    //Inserta los services nuevos
    $('body').on('click', '.insert-service', function(e){
      var inpObj = document.getElementById('descriptionService');
      if (!inpObj.checkValidity()) {
        $('.edit-description input').addClass('input-error');
        $('.edit-description input').attr('placeholder', 'Please fill');
      }else {
        e.preventDefault();
        var description = $('.edit-description input').val();
        var code = $('.edit-code input').val();
        var variant = $('.checkvariant').prop('checked');
        if (description.length <= 200 && code.length <= 5 ) {
          if (variant == false) {
            variant = 0;
          }
          else {
            variant = 1;
          }
          var dataInsert = (
            insertService = {
              description: description,
              code: code,
              variant: variant
            });
            $.ajax({
              url: '/ISS/application/index.php/services/insert',
              type: 'PUT',
              dataType: 'json',
              data: {
                insertService: dataInsert
              },
              success: (data)=>{
                shownotify(0);
                getServices();
                closeEditServicesBox();
              },
              error: (req, msg, error)=>{
                console.log(req);
                console.log(req.responseText);
              }
            })
        } else {
          console.log("Please not edit the max lenght");
        }
      }
    });
    //Agrega a un arreglo los codigos del sistema, si es diferente al actual
    $('body').on('change', '.inputcode', function(){
        var code = $(this).parent().attr('data-code');
        var value = $(this).val();
        var description = $(this).parent().attr('data-description');
        console.log(description);
        if (code != value) {
            dataCode.push({
                description: description,
                code: value
            })
        }
    });
    //Muesra el dialogo de confirmacion y llama la funcion de save
    $('body').on('click', '.save-system-services', function(){
        showdialog(()=>{
            updateCodes();
        }, 'Are you sure save changes in System Service Code?')
    });
});
function openServiceBox(){
    $('.box-shadow').css({"display":"block"});
    $('.add-service-box').css({"display":"block"});
    $('.add-service-box').css({"animation":"pop-in","animation-duration":"0.25s"});
}
function closeEditServicesBox(){
    resetClass();
    $('.box-shadow').css({"display":"none"});
    $('.add-service-box').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.add-service-box').css({"display":"none"});
    },250);
    $('.edit-description input').val('');
    $('.edit-code input').val('');
}
function resetClass(){
    $('.title-dialog img').removeClass('close-insert');
    $('.title-dialog img').addClass('close-edit');
    $('.add-service-box .save-btn').removeClass('insert-service');
    $('.add-service-box .save-btn').addClass('update-service');
    $('.add-service-box .save-btn').val('Update');
    $('.cancel-btn').removeClass('cancel-insert-service');
    $('.cancel-btn').addClass('cancel-update-service');
    $('.edit-description input').removeClass('input-error');
    $('.edit-description input').attr('placeholder', '');
}
//Funcion para actualizar los codigos del sistema
function updateCodes(){
    $.ajax({
      url: '/ISS/application/index.php/services/systemcodes/update',
      type: 'PUT',
      dataType: 'json',
      data: {
          updateCodes: dataCode
      },
      success: (data)=>{
          console.log(data);
          shownotify(0);
      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}
//Escribe los servicios dentro de la tabla
function putServices(services, index, first = false){
    if (first) {
        return `
            <tr>
                <td>Edit</td>
                <td>Description</td>
                <td>Code</td>
            </tr>
        `;
    }
    else {
        return `
            <tr data-system="${services.system}" id="service_${services.serviceid}" data-id="${services.serviceid}" data-variant="${services.variant}" data-status="1" data-change="0">
                <td class="edit-service">
                <img src="../images/icons/edit.svg" alt="Edit">
                    <a href="service_form.php?serviceid=${services.serviceid}">
                    </a>
                </td>
                <td class="description">${services.description}</td>
                <td class="code">${services.code}</td>
            </tr>
        `;
    }
}
//Obtiene los servicios con el metodo ajax y los itera en la funcion purservices
function getServices(){
    var tableServices = $('.service_list_box table');
    tableServices.empty();
    $.ajax({
        url: `/ISS/application/index.php/services/get`,
        type: "GET",
        dataType: 'json',
        data:{
            page: numberpage
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    tableServices.append(putServices({}, 0, true));
                    data.response.services.forEach((service)=>{
                        tableServices.append(putServices(service));
                    });
                    var entries = data.response.numberEntries;
                    var pages = data.response.numberPages;
                    $('.entries-services .entries').html(entries + ' entries');
                    $('.entries-services .pages').html(pages + ' Page(s)');
                    $('.actual-page p').html('Page ' + numberpage);
                    //Ocultar y mostrar flechas de navegacion
                    if (numberpage == pages) {$('.next-services-list').hide();}
                    else {$('.next-services-list').show();}
                    if (numberpage > 1) {$('.back-services-list').show();}
                    else {$('.back-services-list').hide();}
                    break;
                case 404:

                    break;
                default:
            }
        }
    }).done(()=>{
        $('.service_list_box table tr').map(function(){
            var system = $(this).attr('data-system');
            if (system == 1) {
                $(this).find('.edit-service a').remove();
            }
        });
        loadingservices();
    });
}

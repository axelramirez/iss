var numberpage = 1;

$(document).ready(function() {
    getPart();
    $('body').on('click', '.clean-filter', function(){
      $('.clean-filter').hide();
      $('.partsearch').val('');
      $('.search-part').hide();
      $('.add-part').show();
    });
    $('body').on('focus', '.partsearch', function(){
        $('.add-part').hide();
        $('.search-part').show();
        $('.clean-filter').show();
    });
    $('.partrsearch').change(function(){
      var valueFilter = $(this).val();
      $('.clean-filter').show();
      if (valueFilter == '') {
        $('.search-part').hide();
        $('.add-part').show();
        $('.clean-filter').hide();
      }
    });
    $('body').on('click', '.search-part', function(){
      var filter = $('.partsearch').val();
      searchFilter(filter);
    });
    //Abre el dialogo de Create Part
    $('body').on('click', '.add-part', function(){
        $('.title-dialog h2').text('Create Part');
        $('.add-part-box .save-btn').val('Create');
        $('.add-part-box .save-btn').removeClass('update-part');
        $('.add-part-box .save-btn').addClass('insert-part');
        getCustomers(0);
        openPartBox();
    });
    $('body').on('click', '.edit-part img', function(){
        $('.add-part-box .save-btn').val('Update');
        $('.title-dialog h2').text('Edit Part');
        var id = $(this).parent().parent().attr('data-id');
        var productno = $(this).parent().parent().find('.part-no').text();
        var name = $(this).parent().parent().find('.part-name').text();
        var description = $(this).parent().parent().find('.part-description').text();
        var supplier = $(this).parent().parent().find('.part-supplier').text();
        var serial = $(this).parent().parent().find('.part-serial').text();
        var customer = $(this).parent().parent().find('.part-customer').text();
        var customerid = $(this).parent().parent().find('.part-customer').attr('data-customerid');
        var date = $(this).parent().parent().find('.part-date').text();
        $('.selectCustomerPart').attr('data-value', customerid);
        var action = 'update';
        getCustomers(action);
        openPartBox();
        console.log(customer);
        // $('.customerpart select').val(customerid);
        $('.partdescription input').val(description);
        $('.partno input').val(productno);
        $('.partname input').val(name);
        $('.partserial input').val(serial);
        $('.partsupplier input').val(supplier);
        $('.partdate input').val(date);
        $('.update-part').attr('data-id', id);
    });
    //Cierra el dialogo de part y limpia los inputs
    $('body').on('click', '.close-part', function(){
        closePartBox();
    });
    $('body').on('click', '.cancel-insert-part', function(){
        closePartBox();
    });
    $('body').on('click', '.insert-part', function(e){
      var inpObj = document.getElementById('partNo');
      if (!inpObj.checkValidity()) {
        $('.partno input').addClass('input-error');
        $('.partno input').attr('placeholder', 'Please fill');
        $('.partno input').focus();
      }else {
        e.preventDefault();
        var partno = $('.partno input').val();
        var customerpart = $('#customerPart option:selected').val();
        if (partno.length <= 50 && customerpart != 'none') {
          insertPart();
        }else {
          if (partno.length <= 50) {
            $('.partno input').removeClass('input-error');
            $('.partno input').attr('placeholder', '');
          }
          if (customerpart != 'none') {
            $('#customerPart').removeClass('input-error');
          }
          if (customerpart == 'none') {
            $('#customerPart').addClass('input-error');
          }
          console.log('Please not edit the max length');
        }
      }
    });
    $('body').on('click', '.update-part', function(e){
        var inpObj = document.getElementById('partNo');
        if (!inpObj.checkValidity()) {
          $('.partno input').addClass('input-error');
          $('.partno input').attr('placeholder', 'Please fill');
          $('.partno input').focus();
        }else {
          e.preventDefault();
          var partno = $('.partno input').val();
          var customerpart = $('#customerPart option:selected').val();
          if (partno.length <= 50 && customerpart != 'none') {
            var id = parseInt($(this).attr('data-id'));
            updatePart(id);
          }else {
            if (partno.length <= 50) {
              $('.partno input').removeClass('input-error');
              $('.partno input').attr('placeholder', '');
            }
            if (customerpart != 'none') {
              $('#customerPart').removeClass('input-error');
            }
            if (customerpart == 'none') {
              $('#customerPart').addClass('input-error');
            }
            console.log('Please not edit the max length');
          }
        }
    });
    $('body').on('click', '.next-part-list', function(){
        numberpage++;
        getPart();
    });
    $('body').on('click', '.back-part-list', function(){
        if (numberpage > 1) {
            numberpage--;
            getPart();
        }
    });
    $('body').on('click', '.next-list', function(){
        numberpage++;
        searchFilter();
    });
    $('body').on('click', '.back-list', function(){
        if (numberpage > 1) {
            numberpage--;
            searchFilter();
        }
    });
});
//Clean undefined data talbe part
function cleanUndefined(){
  $('.part-box table tr td').map(function(){
    var data = $(this).text();
    var classs = $(this).attr('class');
    if (classs != 'edit-part') {
      if (data == "null" || data == "none" || data == "0000-00-00" || data == "" || data == "NaN" || data == 0 || data == undefined) {
        $(this).text('');
      }
    }
  });
}
//Abre el contenedor para insertar/actualizar part
function openPartBox(){
    $('.box-shadow').css({"display":"block"});
    $('.add-part-box').css({"display":"block"});
    $('.add-part-box').css({"animation":"pop-in","animation-duration":"0.25s"});
    $('.partno input').focus();
}
//Cierra el contenedor para insertar/actualizar part
function closePartBox(){
    $('.box-shadow').css({"display":"none"});
    $('.add-part-box').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.add-part-box').css({"display":"none"});
    },250);
    $('#newpart input').val('');
    $('.partno input').removeClass('input-error');
    $('.partno input').attr('placeholder', '');
    $('#customerPart').removeClass('input-error');
    resetClass();
}
//Resetea las clases del contenedor insertar/actualizar part
function resetClass(){
    $('.add-part-box .save-btn').removeClass('insert-part');
    $('.add-part-box .save-btn').addClass('update-part');
    $('.selectCustomerPart').prop('selectedIndex',0);
    $('.selectCustomerPart').empty();
}
//Funcion para cambiar clases de los botones de paginacion
function filterClassPagination(){
  $('.action-back-list').removeClass('back-part-list');
  $('.action-next-list').removeClass('next-part-list');
  $('.action-back-list').addClass('back-list');
  $('.action-next-list').addClass('next-list');
}
function allClassPagination(){
  $('.action-back-list').removeClass('back-list');
  $('.action-next-list').removeClass('next-list');
  $('.action-back-list').addClass('back-part-list');
  $('.action-next-list').addClass('next-part-list');
}
//Selecciona el cliente
function selectCustomerPart(){
  var actualOption =  $('.selectCustomerPart').attr('data-value');
  $('.selectCustomerPart').val(actualOption);
}
//Escribe los clientes
function optionCustomer(customer, first = false){
    if (first) {
      return `<option value="none" >None</option>`;
    }
    else {
      var optionsCustomer = `<option value="${customer.customerid}" data-value="${customer.name}">${customer.name}</option>`;
      return optionsCustomer;
    }
}
function getCustomers(action){
  var selectCustomer = $('.selectCustomerPart');
  $.ajax({
      url: `/ISS/application/index.php/customer/get`,
      type: "GET",
      dataType: 'json',
      success: data=>{
          switch (data.code) {
              case 200:
                  selectCustomer.append(optionCustomer({}, true));
                  data.response.customers.forEach((customer)=>{
                      selectCustomer.append(optionCustomer(customer));
                  });
                  break;
              case 404:

                  break;
              default:
          }
      }
  }).done(()=>{
      if (action == 'update') {
        selectCustomerPart();
      }
  });
}
//Funcion para busqueda
function searchFilter(filter){
  var tablePart = $('.part-box table');
  filterClassPagination();
  tablePart.empty();
  $.ajax({
      url: `/ISS/application/index.php/part/search`,
      type: "GET",
      dataType: 'json',
      data: {
          page: numberpage,
          filter: filter
      },
      success: data=>{
          switch (data.code) {
              case 200:
                console.log(data);
                  tablePart.append(putPart({}, 0, true));
                  data.response.parts.forEach((part)=>{
                      tablePart.append(putPart(part));
                  });
                  var entries = data.response.numberEntries;
                  var pages = data.response.numberPages;
                  $('.entries-part .entries').html(entries + ' entries');
                  $('.entries-part .pages').html(pages + ' Page(s)');
                  $('.actual-page p').html('Page ' + numberpage);
                  //Ocultar y mostrar flechas de navegacion
                  if (numberpage == pages) {$('.next-list').hide();}
                  else {$('.next-list').show();}
                  if (numberpage > 1) {$('.back-list').show();}
                  else {$('.back-list').hide();}
                  break;
              case 404:
                  console.log(data);
                  break;
              default:
          }
      }, error: (req, msg, error)=>{
        console.log(req);
      }
  }).done(()=>{
      loadingcatalogs();
      cleanUndefined();
  });
}
//Fucnion para insertar part
function insertPart(){
    var customer = $('.customerpart option:selected').val();
    var description = $('.partdescription input').val();
    var partno = $('.partno input').val();
    var name = $('.partname input').val();
    var serial = $('.partserial input').val();
    var supplier = $('.partsupplier input').val();
    var date = $('.partdate input').val();
    if (date == '') {
      date = '0000-00-00'
    }
    var dataInsert = (
        insertPart = {
          customerId: customer,
          description: description,
          productNo: partno,
          name: name,
          description2: serial,
          supplier: supplier,
          date: date,
    });

    $.ajax({
      url: '/ISS/application/index.php/part/insert',
      type: 'PUT',
      dataType: 'json',
      data: {
          insertPart: dataInsert
      },
      success: (data)=>{
          console.log(data);
          getPart();
          closePartBox();
          shownotify(0);
      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}
//Funcion para actualizar part
function updatePart(id){
  var customer = $('.customerpart option:selected').val();
  var description = $('.partdescription input').val();
  var partno = $('.partno input').val();
  var name = $('.partname input').val();
  var serial = $('.partserial input').val();
  var supplier = $('.partsupplier input').val();
  var date = $('.partdate input').val();
  if (date == '') {
    date = '0000-00-00'
  }
  var dataInsert = (
      insertPart = {
        customerId: customer,
        description: description,
        productNo: partno,
        name: name,
        description2: serial,
        supplier: supplier,
        date: date,
  });

  $.ajax({
    url: '/ISS/application/index.php/part/update',
    type: 'PUT',
    dataType: 'json',
    data: {
        updatePart: dataInsert,
        partId: id
    },
    success: (data)=>{
      console.log(data);
        getPart();
        closePartBox();
        shownotify(0);
    },
    error: (req, msg, error)=>{
      console.log(req);
      console.log(req.responseText);
    }
  })
}
//Escribe los part dentro de la tabla
function putPart(part, index, first = false){
    if (first) {
        return `
            <tr>
                <td>Edit</td>
                <td>Part No.</td>
                <td>Part No. 2</td>
                <td>Description</td>
                <td>Supplier</td>
                <td>Serial No.</td>
                <td>Customer</td>
                <td>Manufacturing Date</td>
            </tr>
        `;
    }
    else {
        return `
            <tr id="part_${part.productid}" data-id="${part.productid}">
                <td class="edit-part">
                    <img src="images/icons/edit.svg" alt="Edit">
                </td>
                <td class="part-no">${part.productno}</td>
                <td class="part-name">${part.name}</td>
                <td class="part-description">${part.description}</td>
                <td class="part-supplier">${part.supplier}</td>
                <td class="part-serial">${part.description2}</td>
                <td class="part-customer" data-customerid="${part.customerid}">${part.customername}</td>
                <td class="part-date">${part.date}</td>
            </tr>
        `;
    }
}
//Obtiene los part con el metodo ajax y los itera en la funcion putPart
function getPart(){
    var tablePart = $('.part-box table');
    tablePart.empty();
    $.ajax({
        url: `/ISS/application/index.php/part/get`,
        type: "GET",
        dataType: 'json',
        data: {
            page: numberpage
        },
        success: data=>{
            switch (data.code) {
                case 200:
                  console.log(data);
                    tablePart.append(putPart({}, 0, true));
                    data.response.parts.forEach((part)=>{
                        tablePart.append(putPart(part));
                    });
                    var entries = data.response.numberEntries;
                    var pages = data.response.numberPages;
                    $('.entries-part .entries').html(entries + ' entries');
                    $('.entries-part .pages').html(pages + ' Page(s)');
                    $('.actual-page p').html('Page ' + numberpage);
                    //Ocultar y mostrar flechas de navegacion
                    if (numberpage == pages) {$('.next-part-list').hide();}
                    else {$('.next-part-list').show();}
                    if (numberpage > 1) {$('.back-part-list').show();}
                    else {$('.back-part-list').hide();}
                    break;
                case 404:
                    console.log(data);
                    break;
                default:
            }
        }, error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{
        loadingcatalogs();
        cleanUndefined();
    });
}

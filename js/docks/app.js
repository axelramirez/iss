var numberpage = 1;
$(document).ready(function() {
    getDocks();
    //Abre el dialogo de Edit Service con sus valores
    $('body').on('click', '.edit-dock img', function(){
        $('.title-dialog h2').text('Edit Dock');
        var id = $(this).parent().parent().attr('data-id');
        var name = $(this).parent().parent().find('.dockname').text();

        openDocksBox();
        $('.edit-dockname input').val(name);
        $('.update-dock').attr('data-id', id);
    });
    $('body').on('click', '.add-dock', function(){
        $('.title-dialog h2').text('Create Dock');
        $('.title-dialog img').removeClass('close-edit');
        $('.title-dialog img').addClass('close-insert');
        $('.save-btn').val('Create');
        $('.save-btn').removeClass('update-dock');
        $('.save-btn').addClass('insert-dock');
        $('.cancel-btn').removeClass('cancel-update-dock');
        $('.cancel-btn').addClass('cancel-insert-dock');
        openDocksBox();
    });
    //Cierra el dialogo de service y limpia los inputs
    $('body').on('click', '.close-dock', function(){
        closeDockBox();
    });
    $('body').on('click', '.cancel-update-dock', function(){
        closeDockBox();
    });
    $('body').on('click', '.cancel-insert-dock', function(){
        closeDockBox();
    });
    $('body').on('click', '.update-dock', function(e){
      var inpObj = document.getElementById('nameDock');
      if (!inpObj.checkValidity()) {
        $('.edit-dockname input').addClass('input-error');
        $('.edit-dockname input').attr('placeholder', 'Please fill');
        $('.edit-dockname input').focus();
      }else {
        e.preventDefault();
        var dockname = $('.edit-dockname input').val();
        if (dockname.length <= 30) {
          updateDock();
        }else {
          console.log("Please not edit the max lenght");
        }
      }
    });
    $('body').on('click', '.insert-dock', function(e){
      var inpObj = document.getElementById('nameDock');
      if (!inpObj.checkValidity()) {
        $('.edit-dockname input').addClass('input-error');
        $('.edit-dockname input').attr('placeholder', 'Please fill');
        $('.edit-dockname input').focus();
      }
      else {
        e.preventDefault();
        var dockname = $('.edit-dockname input').val();
        if (dockname.length <= 30) {
          insertDock();
        }else {
          console.log('Please not edit the max lenght');
        }
      }
    });
    $('body').on('click', '.next-docks-list', function(){
        numberpage++;
        getDocks();
    });
    $('body').on('click', '.back-docks-list', function(){
        if (numberpage > 1) {
            numberpage--;
            getDocks();
        }
    });
});
function openDocksBox(){
    $('.box-shadow').css({"display":"block"});
    $('.add-dock-box').css({"display":"block"});
    $('.add-dock-box').css({"animation":"pop-in","animation-duration":"0.25s"});
    $('.edit-dockname input').focus();
}
function closeDockBox(){
    $('.box-shadow').css({"display":"none"});
    $('.add-dock-box').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.add-dock-box').css({"display":"none"});
    },250);
    $('.edit-dockname input').val('');
    $('.edit-dockname input').removeClass('input-error');
    $('.edit-dockname input').attr('placeholder', '');
    resetClass();
}
function resetClass(){
    $('.title-dialog img').removeClass('close-edit');
    $('.title-dialog img').addClass('close-insert');
    $('.save-btn').removeClass('insert-dock');
    $('.save-btn').addClass('update-dock');
    $('.cancel-btn').removeClass('cancel-insert-dock');
    $('.cancel-btn').addClass('cancel-update-dock');
}
//Funcion para actualizar los codigos del sistema
function updateDock(){
    var name = $('.edit-dockname input').val();
    var id = $('.update-dock').attr('data-id');

    $.ajax({
      url: '/ISS/application/index.php/dock/update',
      type: 'PUT',
      dataType: 'json',
      data: {
          dockName: name,
          dockId: id
      },
      success: (data)=>{
          console.log(data);
          getDocks();
          closeDockBox();
          shownotify(0);
      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}
//Fucnion para insertar docks
function insertDock(){
    var name = $('.edit-dockname input').val();
    $.ajax({
      url: '/ISS/application/index.php/dock/insert',
      type: 'PUT',
      dataType: 'json',
      data: {
          dockName: name
      },
      success: (data)=>{
          console.log(data);
          getDocks();
          closeDockBox();
          shownotify(0);
      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}
//Escribe los servicios dentro de la tabla
function putDocks(dock, index, first = false){
    if (first) {
        return `
            <tr>
                <td>Edit</td>
                <td>dock Name</td>
            </tr>
        `;
    }
    else {
        return `
            <tr id="dock_${dock.dockid}" data-id="${dock.dockid}">
                <td class="edit-dock">
                    <img src="images/icons/edit.svg" alt="Edit">
                </td>
                <td class="dockname">${dock.name}</td>
            </tr>
        `;
    }
}
//Obtiene los servicios con el metodo ajax y los itera en la funcion purservices
function getDocks(){
    var tableDocks = $('.dock-box table');
    tableDocks.empty();
    $.ajax({
        url: `/ISS/application/index.php/docks/get`,
        type: "GET",
        dataType: 'json',
        data:{
            page: numberpage
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    console.log(data);
                    tableDocks.append(putDocks({}, 0, true));
                    data.response.docks.forEach((dock)=>{
                        tableDocks.append(putDocks(dock));
                    });
                    var entries = data.response.numberEntries;
                    var pages = data.response.numberPages;
                    $('.entries-docks .entries').html(entries + ' entries');
                    $('.entries-docks .pages').html(pages + ' Page(s)');
                    $('.actual-page p').html('Page ' + numberpage);
                    //Ocultar y mostrar flechas de navegacion
                    if (numberpage == pages) {$('.next-docks-list').hide();}
                    else {$('.next-docks-list').show();}
                    if (numberpage > 1) {$('.back-docks-list').show();}
                    else {$('.back-docks-list').hide();}
                    break;
                case 404:
                    console.log(data);
                    break;
                default:
            }
        }, error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{
        loadingdocks();
    });
}

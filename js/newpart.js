//Abrir dialogo para crear parte nueva
$('body').on('click', '.new-part', function(){
    var customerid = $('#customerid').val();
    var customer = $('#customerid option:selected').text();
    $('.box-shadow').css({"z-index":"103"});
    $('.newpart-box').css({"display":"block"});
    $('.newpart-box').css({"animation":"pop-in","animation-duration":"0.25s"});

    $('.customername-newpart').val(customer);
    $('.customername-newpart').attr('data-customerid', customerid);
});
$('body').on('click', '.new-partrd', function(){
    var customerid = $('#customerid').val();
    var customer = $('#customerid option:selected').text();
    $('.box-shadow').css({"z-index":"9998"});
    $('.newpart-box').css({"z-index":"9999"});
    $('.newpart-box').css({"display":"block"});
    $('.newpart-box').css({"animation":"pop-in","animation-duration":"0.25s"});
    $('.customername-newpart').val(customer);
    $('.customername-newpart').attr('data-customerid', customerid);
});
//Cerrar contenedor
$('body').on('click', '.close-new-part', function(){
    $('.box-shadow').css({"z-index":"102"});
    $('.newpart-box').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.newpart-box').css({"display":"none"});
    },250);
});
$('body').on('submit', '#form-newpart', function(e){
    if (this.checkValidity()) {
        e.preventDefault()
        var section = $('.new-partrd').attr('data-section');
        if (section == 'receivingdetail') {
            var idtmp = $('.new-partrd').attr('data-id');

            var partno =  $('.new-partname').val();
            var description = $('.new-partdescription').val();
            var x = parts.length;
            var indexProduct = 0;
            indexProduct = x;
            $('#'+idtmp).find('.data-part input').show();
            $('#'+idtmp).find('.data-part input').val(partno);
            $('#'+idtmp).find('.data-part').attr('data-value', partno);
            $('#'+idtmp).find('.data-description input').val(description);
            $('#'+idtmp).find('.data-description').attr('data-value', description);
            $('#'+idtmp).find('.data-part').attr('data-exist', 1);
            $('#'+idtmp).find('.data-part').attr('data-productid', 'new');
            $('#'+idtmp).find('.data-part').attr('data-indexpart', indexProduct);
            newpart();
            $('.data-newpart').find('input').val('');
            $('#'+idtmp).find('.part-receivingdetail').hide();
            $('#'+idtmp).find('.data-qrtypart input').focus();
            $('.addornewpart-box').css({"display":"none"});
        }
        else if (section == 'editreceivingdetail') {
            var idtmp = $('.new-partrd').attr('data-id');
            var parttmp = $('#'+idtmp).find('.editPartRD').attr('data-editpart');

            var partno =  $('.new-partname').val();
            var description = $('.new-partdescription').val();
            var x = parts.length;
            var indexProduct = 0;
            indexProduct = x;
            var sectionpart = '.rdpartno';
            var sectiontracking = '.rdtracking'
            var sectiondescription = '.rdpartdescription';
            if (parttmp == 'edit') {
                sectionpart = '.tmp-rdpart';
                sectiondescription = '.tmp-rddescription';
                sectiontracking = '.tmp-rdtracking'
            }
            $('#'+idtmp).find(sectionpart+' input').val(partno);
            $('#'+idtmp).find(sectionpart).attr('data-value', partno);
            $('#'+idtmp).find(sectiondescription+' input').val(description);
            $('#'+idtmp).find(sectiondescription).attr('data-value', description);
            $('#'+idtmp).find(sectionpart).attr('data-exist', 1);
            $('#'+idtmp).find(sectionpart).attr('data-productid', 'new');
            $('#'+idtmp).find(sectionpart).attr('data-newproduct', 1);
            $('#'+idtmp).find(sectionpart).attr('data-indexpart', indexProduct);
            $('#'+idtmp).find(sectionpart).attr('data-asignedpart', 1);
            $('#'+idtmp).find(sectiontracking).attr('data-required', 0);
            $('#'+idtmp).find('.rdtracking input').removeClass('input-error');
            $('#'+idtmp).find('.tmp-rdtracking input').removeClass('input-error');
            $('#'+idtmp).attr('data-change', 1);
            newpart();
            $('.data-newpart').find('input').val('');
            $('.addornewpart-box').css({"display":"none"});
            $('.box-shadow').removeClass('visible');
        }
        else {
            var partno =  $('.new-partname').val();
            var description = $('.new-partdescription').val();
            $('#partno').val(partno);
            $('#partno').css({"display":"block"});
            $('#desriptionpart').val(description);
            $('#partno').attr('data-exist', 0);
            $('#partno').attr('data-productid', 'new');
            newpart();
            $('.data-newpart').find('input').val('');
            $('#packageqtypart').focus();
        }
        $('.box-shadow').css({"z-index":"102"});
        $('.newpart-box').css({"animation":"pop-out","animation-duration":"0.25s"});
        setTimeout(function(){
            $('.newpart-box').css({"display":"none"});
        },250);
    }
});
function newpart(){
    part.push({
        customerid: parseInt($('.customername-newpart').attr('data-customerid')),
        productno: $('.new-partname').val().toString(),
        name: $('.new-partname2').val().toString(),
        description: $('.new-partdescription').val().toString(),
        serial: $('.new-partserial').val().toString(),
        supplier: $('.new-partsupplier').val().toString(),
        date: $('.new-manufactdate').val().toString()
    })
    parts.push({
        customerid: parseInt($('.customername-newpart').attr('data-customerid')),
        productno: $('.new-partname').val().toString(),
        name: $('.new-partname2').val().toString(),
        description: $('.new-partdescription').val().toString(),
        serial: $('.new-partserial').val().toString(),
        supplier: $('.new-partsupplier').val().toString(),
        date: $('.new-manufactdate').val().toString()
    });
    console.log(parts);
}

$(document).ready(function() {
    var receivingid = $('.data-receiving-view').attr('data-receivingid');

    getReceivingDetails(receivingid);
    getDocuments(receivingid);
    var optionst = 0;
    var cmrd = 0;
    $('body').on('click','.button-options',function(){
        if(optionst == 0){
            $(".shadow-table").css({"display":"block"});
            $(this).find('.options-table').css({"display":"block"});
            optionst = 1;
        }
        else{
            optionst = 0;
            $('.options-table').css({"display":"none"});
            $(".shadow-table").css({"display":"none"});
        }
    });
    $('body').on('click', '.show-details', function(){
        if(cmrd == 0){
            $(this).parent().find('.more-details').css({"display":"block"});
            $('.shadow-more-details').css({"display":"block"});
            cmrd = 1;
        }
    });
    $(".shadow-table").click(function(){
        optionst = 0;
        $('.options-table').css({"display":"none"});
        $(".shadow-table").css({"display":"none"});
    });
    $(".shadow-more-details").click(function(){
        if(cmrd == 1){
            $('.more-details').css({"display":"none"});
            $('.shadow-more-details').css({"display":"none"});
            // $('.content-loading-details-view').css({})
            cmrd = 0;
        }
    });
    $('body').on('click', '.close-shippings-view img', function(){
        $('.view-shippings-option').css({"display":"none"})
    });
});
function tableTotals(Totalqtypart, Totalpackqty, TotalOnhand, TotalWeight){
    $('.data-view-receivingdetails .result-table').append(`
        <table class="table">
            <tr>
                <td>Received Qty Part</td>
                <td>Received Package Qty</td>
                <td>OnHand</td>
                <td>Total Weight</td>
            </tr>
            <tr>
                <td>${Totalqtypart}</td>
                <td>${Totalpackqty}</td>
                <td>${TotalOnhand}</td>
                <td>${TotalWeight}</td>
            </tr>
        </table>
    `);
}
//funcion que escribe los receiving details
function putReceivingDetails(receivingdetail, index, first = false){
    if (first) {
        return `
            <tr>
                <td>No.</td>
                <td>Options</td>
                <td>Created Date</td>
                <td>Tracking Number</td>
                <td>Description</td>
                <td>Part #</td>
                <td>Received Qty Part</td>
                <td>Origen</td>
                <td>Package Type</td>
                <td>Received Package Qty</td>
                <td>OnHand.</td>
                <td>Total Weight</td>
                <td>Unit of Measurement</td>
                <td hidden>Location</td>
                <td>Lot Number</td>
            </tr>
        `;
    }
    return `
        <tr data-id="${receivingdetail.receivingdetailid}">
            <td class="details-options">
                <p>${index}</p>
                <img src="images/icons/down.svg" class="show-details">
                <div id="tdDetailsLines${index-1}" class="more-details">
                    <div class="content-loading-details-view">
                        <svg class="ldi-83fims" width="200px"  height="200px"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style="background: none;"><!--?xml version="1.0" encoding="utf-8"?--><!--Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)--><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" style="transform-origin: 50px 50px 0px;" xml:space="preserve"><g style="transform-origin: 50px 50px 0px; transform: scale(0.6);"><g style="transform-origin: 50px 50px 0px;"><style type="text/css" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.9s;">.st0{fill:#040000;} .st1{fill:#050000;} .st2{fill:#E74F0C;} .st3{fill:#4D494C;} .st4{fill:#E72E11;} .st5{fill:#241C1D;}</style><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.75385s;"><circle class="st0" cx="72.6" cy="69.575" r="17.4" fill="#e85757" style="fill: rgb(232, 87, 87);"></circle></g><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.60769s;"><circle class="st0" cx="27.4" cy="69.575" r="17.4" fill="#e85757" style="fill: rgb(232, 87, 87);"></circle></g><g class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.46154s;"><path class="st0" d="M67.4,30.425c0,9.61-7.79,17.4-17.4,17.4s-17.4-7.79-17.4-17.4c0-9.6,7.79-17.4,17.4-17.4 S67.4,20.825,67.4,30.425z" fill="#e85757" style="fill: rgb(232, 87, 87);"></path></g><metadata xmlns:d="https://loading.io/stock/" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.31538s;">
                           <d:name class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.16923s;">asana</d:name>
                           <d:tags class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -1.02308s;">asana,brand wb</d:tags>
                           <d:license class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.876923s;">cc0</d:license>
                           <d:slug class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.730769s;">83fims</d:slug>
                           </metadata></g></g><style type="text/css" class="ld ld-heartbeat" style="transform-origin: 50px 50px 0px; animation-duration: 1.9s; animation-delay: -0.584615s;">
                           </style></svg></svg>
                    </div>
                    <div class="details-table">
                        <div class='title'>
                            <h1>Details</h1>
                        </div>
                        <table class="table"></table>
                    </div>
                </div>
            </td>
            <td class="button-options">
                <img src="images/icons/more.svg">
                <div class="options-table">
                    <div>
                        <a href="javascript:WinOpen=open('print_label.php?receivingid=${receivingdetail.receivingid}&amp;receivingdetailid=${receivingdetail.receivingdetailid}','WinOpen','Width=500,height=400,scrollbars=1,left=25,top=25,resizable=0,menubar=1'); WinOpen.focus()">
                            <img src="images/icons/barcode.svg" width="30" height="16" alt="Print">
                            <p>Label</p>
                        </a>
                    </div>
                    <div class="showShippings" data-receivingdetailid="${receivingdetail.receivingdetailid}">
                        <img src="images/icons/view.svg" alt="Search">
                        <p>Shippings</p>
                    </div>
                </div>
            </td>
            <td>${receivingdetail.createddate}</td>
            <td>${receivingdetail.truckingno}</td>
            <td>${receivingdetail.description}</td>
            <td>${receivingdetail.productNo}</td>
            <td>${receivingdetail.qtypart}</td>
            <td>${receivingdetail.origen}</td>
            <td>${receivingdetail.packageName}</td>
            <td>${receivingdetail.originalquantity}</td>
            <td>${receivingdetail.currentquantity}</td>
            <td>${receivingdetail.weight}</td>
            <td>${receivingdetail.unitName}</td>
            <td>${receivingdetail.location}</td>
            <td>${receivingdetail.lotnumber}</td>
        </tr>
    `;
}
//Funcion que trae los receipts details de un receiving
function getReceivingDetails(receivingid){
    var tableReceivingDetails = $('.view-receivingdetail-table');
    var Totalqtypart = 0;
    var Totalpackqty = 0;
    var TotalOnhand = 0;
    var TotalWeight = '0.00';
    tableReceivingDetails.empty();
    $.ajax({
        url: `/ISS/application/index.php/receivingdetail`,
        type: "GET",
        dataType: 'json',
        data: {
            receivingId: receivingid
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    var dataReceiving = data.response.receivingDetail;
                    tableReceivingDetails.append(putReceivingDetails({}, 0, true));
                    if (dataReceiving != null) {
                      data.response.receivingDetail.forEach((receivingdetail, index)=>{
                        Totalqtypart = (receivingdetail.qtypart + Totalqtypart);
                        Totalpackqty = (receivingdetail.originalquantity + Totalpackqty);
                        TotalOnhand = (receivingdetail.currentquantity + TotalOnhand);
                        TotalWeight = (parseFloat(receivingdetail.weight) + parseFloat(TotalWeight));
                        tableReceivingDetails.append(putReceivingDetails(receivingdetail, index+1));
                      })
                    }
                    break;
                case 404:

                    break;
                default:
            }
        }
    }).done(()=>{
        loadingreceivingdetails();
        TotalWeight = TotalWeight.toFixed(2);
        tableTotals(Totalqtypart, Totalpackqty, TotalOnhand, TotalWeight);
        cleanData();
        $('body').on('click','.show-details', function(){
            var receivingdetailid = $(this).parent().parent().attr('data-id');
            getDetails(receivingdetailid);
        });
        $('body').on('click','.showShippings', function(){
            var receivingdetailid = $(this).attr('data-receivingdetailid');
            var position = $(this).parent().offset();
            var left = (position.left + 140);
            getShippings(receivingdetailid);
            $('.view-shippings-option').css({"display":"block", "top": position.top, "left": left});
        });
    });
}
//funcion que escribe los receiving lines de un receiving details
function putDetails(detail, index, first = false){
    if (first) {
        return `
            <tr>
                <td>Part #</td>
                <td>Description</td>
                <td>Inner Package Qty</td>
                <td>Package Type</td>
                <td>Received Qty Part</td>
                <td>Lot Number</td>
                <td>Exp Date</td>
                <td>PO</td>
                <td>PO date</td>
            </tr>
        `;
    }
    return `
        <tr data-id="${detail.receivinglinesid}">
            <td>${detail.productno}</td>
            <td>${detail.description}</td>
            <td>${detail.originalQty}</td>
            <td>${detail.unitdescription}</td>
            <td>${detail.partQty}</td>
            <td>${detail.lotnumber}</td>
            <td>${detail.expDate}</td>
            <td>${detail.po}</td>
            <td>${detail.podate}</td>
        </tr>
    `;
}
//Funcion que trae los receiving lines de un receipt detail
function getDetails(receivingdetailid){
    var tableDetails = $('.details-table table');
    tableDetails.empty();
    $.ajax({
        url: `/ISS/application/index.php/receivingdetail/details`,
        type: "GET",
        dataType: 'json',
        data: {
            receivingdetailId: receivingdetailid
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    console.log(data.response.details.length);
                    var length = data.response.details.length;
                    if (length > 0) {
                        tableDetails.append(putDetails({}, 0, true));
                        data.response.details.forEach((detail, index)=>{
                            tableDetails.append(putDetails(detail, index+1));
                        })
                    }else if(length == 0){
                        $('.details-table').html('<div class="not-found"><img src="images/icons/empty.svg"><span>Not details</span></div>');
                        $('.not-found').css({"display":"-webkit-flex", "display":"-moz-flex", "display":"-ms-flex", "display":"-o-flex", "display":"flex", "height":"250px"});
                    }
                    break;
                case 404:

                    break;
                default:
            }
        }
    }).done(()=>{
        cleanData();
        loadingdetailsviewreceiving();
    });
}
function putShipping(shipping, index){
    return `<a href="shipping_summary.php?shippingid=${shipping.shippingid}">${shipping.shippingno}</a>`;
}
function getShippings(receivingdetailid){
    var sectionShippings = $('.data-shippings-view');
    sectionShippings.empty();
    $.ajax({
        url: `/ISS/application/index.php/receiving/details/shippings`,
        type: "GET",
        dataType: 'json',
        data: {
            receivingdetailId: receivingdetailid
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    console.log(data.response.shippings);
                    var length = data.response.shippings.length;
                    if (length > 0) {
                        sectionShippings.append(putShipping({}, 0));
                        data.response.shippings.forEach((shipping, index)=>{
                            sectionShippings.append(putShipping(shipping, index+1));
                        })
                    }else if(length == 0){
                        sectionShippings.html('<div class="not-found"><img src="images/icons/empty.svg"><span>Not shippings</span></div>');
                        $('.not-found').css({"display":"-webkit-flex", "display":"-moz-flex", "display":"-ms-flex", "display":"-o-flex", "display":"flex", "height":"150px", "padding":"10px"});
                        $('.not-found img').css({"padding":"5px"});
                    }
                    break;
                case 404:

                    break;
                default:
            }
        }
    }).done(()=>{
        // cleanData();
    });
}
function getDocuments(receivingid){
    var tableDocuments = $('#documentList');
    tableDocuments.empty();
    console.log('receiving: '+receivingid);
    $.ajax({
        url: `/ISS/application/index.php/document/receiving/get`,
        type: "GET",
        dataType: 'json',
        data: {
            receivingId: receivingid
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    console.log(data);
                    tableDocuments.append(putDocument({}, 0, true));
                    data.response.documents.forEach((document)=>{
                        tableDocuments.append(putDocument(document, -1));
                    })
                    break;
                case 404:
                    // notFoundContent();
                    break;
                default:
            }
        },
        error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{

    });
}
function putDocument(document, index, first=false){
    if(first){
        return `
        <tr>
            <td>View</td>
            <td>File</td>
            <td>Created</td>
        </tr>`;
    }
    return `
    <tr data-documentid=${document.documentid} data-index=${index} id="Doc_${document.documentid}">
        <td>
          <a href="/ISS/application/index.php/document/show?documentId=${document.documentid} " target="upload">
            <img src="/ISS/images/icons/view.svg" width="16" height="16" alt="View">
          </a>
        </td>
        <td>
          ${(document.description.localeCompare('EDI') == 0) ? 'EDI FILE' : document.original_name}
        </td>
        <td>
          ${document.created}
        </td>
    </tr>`;
}
function cleanData(){
    $('table tr').each(function(){
        $('td').each(function(){
            var data = $(this).text();
            if (data == 'null' || data == 'none' || data == undefined) {
                $(this).text('');
            }
        });
    });
}

var receiving = [];
var receivingDetail = [];
var purchaseOrder = [];
var freightbill = [];
var tande = [];
var nolineReceivingDetail = [];
var nolinePurchaseOrder = [];
var extraCargos = [];
var contentNotFound = $('.not-found');
var isFileUpload = false;
var details = [];
var files = [];
var indexFile = 0;
var part = [];
var parts = [];
/************** Los arreglos cuenta con status**************************/
/************* Eliminar: Status 0 **************************************/
/************* Actualizar: Status 1 & Change 1 *************************/
/************* Insertar: Status 2  ************************************/

/***********************Tipos de configuracion**************************/
/*
        Configuracion pick and pack: Mode 1
        Configuracion por defecto: Mode 0
*/
$(document).ready(function() {

/************************* Navigation var***************************
*******************************************************************/
    var searchFilter = {};
    getReceiving();
    getPo();
    getFb();
    getTd();
    getDocuments();
    inputReadOnly();
    var receivingid = $('main').data("receiving-id");

    var flagwarning = 0;
    $('#receivingFile').change(convertFiles);
    $('body').on('click', '.delete-file', function(){
        var documentid = $(this).parents('tr').data('documentid');
        showdialog(()=>{
            deleteFile(documentid)
        }, 'Are you sure delete this file ?')
    })
    $('body').on('click', '.down-warning-errordata', function(){
        if (flagwarning == 0) {
            $('.error-data-edit').css({'animation':'slash-down','animation-duration':'0.35s'});
            $('.down-warning-errordata').css({'transform':'rotate(180deg)','transition':'0.35s'});
            $('.titlebar').addClass('titlebar-bg');
            setTimeout(function(){
                $('.warning-edit-box').css({"bottom":"-200px"});
            },220);
            flagwarning = 1;
        }
        else if (flagwarning == 1) {
            $('.error-data-edit').css({'animation':'slash-up','animation-duration':'0.35s'});
            $('.down-warning-errordata').css({'transform':'rotate(0deg)','transition':'0.35s'});
            $('.titlebar').removeClass('titlebar-bg');
            setTimeout(function(){
                $('.warning-edit-box').css({"bottom":"0px"});
            },220);
            flagwarning = 0;
        }
    });

    function inputReadOnly(){
        var receivinginTransit = $('main').attr('data-edit');
        if (receivinginTransit == 1) {
            $('.inputdata').attr('readonly', true);
        }
    }
    //
    $('#btnServices').click(function(){
        getCustomServices();
    });
    //Abrir contenedor add details
    $('body').on('click', '.addDetails', function(){
        var receivingdetailid = $(this).parent().parent().attr('data-rdid');
        var line = $(this).parent().parent().attr('data-noline');
        var idline = $(this).parent().parent().attr('id');
        getDetails(receivingdetailid, line);

        $('.datadetails-box').addClass('flex');
        $('.addpart-box').css({"display":"block"});
        $('.addpart-box').css({"animation":"pop-in","animation-duration":"0.25s"});
        $('.box-shadow').addClass('visible');
        $('.btn-details').attr('data-line', line);
        $('.btn-details').attr('data-idline', idline);
        $('.addpart-box .title-dialog h2').html('Details for Line no.' + line);
        var ownInventory = $('.selectCustomer').attr('data-owninventory');
        if (ownInventory == 1) {
            getAllPackagetype();
        }
    });
    $('body').on('click', '.close-add-partrd', function(){
        $('.box-shadow').css({"z-index":"102"});
        $('.addornewpart-box').css({"animation":"pop-out","animation-duration":"0.25s"});
        setTimeout(function(){
            $('.addornewpart-box').css({"display":"none"});
        },250);
    });
    $('body').on('click', '.close-add-details', function(){
        $('.box-shadow').removeClass('visible');
        $('.addpart-box').css({"animation":"pop-out","animation-duration":"0.25s"});
        $('.viewdetails-box').css({"animation":"pop-out","animation-duration":"0.25s"});
        setTimeout(function(){
            $('.addpart-box').css({"display":"none"});
            $('.viewdetails-box').css({"display":"none"});
        },250);
        setTimeout(function(){
            $('.datadetails-box').removeClass('flex');
        },250);
        $('.data-adddetail input:text').val('');
        $('.data-adddetail input[type=number]').val('');
        $('.search-part-detail').css({"height":"520px"});
        $('.addpart-box .title-dialog img').show();
        $('.in-xpand').hide();
        $('.viewdetails-box').removeClass('expand-box');
        $('.item-minimize').addClass('display-none');
        $('.viewdetails-box .title-dialog .minimize-details').attr('src', 'images/icons/expand.svg');
        $('.viewdetails-box .title-dialog .minimize-details').attr('class', 'expand-details');
    });
    //Abrir dialogo de busqueda de partes
    $('body').on('click', '.search-part', function(){
        var customerid = $('#customerid').val();
        $('.box-shadow').css({"z-index":"103"});
        $('.search-part-detail').css({"display":"block"});
        $('.search-part-detail').css({"animation":"pop-in","animation-duration":"0.25s"});
        $('.search-part-detail').load('/ISS/partList.php?customerid=' + customerid);
    });
    $('body').on('click', '.search-partrd', function(){
        var customerid = $('#customerid').val();
        $('.box-shadow').css({"z-index":"9998"});
        $('.search-part-detail').css({"display":"block"});
        $('.search-part-detail').css({"z-index":"9999"});
        $('.search-part-detail').css({"animation":"pop-in","animation-duration":"0.25s"});
        $('.search-part-detail').load('/ISS/partList.php?customerid=' + customerid);
    });
    //Expandir contenedor de receivinglines
    $('body').on('click', '.expand-details', function(){
        $('.viewdetails-box').addClass('expand-box');
        $('.viewdetails-box table td').removeClass('display-none');
        $('.viewdetails-box .title-dialog .expand-details').attr('src', 'images/icons/minimize.svg');
        $('.viewdetails-box .title-dialog .expand-details').attr('class', 'minimize-details');
    });
    $('body').on('click', '.minimize-details', function(){
        $('.viewdetails-box').removeClass('expand-box');
        $('.item-minimize').addClass('display-none');
        $('.viewdetails-box .title-dialog .minimize-details').attr('src', 'images/icons/expand.svg');
        $('.viewdetails-box .title-dialog .minimize-details').attr('class', 'expand-details');
    });
    //Doble click para agregar datos de una parte de la busqueda al contenedor de add detail
    $('body').on('dblclick', '#wrapper table tr', function(){
        var status = $(this).data('status');
        var name = $(this).find('.productName').text();
        var description = $(this).find('.descr').text();
        var section = $('.search-partrd').attr('data-section');

        if (section == 'receivingdetail') {
            var idtmp = $('.search-partrd').attr('data-id');

            if (status == 2) {
                var indexarray = $(this).find('.productName').attr('data-index');
                $('#'+idtmp).find('.data-part').attr('data-indexpart', indexarray);
                $('#'+idtmp).find('.data-part').attr('data-exist', 1);
            }
            else {
                var productid = $(this).find('.prodid').val();
                $('#'+idtmp).find('.data-part').attr('data-productid', productid);
                $('#'+idtmp).find('.data-part').attr('data-exist', 1);
            }
            $('.box-shadow').css({"z-index":"102"});
            $('.search-part-detail').css({"animation":"pop-out","animation-duration":"0.25s"});
            setTimeout(function(){
                $('.search-part-detail').css({"display":"none"});
            },250);
            $('#'+idtmp).find('.data-part input').val(name);
            $('#'+idtmp).find('.data-part').attr('data-value', name);
            $('#'+idtmp).find('.data-description input').val(description);
            $('#'+idtmp).find('.data-description').attr('data-value', description);
            $('#'+idtmp).find('.partrd-insert').show();
            $('#'+idtmp).find('.part-receivingdetail').hide();
            $('#'+idtmp).find('.data-qrtypart input').focus();
            $('.addornewpart-box').css({"animation":"pop-out","animation-duration":"0.25s"});
            setTimeout(function(){
                $('.addornewpart-box').css({"display":"none"});
            },250);
        }
        if (section == 'editreceivingdetail') {
            var idtmp = $('.search-partrd').attr('data-id');
            if (status == 2) {
                var indexarray = $(this).find('.productName').attr('data-index');
                $('#'+idtmp).find('.rdpartno').attr('data-indexpart', indexarray);
                $('#'+idtmp).find('.rdpartno').attr('data-exist', 1);
                $('#'+idtmp).find('.rdpartno').attr('data-newproduct', 1);
                $('#'+idtmp).find('.rdpartno').attr('data-asignedpart', 1);
                $('#'+idtmp).find('.rdtracking').attr('data-required', 0);
                $('#'+idtmp).find('.rdtracking input').removeClass('input-error');
            }
            else {
                var productid = $(this).find('.prodid').val();
                $('#'+idtmp).find('.rdpartno').attr('data-productid', productid);
                $('#'+idtmp).find('.rdpartno').attr('data-exist', 1);
                $('#'+idtmp).find('.rdpartno').attr('data-newproduct', 0);
                $('#'+idtmp).find('.rdpartno').attr('data-asignedpart', 1);
                $('#'+idtmp).find('.rdtracking').attr('data-required', 0);
                $('#'+idtmp).find('.rdtracking input').removeClass('input-error');
            }
            $('.box-shadow').css({"z-index":"102"});
            $('.search-part-detail').css({"animation":"pop-out","animation-duration":"0.25s"});
            setTimeout(function(){
                $('.search-part-detail').css({"display":"none"});
            },250);
            $('#'+idtmp).find('.rdpartno input').val(name);
            $('#'+idtmp).find('.rdpartno').attr('data-value', name);
            $('#'+idtmp).find('.rdpartdescription input').val(description);
            $('#'+idtmp).find('.rdpartdescription').attr('data-value', description);
            $('#'+idtmp).attr('data-change', 1);
            $('.addornewpart-box').css({"animation":"pop-out","animation-duration":"0.25s"});
            setTimeout(function(){
                $('.addornewpart-box').css({"display":"none"});
            },250);
            $('.box-shadow').removeClass('visible');
        }
        else {
            if (status == 2) {
                var indexarray = $(this).find('.productName').attr('data-index');
                $('#partno').attr('data-indexpart', indexarray);
                $('#partno').attr('data-exist', 1);
            }
            else {
                var productid = $(this).find('.prodid').val();
                $('#partno').attr('data-productid', productid);
                $('#partno').attr('data-exist', 1);
            }

            $('.box-shadow').css({"z-index":"102"});
            $('.search-part-detail').css({"animation":"pop-out","animation-duration":"0.25s"});
            setTimeout(function(){
                $('.search-part-detail').css({"display":"none"});
            },250);
            $('#partno').val(name);
            $('#desriptionpart').val(description);
            $('#partno').css({"display":"block"});
            $('#packageqtypart').focus();
        }
    });
    $('body').on('click', '.close-search-part', function(){
        $('.box-shadow').css({"z-index":"102"});
        $('.search-part-detail').css({"animation":"pop-out","animation-duration":"0.25s"});
        setTimeout(function(){
            $('.search-part-detail').css({"display":"none"});
        },250);
        $('.search-part-detail').css({"height":"520px"});
    });
    //Insertar detail creando un arreglo
    $('body').on('submit', '#form-adddetails', function(e){
        if (this.checkValidity()) {
            e.preventDefault()
            preinsertDetails = Array();
            var sizedetails = details.length;
            var cont = 0;
            $('.viewdetails-box .table tr').each(function(i){
                if (i != 0) {
                    var status = $(this).attr("data-status");
                    if (status == 2) {
                        cont++;
                    }
                }
            });
            var lineid = "tmpDl_"+cont;
            var partcontent = $('#partno').val().length;
            var havepart = 0;
            if (partcontent > 0) {havepart = 1}
            //Arreglo temporal para insertar details
            preinsertDetails.push({
                receivingdetailno: parseInt($('.btn-details').attr('data-line')),
                productid: parseInt($('#partno').attr('data-productid')),
                productname: $('#partno').val().toString(),
                description: $('#desriptionpart').val().toString(),
                unitid: parseInt($('.selectPackType-part').val()),
                unitPackage: $('.selectPackType-part option:selected').text(),
                originalQty: parseInt($('#packageqtypart').val()),
                partQty: parseInt($('#receivedqtypart').val()),
                lotnumber: $('#lotnumberpart').val().toString(),
                expDate: $('#expdatepart').val().toString(),
                po: $('#popart').val().toString(),
                podate: $('#podatepart').val().toString(),
                index: sizedetails,
                receivinglinesid: lineid,
                datapart: havepart
            })
            var partnew = $('#partno').attr('data-productid');
            var exist = parseInt($('#partno').attr('data-exist'));
            var varProduct = 0;
            var indexProduct;
            if (partnew == 'new') {
                varProduct = 1;
                indexProduct = 0;
                if (exist == 1) {
                    indexProduct = $('#partno').attr('data-indexpart');
                }
                else {
                    var partsl = parts.length;
                    if (partsl > 0) {
                        partsl = (partsl - 1);
                    }
                    if (partsl >= 0) {
                        indexProduct = partsl;
                    }
                }
            }
            if (exist == 1) {
                indexProduct = $('#partno').attr('data-indexpart');
            }
            var partinsert;

            if ($('#partno').val().length > 0) {
                var receiptline = $('.btn-details').attr('data-idline');
                var row = $('#' + receiptline);
                var status = row.attr('data-status');
                if (status == 1) {
                    row.find('.rdtracking').attr('data-required', 0);
                    row.find('.rdtracking input').removeClass('input-error');
                }
                if (status == 2) {
                    row.find('.tmp-rdtracking').attr('data-required', 0);
                    row.find('.tmp-rdtracking').attr('data-asignedpart', 1);
                    row.find('.tmp-rdtracking input').removeClass('input-error');
                }
            }
            var originalqtyy = $('#packageqtypart').val();
            var productidd = $('#partno').attr('data-productid');
            var productt = indexProduct;
            var poo = $('#popart').val();
            var podatee = $('#podatepart').val();
            var lotnumber = $('#lotnumberpart').val();
            var expdate = $('#expdatepart').val();

            if (originalqtyy == "NaN" || originalqtyy == undefined || originalqtyy == "") {originalqtyy = 0}
            if (productidd == "NaN" || productidd == undefined || productidd == "" || productidd == "0" || productidd == "new") {productidd = 0}
            if (productt == "NaN" || productt == undefined || productt == "") {productt = 0}
            if (poo == "NaN" || poo == undefined || poo == "") {poo = "none"}
            if (podatee == "NaN" || podatee == undefined || podatee == "") {podatee = '0000-00-00'}
            if (lotnumber == "NaN" || lotnumber == undefined || lotnumber == "") {lotnumber = "none"}
            if (expdate == "NaN" || expdate == undefined || expdate == "") {expdate = '0000-00-00'}
            //Arreglo para details | con status 2
            details.push({
                receivingdetailno: parseInt($('.btn-details').attr('data-line')),
                productid: parseInt(productidd),
                productname: $('#partno').val().toString(),
                description: $('#desriptionpart').val().toString(),
                unitid: parseInt($('.selectPackType-part').val()),
                unitPackage: $('.selectPackType-part option:selected').text(),
                originalQty: originalqtyy,
                currentQty: originalqtyy,
                partQty: parseInt($('#receivedqtypart').val()),
                lotnumber: lotnumber,
                expDate: expdate,
                po: poo,
                podate: podatee,
                newProduct: varProduct,
                status: 2,
                product: productt,
                index: sizedetails,
                receivinglinesid: lineid
            })
            // indexProduct++;
            part = [];
            writenewdetail();
            $('.data-adddetail input:text').val('');
            $('.data-adddetail input[type=number]').val('');
            $('#partno').css({"display":"none"});
            $('#partno').attr('data-productid', 0);
        }
    });
    function writenewdetail(){
        var tableDetails = $('.viewdetails-box .table');
        var nodetailline = $('.viewdetails-box .table-height > table tr').length;
        var status;
        var tempcont = 0;

        $('.viewdetails-box .table-height > table tr').map(function(i){
            if (i != 0) {
                status = $(this).data('status');
                if (status == 0) {
                    tempcont++;
                }
            }
        });
        if (nodetailline == 0) {
            $('.viewdetails-box').css({"display":"block"});
            $('.viewdetails-box').css({"animation":"pop-in","animation-duration":"0.25s"});
            nodetailline = 1;
            tableDetails.append(`
                <tr data-table-column=0>
                    <td style="width: 25px;">No.</td>
                    <td>Delete</td>
                    <td>Edit</td>
                    <td>Part #</td>
                    <td>Description</td>
                    <td>Package Qty</td>
                    <td class="item-minimize display-none">Package Type</td>
                    <td class="item-minimize display-none">Received Qty Part</td>
                    <td class="item-minimize display-none">Lot Number</td>
                    <td class="item-minimize display-none">Exp. Date</td>
                    <td class="item-minimize display-none">PO</td>
                    <td class="item-minimize display-none">PO Date</td>
                </tr>
            `);
        }
        nodetailline = (nodetailline - tempcont);

        for (var i = 0; i < preinsertDetails.length; i++) {
            tableDetails.append(`
            <tr data-status="2" data-havepart="${preinsertDetails[i].datapart}" data-change="1" data-indexarray="${preinsertDetails[i].index}" data-rdline="${preinsertDetails[i].receivingdetailno}" data-rlid="${preinsertDetails[i].receivinglinesid}" id="${preinsertDetails[i].receivinglinesid}" data-lotnumber="${preinsertDetails[i].lotnumber}" data-linedetail="${nodetailline + i}">
                <td class="linedetail nolinedetail_${nodetailline + i}">${nodetailline + i}</td>
                <td class="icondetails"><img src="images/icons/delete.svg" class="deleteDetail btn-action-delete"></td>
                <td class="icondetails"><img src="images/icons/edit.svg" class="editDetail btn-action-edit"></td>
                <td class="productname-bd" data-productid="${preinsertDetails[i].productid}" data-productno="${preinsertDetails[i].productname}" data-columnname="Part #">${preinsertDetails[i].productname}</td>
                <td class="productdescription-bd" data-productdesc="${preinsertDetails[i].description}" data-columnname="Description">${preinsertDetails[i].description}</td>
                <td class="originalqty-bd">${preinsertDetails[i].originalQty}</td>
                <td class="unit-bd item-minimize display-none" data-value="${preinsertDetails[i].unitid}">${preinsertDetails[i].unitPackage}</td>
                <td class="partqty-bd item-minimize display-none">${preinsertDetails[i].partQty}</td>
                <td class="lotnumber-bd item-minimize display-none" data-value="${preinsertDetails[i].lotnumber}">${preinsertDetails[i].lotnumber}</td>
                <td class="expdate-bd item-minimize display-none">${preinsertDetails[i].expDate}</td>
                <td class="productpo-bd item-minimize display-none">${preinsertDetails[i].po}</td>
                <td class="productpodate-bd item-minimize display-none" data-value="${preinsertDetails[i].podate}">${preinsertDetails[i].podate}</td>
            </tr>`);
        }
    }
    //Eliminar extra extra cargos
    preextraCargos = Array();
    $('body').on('click', '.btn_delete_cargo', function(){
        var row = $(this).parent().parent();
        var status = row.data('status');

        if (status == 1) {
            row.data('status', 0)
            row.hide();
            preextraCargos.push({
                servicereceivingid: row.data('id'),
                receivingid: $('#receivingid_').val(),
                status: row.data('status')
            })
        }
        if (status == 2) {
            row.remove();
        }
    });
    //
    $('body').on('click', '.cancel-exit-btn', function(){
        $('.data-notsaved').css({"animation":"pop-out","animation-duration":"0.25s"});
        setTimeout(function(){
            $('.data-notsaved').css({"display":"none"});
            $('.box-unsaved').removeClass('flex');
        },250);
        $('.changes-conts').empty();
        $('.unsaved-buttons').remove();
        $('.updates-changes .data-changes > div').empty();
        $('.add-changes .data-changes > div').empty();
        $('.delete-changes .data-changes > div').empty();
    });
    //Evento change del Select New Charge
    $('body').on('change', '#service', function(){
        //Variables con los datos de services
        var serviceid = $('#service').val();
        var description = $('#service option:selected').text().trim();
        var cost = $('#service option:selected').attr('data-cost');
        var code = $('#service option:selected').attr('data-code');
        var variant = $('#service option:selected').attr('data-variant');
        var increment = $('#increment');
        if (serviceid == 'custom0') {
            return;
        }
        $('#rowBaseRate').hide();
        $('#select_service').hide();
        $('.applied-charges').hide();
        $('#add_service').show();
        $('#other_cost').hide();
        //Se escriben los datos
        $('#service_code').html(code);
        $('#service_description').html(description);
        $('#service_cost').html(cost);
        $('#serviceQty').val(1);
        //
        if (variant == 1) {
            $('#service_cost').html('Set');
            $('#other_cost').show();
            $('#other_cost input').val(cost);
            $('.inminutes').hide();
        }
        else{
            $('#service_cost').html(cost);
            if (description.toLowerCase() === 'revision' && increment != 0) {
                $('#rowBaseRate').show();
                var baserate = $('#baserate').val();
                $('#service_baserate').html(baserate);
                $('#other_cost input').val('');
                $('#other_cost p').html('Total Time');
                $('#other_cost').show();
                $('.inminutes').show();
                $('#other_cost input').css({"width":"50px"});
                $('#other_cost input').focus();
            }
            else if (description.toLowerCase() === 'revision' && increment == 0) {
                $('#other_cost input').val('');
                $('#other_cost p').html('Total Time');
                $('.inminutes').show();
                $('#other_cost').show();
                $('#other_cost input').css({"width":"50px"});
                $('#other_cost input').focus();
            }
        }

    });
    //Cancelar agregacion de un nuevo extra cargo
    $("#btn_cancel").click(function() {
        $('#add_service').hide();
        $('#select_service').show();
        $('#rowBaseRate').hide();
        $('#other_cost p').html('');
        $('#other_cost p').html('Variant Cost');
        $('#service').val("custom0");
        $('#other_cost input').val("");
        $('#serviceQty').val('1');
        $('#information').val('');
        $('.applied-charges').show();
        $('#other_cost input').css({"width":"100%"});
    });
    //Agregar un nuevo extra cargo
    $('#add-charge').click(function(){
        var preinsertextraCargos = Array();
        var cost = $('#service_cost').text();
        var other_cost = $('#other_cost input').val();
        var qty = $('#serviceQty').val();
        var description = $('#service_description').text().trim().toLowerCase();
        var increment = $('#service option:selected').attr('data-increment');
        var baserate = $('#service option:selected').attr('data-baserate');

        if (cost == "Set") {
            cost = $('#other_cost input').val();
        }
        if (cost == "Set" && other_cost == "") {
            $('#other_cost input').addClass('input-error');
            $('#other_cost input').attr('placeholder', "Please fill");
        }
        else {
            if (description == "revision" && increment != 0) {
                var tiempoCubierto = (baserate / cost) * 60;
                var tiempoRestante = other_cost - tiempoCubierto;
                if (tiempoRestante < 0){
                    other_cost = baserate;
                }
                else if (tiempoRestante > 0){
                 var periodos = Math.ceil(tiempoRestante / increment);
                 var costoPorPeriodo = (cost / 60) * increment;
                 var costoAdicional = costoPorPeriodo * periodos;
                 other_cost = parseInt(costoAdicional) + parseInt(baserate);
                }
            }
            else if (description =='revision' && increment == 0){
                other_cost = cost;
            }
            // if (other_cost != "") {
            //     cost = (isNaN(other_cost)) ? "0.00" : other_cost;
            // }
            if (qty === "" || isNaN(qty) || qty < 1) {
                $('#serviceQty').val('1');
                qty = "1";
            }
        }
        for (var i = 0; i < qty; i++) {
            preinsertextraCargos.push({
                receivingid: $('#receivingid_').val(),
                serviceid: $('#service').val(),
                cost: cost,
                status: '2',
                information: $('#information').val(),
                description: $('#service_description').text()
            })
        }

        $('#select_service').show();
        $('.applied-charges').show();
        $('#add_service').hide();
        $('#service').val('custom0');
        $('.items-add-service input').val('');
        writeextracargos(preinsertextraCargos);
    });
    function writeextracargos(preinsertextraCargos){
        var tablecargos = $('#services_list table');
        for (var i = 0; i < preinsertextraCargos.length; i++) {
            if (preinsertextraCargos[i].status == 2) {
                tablecargos.append(`
                    <tr data-status="2" id="tmpcharge_${[i]}" data-serviceid="${preinsertextraCargos[i].serviceid}" data-cost="${preinsertextraCargos[i].cost}" data-information="${preinsertextraCargos[i].information}">
                        <td>
                            <img src="images/icons/delete.svg" class="btn_delete_cargo">
                        </td>
                        <td></td>
                        <td>${preinsertextraCargos[i].description}</td>
                        <td>$ ${preinsertextraCargos[i].cost}</td>
                    </tr>
                    `);
            }
        }
    }
    $('body').on('change', '.truckingbox', function(){
        // var row = $(this).parent().parent();
        var value = $(this).val();
        var status = $(this).parent().parent().attr('data-status');
        var row = $(this).parent().parent().attr('id');
        var mode = $('#customerid option:selected').attr('data-mode');

        if (status == 1) {var receiptid = $(this).parent().parent().attr('data-rdid');}
        if (value.length > 0) {
            $(this).parent().attr('data-required', 0);
        }
        else {
            if(status == 1){
                if (mode == 1) {
                    qtyPart(receiptid);
                }
                if (mode == 0) {
                    var asigendpart = $('#'+row).find('.rdpartno').attr('data-asignedpart');
                    if (asigendpart == 1) {$(this).parent().attr('data-required', 0);}
                    if (asigendpart == 0) {$(this).parent().attr('data-required', 1);}
                }
            }
            if (status == 2) {$(this).parent().attr('data-required', 1);}
        }
    });
    $('body').on('change', '#vendorid', function(){
        var name = $('#vendorid option:selected').text();
        $(this).attr('title', name);
    });
    $('body').on('change', '#carrierid', function(){
        var name = $('#carrierid option:selected').text();
        $(this).attr('title', name);
    });
    $('body').on('change', '#customerid', function(){
        var id = $('#customerid').val();
        var mode = $('#customerid option:selected').attr('data-mode');
        var name = $('#customerid option:selected').text();
        var modein = $(this).attr('data-mode');
        getParts(id);
        if (mode != modein) {
            details = [];
            parts = [];
            receivinglines = [];
            getRd();
        }
        if (mode === modein){
            receiptMode(mode);
            $('#customerid').data('select', id);
            var own = $('.selectCustomer option:selected').attr('data-owninventory');
            $('.selectCustomer').attr('data-owninventory', own);
            getCustomServices();
            getPackagetype();
            selectPackage();
        }
        $(this).attr('data-mode', mode);
        $(this).attr('title', name);
    });
    //Se detectan cambios
    $('body').on('change', '.inputdatareceiving', function(event){
        var id = event.target.id;
        var customerSelect = $("#customerid option:selected").val();
        var vendorSelect = $("#vendorid option:selected").val();
        var carrierSelect = $("#carrierid option:selected").val();
        var vendornew = $('#vendornew').val().length;
        var carriernew = $('#carriernew').val().length;
        sectionbox = 'receiving';
        this.dataset.ch = 1;
        changes(id, 0, sectionbox);
        var inputlength = $(this).val().length;
        var inputvalue = $(this).val();
        if (inputlength == 0 || inputvalue == 'select0') {
            $(this).addClass('input-error');
        }
        if (inputlength > 0) {
            $(this).removeClass('input-error');
        }
        if (customerSelect == 'select0' || vendorSelect == 'select0'|| carrierSelect == 'select0'){
            $('.barcode').hide();
        }
        if (customerSelect >= 0 && vendorSelect >= 0 && carrierSelect >= 0){
            $('.barcode').show();
        }
        if(vendornew > 0 || carriernew > 0){
            $('.barcode').show();
        }
        if (id == "customerid" && $('#customerid').val() !== ''){
            // UpdateDetails();
        }
    });
    //Click a boton nuevo vendor
    $('body').on('click', '.newvendor', function(){
        $('#vendorid').hide();
        $('.newvendor').hide();
        $('.selectvendor').show();
        $('#vendornew').show();
        $('#vendornew').data('value', 1);
        $('#vendorid').attr('data-ch', 1);
        var valuevendor = $('#vendornew').data('value');
        if (valuevendor == "1") {
            $('.barcode').hide();
        }
        $('#vendorid').attr('data-active', 0);
        $('#vendornew').attr('data-active', 1);
        changes(0,0, 'receiving');
    });
    $('body').on('click', '.selectvendor', function(){
        $('#vendorid').show();
        $('.newvendor').show();
        $('.selectvendor').hide();
        $('#vendornew').val('');
        $('#vendornew').hide();
        var vendorSelect = $("#vendorid option:selected").val();
        if (vendorSelect == 'select0') {
            $('.barcode').hide();
            $('#vendorid').attr('data-ch', 1);
        }
        $('#vendorid').attr('data-ch', 0);
        $('#vendorid').attr('data-active', 1);
        $('#vendornew').attr('data-active', 0);
        changes(0,0, 'receiving');
    });
    //Click a boton nuevo carrier
    $('body').on('click', '.newcarrier', function(){
        $('#carrierid').hide();
        $('.newcarrier').hide();
        $('.selectcarrier').show();
        $('#carriernew').show();
        $('#carrierid').data('value', 1);
        $('#carrierid').attr('data-ch', 1);
        var valuecarrier = $('#carrierid').data('value');
        if (valuecarrier == "1") {
            $('.barcode').hide();
        }
        $('#carrierid').attr('data-active', 0);
        $('#carriernew').attr('data-active', 1);
        changes(0,0, 'receiving');
    });
    $('body').on('click', '.selectcarrier', function(){
        $('#carrierid').show();
        $('.newcarrier').show();
        $('.selectcarrier').hide();
        $('#carriernew').val('');
        $('#carriernew').hide();
        var carrierSelect = $("#carrierid option:selected").val();
        if (carrierSelect == 'select0') {
            $('.barcode').hide();
        }
        $('#carrierid').attr('data-ch', 0);
        $('#carrierid').attr('data-active', 1);
        $('#carriernew').attr('data-active', 0);
        changes(0,0, 'receiving');
    });
    $('.cancel-receiving').click(function(){
        showdialog(()=>{
            backSearchReceiving();
        }, 'Are you sure cancel edit this Receiving?')
    });
    $("body").on("click",'.delete-tmp-po', function(){
        $(this).parent().parent().remove();
    });
    $("body").on("click",'.delete-tmp-fb', function(){
        $(this).parent().parent().remove();
    });
    $("body").on("click",'.delete-tmp-rd', function(){
        $(this).parent().parent().remove();
    });
    $("body").on("click",'.delete-tmp-td', function(){
        $(this).parent().parent().remove();
    });
    $(".calendar").datepicker();

    $("body").on("click",'.deleteDetail', function(){
        var row = $(this).parent().parent();
        var id = row.attr('data-rlid');
        var nodetail = $(this).parent().parent().find('.linedetail').text();
        showdialog(()=>{
            deletebystatusdetails(id);
        }, 'Are you sure delete this Detail ' + nodetail + '?')
    });
    //Boton cancelar edicion de un receiving line
    //ACtiva el boton de eliminar, elimina el estilo del renglon y habilita el boton de edicion en los renglones
    $('body').on("click", '.cancel-edit-part', function(){
        $('.details-form').removeAttr('id');
        $('.details-form').attr('id', "form-adddetails");
        $('.cancel-edit-part').css({"display":"none"});
        $('.btn-insertpart').css({"justify-content":"flex-end"});
        $('.add-detail').removeClass('update-detail');
        var id = $(this).attr('data-detailid');
        var row = $('.viewdetails-box #'+id);
        $('.btn-action-edit').addClass('editDetail');
        $('.add-detail').addClass('btn-details');
        $('.add-detail').attr('value', 'Insert');
        row.find('.btn-action-delete').addClass('deleteDetail');
        row.find('td').css({"background":"#fff"});
        //Limpiar los inputs
        $('#partno').css({"display":"none"});
        $('.data-adddetail input:text').val('');
        $('.data-adddetail input[type=number]').val('');
    });
    $("body").on("click",'.editDetail', function(){
        $('.details-form').removeAttr('id');
        $('.details-form').attr('id', "form-editdetails");
        $('.viewdetails-box').removeClass('expand-box');
        $('.item-minimize').addClass('display-none');
        $('.viewdetails-box .title-dialog .minimize-details').attr('src', 'images/icons/expand.svg');
        $('.viewdetails-box .title-dialog .minimize-details').attr('class', 'expand-details');
        $('.cancel-edit-part').css({"display":"block"});
        $('.btn-insertpart').css({"justify-content":"space-between"});
        $('.editDetail').removeClass('editDetail');
        $('.btn-details').removeClass('btn-details');
        $('.add-detail').addClass('update-detail');
        $('.add-detail').attr('value', 'Update');
        var productid = $(this).parent().parent().find('.productname-bd').attr('data-productid');
        $('#partno').attr('data-productid', productid);
        $('#partno').css({"display":"block"});
        var status = $(this).parent().parent().attr('data-status');
        var detailid = $(this).parent().parent().attr('id');
        var nodetail = $(this).parent().parent().find('.linedetail').text();

        $('.cancel-edit-part').attr('data-detailid', detailid);
        $('.add-detail').attr('data-idline', detailid);
        $('.add-detail').attr('data-lineno', nodetail);
        $(this).parent().parent().find('.deleteDetail').removeClass('deleteDetail');
        $(this).parent().parent().find('td').css({"background":"rgb(214, 214, 214)"});

        if (status == 1) {
            var idreceivingline = $(this).parent().parent().attr('data-rlid');
            $('.update-detail').attr('data-receivinglinesid', idreceivingline);
            var rowid;

            $('.viewdetails-box .table tr').each(function(i){
                if (i != 0) {
                    rowid = $(this).attr('id');

                    if (rowid == detailid) {
                        var r = $('#'+detailid);
                        var name = r.find('.productname-bd').attr('data-value');
                        var description = r.find('.productdescription-bd').attr('data-value');
                        var packageqty = r.find('.originalqty-bd').attr('data-value');
                        var unit = r.find('.unit-bd').attr('data-value');
                        var partqty = r.find('.partqty-bd').attr('data-value');
                        var lotnumber = r.find('.lotnumber-bd').attr('data-value');
                        var expadate = r.find('.expdate-bd').attr('data-value');
                        var po = r.find('.productpo-bd').attr('data-value');
                        var podate = r.find('.productpodate-bd').attr('data-value');
                        if (name == "null") {name = "";}
                        if (description == "null") {description = "";}
                        if (packageqty == 0) {packageqty = "";}
                        if (lotnumber == "none") {lotnumber = "";}
                        if (po == "none") {po = "";}
                        if (podate == "0000-00-00") {podate = "";}
                        if (expadate == "0000-00-00") {expadate = "";}
                        $('#partno').val(name);
                        $('#desriptionpart').val(description);
                        $('#packageqtypart').val(packageqty);
                        $('.selectPackType-part').val(unit);
                        $('#receivedqtypart').val(partqty);
                        $('#lotnumberpart').val(lotnumber);
                        $('#expdatepart').val(expadate);
                        $('#popart').val(po);
                        $('#podatepart').val(podate);
                    }
                }
            });
        }
        if (status == 2) {
            for (var i = 0; i < details.length; i++) {
                if (details[i].receivinglinesid == detailid) {
                    var packageqty = details[i].originalQty;
                    var lotnumber = details[i].lotnumber;
                    var expdate = details[i].expDate;
                    var po = details[i].po;
                    var podate = details[i].podate;
                    if (packageqty == 0) {packageqty = "";}
                    if (lotnumber == "none") {lotnumber = "";}
                    if (expdate == "0000-00-00") {expdate = "";}
                    if (po == "none") {po = "";}
                    if (podate == "0000-00-00") {podate = "";}
                    $('#partno').val(details[i].productname);
                    $('#desriptionpart').val(details[i].description);
                    $('#packageqtypart').val(packageqty);
                    $('.selectPackType-part').val(details[i].unitid);
                    $('#receivedqtypart').val(details[i].partQty);
                    $('#lotnumberpart').val(lotnumber);
                    $('#expdatepart').val(expdate);
                    $('#popart').val(po);
                    $('#podatepart').val(podate);
                }
            }
        }
    });
    //Evento click para actualizar receiving lines
    $('body').on('submit', '#form-editdetails', function(e){
        if (this.checkValidity()) {
            e.preventDefault()
            var lineid = $('.update-detail').attr('data-lineno');
            var line = $('.update-detail').attr('data-idline');

            var partnew = $('#partno').attr('data-productid');
            var status = $('#' + line).attr('data-status');

            var varProduct = 0;
            var indexProduct;
            if (partnew == 'new') {
                varProduct = 1;
                indexProduct = 0;
                var partsl = parts.length;
                partsl = (partsl - 1);
                if (partsl >= 0) {
                    indexProduct = (partsl + indexProduct);
                }
            }

            var change = $('#'+line).attr('data-change');
            //Si un receiving line ya habia tenido cambios y se creo su arreglo, se actualizan
            //los cambios en el arreglo, obteniendo la posicion del arreglo
            if (change == 1) {
                var indexarray = $('#'+line).attr('data-indexarray');
                details[indexarray].receivingdetailno = parseInt($('.update-detail').attr('data-line'))
                details[indexarray].productid = parseInt($('#partno').attr('data-productid'))
                details[indexarray].productname = $('#partno').val().toString()
                details[indexarray].description = $('#desriptionpart').val().toString()
                details[indexarray].unitid = parseInt($('.selectPackType-part').val())
                details[indexarray].unitPackage = $('.selectPackType-part option:selected').text()
                details[indexarray].originalQty = parseInt($('#packageqtypart').val())
                details[indexarray].partQty = parseInt($('#receivedqtypart').val())
                details[indexarray].lotnumber = $('#lotnumberpart').val().toString()
                details[indexarray].expDate = $('#expdatepart').val().toString()
                details[indexarray].po = $('#popart').val().toString()
                details[indexarray].podate = $('#podatepart').val().toString()
                details[indexarray].newProduct = varProduct
                details[indexarray].product = indexProduct
            }
            else if(status == 1){
                var productidd = $('#partno').attr('data-productid');
                var productnamee = $('#partno').val();
                var originalQtyy = $('#packageqtypart').val();
                var currentQtyy = $('#packageqtypart').val();
                var expDatee = $('#expdatepart').val();
                var lotnumberr = $('#lotnumberpart').val();
                var poo = $('#popart').val();
                var podatee = $('#podatepart').val();

                if (productidd == "new" || productidd == "NaN" || productidd == "null" || productidd =="undefined" || productidd == "") {productidd = 0;}
                if (indexProduct == "new" || indexProduct == "NaN" || indexProduct == "null" || indexProduct == undefined || indexProduct == "") {indexProduct = 0;}
                if (productnamee == "new" || productnamee == "NaN" || productnamee == "null" || productnamee == undefined || productnamee == "") {productnamee = "none";}
                if (originalQtyy == "new" || originalQtyy == "NaN" || originalQtyy == "null" || originalQtyy == undefined || originalQtyy == "") {originalQtyy = 0; currentQtyy = 0;}
                if (expDatee == "new" || expDatee == "NaN" || expDatee == "null" || expDatee =="undefined" || expDatee == "") {expDatee = "0000-00-00";}
                if (lotnumberr == "new" || lotnumberr == "NaN" || lotnumberr == "null" || lotnumberr =="undefined" || lotnumberr == "") {lotnumberr = "none";}
                if (poo == "new" || poo == "NaN" || poo == "null" || poo =="undefined" || poo == "") {poo = "none";}
                if (podatee == "new" || podatee == "NaN" || podatee == "null" || podatee =="undefined" || podatee == "") {podatee = "0000-00-00";}

                details.push({
                    receivinglinesid: parseInt($('.update-detail').attr('data-receivinglinesid')),
                    receivingdetailno: parseInt($('.update-detail').attr('data-line')),
                    productid: parseInt(productidd),
                    productname: productnamee,
                    description: $('#desriptionpart').val().toString(),
                    unitid: parseInt($('.selectPackType-part').val()),
                    unitPackage: $('.selectPackType-part option:selected').text(),
                    originalQty: parseInt(originalQtyy),
                    currentQty: parseInt(currentQtyy),
                    partQty: parseInt($('#receivedqtypart').val()),
                    lotnumber: currentQtyy,
                    expDate: currentQtyy,
                    po: poo,
                    podate: podatee,
                    newProduct: varProduct,
                    product: indexProduct,
                    status: 1
                })
            }
            preinsertDetails = Array();
            preinsertDetails.push({
                receivingdetailno: parseInt($('.update-detail').attr('data-line')),
                productid: parseInt($('#partno').attr('data-productid')),
                productname: $('#partno').val().toString(),
                description: $('#desriptionpart').val().toString(),
                unitid: parseInt($('.selectPackType-part').val()),
                unitPackage: $('.selectPackType-part option:selected').text(),
                originalQty: parseInt($('#packageqtypart').val()),
                partQty: parseInt($('#receivedqtypart').val()),
                lotnumber: $('#lotnumberpart').val().toString(),
                expDate: $('#expdatepart').val().toString(),
                po: $('#popart').val().toString(),
                podate: $('#podatepart').val().toString(),
                newProduct: varProduct,
                product: indexProduct,
            })

            var lineupdate = $('#' + line);
            lineupdate.attr('data-change', 1);
            var lengthdetail = details.length;
            lengthdetail = (lengthdetail - 1);
            if (lengthdetail == -1) {
                lengthdetail = 0;
            }
            var existindex = lineupdate.attr('data-indexarray');
            if (existindex == "") {
                lineupdate.attr('data-indexarray', lengthdetail);
            }
            for (var i = 0; i < preinsertDetails.length; i++) {
                lineupdate.find('.productname-bd').text(preinsertDetails[i].productname);
                lineupdate.find('.productname-bd').attr('data-value', preinsertDetails[i].productname);
                lineupdate.find('.productname-bd').attr('data-productid', preinsertDetails[i].productid);
                lineupdate.find('.productname-bd').attr('data-productposition', preinsertDetails[i].product);
                lineupdate.find('.productname-bd').attr('data-newproduct', preinsertDetails[i].newProduct);
                lineupdate.find('.productdescription-bd').text(preinsertDetails[i].description);
                lineupdate.find('.productdescription-bd').attr('data-value',preinsertDetails[i].description);
                lineupdate.find('.originalqty-bd').text(preinsertDetails[i].originalQty);
                lineupdate.find('.originalqty-bd').attr('data-value',preinsertDetails[i].originalQty);
                lineupdate.find('.unit-bd').text(preinsertDetails[i].unitPackage);
                lineupdate.find('.unit-bd').attr('data-value',preinsertDetails[i].unitid);
                lineupdate.find('.partqty-bd').text(preinsertDetails[i].partQty);
                lineupdate.find('.partqty-bd').attr('data-value',preinsertDetails[i].partQty);
                lineupdate.find('.lotnumber-bd').text(preinsertDetails[i].lotnumber);
                lineupdate.find('.lotnumber-bd').attr('data-value',preinsertDetails[i].lotnumber);
                lineupdate.find('.expdate-bd').text(preinsertDetails[i].expDate);
                lineupdate.find('.expdate-bd').attr('data-value',preinsertDetails[i].expDate);
                lineupdate.find('.productpo-bd').text(preinsertDetails[i].po);
                lineupdate.find('.productpo-bd').attr('data-value',preinsertDetails[i].po);
                lineupdate.find('.productpodate-bd').text(preinsertDetails[i].podate);
                lineupdate.find('.productpodate-bd').attr('data-value',preinsertDetails[i].podate);
            }

            //Limpiar los inputs
            $('#partno').css({"display":"none"});
            $('.data-adddetail input:text').val('');
            $('.data-adddetail input[type=number]').val('');
            $('.cancel-edit-part').css({"display":"none"});
            $('.btn-insertpart').css({"justify-content":"flex-end"});
            $('.add-detail').removeClass('update-detail');
            lineupdate.find('.btn-action-delete').addClass('deleteDetail');
            lineupdate.find('td').css({"background":"#fff"});
            $('.add-detail').attr('value', 'Insert');
            $('.add-detail').addClass('btn-details');
            $('.btn-action-edit').addClass('editDetail');
            $('.details-form').removeAttr('id');
            $('.details-form').attr('id', "form-adddetails");
        }
    });
	$("body").on("click",'.deletePo', function(){
        var row = $(this).parent().parent();
        var id = row.data('poid');
        var nopo = $(this).parent().next().next().children().val();
        showdialog(()=>{
            deletebystatuspo(id);
        }, 'Are you sure delete Purchase Order ' + nopo + '?')
    });
    $("body").on("click",'.deleteRd', function(){
        var row = $(this).parent().parent();
        var id = row.data('rdid');
        var nord = $(this).parent().parent().data('rdid');
        showdialog(()=>{
            deletebystatusrd(id)
        }, 'Are you sure delete Receiving Detail ' + nord + '?')
    });
    $("body").on("click",'.deleteFb', function(){
        var row = $(this).parent().parent();
        var id = row.data('fbid');
        var nofb = $(this).parent().next().children().val();
        showdialog(()=>{
            deletebystatusfb(id);
        }, 'Are you sure delete Purchase Order ' + nofb + '?')
    });
    $("body").on("click",'.deleteTd', function(){
        var row = $(this).parent().parent();
        var id = row.data('tdid');
        var notd = $(this).parent().next().children().val();
        showdialog(()=>{
            deletebystatustd(id);
        }, 'Are you sure delete T and E / Inbond ' + notd + '?')
    });
    $("body").on("click",'.exit-btn', function(){
        $('.data-notsaved').css({"animation":"pop-out","animation-duration":"0.25s"});
        setTimeout(function(){
            $('.data-notsaved').css({"display":"none"});
        },250);
        location.href = "/ISS/receiving_search.php";
    });
    $('.close-newfb').click(function(){
        var headT = $('.content-table-newfb .table');
        headT.empty();
        $('.box-shadow').removeClass('visible');
        $('.add-freightbill-box').css({"animation":"pop-out","animation-duration":"0.25s"});
        setTimeout(function(){
            $('.add-freightbill-box').css({"display":"none"});
        },250);
    });
    $('.close-newtd').click(function(){
        var headT = $('.content-table-newtd .table');
        headT.empty();
        $('.box-shadow').removeClass('visible');
        $('.add-tande-box').css({"animation":"pop-out","animation-duration":"0.25s"});
        setTimeout(function(){
            $('.add-tande-box').css({"display":"none"});
        },250);
    });
    $('.close-newpo').click(function(){
        var headT = $('.content-table-newpo .table');
        headT.empty();
        $('.box-shadow').removeClass('visible');
        $('.add-purchase-box').css({"animation":"pop-out","animation-duration":"0.25s"});
        setTimeout(function(){
            $('.add-purchase-box').css({"display":"none"});
        },250);
    });
    $('.close-ups').click(function(){
        var headT = $('#dialog-form');
        headT.empty();
        $('.box-shadow').removeClass('visible');
        $('.ups-box').css({"animation":"pop-out","animation-duration":"0.25s"});
        setTimeout(function(){
            $('.ups-box').css({"display":"none"});
        },250);
    });
    $('.close-newrd').click(function(){
        var headTrd = $('.content-table-newrd .table');
        headTrd.empty();
        $('.box-shadow').removeClass('visible');
        $('.add-receiving-box').css({"animation":"pop-out","animation-duration":"0.25s"});
        setTimeout(function(){
            $('.add-receiving-box').css({"display":"none"});
        },250);
    });
    $('.add-purchase-orders').click(function(){
        $('.box-shadow').addClass('visible');
        $('.add-purchase-box').css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
        writePo();
        getReceivingRowsnewPO();
    });

    $('.insert-po').click(function(){
        var receivingid = $('main').data("receiving-id");
        var newPo = Array();
        insertpo(receivingid, newPo);
        validacionInputsPO(newPo);
    });
    $('body').on('click', '.editPartRD', function(){
        $('.box-shadow').addClass('visible');
        $('.new-partrd').attr('data-section', 'editreceivingdetail');
        $('.search-partrd').attr('data-section', 'editreceivingdetail');
        $('.addornewpart-box').css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
        var id = $(this).parent().parent().attr('id');
        var editpart = $(this).attr('data-editpart');

        $('.search-partrd').attr('data-id', id);
        $('.new-partrd').attr('data-id', id);
        if (editpart == "edit") {
            $('.new-partrd').attr('data-editpart', editpart);
        }
        $('.close-add-partrd').addClass('close-add-editpartrd');
        $('.close-add-partrd').removeClass('close-add-partrd');
    });
    $('body').on('click', '.close-add-editpartrd', function(){
        $('.box-shadow').removeClass('visible');
        $('.addornewpart-box').css({"animation":"pop-out","animation-duration":"0.25s"});
        setTimeout(function(){
            $('.addornewpart-box').css({"display":"none"});
        },250);
    });
    $('body').on('click', '.part-receivingdetail', function(){
        $('.box-shadow').addClass('visible');
        $('.box-shadow').css({"z-index":"9998"});
        $('.new-partrd').attr('data-section', 'receivingdetail');
        $('.search-partrd').attr('data-section', 'receivingdetail');
        $('.addornewpart-box').css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
        var id = $(this).parent().parent().attr('data-id');
        $('.search-partrd').attr('data-id', id);
        $('.new-partrd').attr('data-id', id);
    });
    $('.add-receiving-details').click(function(){
        var modecolumns = $('#customerid option:selected').attr('data-mode');

        $('.box-shadow').addClass('visible');
        $('.add-receiving-box').css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
        writeRD();
        if (modecolumns == 1) {
            $('.default-column').remove();
        }
        else if (modecolumns == 0) {
            $('.pickandpack-column').remove();
        }
        getUnitMesurament();
        getLocation();
        getPackagetype();
    });
    $('.insert-rd').click(function(){
        var newRd = Array();
        var receivingid = $('main').data("receiving-id");
        var modereceipt = $('#customerid option:selected').attr('data-mode');
        insertrd(receivingid, newRd, modereceipt);
        validacionInputsRD(newRd);
    });
    $('.add-freightbill').click(function(){
        $('.box-shadow').addClass('visible');
        $('.add-freightbill-box').css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
        writeFb();
    });
    $('.add-tande').click(function(){
        $('.box-shadow').addClass('visible');
        $('.add-tande-box').css({"animation":"pop-in","animation-duration":"0.25s","display":"block"});
        writeTd();
    });
    $('.insert-fb').click(function(){
        var receivingid = $('main').data("receiving-id");
        var newFb = Array();
        insertfb(receivingid, newFb);
        validacionInputsFB(newFb);
    });
    $('.insert-td').click(function(){
        var receivingid = $('main').data("receiving-id");
        var newTd = Array();
        inserttd(receivingid, newTd);
        validacionInputsTE(newTd);
    });

});

async function convertFiles(){
  isFileUpload = true;
  for(var i = 0; i < this.files.length; i++){
    await encodeBase64(this.files[i]);
  }
  isFileUpload = false;
}

function encodeBase64(file){
  return new Promise((resolve, reject) => {
    var reader = new FileReader();
    reader.onload = () => {
      var file64 = {};
      file64.name = file.name;
      file64.type = file.type;
      file64.fileBase64 = reader.result;
      file64.status = 2;
      file64.documentid = 0;
      file64.index = indexFile;
      files.push(file64);
      indexFile++;
      $('#documentList').append(putDocument({
        documentid: file64.documentid,
        original_name: file64.name,
        created: new Date().toString('yyyy-mm-dd hh:mm:ss'),
        description: ''
      }, file64.index));
      resolve();
    }
    if(file)
    reader.readAsDataURL(file);
    else{
      reject();
    }
  });
}

function deleteFile(documentid){
  var index = 0;

  var isDelete = false;
  if(documentid == 0){
    index = $(this).parents('tr').data('index');
    files.forEach((file, i)=>{
      if(file.index == index){
        files.splice(i, 1);
      }
    });
  }else{
    files.forEach((file, i)=>{
      if(file.documentid == documentid){
        isDelete = true
        return;
      }
    });
    if(!isDelete){
      files.push({
        documentid: documentid,
        status: 0
      });
    }
  }
  $('#Doc_'+documentid).remove();
}
function foundData(){
    var cont = 0;
    var length = 0;
    var status;
    $('#tablefb tr').each(function(i){
        if (i != 0) {
            status = $(this).data('status');
            length++;
            if (status == 0) {
                cont++;
            }
            return length;
            return cont;
        }
    });
    if (cont == length) {
        $('#tablefb').addClass('hide');
        $('#foundfb').css({"display":"flex"});
    }
}
function deletebystatustd(id){
    tande = [];
    var status;
    var tdid;
    getArrayFB();
    $('.tande-box .result-table .table tr').each(function(i){
        status = $(this).data('status');
        tdid = $(this).data('tdid');

        if(i != 0){
            if (status == 1) {
                if (tdid === id) {
                    $(this).data("status",0);
                    $('#rowTd_'+id).hide();
                }
            }
            else if (status == 2){
                if (tdid === id) {
                    $('#'+tdid).remove();
                }
            }
        }
    });
    linestd();
}
//funcion que re-enumera los numeros del renglon que se llama en la funcion que se encarga de eliminar un renglon
function linestd(){
    var cont = 0;
    var c = 0;
    var status;
    var linetd;
    var afterline;
    $('.tande-box .result-table .table tr').each(function(i){
        if (i != 0) {
            status = $(this).data('status');

            if (status != 0) {
                linetd = $(this).data('linetd');
                cont++;
                $(this).find('.linetd').html(cont);
                $(this).attr('data-linetd', cont);
                if (status == 2) {
                    $(this).attr('data-tdid', 'tmptd_' + cont);
                    $(this).attr('id', 'tmptd_' + cont);
                }
            }
            $(this).find('.linetd').attr('class', 'linetd nolinetd_'+cont);
        }
    });
}
function deletebystatusfb(id){
    freightbill = [];
    var status;
    var fbid;
    getArrayFB();
    $('.freightbill-box .result-table .table tr').each(function(i){
        status = $(this).data('status');
        fbid = $(this).data('fbid');

        if(i != 0){
            if (status == 1) {
                if (fbid === id) {
                    $(this).data("status",0);
                    $('#rowFb_'+id).hide();
                }
            }
            else if (status == 2){
                if (fbid === id) {
                    $('#'+fbid).remove();
                }
            }
        }
    });
    linesfb();
    foundData();
}
//funcion que re-enumera los numeros del renglon que se llama en la funcion que se encarga de eliminar un renglon
function linesfb(){
    var cont = 0;
    var status;
    var linefb;
    $('.freightbill-box .result-table .table tr').each(function(i){
        if (i != 0) {
            status = $(this).data('status');
            linefb = $(this).data('linefb');
            if (status != 0) {
                cont++;
                $(this).find('.linefb').html(cont);
                $(this).attr('data-linefb', cont);
                if (status == 2) {
                    $(this).attr('data-fbid', 'tmpfb_' + cont);
                    $(this).attr('id', 'tmpfb_' + cont);
                }
            }
        }
    });
}
function deletebystatusdetails(id){
    var status;
    var receivinglinesid;
    var change;
    var indexarray;

    $('.viewdetails-box .table tr').each(function(i){
        if (i != 0) {
            status = $(this).data('status');
            change == $(this).attr('data-change');

            if (status == 1) {
                detailid = parseInt($(this).attr('data-rlid'));
                if (detailid == id) {
                    $(this).data('status', 0);
                    $('#rowDl_' + id).hide();
                    details.push({
                        receivinglinesid: detailid,
                        status: 0
                    })
                }
            }
            else if (status == 2) {
                detailid = $(this).attr('id');
                if (detailid == id) {
                    indexarray = $('#'+detailid).attr('data-indexarray');
                    $('#' + detailid).remove();
                    delete details[indexarray];
                    details = details.filter(function(e){ return e === 0 || e });
                }
            }
        }
    });
    linesdetail();

}
//Funcion que renumera los numeros de linea cuando se elimina un receiving line
function linesdetail(){
    var cont = 0;
    var contp = 0;
    var status;
    var linedetail;
    var existpart;
    var countpart = 0;
    $('.viewdetails-box .table tr').each(function(i){
        if (i != 0) {
            status = $(this).data('status');

            if (status != 0) {
                linedetail = $(this).data('linedetail');
                rdline = $(this).attr('data-rdline');
                cont++;
                $(this).find('.linedetail').html(cont);
                $(this).attr('data-linedetail', cont);
                if (status == 1) {
                    existpart = $(this).attr('data-havepart');
                    if (existpart == 1) {
                        countpart++;
                    }
                }
                if (status == 2) {
                    existpart = $(this).attr('data-havepart');
                    if (existpart == 1) {
                        countpart++;
                    }
                }
            }
        }
    });
    var row = $('.btn-details').attr('data-idline');
    var status = $('#'+row).attr('data-status');

    if (countpart == 0) {
        if (status == 1) {
            var data = $('#'+row).find('.rdtracking input').val().length;
            if (data == 0) {
                $('#'+row).find('.rdtracking').attr('data-required', 1);
                $('#'+row).find('.rdtracking input').addClass('input-error')
            }
        }
        if (status == 2) {
            var data = $('#'+row).find('.tmp-rdtracking input').val().length;
            $('#'+row).find('.tmp-rdtracking').attr('data-asignedpart', 0);
            if (data == 0) {
                $('#'+row).find('.tmp-rdtracking').attr('data-required', 1);
                $('#'+row).find('.tmp-rdtracking input').addClass('input-error')
            }
        }
    }
}
function deletebystatuspo(id){
    purchaseOrder = [];
    var status;
    var poid;
    getArrayPO();
    $('.purchase-order-box .result-table .table tr').each(function(i){
        status = $(this).data('status');
        poid = $(this).data('poid');

        if(i != 0){
            if (status == 1) {
                if (poid === id) {
                    $(this).data("status",0);
                    $('#rowPo_'+id).hide();
                }
            }
            else if (status == 2){
                if (poid === id) {
                    $('#'+poid).remove();
                }
            }
        }
    });
    linespo();
}
function linespo(){
    var cont = 0;
    var status;
    var linepo;
    $('.purchase-order-box .result-table .table tr').each(function(i){
        if (i != 0) {
            status = $(this).data('status');

            if (status != 0) {
                linepo = $(this).data('linepo');
                cont++;
                $(this).find('.linepo').html(cont);
                $(this).attr('data-linepo', cont);
                if (status == 2) {
                    $(this).attr('data-poid', 'tmppo_' + cont);
                    $(this).attr('id', 'tmppo_' + cont);
                }
            }
        }
    });
}
function deletebystatusrd(id){
    receivingDetail = [];
    var status;
    var receivingdetailid;
    getArrayRD();
    $('.receipt-details-box .result-table .table tr').each(function(i){
        status = $(this).data('status');
        receivingdetailid = $(this).data('rdid');

        if(i != 0){
            if (status == 1) {
                if (receivingdetailid === id) {
                    $(this).data('status', 0);
                    $('#rowRd_'+id).hide();
                }
            }
            else if (status == 2){
                if (receivingdetailid === id) {
                    $('#'+receivingdetailid).remove();
                }
            }
        }
    });
    linesrd();
}
//Se encarga de re enumerar los renglones de Receiving Details y tambien en Purchase Order
function linesrd(){
    var status;
    var noline;
    var afterline;
    var cont = 0;
    var idc;
    var val;
    var sectionbox;
    $('.receipt-details-box .result-table .table tr').each(function(i){
        status = $(this).data('status');
        noline = $(this).data('noline');
        afterline = $(this).data('afterline');
        var h = $('.selectpo_'+afterline);
        var j = h.data('selected');

        if(i != 0){
            //No se tomaran en cuenta los receiving eliminados en el re enumeramiento
            if (status != 0) {
                cont ++;
                //re enumera las columnas y los atributos data
                $(this).find('.noRowRD').html(cont);
                $(this).attr('data-noline', cont);
                $(this).attr('data-rowrd', cont);
                //Re enumera los Select de Purchase Orders
                $('.selectpo_'+afterline).attr('data-selected', cont);
                $('.selectpo_'+afterline).attr('data-value', cont);
                $('.selectpo_'+afterline).parent().parent().attr('data-srdline', cont);
                changes();
                if (status == 2) {
                    $(this).attr('data-rdid', 'rd_' + cont);
                    $(this).attr('id', 'rd_' + cont);
                }
            }
            //Si el status es 0 (eliminado) se coloca value 0 en el Select de Purchase Order que hace referencia al Receiving Line y su atributo data-ch cambia a 1 para que se detecte que hubo un cambio.
            if (status == 0) {
                $('.selectpo_'+afterline).attr('data-ch', 1);
                $('.selectpo_'+afterline).attr('data-selected', 0);
                $('.selectpo_'+afterline).val(0);
                $('.selectpo_'+afterline).parent().parent().attr('data-srdline', 0);
            }
        }
    });
    //Se comprueba si hay cambios en los select de los purchases orders
    $('.purchase-order-box .result-table .table tr').map(function(i){
        if (i != 0) {
            //Se obtiene el valor actual seleccionado
            var nolinepo = $(this).find('.selectRD').attr('data-selected');
            //Se obtiene el valor anterior
            var afterlinepo = $(this).find('.selectRD').attr('data-afterline');
            //Se comparan, si son diferentes el atributo data-ch cambia de 0 a 1: Que significa que hubo un cambio
            if(nolinepo != afterlinepo){
                $(this).find('td select').attr('data-ch', 1);
            }
            //Se obtiene el id, la columna actual y su clase para enviarlo a la funcion changes
            idc = $(this).find('.selectRD').attr('id');
            val = $('#'+idc).parent().parent();
            sectionbox = val.attr('class');

            changes(idc, val, sectionbox);
        }
    });
    getReceivingRows();
    getPoRD();
}
function getItems(numberPage){
    var tableInvoice         =               $('.table')
    var nextPageElement      =                $('.next')
    var resultPagination     =        $('.table-search')
    var numberPageElement    = $('[data-number-page=0]')
    var numberPagesElement   =       $('[data-pages=0]')
    var previousPageElement  =            $('.previous')
    var numberEntriesElement =     $('[data-entries=0]')
    openloadingcontent()
    tableInvoice.empty()
    $.ajax({
        type: "POST",
        dataType: "json",
        data: searchFilter,
        url: "queries/searchCustomers.php",
        succes: data=>{
            switch (data.code) {
                case 200:
                    if (result.status == 'Success') {
                        //Se llena el arreglo notIn con los ids de los clientes no configurados
                        if (page == 1 && result.notIn.length > 0) {
                            for (var i = 0; result.notIn.length > i; i++) {
                                notIn.push(result.notIn[i].customerid);
                            }
                        }
                        var customerStatus = "";
                        var filas = "";

                        for (var i = 0; result.items.length > i; i++) {
                            if (type == "all" || result.isCustomer == 1) {
                                if(jQuery.inArray(result.items[i].customerid, notIn) == -1) {
                                    customerStatus = "Configured";
                                }
                                else {
                                    customerStatus = "Pending";
                                }
                            }
                            else if (type == "pending")
                                customerStatus = "Pending";
                            else if (type == "configured")
                                customerStatus = "Configured";

                            filas += "<tr data-idCustomer=" + result.items[i].customerid + ">" +
                                    "<td align='center' class='admin'>" +
                                    "<img src='images/icons/edit.svg' width='16' style='cursor: pointer;' + \n\
                                     height='16' alt='Edit' onclick='Recargar("+result.items[i].customerid+");'>" +
                                    "</td>" +
                                    "<td>" + result.items[i].custno + "</td>" +
                                    "<td>" + result.items[i].name + "</td>" +
                                    "<td>" + result.items[i].taxid + "</td>" +
                                    "<td>" + customerStatus + "</td>";
                        }
                        totalRows = result.totalRows;
                        if (type == "pending")
                            totalRows = result.notIn.length;
                        else if (type == "configured")
                            totalRows = totalRows - result.notIn.length;
                        $('#tbody').html("");
                        $('#tbody').html(filas);
                        $('#total').text(totalRows);
                        numPages = Math.ceil(totalRows / 10);
                        $('#pageslbl').text(numPages);
                        $('#pagelbl').text(page);
                        if (page == numPages){
                            $('#btnNext').attr('disabled', true)
                                                    .removeClass('button_link')
                                                    .addClass(' _disable');
                            $('#btnLast').attr('disabled', true)
                                                    .removeClass('button_link')
                                                    .addClass('button_disable');
                        }
                        else{
                            $('#btnNext').attr('disabled', false)
                                                    .addClass('button_link')
                                                    .removeClass('button_disable');
                            $('#btnLast').attr('disabled', false)
                                                    .addClass('button_link')
                                                    .removeClass('button_disable');
                        }
                        if (page == 1)
                            $('#btnPrevious').attr('disabled', true)
                                                    .removeClass('button_link')
                                                    .addClass('button_disable');
                        else
                            $('#btnPrevious').attr('disabled', false)
                                                    .addClass('button_link')
                                                    .removeClass('button_disable');
                    }
                    break;
                case 404:
                    contentNotFound.css({"display":"flex"});
                    resultPagination.addClass("hide");
                    break;
                default:
            }
        }
    }).done(()=>{

    })
}

function putItems(items, first = false){
    if(first){
        return`
        <tr>
            <td>Config</td>
            <td>Customer No.</td>
            <td>Name</td>
            <td>Tax ID</td>
            <td>Status</td>
        </tr>
        `;
    }
    return `
    <tr data-idCustomer=" ${itemsresult.items[i].customerid}>
        <td align='center' class='admin'> +
            <img src='images/icons/edit.svg' onclick='Recargar("+result.items[i].customerid+");'>" +
        </td>
        <td> ${result.items[i].custno}</td>
        <td> ${result.items[i].name}</td>
        <td> ${result.items[i].taxid}</td>
        <td> ${items.customerStatus} </td>
    `;
}

function writeFb(){
    var irfb = $('.add-fb input').val();
    var irfbp = parseInt(irfb) + 1;
    var cont = 1;
    var headT = $('.content-table-newfb .table');
    $('.insert-fb').focus();
    headT.append(`
        <tr>
            <td>No</td>
            <td>Delete</td>
            <td>Freightbill</td>
        </tr>`);
    while (cont < irfbp) {
        headT.append(`
            <tr data-insert="1">
                <td>${cont}</td>
                <td>
                    <img src="images/icons/delete.svg" class="delete-tmp-fb">
                </td>
                <td class="tmp-data-namefb">
                    <input type="text" value="" maxlength="50" placeholder="Freightbill" placeholder="Please fill"/>
                </td>
            </tr>`);
        cont++;
    }
}
function writeTd(){
    var irtd = $('.add-td input').val();
    var irtdp = parseInt(irtd) + 1;
    var cont = 1;
    var headT = $('.content-table-newtd .table');
    $('.insert-td').focus();
    headT.append(`
        <tr>
            <td>No</td>
            <td>Delete</td>
            <td>T and E</td>
            <td>T and E Created</td>
        </tr>`);
    while (cont < irtdp) {
        headT.append(`
            <tr data-insert="1">
                <td>${cont}</td>
                <td>
                    <img src="images/icons/delete.svg" class="delete-tmp-td">
                </td>
                <td class="tmp-data-nametd">
                    <input type="text" value="" maxlength="50" placeholder="T and E" placeholder="Please fill"/>
                </td>
                <td class="tmp-data-tddate">
                    <input type="text" class="datepicker" value="" placeholder="Date created" placeholder="Please fill"/>
                </td>
            </tr>`);
        cont++;
    }
}
function writePo(){
    var irpo = $('.box-add-row input').val();
    var irpop = parseInt(irpo) + 1;
    var cont = 1;
    var headT = $('.content-table-newpo .table');
    $('.insert-po').focus();
    headT.append(`
        <tr>
            <td>No</td>
            <td>Delete</td>
            <td>Receiving Detail Row</td>
            <td>Purchase Order</td>
            <td>PO Date</td>
        </tr>`);
    while (cont < irpop) {
        headT.append(`
            <tr data-insert="1">
                <td>${cont}</td>
                <td>
                    <img src="images/icons/delete.svg" class="delete-tmp-po">
                </td>
                <td class="data-selected">
                    <select class="selectRDnewPO">
                        <option value="0">None</option>
                    </select>
                </td>
                <td class="data-namepo">
                    <input type="text" value="" maxlength="50" placeholder="Purchase order"/>
                </td>
                <td class="data-podate">
                    <input type="text" class="datepicker" value="" placeholder="Date"/>
                </td>
            </tr>`);
        cont++;
    }
}
function writeRD(){
    var irrd = $('.box-add-rd input').val();
    var irrds = parseInt(irrd) + 1;
    var cont = 1;
    var headTrd = $('.content-table-newrd .table');
    var receivingid = $('main').data("receiving-id");
    $('.insert-rd').focus();
    headTrd.append(`
        <tr>
            <td>No.</td>
            <td>Delete</td>
            <td>Tracking Number</td>
            <td class="default-column">Part No.</td>
            <td class="default-column">Description</td>
            <td class="default-column">Received Qty Part</td>
            <td class="default-column">Origen</td>
            <td>Received Package Qty</td>
            <td>Package Type</td>
            <td>Total Weight</td>
            <td>Unit Mesurament</td>
            <td hidden>Location</td>
            <td class="default-column">Lot Number</td>
            <td class="pickandpack-column">PO</td>
            <td class="pickandpack-column">PO Date</td>
        </tr>`);
    while (cont < irrds) {
        headTrd.append(`
            <tr data-receivingid="${receivingid}" data-insert="1" data-id="rdnewtmp_${cont}" id="rdnewtmp_${cont}">
                <td>${cont}</td>
                <td>
                    <img src="images/icons/delete.svg" class="delete-tmp-rd">
                </td>
                <td class="data-tracking">
                    <input type="text" value="" maxlength="20" placeholder="Tracking number" required placeholder="Please fill"/>
                </td>
                <td class="data-part default-column" data-value>
                    <input type="text" class="partrd-insert" value="" maxlength="20" placeholder="Part No." required placeholder="Please fill" hidden/>
                    <input type="button" class="button part-receivingdetail" value="Add Part"/>
                </td>
                <td class="data-description default-column" data-value>
                    <input type="text" value="" maxlength="20" placeholder="Description" required placeholder="Please fill"/>
                </td>
                <td class="data-qrtypart default-column">
                    <input type="number" value="" maxlength="20" placeholder="Received Qty Part" required placeholder="Please fill"/>
                </td>
                <td class="data-origen default-column">
                    <input type="text" value="" maxlength="20" placeholder="Origen" required placeholder="Please fill"/>
                </td>
                <td class="data-quantity">
                    <input type="number" min="0" value="" maxlength="15" placeholder="Package Qty" required placeholder="Please fill"/>
                </td>
                <td class="data-package">
                    <select class="selectPT">
                    </select>
                </td>
                <td class="data-weight">
                    <input type="number" min="0" value="" maxlength="15" placeholder="Weight" style="width: 50px;" required placeholder="Please fill"/>
                </td>
                <td class="data-um">
                    <select class="selectUM">
                    </select>
                </td>
                <td class="data-location" hidden>
                    <select class="selectLO">
                    </select>
                </td>
                <td class="data-lotnumber default-column">
                    <input type="text" value="" maxlength="20" placeholder="Lot Number" required placeholder="Please fill"/>
                </td>
                <td class="data-namepo pickandpack-column">
                    <input type="text" value="" maxlength="15" placeholder="Purchase order" required placeholder="Please fill"/>
                </td>
                <td class="data-podate pickandpack-column">
                    <input type="text" value="" maxlength="15" class="datepicker" placeholder="Po Date" required placeholder="Please fill"/>
                </td>
            </tr>`);
        cont++;
    }
}
/*********************************************************************************************************
****************************************** Print Zebra*******************************************
**********************************************************************************************************/
function printBar(tipo) {
    var testingLabels = 0;
    if (tipo == 'remote')
        testingLabels = 1;

    var customer = $("#customerid option:selected").text();
    var vendor = $("#vendorid option:selected").text();
    var carrier = $("#carrierid option:selected").text();
    if ($('#vendornew').val() != "")
        vendor = $('#vendornew').val();
    if ($('#carriernew').val() != "")
        carrier = $('#carriernew').val();
    var internal = $('#freightbill').val();
    var priority = $("#priority option:selected").text();
    var bound = $('#South').val()
    if ($('#North').is(':checked'))
        var bound = $('#North').val();
    var state = '';
    if ($('#hazmat').is(':checked'))
        state += '- Hazmat    ';
    if ($('#damaged').is(':checked'))
        state += '- Damaged';
    var printer = $('#selectP').val();

    $.ajax({
    type: "POST",
    dataType: "json",
    data: {
        code: $('#recZebra').attr('data-recZebra'),
            cust: customer,
            vend: vendor,
            carr: carrier,
            irec: internal,
            prior: priority,
            bound: bound,
            state: state,
            prin: printer,
            TestingLabels:testingLabels,
            quantity:parseInt($('#noCodes').val())
    },
    async: false,
    url: "print_zebra_rec.php"
    }).done(function(response) {
        if (response.status == 'Success')
            location.href = "printlabel: " + response.addressLink;
    });
}
/*********************************************************************************************************
****************************************** Write Receiving Data*******************************************
**********************************************************************************************************/
function getReceiving(){
    var receivingid = $('main').data("receiving-id");

    if (receivingid != '') {
        var tableReceiving = $('.receiving-data-box');
        tableReceiving.empty();
        $.ajax({
            url: `/ISS/application/index.php/receiving/get`,
            type: "GET",
            dataType: 'json',
            data: {
                receivingId: receivingid
            },
            success: data=>{
                switch (data.code) {
                    case 200:
                    var receiving = data.response.receiving;
                    tableReceiving.html(`
                        <div>
                            <p>Rec. #</p>
                            <p id="recZebra" data-recZebra="${receiving.receivingno}">${receiving.receivingno}</p>
                        </div>
                        <div class="itemdata-receiving">
                            <p>Internal # Rec.</p>
                            <input id="freightbill" data-status="1" type="text" class="inputdatareceiving" value="${receiving.freightbill}" maxlength="20" size="20" data-ch="0" data-value="${receiving.freightbill}"/>
                        </div>
                        <div class="itemdata-receiving">
                            <p>Customer</p>
                            <select id="customerid" data-status="1" data-ch="0" data-value="${receiving.customerid}" data-select="${receiving.customerid}"  data-owninventory="" class="inputdata selectCustomer inputdatareceiving">
                            <option selected="selected" value="select0">Please Select</option>
                            </select>
                        </div>
                        <div class="vendor-box">
                            <p>Vendor</p>
                            <div class="select-vendor-box">
                                <select id="vendorid" data-active="1" data-status="1" data-ch="0" data-value="${receiving.vendorid}" class="inputdata selectVendor inputdatareceiving">
                                    <option selected="selected" value="select0">Please Select</option>
                                </select>
                                <input type="text" data-active="0" data-status="2" placeholder="New vendor" class="inputdatareceiving" data-value="0" id="vendornew" value="" size="20" max-length="60" hidden/>
                            </div>
                            <input type="button" class="button newvendor" value="New Vendor">
                            <input type="button" class="button selectvendor" value="Select exist vendor" hidden/>
                        </div>
                        <div class="carrier-box">
                            <p>Carrier</p>
                            <div class="select-carrier-box">
                                <select id="carrierid" data-active="1" data-status="1" data-ch="0" data-value="${receiving.carrierid}" class="inputdata selectCarrier inputdatareceiving">
                                    <option selected="selected" value="select0">Please Select</option>
                                </select>
                                <input type="text" data-active="0" data-status="2" placeholder="New carrier" class="inputdatareceiving" data-value="0" id="carriernew" value="" size="20" max-length="60" hidden/>
                            </div>
                            <input type="button" class="button newcarrier" value="New Carrier">
                            <input type="button" class="button selectcarrier" value="Select exist carrier" hidden/>
                        </div>
                        <div class="itemdata-receiving">
                            <p>Priority</p>
                            <select id="priority" data-status="1" class="inputdatareceiving selectPriority" data-value="${receiving.priority}" data-ch="0"></select>
                        </div>
                        <div>
                            <p>Date</p>
                            <p>${new Date(receiving.createddate).toString('ddd MMMM dd, yyyy HH:mm')}</p>
                        </div>
                        `);
                        break;
                        case 404:

                        break;
                        default:
                    }
                }
            }).done(()=>{
                getCustomers();
                getVendors();
                getCarriers();
                optionPriority();
                getCustomServices();
                getExtraCargos();
                var internal = $('#freightbill').val()
                if (internal == 'null' || internal == '' || internal == undefined || internal == 'none') {
                    $('#freightbill').val('');
                }
            });
    }else {
        $('.barcode').remove();
        $('.print-options').remove();
        getCustomers();
        getVendors();
        getCarriers();
        optionPriority();
    }
}
/*********************************************************************************************************
****************************************** Write options priority ****************************************
**********************************************************************************************************/
function selectPriority(){
    var receivingid = $('main').data("receiving-id");
    if (receivingid != '') {
        var selectPr = $('.selectPriority');
        var actualOption = selectPr.data('value');
        selectPr.val(actualOption)
    }
}
function optionPriority(){
    var selectPr = $('.selectPriority');
    selectPr.append(`
        <option value="3">Low</option>
        <option value="2">Medium</option>
        <option value="1">High</option>
    `);
    selectPriority();
}
/*********************************************************************************************************
****************************************** Get Customers**************************************************
**********************************************************************************************************/
//Selecciona el cliente
function selectCustomer(){
    var receivingid = $('main').data("receiving-id");
    if (receivingid != '') {
        var selectC = $('.selectCustomer');
        var actualOption = selectC.data('value');
        selectC.val(actualOption);
        var own = $('.selectCustomer option:selected').attr('data-owninventory');
        $('.selectCustomer').attr('data-owninventory', own);
    }
}
//Escribe los clientes
function optionCustomer(c){
    var optionsCustomer = `<option value="${c.customerid}" data-mode="${c.receiptMode}" data-value="${c.name}" data-owninventory="${c.ownInventory}" title="${c.name}">${c.name}</option>`;
    return optionsCustomer;
}
//Obtiene los clientes con el metodo ajax y los itera en la funcion optioncustomer
function getCustomers(){
    var selectC = $('.selectCustomer');
    $.ajax({
        url: `/ISS/application/index.php/customer/get`,
        type: "GET",
        dataType: 'json',
        success: data=>{
            switch (data.code) {
                case 200:
                    data.response.customers.forEach((c)=>{
                        selectC.append(optionCustomer(c));
                    });
                    break;
                case 404:

                    break;
                default:
            }
        }
    }).done(()=>{
        selectCustomer();
        var receiptmode = $('#customerid option:selected').attr('data-mode');
        $('#customerid').attr('data-mode', receiptmode);
        receiptMode(receiptmode)
        getRd();
    });
}
/*********************************************************************************************************
****************************************** Get vendors**************************************************
**********************************************************************************************************/
//Selecciona el cliente
function selectVendor(){
    var receivingid = $('main').data("receiving-id");
    if (receivingid != '') {
        var selectV = $('.selectVendor');
        var actualOption = selectV.data('value');
        selectV.val(actualOption)
    }
}
//Escribe los clientes
function optionVendor(v){
    var optionsCustomer = `<option value="${v.vendorid}" data-value="${v.name}">${v.name}</option>`;
    return optionsCustomer;
}
//Obtiene los clientes con el metodo ajax y los itera en la funcion optioncustomer
function getVendors(){
    var selectV = $('.selectVendor');
    $.ajax({
        url: `/ISS/application/index.php/vendor/get`,
        type: "GET",
        dataType: 'json',
        success: data=>{
            switch (data.code) {
                case 200:
                    data.response.vendors.forEach((v)=>{
                        selectV.append(optionVendor(v));
                    });
                    break;
                case 404:

                    break;
                default:
            }
        }
    }).done(()=>{
        selectVendor();
    });
}
/*********************************************************************************************************
****************************************** Get carriers**************************************************
**********************************************************************************************************/
//Selecciona el transportista
function selectCarrier(){
    var receivingid = $('main').data("receiving-id");
    if (receivingid != '') {
        var selectCa = $('.selectCarrier');
        var actualOption = selectCa.data('value');
        selectCa.val(actualOption)
    }
}
//Escribe los transportistas
function optionCarrier(ca){
    var optionsCarrier = `<option value="${ca.carrierid}" data-value="${ca.name}">${ca.name}</option>`;
    return optionsCarrier;
}
//Obtiene los clientes con el metodo ajax y los itera en la funcion optioncustomer
function getCarriers(){
    var selectCa = $('.selectCarrier');
    $.ajax({
        url: `/ISS/application/index.php/carrier/get`,
        type: "GET",
        dataType: 'json',
        success: data=>{
            switch (data.code) {
                case 200:
                    data.response.carriers.forEach((ca)=>{
                        selectCa.append(optionCarrier(ca));
                    });
                    break;
                case 404:

                    break;
                default:
            }
        }
    }).done(()=>{
        selectCarrier();
    });
}
function getReceivingRowsnewPO(){
    var selectRDnewPO = $('.selectRDnewPO');
    var array = [];
    var status;
    $('.receipt-details-box .result-table .table tr').map(function(i){
        status = $(this).data('status');
        if(i > 0){
            if (status != 0) {
                array = {
                    rowrd: $(this).data('rowrd'),
                    // id: $(this).data('rdid')
                    noline: $(this).data('noline')
                }
                selectRDnewPO.append(`<option value="${array.noline}">${array.rowrd}</option>`);
            }

        }
    });
}
function putReceivinLines(detail, index, first=false){
    if(first){
        return `
        <tr data-table-column=0>
            <td style="width: 25px;">No.</td>
            <td>Delete</td>
            <td>Edit</td>
            <td>Part #</td>
            <td>Description</td>
            <td>Package Qty</td>
            <td class="item-minimize display-none">Package Type</td>
            <td class="item-minimize display-none">Received Qty Part</td>
            <td class="item-minimize display-none">Lot Number</td>
            <td class="item-minimize display-none">Exp. Date</td>
            <td class="item-minimize display-none">PO</td>
            <td class="item-minimize display-none">PO Date</td>
        </tr>`;
    }
    return`
    <tr data-status=1 data-change="0" data-indexarray data-rlid="${detail.receivinglinesid}" id="rowDl_${detail.receivinglinesid}" data-lotnumber="${detail.lotnumber}" data-linedetail="${index}">
        <td class="linedetail nolinedetail_${index}">${index}</td>
        <td class="icondetails">
            <img src="images/icons/delete.svg" value="${detail.receivinglinesid}" class="deleteDetail btn-action-delete">
        </td>
        <td class="icondetails"><img src="images/icons/edit.svg" class="editDetail btn-action-edit"></td>
        <td class="productname-bd" data-productid="${detail.productid}"data-value="${detail.productno}" data-columnname="Part #">${detail.productno}</td>
        <td class="productdescription-bd" data-value="${detail.description}" data-columnname="Description">${detail.description}</td>
        <td class="originalqty-bd" data-value="${detail.originalQty}">${detail.originalQty}</td>
        <td class="unit-bd item-minimize display-none" data-value="${detail.unitid}">${detail.unitdescription}</td>
        <td class="partqty-bd item-minimize display-none" data-value="${detail.partQty}">${detail.partQty}</td>
        <td class="lotnumber-bd item-minimize display-none" data-value="${detail.lotnumber}">${detail.lotnumber}</td>
        <td class="expdate-bd item-minimize display-none" data-value="${detail.expDate}">${detail.expDate}</td>
        <td class="productpo-bd item-minimize display-none" data-value="${detail.po}">${detail.po}</td>
        <td class="productpodate-bd item-minimize display-none" data-value="${detail.podate}">${detail.podate}</td>
    </tr>`;
}
function writeUpdatesDetails(){

}
function getDetails(receivingdetailid, line){
    var tableDetails = $('.viewdetails-box .table');
    tableDetails.empty();
    $.ajax({
        url: `/ISS/application/index.php/receivingdetail/details`,
        type: "GET",
        dataType: 'json',
        data: {
            receivingdetailId: receivingdetailid
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    var lenght = data.response.details.length;

                    if (lenght > 0) {
                        $('.viewdetails-box').css({"display":"block"});
                        $('.viewdetails-box').css({"animation":"pop-in","animation-duration":"0.25s"});
                        $('.addpart-box .title-dialog img').hide();
                        $('.in-xpand').show();
                        tableDetails.append(putReceivinLines({}, 0, true));
                        data.response.details.forEach((detail, index)=>{
                            tableDetails.append(putReceivinLines(detail, index+1));
                        })
                    }
                    for (var i = 0; i < details.length; i++) {
                        if (details[i].status == 0) {
                            $('#rowDl_' + details[i].receivinglinesid).remove();
                            $('#rowDl_' + details[i].receivinglinesid).removeAttr('data-linedetail');
                        }
                        if (details[i].status == 1) {
                            $('#rowDl_' + details[i].receivinglinesid).find('.productname-bd').text(details[i].productname);
                            $('#rowDl_' + details[i].receivinglinesid).find('.productdescription-bd').text(details[i].description);
                            $('#rowDl_' + details[i].receivinglinesid).find('.originalqty-bd').text(details[i].originalQty);
                            $('#rowDl_' + details[i].receivinglinesid).find('.unit-bd').text(details[i].unitPackage);
                            $('#rowDl_' + details[i].receivinglinesid).find('.partqty-bd').text(details[i].partQty);
                            $('#rowDl_' + details[i].receivinglinesid).find('.lotnumber-bd').text(details[i].lotnumber);
                            $('#rowDl_' + details[i].receivinglinesid).find('.expdate-bd').text(details[i].expDate);
                            $('#rowDl_' + details[i].receivinglinesid).find('.productpo-bd').text(details[i].po);
                            $('#rowDl_' + details[i].receivinglinesid).find('.productpodate-bd').text(details[i].podate);
                        }
                    }
                    if (details.length > 0) {
                        for (var i = 0; i < details.length; i++) {
                            if (details[i].status == 2) {
                                if (line == details[i].receivingdetailno) {
                                    var lt = $('.viewdetails-box .table tr');
                                    $('.viewdetails-box').css({"display":"block"});
                                    $('.viewdetails-box').css({"animation":"pop-in","animation-duration":"0.25s"});
                                    if (lt.length == 0) {
                                        tableDetails.append(`
                                            <tr data-table-column=0>
                                                <td style="width: 25px;">No.</td>
                                                <td>Delete</td>
                                                <td>Edit</td>
                                                <td>Part #</td>
                                                <td>Description</td>
                                                <td>Package Qty</td>
                                                <td class="item-minimize display-none">Package Type</td>
                                                <td class="item-minimize display-none">Received Qty Part</td>
                                                <td class="item-minimize display-none">Lot Number</td>
                                                <td class="item-minimize display-none">Exp. Date</td>
                                                <td class="item-minimize display-none">PO</td>
                                                <td class="item-minimize display-none">PO Date</td>
                                            </tr>
                                        `);
                                    }
                                    tableDetails.append(`
                                        <tr data-status="2" data-change="1" data-havepart data-indexarray="${details[i].index}" data-rdline="${details[i].receivingdetailno}" data-rlid="${details[i].receivinglinesid}" id="${details[i].receivinglinesid}" data-lotnumber="${details[i].lotnumber}">
                                            <td class="linedetail">${i + 1}</td>
                                            <td class="icondetails"><img src="images/icons/delete.svg" class="deleteDetail btn-action-delete"></td>
                                            <td class="icondetails"><img src="images/icons/edit.svg" class="editDetail btn-action-edit"></td>
                                            <td class="productname-bd" data-productid="${details[i].productid}" data-productno="${details[i].productname}" data-columnname="Part #">${details[i].productname}</td>
                                            <td class="productdescription-bd" data-productdesc="${details[i].description}" data-columnname="Description">${details[i].description}</td>
                                            <td class="originalqty-bd">${details[i].originalQty}</td>
                                            <td class="unit-bd item-minimize display-none" data-value="${details[i].unitid}">${details[i].unitPackage}</td>
                                            <td class="partqty-bd item-minimize display-none">${details[i].partQty}</td>
                                            <td class="lotnumber-bd item-minimize display-none" data-value="${details[i].lotnumber}">${details[i].lotnumber}</td>
                                            <td class="expdate-bd item-minimize display-none">${details[i].expDate}</td>
                                            <td class="productpo-bd item-minimize display-none">${details[i].po}</td>
                                            <td class="productpodate-bd item-minimize display-none" data-value="${details[i].podate}">${details[i].podate}</td>
                                        </tr>
                                        `);
                                    }
                            }
                        }
                    }
                    linesdetails();
                    break;
                case 404:

                    break;
                default:
            }
        }
    }).done(()=>{
        $('.viewdetails-box .table tr').each(function(i){
            var contentpart = $(this).find('.productname-bd').text().length;
            if (contentpart > 0) {$(this).attr('data-havepart', 1);}
            else {$(this).attr('data-havepart', 0);}
        });
    });
}
function linesdetails(){
    $('.viewdetails-box table tr').each(function(i){
        if (i != 0) {
            var status = $(this).attr('data-status');
            if (status != 0) {
                $(this).attr('data-linedetail', i);
                $(this).find('.linedetail').text(i);
            }
            $('.viewdetails-box table tr td').each(function(i){
                var data = $(this).text();
                var classs = $(this).attr('class');
                if (classs != 'icondetails') {
                    if (data == "null" || data == "none" || data == "0000-00-00" || data == "" || data == "NaN" || data == undefined) {
                        $(this).text('');
                    }
                }
            });

        }
    });
}
function getFb(){
    var receivingid = $('main').data("receiving-id");
    var tableFb = $('.freightbill-box > .result-table > .table');
    tableFb.empty();

    $.ajax({
        url: `/ISS/application/index.php/freightbill/get`,
        type: "GET",
        dataType: 'json',
        data: {
            receivingId: receivingid
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    var rowno = data.response.freightbill.length;
                    if (rowno == 0) {
                        $('#tablefb').addClass('hide');
                        $('#foundfb').css({"display":"flex"});
                    }
                    else {
                        $('#tablefb').removeClass('hide');
                        $('#foundfb').css({"display":"none"});
                        tableFb.append(putfreightbill({}, 0, true));
                        data.response.freightbill.forEach((freightbill, index)=>{
                            tableFb.append(putfreightbill(freightbill, index+1));
                        })
                    }
                    break;
                case 404:
                    // notFoundContent();
                    break;
                default:
            }
        }
    }).done(()=>{
        loadingfreightbills();
        // getPoRD();
        // getReceivingRows();
    });
}
function putfreightbill(freightbill, index, first=false){
    if(first){
        return `
        <tr data-table-column=0>
            <td>No.</td>
            <td>Delete</td>
            <td>Freightbill</td>
        </tr>`;
    }
    return`
    <tr data-status=1 data-change=0 class="itemfb" data-fbid="${freightbill.frid}" id="rowFb_${freightbill.frid}" data-nopo="${freightbill.fr}" data-linefb="${index}">
        <td class="linefb nolinefb_${index}">${index}</td>
        <td>
            <img src="images/icons/delete.svg" value="${freightbill.frid}" class="deleteFb btn-action-delete">
        </td>
        <td class="data-namefb" data-value="${freightbill.fr}" data-columnname="Freightbill">
            <input data-ch="0" data-namefb=${freightbill.fr} type="text" value="${freightbill.fr}" maxlength="50" size="35" id="fb_${freightbill.frid}" class="inputdata fb-name" required placeholder="Please fill"/>
        </td>
    </tr>`;
}
function getTd(){
    var receivingid = $('main').data("receiving-id");
    var tableTd = $('.tande-box > .result-table > .table');
    tableTd.empty();

    $.ajax({
        url: `/ISS/application/index.php/tande/get`,
        type: "GET",
        dataType: 'json',
        data: {
            receivingId: receivingid
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    var rowno = data.response.tande.length;
                    if (rowno == 0) {
                        $('#tabletd').addClass('hide');
                        $('#foundtd').css({"display":"flex"});
                    }
                    else {
                        $('#tabletd').removeClass('hide');
                        $('#foundtd').css({"display":"none"});
                        tableTd.append(puttande({}, 0, true));
                        data.response.tande.forEach((tande, index)=>{
                            tableTd.append(puttande(tande, index+1));
                        })
                    }
                    break;
                case 404:
                    // notFoundContent();
                    break;
                default:
            }
        }
    }).done(()=>{
        loadingtande();
        // tandeExpires();
    });
}
function puttande(tande,index, first=false){
    if(first){
        return `
        <tr data-table-column=0>
            <td>No.</td>
            <td>Delete</td>
            <td>T and E</td>
            <td>T and E Created</td>
            <td>T and E Expires</td>
        </tr>`;
    }
    return`
    <tr data-status=1 data-change=0 class="itemtd" data-tdid="${tande.tandeid}" id="rowTd_${tande.tandeid}" data-notd="${tande.tande}" data-linetd="${index}">
        <td class="linetd nolinetd_${index}">${index}</td>
        <td>
            <img src="images/icons/delete.svg" value="${tande.tandeid}" class="deleteTd btn-action-delete">
        </td>
        <td class="data-nametd" data-value="${tande.tande}" data-columnname="T and E">
            <input data-ch="0" data-nametd=${tande.tande} type="text" value="${tande.tande}" maxlength="50" size="35" id="td_${tande.tandeid}" class="inputdata td-name" placeholder="Please fill"/>
        </td>
        <td class="data-datetd" data-value="${tande.tandeexpires}" data-columnname="T and E Created">
            <input data-ch="0" type="text" class="inputdata datepicker td-date" value="${tande.tandeexpires}" placeholder="Please fill"/>
        </td>
        <td class="data-dateexpirestd" data-value="${new Date(tande.tandeexpires).addDays(15).toString('yyyy-MM-dd')}">
            <input data-ch="0" type="text" class="inputdata datepicker td-date" value="${new Date(tande.tandeexpires).addDays(15).toString('yyyy-MM-dd')}" disabled/>
        </td>
    </tr>`;
}
function getPo(){
    var receivingid = $('main').data("receiving-id");
    var tablePo = $('.purchase-order-box > .result-table > .table');
    tablePo.empty();

    $.ajax({
        url: `/ISS/application/index.php/purchaseorder/get`,
        type: "GET",
        dataType: 'json',
        data: {
            receivingId: receivingid
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    var rowno = data.response.purchaseOrders.length;
                    if (rowno == 0) {
                        $('#tablepo').addClass('hide');
                        $('#foundpo').css({"display":"flex"});
                    }
                    else {
                        $('#tablepo').removeClass('hide');
                        $('#foundpo').css({"display":"none"});
                        tablePo.append(putPurchaseOrder({}, 0, true));
                        data.response.purchaseOrders.forEach((purchaseorder, index)=>{
                            tablePo.append(putPurchaseOrder(purchaseorder, index+1));
                        })
                    }
                    break;
                case 404:
                    // notFoundContent();
                    break;
                default:
            }
        },
        error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{
        loadingpurchasesorders();
        // getPoRD();
        // getReceivingRows();
    });
}
function putPurchaseOrder(purchaseorder, index, first=false){
    if(first){
        return `
        <tr data-table-column=0>
            <td>No.</td>
            <td>Delete</td>
            <td>Receiving Line No.</td>
            <td>Purchase Order</td>
            <td>PO Date</td>
        </tr>`;
    }
    return`
    <tr data-status=1 data-change=0 class="itempo" data-poid=${purchaseorder.poid} id="rowPo_${purchaseorder.poid}" data-nopo=${purchaseorder.nopo} data-srd="${purchaseorder.receivingdetailid}" data-srdline="${purchaseorder.rowno}" data-linepo="${index}" data-afterline="${index}">
        <td class="linepo nolinepo_${index}">${index}</td>
        <td>
            <img src="images/icons/delete.svg" value="${purchaseorder.poid}" name="poid[]"  class="deletePo btn-action-delete">
        </td>
        <td class="purchaserow" id="select_${purchaseorder.poid}" data-value="${purchaseorder.no}" data-columnname="Receiving Line No.">
            <select data-s="1" data-ch="0" id="rd_${purchaseorder.poid}" name="poReceivingdetailid_${purchaseorder.poid}" data-selected="${purchaseorder.rowno}" data-afterline="${purchaseorder.rowno}" class="inputdata selectRD rdid_${purchaseorder.receivingdetailid} selectpo_${purchaseorder.rowno}"></select>
        </td>
        <td class="data-namepo" data-value="${purchaseorder.po}" data-columnname="Purchase Order">
            <input data-ch="0" data-namepo=${purchaseorder.po} type="text" value="${purchaseorder.po}" maxlength="50" size="35" id="po_${purchaseorder.poid}" class="inputdata po-name" placeholder="Please fill"/>
        </td>
        <td class="data-datepo" data-value="${purchaseorder.poDate}" data-columnname="PO Date">
            <input data-ch="0" type="text" class="inputdata datepicker po-date" value="${purchaseorder.poDate}" placeholder="Please fill"/>
        </td>
    </tr>`;
}
function getDocuments(){
    var receivingid = $('main').data("receiving-id");
    var tableDocuments = $('#documentList');
    tableDocuments.empty();
    $.ajax({
        url: `/ISS/application/index.php/document/receiving/get`,
        type: "GET",
        dataType: 'json',
        data: {
            receivingId: receivingid
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    tableDocuments.append(putDocument({}, 0, true));
                    data.response.documents.forEach((document)=>{
                        tableDocuments.append(putDocument(document, -1));
                    })
                    break;
                case 404:
                    // notFoundContent();
                    break;
                default:
            }
        },
        error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{
        loadingpurchasesorders();
        // getPoRD();
        // getReceivingRows();
    });
}
function putDocument(document, index, first=false){
    if(first){
        return `
        <tr>
            <td>View</td>
            <td>Delete</td>
            <td>File</td>
            <td>Created</td>
        </tr>`;
    }
    return `
    <tr data-documentid=${document.documentid} data-index=${index} id="Doc_${document.documentid}">
        <td>
          <a href="/ISS/application/index.php/document/show?documentId=${document.documentid} " target="upload">
            <img src="/ISS/images/icons/view.svg" width="16" height="16" alt="View">
          </a>
        </td>
        <td>
          <img class="delete-file" src="images/icons/delete${(document.description.localeCompare('EDI') == 0) ? '_disabled' : ''}.svg" alt="Delete">
        </td>
        <td>
          ${(document.description.localeCompare('EDI') == 0) ? 'EDI FILE' : document.original_name}
        </td>
        <td>
          ${document.created}
        </td>
    </tr>`;
}
function getReceivingRows(){
    var selectRD = $('.selectRD');
    selectRD.empty();
    var array = [];
    var status;
    selectRD.append(`<option value="0">None</option>`);
    $('.receipt-details-box .result-table .table tr').map(function(i){
        status = $(this).data('status');
        if(i > 0){
            if (status != 0) {
                array = {
                    rowrd: $(this).attr('data-rowrd'),
                    // id: $(this).data('rdid')
                    noline: $(this).attr('data-noline')
                }
                selectRD.append(`<option value="${array.noline}">${array.rowrd}</option>`);
            }

        }
    });
}
function getPoRD(){
  var poarray = [];
  var array = [];
  var actualOption;
  var status;
  $('.purchase-order-box .result-table .table tr').map(function(i){
      var selectRD = $(this).find('.selectRD');
      actualOption =  selectRD.data('selected');

      if(i > 0){
          poarray = {
              // selectedRdId: $(this).data('srd')
              selectedRdLine: $(this).attr('data-srdline')
          }
        $('.receipt-details-box .result-table .table tr').map(function(i){
            status = $(this).data('status');
            if (i != 0) {
                if (status != 0) {
                    array = {
                        rowrd: $(this).attr('data-rowrd'),
                        // id: $(this).data('rdid')
                        noline: $(this).attr('data-noline')
                    }
                  if(poarray.selectedRdLine == array.noline){
                      selectRD.val(poarray.selectedRdLine);
                  }
                }
            }
        });
      }
  });
}
function writenewrd(newRd){
    var x = newRd.length;
    var nordline = $('.receipt-details-box .result-table .table tr').length;
    var tempcont = 0;
    var tablenewRD = $('.receipt-details-box .result-table .table');
    var mode = $('#customerid option:selected').attr('data-mode');
    $('.receipt-details-box .result-table .table tr').map(function(i){
        if (i != 0) {
            status = $(this).data('status');
            if (status == 0) {
                tempcont++;
            }
        }
    });
    nordline = (nordline - tempcont);
    if (nordline == 0) {
        $('#tablerd').removeClass('hide');
        $('#foundrd').css({"display":"none"});
        nordline = 1;
        if (mode == 1) {
            tablenewRD.append(
                `<tr id="receiptDetailsTitles">
                    <td>No.</td>
                    <td>Delete</td>
                    <td>Tracking Number</td>
                    <td>Received Package Qty</td>
                    <td>Package Type</td>
                    <td>Total Weight</td>
                    <td>Unit Mesurament</td>
                    <td hidden>Location</td>
                    <td>PO</td>
                    <td>PO Date</td>
                    <td>Add Details</td>
                </tr>`
            );
        }
        else if (mode == 0) {
            tablenewRD.append(
                `<tr id="receiptDetailsTitles">
                    <td>No.</td>
                    <td>Delete</td>
                    <td>Tracking Number</td>
                    <td>Part No.</td>
                    <td>Description</td>
                    <td>Received Qty Part</td>
                    <td>Origen</td>
                    <td>Received Package Qty</td>
                    <td>Package Type</td>
                    <td>Total Weight</td>
                    <td>Unit Mesurament</td>
                    <td hidden>Location</td>
                    <td>Lot number</td>
                </tr>`
            );
        }

    }
    if (mode == 1) {
        for (var i = 0; i < x; i++) {
            $('.receipt-details-box .result-table .table').append(`
                <tr data-status="2" data-rowrd="${nordline + i}" data-noline="${nordline + i}" data-rdid="rd_${nordline +i}" id="rd_${nordline +i}" class="data-tracking">
                    <td class="receivingline noline_${nordline + i} noRowRD" data-rdline="${nordline + i}">${nordline + i}</td>
                    <td>
                        <img src="images/icons/delete.svg" class="deleteRd btn-action-delete">
                    </td>
                    <td class="tmp-rdtracking" data-required="1" data-asignedpart="0">
                        <input type="text" class="truckingbox" value="${newRd[i].trackingno}" maxlength="20" placeholder="Please fill"/>
                    </td>
                    <td class="tmp-rdquantity">
                        <input type="number" min="0" value="${newRd[i].originalquantity}" maxlength="15" placeholder="Please fill"/>
                    </td>
                    <td class="tmp-rdselect">
                        <select data-selected="${newRd[i].unitid}" class="selectPT"></select>
                    </td>
                    <td class="tmp-rdweight">
                        <input type="number" min="0" value="${newRd[i].weight}" maxlength="15" style="width: 50px;" placeholder="Please fill"/>
                    </td>
                    <td class="tmp-rdum">
                        <select class="selectUM" data-value="${newRd[i].weightunitid}">
                        </select>
                    </td>
                    <td class="tmp-rdlocation" hidden>
                        <select class="selectLO" data-selected=${newRd[i].idLocation}>
                        </select>
                    </td>
                    <td class="tmp-rdnamepo">
                        <input type="text" value="${newRd[i].po}" maxlength="15" placeholder="Please fill"/>
                    </td>
                    <td class="tmp-rdpodate">
                        <input type="text" value="${newRd[i].podate}" maxlength="15" class="datepicker" placeholder="Please fill"/>
                    </td>
                    <td class="global" data-receivingdetailno="${nordline + i}">
                        <img class="addDetails" src="images/icons/add-detail.svg" data-tipo="global">
                    </td>
                </tr>
                `);
        }
    }
    else if (mode == 0) {
        for (var i = 0; i < x; i++) {
            $('.receipt-details-box .result-table .table').append(`
                <tr data-status="2" data-rowrd="${nordline + i}" data-noline="${nordline + i}" data-rdid="rd_${nordline +i}" id="rd_${nordline +i}" class="data-tracking">
                    <td class="receivingline noline_${nordline + i} noRowRD" data-rdline="${nordline + i}">${nordline + i}</td>
                    <td>
                        <img src="images/icons/delete.svg" class="deleteRd btn-action-delete">
                    </td>
                    <td class="tmp-rdtracking" data-required="1">
                        <input type="text" class="truckingbox" value="${newRd[i].trackingno}" maxlength="20" placeholder="Please fill"/>
                    </td>
                    <td style="position: relative;" class="tmp-rdpart" data-asignedpart="0" data-newproduct="${newRd[i].newProduct}" data-product="${newRd[i].product}" data-productid="${newRd[i].productid}" data-value="${newRd[i].productno}">
                        <input type="text" class="partinreceipt" value="${newRd[i].productno}" maxlength="20" placeholder="Please fill"/>
                        <img src="images/icons/edit.svg" class="editPartRD btn-action-editPart" data-editpart="edit">
                    </td>
                    <td class="tmp-rddescription" data-value="${newRd[i].description}">
                        <input type="text" value="${newRd[i].description}" maxlength="20" placeholder="Please fill"/>
                    </td>
                    <td class="tmp-rdqtypart">
                        <input type="text" value="${newRd[i].receivedqtypart}" maxlength="20" placeholder="Please fill"/>
                    </td>
                    <td class="tmp-rdorigen">
                        <input type="text" value="${newRd[i].origen}" maxlength="20" placeholder="Please fill"/>
                    </td>
                    <td class="tmp-rdquantity">
                        <input type="number" min="0" value="${newRd[i].originalquantity}" maxlength="15" placeholder="Please fill"/>
                    </td>
                    <td class="tmp-rdselect">
                        <select data-selected="${newRd[i].unitid}" class="selectPT"></select>
                    </td>
                    <td class="tmp-rdweight">
                        <input type="number" min="0" value="${newRd[i].weight}" maxlength="15" style="width: 50px;" placeholder="Please fill"/>
                    </td>
                    <td class="tmp-rdum">
                        <select class="selectUM" data-value="${newRd[i].weightunitid}">
                        </select>
                    </td>
                    <td class="tmp-rdlocation">
                        <select class="selectLO" data-selected=${newRd[i].idLocation}>
                        </select>
                    </td>
                    <td class="tmp-rdlotnumber">
                        <input type="text" min="0" value="${newRd[i].lotnumber}" maxlength="15" style="width: 50px;" placeholder="Please fill"/>
                    </td>
                </tr>
                `);
        }
    }
}
function insertrd(receivingid, newRd, mode){

    $('.content-table-newrd .table tr').map(function(i){
        if (i != 0) {
            if (mode == 1) {
                newRd.push({
                    trackingno: $(this).find('.data-tracking input').val().toString(),
                    originalquantity: $(this).find('.data-quantity input').val().toString(),
                    unitid: parseInt($(this).find('.data-package select').val()),
                    weight: $(this).find('.data-weight input').val().toString(),
                    weightunitid: parseInt($(this).find('.data-um select').val()),
                    idLocation: parseInt($(this).find('.data-location select').val()),
                    po: $(this).find('.data-namepo input').val().toString(),
                    podate: $(this).find('.data-podate input').val().toString()
                })
            }
            if (mode == 0) {
                var exist = parseInt($(this).find('.data-part').attr('data-exist'));
                var partnew = $(this).find('.data-part').attr('data-productid');
                var varProduct = 0;
                var indexProduct = 0;
                if (partnew == 'new') {
                    varProduct = 1;
                    indexProduct = 0;
                    if (exist == 1) {
                        indexProduct = $(this).find('.data-part').attr('data-indexpart');
                    }
                    else {
                        var partsl = parts.length;
                        if (partsl > 0) {
                            partsl = (partsl - 1);
                        }
                        if (partsl >= 0) {
                            indexProduct = partsl;
                        }
                    }
                }
                if (exist == 1) {
                    indexProduct = $(this).find('.data-part').attr('data-indexpart');
                }

                newRd.push({
                    trackingno: $(this).find('.data-tracking input').val().toString(),
                    productid: parseInt($(this).find('.data-part').attr('data-productid')),
                    productno: $(this).find('.data-part').attr('data-value').toString(),
                    description: $(this).find('.data-description').attr('data-value').toString(),
                    receivedqtypart: parseInt($(this).find('.data-qrtypart input').val()),
                    origen: $(this).find('.data-origen input').val().toString(),
                    originalquantity: $(this).find('.data-quantity input').val().toString(),
                    unitid: parseInt($(this).find('.data-package select').val()),
                    weight: $(this).find('.data-weight input').val().toString(),
                    weightunitid: parseInt($(this).find('.data-um select').val()),
                    idLocation: parseInt($(this).find('.data-location select').val()),
                    lotnumber: $(this).find('.data-lotnumber input').val().toString(),
                    product: indexProduct,
                    newProduct: varProduct
                })
            }

        }
    });
    return newRd;
}
function inserttd(receivingid, newTd){
    $('.content-table-newtd .table tr').map(function(i){
        if (i != 0) {
            newTd.push({
                tande: $(this).find('.tmp-data-nametd input').val().toString(),
                tandeDate: $(this).find('.tmp-data-tddate input').val().toString()
            })
        }
    });
    return newTd;
}
function writenewtd(newTd){
    var x = newTd.length;
    var notdline = $('.tande-box .result-table .table tr').length;
    var tempcont = 0;
    var tablenewTD = $('.tande-box .result-table .table');
    $('.tande-box .result-table .table tr').map(function(i){
        if (i != 0) {
            status = $(this).data('status');
            if (status == 0) {
                tempcont++;
            }
        }
    });
    notdline = (notdline - tempcont);
    if (notdline == 0) {
        $('#tabletd').removeClass('hide');
        $('#foundtd').css({"display":"none"});
        notdline = 1;
        tablenewTD.append(
            `<tr data-table-column=0>
                <td>No.</td>
                <td>Delete</td>
                <td>T and E</td>
                <td>T and E Created</td>
                <td>T and E Expires</td>
            </tr>`
        );
    }
    for (var i = 0; i < x; i++) {
        $('.tande-box .result-table .table').append(`
            <tr data-status="2" data-tdid="tmptd_${notdline+ i}" id="tmptd_${notdline + i}" data-linetd="${notdline + i}">
                <td class="linetd nolinetd_${notdline + i}">${notdline + i}</td>
                <td>
                    <img src="images/icons/delete.svg" class="deleteTd btn-action-delete">
                </td>
                <td class="tmp-tande">
                    <input type="text" value="${newTd[i].tande}" maxlength="50" size="35" required placeholder="Please fill"/>
                </td>
                <td class="tmp-data-datetd">
                    <input type="text" class="datepicker" value="${newTd[i].tandeDate}" required placeholder="Please fill"/>
                </td>
                <td class="tmp-data-dateexpirestd">
                    <input type="text" class="datepicker" value="${new Date(newTd[i].tandeDate).addDays(15).toString('MM-dd-yyyy')}" disabled/>
                </td>
            </tr>
            `);
    }
}
function insertfb(receivingid, newFb){
    // var arrayInsertPO = Array();
    $('.content-table-newfb .table tr').map(function(i){
        if (i != 0) {
            newFb.push({
                fbname: $(this).find('.tmp-data-namefb input').val().toString()
            })
        }
    });
    return newFb;
}
function writenewfb(newFb){
    var x = newFb.length;
    var nofbline = $('.freightbill-box .result-table .table tr').length;
    var tempcont = 0;
    var tablenewFB = $('.freightbill-box .result-table .table');
    $('.freightbill-box .result-table .table tr').map(function(i){
        if (i != 0) {
            status = $(this).data('status');
            if (status == 0) {
                tempcont++;
            }
        }
    });
    nofbline = (nofbline - tempcont);
    if (nofbline == 0) {
        $('#tablefb').removeClass('hide');
        $('#foundfb').css({"display":"none"});
        nofbline = 1;
        tablenewFB.append(
            `<tr data-table-column=0>
                <td>No.</td>
                <td>Delete</td>
                <td>Freightbill</td>
            </tr>`
        );
    }
    for (var i = 0; i < x; i++) {
        $('.freightbill-box .result-table .table').append(`
            <tr data-status="2" data-fbid="tmpfb_${nofbline + i}" id="tmpfb_${nofbline + i}" data-linefb="${nofbline + i}">
                <td class="linefb nolinefb_${nofbline + i}">${nofbline + i}</td>
                <td>
                    <img src="images/icons/delete.svg" class="deleteFb btn-action-delete">
                </td>
                <td class="tmp-freightbill">
                    <input type="text" value="${newFb[i].fbname}" maxlength="50" size="35" required placeholder="Please fill"/>
                </td>
            </tr>
            `);
    }
}
function insertpo(receivingid, newPo){
    // var arrayInsertPO = Array();
    var statusInsert;
    $('.content-table-newpo .table tr').map(function(i){
        if (i != 0) {
            newPo.push({
                // receivingdetailid: parseInt($(this).find('.data-selected select').val()),
                rdline: parseInt($(this).find('.data-selected select').val()),
                po: $(this).find('.data-namepo input').val().toString(),
                poDate: $(this).find('.data-podate input').val().toString()
            })
        }
    });
    return newPo;
}
function writenewpo(newPo, first=false){
    var x = newPo.length;
    var nopoline = $('.purchase-order-box .result-table .table tr').length;
    var tempcont = 0;
    var tablenewPO = $('.purchase-order-box .result-table .table');
    $('.purchase-order-box .result-table .table tr').map(function(i){
        if (i != 0) {
            status = $(this).data('status');
            if (status == 0) {
                tempcont++;
            }
        }
    });
    if (nopoline == 0) {
        $('#tablepo').removeClass('hide');
        $('#foundpo').css({"display":"none"});
        nopoline = 1;
        tablenewPO.append(
            `<tr data-table-column=0>
                <td>No.</td>
                <td>Delete</td>
                <td>Receiving Line No.</td>
                <td>Purchase Order</td>
                <td>PO Date</td>
            </tr>`
        );
    }
    nopoline = (nopoline - tempcont);
    for (var i = 0; i < x; i++) {
        $('.purchase-order-box .result-table .table').append(`
            <tr data-status="2" data-srdline="${newPo[i].rdline}" data-poid="tmppo_${nopoline + i}" id="tmppo_${nopoline + i}" data-linepo="${nopoline +i}">
                <td class="linepo nolinepo_${nopoline + i}">${nopoline + i}</td>
                <td>
                    <img src="images/icons/delete.svg" class="deletePo btn-action-delete">
                </td>
                <td class="tmp-purchaserow">
                    <select data-selected="${newPo[i].receivingdetailid}" class="selectRD"></select>
                </td>
                <td class="tmp-data-namepo">
                    <input type="text" value="${newPo[i].po}" maxlength="50" size="35" required placeholder="Please fill"/>
                </td>
                <td class="tmp-data-datepo">
                    <input type="text" class="datepicker" value="${newPo[i].poDate}" required placeholder="Please fill"/>
                </td>
            </tr>
            `);
    }
}
//Detecta los cambios de los inputs y select de esta pagina.
//Obtiene el renglon del cual pertenece y cambia el atributo data-change a 1 para posteriormente agregarlo al arreglo para actualizarlo
//Se almacena en el atributo data-value el valor de la base de datos y en caso que los valores se regresen a los valores anteriores se cambia data-change a 0 para no asignarlo al arreglo
var dataerror = 0;
$('body').on('change', '.inputdata', function(event) {
    var idc = event.target.id;
    var val = $('#'+idc).parent().parent();
    var sectionbox = val.attr('class');
    this.dataset.ch = 1;
    changes(idc, val, sectionbox);
    var inputlength = $(this).val().length;
    var classs = $(this).attr('class');

    if (inputlength == 0 && classs != 'inputdata truckingbox' && classs != 'inputdata partinreceipt') {
        $(this).addClass('input-error');
        dataerror = dataerror + 1;
    }
    if (inputlength > 0) {
        $(this).removeClass('input-error');
        $(this).parent().attr('data-error', 0);
        if (dataerror > 0) {
            dataerror = dataerror - 1;
        }
    }
});
//Se decta si se vacia el input de parts y se verifica si ese receiving detail tiene tracking
$('body').on('change', '.partinreceipt', function(){
    var data = $(this).val().length;
    var id = $(this).parent().parent().attr('id');
    var section = $('#'+id);

    var trackingdata = section.find('.truckingbox').val().length;

    if (data == 0) {
        $(this).parent().attr('data-asignedpart', 0);
        $(this).parent().attr('data-value', 0);
        $(this).parent().attr('data-productid', 0);
        section.find('.rdpartdescription  input').val('');
        if (trackingdata == 0) {
            section.find('.rdtracking').attr('data-required', 1);
        }
    }
});
$('body').on("click", ".save-receiving", function(){
    var row = $('.sid');
    var receivingid = row.data('rid');
    var cont = 0;
    var contqty = 0;
    var mode = $('#customerid option:selected').attr('data-mode');

    $('.receipt-details-box .result-table .table tr').each(function(i){
        if (i != 0) {
            var status = $(this).attr('data-status');
            if (status == 1) {
                var insertedtr = $(this).find('.rdtracking input').val().length;
                if (insertedtr > 0) {
                    $(this).find('.rdtracking').attr('data-required', 0)
                }

                var required = $(this).find('.rdtracking').attr('data-required');
                var receivedqtypart = $(this).find('.rdqtypart input').val().length;
                var asignedpart = $(this).find('.rdpartno').attr('data-asignedpart');

                if (mode == 0) {
                    if (required == 1 && asignedpart == 0) {
                        cont++;
                        $(this).find('.rdtracking input').addClass('input-error');
                        $('body,html').animate({ scrollTop: 530 }, 700);
                    }
                }
                if (mode == 1) {
                    if (required == 1) {
                        cont++;
                        $(this).find('.rdtracking input').addClass('input-error');
                        $('body,html').animate({ scrollTop: 530 }, 700);
                    }
                }
                if (receivedqtypart == 0) {
                    contqty++;
                    $(this).find('.rdqtypart input').addClass('input-error');
                    $('body,html').animate({ scrollTop: 530 }, 700);
                }
            }
            if (status == 2) {
                var insertedtr = $(this).find('.tmp-rdtracking input').val().length;
                var asignedpart;
                if (mode == 1) {asignedpart = $(this).find('.tmp-rdtracking').attr('data-asignedpart');}
                if (mode == 0) {asignedpart = $(this).find('.tmp-rdpart').attr('data-asignedpart');}
                if (insertedtr > 0) {
                    $(this).find('.tmp-rdtracking').attr('data-required', 0)
                }
                if (insertedtr == 0) {
                    $(this).find('.tmp-rdtracking').attr('data-required', 1)
                }
                if (asignedpart == 1) {
                    $(this).find('.tmp-rdtracking').attr('data-required', 0)
                }
                var required = $(this).find('.tmp-rdtracking').attr('data-required');
                if (required == 1 && asignedpart == 0) {
                    cont++;
                    $(this).find('.tmp-rdtracking input').addClass('input-error');
                    $('body,html').animate({ scrollTop: 530 }, 700);
                }
            }
        }
    });
    var selectcustomer = $('#customerid option:selected').val();
    var selectvendor = $('#vendorid option:selected').val();
    var selectcarrier = $('#carrierid option:selected').val();
    var notselect = 0;
    var statusreceiving = $('main').attr('receiving-status');
    if (selectcustomer == 'select0' || selectvendor == 'select0' || selectcarrier == 'select0') {
        if(selectcustomer == 'select0'){notselect = 1; $('body,html').animate({ scrollTop: 100 }, 700); $('#customerid').addClass('input-error');}
        if(selectvendor == 'select0'){
            var selectexistvendor = $('#vendorid').attr('data-active');
            if (selectexistvendor == 1) {
                notselect = 1;
                $('body,html').animate({ scrollTop: 100 }, 700);
                $('#vendorid').addClass('input-error');
            }
        }
        if(selectcarrier == 'select0'){
            var selectexistcarrier = $('#carrierid').attr('data-active');
            if (selectexistcarrier == 1) {
                notselect = 1;
                $('body,html').animate({ scrollTop: 100 }, 700);
                $('#carrierid').addClass('input-error');
            }
        }
    }
    //Condiciones para detectar si se encuentra algun error en los campos
    if (cont == 0 && dataerror == 0 && notselect == 0) {
        if (mode == 0) {
            if (contqty == 0) {
                showdialog(()=>{
                    saveReceiving(receivingid);
                }, 'Are you sure save changes in this Receiving ?')
            }
        }
        if (mode == 1) {
            showdialog(()=>{
                saveReceiving(receivingid);
            }, 'Are you sure save changes in this Receiving ?')
        }
    }
    else{
        console.log("Favor de llenar los campos requeridos");
        $('.error-data-edit').css({"display":"block"});
    }
});
//Detectar cambios en Southbound y Northbound
$('body').on('click', '#South', function(){
    var val = $(this).attr('value');
    var dbvalue= $('.nothosouth-select').attr('data-value');
    if (val != dbvalue) {
        $('.nothosouth-select').attr('data-ch', 1);
    }
    else if (val === dbvalue){
        $('.nothosouth-select').attr('data-ch', 0);
    }
    changes(0, 0, 'receiving')
});
$('body').on('click', '#North', function(){
    var val = $(this).attr('value');
    var dbvalue= $('.nothosouth-select').attr('data-value');
    if (val != dbvalue) {
        $('.nothosouth-select').attr('data-ch', 1);
    }
    else if (val === dbvalue){
        $('.nothosouth-select').attr('data-ch', 0);
    }
    changes(0, 0, 'receiving')
});
//Detecta si hay cambios en Damaged y hazmat
$('body').on('click', '.hazmat-select', function(){
    var val = $(this).attr('value');
    var dbvalue= $('.hazmat-select').attr('data-value');
    if (val != dbvalue) {
        $('.hazmat-select').attr('data-ch', 1);
    }
    else if (val === dbvalue){
        $('.hazmat-select').attr('data-ch', 0);
    }
    var sectionbox = 'receiving';
    changes(0,0, sectionbox);
});
$('body').on('click', '.damaged-select', function(){
    var val = $(this).attr('value');
    var dbvalue= $('.damaged-select').attr('data-value');
    if (val != dbvalue) {
        $('.damaged-select').attr('data-ch', 1);
    }
    else if (val === dbvalue){
        $('.damaged-select').attr('data-ch', 0);
    }
    var sectionbox = 'receiving';
    changes(0,0, sectionbox);
});
//Detecta si hay cambios en los comentarios
$('body').on('change', '#comments', function(){
    var dbvaluer;
    var currentvaluer;
    dbvaluer = $(this).attr("data-value");
    currentvaluer = $(this).val();

    if (dbvaluer == currentvaluer) {
        $('#comments').attr('data-ch', 0);
    }
    else{
        $('#comments').attr('data-ch', 1);
    }
    var sectionbox = 'receiving';
    changes(0,0, sectionbox);
});
//funcion que se encarga de detectar cambios
function changes(idc, val, sectionbox){
    receivingchanged = Array();
    pochanged = Array();
    receivingdetailchanged = Array();
    tandechanged = Array();
    freightbillchanged = Array();

    //Se detectan los cambios en la informacion de RECEIVING
    if (sectionbox == 'receiving') {
        var dbvalue;
        var currentvalue;
        $('.receiving-data-box .itemdata-receiving').map(function(i){
            dbvalue = $(this).find('.inputdatareceiving').attr('data-value');
            currentvalue = $(this).find('.inputdatareceiving').val();
            if (dbvalue == currentvalue){
                $(this).find('.inputdatareceiving').attr('data-ch', 0)
            }
        });
        //Se hace un arreglo que guarda que elementos tienen cambio, obteniendo los vaores del atributo data-ch: 0 no hay cambio | 1 hay cambio
        receivingchanged.push({
            freightbill: $('.itemdata-receiving').find('#freightbill').attr('data-ch'),
            customer: $('.itemdata-receiving').find('#customerid').attr('data-ch'),
            vendor: $('.select-vendor-box').find('#vendorid').attr('data-ch'),
            carrier: $('.select-carrier-box').find('#carrierid').attr('data-ch'),
            priority: $('.itemdata-receiving').find('#priority').attr('data-ch'),
            NorthOrSouth: $('.nothosouth-select').attr('data-ch'),
            hazmat: $('.hazmat-select').attr('data-ch'),
            damaged: $('.damaged-select').attr('data-ch'),
            comments: $('#comments').attr('data-ch')
        })
        var isUpdateReceiving = false;
        for (var i = 0; i < receivingchanged.length; i++) {
            if (receivingchanged[i].freightbill == 1 || receivingchanged[i].customer == 1 || receivingchanged[i].vendor == 1 || receivingchanged[i].carrier == 1 || receivingchanged[i].priority == 1 || receivingchanged[i].NorthOrSouth == 1 || receivingchanged[i].hazmat == 1 || receivingchanged[i].damaged == 1 || receivingchanged[i].comments == 1) {
                $('.receiving-data-box').attr('data-change', 1);
                isUpdateReceiving = true;
            }
        }
        if (!isUpdateReceiving) {
            $('.receiving-data-box').attr('data-change', 0);
        }
    }
    //Se detecta de que seccion son los cambios; En este caso es para purchase orders
    if (sectionbox == 'itempo') {
        var dbvaluepo;
        var currentvaluepo;
        $('.purchase-order-box .result-table .table tr').map(function(i){
            if (i != 0) {
                //Se obtiene el valor actual por medio de val y el valor anterior de la DB que esta almacenado en un atributo data-value
                $(this).find('td').each(function(){
                        dbvaluepo = $(this).attr("data-afterline");
                        currentvaluepo = $(this).find('.inputdata').val();
                        //Si ambos valores son iguales el atributo data-ch cambia a 0: que significa que no tiene cambios
                        if (dbvaluepo == currentvaluepo) {
                            $(this).find('.inputdata').attr('data-ch', 0);
                        }
                });
                //Se hace un arreglo que guarda que elementos tienen cambio, obteniendo los vaores del atributo data-ch: 0 no hay cambio | 1 hay cambio
                pochanged.push({
                    poid: $(this).attr('data-poid'),
                    select: $(this).find('.purchaserow select').attr('data-ch'),
                    poname: $(this).find('.data-namepo input').attr('data-ch'),
                    podate: $(this).find('.data-datepo input').attr('data-ch')
                })
            }
        });
        var isUpdatePo = false;
        for (var i = 0; i < pochanged.length; i++) {
            if (pochanged[i].select == 1 || pochanged[i].poname == 1 || pochanged[i].podate == 1) {
                $('#rowPo_'+pochanged[i].poid).attr('data-change', 1);
                isUpdatePo = true;
            }
        }
        if(!isUpdatePo){
            val.attr('data-change', 0);
        }
    }
    if (sectionbox == 'itemrd sid') {
        var dbvaluerd;
        var currentvaluerd;

        $('.receipt-details-box .result-table .table tr').map(function(i){
            if (i != 0) {
                $(this).find('td').each(function(){
                        dbvaluerd = $(this).data("value");
                        currentvaluerd = $(this).find('.inputdata').val();

                        if (dbvaluerd == currentvaluerd) {
                            $(this).find('.inputdata').attr('data-ch', 0);
                        }

                });
                receivingdetailchanged.push({
                    receivingidetaild: $(this).attr('data-rdid'),
                    truckingno: $(this).find('.rdtracking input').attr('data-ch'),
                    originalquantity: $(this).find('.rdquantity input').attr('data-ch'),
                    unitid: $(this).find('.rdselect select').attr('data-ch'),
                    weight: $(this).find('.rdweight input').attr('data-ch'),
                    weightunitid:$(this).find('.rdum select').attr('data-ch'),
                    idLocation: $(this).find('.rdlocation select').attr('data-ch'),
                    po: $(this).find('.rdnamepo input').attr('data-ch'),
                    podate: $(this).find('.rdpodate input').attr('data-ch'),
                })

            }
        });
        var isUpdateRd = false;
        for (var i = 0; i < receivingdetailchanged.length; i++) {
            if (receivingdetailchanged[i].truckingno == 1 || receivingdetailchanged[i].originalquantity == 1 || receivingdetailchanged[i].unitid == 1 || receivingdetailchanged[i].weight == 1 || receivingdetailchanged[i].weightunitid == 1 || receivingdetailchanged[i].idLocation == 1 || receivingdetailchanged[i].po == 1 || receivingdetailchanged[i].podate == 1) {
                $(val).data('change', 1);
                isUpdateRd = true;
            }
        }
        if(!isUpdateRd){
            $(val).data('change', 0);
        }
    }
    if (sectionbox == 'itemrd sid itemrddefault') {
        var dbvaluerd;
        var currentvaluerd;

        $('.receipt-details-box .result-table .table tr').map(function(i){
            if (i != 0) {
                $(this).find('td').each(function(){
                        dbvaluerd = $(this).data("value");
                        currentvaluerd = $(this).find('.inputdata').val();

                        if (dbvaluerd == currentvaluerd) {
                            $(this).find('.inputdata').attr('data-ch', 0);
                        }

                });
                receivingdetailchanged.push({
                    receivingidetaild: parseInt($(this).attr('data-rdid')),
                    truckingno: parseInt($(this).find('.rdtracking input').attr('data-ch')),
                    originalquantity: parseInt($(this).find('.rdquantity input').attr('data-ch')),
                    productid: parseInt($(this).find('.rdpartno input').attr('data-ch')),
                    receivedqtypart: parseInt($(this).find('.rdqtypart input').attr('data-ch')),
                    origen: parseInt($(this).find('.rdorigen input').attr('data-ch')),
                    unitid: parseInt($(this).find('.rdselect select').attr('data-ch')),
                    weight: parseInt($(this).find('.rdweight input').attr('data-ch')),
                    weightunitid:parseInt($(this).find('.rdum select').attr('data-ch')),
                    idLocation: parseInt($(this).find('.rdlocation select').attr('data-ch')),
                    lotnumber: parseInt($(this).find('.rdlotnumber input').attr('data-ch')),
                })

            }
        });
        var isUpdateRd = false;
        for (var i = 0; i < receivingdetailchanged.length; i++) {
            if (receivingdetailchanged[i].truckingno == 1 || receivingdetailchanged[i].originalquantity == 1 || receivingdetailchanged[i].unitid == 1 || receivingdetailchanged[i].weight == 1 || receivingdetailchanged[i].weightunitid == 1 || receivingdetailchanged[i].idLocation == 1 || receivingdetailchanged[i].productid == 1 || receivingdetailchanged[i].receivedqtypart == 1
            || receivingdetailchanged[i].origen == 1 || receivingdetailchanged[i].lotnumber == 1) {
                $(val).data('change', 1);
                isUpdateRd = true;
            }
        }
        if(!isUpdateRd){
            $(val).data('change', 0);
        }
    }
    //Si en la seccion TandE los datos del input vuelven al valor anterior (valor actual de la base de datos) su atributo data-ch vuelve a 0
    //Arreglo donde se muestra los cambios donde 0 significa que no hay cambios y 1 es que tiene cambios
    if (sectionbox == 'itemtd') {
        var dbvaluerd;
        var currentvaluerd;
        $('.tande-box .result-table .table tr').map(function(i){
            if (i != 0) {
                $(this).find('td').each(function(){
                        dbvaluerd = $(this).data("value");
                        currentvaluerd = $(this).find('.inputdata').val();

                        if (dbvaluerd == currentvaluerd) {
                            $(this).find('.inputdata').attr('data-ch', 0);
                        }

                });
                tandechanged.push({
                    tandeid: $(this).attr('data-tdid'),
                    tande: $(this).find('.data-nametd input').attr('data-ch'),
                    tandeexpires: $(this).find('.data-datetd input').attr('data-ch'),
                })
            }
        });
        var isUpdateTd = false;
        for (var i = 0; i < tandechanged.length; i++) {
            if (tandechanged[i].tande == 1 || tandechanged[i].tandeexpires == 1) {
                $(val).data('change', 1);
                isUpdateTd = true;
            }
        }
        if(!isUpdateTd){
            $(val).data('change', 0);
        }
    }
    if (sectionbox == 'itemfb') {
        var dbvaluefb;
        var currentvaluefb;
        $('.freightbill-box .result-table .table tr').map(function(i){
            if (i != 0) {
                $(this).find('td').each(function(){
                        dbvaluefb = $(this).data("value");
                        currentvaluefb = $(this).find('.inputdata').val();

                        if (dbvaluefb == currentvaluefb) {
                            $(this).find('.inputdata').attr('data-ch', 0);
                        }

                });
                freightbillchanged.push({
                    fbid: $(this).attr('data-fbid'),
                    freightbill: $(this).find('.data-namefb input').attr('data-ch')
                })
            }
        });
        var isUpdateFb = false;
        for (var i = 0; i < freightbillchanged.length; i++) {
            if (freightbillchanged[i].freightbill == 1) {
                $(val).data('change', 1);
                isUpdateFb = true;
            }
        }
        if(!isUpdateFb){
            $(val).data('change', 0);
        }
    }
}
//Funcion que verifica que los inputs tengan valor y de no ser así se agrega un estilo de alerta a los inputs sin valor y no se cierra el contenedor
function validacionInputsRD(newRd){
    jsonNewRD = Array();
    var mode = $('#customerid option:selected').attr('data-mode');
    var tmpflag = false;
    $('.content-table-newrd .table tr').map(function(i){
        if (i != 0) {
            if (mode == 1) {
                jsonNewRD.push({
                    tracking: $(this).find('.data-tracking input').val().length,
                    quantity: $(this).find('.data-quantity input').val().length,
                    weight: $(this).find('.data-weight input').val().length,
                    po: $(this).find('.data-namepo input').val().length,
                    podate: $(this).find('.data-podate input').val().length,
                })
                var quantity = $(this).find('.data-quantity input').val().length;

                if (quantity == 0) {$(this).find('.data-quantity input').addClass('input-error');}

                //Todos los inputs tienen que tener valor para poder mandar llamar la funcion que se encargara de escribir los nuevos receiving details
                for (var i = 0; i < jsonNewRD.length; i++) {
                    if (jsonNewRD[i].quantity == 0) {
                        tmpflag = false;
                    }
                    else{
                        tmpflag = true;
                    }
                }
            }
            else if (mode == 0) {
                jsonNewRD.push({
                    tracking: $(this).find('.data-tracking input').val().length,
                    partno: $(this).find('.data-part input').val().length,
                    description: $(this).find('.data-description input').val().length,
                    qtypart: $(this).find('.data-qrtypart input').val().length,
                    origen: $(this).find('.data-origen input').val().length,
                    quantity: $(this).find('.data-quantity input').val().length,
                    weight: $(this).find('.data-weight input').val().length,
                    lotnumber: $(this).find('.data-lotnumber input').val().length
                })
                var tracking = $(this).find('.data-tracking input').val().length;
                var partno = $(this).find('.data-part input').val().length;
                var qtypart = $(this).find('.data-qrtypart input').val().length;

                if (qtypart == 0) {$(this).find('.data-qrtypart input').addClass('input-error');}

                //Todos los inputs tienen que tener valor para poder mandar llamar la funcion que se encargara de escribir los nuevos receiving details
                for (var i = 0; i < jsonNewRD.length; i++) {
                    if (jsonNewRD[i].qtypart == 0) {
                        tmpflag = false;
                    }
                    else{
                        tmpflag = true;
                    }
                }
            }


        }
    });

    if (tmpflag == true) {
        $('.add-receiving-box').css({"animation":"pop-out","animation-duration":"0.25s"});
        setTimeout(function(){
            $('.add-receiving-box').css({"display":"none"});
        },250);
        $('.box-shadow').removeClass('visible');
        var headT = $('.content-table-newrd .table');
        headT.empty();
        $('.receipt-details-box .result-table').css({"height":"430px"});
        writenewrd(newRd);
        getUnitMesurament();
        getLocation();
        getPackagetype();
        getReceivingRows();
        getPoRD();
        $('.receipt-details-box table tr').each(function(i){
            if (i != 0) {
                var data = $(this).find('.truckingbox').val().length;
                var status = $(this).attr('data-status');
                if (mode == 0) {
                    if (status == 2) {
                        var dataasignedpart = $(this).find('.tmp-rdpart input').val().length;
                        if (dataasignedpart > 0) {
                            $(this).find('.tmp-rdpart').attr('data-asignedpart', 1);
                            $(this).find('.truckingbox').parent().attr('data-required', 0);
                        }
                    }
                }
                if (data > 0) {
                    $(this).find('.truckingbox').parent().attr('data-required', 0);
                }
            }
        });
    }
}
//Funcion que verifica que los inputs tengan valor y de no ser así se agrega un estilo de alerta a los inputs sin valor y no se cierra el contenedor
function validacionInputsPO(newPo){
    jsonNewPO = Array();
    $('.content-table-newpo .table tr').map(function(i){
        if (i != 0) {
            jsonNewPO.push({
                po: $(this).find('.data-namepo input').val().length,
                podate: $(this).find('.data-podate input').val().length
            });

            var po = $(this).find('.data-namepo input').val().length;
            var podate = $(this).find('.data-podate input').val().length;

            if (po == 0) {$(this).find('.data-namepo input').addClass('input-error');}
            if (podate == 0) {$(this).find('.data-podate input').addClass('input-error');}
        }
    });
    var tmpflag = false;
    //Todos los inputs tienen que tener valor para poder mandar llamar la funcion que se encargara de escribir los nuevos receiving details
    for (var i = 0; i < jsonNewPO.length; i++) {
        if (jsonNewPO[i].po == 0 || jsonNewPO[i].podate == 0){
            tmpflag = false;
        }
        else{
            tmpflag = true;
        }
    }
    if (tmpflag == true) {
        writenewpo(newPo);
        getReceivingRows();
        getPoRD();
        $('.add-purchase-box').css({"animation":"pop-out","animation-duration":"0.25s"});
        setTimeout(function(){
            $('.add-purchase-box').css({"display":"none"});
        },250);
        $('.box-shadow').removeClass('visible');
        var headT = $('.content-table-newpo .table');
        headT.empty();
    }
}
//Funcion que verifica que los inputs tengan valor y de no ser así se agrega un estilo de alerta a los inputs sin valor y no se cierra el contenedor
function validacionInputsFB(newFb){
    jsonNewFB = Array();
    $('.content-table-newfb .table tr').map(function(i){
        if (i != 0) {
            jsonNewFB.push({
                freightbill: $(this).find('.tmp-data-namefb input').val().length
            });

            var freightbill = $(this).find('.tmp-data-namefb input').val().length;

            if (freightbill == 0) {$(this).find('.tmp-data-namefb input').addClass('input-error');}
        }
    });
    var tmpflag = false;
    //Todos los inputs tienen que tener valor para poder mandar llamar la funcion que se encargara de escribir los nuevos receiving details
    for (var i = 0; i < jsonNewFB.length; i++) {
        if (jsonNewFB[i].freightbill == 0){
            tmpflag = false;
        }
        else{
            tmpflag = true;
        }
    }
    if (tmpflag == true) {
        $('#tablefb').removeClass('hide');
        $('#foundfb').css({"display":"none"});
        writenewfb(newFb);
        $('.add-freightbill-box').css({"animation":"pop-out","animation-duration":"0.25s"});
        setTimeout(function(){
            $('.add-freightbill-box').css({"display":"none"});
        },250);
        $('.box-shadow').removeClass('visible');
        var headT = $('.content-table-newfb .table');
        headT.empty();
    }
}
function validacionInputsTE(newTd){
    jsonNewTE = Array();
    $('.content-table-newtd .table tr').map(function(i){
        if (i != 0) {
            jsonNewTE.push({
                tande: $(this).find('.tmp-data-nametd input').val().length,
                tandecreated: $(this).find('.tmp-data-tddate input').val().length
            });

            var tande = $(this).find('.tmp-data-nametd input').val().length;
            var tandecreated = $(this).find('.tmp-data-tddate input').val().length;

            if (tande == 0) {$(this).find('.tmp-data-nametd input').addClass('input-error');}
            if (tandecreated == 0) {$(this).find('.tmp-data-tddate input').addClass('input-error');}
        }
    });
    var tmpflag = false;
    //Todos los inputs tienen que tener valor para poder mandar llamar la funcion que se encargara de escribir los nuevos receiving details
    for (var i = 0; i < jsonNewTE.length; i++) {
        if (jsonNewTE[i].tande == 0 || jsonNewTE[i].tandecreated == 0){
            tmpflag = false;
        }
        else{
            tmpflag = true;
        }
    }
    if (tmpflag == true) {
        writenewtd(newTd);
        $('.add-tande-box').css({"animation":"pop-out","animation-duration":"0.25s"});
        setTimeout(function(){
            $('.add-tande-box').css({"display":"none"});
        },250);
        $('.box-shadow').removeClass('visible');
        var headT = $('.content-table-newtd .table');
        headT.empty();
    }
}
function getArrayFB(){
    var preUpdateFB = Array();
    var preInsertFB = Array();
    var status;
    var idfb;
    var changestatus;
  $('.freightbill-box .result-table .table tr').each(function(i){
      status = $(this).data('status');
      idfb = $(this).data('fbid');
      changestatus = $(this).data('change');
      if(i != 0){
          if (status == 0) {
              freightbill.push({
                  status: $(this).data('status'),
                  frid: $(this).data('fbid'),
                  freightbill: $(this).find('.data-namefb').attr('data-value')
              })
          }
          if (status == 1) {
              if (changestatus == 1) {
                  freightbill.push({
                      status: $(this).data('status'),
                      change: $(this).data("change"),
                      line: $(this).data('linefb'),
                      frid: $(this).data('fbid'),
                      fr: $(this).find('.data-namefb input').val().toString()
                  })
              }
          }
          if(status == 2){
              freightbill.push({
                  status: $(this).data('status'),
                  fr: $(this).find('.tmp-freightbill input').val().toString(),
                  line: $(this).data('linefb'),
              })
          }
      }
  });
}
function getArrayTD(){
    var preUpdateTD = Array();
    var preInsertTD = Array();
    var status;
    var idtd;
    var changestatus;
  $('.tande-box .result-table .table tr').each(function(i){
      status = $(this).data('status');
      idtd = $(this).data('tdid');
      changestatus = $(this).data('change');
      if(i != 0){
          if (status == 0) {
              tande.push({
                  status: $(this).data('status'),
                  tandeid: $(this).data('tdid'),
                  tande: $(this).find('.data-nametd').attr('data-value')
              })
          }
          if (status == 1) {
              if (changestatus == 1) {
                  tande.push({
                      status: $(this).data('status'),
                      change: $(this).data("change"),
                      line: $(this).data('linetd'),
                      tandeid: $(this).data('tdid'),
                      tande: $(this).find('.data-nametd input').val().toString(),
                      tandeexpires: $(this).find('.data-datetd input').val().toString()
                  })
              }
          }
          if(status == 2){
              tande.push({
                  status: $(this).data('status'),
                  tande: $(this).find('.tmp-tande input').val().toString(),
                  tandeexpires: $(this).find('.tmp-data-datetd input').val().toString(),
                  line: $(this).data('linetd'),
              })
          }
      }
  });
}
function getDataReceiving(){
    var changereceiving = $('.receiving-data-box').attr('data-change');
    var vendorid = $('#vendorid').data('active');
    var vendornew = $('#vendornew').data('active');
    var NorthOrSouth;
    var hazmat;
    var damaged;
    var north = $('#North').prop('checked');
    var south = $('#South').prop('checked');
    var h = $('#hazmat').prop('checked');
    var d = $('#damaged').prop('checked');

    if (north == true) {NorthOrSouth = 'Northbound';}
    if (south == true) {NorthOrSouth = 'Southbound';}
    if (h == true) {hazmat = 'yes';}
    else {hazmat = 'no';}
    if (d == true) {damaged = 'yes';}
    else {damaged = 'no';}
    var receivingid = $('main').data('receiving-id');

    var insert = 1;
    if (receivingid != "") {
        insert = 0;
    }
    if (changereceiving == 1) {
        receiving = {
            receivingid: $('main').data('receiving-id'),
            receivingno: $('#recZebra').attr('data-reczebra'),
            freightbill: $('.itemdata-receiving').find('#freightbill').val(),
            customerid: parseInt($('.itemdata-receiving').find('#customerid').val()),
            vendorid: parseInt($('.select-vendor-box').find('#vendorid').val()),
            carrierid: parseInt($('.select-carrier-box').find('#carrierid').val()),
            priority: parseInt($('.itemdata-receiving').find('#priority').val()),
            NorthOrSouth: NorthOrSouth,
            hazmat: hazmat,
            damaged: damaged,
            qtylabels: parseInt($('#qtylabel').val()),
            comments: $('#comments').val().toString(),
            newvendor: parseInt($('#vendornew').attr('data-active')),
            vendor:  $('#vendornew').val().toString(),
            newcarrier: parseInt($('#carriernew').attr('data-active')),
            carrier: $('#carriernew').val().toString(),
            status: 1,
            insert: insert,
            user: $('main').attr('data-user')
        }
    }
}
function getArrayExtraCargos(){
    $('#services_list .table tr').map(function(i){
        if (i != 0) {
            var status = $(this).data('status');
            if (status == 0) {
                extraCargos.push({
                    servicereceivingid: $(this).data('id'),
                    status: 0
                })
            }
            if (status == 2) {
                extraCargos.push({
                    serviceid: $(this).data('serviceid'),
                    cost: $(this).data('cost'),
                    status: 2,
                    information: $(this).data('information')
                })
            }
        }
    });
}
function getArrayPO(){
    var status;
    var idpo;
    var changestatus;
  $('.purchase-order-box .result-table .table tr').each(function(i){
      status = $(this).data('status');
      idpo = $(this).data('poid');
      changestatus = $(this).attr('data-change');
      if(i != 0){
          if (status == 0) {
              purchaseOrder.push({
                  status: $(this).data('status'),
                  poid: $(this).data('poid'),
                  po: $(this).find('.data-namepo').attr('data-value'),
                  receivingdetailid: parseInt($(this).find('.purchaserow select').val())
              })
          }
          if (status == 1) {
              if (changestatus == 1) {
                  purchaseOrder.push({
                      status: $(this).data('status'),
                      change: $(this).data("change"),
                      poid: $(this).data('poid'),
                      line: $(this).data('linepo'),
                      lineno: parseInt($(this).find('.purchaserow select').val()),
                      receivingdetailno: parseInt($(this).find('.purchaserow select').val()),
                      po: $(this).find('.data-namepo input').val().toString(),
                      poDate: $(this).find('.data-datepo input').val().toString()
                  })
              }
          }
          if(status == 2){
              purchaseOrder.push({
                  status: $(this).data('status'),
                  line: $(this).data('linepo'),
                  lineno: $(this).find('.tmp-purchaserow select').val().toString(),
                  receivingdetailno: $(this).find('.tmp-purchaserow select').val().toString(),
                  po: $(this).find('.tmp-data-namepo input').val().toString(),
                  poDate: $(this).find('.tmp-data-datepo input').val().toString()
              })
          }
      }
  });
}
function getArrayRD(){
     var status;
     var changestatus;
     var mode = $('#customerid option:selected').attr('data-mode');
  $('.receipt-details-box .result-table .table tr').each(function(i){
      status = $(this).data('status');
      changestatus = $(this).data('change');

      if(i != 0){

          if (status == 0) {
              receivingDetail.push({
                  status: $(this).data('status'),
                  receivingdetailid: $(this).data('rdid'),
                  truckingid: $(this).find('.rdtracking').data('id'),
                  receivingdetailno: $(this).data('afterline'),
                  truckingno: $(this).find('.rdtracking').attr('data-value')
              })
          }
          if (mode == 1) {
              if (status == 1) {
                  if (changestatus == 1) {
                      var truckingidd = $(this).find('.rdtracking').attr('data-id');
                      var poo = $(this).find('.rdnamepo input').val();
                      var podatee = $(this).find('.rdpodate input').val();
                      var weightt = $(this).find('.rdweight input').val();
                      var truckingnoo = $(this).find('.rdtracking input').val();
                      var originalqtyy = $(this).find('.rdquantity input').val();

                      if (truckingnoo == "NaN" || truckingnoo == "undefined" || truckingnoo == "" || truckingnoo == "null") {truckingnoo = "none"}
                      if (truckingidd == "NaN" || truckingidd == "undefined" || truckingidd == "" || truckingidd == "null") {truckingidd = 0}
                      if (originalqtyy == "NaN" || originalqtyy == "undefined" || originalqtyy == "" || originalqtyy == "null") {originalqtyy = 0}
                      if (poo == "NaN" || poo == "undefined" || poo == "") {poo = "none"}
                      if (podatee == "NaN" || podatee == "undefined" || podatee == "") {podatee = '0000-00-00'}
                      if (weightt == "NaN" || weightt == "undefined" || weightt == "") {weightt = 0}

                      receivingDetail.push({
                          status: 1,
                          change: 1,
                          receivingdetailid: $(this).data('rdid'),
                          receivingdetailno: $(this).data('rowrd'),
                          truckingid: truckingidd,
                          truckingno: truckingnoo,
                          truckingnodb: $(this).find('.rdtracking').attr('data-value'),
                          originalquantity: originalqtyy,
                          unitid: parseInt($(this).find('.rdselect select').val()),
                          weight: weightt,
                          weightunitid: parseInt($(this).find('.rdum select').val()),
                          idLocation: parseInt($(this).find('.rdlocation select').val()),
                          po: poo,
                          podate: podatee,
                          mode: 1
                      })
                  }
              }
              if (status == 2) {
                  var necesarytruck = $(this).find('.tmp-rdtracking').attr('data-required');
                  var truckingnoo;
                  var valuetrack = $(this).find('.tmp-rdtracking input').val();
                  var insertrack = 1;
                  if (valuetrack.length > 0) {
                      truckingnoo = $(this).find('.tmp-rdtracking input').val();
                      necesarytruck = 0;
                  }
                  if (necesarytruck == 1) {
                      $(this).find('.tmp-rdtracking input').addClass('input-error');
                      truckingnoo = null;
                      insertrack = 0;
                      return;
                  }
                  if (necesarytruck == 0) {
                      truckingnoo = $(this).find('.tmp-rdtracking input').val();
                      if (truckingnoo == "NaN" || truckingnoo == "undefined" || truckingnoo == "") {truckingnoo = "none", insertrack = 0;}
                  }
                  var weightt = $(this).find('.tmp-rdweight input').val();
                  var poo = $(this).find('.tmp-rdnamepo input').val();
                  var podatee = $(this).find('.tmp-rdpodate input').val();
                  if (weightt == "NaN" || weightt == "undefined" || weightt == "") {weightt = 0}
                  if (poo == "NaN" || poo == "undefined" || poo == "") {poo = "none"}
                  if (podatee == "NaN" || podatee == "undefined" || podatee == "") {podatee = '0000-00-00'}
                  receivingDetail.push({
                      status: 2,
                      receivingdetailno: $(this).find('.receivingline').attr('data-rdline'),
                      truckingno: truckingnoo,
                      originalquantity: parseInt($(this).find('.tmp-rdquantity input').val()),
                      unitid: parseInt($(this).find('.tmp-rdselect select').val()),
                      weight: weightt,
                      weightunitid: parseInt($(this).find('.tmp-rdum select').val()),
                      idLocation: parseInt($(this).find('.tmp-rdlocation select').val()),
                      po: poo,
                      podate: podatee,
                      mode: 1,
                      trackinginsert: insertrack
                  })
              }
          }
          else if(mode == 0){
              if (status == 1) {
                  if (changestatus == 1) {
                      var necesarytruck = $(this).find('.rdtracking').attr('data-required');
                      var truckingnoo;
                      var valuetrack = $(this).find('.rdtracking input').val();
                      var insertrack = 1;
                      if (valuetrack.length > 0) {
                          truckingnoo = $(this).find('.rdtracking input').val();
                          necesarytruck = 0;
                      }
                      if (necesarytruck == 1) {
                          $(this).find('.rdtracking input').addClass('input-error');
                          truckingnoo = null;
                          insertrack = 0;
                          return;
                      }
                      if (necesarytruck == 0) {
                          truckingnoo = $(this).find('.rdtracking input').val();
                          if (truckingnoo == "NaN" || truckingnoo == "undefined" || truckingnoo == "") {truckingnoo = "none", insertrack = 0;}
                      }

                      var productidt = $(this).find('.rdpartno').attr('data-productid');
                      if (productidt == "new") {productidt = 0;}

                      var weightt = $(this).find('.rdweight input').val();
                      var origenn = $(this).find('.rdorigen input').val();
                      var lotnumberr = $(this).find('.rdlotnumber input').val();
                      var receivedqtypartt = $(this).find('.rdqtypart input').val();
                      var originalquantityy = $(this).find('.rdquantity input').val();
                      var truckingidd = $(this).find('.rdtracking').attr('data-id');

                      if (weightt == "NaN" || weightt == "undefined" || weightt == "") {weightt = 0}
                      if (origenn == "NaN" || origenn == "undefined" || origenn == "" || origenn == "none") {origenn = "none"}
                      if (lotnumberr == "NaN" || lotnumberr == "undefined" || lotnumberr == "" || lotnumberr == "none") {lotnumberr = "none"}
                      if (receivedqtypartt == "NaN" || receivedqtypartt == "undefined" || receivedqtypartt == "") {receivedqtypartt = 0}
                      if (originalquantityy == "NaN" || originalquantityy == "undefined" || originalquantityy == "") {originalquantityy = 0}
                      if (truckingidd == "NaN" || truckingidd == "undefined" || truckingidd == "" || truckingidd == "null") {truckingidd = 0}

                      receivingDetail.push({
                          status: 1,
                          change: 1,
                          receivingdetailid: $(this).data('rdid'),
                          receivingdetailno: $(this).data('rowrd'),
                          truckingid: truckingidd,
                          truckingno: truckingnoo,
                          truckingnodb: $(this).find('.rdtracking').attr('data-value'),
                          productid: parseInt(productidt),
                          productno: $(this).find('.rdpartno').attr('data-value').toString(),
                          description: $(this).find('.rdpartdescription').attr('data-value').toString(),
                          receivedqtypart: receivedqtypartt,
                          origen: origenn,
                          originalquantity: originalquantityy,
                          unitid: parseInt($(this).find('.rdselect select').val()),
                          weight: weightt,
                          weightunitid: parseInt($(this).find('.rdum select').val()),
                          idLocation: parseInt($(this).find('.rdlocation select').val()),
                          lotnumber: lotnumberr,
                          newProduct: parseInt($(this).find('.rdpartno').attr('data-newproduct')),
                          product: parseInt($(this).find('.rdpartno').attr('data-indexpart')),
                          trackinginsert: insertrack,
                          mode: 0
                      })
                  }
              }
              if (status == 2) {
                  var necesarytruck = $(this).find('.tmp-rdtracking').attr('data-required');
                  var truckingnoo;
                  var valuetrack = $(this).find('.tmp-rdtracking input').val();
                  var insertrack = 1;
                  if (valuetrack.length > 0) {
                      truckingnoo = $(this).find('.tmp-rdtracking input').val();
                      necesarytruck = 0;
                  }
                  if (necesarytruck == 1) {
                      $(this).find('.tmp-rdtracking input').addClass('input-error');
                      truckingnoo = null;
                      insertrack = 0;
                      return;
                  }
                  if (necesarytruck == 0) {
                      truckingnoo = $(this).find('.tmp-rdtracking input').val();
                      if (truckingnoo == "NaN" || truckingnoo == "undefined" || truckingnoo == "") {truckingnoo = "none", insertrack = 0;}
                  }

                  var productidt = $(this).find('.tmp-rdpart').attr('data-productid');
                  if (productidt == "new") {productidt = 0;}

                  var weightt = $(this).find('.tmp-rdweight input').val();
                  var origenn = $(this).find('.tmp-rdorigen input').val();
                  var lotnumberr = $(this).find('.tmp-rdlotnumber input').val();
                  var receivedqtypartt = $(this).find('.tmp-rdqtypart input').val();
                  var productt = $(this).find('.tmp-rdpart').attr('data-product');
                  var productnoo = $(this).find('.tmp-rdpart').attr('data-value');
                  var originalquantityy = $(this).find('.tmp-rdquantity input').val();


                  if (weightt == "NaN" || weightt == "undefined" || weightt == "") {weightt = 0}
                  if (origenn == "NaN" || origenn == "undefined" || origenn == "" || origenn == "none") {origenn = "none"}
                  if (lotnumberr == "NaN" || lotnumberr == "undefined" || lotnumberr == "" || lotnumberr == "none") {lotnumberr = "none"}
                  if (receivedqtypartt == "NaN" || receivedqtypartt == "undefined" || receivedqtypartt == "") {receivedqtypartt = 0}
                  if (productt == "undefined" || productt == "NaN" || productt == "null" || productt == "none") {productt = 0;}
                  if (productnoo == "undefined" || productnoo == "NaN" || productnoo == "null" || productnoo == "none" || productnoo == "") {productnoo = 0;}
                  if (originalquantityy == "NaN" || originalquantityy == "undefined" || originalquantityy == "null" || originalquantityy == "none" || originalquantityy == "") {originalquantityy = 0;}
                  if (productidt == "NaN" || productidt == "undefined" || productidt == "null" || productidt == "none" || productidt == "") {productidt = 0;}
                  //Arreglo para receivingdetails
                  receivingDetail.push({
                      status: 2,
                      receivingdetailno: $(this).find('.receivingline').attr('data-rdline'),
                      truckingno: truckingnoo,
                      productid: parseInt(productidt),
                      productno: productnoo,
                      description: $(this).find('.tmp-rddescription').attr('data-value').toString(),
                      receivedqtypart: receivedqtypartt,
                      origen: origenn,
                      originalquantity: originalquantityy,
                      unitid: parseInt($(this).find('.tmp-rdselect select').val()),
                      weight: weightt,
                      weightunitid: parseInt($(this).find('.tmp-rdum select').val()),
                      idLocation: parseInt($(this).find('.tmp-rdlocation select').val()),
                      lotnumber: lotnumberr,
                      newProduct: parseInt($(this).find('.tmp-rdpart').attr('data-newproduct')),
                      product: parseInt(productt),
                      trackinginsert: insertrack,
                      mode: 0
                  })
              }
          }

      }
  });
}
function updatePo(receivingid){
    // var valuesPO = getArrayUpdaPO();
    $.ajax({
        url: `/ISS/application/index.php/purchaseorder`,
        type: "PUT",
        dataType: 'json',
        data: {
            purchasesOrder: valuesPO,
            receivingId: receivingid
        },
        dataType: 'json',
        success: data =>{
            switch (data.code) {
                case 200:
                    // shownotify(0);
                    break;
                case 500:
                    break;
                default:
            }
        },
        error: function(request,msg,error) {
            console.log(request)
        }
    });
}
function qtyPart(receiptid){
    var mode = $('#customerid option:selected').attr('data-mode');

    $.ajax({
        url: '/ISS/application/index.php/receivingdetail/qtypart',
        type: 'GET',
        dataType: 'json',
        data: {
            receivingdetailId: receiptid
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    var array = data.response.qtyParts;
                    var count = 0;
                    for (var i = 0; i < array.length; i++) {
                        count = array[i].quantity;
                    }
                    var section = $('#rowRd_'+receiptid);
                    if (count == 0) {
                        $('#rowRd_'+receiptid).attr('data-qtypart', count);
                        section.find('.truckingbox').parent().attr('data-required', 1);
                        section.find('.truckingbox').addClass('input-error');
                    }
                    if (count > 0) {
                        $('#rowRd_'+receiptid).attr('data-qtypart', count);
                        $('#rowRd_'+receiptid).find('.rdtracking').attr('data-required', 0);
                    }
                    break;
                default:

            }
        }
    })
}
/*********************Reeceiving Details*********************/
function getRd(){
    var receivingid = $('main').data("receiving-id");
    var tableRd = $('.receipt-details-box > .result-table > .table');
    var noRoRD = 0;
    var mode = $('#customerid option:selected').attr('data-mode');
    tableRd.empty();
    if (receivingid != '') {
        $.ajax({
            url: `/ISS/application/index.php/receivingdetail`,
            type: "GET",
            dataType: 'json',
            data: {
                receivingId: receivingid
            },
            dataType: 'json',
            success: data=>{
                switch (data.code) {
                    case 200:
                    console.log(data);
                    var rowno = data.response.receivingDetail;
                    if (rowno == null) {
                      rowno = 0;
                    }
                    if (rowno == 0) {
                        $('#tablerd').addClass('hide');
                        $('.receipt-details-box .result-table').css({"height":"0px"});
                        $('#foundrd').css({"display":"flex"});
                    }
                    else {
                        $('#tablerd').removeClass('hide');
                        $('#foundrd').css({"display":"none"});
                        tableRd.append(putReceivingDetail({}, 0, true));
                        data.response.receivingDetail.forEach((receivingdetail, index)=>{
                            tableRd.append(putReceivingDetail(receivingdetail, index+1));
                        })
                    }
                    break;
                    case 404:

                    break;
                    default:
                }
            }
        })
        .done(()=>{
            getPackagetype();
            getUnitMesurament();
            getReceivingRows();
            getLocation();
            getPoRD();
            receiptMode(mode);
            loadingreceivingdetails();
            var id = $('#customerid').val();
            getParts(id);
            if (mode == 0) {
                $('.receipt-details-box .table .itemrd').addClass('itemrddefault');
                $('.receipt-details-box table .itemrddefault').each(function(i){
                    var data = $(this).find('.rdpartno input').val();
                    var contentpart = $(this).find('.rdpartno input').val().length;
                    var datatracking = $(this).find('.rdtracking input').val();
                    var contenttracking = $(this).find('.rdtracking input').val().length;
                    var datapartqty = $(this).find('.rdqtypart input').val().length;

                    if (data == "null" || data == "undefined" || data == "NaN" || data == "") {contentpart = 0;}
                    if (datatracking == "null" || datatracking == "undefined" || datatracking == "NaN" || datatracking == "none" || datatracking == "") {contenttracking = 0;}
                    if (contentpart > 0) {$(this).find('.rdpartno').attr('data-asignedpart', 1);}
                    if (datapartqty == 0) {$(this).find('.rdqtypart').attr('data-required', 1)}
                    if (datapartqty > 0) {$(this).find('.rdqtypart').attr('data-required', 0)}
                    if (contentpart == 0) {
                        $(this).find('.rdpartno').attr('data-asignedpart', 0);
                        if (contenttracking == 0) {
                            $(this).find('.rdtracking').attr('data-required', 1);
                        }
                    }
                });
            }
            if (mode == 1) {
                $('.receipt-details-box table tr').each(function(i){
                    if (i != 0) {
                        var value;
                        var status = $(this).attr('data-status');

                        if (status == 1) {
                            var receiptid = $(this).attr('data-rdid');
                            value = $(this).find('.rdtracking input').val();
                            datalength = value.length;
                            if (value == "null" || value == "undefined" || value == "none" || value =="NaN" || value == "") {datalength = 0;}
                            if (datalength > 0) {
                                $(this).attr('data-required', 0);
                            }
                            if (datalength == 0){
                                qtyPart(receiptid);
                            }
                        }
                    }
                });
            }
            $('.receipt-details-box .result-table .table tr').each(function(i){
                if (i != 0) {
                    $('td').each(function(){
                        var data = $(this).find('input').val();

                        if (data == "null" || data == "none" || data == "0000-00-00" || data == 0 || data == null) {
                            $(this).find('input').val('');
                        }
                    })
                }
            });
        });
    }
}
function putProducts(p){
    return `<li data-id="${p.productid}" data-description="${p.description}" class="item-product"><a>${p.productno}</a></li>`;
}
function getParts(id){
    var section = $('.boxProducts');
    section.empty();
    $.ajax({
        url: `/ISS/application/index.php/customer/products`,
        type: "GET",
        dataType: 'json',
        data: {
            customerId: id
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    section.append(putProducts({}));
                    data.response.products.forEach((p)=>{
                        section.append(putProducts(p));
                    })
                    break;
                case 404:

                    break;
                default:
            }
        }
    }).done(()=>{

    });
}

function receiptMode(receiptmode){
    if (receiptmode == 1) {
        $('.default-mode').hide();
        $('.pickandpack-mode').show();
    }
    else if(receiptmode == 0) {
        $('.default-mode').show();
        $('.pickandpack-mode').hide();
    }
}
function putReceivingDetail(receivingdetail, index, first=false){
    if(first){
        return `
        <tr id="receiptDetailsTitles">
            <td>No.</td>
            <td>Delete</td>
            <td>Tracking Number</td>
            <td class="default-mode">Part No.</td>
            <td class="default-mode">Description</td>
            <td class="default-mode">Received Qty Part</td>
            <td class="default-mode">Origen</td>
            <td>Received Package Qty</td>
            <td>Package Type</td>
            <td>Total Weight</td>
            <td>UOM</td>
            <td hidden>Location</td>
            <td class="default-mode">Lot Number</td>
            <td class="pickandpack-mode">PO</td>
            <td class="pickandpack-mode">PO Date</td>
            <td class="pickandpack-mode">Add Details</td>
        </tr>`;
    }
    return`
    <tr class="itemrd sid" data-rdid="${receivingdetail.receivingdetailid}" data-noline="${index}" data-afterline="${index}" data-rid="${receivingdetail.receivingid}" id="rowRd_${receivingdetail.receivingdetailid}" data-rowrd="${index}" data-status="1" data-change="0">
        <td class="noRowRD noline_${index}">${index}</td>
        <td>
            <img src="images/icons/delete.svg" value="${receivingdetail.receivingdetailid}" class="deleteRd btn-action-delete">
        </td>
        <td class="rdtracking" data-required="1" data-value="${receivingdetail.truckingno}" data-id="${receivingdetail.truckingid}" data-columnname="Tracking Number">
            <input id="tracking_${receivingdetail.receivingdetailid}" type="text" value="${receivingdetail.truckingno}" maxlength="20" data-ch="0" class="inputdata truckingbox" placeholder="Please fill"/>
        </td>
        <td class="rdpartno default-mode" data-value="${receivingdetail.productNo}" data-productid="${receivingdetail.productid}" data-columnname="Product No." data-newproduct="0" data-indexpart="0">
            <input id="product_${receivingdetail.receivingdetailid}" data-receivingdid="${receivingdetail.receivingdetailid}" type="text" ng-model="searchpart" value="${receivingdetail.productNo}" maxlength="20" data-ch="0" class="inputdata partinreceipt" placeholder="Please fill"/>
            <img src="images/icons/edit.svg" class="editPartRD btn-action-editPart">
            <div id="products_${index}" class="boxProducts">
                <h3 class="notfoundpart">Not found</h3>
            </div>
        </td>
        <td class="rdpartdescription default-mode" data-value="${receivingdetail.description}" data-columnname="Description">
            <input readonly id="description_${receivingdetail.productid}" type="text" value="${receivingdetail.description}" maxlength="20" data-ch="0" placeholder="Please fill"/>
        </td>
        <td class="rdqtypart default-mode" data-value="${receivingdetail.qtypart}" data-columnname="Received Qty Part">
            <input id="qtypart_${receivingdetail.receivingdetailid}" type="text" value="${receivingdetail.qtypart}" maxlength="20" data-ch="0" class="inputdata" placeholder="Please fill"/>
        </td>
        <td class="rdorigen default-mode" data-value="${receivingdetail.origen}" data-columnname="Origen">
            <input id="origen_${receivingdetail.receivingdetailid}" type="text" value="${receivingdetail.origen}" maxlength="20" data-ch="0" placeholder="Please fill"/>
        </td>
        <td class="rdquantity" data-value="${receivingdetail.originalquantity}" data-columnname="Received Package Qty">
            <input id="rdquantity_${receivingdetail.receivingdetailid}" type="number" min="0" value="${receivingdetail.originalquantity}" maxlength="15" data-ch="0" placeholder="Please fill"/>
        </td>
        <td class="rdselect" data-value="${receivingdetail.unitid}" data-columnname="Package Type">
            <select data-s="1" class="selectPT" data-selected="${receivingdetail.unitid}" id="rdselect_${receivingdetail.receivingdetailid}" data-ch="0">
            </select>
        </td>
        <td class="rdweight" data-value="${receivingdetail.weight}" data-columnname="Total Weight">
            <input type="number" min="0" value="${receivingdetail.weight}" id="rdweight_${receivingdetail.receivingdetailid}" maxlength="15" style="width: 50px;" data-ch="0" placeholder="Please fill"/>
        </td>
        <td class="rdum" data-value="${receivingdetail.weightunitid}" data-columnname="Unit Mesurament">
            <select data-s="1" class="selectUM" data-value="${receivingdetail.weightunitid}" id="rdum_${receivingdetail.receivingdetailid}" data-ch="0">
            </select>
        </td>
        <td class="rdlocation" data-value="${receivingdetail.idLocation}" data-columnname="Location" hidden>
            <select data-s="1" class="selectLO" data-selected=${receivingdetail.idLocation} id="rdlocation_${receivingdetail.receivingdetailid}" data-ch="0">
            </select>
        </td>
        <td class="rdlotnumber default-mode" data-value="${receivingdetail.lotnumber}" data-columnname="Lot Number">
            <input id="lotnumber_${receivingdetail.receivingdetailid}" type="text" value="${receivingdetail.lotnumber}" maxlength="20" data-ch="0" placeholder="Please fill"/>
        </td>
        <td class="rdnamepo pickandpack-mode" data-value="${receivingdetail.po}" data-columnname="PO">
            <input type="text" value="${receivingdetail.po}" id="rdnamepo_${receivingdetail.receivingdetailid}" maxlength="15" data-ch="0" placeholder="Please fill"/>
        </td>
        <td class="rdpodate pickandpack-mode" data-value="${receivingdetail.podate}" data-columnname="PO Date">
            <input type="text" value="${receivingdetail.podate}" id="rdpodate_${receivingdetail.receivingdetailid}" maxlength="15" class="datepicker" data-ch="0" placeholder="Please fill"/>
        </td>
        <td class="global pickandpack-mode" data-receivingdetailid="${receivingdetail.receivingdetailid}">
            <img class="addDetails" src="images/icons/add-detail.svg" data-tipo="global"/>
        </td>
    </tr>`;
}
//
function selectPackage(){
    var actualOption;
    var selectPT;
    $(".receipt-details-box > .result-table > .table tr").each(function(i){
        if (i != 0) {
            selectPT = $(this).find('.selectPT');
            actualOption =  selectPT.data('selected');
             selectPT.val(actualOption);
        }
    });
}
//Escribe los tipo de paquetes
function optionPackageType(type){
    var optionsType = `<option value="${type.unitid}" data-value="${type.unitid}" class="option_${type.unitid}">${type.description}</option>`;
    return optionsType;
}
//Obtiene los tipos de paquetes con el metodo ajax y los itera en la funcion optionpackagetype
function getPackagetype(){
    var selectPT = $('.selectPT');
    var selectPTPart = $('.selectPackType-part');
    selectPT.empty();
    selectPTPart.empty();
    var customerid = $('#customerid option:selected').val();

    $.ajax({
        url: `/ISS/application/index.php/receivingdetail/packagetype`,
        type: "GET",
        dataType: 'json',
        data: {
            customerId: customerid
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    data.response.packageType.forEach((type)=>{
                        selectPT.append(optionPackageType(type));
                        selectPTPart.append(optionPackageType(type));
                    });
                    break;
                case 404:

                    break;
                default:
            }
        }
    }).done(()=>{
        selectPackage();
    });
}
//Escribe los tipo de paquetes
function optionAllPackageType(type){
    var optionsType = `<option value="${type.unitid}" data-value="${type.unitid}" class="option_${type.unitid}">${type.description}</option>`;
    return optionsType;
}
//Obtiene los tipos de paquetes con el metodo ajax y los itera en la funcion optionpackagetype
function getAllPackagetype(){
    var selectPTPart = $('.selectPackType-part');
    selectPTPart.empty();

    $.ajax({
        url: `/ISS/application/index.php/receivingdetail/allpackagetype`,
        type: "GET",
        dataType: 'json',
        success: data=>{
            switch (data.code) {
                case 200:
                    data.response.packageAllType.forEach((type)=>{
                        selectPTPart.append(optionAllPackageType(type));
                    });
                    break;
                case 404:

                    break;
                default:
            }
        }
    }).done(()=>{
    });
}
//Se selecciona el unit mesurement de cada receiving detail
function selectUnitMesurement(){
    var actualOption;
    $(".receipt-details-box > .result-table > .table tr").each(function(i){
        if (i != 0) {
            var selectUM = $(this).find('.selectUM');
            actualOption =  selectUM.data('value');
             selectUM.val(actualOption);
        }
    });
}
//Se escriben en el select las opciones de unit mesurement
function optionUnitMesurament(unitmesurament){
    var optionsUnit = `<option value="${unitmesurament.uomid}" class="unit_${unitmesurament.uomid}">${unitmesurament.description}</option>`;
    return optionsUnit;
}
//Se obtienen los unit mesurement
function getUnitMesurament(){
    var selectUM = $('.selectUM');
    selectUM.empty();
    $.ajax({
        url: `/ISS/application/index.php/receivingdetail/unitmesurament`,
        type: "GET",
        dataType: 'json',
        success: function(data){
            switch (data.code) {
                case 200:
                    selectUM.append(`<option value="0">Select UOM</option>`)
                    data.response.unitMesurament.forEach((unit)=>{
                        selectUM.append(optionUnitMesurament(unit));
                    });
                    break;
                case 404:
                    break;
                default:
            }
        }
    }).done(()=>{
        selectUnitMesurement();
    });
}
//Se selecciona location de un determinado receiving detail
function selectLocation(){
    var actualOption;
    $(".receipt-details-box > .result-table > .table tr").each(function(i){
        if (i != 0) {
            var selectLO = $(this).find('.selectLO');
            actualOption =  selectLO.data('selected');
            selectLO.val(actualOption);
        }
    });
}
//Escribe en el select las opciones para Location
function optionLocation(loc){
    var optionsLocation = `<option value="${loc.idLocation}" data-value="${loc.idLocation}">${loc.location}</option>`;
    return optionsLocation;
}
//Se obtiene Locations
function getLocation(){
    var selectLO = $('.selectLO');
    $.ajax({
        url: `/ISS/application/index.php/receivingdetail/locations`,
        type: "GET",
        dataType: 'json',
        success: data=>{
            switch (data.code) {
                case 200:
                    selectLO.append(`<option value="0">Select Location</option>`);
                    data.response.Locations.forEach((loc)=>{
                        selectLO.append(optionLocation(loc));
                    });
                    break;
                case 404:

                    break;
                default:
            }
        }
    }).done(()=>{
        selectLocation();
    });
}
//Get custom services
function getCustomServices(){
    var selectCustom = $('#service');
    selectCustom.empty();

    var customerid = $('#customerid').val();
    $.ajax({
        url: `/ISS/application/index.php/receiving/servicecustom`,
        type: "GET",
        dataType: 'json',
        data: {
            customerId: customerid
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    selectCustom.append(`<option value="custom0">Select</option>`);
                    data.response.services.forEach((services)=>{
                        selectCustom.append(putcustomservices(services));
                    });
                    break;
                case 404:

                    break;
                default:
            }
        }
    }).done(()=>{

    });
}
function putcustomservices(services){
    return `<option value="${services.serviceid}" data-cost="${services.cost}" data-code="${services.code}" data-variant="${services.variant}" data-baserate="${services.baserate}" data-increment="${services.increment}">${services.description}</option>`;
}
//Get extra cargos
function getExtraCargos(){
    var tableExtraCargos = $('#services_list table');
    tableExtraCargos.empty();

    var receivingid = $('#receivingid_').val();
    $.ajax({
        url: `/ISS/application/index.php/receiving/extracargos`,
        type: "GET",
        dataType: 'json',
        data: {
            receivingId: receivingid
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    tableExtraCargos.append(putextracargos({}, true));
                    data.response.extracargos.forEach((ec)=>{
                        tableExtraCargos.append(putextracargos(ec));
                    });
                    break;
                case 404:

                    break;
                default:
            }
        }
    }).done(()=>{

    });
}
function putextracargos(ec, first=false){
    if (first) {
        return `
            <tr>
                <td>Delete</td>
                <td>Code</td>
                <td>Description</td>
                <td>Cost</td>
            </tr>`;
    }
    return `
        <tr data-status="1" id="charge_${ec.servicereceivingid}" data-id="${ec.servicereceivingid}">
            <td>
                <img src="images/icons/delete.svg" class="btn_delete_cargo">
            </td>
            <td>${ec.code}</td>
            <td>${ec.description}</td>
            <td>$${ec.cost}</td>
        </tr>
    `;
}
function saveReceiving(receivingid){
    purchaseOrder = [];
    receivingDetail = [];
    freightbill = [];
    tande = [];
    receiving = {status: 0};
    extraCargos = [];
    getDataReceiving();
    getArrayExtraCargos();
    getArrayPO();
    getArrayRD();
    getArrayFB();
    getArrayTD();
    for (var i = 0; i < details.length; i++) {
        if (details[i].status == 1) {
            for (var o = 0; o < details.length; o++) {
                if (details[o].status == 0) {
                    if (details[i].receivinglinesid === details[o].receivinglinesid) {
                        console.log("Delete array with ID " + details[o].receivinglinesid);
                        delete details[i];
                        details = details.filter(function(e){ return e === 0 || e });
                    }
                }
            }
        }
    }
    JsonReceiving = {
        receiving,
        purchaseOrder,
        receivingDetail,
        freightbill,
        tande,
        extraCargos,
        document: files,
        product: parts,
        receivingLine: details,
        receivingid: $('main').data("receiving-id")
    };
    console.log(JsonReceiving);
    $.ajax({
      url: '/ISS/application/index.php/receiving/update',
      type: 'PUT',
      dataType: 'json',
      data: JSON.stringify(JsonReceiving),
      success: (data)=>{
        var id = data.response.receivingId;
        window.location.href = 'http://localhost/ISS/receiving_summary.php?receivingid='+id;
      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}
//Funcion que se encarga de verificar si hay datos en los arreglos, de ser así es porque hay modificaciones, eliminaciones o agregacion de nuevos datos
function backSearchReceiving(){
    purchaseOrder = [];
    receivingDetail = [];
    freightbill = [];
    tande = [];
    receiving = [];
    extraCargos = [];
    getArrayPO();
    getArrayRD();
    getArrayFB();
    getArrayTD();
    getDataReceiving();
    getArrayExtraCargos();
    //Si los arreglos estan vacios esto significa que no hay modificaciones y por lo tanto puede regresar hacia atras
    if (purchaseOrder.length == 0 && receivingDetail.length == 0 && freightbill.length == 0 && tande.length == 0 && receiving.length == 0 && extraCargos.length == 0 && details.length == 0 && parts.length == 0) {
        window.location.href = 'receiving_search.php';
    }
    //si hay datos en los arreglos se abrira un dialogo donde se mostraran los cambios hechos sin guardar
    else {
        $('.box-unsaved').addClass('flex');
        $('.data-notsaved').css({"display":"block"});
        $('.data-notsaved').css({"animation":"pop-in","animation-duration":"0.25s"});
        $('.title-warning').html(`
            <div>
                <img src="images/icons/warning.svg">
                <h1>Unsaved Changes Detected</h1>
            </div>
            <h3>Changes made in the following fields will be lost.</h3>
        `);
    }
    //Se verifica si se crearon nuevos receiving lines (detalles de entradas)
    if (details.length > 0) {
        var quantityDetails;
        for (var i = 0; i < details.length; i++) {
            if (details[i].status === 2) {
                quantityDetails = i;
            }
        }
        if (quantityDetails > 0) {
            $('.changes-conts').append(`
                <div class="unsaved-details">
                <p>Created</p>&ensp;<span>${quantityDetails}</span>&ensp;<p>Details</p>
                </div>
            `);
        }
    }
    //Se everifica si se crearon nuevas partes
    if (parts.length > 0) {
        var quantityParts = parts.length;
        $('.changes-conts').append(`
            <div class="unsaved-newpart">
                <p>Created</p>&ensp;<span>${quantityParts}</span>&ensp;<p>Parts</p>
            </div>
        `);
    }
    //Se verifica si se crearon nuevos extra cargos
    if (extraCargos.length > 0) {
        var quantityCargos = extraCargos.length;
        $('.changes-conts').append(`
            <div class="unsaved-cargos">
                <p>Created</p>&ensp;<span>${quantityCargos}</span>&ensp;<p>Extra Charges</p>
            </div>
        `);
    }
    //Se verifica si hay algo en el arreglo de Receiving Details y de ser así se mostraran
    if (receivingDetail.length > 0) {
        var temporalReceivingData = Array();
        temporalReceivingData = receivingDetail;

        for (var i = 0; i < temporalReceivingData.length; i++) {
            //Si hay cambios los escribe
            if (temporalReceivingData[i].status == 1 && temporalReceivingData[i].change == 1) {
                $('.updates-changes').show();
                $('.updates-changes .datard').show();
                $('.updates-changes .data-changes .datard').append(`
                        <div class="title-update">
                            <h3>Line Receipt:</h3>
                            <p>${temporalReceivingData[i].receivingdetailno}</p>
                        </div>
                        <div class="box-updaterd" id="notRD_${temporalReceivingData[i].receivingdetailid}"></div>
                    </div>
                `);
            }
            //Si hay nuevos receiving details agregados los escribe
            if (temporalReceivingData[i].status == 2) {
                $('.add-changes').show();
                $('.add-changes .datard').show();
                $('.add-changes .data-changes .datard').append(`
                        <div class="title-add">
                            <h3>Line Receipt:</h3>
                            <p>${temporalReceivingData[i].receivingdetailno}</p>
                        </div>
                        <div class="box-newaddrd">
                            <div class="append-changes">
                                <p>Name</p>
                                <input type="text" value="${temporalReceivingData[i].truckingno}" readonly>
                            </div>
                        </div>
                    </div>
                `);
            }
            //Se detecta si hay receiving details eliminados y los escribe
            if (temporalReceivingData[i].status == 0) {
                $('.delete-changes').show();
                $('.delete-changes .datard').show();
                $('.delete-changes .data-changes .datard').append(`
                    <div class="box-deleterd">
                        <div class="append-changes">
                            <p>Trucking number</p>
                            <input type="text" value="${temporalReceivingData[i].truckingno}" readonly>
                        </div>
                    </div>
                `);
            }

        }
    }
    if (purchaseOrder.length > 0) {
        var temporalpruchaseOrderData = Array();
        temporalpruchaseOrderData = purchaseOrder;

        for (var i = 0; i < temporalpruchaseOrderData.length; i++) {
            if (temporalpruchaseOrderData[i].status == 1 && temporalpruchaseOrderData[i].change == 1) {
                $('.updates-changes').show();
                $('.updates-changes .datapo').show();
                $('.updates-changes .data-changes .datapo').append(`
                        <div class="title-update">
                            <h3>Line Po:</h3>
                            <p>${temporalpruchaseOrderData[i].line}</p>
                        </div>
                        <div class="box-updatepo" id="notPO_${temporalpruchaseOrderData[i].poid}"></div>
                    </div>
                `);
            }
            if (temporalpruchaseOrderData[i].status == 2) {
                $('.add-changes').show();
                $('.add-changes .datapo').show();
                $('.add-changes .data-changes .datapo').append(`
                        <div class="title-add">
                            <h3>Line Po:</h3>
                            <p>${temporalpruchaseOrderData[i].line}</p>
                        </div>
                        <div class="box-newaddpo">
                            <div class="append-changes">
                                <p>Name</p>
                                <input type="text" value="${temporalpruchaseOrderData[i].po}" readonly>
                            </div>
                        </div>
                    </div>
                `);
            }
            if (temporalpruchaseOrderData[i].status == 0) {
                $('.delete-changes').show();
                $('.delete-changes .datapo').show();
                $('.delete-changes .data-changes .datapo').append(`
                    <div class="box-deletepo">
                        <div class="append-changes">
                            <p>Purchase order</p>
                            <input type="text" value="${temporalpruchaseOrderData[i].po}" readonly>
                        </div>
                    </div>
                `);
            }
        }
    }
    if (freightbill.length > 0) {
        var temporalfreightbillData= Array();
        temporalfreightbillData = freightbill;

        for (var i = 0; i < temporalfreightbillData.length; i++) {
            if (temporalfreightbillData[i].status == 1 && temporalfreightbillData[i].change == 1) {
                $('.updates-changes').show();
                $('.updates-changes .datafb').show();
                $('.updates-changes .data-changes .datafb').append(`
                        <div class="title-update">
                            <h3>Line Freighbtill:</h3>
                            <p>${temporalfreightbillData[i].line}</p>
                        </div>
                        <div class="box-updatefb" id="notFB_${temporalfreightbillData[i].frid}"></div>
                    </div>
                `);
            }
            if (temporalfreightbillData[i].status == 2) {
                $('.add-changes').show();
                $('.add-changes .datafb').show();
                $('.add-changes .data-changes .datafb').append(`
                        <div class="title-add">
                            <h3>Line Freighbtill:</h3>
                            <p>${temporalfreightbillData[i].line}</p>
                        </div>
                        <div class="box-newaddfb">
                            <div class="append-changes">
                                <p>Name</p>
                                <input type="text" value="${temporalfreightbillData[i].fr}" readonly>
                            </div>
                        </div>
                    </div>
                `);
            }
            if (temporalfreightbillData[i].status == 0) {
                $('.delete-changes').show();
                $('.delete-changes .datafb').show();
                $('.delete-changes .data-changes .datafb').append(`
                    <div class="box-deletefb">
                        <div class="append-changes">
                            <p>Freightbill</p>
                            <input type="text" value="${temporalfreightbillData[i].freightbill}" readonly>
                        </div>
                    </div>
                `);
            }
        }
    }
    if (tande.length > 0) {
        var temporaltandeData= Array();
        temporaltandeData = tande;

        for (var i = 0; i < temporaltandeData.length; i++) {
            if (temporaltandeData[i].status == 1 && temporaltandeData[i].change == 1) {
                $('.updates-changes').show();
                $('.updates-changes .datate').show();
                $('.updates-changes .data-changes .datate').append(`
                        <div class="title-update">
                            <h3>Line T and E:</h3>
                            <p>${temporaltandeData[i].line}</p>
                        </div>
                        <div class="box-updatete" id="notTE_${temporaltandeData[i].tandeid}"></div>
                    </div>
                `);
            }
            if (temporaltandeData[i].status == 2) {
                $('.add-changes').show();
                $('.add-changes .datate').show();
                $('.add-changes .data-changes .datate').append(`
                        <div class="title-add">
                            <h3>Line T and E:</h3>
                            <p>${temporaltandeData[i].line}</p>
                        </div>
                        <div class="box-newaddte">
                            <div class="append-changes">
                                <p>Name</p>
                                <input type="text" value="${temporaltandeData[i].tande}" readonly>
                            </div>
                        </div>
                    </div>
                `);
            }
            if (temporaltandeData[i].status == 0) {
                $('.delete-changes').show();
                $('.delete-changes .datate').show();
                $('.delete-changes .data-changes .datate').append(`
                    <div class="box-deletete">
                        <div class="append-changes">
                            <p>T and E</p>
                            <input type="text" value="${temporaltandeData[i].tande}" readonly>
                        </div>
                    </div>
                `);
            }
        }
    }
    //Se obtienen los datos que tienen cambios y se escriben en el contenedor de updates changes.
    //Se comparan las variables dbvalue y current value, de ser diferentes se escribe el cambio.
    //Se obtiene el nombre de la columna al que pertenece dicho cambio y el id para escribirlo en su contenedor unico.
    $('.receipt-details-box .result-table .table tr').map(function(i){
        if (i != 0) {
            $(this).find('td').each(function(){
                    dbvalue = $(this).data("value");
                    currentvalue = $(this).find('.inputdata').val();
                    reid = $(this).parent().attr('data-rdid');
                    columnname = $(this).attr('data-columnname');
                    element = $(this).find('.inputdata').attr('data-s');

                    if (dbvalue != currentvalue) {
                        if (element == 1) {
                            currentvalueS = $(this).find('.inputdata option:selected').text();
                            $('#notRD_'+reid).append(`
                                <div class="append-changes">
                                <p>${columnname}</p>
                                <input type="text" value="${currentvalueS}" readonly>
                                </div>
                            `);
                        }
                        else {
                            $('#notRD_'+reid).append(`
                                <div class="append-changes">
                                <p>${columnname}</p>
                                <input type="text" value="${currentvalue}" readonly>
                                </div>
                            `);
                        }
                    }
            });
        }
    });
    $('.purchase-order-box .result-table .table tr').map(function(i){
        if (i != 0) {
            $(this).find('td').each(function(){
                    dbvalue = $(this).data("value");
                    currentvalue = $(this).find('.inputdata').val();
                    poid = $(this).parent().attr('data-poid');
                    columnname = $(this).attr('data-columnname');
                    element = $(this).find('.inputdata').attr('data-s');

                    if (dbvalue != currentvalue) {
                        //En la variable element se guarda el valor del atributo data-s que se encuentra en los elementos Select
                        //Tiene el valor 1, entonces tomara el texto de la opcion seleccionada. Esto para que no se muestre el value como numero cuando haya texto en el valor
                        if (element == 1) {
                            currentvalueS = $(this).find('.inputdata option:selected').text();
                            $('#notPO_'+poid).append(`
                                <div class="append-changes">
                                    <p>${columnname}</p>
                                    <input type="text" value="${currentvalueS}" readonly>
                                </div>
                            `);
                        }
                        else{
                            $('#notPO_'+poid).append(`
                                <div class="append-changes">
                                    <p>${columnname}</p>
                                    <input type="text" value="${currentvalue}" readonly>
                                </div>
                            `);
                        }
                    }
            });
        }
    });
    $('.freightbill-box .result-table .table tr').map(function(i){
        if (i != 0) {
            $(this).find('td').each(function(){
                    dbvalue = $(this).data("value");
                    currentvalue = $(this).find('.inputdata').val();
                    fbid = $(this).parent().attr('data-fbid');
                    columnname = $(this).attr('data-columnname');

                    if (dbvalue != currentvalue) {
                        $('#notFB_'+fbid).append(`
                            <div class="append-changes">
                                <p>${columnname}</p>
                                <input type="text" value="${currentvalue}" readonly>
                            </div>
                        `);
                    }
            });
        }
    });
    $('.tande-box .result-table .table tr').map(function(i){
        if (i != 0) {
            $(this).find('td').each(function(){
                    dbvalue = $(this).data("value");
                    currentvalue = $(this).find('.inputdata').val();
                    teid = $(this).parent().attr('data-tdid');
                    columnname = $(this).attr('data-columnname');

                    if (dbvalue != currentvalue) {
                        $('#notTE_'+teid).append(`
                            <div class="append-changes">
                                <p>${columnname}</p>
                                <input type="text" value="${currentvalue}" readonly>
                            </div>
                        `);
                    }
            });
        }
    });
    $('.data-notsaved').append(`
        <div class="unsaved-buttons">
            <input type="button" class="button cancel-exit-btn" value="Cancel">
            <div>
                <input type="button" class="button exit-btn" value="Exit anyway">
                <input type="button" class="button save-btn save-receiving" value="Save and exit">
            </div>
        </div>
    `);
    //Add parts

}

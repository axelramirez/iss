var flag = 0;
$('body').on('keyup', '.partinreceipt', function(){
    var no = $(this).parent().parent().attr('data-noline');
    var input, filter, ul, li, a, i;
    var section = 'products_' + no;
    var id = $(this).parent().parent().attr('id');

    $(this).parent().find('.boxProducts').show();
    $(this).parent().find('.boxProducts').attr('data-rowid', id);
    if ($(this).val().length == 0) {
        $(this).parent().find('.boxProducts').hide();
    }
    filter = this.value.toUpperCase();
    div = document.getElementById(section);
    li = div.getElementsByTagName("li");

    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
    flag = 1;
});
$('body').on('click', '.item-product', function(){
    var row = $(this).parent().attr('data-rowid');
    var productno = $(this).find('a').text();
    var productid = $(this).attr('data-id');
    var description = $(this).attr('data-description');

    var section = $('#'+row);
    section.find('.rdpartno').attr('data-value', productno);
    section.find('.rdpartno input').val(productno);
    section.find('.rdpartno').attr('data-productid', productid);
    section.find('.rdpartno').attr('data-newproduct', 0);
    section.find('.rdpartno').attr('data-asignedpart', 1);
    section.find('.rdpartdescription input').val(description);
    section.find('.rdtracking').attr('data-required', 0);

    $(this).parent().hide();
});
if (flag == 1) {
    $('html').click(function(){
        $('.boxProducts').hide();
        $('.receipt-details-box table tr').each(function(i){
            if (i != 0) {
                var data = $(this).find('.rdpartno').attr('data-value');
                var value = $(this).find('.rdpartno input').val();
                if (data != value) {
                    if (data == 'null') {
                        $(this).find('.rdpartno input').val('');
                    }
                }
            }
        });
    })
}

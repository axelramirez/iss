$(document).ready(function() {
    hideSections();
    $('body').on('change', '.importantoption', function(){
        checkSelects();
    });
    $('body').on('click', '.newcarrier', function(){
        checkSelects();
    });
    $('body').on('click', '.newvendor', function(){
        checkSelects();
    });
    $('body').on('click', '.selectvendor', function(){
        checkSelects();
    });
    $('body').on('click', '.selectcarrier', function(){
        checkSelects();
    });
    $('body').on('change', '#vendornew', function(){
        checkSelects();
    });
    $('body').on('change', '#carriernew', function(){
        checkSelects();
    });
});
function checkSelects(){
    var customerSelect = $("#customerid option:selected").val();
    var vendorSelect = $("#vendorid option:selected").val();
    var carrierSelect = $("#carrierid option:selected").val();
    var createnewvendor = $('#vendorid').attr('data-active');
    var createnewcarrier = $('#carrierid').attr('data-active');
    var datanewvendor = $('#vendornew').val();
    var datanewcarrier = $('#carriernew').val();
    console.log(createnewvendor, createnewcarrier);
    if (customerSelect == 'select0' || vendorSelect == 'select0'|| carrierSelect == 'select0'){
        if (customerSelect == 'select0') {hideSections();}
        if (vendorSelect == 'select0') {
            if (createnewvendor == 1) {
                hideSections();
            }
        }
        if (carrierSelect == 'select0') {
            if (createnewcarrier == 1) {
                hideSections();
            }
        }
    }
    if (customerSelect >= 0 && vendorSelect >= 0 && carrierSelect >= 0){
        if (customerSelect >= 0) {showSections();}
    }
    if (customerSelect >= 0 && createnewvendor == 0 && createnewcarrier == 0) {
        if (datanewvendor != '' && datanewcarrier != '') {
            showSections();
        }
    }

}
function hideSections(){
    $('.receipt-details-box').hide();
    $('.purchase-order-box').hide();
    $('.freightbill-box').hide();
    $('.tande-box').hide();
    $('.documents-box').hide();
}
function showSections(){
    $('.receipt-details-box').show();
    $('.purchase-order-box').show();
    $('.freightbill-box').show();
    $('.tande-box').show();
    $('.documents-box').show();
}

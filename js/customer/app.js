var numberpage = 1;
//States
var usa = ['Alaska', 'Alabama', 'Arkansas', 'Arizona', 'California', 'Colorado', 'Connecticut', 'District of Columbia', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Iowa', 'Idaho', 'Illinois', 'Indiana', 'Kansas', 'Kentucky', 'Louisiana', 'Massachusetts', 'Maryland', 'Maine', 'Michigan', 'Minnesota', 'Missouri', 'Mississippi', 'Montana', 'North Carolina', 'North Dakota', 'Nebraska', 'New Hampshire', 'New Jersey', 'New Mexico', 'Nevada', 'New York', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Virginia', 'Vermont', 'Washington', 'Wisconsin', 'West Virginia', 'Wyoming', 'American Samoa', 'Federated States of Micronesia', 'Guam', 'Marshall Islands', 'Northern Mariana Islands', 'Palau', 'Puerto Rico', 'Virgin Islands'];

var mx = ['Aguascalientes', 'Baja California', 'Baja California Sur', 'Campeche', 'Coahuila', 'Colima', 'Chiapas', 'Chihuahua', 'Distrito Federal', 'Durango', 'Guanajuato', 'Guerrero', 'Hidalgo', 'Jalisco', 'Mexico', 'Michoacan', 'Morelos', 'Nayarit', 'Nuevo Leon', 'Oaxaca', 'Puebla', 'Queretaro', 'Quintana Roo', 'San Luis Potosi', 'Sinaloa', 'Sonora', 'Tabasco', 'Tamaulipas', 'Tlaxcala', 'Veracruz', 'Yucatan', 'Zacatecas'];

var ca = ['Alberta', 'British Columbia', 'Manitoba', 'New Brunswick', 'Newfoundland and Labrador', 'Northwest Territories', 'Nova Scotia', 'Nuvavut', 'Ontario', 'Prince Edward Island', 'Quebec', 'Saskatchewan', 'Yukon Territory'];
$(document).ready(function() {
    getCustomer();
    //Sections General info/Mail Conifiguration
    $('.general-info').click(function(){
      $('.general-info').removeClass('desactive-section');
      $('.general-info').addClass('active-section');
      $('.email-configuation').addClass('desactive-section');
      $('.email-configuation').removeClass('active-section');
      $('.customer-configuration').removeClass('active-section');
      $('.customer-configuration').addClass('desactive-section');
      $('#newcustomer').css({"display":"block"});
      $('#customeremail').css({"display":"none"});
      $('#customersettings').css({"display":"none"});
    });
    $('.email-configuation').click(function(){
      $('.email-configuation').removeClass('desactive-section');
      $('.email-configuation').addClass('active-section');
      $('.general-info').addClass('desactive-section');
      $('.general-info').removeClass('active-section');
      $('.customer-configuration').removeClass('active-section');
      $('.customer-configuration').addClass('desactive-section');
      $('#newcustomer').css({"display":"none"});
      $('#customersettings').css({"display":"none"});
      $('#customeremail').css({"display":"block"});
    });
    $('.customer-configuration').click(function(){
      $('.customer-configuration').addClass('active-section');
      $('.customer-configuration').removeClass('desactive-section');
      $('.email-configuation').addClass('desactive-section');
      $('.email-configuation').removeClass('active-section');
      $('.general-info').removeClass('active-section');
      $('.general-info').addClass('desactive-section');
      $('#newcustomer').css({"display":"none"});
      $('#customeremail').css({"display":"none"});
      $('#customersettings').css({"display":"block"});
    });
    $('body').on('click', '.clean-filter', function(){
      $('.clean-filter').hide();
      $('.customersearch').val('');
      $('.search-customer').hide();
      $('.add-customer').show();
    });
    $('body').on('focus', '.customersearch', function(){
        $('.add-customer').hide();
        $('.search-customer').show();
        $('.clean-filter').show();
    });
    $('.customersearch').change(function(){
      var valueFilter = $(this).val();
      $('.clean-filter').show();
      if (valueFilter == '') {
        $('.search-customer').hide();
        $('.add-customer').show();
        $('.clean-filter').hide();
      }
    });
    $('body').on('click', '.search-customer', function(){
      var filter = $('.customersearch').val();
      searchFilter(filter);
    });
    //Abre el dialogo de Create customer
    $('body').on('click', '.add-customer', function(){
        $('.title-dialog h2').text('Create Customer');
        $('.add-customer-box .save-btn').val('Create');
        $('.add-customer-box .save-btn').removeClass('update-customer');
        $('.add-customer-box .save-btn').addClass('insert-customer');
        openCustomerBox();
    });
    $('body').on('click', '.edit-customer img', function(){
        $('.add-customer-box .save-btn').val('Update');
        $('.title-dialog h2').text('Edit Customer');
        var id = $(this).parent().parent().attr('data-id');
        var no = $(this).parent().parent().find('.customer-no').text();
        var name = $(this).parent().parent().find('.customer-name').text();
        var taxid = $(this).parent().parent().find('.customer-taxid').text();
        var address = $(this).parent().parent().find('.customer-address').text();
        var city = $(this).parent().parent().find('.customer-city').text();
        var state = $(this).parent().parent().find('.customer-state').text();
        var zipcode = $(this).parent().parent().find('.customer-zipcode').text();
        var country = $(this).parent().parent().find('.customer-country').text();
        var phone = $(this).parent().parent().find('.customer-phone').text();
        var fax = $(this).parent().parent().find('.customer-fax').text();
        var contact = $(this).parent().parent().find('.customer-contact').text();
        var email = $(this).parent().parent().find('.customer-email').text();
        var destinationname = $(this).parent().parent().attr('data-destinationname');
        var destinationaddress = $(this).parent().parent().attr('data-destinationaddress');
        var sendRecMail = $(this).parent().parent().attr('data-sendRecMail');
        var sendShipMail = $(this).parent().parent().attr('data-sendShipMail');
        var outFile = $(this).parent().parent().attr('data-outFile');
        var receiptMode = $(this).parent().parent().attr('data-receiptMode');
        var labelFormat = $(this).parent().parent().attr('data-labelFormat');
        var ownInventory = $(this).parent().parent().attr('data-ownInventory');
        openCustomerBox();
        $('.customerno input').val(no);
        $('.customername input').val(name);
        $('.destinationname input').val(destinationname);
        $('.destinationaddress input').val(destinationaddress);
        $('.customertax input').val(taxid);
        $('.customeraddress input').val(address);
        $('.customercity input').val(city);
        $('.selectCustomerCountry').val(country);
        selectState(country);
        $('.selectCustomerState').val(state);
        $('.customerzipcode input').val(zipcode);
        $('.customerphone input').val(phone);
        $('.customerfax input').val(fax);
        $('.customercontactname input').val(contact);
        $('.customeremails textarea').val(email);
        $('.update-customer').attr('data-id', id);
        //For label format
        switch (labelFormat) {
          case '0':
            $('#customerFormatDefault').prop('checked', true);
            break;
          case '1':
            $('#customerFormatAlternate').prop('checked', true);
            break;
          default:
        }
        //For customer type
        switch (ownInventory) {
          case '0':
            $('#customerTypeDefault').prop('checked', true);
            break;
          case '1':
            $('#customerTypeWarehouse').prop('checked', true);
            break;
          default:
        }
        //For receipt mode
        switch (receiptMode) {
          case '0':
            $('#customerModeDefault').prop('checked', true);
            break;
          case '1':
            $('#customerModePP').prop('checked', true);
            break;
          default:
        }
        //For receivings
        switch (sendRecMail) {
          case 'F':
            $('#dontsend-r').prop('checked', true);
            break;
          case 'T':
            $('#sendemail-r').prop('checked', true);
            break;
          case 'A':
            $('#sendattachments-r').prop('checked', true);
            break;
          default:
        }
        //For shippings
        switch (sendShipMail) {
          case 'F':
            $('#dontsend-s').prop('checked', true);
            break;
          case 'T':
            $('#sendemail-s').prop('checked', true);
            break;
          case 'A':
            $('#sendattachments-s').prop('checked', true);
            break;
          default:
        }
        //For EDI File
        switch (outFile) {
          case '0':
            $('#customerEdi').prop('checked', false);
            break;
          case '1':
            $('#customerEdi').prop('checked', true);
            break;
          default:
        }
    });
    //Mostrar estados dependiendo del pais
    $('.selectCustomerCountry').change(function(){
      var country = $(this).val();
      $('.selectCustomerState').empty();
      selectState(country);
    });
    //Cierra el dialogo de customer y limpia los inputs
    $('body').on('click', '.close-customer', function(){
        closeCustomerBox();
    });
    $('body').on('click', '.cancel-insert-customer', function(){
        closeCustomerBox();
    });
    $('body').on('click', '.insert-customer', function(e){
        var inpObj = document.getElementById('customerName');
        if (!inpObj.checkValidity()) {
          $('.customername input').addClass('input-error');
          $('.customername input').attr('placeholder', 'Please fill');
          $('.customername input').focus();
        }else {
          e.preventDefault();
          var customername = $('.customername input').val();
          if (customername.length <= 120) {
            insertCustomer();
          }else {
            console.log('Please not edit the max length');
          }
        }
    });
    $('body').on('click', '.update-customer', function(e){
        var inpObj = document.getElementById('customerName');
        if (!inpObj.checkValidity()) {
          $('.customername input').addClass('input-error');
          $('.customername input').attr('placeholder', 'Please fill');
          $('.customername input').focus();
        }else {
          e.preventDefault();
          var customername = $('.customername input').val();
          if (customername.length <= 120) {
            var id = parseInt($(this).attr('data-id'));
            updateCustomer(id);
          }else {
            console.log('Please not edit the max length');
          }
        }
    });
    $('body').on('click', '.next-customer-list', function(){
        numberpage++;
        getCustomer();
    });
    $('body').on('click', '.back-customer-list', function(){
        if (numberpage > 1) {
            numberpage--;
            getCustomer();
        }
    });
    $('body').on('click', '.next-list', function(){
        numberpage++;
        searchFilter();
    });
    $('body').on('click', '.back-list', function(){
        if (numberpage > 1) {
            numberpage--;
            searchFilter();
        }
    });
});
//Muestra los estados dependiendo del pais
function selectState(country){
  switch (country) {
    case 'Canada':
      for (var i = 0; i < ca.length; i++) {
        $('.selectCustomerState').append(`<option value="${ca[i]}">${ca[i]}</option>`);
      }
      break;
    case 'Mexico':
      for (var i = 0; i < mx.length; i++) {
        $('.selectCustomerState').append(`<option value="${mx[i]}">${mx[i]}</option>`);
      }
      break;
    case 'United States':
      for (var i = 0; i < usa.length; i++) {
        $('.selectCustomerState').append(`<option value="${usa[i]}">${usa[i]}</option>`);
      }
      break;
    default:
  }
}
//Clean undefined data talbe customer
function cleanUndefined(){
  $('.customer-box table tr td').map(function(){
    var data = $(this).text();
    var classs = $(this).attr('class');
    if (classs != 'edit-customer') {
      if (data == "null" || data == "none" || data == "0000-00-00" || data == "" || data == "NaN" || data == 0 || data == undefined) {
        $(this).text('');
      }
    }
  });
}
//Abre el contenedor para insertar/actualizar customer
function openCustomerBox(){
    $('.box-shadow').css({"display":"block"});
    $('.add-customer-box').css({"display":"block"});
    $('.add-customer-box').css({"animation":"pop-in","animation-duration":"0.25s"});
    $('.customername input').focus();
}
//Cierra el contenedor para insertar/actualizar customer
function closeCustomerBox(){
    $('.customername input').removeClass('input-error');
    $('.customername input').attr('placeholder', '');
    $('.box-shadow').css({"display":"none"});
    $('.add-customer-box').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.add-customer-box').css({"display":"none"});
    },250);
    $('#customeremail').css({"display":"none"});
    $('#customersettings').css({"display":"none"});
    $('#newcustomer').css({"display":"block"});
    $('.customer-configuration').removeClass('active-section');
    $('.customer-configuration').addClass('desactive-section');
    $('.general-info').removeClass('desactive-section');
    $('.general-info').addClass('active-section');
    $('.email-configuation').addClass('desactive-section');
    $('.email-configuation').removeClass('active-section');
    $('#newcustomer input').val('');
    $('#customeremail textarea').val('');
    $('#customerFormatDefault').prop('checked', true);
    $('#customerTypeDefault').prop('checked', true);
    $('#customerModeDefault').prop('checked', true);
    $('#dontsend-r').prop('checked', true);
    $('#dontsend-s').prop('checked', true);
    $('#customerEdi').prop('checked', false);
    resetClass();
}
//Resetea las clases del contenedor insertar/actualizar customer
function resetClass(){
    $('.add-customer-box .save-btn').removeClass('insert-customer');
    $('.add-customer-box .save-btn').addClass('update-customer');
    $('.selectCustomerCountry').prop('selectedIndex',0);
    $('.selectCustomerState').empty();
}
//Funcion para cambiar clases de los botones de paginacion
function filterClassPagination(){
  $('.action-back-list').removeClass('back-customer-list');
  $('.action-next-list').removeClass('next-customer-list');
  $('.action-back-list').addClass('back-list');
  $('.action-next-list').addClass('next-list');
}
function allClassPagination(){
  $('.action-back-list').removeClass('back-list');
  $('.action-next-list').removeClass('next-list');
  $('.action-back-list').addClass('back-customer-list');
  $('.action-next-list').addClass('next-customer-list');
}
//Funcion para busqueda
function searchFilter(filter){
  var tableCustomer = $('.customer-box table');
  filterClassPagination();
  tableCustomer.empty();
  $.ajax({
      url: `/ISS/application/index.php/customer/search`,
      type: "GET",
      dataType: 'json',
      data: {
          page: numberpage,
          filter: filter
      },
      success: data=>{
          switch (data.code) {
              case 200:
                console.log(data);
                  tableCustomer.append(putCustomer({}, 0, true));
                  data.response.customers.forEach((customer)=>{
                      tableCustomer.append(putCustomer(customer));
                  });
                  var entries = data.response.numberEntries;
                  var pages = data.response.numberPages;
                  $('.entries-customer .entries').html(entries + ' entries');
                  $('.entries-customer .pages').html(pages + ' Page(s)');
                  $('.actual-page p').html('Page ' + numberpage);
                  //Ocultar y mostrar flechas de navegacion
                  if (numberpage == pages) {$('.next-list').hide();}
                  else {$('.next-list').show();}
                  if (numberpage > 1) {$('.back-list').show();}
                  else {$('.back-list').hide();}
                  break;
              case 404:
                  console.log(data);
                  break;
              default:
          }
      }, error: (req, msg, error)=>{
        console.log(req);
      }
  }).done(()=>{
      loadingcatalogs();
      cleanUndefined();
  });
}
//Fucnion para insertar customer
function insertCustomer(){
    var customerno = $('.customerno input').val();
    var customername = $('.customername input').val();
    var customertax = $('.customertax input').val();
    var customeraddress = $('.customeraddress input').val();
    var customercity = $('.customercity input').val();
    var customercountry = $('.selectCustomerCountry option:selected').val();
    var customerstate = $('.selectCustomerState option:selected').val();
    var customerzipcode = $('.customerzipcode input').val();
    var customerphone = $('.customerphone input').val();
    var customerfax = $('.customerfax input').val();
    var customercontactname = $('.customercontactname input').val();
    var customerdestinationname = $('.destinationname input').val();
    var customerdestinationaddress = $('.destinationaddress input').val();
    var customeremails = $('.customeremails textarea').val();
    var receivingemail = $('input[name=receiving]:checked').attr('val');
    var shippingemail = $('input[name=shipping]:checked').attr('val');
    var labelformat = parseInt($('input[name=format]:checked').attr('val'));
    var customertype = parseInt($('input[name=type]:checked').attr('val'));
    var receiptmode = parseInt($('input[name=mode]:checked').attr('val'));
    var edifile = $('#customerEdi').prop('checked');
    if (edifile == true) {
      edifile = 1;
    } else {
      edifile = 0;
    }
    var dataInsert = (
        insertCustomer = {
          customerNo: customerno,
          customerName: customername,
          customerTax: customertax,
          customerAddress: customeraddress,
          customerCity: customercity,
          customerCountry: customercountry,
          customerState: customerstate,
          customerZipcode: customerzipcode,
          customerPhone: customerphone,
          customerFax: customerfax,
          customerContactname: customercontactname,
          customerEmails: customeremails,
          destinationName: customerdestinationname,
          destinationAddress: customerdestinationaddress,
          receivingEmail: receivingemail,
          shippingEmail:  shippingemail,
          labelFormat: labelformat,
          customerType: customertype,
          receiptMode: receiptmode,
          ediFile: edifile
    });

    $.ajax({
      url: '/ISS/application/index.php/customer/insert',
      type: 'PUT',
      dataType: 'json',
      data: {
          insertCustomer: dataInsert
      },
      success: (data)=>{
          console.log(data);
          getCustomer();
          closeCustomerBox();
          shownotify(0);
      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}
//Funcion para actualizar customer
function updateCustomer(id){
  var customerno = $('.customerno input').val();
  var customername = $('.customername input').val();
  var customertax = $('.customertax input').val();
  var customeraddress = $('.customeraddress input').val();
  var customercity = $('.customercity input').val();
  var customercountry = $('.selectCustomerCountry option:selected').val();
  var customerstate = $('.selectCustomerState option:selected').val();
  var customerzipcode = $('.customerzipcode input').val();
  var customerphone = $('.customerphone input').val();
  var customerfax = $('.customerfax input').val();
  var customercontactname = $('.customercontactname input').val();
  var customerdestinationname = $('.destinationname input').val();
  var customerdestinationaddress = $('.destinationaddress input').val();
  var customeremails = $('.customeremails textarea').val();
  var receivingemail = $('input[name=receiving]:checked').attr('val');
  var shippingemail = $('input[name=shipping]:checked').attr('val');
  var labelformat = parseInt($('input[name=format]:checked').attr('val'));
  var customertype = parseInt($('input[name=type]:checked').attr('val'));
  var receiptmode = parseInt($('input[name=mode]:checked').attr('val'));
  var edifile = $('#customerEdi').prop('checked');
  if (edifile == true) {
    edifile = 1;
  } else {
    edifile = 0;
  }
  var dataInsert = (
      insertCustomer = {
        customerNo: customerno,
        customerName: customername,
        customerTax: customertax,
        customerAddress: customeraddress,
        customerCity: customercity,
        customerCountry: customercountry,
        customerState: customerstate,
        customerZipcode: customerzipcode,
        customerPhone: customerphone,
        customerFax: customerfax,
        customerContactname: customercontactname,
        customerEmails: customeremails,
        destinationName: customerdestinationname,
        destinationAddress: customerdestinationaddress,
        receivingEmail: receivingemail,
        shippingEmail:  shippingemail,
        labelFormat: labelformat,
        customerType: customertype,
        receiptMode: receiptmode,
        ediFile: edifile
  });

  $.ajax({
    url: '/ISS/application/index.php/customer/update',
    type: 'PUT',
    dataType: 'json',
    data: {
        updateCustomer: dataInsert,
        customerId: id
    },
    success: (data)=>{
      console.log(data);
        getCustomer();
        closeCustomerBox();
        shownotify(0);
    },
    error: (req, msg, error)=>{
      console.log(req);
      console.log(req.responseText);
    }
  })
}
//Escribe los customer dentro de la tabla
function putCustomer(customer, index, first = false){
    if (first) {
        return `
            <tr>
                <td>Edit</td>
                <td>Customer No.</td>
                <td>Name</td>
                <td>TAX ID</td>
                <td>Address</td>
                <td>City</td>
                <td>State</td>
                <td>Zipcode</td>
                <td>Country</td>
                <td>Phone</td>
                <td>Fax</td>
                <td>Contact</td>
                <td>Email</td>
            </tr>
        `;
    }
    else {
        return `
            <tr id="customer_${customer.customerid}" data-id="${customer.customerid}" data-sendRecMail="${customer.sendRecMail}" data-sendShipMail="${customer.sendShipMail}" data-outFile="${customer.outFile}" data-receiptMode="${customer.receiptMode}" data-labelFormat="${customer.labelFormat}" data-ownInventory="${customer.ownInventory}" data-destinationname="${customer.destinationname}" data-destinationaddress="${customer.destinationaddress}">
                <td class="edit-customer">
                    <img src="images/icons/edit.svg" alt="Edit">
                </td>
                <td class="customer-no">${customer.custno}</td>
                <td class="customer-name">${customer.name}</td>
                <td class="customer-taxid">${customer.taxid}</td>
                <td class="customer-address">${customer.address}</td>
                <td class="customer-city">${customer.city}</td>
                <td class="customer-state">${customer.state}</td>
                <td class="customer-zipcode">${customer.zipcode}</td>
                <td class="customer-country">${customer.country}</td>
                <td class="customer-phone">${customer.phone}</td>
                <td class="customer-fax">${customer.fax}</td>
                <td class="customer-contact">${customer.contact}</td>
                <td class="customer-email">${customer.email}</td>
            </tr>
        `;
    }
}
//Obtiene los customer con el metodo ajax y los itera en la funcion putcustomer
function getCustomer(){
    var tableCustomer = $('.customer-box table');
    tableCustomer.empty();
    $.ajax({
        url: `/ISS/application/index.php/customer/get/p`,
        type: "GET",
        dataType: 'json',
        data: {
            page: numberpage
        },
        success: data=>{
            switch (data.code) {
                case 200:
                  console.log(data);
                    tableCustomer.append(putCustomer({}, 0, true));
                    data.response.customers.forEach((customer)=>{
                        tableCustomer.append(putCustomer(customer));
                    });
                    var entries = data.response.numberEntries;
                    var pages = data.response.numberPages;
                    $('.entries-customer .entries').html(entries + ' entries');
                    $('.entries-customer .pages').html(pages + ' Page(s)');
                    $('.actual-page p').html('Page ' + numberpage);
                    //Ocultar y mostrar flechas de navegacion
                    if (numberpage == pages) {$('.next-customer-list').hide();}
                    else {$('.next-customer-list').show();}
                    if (numberpage > 1) {$('.back-customer-list').show();}
                    else {$('.back-customer-list').hide();}
                    break;
                case 404:
                    console.log(data);
                    break;
                default:
            }
        }, error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{
        loadingcatalogs();
        cleanUndefined();
    });
}

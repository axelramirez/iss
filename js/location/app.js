var numberpage = 1;
$(document).ready(function() {
    getLocations();
    $('body').on('click', '.clean-filter', function(){
      $('.clean-filter').hide();
      $('.locationsearch').val('');
      $('.search-location').hide();
      $('.add-location').show();
    });
    $('body').on('focus', '.locationsearch', function(){
        $('.add-location').hide();
        $('.search-location').show();
        $('.clean-filter').show();
    });
    //Abre el dialogo de Create Location
    $('body').on('click', '.add-location', function(){
        openLocationBox();
    });
    //Cierra el dialogo de service y limpia los inputs
    $('body').on('click', '.close-location', function(){
        closeLocationBox();
    });
    $('body').on('click', '.cancel-insert-location', function(){
        closeLocationBox();
    });
    $('body').on('click', '.insert-location', function(e){
      var inpObj = document.getElementById('locationName');
      if (!inpObj.checkValidity()) {
        $('.locationname input').addClass('input-error');
        $('.locationname input').attr('placeholder', 'Please fill');
        $('.locationname input').focus();
      }else {
        e.preventDefault();
        var locationname = $('.locationname input').val();
        if (locationname.length <= 10) {
          insertLocation();
        }else {
          console.log('Please not edit the max lenght');
        }
      }
    });
    $('body').on('click', '.delete-location img', function(){
        var locationid = $(this).parent().parent().attr('data-id');
        var name = $(this).parent().parent().find('.locationname').text();

        showdialog(()=>{
            deleteLocation(locationid)
        }, 'Are you sure delete location ' + name + '?')
    });
    $('body').on('click', '.search-location', function(){
      var filter = $('.locationsearch').val();
      searchFilter(filter);
    });
    $('body').on('click', '.next-locations-list', function(){
        numberpage++;
        getLocations();
    });
    $('body').on('click', '.back-locations-list', function(){
        if (numberpage > 1) {
            numberpage--;
            getLocations();
        }
    });
    $('body').on('click', '.next-list', function(){
        numberpage++;
        searchFilter();
    });
    $('body').on('click', '.back-list', function(){
        if (numberpage > 1) {
            numberpage--;
            searchFilter();
        }
    });
});
function openLocationBox(){
    $('.box-shadow').css({"display":"block"});
    $('.add-location-box').css({"display":"block"});
    $('.add-location-box').css({"animation":"pop-in","animation-duration":"0.25s"});
}
function closeLocationBox(){
    $('.box-shadow').css({"display":"none"});
    $('.add-location-box').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.add-location-box').css({"display":"none"});
    },250);
    $('.locationname input').val('');
    $('.locationname input').removeClass('input-error');
    $('.locationname input').attr('placeholder', '');
}
//Funcion para eliminar un location
function deleteLocation(locationid){
    $.ajax({
      url: '/ISS/application/index.php/location/delete',
      type: 'DELETE',
      dataType: 'json',
      data: {
          idLocation: locationid
      },
      success: (data)=>{
          switch (data.code) {
              case 200:
                  getLocations();
                  closeLocationBox();
                  shownotify(1);
                  break;
              case 400:
                console.log("Error 400");
                break;
            case 500:
              console.log("Error 500");
              break;
              default:
          }

      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}
//Fucnion para insertar locations
function insertLocation(){
    var location = $('.locationname input').val();
    $.ajax({
      url: '/ISS/application/index.php/location/insert',
      type: 'PUT',
      dataType: 'json',
      data: {
          locationName: location
      },
      success: (data)=>{
          console.log(data);
          getLocations();
          closeLocationBox();
          shownotify(0);
      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}
//Escribe los locations dentro de la tabla
function putLocations(location, index, first = false){
    if (first) {
        return `
            <tr>
                <td style="width: 55px;">Delete</td>
                <td style="width: 150px;">Location</td>
                <td>Created Date</td>
            </tr>
        `;
    }
    else {
        return `
            <tr id="location_${location.idLocation}" data-id="${location.idLocation}">
                <td class="delete-location" style="width: 55px;">
                    <img src="../images/icons/delete.svg" alt="Delete">
                </td>
                <td class="locationname" style="width: 150px;">${location.location}</td>
                <td class="created">${location.createdTimestamp}</td>
            </tr>
        `;
    }
}
//Obtiene los locations con el metodo ajax y los itera en la funcion putLocations
function getLocations(){
    var tableLocations = $('.location-box table');
    tableLocations.empty();
    $.ajax({
        url: `/ISS/application/index.php/locations/get`,
        type: "GET",
        dataType: 'json',
        data:{
            page: numberpage
        },
        success: data=>{
            switch (data.code) {
                case 200:
                    tableLocations.append(putLocations({}, 0, true));
                    data.response.locations.forEach((location)=>{
                        tableLocations.append(putLocations(location));
                    });
                    var entries = data.response.numberEntries;
                    var pages = data.response.numberPages;
                    $('.entries-locations .entries').html(entries + ' entries');
                    $('.entries-locations .pages').html(pages + ' Page(s)');
                    $('.actual-page p').html('Page ' + numberpage);
                    //Ocultar y mostrar flechas de navegacion
                    if (numberpage == pages) {$('.next-locations-list').hide();}
                    else {$('.next-locations-list').show();}
                    if (numberpage > 1) {$('.back-locations-list').show();}
                    else {$('.back-locations-list').hide();}
                    break;
                case 404:
                    console.log(data);
                    break;
                default:
            }
        }, error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{
        loadinglocations();
    });
}
//Funcion para cambiar clases de los botones de paginacion
function filterClassPagination(){
  $('.action-back-list').removeClass('back-locations-list');
  $('.action-next-list').removeClass('next-locations-list');
  $('.action-back-list').addClass('back-list');
  $('.action-next-list').addClass('next-list');
}
function allClassPagination(){
  $('.action-back-list').removeClass('back-list');
  $('.action-next-list').removeClass('next-list');
  $('.action-back-list').addClass('back-location-list');
  $('.action-next-list').addClass('next-location-list');
}
//Funcion para busqueda
function searchFilter(filter){
  var tableLocations = $('.location-box table');
  filterClassPagination();
  tableLocations.empty();
  $.ajax({
      url: `/ISS/application/index.php/location/search`,
      type: "GET",
      dataType: 'json',
      data: {
          page: numberpage,
          filter: filter
      },
      success: data=>{
          switch (data.code) {
              case 200:
                console.log(data);
                  tableLocations.append(putLocations({}, 0, true));
                  data.response.locations.forEach((location)=>{
                      tableLocations.append(putLocations(location));
                  });
                  var entries = data.response.numberEntries;
                  var pages = data.response.numberPages;
                  $('.entries-locations .entries').html(entries + ' entries');
                  $('.entries-locations .pages').html(pages + ' Page(s)');
                  $('.actual-page p').html('Page ' + numberpage);
                  //Ocultar y mostrar flechas de navegacion
                  if (numberpage == pages) {$('.next-list').hide();}
                  else {$('.next-list').show();}
                  if (numberpage > 1) {$('.back-list').show();}
                  else {$('.back-list').hide();}
                  break;
              case 404:
                  console.log(data);
                  break;
              default:
          }
      }, error: (req, msg, error)=>{
        console.log(req);
      }
  }).done(()=>{
      loadinglocations();
  });
}

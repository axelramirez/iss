$(document).ready(function() {
    $('.dataTables_processing').hide();
    var customerid = $('#customerid').val();
    // console.log(customerid);
    table = $('#ProductTable').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":{
            url :"queries/getPartsList.php", // json datasource
            type: "post",  // method, by default get
            data: {customerid:customerid},
            error: function (xhr, error, thrown) {
                $(".ProductTable-error").html("");
                $("#ProductTable").append('<tr class="not-foundparts">No data found in the server</tr>');
                $("#ProductTable_processing").css("display","none");
                // Display error info in console.
                console.log(xhr);
                console.log(error);
                console.log(thrown);
            }
        },
        // Sort by created date descending.
        "order": [[ 3, 'ASC' ]],
        "columnDefs": [
            {className: "dt-body-center", "targets": [0]},
            {"targets": 0, "orderable": false}
        ]
    });
    if (parts.length > 0) {
        $('.not-foundparts').hide();

        for (var i = 0; i < parts.length; i++) {
            if (parts[i].customerid == customerid) {
                $('.search-part-detail').css({"height":"750px"});
                $('.newparts-box-table').css({"display":"block"});
                $('#newparts-table').append(`
                    <tr data-status="2">
                        <td class="productName" data-index="${[i]}">${parts[i].productno}</td>
                        <td>${parts[i].name}</td>
                        <td class="descr">${parts[i].description}</td>
                    </tr>
                    `);
                }
            }
        }
});
function SetValues(id){
    $('#text_description',window.parent.document).val($('#desc_'+id).text());
    $('#text_productid',window.parent.document).val(id);
    $('#text_productName',window.parent.document).val($('#prodname_'+id).text());
}
function AllCellClick(id){
    $('input[name="productid"]').val([id]);
    SetValues(id);
}

var numberpage = 1;

$(document).ready(function() {
    getCarrier();
    $('body').on('focus', '.carriersearch', function(){
      $('.add-carrier').hide();
      $('.search-carrier').show();
      $('.clean-filter').show();
    });
    $('body').on('click', '.clean-filter', function(){
      $('.clean-filter').hide();
      $('.carriersearch').val('');
      $('.search-carrier').hide();
      $('.add-carrier').show();
    });
    $('.carriersearch').change(function(){
      var valueFilter = $(this).val();
      $('.clean-filter').show();
      if (valueFilter == '') {
        $('.search-carrier').hide();
        $('.add-carrier').show();
        $('.clean-filter').hide();
      }
    });
    $('body').on('click', '.search-carrier', function(){
      var filter = $('.carriersearch').val();
      searchFilter(filter);
    });
    //Abre el dialogo de Create Vendor
    $('body').on('click', '.add-carrier', function(){
        $('.title-dialog h2').text('Create Carrier');
        $('.add-carrier-box .save-btn').val('Create');
        $('.add-carrier-box .save-btn').removeClass('update-carrier');
        $('.add-carrier-box .save-btn').addClass('insert-carrier');
        openCarrierBox();
    });
    $('body').on('click', '.edit-carrier img', function(){
        $('.title-dialog h2').text('Edit Carrier');
        var id = $(this).parent().parent().attr('data-id');
        var no = $(this).parent().parent().find('.carrier-no').text();
        var name = $(this).parent().parent().find('.carrier-name').text();
        openCarrierBox();
        $('.carrierno input').val(no);
        $('.carriername input').val(name);
        $('.update-carrier').attr('data-id', id);
    });
    //Cierra el dialogo de vendor y limpia los inputs
    $('body').on('click', '.close-carrier', function(){
        closeCarrierBox();
    });
    $('body').on('click', '.cancel-insert-carrier', function(){
        closeCarrierBox();
    });
    $('body').on('click', '.insert-carrier', function(e){
        var inpObj = document.getElementById('carrierName');
        if (!inpObj.checkValidity()) {
          $('.carriername input').addClass('input-error');
          $('.carriername input').attr('placeholder', 'Please fill');
          $('.carriername input').focus();
        }else {
          e.preventDefault();
          var carriername = $('.carriername input').val();
          if (carriername.length <= 50) {
            insertCarrier();
          }else {
            console.log('Please not edit the max length');
          }
        }
    });
    $('body').on('click', '.update-carrier', function(e){
      var inpObj = document.getElementById('carrierName');
      if (!inpObj.checkValidity()) {
        $('.carriername input').addClass('input-error');
        $('.carriername input').attr('placeholder', 'Please fill');
        $('.carriername input').focus();
      }else {
        e.preventDefault();
        var carriername = $('.carriername input').val();
        if (carriername.length <= 50) {
          var id = parseInt($(this).attr('data-id'));
          updateCarrier(id);
        }
        else {
          console.log('Please not edit the max length');
        }
      }
    });
    $('body').on('click', '.next-carrier-list', function(){
        numberpage++;
        getCarrier();
    });
    $('body').on('click', '.back-carrier-list', function(){
        if (numberpage > 1) {
            numberpage--;
            getCarrier();
        }
    });
    $('body').on('click', '.next-list', function(){
        numberpage++;
        searchFilter();
    });
    $('body').on('click', '.back-list', function(){
        if (numberpage > 1) {
            numberpage--;
            searchFilter();
        }
    });
});
//Clean undefined data talbe customer
function cleanUndefined(){
  $('.carrier-box table tr td').map(function(){
    var data = $(this).text();
    var classs = $(this).attr('class');
    if (classs != 'edit-carrier') {
      if (data == "null" || data == "none" || data == "0000-00-00" || data == "" || data == "NaN" || data == 0 || data == undefined) {
        $(this).text('');
      }
    }
  });
}
//Abre el contenedor para insertar/actualizar vendor
function openCarrierBox(){
    $('.box-shadow').css({"display":"block"});
    $('.add-carrier-box').css({"display":"block"});
    $('.add-carrier-box').css({"animation":"pop-in","animation-duration":"0.25s"});
    $('.carriername input').focus();
}
//Cierra el contenedor para insertar/actualizar unit
function closeCarrierBox(){
    $('.box-shadow').css({"display":"none"});
    $('.add-carrier-box').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.add-carrier-box').css({"display":"none"});
    },250);
    $('.item-carrier input').val('');
    $('.carriername input').removeClass('input-error');
    $('.carriername input').attr('placeholder', '');
    resetClass();
}
//Resetea las clases del contenedor insertar/actualizar carrier
function resetClass(){
    $('.add-carrier-box .save-btn').removeClass('insert-carrier');
    $('.add-carrier-box .save-btn').addClass('update-carrier');
}
//Funcion para cambiar clases de los botones de paginacion
function filterClassPagination(){
  $('.action-back-list').removeClass('back-carrier-list');
  $('.action-next-list').removeClass('next-carrier-list');
  $('.action-back-list').addClass('back-list');
  $('.action-next-list').addClass('next-list');
}
function allClassPagination(){
  $('.action-back-list').removeClass('back-list');
  $('.action-next-list').removeClass('next-list');
  $('.action-back-list').addClass('back-carrier-list');
  $('.action-next-list').addClass('next-carrier-list');
}
//Funcion para busqueda
function searchFilter(filter){
  var tableCarrier = $('.carrier-box table');
  filterClassPagination();
  tableCarrier.empty();
  $.ajax({
      url: `/ISS/application/index.php/carrier/search`,
      type: "GET",
      dataType: 'json',
      data: {
          page: numberpage,
          filter: filter
      },
      success: data=>{
          switch (data.code) {
              case 200:
                console.log(data);
                  tableCarrier.append(putCarrier({}, 0, true));
                  data.response.carriers.forEach((carrier)=>{
                      tableCarrier.append(putCarrier(carrier));
                  });
                  var entries = data.response.numberEntries;
                  var pages = data.response.numberPages;
                  $('.entries-carrier .entries').html(entries + ' entries');
                  $('.entries-carrier .pages').html(pages + ' Page(s)');
                  $('.actual-page p').html('Page ' + numberpage);
                  //Ocultar y mostrar flechas de navegacion
                  if (numberpage == pages) {$('.next-list').hide();}
                  else {$('.next-list').show();}
                  if (numberpage > 1) {$('.back-list').show();}
                  else {$('.back-list').hide();}
                  break;
              case 404:
                  console.log(data);
                  break;
              default:
          }
      }, error: (req, msg, error)=>{
        console.log(req);
      }
  }).done(()=>{
      loadingcatalogs();
  });
}
//Fucnion para insertar carrier
function insertCarrier(){
    var carrierno = $('.carrierno input').val();
    var carriername = $('.carriername input').val();
    var dataInsert = (
        insertCarrier = {
          carrierNo: carrierno,
          carrierName: carriername
    });
    $.ajax({
      url: '/ISS/application/index.php/carrier/insert',
      type: 'PUT',
      dataType: 'json',
      data: {
          insertCarrier: dataInsert
      },
      success: (data)=>{
          getCarrier();
          closeCarrierBox();
          shownotify(0);
      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}
//Funcion para actualizar vendor
function updateCarrier(id){
  var carrierno = $('.carrierno input').val();
  var carriername = $('.carriername input').val();
  var dataInsert = (
      updateCarrier = {
        carrierNo: carrierno,
        carrierName: carriername
  });
  $.ajax({
    url: '/ISS/application/index.php/carrier/update',
    type: 'PUT',
    dataType: 'json',
    data: {
        updateCarrier: dataInsert,
        carrierId: id
    },
    success: (data)=>{
      console.log(data);
        getCarrier();
        closeCarrierBox();
        shownotify(0);
    },
    error: (req, msg, error)=>{
      console.log(req);
      console.log(req.responseText);
    }
  })
}
//Escribe los carriers dentro de la tabla
function putCarrier(carrier, index, first = false){
    if (first) {
        return `
            <tr>
                <td>Edit</td>
                <td>Carrier No.</td>
                <td>Name</td>
            </tr>
        `;
    }
    else {
        return `
            <tr id="carrier_${carrier.carrierid}" data-id="${carrier.carrierid}">
                <td class="edit-carrier">
                    <img src="images/icons/edit.svg" alt="Edit">
                </td>
                <td class="carrier-no">${carrier.carrierno}</td>
                <td class="carrier-name">${carrier.name}</td>
            </tr>
        `;
    }
}
//Obtiene los carrier con el metodo ajax y los itera
function getCarrier(){
    var tableCarrier = $('.carrier-box table');
    tableCarrier.empty();
    $.ajax({
        url: `/ISS/application/index.php/carrier/get/p`,
        type: "GET",
        dataType: 'json',
        data: {
            page: numberpage
        },
        success: data=>{
            switch (data.code) {
                case 200:
                  console.log(data);
                    tableCarrier.append(putCarrier({}, 0, true));
                    data.response.carriers.forEach((carrier)=>{
                        tableCarrier.append(putCarrier(carrier));
                    });
                    var entries = data.response.numberEntries;
                    var pages = data.response.numberPages;
                    $('.entries-carrier .entries').html(entries + ' entries');
                    $('.entries-carrier .pages').html(pages + ' Page(s)');
                    $('.actual-page p').html('Page ' + numberpage);
                    //Ocultar y mostrar flechas de navegacion
                    if (numberpage == pages) {$('.next-carrier-list').hide();}
                    else {$('.next-carrier-list').show();}
                    if (numberpage > 1) {$('.back-carrier-list').show();}
                    else {$('.back-carrier-list').hide();}
                    break;
                case 404:
                    console.log(data);
                    break;
                default:
            }
        }, error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{
        loadingcatalogs();
        cleanUndefined();
    });
}

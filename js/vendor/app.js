var numberpage = 1;
//States
var usa = ['Alaska', 'Alabama', 'Arkansas', 'Arizona', 'California', 'Colorado', 'Connecticut', 'District of Columbia', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Iowa', 'Idaho', 'Illinois', 'Indiana', 'Kansas', 'Kentucky', 'Louisiana', 'Massachusetts', 'Maryland', 'Maine', 'Michigan', 'Minnesota', 'Missouri', 'Mississippi', 'Montana', 'North Carolina', 'North Dakota', 'Nebraska', 'New Hampshire', 'New Jersey', 'New Mexico', 'Nevada', 'New York', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Virginia', 'Vermont', 'Washington', 'Wisconsin', 'West Virginia', 'Wyoming', 'American Samoa', 'Federated States of Micronesia', 'Guam', 'Marshall Islands', 'Northern Mariana Islands', 'Palau', 'Puerto Rico', 'Virgin Islands'];

var mx = ['Aguascalientes', 'Baja California', 'Baja California Sur', 'Campeche', 'Coahuila', 'Colima', 'Chiapas', 'Chihuahua', 'Distrito Federal', 'Durango', 'Guanajuato', 'Guerrero', 'Hidalgo', 'Jalisco', 'Mexico', 'Michoacan', 'Morelos', 'Nayarit', 'Nuevo Leon', 'Oaxaca', 'Puebla', 'Queretaro', 'Quintana Roo', 'San Luis Potosi', 'Sinaloa', 'Sonora', 'Tabasco', 'Tamaulipas', 'Tlaxcala', 'Veracruz', 'Yucatan', 'Zacatecas'];

var ca = ['Alberta', 'British Columbia', 'Manitoba', 'New Brunswick', 'Newfoundland and Labrador', 'Northwest Territories', 'Nova Scotia', 'Nuvavut', 'Ontario', 'Prince Edward Island', 'Quebec', 'Saskatchewan', 'Yukon Territory'];

$(document).ready(function() {
    getVendor();
    $('body').on('focus', '.vendorsearch', function(){
        $('.add-vendor').hide();
        $('.search-vendor').show();
    });
    $('.vendorsearch').change(function(){
      var valueFilter = $(this).val();
      $('.clean-filter').show();
      if (valueFilter == '') {
        $('.search-vendor').hide();
        $('.add-vendor').show();
        $('.clean-filter').hide();
      }
    });
    $('body').on('click', '.search-vendor', function(){
      var filter = $('.vendorsearch').val();
      searchFilter(filter);
    });
    $('body').on('click', '.clean-filter', function(){
      $('.clean-filter').hide();
      $('.vendorsearch').val('');
      $('.search-vendor').hide();
      $('.add-vendor').show();
    });
    $('body').on('focus', '.vendorsearch', function(){
        $('.add-vendor').hide();
        $('.search-vendor').show();
        $('.clean-filter').show();
    });
    //Abre el dialogo de Create Vendor
    $('body').on('click', '.add-vendor', function(){
        $('.title-dialog h2').text('Create Vendor');
        $('.add-vendor-box .save-btn').val('Create');
        $('.add-vendor-box .save-btn').removeClass('update-vendor');
        $('.add-vendor-box .save-btn').addClass('insert-vendor');
        openVendorBox();
    });
    $('body').on('click', '.edit-vendor img', function(){
        $('.title-dialog h2').text('Edit Vendor');
        var id = $(this).parent().parent().attr('data-id');
        var no = $(this).parent().parent().find('.vendor-no').text();
        var name = $(this).parent().parent().find('.vendor-name').text();
        var address = $(this).parent().parent().find('.vendor-address').text();
        var city = $(this).parent().parent().find('.vendor-city').text();
        var state = $(this).parent().parent().find('.vendor-state').text();
        var zipcode = $(this).parent().parent().find('.vendor-zipcode').text();
        var country = $(this).parent().parent().find('.vendor-country').text();
        var phone = $(this).parent().parent().find('.vendor-phone').text();
        var fax = $(this).parent().parent().find('.vendor-fax').text();
        var contact = $(this).parent().parent().find('.vendor-contact').text();
        var email = $(this).parent().parent().find('.vendor-email').text();
        openVendorBox();
        $('.vendorno input').val(no);
        $('.vendorname input').val(name);
        $('.vendoraddress input').val(address);
        $('.vendorcity input').val(city);
        $('.selectVendorCountry').val(country);
        selectState(country);
        $('.selectVendorState').val(state);
        $('.vendorzipcode input').val(zipcode);
        $('.vendorphone input').val(phone);
        $('.vendorfax input').val(fax);
        $('.vendorcontactname input').val(contact);
        $('.vendoremail input').val(email);
        $('.update-vendor').attr('data-id', id);
    });
    //Mostrar estados dependiendo del pais
    $('.selectVendorCountry').change(function(){
      var country = $(this).val();
      $('.selectVendorState').empty();
      selectState(country)
    })
    //Cierra el dialogo de vendor y limpia los inputs
    $('body').on('click', '.close-vendor', function(){
        closeVendorBox();
    });
    $('body').on('click', '.cancel-insert-vendor', function(){
        closeVendorBox();
    });
    $('body').on('click', '.insert-vendor', function(e){
      var inpObj = document.getElementById('vendorName');
      if (!inpObj.checkValidity()) {
        $('.vendorname input').addClass('input-error');
        $('.vendorname input').attr('placeholder', 'Please fill');
        $('.vendorname input').focus();
      }else {
        e.preventDefault();
        var vendorname = $('.vendorname input').val();
        if (vendorname.length <= 50) {
          insertVendor();
        }else {
          console.log('Please not edit the max length');
        }
      }
    });
    $('body').on('click', '.update-vendor', function(e){
        var inpObj = document.getElementById('vendorName');
        if (!inpObj.checkValidity()) {
          $('.vendorname input').addClass('input-error');
          $('.vendorname input').attr('placeholder', 'Please fill');
          $('.vendorname input').focus();
        }else {
          e.preventDefault();
          var vendorname = $('.vendorname input').val();
          if (vendorname.length <= 50) {
            var id = parseInt($(this).attr('data-id'));
            updateVendor(id);
          }else {
            console.log('Please not edit the max length');
          }
        }
    });
    $('body').on('click', '.next-vendor-list', function(){
        numberpage++;
        getVendor();
    });
    $('body').on('click', '.back-vendor-list', function(){
        if (numberpage > 1) {
            numberpage--;
            getVendor();
        }
    });
    $('body').on('click', '.next-list', function(){
        numberpage++;
        searchFilter();
    });
    $('body').on('click', '.back-list', function(){
        if (numberpage > 1) {
            numberpage--;
            searchFilter();
        }
    });
});
//Muestra los estados dependiendo del pais
function selectState(country){
  switch (country) {
    case 'Canada':
      for (var i = 0; i < ca.length; i++) {
        $('.selectVendorState').append(`<option value="${ca[i]}">${ca[i]}</option>`);
      }
      break;
    case 'Mexico':
      for (var i = 0; i < mx.length; i++) {
        $('.selectVendorState').append(`<option value="${mx[i]}">${mx[i]}</option>`);
      }
      break;
    case 'United States':
      for (var i = 0; i < usa.length; i++) {
        $('.selectVendorState').append(`<option value="${usa[i]}">${usa[i]}</option>`);
      }
      break;
    default:
  }
}
//Clean undefined data talbe customer
function cleanUndefined(){
  $('.vendor-box table tr td').map(function(){
    var data = $(this).text();
    var classs = $(this).attr('class');
    if (classs != 'edit-vendor') {
      if (data == "null" || data == "none" || data == "0000-00-00" || data == "" || data == "NaN" || data == 0 || data == undefined) {
        $(this).text('');
      }
    }
  });
}
//Abre el contenedor para insertar/actualizar vendor
function openVendorBox(){
    $('.box-shadow').css({"display":"block"});
    $('.add-vendor-box').css({"display":"block"});
    $('.add-vendor-box').css({"animation":"pop-in","animation-duration":"0.25s"});
    $('.vendorname input').focus();
}
//Cierra el contenedor para insertar/actualizar unit
function closeVendorBox(){
    $('.box-shadow').css({"display":"none"});
    $('.add-vendor-box').css({"animation":"pop-out","animation-duration":"0.25s"});
    setTimeout(function(){
        $('.add-vendor-box').css({"display":"none"});
    },250);
    $('.item-vendor input').val('');
    $('.vendorname input').removeClass('input-error');
    $('.vendorname input').attr('placeholder', '');
    resetClass();
}
//Resetea las clases del contenedor insertar/actualizar unit
function resetClass(){
    $('.add-vendor-box .save-btn').removeClass('insert-vendor');
    $('.add-vendor-box .save-btn').addClass('update-vendor');
    $('.selectVendorCountry').prop('selectedIndex',0);
    $('.selectVendorState').empty();
}
//Funcion para cambiar clases de los botones de paginacion
function filterClassPagination(){
  $('.action-back-list').removeClass('back-vendor-list');
  $('.action-next-list').removeClass('next-vendor-list');
  $('.action-back-list').addClass('back-list');
  $('.action-next-list').addClass('next-list');
}
function allClassPagination(){
  $('.action-back-list').removeClass('back-list');
  $('.action-next-list').removeClass('next-list');
  $('.action-back-list').addClass('back-vendor-list');
  $('.action-next-list').addClass('next-vendor-list');
}
//Funcion para busqueda
function searchFilter(filter){
  var tableVendor = $('.vendor-box table');
  filterClassPagination();
  tableVendor.empty();
  $.ajax({
      url: `/ISS/application/index.php/vendor/search`,
      type: "GET",
      dataType: 'json',
      data: {
          page: numberpage,
          filter: filter
      },
      success: data=>{
          switch (data.code) {
              case 200:
                console.log(data);
                  tableVendor.append(putVendor({}, 0, true));
                  data.response.vendors.forEach((vendor)=>{
                      tableVendor.append(putVendor(vendor));
                  });
                  var entries = data.response.numberEntries;
                  var pages = data.response.numberPages;
                  $('.entries-vendor .entries').html(entries + ' entries');
                  $('.entries-vendor .pages').html(pages + ' Page(s)');
                  $('.actual-page p').html('Page ' + numberpage);
                  //Ocultar y mostrar flechas de navegacion
                  if (numberpage == pages) {$('.next-list').hide();}
                  else {$('.next-list').show();}
                  if (numberpage > 1) {$('.back-list').show();}
                  else {$('.back-list').hide();}
                  break;
              case 404:
                  console.log(data);
                  break;
              default:
          }
      }, error: (req, msg, error)=>{
        console.log(req);
      }
  }).done(()=>{
      loadingcatalogs();
  });
}
//Fucnion para insertar vendor
function insertVendor(){
    var vendorno = $('.vendorno input').val();
    var vendorname = $('.vendorname input').val();
    var vendoraddress = $('.vendoraddress input').val();
    var vendorcity = $('.vendorcity input').val();
    var vendorcountry = $('.selectVendorCountry option:selected').val();
    var vendorstate = $('.selectVendorState option:selected').val();
    var vendorzipcode = $('.vendorzipcode input').val();
    var vendorphone = $('.vendorphone input').val();
    var vendorfax = $('.vendorfax input').val();
    var vendorcontactname = $('.vendorcontactname input').val();
    var vendoremail = $('.vendoremail input').val();
    var dataInsert = (
        insertVendor = {
          vendorNo: vendorno,
          vendorName: vendorname,
          vendorAddress: vendoraddress,
          vendorName: vendorname,
          vendorCity: vendorcity,
          vendorCountry: vendorcountry,
          vendorState: vendorstate,
          vendorZipcode: vendorzipcode,
          vendorPhone: vendorphone,
          vendorFax: vendorfax,
          vendorContactname: vendorcontactname,
          vendorEmail: vendoremail
    });
    $.ajax({
      url: '/ISS/application/index.php/vendor/insert/p',
      type: 'PUT',
      dataType: 'json',
      data: {
          insertVendor: dataInsert
      },
      success: (data)=>{
          getVendor();
          closeVendorBox();
          shownotify(0);
      },
      error: (req, msg, error)=>{
        console.log(req);
        console.log(req.responseText);
      }
    })
}
//Funcion para actualizar vendor
function updateVendor(id){
  var vendorno = $('.vendorno input').val();
  var vendorname = $('.vendorname input').val();
  var vendoraddress = $('.vendoraddress input').val();
  var vendorcity = $('.vendorcity input').val();
  var vendorcountry = $('.selectVendorCountry option:selected').val();
  var vendorstate = $('.selectVendorState option:selected').val();
  var vendorzipcode = $('.vendorzipcode input').val();
  var vendorphone = $('.vendorphone input').val();
  var vendorfax = $('.vendorfax input').val();
  var vendorcontactname = $('.vendorcontactname input').val();
  var vendoremail = $('.vendoremail input').val();
  var dataInsert = (
      updateVendor = {
        vendorNo: vendorno,
        vendorName: vendorname,
        vendorAddress: vendoraddress,
        vendorName: vendorname,
        vendorCity: vendorcity,
        vendorCountry: vendorcountry,
        vendorState: vendorstate,
        vendorZipcode: vendorzipcode,
        vendorPhone: vendorphone,
        vendorFax: vendorfax,
        vendorContactname: vendorcontactname,
        vendorEmail: vendoremail
  });
  $.ajax({
    url: '/ISS/application/index.php/vendor/update/p',
    type: 'PUT',
    dataType: 'json',
    data: {
        updateVendor: dataInsert,
        vendorId: id
    },
    success: (data)=>{
      console.log(data);
        getVendor();
        closeVendorBox();
        shownotify(0);
    },
    error: (req, msg, error)=>{
      console.log(req);
      console.log(req.responseText);
    }
  })
}
//Escribe los uoms dentro de la tabla
function putVendor(vendor, index, first = false){
    if (first) {
        return `
            <tr>
                <td>Edit</td>
                <td>Vendor No.</td>
                <td>Name</td>
                <td>Address</td>
                <td>City</td>
                <td>State</td>
                <td>Zipcode</td>
                <td>Country</td>
                <td>Phone</td>
                <td>Fax</td>
                <td>Contact</td>
                <td>Email</td>
            </tr>
        `;
    }
    else {
        return `
            <tr id="vendor_${vendor.vendorid}" data-id="${vendor.vendorid}">
                <td class="edit-vendor">
                    <img src="images/icons/edit.svg" alt="Edit">
                </td>
                <td class="vendor-no">${vendor.vendorno}</td>
                <td class="vendor-name">${vendor.name}</td>
                <td class="vendor-address">${vendor.address}</td>
                <td class="vendor-city">${vendor.city}</td>
                <td class="vendor-state">${vendor.state}</td>
                <td class="vendor-zipcode">${vendor.zipcode}</td>
                <td class="vendor-country">${vendor.country}</td>
                <td class="vendor-phone">${vendor.phone}</td>
                <td class="vendor-fax">${vendor.fax}</td>
                <td class="vendor-contact">${vendor.contact}</td>
                <td class="vendor-email">${vendor.contactemail}</td>
            </tr>
        `;
    }
}
//Obtiene los uoms con el metodo ajax y los itera en la funcion putUom
function getVendor(){
    var tableVendor = $('.vendor-box table');
    tableVendor.empty();
    $.ajax({
        url: `/ISS/application/index.php/vendor/get/p`,
        type: "GET",
        dataType: 'json',
        data: {
            page: numberpage
        },
        success: data=>{
            switch (data.code) {
                case 200:
                  console.log(data);
                    tableVendor.append(putVendor({}, 0, true));
                    data.response.vendors.forEach((vendor)=>{
                        tableVendor.append(putVendor(vendor));
                    });
                    var entries = data.response.numberEntries;
                    var pages = data.response.numberPages;
                    $('.entries-vendor .entries').html(entries + ' entries');
                    $('.entries-vendor .pages').html(pages + ' Page(s)');
                    $('.actual-page p').html('Page ' + numberpage);
                    //Ocultar y mostrar flechas de navegacion
                    if (numberpage == pages) {$('.next-vendor-list').hide();}
                    else {$('.next-vendor-list').show();}
                    if (numberpage > 1) {$('.back-vendor-list').show();}
                    else {$('.back-vendor-list').hide();}
                    break;
                case 404:
                    console.log(data);
                    break;
                default:
            }
        }, error: (req, msg, error)=>{
          console.log(req);
        }
    }).done(()=>{
        loadingcatalogs();
        cleanUndefined();
    });
}

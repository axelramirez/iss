<?php

$iniFile = parse_ini_file("settings/settings.ini");
$host = $iniFile['host'];
$user = $iniFile['user'];
$pass = $iniFile['pass'];
$dataBase = $iniFile['db'];

if (!isset($mysql) || $mysql != false) {
    $db = mysql_connect($host, $user, $pass) or
            die("Unable to connect: " . mysql_error());
    mysql_select_db($dataBase, $db) or
            die("Unable to select database: " . mysql_error());
    mysql_query("SET NAMES 'utf8'");
}

if (isset($mysqli) && $mysqli) {
    $mysqli = @new mysqli($host, $user, $pass, $dataBase);
    if ($mysqli->connect_error)
        die($mysqli->connect_error);
}
?>

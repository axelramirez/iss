create database solosynergy;
USE solosynergy;
CREATE TABLE IF NOT EXISTS `user` (
  `userid` int(10) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `roleid` int(10) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userid`),
  UNIQUE KEY `login` (`login`),
  KEY `roleid` (`roleid`)
);

CREATE TABLE IF NOT EXISTS `log_login` (
  `idLog` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NULL DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(15) NULL DEFAULT NULL,
  `observation` varchar(30) NULL DEFAULT NULL,
  PRIMARY KEY (`idLog`)
);

INSERT INTO user(login, password, roleid, firstname, lastname)
VALUES('axelramirez', 'holacrayola', 0, 'Axel', 'Ramirez');

CREATE TABLE IF NOT EXISTS `producto` (
id int NOT NULL AUTO_INCREMENT,
name varchar(255),
image varchar(255),
description varchar(255),
quantity int(255),
idcolor int(11),
PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS `setproduct` (
id int NOT NULL AUTO_INCREMENT,
name varchar(255),
image varchar(255),
description varchar(255),
idproducts varchar(255),
idbolt int(11),
bolt int(10),
nut int(10),
PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS `color` (
id int NOT NULL AUTO_INCREMENT,
name varchar(255),
description varchar(255),
PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS `tornillos` (
id int NOT NULL AUTO_INCREMENT,
name varchar(255),
image varchar(255),
description varchar(255),
quantity int(255),
idcolor int (11),
PRIMARY KEY (id)
);

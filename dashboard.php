<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Inventory Dashboard</title>
    <?php include('header.php'); ?>
    <link href="style/dashboard.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <?php include('menu.php'); ?>
    <main>
      <h1>Inventario de productos</h1>
      <h2 class="information">La cantidad de unidades por producto se muestra de color. Cada color representa
      la importancia de atender el surtido de ese producto.</h2>
      <div class="info-colors">
        <div class="color-box red-circle">
          <p>Menor a 100 unidades</p>
        </div>
        <div class="color-box orange-circle">
          <p>Menor a 200 unidades</p>
        </div>
        <div class="color-box green-circle">
          <p>Mayor a 300 unidades</p>
        </div>
      </div>
      <div class="products-quantity">
        <table
        </table>
      </div>
    </main>
  </body>
  <script src="js/dashboard/app.js"></script>
</html>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <?php include('header.php'); ?>
    <title>Inventory Menu</title>
    <link href="style/inventory.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <?php include('menu.php'); ?>
    <div class="box-shadow" hidden></div>
    <div class="info-product-box" hidden>
      <div class="title-dialog">
        <h1>Información del producto</h1>
        <img src="images/icons/close.svg" class="closeInformationItem">
      </div>
      <div class="section-data"></div>
    </div>
    <div class="box-confirmation-not">
        <span class="message-dialog-not"></span>
        <div>
            <input type="button" class="yes-confirmation" name="" value="Sí, estoy seguro">
            <input type="button" class="no-confirmation" name="" value="No">
        </div>
    </div>
    <div class="new-product-box" hidden>
      <div class="title-dialog">
        <h1>Crear nuevo Producto</h1>
        <img src="images/icons/close.svg" class="close-newproduct">
      </div>
      <form id="inserProduct">
        <div class="inputs-product-info">
          <div>
            <p>Nombre del producto</p>
            <input id="product-name" type="text" name="" value="" required>
          </div>
          <div>
            <p>Imagen del producto</p>
            <input id="product-image" type="text" name="" placeholder="Inserta el url de la imagen" value="">
          </div>
          <div>
            <p>Descripción</p>
            <input id="product-description" type="text" name="" value="" required>
          </div>
          <div>
            <p>Color</p>
            <select id="product-color" name="">
            </select>
          </div>
          <div>
            <p>Cantidad</p>
            <input id="product-quantity" type="number" name="" value="" required>
          </div>
        </div>
      </form>
      <input type="button" name="" class="saveProduct save-button" value="Guardar">
    </div>
    <div class="new-set-box" hidden>
      <div class="title-dialog">
        <h1>Crear nuevo Set</h1>
        <img src="images/icons/close.svg" class="close-newset">
      </div>
      <p class="info-set">Selecciona 1 o mas productos para crear un Set</p>
      <form id="insertSetSection">
        <div class="inputs-product-info">
          <div>
            <p>Nombre del Set</p>
            <input id="set-name" type="text" name="" value="" required>
          </div>
          <div>
            <p>Imagen del Set</p>
            <input id="set-image" type="text" name="" placeholder="Inserta el url de la imagen" value="">
          </div>
          <div>
            <p>Productos del Set</p>
            <div class="setItemsSection">
              <input type="button" name="" class="openItemsDB create-button" value="Seleccionar Productos">
              <input type="text" class="productsSet" value="" readonly required hidden>
            </div>
          </div>
          <div>
            <p>Descripción</p>
            <input id="set-description" type="text" name="" value="" required>
          </div>
          <div>
            <p>Tornillos</p>
            <div class="section-select-tornillos">
              <input type="number" class="qtyBolt" placeholder="Cantidad" value="" required>
              <select id="set-bolt" name=""></select>
            </div>
          </div>
        </div>
      </form>
      <div class="set-items-box" hidden>
        <div class="back-set-box">
          <img src="images/icons/back.svg" alt="">
          <p>Regresar</p>
        </div>
        <div id="showItemsToSet"></div>
        <input type="button" class="insertSelectItems save-button" value="Insertar Seleccionados">
      </div>
      <input type="button" name="" class="saveSet save-button" value="Guardar">
    </div>
    <div class="capture-receipt" hidden>
      <div class="title-dialog">
        <h1>Cantidad de mercancía</h1>
        <img src="images/icons/close.svg" class="close-capture-receipt">
      </div>
      <div class="name-receipt-product">
        <p></p>
      </div>
      <form id="insertReceipt">
        <div>
          <p>Ingresa la cantidad de mercancía</p>
          <input type="number" name="" class="qtyReceipt" value="" required>
        </div>
      </form>
      <input type="button" class="save-button insertReceiptProduct" name="" value="Insertar">
    </div>
    <div class="capture-shipping" hidden>
      <div class="title-dialog">
        <h1>Salida de mercancía</h1>
        <img src="images/icons/close.svg" class="close-capture-shipping">
      </div>
      <div class="name-shipping-product">
        <p></p>
      </div>
      <form id="insertShipping">
        <div>
          <p>Ingresa la cantidad de mercancía por salir</p>
          <input type="number" name="" class="qtyShipping" value="" required>
        </div>
      </form>
      <input type="button" class="save-button insertShippingProduct" name="" value="Guardar">
    </div>
    <main>
      <div class="menu-options">
        <h1>Hola, Axel. Selecciona una opción.</h1>
        <div class="option-inventory">
          <div class="search-inventory">
            <img src="images/icons/search.svg" alt="">
            <p>Buscar en Inventario</p>
          </div>
          <div class="receipt-inventory">
            <img src="images/icons/receipt.svg" alt="">
            <p>Entrada de Productos</p>
          </div>
          <div class="shipping-inventory">
            <img src="images/icons/shipping.svg" alt="">
            <p>Salida de Productos</p>
          </div>
          <div class="inventory-control">
            <img src="images/icons/control.svg" alt="">
            <p>Control de Inventario</p>
          </div>
        </div>
      </div>
      <div id="search-inventory" hidden>
        <h1>Buscar productos</h1>
        <div class="total-items">
          <div class="info-total-items">
            <h2>Productos totales en Inventario.</h2>
            <div>
              <label></label>
              <p>piezas</p>
            </div>
          </div>
          <div class="search-section-box">
            <input type="text" name="" value="" placeholder="Buscar por nombre o descripcion" class="filterInput">
            <input type="button" class="save-button searchItem" name="" value="Buscar">
          </div>
          <div class="items-box">
            <table></table>
          </div>
        </div>
      </div>
      <div id="inventory" hidden>
        <h1>Control de Productos</h1>
        <div class="section-control-products">
          <input type="button" class="createProduct create-button" value="Crear nuevo producto">
          <input type="button" class="createSet create-button" value="Crear nuevo Set">
        </div>
        <div class="onlyItems overflowTable">
            <table></table>
        </div>
        <div class="onlySet overflowTable">
          <table></table>
        </div>
      </div>
      <div id="receipt-inventory" hidden>
        <h1>Entrada de Productos</h1>
        <p class="info-receipt">Selecciona un producto para capturar mercancía.</p>
        <div class="items-receipt-box">

        </div>
      </div>
      <div id="shipping-inventory" hidden>
        <h1>Salida de Productos</h1>
        <p class="info-shipping">Selecciona un producto para descontar mercancía.</p>
        <div class="items-shipping-box">

        </div>
      </div>
    </main>
  </body>
  <script src="js/inventory/app.js"></script>
</html>
